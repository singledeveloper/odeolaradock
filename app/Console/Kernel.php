<?php

namespace Odeo\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use Odeo\Domains\Account\Scheduler\DebtChecker;
use Odeo\Domains\Account\Scheduler\RemoveExpiredTokenScheduler;
use Odeo\Domains\Account\User\Scheduler\UserDormantScheduler;
use Odeo\Domains\Account\Userreport\Scheduler\UserReportRefresher;
use Odeo\Domains\Activity\Scheduler\AutoRecordSKUDataScheduler;
use Odeo\Domains\Activity\Scheduler\AutoSendPPOBReportScheduler;
use Odeo\Domains\Activity\Scheduler\AutoSendReportScheduler;
use Odeo\Domains\Approval\Scheduler\DispatchApprovalPendingScheduler;
use Odeo\Domains\Biller\Blackhawk\Scheduler\TransactionChecker;
use Odeo\Domains\Biller\TCash\Scheduler\PurchaseResetter;
use Odeo\Domains\Biller\TCash\Scheduler\SessionChecker;
use Odeo\Domains\Core\Scheduler;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Scheduler\PulsaAutoChanger;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Scheduler\PulsaAutoPendingDisabler;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Scheduler\SwitcherForceSuccessBalancer;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Scheduler\SwitcherVoidFailBalancer;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Scheduler\LateChargeUpdater;
use Odeo\Domains\Invoice\Scheduler\SettleInvoicePaymentScheduler;
use Odeo\Domains\Marketing\Commands\RefreshMultiplier;
use Odeo\Domains\Marketing\Commands\RefreshRevenue;
use Odeo\Domains\Marketing\Rateapp\Scheduler\RateScheduler;
use Odeo\Domains\Marketing\Scheduler\BudgetRefresher;
use Odeo\Domains\Marketing\Scheduler\BusinessBonusAccumulator;
use Odeo\Domains\Marketing\Scheduler\BusinessBonusSender;
use Odeo\Domains\Marketing\Scheduler\LastDepositRecorder;
use Odeo\Domains\Marketing\Scheduler\StoreRevenueRefresher;
use Odeo\Domains\Network\Commands\RefreshStoreReferredList;
use Odeo\Domains\Order\Salesreport\Scheduler\SalesReportRefresher;
use Odeo\Domains\Order\Scheduler\AutoCancelScheduler;
use Odeo\Domains\Order\Scheduler\AutoConfirmScheduler;
use Odeo\Domains\Order\Scheduler\BulkPurchaseScheduler;
use Odeo\Domains\Order\Scheduler\PulsaRecurringDeactivateScheduler;
use Odeo\Domains\Order\Scheduler\PulsaRecurringScheduler;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Commands\ScrapeBri;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Commands\ScrapeMandiri;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Scheduler\AutoVerifyScheduler;
use Odeo\Domains\Payment\Odeo\CompanyCash\Scheduler\CompanyCashTransactionScheduler;
use Odeo\Domains\PaymentGateway\Scheduler\DispatchSettlementScheduler;
use Odeo\Domains\Reminder\Scheduler\BillReminderScheduler;
use Odeo\Domains\Reminder\Scheduler\RemoveExpiredBillReminderScheduler;
use Odeo\Domains\Subscription\ManageInventory\Scheduler\StoreInventoryUpdatePricingScheduler;
use Odeo\Domains\Subscription\ManageInventory\Scheduler\StoreInventoryUpdateUserGroupPrice;
use Odeo\Domains\Subscription\MdPlus\Scheduler\AutoRenewScheduler;
use Odeo\Domains\Subscription\Scheduler\RenewalChecker;
use Odeo\Domains\Support\Qiscus\Scheduler\QiscusBackupScheduler;
use Odeo\Domains\Transaction\Scheduler\CashRecurringScheduler;
use Odeo\Domains\Transaction\Scheduler\CashSettler;
use Odeo\Domains\Transaction\Scheduler\CashSnapshooter;
use Odeo\Domains\Transaction\Scheduler\ExportCleaner;
use Odeo\Domains\Vendor\Jabber\Commands\StartConnection;

class Kernel extends ConsoleKernel {

  protected $commands = [

    /*refresh margin multiplier*/
    RefreshMultiplier::class,
    RefreshRevenue::class,

    RefreshStoreReferredList::class,

    /*bank scraper*/
    ScrapeBri::class,
    ScrapeMandiri::class,

    /*jabber*/
    StartConnection::class,

    TestCommand::class,

    /*Agent feature*/
    //\Odeo\Domains\Subscription\ManageInventory\Commands\MigrateToAgentFeature::class,

    /*Distribution Channel Feature*/
    //\Odeo\Domains\Subscription\DistributionChannel\Commands\PopulateChannel::class,
  ];

  protected function schedule(Schedule $ch) {
    $scheduler = new Scheduler($ch);

    $legacyScheduler = new LegacyScheduler();
    $legacyScheduler->run($scheduler);

    $prodScheduler = new ProdScheduler();
    $prodScheduler->run($scheduler);

    $scheduler->daily('business_bonus_accumulator_scheduler',
      function () {
        (new BusinessBonusAccumulator)->run();
      })->at('02:00');

    $scheduler->everyFiveMinutes('debt_checker_scheduler',
      function () {
        (new DebtChecker)->run();
      });

    $scheduler->everyMinute('cash_recurring_scheduler',
      function () {
        (new CashRecurringScheduler)->run();
      });

    $scheduler->everyFiveMinutes('export_downloader_cleaner_scheduler',
      function () {
        (new ExportCleaner)->run();
      });

    $scheduler->everyMinute('temp_bulk_purchase_scheduler', function () {
      (new BulkPurchaseScheduler)->run();
    });

    $scheduler->everyMinute('business_bonus_sender_scheduler', function(){
      (new BusinessBonusSender)->run();
    });

    $scheduler->everyFiveMinutes('dispatch_approval_pending_scheduler', function() {
      (new DispatchApprovalPendingScheduler())->run();
    });

    $scheduler->everyMinute('store_inventory_update_pricing_scheduler',
      function () {
        (new StoreInventoryUpdatePricingScheduler)->run();
      });

    $scheduler->everyMinute('store_inventory_update_user_group_price_scheduler',
      function () {
        (new StoreInventoryUpdateUserGroupPrice)->run();
      });

    $scheduler->everyMinute('auto_verify_order_from_bank_mutation_scheduler',
      function () {
        (new AutoVerifyScheduler)->run();
      });

    $scheduler->everyFiveMinutes('auto_cancel_scheduler',
      function () {
        (new AutoCancelScheduler)->run();
      });

    $scheduler->everyMinute('auto_confirm_scheduler',
      function () {
        (new AutoConfirmScheduler)->run();
      });

    $scheduler->everyTenMinutes('pulsa_auto_changer_scheduler',
      function () {
        (new PulsaAutoChanger)->run();
      });

    $scheduler->everyTenMinutes('switcher_void_fail_balancer_scheduler',
      function () {
        (new SwitcherVoidFailBalancer)->run();
      });

    $scheduler->everyFiveMinutes('pulsa_auto_pending_disabler_scheduler',
      function () {
        (new PulsaAutoPendingDisabler)->run();
      });

    $scheduler->everyFiveMinutes('switcher_force_success_balancer_scheduler',
      function () {
        (new SwitcherForceSuccessBalancer)->run();
      });

    $scheduler->everyMinute('temp_cash_settler',
      function () {
        (new CashSettler)->run();
      });

    $scheduler->everyTenMinutes('ocommerce_sales_report_scheduler',
      function () {
        (new SalesReportRefresher)->runOcommerceSalesReport();
      });

    $scheduler->everyTenMinutes('payment_sales_report_scheduler',
      function () {
        (new SalesReportRefresher)->runPaymentSalesReport();
      });

    $scheduler->everyTenMinutes('user_report_scheduler',
      function () {
        (new UserReportRefresher)->run();
      });

    $scheduler->daily('user_dormant_scheduler',
      function () {
        (new UserDormantScheduler)->run();
      });
    
    $scheduler->daily('qiscus_backup_scheduler',
      function () {
        (new QiscusBackupScheduler)->run();
      })->at('01:00');

    $scheduler->daily('remove_expired_token_scheduler',
      function () {
        (new RemoveExpiredTokenScheduler)->run();
      });

    $scheduler->daily('auto_send_report_scheduler',
      function () {
        (new AutoSendReportScheduler)->run();
      })->at('00:15');

    $scheduler->daily('auto_send_ppob_report_scheduler',
      function () {
        (new AutoSendPPOBReportScheduler)->run();
      })->at('00:15');

    $scheduler->daily('auto_record_sku_data_scheduler',
      function () {
        (new AutoRecordSKUDataScheduler)->run();
      })->at('00:45');

    $scheduler->daily('renewal_checker_scheduler',
      function () {
        (new RenewalChecker)->check();
      });

    $scheduler->daily('budget_refresher_scheduler',
      function () {
        (new BudgetRefresher)->run();
      })->at('00:05');

    $scheduler->daily('last_deposit_recorder_scheduler',
      function () {
        (new LastDepositRecorder)->run();
      })->at('03:00');

    $scheduler->hourly('store_revenue_refresher_run_scheduler',
      function () {
        (new StoreRevenueRefresher)->run();
      });

    $scheduler->daily('store_revenue_refresher_refresh_scheduler',
      function () {
        (new StoreRevenueRefresher)->refresh();
      })->at('00:05');

    $scheduler->daily('user_invoice_late_charge_updater_scheduler',
      function () {
        (new LateChargeUpdater)->run();
      });

    $scheduler->everyMinute('marketing_rate_scheduler',
      function () {
        (new RateScheduler)->run();
      });

    $scheduler->everyMinute('pulsa_recurring_scheduler',
      function () {
        (new PulsaRecurringScheduler)->run();
      });

    $scheduler->daily('deactivate_pulsa_recurring_scheduler',
      function () {
        (new PulsaRecurringDeactivateScheduler)->run();
      });

    $scheduler->everyMinute('bill_reminder_scheduler',
      function () {
        (new BillReminderScheduler)->run();
      });

    $scheduler->daily('remove_expired_bill_reminder_scheduler',
      function () {
        (new RemoveExpiredBillReminderScheduler)->run();
      });

    $scheduler->everyFiveMinutes('tcash_session_checker_scheduler',
      function () {
        (new SessionChecker)->run();
      });

    /*$scheduler->daily('tcash_deposit_distribute_scheduler',
      function () {
        (new PurchaseResetter)->run();
      })->at('07:31');

    $scheduler->daily('tcash_purchase_resetter_scheduler',
      function () {
        (new PurchaseResetter)->reset();
      });*/

    $scheduler->everyFiveTeenMinutes('blackhawk_transaction_checker_scheduler',
      function () {
        (new TransactionChecker)->run();
      });

    $scheduler->daily('md_plus_auto_renew_scheduler',
      function () {
        (new AutoRenewScheduler)->run();
      })->at('00:05');

    /*$scheduler->everyFiveMinutes('company_cash_transaction_updater',
      function () {
        (new CompanyCashTransactionScheduler)->run();
      });*/

    $scheduler->daily('cash_snapshooter',
      function () {
        (new CashSnapshooter)->run();
      });

    $scheduler->everyMinute('dispatch_pg_settlement', function () {
      (new DispatchSettlementScheduler())->run();
    });

    $scheduler->everyFiveMinutes('settle_invoice_payment_scheduler', function() {
      (new SettleInvoicePaymentScheduler())->run();
    });
  }
}
