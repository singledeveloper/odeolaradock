<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 27/04/18
 * Time: 19.10
 */

namespace Odeo\Console;

use Odeo\Domains\Constant\ServerConfig;
use Odeo\Domains\Core\Scheduler;
use Odeo\Domains\Disbursement\BillerReplenishment\Setting\Scheduler\JabberHistoryChecker;
use Odeo\Domains\Supply\Biller\Scheduler\TransactionResender;
use Odeo\Domains\Vendor\Jabber\Scheduler\Pinger;

class LegacyScheduler {

  public function run(Scheduler $scheduler) {
    if (env('SERVER_NICKNAME') != ServerConfig::SERVER_C) return;
    if (!isProduction()) return;

    $scheduler->everyMinute('replenishment_jabber_history_checker_scheduler', function () {
      (new JabberHistoryChecker)->run();
    });

    $scheduler->everyFiveMinutes('auto_jabber_transaction_checker_scheduler', function () {
      (new TransactionResender)->run();
    });

    $scheduler->everyMinute('jabber_odeo_pinger_scheduler', function () {
      (new Pinger)->run();
    });
  }

}
