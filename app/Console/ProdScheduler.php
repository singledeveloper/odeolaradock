<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 27/04/18
 * Time: 19.31
 */

namespace Odeo\Console;

use Carbon\Carbon;
use Odeo\Domains\Accounting\Scheduler\AccountingReportGenerator;
use Odeo\Domains\Agent\Scheduler\AgentRequestCanceller;
use Odeo\Domains\Biller\Gojek\Scheduler\GojekOnHoldChecker;
use Odeo\Domains\Biller\Gojek\Scheduler\GojekRedisChecker;
use Odeo\Domains\Biller\Gojek\Scheduler\GojekSalesChecker;
use Odeo\Domains\Biller\Gojek\Scheduler\GojekVaInquiryChecker;
use Odeo\Domains\Biller\Gojek\Scheduler\GojekVersionUpdater;
use Odeo\Domains\Biller\TCash\Scheduler\TCashVaInquiryChecker;
use Odeo\Domains\Core\Scheduler;
use Odeo\Domains\Disbursement\ApiDisbursement\Bca\Scheduler\UpdateApiDisbursementViaBcaCostScheduler;
use Odeo\Domains\Disbursement\ApiDisbursement\Scheduler\ApiDisbursementDailyReportScheduler;
use Odeo\Domains\Disbursement\ApiDisbursement\Scheduler\ApiDisbursementMonthlyInvoiceScheduler;
use Odeo\Domains\Disbursement\ApiDisbursement\Scheduler\ApiDisbursementSuspectNotRunningAlertScheduler;
use Odeo\Domains\Disbursement\ApiDisbursement\Scheduler\DelayedApiDisbursementNotRunningAlertScheduler;
use Odeo\Domains\Disbursement\ApiDisbursement\Scheduler\DelayedApiDisbursementScheduler;
use Odeo\Domains\Disbursement\BillerReplenishment\Scheduler\AutoReplenishmentScheduler;
use Odeo\Domains\Disbursement\BillerReplenishment\Scheduler\DailyReplenishmentScheduler;
use Odeo\Domains\Disbursement\BillerReplenishment\Scheduler\ReplenishmentStatusChecker;
use Odeo\Domains\Disbursement\Scheduler\VendorDisbursementLowBalanceAlertScheduler;
use Odeo\Domains\Disbursement\Withdrawal\Scheduler\AutoWithdrawDisbursementScheduler;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Scheduler\MutationChecker;
use Odeo\Domains\Inventory\Pln\Scheduler\TempPlnAutoScrapper;
use Odeo\Domains\Marketing\Competitor\Payfazz\Scheduler\PriceScraperScheduler;
use Odeo\Domains\Order\OrderAccumulator;
use Odeo\Domains\Order\Reconciliation\Scheduler\OrderReconcileScheduler;
use Odeo\Domains\Order\Reconciliation\Scheduler\OrderReconciliationSummarizer;
use Odeo\Domains\Payment\Akulaku\Scheduler\PaymentConfirmerScheduler;
use Odeo\Domains\Payment\Akulaku\Scheduler\PaymentVerificationScheduler;
use Odeo\Domains\Payment\Akulaku\Scheduler\PaymentVoidScheduler;
use Odeo\Domains\Payment\Artajasa\Va\Scheduler\ArtajasaVaReconScheduler;
use Odeo\Domains\Payment\Artajasa\Va\Scheduler\CheckStatusScheduler as ArtajasaCheckStatusScheduler;
use Odeo\Domains\Payment\Cimb\Scheduler\CheckStatusScheduler as CimbCheckStatusScheduler;
use Odeo\Domains\Payment\Doku\VaDirect\Scheduler\CheckStatusScheduler as DokuCheckStatusScheduler;
use Odeo\Domains\Payment\Doku\VaDirect\Scheduler\CheckPaymentScheduler as DokuCheckPaymentScheduler;
use Odeo\Domains\Payment\Doku\VaDirect\Scheduler\DokuVaReconScheduler;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Scheduler\ReconBriAccountStatementScheduler;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Scheduler\CimbInquirySequenceScheduler;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Scheduler\ScrapeReconCimbInquiryScheduler;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Scheduler\ReconMandiriStatementScheduler;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Scheduler\ReconMandiriGiroStatementScheduler;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Scheduler\PermataInquirySequenceScheduler;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Scheduler\ReconPermataStatementScheduler;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Scheduler\CheckBankMutationScraperScheduler;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Scheduler\ReconMissingMutationScheduler;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Doku\Scheduler\RecordDokuTransactionScheduler;
use Odeo\Domains\Payment\Pax\Bni\Scheduler\CheckUnsettledBniEdcTransaction;
use Odeo\Domains\Payment\Pax\Bni\Scheduler\ReconBankBniEdcSettlement;
use Odeo\Domains\Payment\Pax\Bni\Scheduler\ReconBankBniEdcSettlementV2;
use Odeo\Domains\Payment\Pax\Scheduler\AutoSuccessUnknownBniTransaction;
use Odeo\Domains\Payment\Pax\Scheduler\ParseBniSettlementAttachment;
use Odeo\Domains\Payment\Pax\Scheduler\ReconSettledBniPaxPayments;
use Odeo\Domains\Payment\Pax\Scheduler\QueryTerminalTransaction;
use Odeo\Domains\Payment\Permata\Va\Scheduler\CheckStatusScheduler as PermataCheckStatusScheduler;
use Odeo\Domains\Payment\Prismalink\VaDirect\Scheduler\CheckStatusScheduler as PrismalinkCheckStatusScheduler;
use Odeo\Domains\Payment\Prismalink\VaDirect\Scheduler\PrismalinkVaDirectReconScheduler;
use Odeo\Domains\PaymentGateway\Scheduler\PaymentGatewayMonthlyInvoiceScheduler;
use Odeo\Domains\PaymentGateway\Scheduler\PaymentGatewayUsageScheduler;
use Odeo\Domains\PaymentGateway\Scheduler\RetrySuspectTransactions;
use Odeo\Domains\Subscription\ManageInventory\Scheduler\StoreInventorySyncInventory;
use Odeo\Domains\Supply\Biller\Scheduler\TransactionChecker as SupplyTransactionChecker;
use Odeo\Domains\Support\Zoho\Scheduler\DownloadBniEdcSettlementScheduler;
use Odeo\Domains\Support\Zoho\Scheduler\FetchBniEdcSettlementScheduler;
use Odeo\Domains\Transaction\Scheduler\BankMutationApiDisbursementReconScheduler;
use Odeo\Domains\Transaction\Scheduler\BankMutationBillerReplenishmentReconScheduler;
use Odeo\Domains\Transaction\Scheduler\BankMutationWithdrawReconScheduler;
use Odeo\Domains\Transaction\Scheduler\BankMutationTransferReconScheduler;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Scheduler\BalanceChecker;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Scheduler\NetworkChecker;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Scheduler\TimeoutTransferStatusInquiryScheduler;
use Odeo\Domains\Vendor\Bca\Scheduler\FetchMutationScheduler as BcaFetchMutationScheduler;
use Odeo\Domains\Vendor\Bca\Scheduler\CheckBalanceScheduler as BcaCheckBalanceScheduler;
use Odeo\Domains\Vendor\Bni\Scheduler\AutoRecordBalanceScheduler;
use Odeo\Domains\Vendor\Bri\Scheduler\AutoRecordBalanceScheduler as BriAutoRecordBalanceScheduler;
use Odeo\Domains\Vendor\Cimb\Scheduler\CheckBalanceScheduler;
use Odeo\Domains\Vendor\Permata\Scheduler\CheckBalanceScheduler as PermataCheckBalanceScheduler;
use Odeo\Domains\Vendor\Mandiri\Scheduler\CheckBalanceScheduler as MandiriCheckBalanceScheduler;
use Odeo\Domains\Vendor\Redig\Scheduler\CheckRedigBalance;
use Odeo\Domains\Vendor\SMS\Obox\Scheduler\StatusChecker;

class ProdScheduler {

  public function run(Scheduler $scheduler) {
    if (!isProduction()) return;

    $scheduler->daily('gojek_sales_checker_scheduler', function () {
      (new GojekSalesChecker)->run();
    })->at('03:00');

    $scheduler->daily('gojek_version_updater_scheduler', function () {
      (new GojekVersionUpdater)->run();
    })->at('02:00');

    $scheduler->everyMinute('gojek_onhold_checker_scheduler', function () {
      (new GojekOnHoldChecker)->run();
    });

    $scheduler->everyFiveMinutes('fetch_bca_mutation_scheduler', function () {
      (new BcaFetchMutationScheduler)->run();
    })->when(function () {
      return date('H') >= 1 && date('H') <= 20;
    });

    $scheduler->daily('order_accumulator_scheduler', function () { // temporary
      (new OrderAccumulator)->run();
    })->at('00:20');

    $scheduler->daily('ppob_mutation_checker_scheduler', function () {
      (new MutationChecker)->run();
    })->at('00:25');

    // a
    $scheduler->everyFiveTeenMinutes('obox_status_checker_scheduler', function () {
      (new StatusChecker)->run();
    });

    $scheduler->everyThirtyMinutes('check_bank_mutation_scraper_scheduler', function () {
      (new CheckBankMutationScraperScheduler)->run();
    });

    $scheduler->everyFiveMinutes('supply_transaction_checker_scheduler', function () {
      (new SupplyTransactionChecker)->run();
    });

    $scheduler->everyFiveMinutes('gojek_va_checker_scheduler', function () {
      (new GojekVaInquiryChecker)->run();
    });

    $scheduler->monthlyOn('gojek_va_resetter_scheduler', function () {
      (new GojekVaInquiryChecker)->reset();
    }, 1, "00:00");

    $scheduler->everyMinute('gojek_redis_checker_scheduler', function () {
      (new GojekRedisChecker)->run();
    });

    $scheduler->everyFiveMinutes('tcash_va_checker_scheduler', function () {
      (new TCashVaInquiryChecker)->run();
    });

    $scheduler->monthlyOn('tcash_va_resetter_scheduler', function () {
      (new TCashVaInquiryChecker)->reset();
    });

    /*$scheduler->everyMinute('temp_pln_scrapper_scheduler', function () {
      (new TempPlnAutoScrapper)->run();
    });*/

    $scheduler->everyMinute('agent_request_canceller_scheduler', function () {
      (new AgentRequestCanceller)->run();
    });

//    $scheduler->everyTenMinutes('transfer_suspect_scheduler', function () {
//      (new TransferSuspectScheduler)->run();
//    });

    $scheduler->everyMinute('auto_withdraw_disbursement_scheduler', function () {
      (new AutoWithdrawDisbursementScheduler)->run();
    });

    $scheduler->everyMinute('disbursement_artajasa_timeout_transfer_inquiry_scheduler', function () {
      (new TimeoutTransferStatusInquiryScheduler)->run();
    });

    $scheduler->everyMinute('disbursement_artajasa_network_check_scheduler', function () {
      (new NetworkChecker)->run();
    });

    $scheduler->everyFiveMinutes('disbursement_artajasa_balance_check_scheduler', function () {
      (new BalanceChecker)->run();
    });

//    $scheduler->everyTenMinutes('flip_check_balance_scheduler', function () {
//      (new \Odeo\Domains\Vendor\Flip\Scheduler\CheckBalanceScheduler)->run();
//    });

    $scheduler->hourly('bank_mutation_withdraw_recon_scheduler', function () {
      (new BankMutationWithdrawReconScheduler)->run();
    });

    $scheduler->hourly('bank_mutation_transfer_recon_scheduler', function () {
      (new BankMutationTransferReconScheduler)->run();
    });

    $scheduler->hourly('bank_mutation_biller_replenishment_recon_scheduler', function () {
      (new BankMutationBillerReplenishmentReconScheduler)->run();
    });

    $scheduler->everyFiveMinutes('bank_mutation_api_disbursement_recon_scheduler', function () {
      (new BankMutationApiDisbursementReconScheduler)->run();
    });

    $scheduler->everyThirtyMinutes('akulaku_payment_verification_scheduler', function () {
      (new PaymentVerificationScheduler)->run();
    });

    $scheduler->everyThirtyMinutes('akulaku_payment_confirmation_scheduler', function () {
      (new PaymentConfirmerScheduler)->run();
    });

    $scheduler->everyFiveMinutes('akulaku_payment_void_scheduler', function () {
      (new PaymentVoidScheduler)->run();
    });

    $scheduler->everyMinute('auto_biller_replenishment_disbursement_scheduler', function () {
      (new AutoReplenishmentScheduler)->run();
    });

    $scheduler->everyFiveMinutes('auto_biller_replenishment_status_checker_scheduler', function () {
      (new ReplenishmentStatusChecker)->run();
    });

    $scheduler->everyThreeHour('query_terminal_transaction', function () {
      (new QueryTerminalTransaction)->run();
    });

    $scheduler->everyMinute('check_status_prismalink_payment_scheduler', function () {
      (new PrismalinkCheckStatusScheduler())->run();
    });

    $scheduler->everyMinute('check_status_doku_payment_scheduler', function () {
      (new DokuCheckStatusScheduler())->run();
    });

    $scheduler->everyMinute('check_status_artajasa_payment_scheduler', function () {
      (new ArtajasaCheckStatusScheduler())->run();
    });

    $scheduler->everyMinute('check_status_permata_payment_scheduler', function () {
      (new PermataCheckStatusScheduler())->run();
    });

    // $scheduler->everyMinute('check_status_mandiri_payment_scheduler', function () {
    //   (new MandiriCheckStatusScheduler())->run();
    // });

    $scheduler->everyMinute('check_status_cimb_payment_scheduler', function () {
      (new CimbCheckStatusScheduler())->run();
    });

    $scheduler->hourly('check_payment_doku_scheduler', function () {
      (new DokuCheckPaymentScheduler())->run();
    });

    $scheduler->daily('daily_replenishment_morning_scheduler', function () {
      (new DailyReplenishmentScheduler(
        Carbon::now()->subDays(1)->format('Y-m-d') . ' 10:00:00',
        Carbon::now()->subDays(0)->format('Y-m-d') . ' 09:59:59'
      ))->run();
    })->at('10:00');

    $scheduler->daily('daily_replenishment_noon_scheduler', function () {
      (new DailyReplenishmentScheduler(
        Carbon::now()->subDays(1)->format('Y-m-d') . ' 16:00:00',
        Carbon::now()->subDays(0)->format('Y-m-d') . ' 09:59:59'
      ))->run();
    })->at('16:00');

//    $scheduler->daily('sendgrid_contact_updater_scheduler', function () {
//      (new SendgridContactUpdater)->run();
//    })->at('01:00');

    $scheduler->everyThirtyMinutes('store_inventory_sync_scheduler', function () {
      (new StoreInventorySyncInventory)->run();
    })->when(function () {
      return date('H') >= 2 && date('H') <= 4;
    });

    $scheduler->daily('payfazz_price_scraper_scheduler', function () {
      (new PriceScraperScheduler)->run();
    })->at('00:30');

    $scheduler->daily('bank_transfer_recon_missing_mutation_scheduler', function () {
      (new ReconMissingMutationScheduler)->run();
    })->at('00:35');

    $scheduler->everyFiveMinutes('order_reconcile_init_scheduler', function () {
      (new OrderReconcileScheduler)->init();
    });

    $scheduler->hourly('order_reconcile_scheduler', function () {
      (new OrderReconcileScheduler)->run();
    });

    $scheduler->everyFiveMinutes('order_reconciliation_summarizer_scheduler', function () {
      (new OrderReconciliationSummarizer)->run();
    });

    // $scheduler->daily('mandiri_ecash_scraper_scheduler', function () {
    //   (new MandiriEcashScraperScheduler)->run();
    // })->at('02:00');

    // $scheduler->daily('mandiri_ecash_reconciliation_scheduler', function () {
    //   (new ReconcileMandiriEcashScheduler)->run();
    // })->at('02:30');

    // $scheduler->daily('record_doku_transaction_scheduler', function () {
    //   (new RecordDokuTransactionScheduler)->run();
    // })->at('02:00');

    // $scheduler->daily('mandiri_ecash_recon_mutation_settlement_scheduler', function () {
    //   (new MandiriEcashReconMutationSettlementScheduler)->run();
    // })->at('03:00');

    $scheduler->everyFiveMinutes('cimb_check_balance_scheduler', function () {
      (new CheckBalanceScheduler)->run();
    });

    $scheduler->everyFiveMinutes('permata_check_balance_scheduler', function () {
      (new PermataCheckBalanceScheduler)->run();
    });

    $scheduler->everyFiveMinutes('mandiri_check_balance_scheduler', function () {
      (new MandiriCheckBalanceScheduler())->run();
    });

    $scheduler->everyFiveMinutes('bca_check_balance_scheduler', function () {
      (new BcaCheckBalanceScheduler())->run();
    });

    $scheduler->everyMinute('fetch_cimb_mutation_scheduler', function () {
      (new ScrapeReconCimbInquiryScheduler)->run();
    });

    // $scheduler->everyMinute('fetch_mandiri_mutation_scheduler', function () {
    //   (new ReconMandiriStatementScheduler)->run();
    // })->when(function () {
    //   return date('H') >= 3 && date('H') < 23;
    // });

    // $scheduler->everyMinute('fetch_mandiri_giro_mutation_scheduler', function () {
    //   (new ReconMandiriGiroStatementScheduler)->run();
    // })->when(function () {
    //   return date('H') >= 3 && date('H') < 23;
    // });

    $scheduler->everyMinute('fetch_bri_mutation_scheduler', function () {
      (new ReconBriAccountStatementScheduler)->run();
    })->when(function () {
      return date('H') >= 6 && date('H') < 23;
    });

   $scheduler->everyFiveMinutes('fetch_permata_mutation_scheduler', function() {
     (new ReconPermataStatementScheduler())->run();
   });

   $scheduler->hourly('permata_mutation_sequence_sort_scheduler', function() {
     (new PermataInquirySequenceScheduler())->run();
   });

    $scheduler->hourly('cimb_mutation_sequence_sort_scheduler', function () {
      (new CimbInquirySequenceScheduler)->run();
    });

    $scheduler->daily('payment_gateway_daily_report_scheduler', function () {
      (new PaymentGatewayUsageScheduler)->run();
    })->at('13:00');

    $scheduler->monthlyOn('payment_gateway_monthly_invoice_scheduler', function () {
      (new PaymentGatewayMonthlyInvoiceScheduler)->run();
    }, 1, "13:00");

    $scheduler->daily('api_disbursement_daily_report_scheduler', function () {
      (new ApiDisbursementDailyReportScheduler())->run();
    })->at('13:00');

    $scheduler->monthlyOn('api_disbursement_monthly_invoice_scheduler', function () {
      (new ApiDisbursementMonthlyInvoiceScheduler())->run();
    }, 1, "13:00");

    $scheduler->everyFiveMinutes('api_disbursement_suspect_not_running_alert_scheduler', function () {
      (new ApiDisbursementSuspectNotRunningAlertScheduler())->run();
    });

    $scheduler->everyFiveMinutes('bni_auto_record_balance_scheduler', function () {
      (new AutoRecordBalanceScheduler)->run();
    });

    $scheduler->everyFiveMinutes('bri_auto_record_balance_scheduler', function () {
      (new BriAutoRecordBalanceScheduler)->run();
    });

    $scheduler->everyFiveMinutes('delayed_api_disbursement_scheduler', function () {
      (new DelayedApiDisbursementScheduler())->run();
    });

    $scheduler->everyFiveMinutes('delayed_api_disbursement_not_running_alert_scheduler', function () {
      (new DelayedApiDisbursementNotRunningAlertScheduler())->run();
    });

    $scheduler->everyFiveMinutes('vendor_disbursement_low_balance_alert', function () {
      (new VendorDisbursementLowBalanceAlertScheduler())->run();
    });

    $scheduler->daily('api_disbursement_via_bca_update_cost_scheduler', function () {
      (new UpdateApiDisbursementViaBcaCostScheduler())->run();
    })->at('00:02');

    $scheduler->everyThirtyMinutes('auto_topup_deposit_scheduler', function () {
      (new AutoTopupDeposit())->run();
    });

//    $scheduler->daily('recon_bni_edc_settlement', function () {
//      (new ReconBankBniEdcSettlement())->run();
//    })->at('01:00');
    $scheduler->daily('recon_bni_edc_settlement', function() {
      (new ReconBankBniEdcSettlementV2())->run();
    })->at('01:00');

    $scheduler->daily('recon_settled_bni_pax_payments', function () {
      (new ReconSettledBniPaxPayments())->run();
    })->at('01:10');

    $scheduler->daily('auto_success_unknown_bni_transaction', function () {
      (new AutoSuccessUnknownBniTransaction())->run();
    })->at('01:00');

    $scheduler->daily('check_unsettled_bni_edc_transaction', function () {
      (new CheckUnsettledBniEdcTransaction())->run();
    })->at('01:10');

    $scheduler->dailyAt('fetch_zoho_bni_edc_settlement', function() {
      (new FetchBniEdcSettlementScheduler())->run();
    }, '00:15');

    $scheduler->daily('download_zoho_bni_edc_settlement', function() {
      (new DownloadBniEdcSettlementScheduler())->run();
    })->at('00:20');

    $scheduler->daily('parse_zoho_bni_edc_transaction', function() {
      (new ParseBniSettlementAttachment())->run();
    })->at('00:25');

    $scheduler->daily('prismalink_va_direct_recon_scheduler', function() {
      (new PrismalinkVaDirectReconScheduler())->run();
    })->at('03:15');

    $scheduler->daily('doku_va_recon_scheduler', function() {
      (new DokuVaReconScheduler())->run();
    })->at('03:20');

    $scheduler->daily('artajasa_va_recon_scheduler', function() {
      (new ArtajasaVaReconScheduler())->run();
    })->at('18:00');

    // $scheduler->daily('doku_va_direct_recon_scheduler', function() {
    //   (new DokuVaDirectReconScheduler())->run();
    // })->at('03:20');

    $scheduler->everyFiveTeenMinutes('redig_check_balance', function () {
      (new CheckRedigBalance())->run();
    });

    $scheduler->everyFiveMinutes('retry_pg_suspect_transactions', function() {
      (new RetrySuspectTransactions())->run();
    });

    $scheduler->monthlyOn('accounting_report_generator', function () {
      (new AccountingReportGenerator)->run();
    }, 1, "05:00");
  }
}
