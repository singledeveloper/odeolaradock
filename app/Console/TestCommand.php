<?php

namespace Odeo\Console;

use Illuminate\Console\Command;
use Odeo\Domains\Payment\Pax\Bni\Scheduler\ReconBankBniEdcSettlement;
use Odeo\Domains\Payment\Pax\Scheduler\AutoSuccessUnknownBniTransaction;

class TestCommand extends Command {

  protected $signature = 'test-command {--dry-run=true}';

  protected $description = 'test command';

  public function __construct() {
    parent::__construct();
  }

  private function isDryRun() {
    return $this->option('dry-run') == 'true';
  }

  public function handle() {
    (new ReconBankBniEdcSettlement())->run();
  }
}
