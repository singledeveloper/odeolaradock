<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/2/17
 * Time: 4:35 PM
 */

namespace Odeo\Domains\Account\Appidentifier;

use Odeo\Domains\Account\Appidentifier\Repository\UserAndroidIdentifierRepository;
use Odeo\Domains\Account\Jobs\SendUserBannedAlert;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Account\UserHistoryCreator;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Core\PipelineListener;

class AppIdentifierRegistrar {

  private $appIdentifierRepo, $userRepo, $userHistoryCreator;

  public function __construct() {
    $this->appIdentifierRepo = app()->make(UserAndroidIdentifierRepository::class);
    $this->userRepo = app()->make(UserRepository::class);
    $this->userHistoryCreator = app()->make(UserHistoryCreator::class);
  }

  public function register(PipelineListener $listener, $data) {


    $user = $this->userRepo->findById($data['auth']['user_id']);

    switch ($data['auth']['platform_id']) {

      case Platform::ANDROID:

        if ($temp = $this->appIdentifierRepo->findByUserId($data['auth']['user_id'])) {

          $identifier = $temp;
        } else {

          $identifier = $this->appIdentifierRepo->getNew();
          $identifier->user_id = $data['auth']['user_id'];

          if ($this->checkIsDeviceHasReachLimit($user, $data)) {
            $listener->pushQueue($this->produceSendUserBannedAlertData($user, $data));
          }

        }

        $identifier->ssaid = $data['ssaid'];
        $identifier->instance_id = $data['instance_id'];

        $this->appIdentifierRepo->save($identifier);

        break;

      default:
        return $listener->response(400);
        break;
    }

    return $listener->response(200);

  }


  private function checkIsDeviceHasReachLimit($user, $data) {


    if ($this->appIdentifierRepo->getSSAIDCount($data['ssaid']) < UserStatus::ANDROID_SSAID_LIMIT) {
      return false;
    }

    $user->status = UserStatus::BANNED_DEVICE_SSAID_REACH_LIMIT;
    $this->userRepo->save($user);

    $this->userHistoryCreator->create($user);

    return true;

  }

  private function produceSendUserBannedAlertData($user, $data) {
    return new SendUserBannedAlert([
      'user_id' => $user->id,
      'banned_reason' => 'User (' . $user->email . ') using an Android SSAID Identifier [' . $data['ssaid'] . '] that has registered for ' . UserStatus::ANDROID_SSAID_LIMIT . ' rows or more.'
    ]);
  }



}
