<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Constant\AssetAws;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;

class BankAccountSelector implements SelectorListener {

  private $bankAccount;

  public function __construct() {
    $this->bankAccount = app()->make(\Odeo\Domains\Account\Repository\UserBankAccountRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    $bankAccount = [];
    if ($item->id) $bankAccount["id"] = $item->id;
    if ($repository->hasExpand('user') && $user = $item->user) {
      $bankAccount["user"] = [
        "id" => $user->id,
        "name" => $user->name
      ];
    }

    if ($repository->hasExpand('bank') && $bank = $item->bank) {
      $bankAccount["bank"] = [
        "id" => $bank->id,
        "name" => $bank->name,
        "logo" => AssetAws::getAssetPath(AssetAws::BANK, $bank->logo)
      ];
    }
    if ($item->account_name) $bankAccount["account_name"] = $item->account_name;
    if ($item->account_number) $bankAccount["account_number"] = $item->account_number;
    if ($item->is_own_account) $bankAccount["is_own_account"] = $item->is_own_account;
    if ($item->alias) $bankAccount["alias"] = $item->alias;
    if ($item->created_at) $bankAccount["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    if ($item->updated_at) $bankAccount["updated_at"] = $item->updated_at->format("Y-m-d H:i:s");

    return $bankAccount;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {
    $this->bankAccount->normalizeFilters($data);
    $this->bankAccount->setSimplePaginate(true);

    $bankAccounts = [];
    foreach($this->bankAccount->get() as $item) {
      $bankAccounts[] = $this->_transforms($item, $this->bankAccount);
    }

    if (sizeof($bankAccounts) > 0)
      return $listener->response(200, array_merge(
        ["bank_accounts" => $this->_extends($bankAccounts, $this->bankAccount)],
        $this->bankAccount->getPagination()
      ));
    return $listener->response(204, ["bank_accounts" => []]);
  }
}
