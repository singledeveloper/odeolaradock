<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 23/11/18
 * Time: 21.33
 */

namespace Odeo\Domains\Account;


use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\Gojek;
use Odeo\Domains\Core\PipelineListener;

class BankAccountTransformer {

  public function intercept(PipelineListener $listener, $data) {
    switch ($data['bank_id']) {
      case Bank::GOPAY:
        return $listener->response(200, [
          'account_number' => Gojek::WITHDRAW_PREFIX . $data['account_number'],
          'account_name' => "GOPAY {$data['account_name']}",
        ]);
    }

    return $listener->response(200);
  }
}
