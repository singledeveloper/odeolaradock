<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Account\Repository\UserBankAccountRepository;
use Odeo\Domains\Core\PipelineListener;

class BankAccountUpdater {

  private $bankAccountRepo, $selector;

  public function __construct() {
    $this->bankAccountRepo = app()->make(UserBankAccountRepository::class);
    $this->selector = app()->make(BankAccountSelector::class);
  }

  public function update(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];

    $account = $this->bankAccountRepo->findById($data['bank_account_id']);

    $account->user_id = $userId;
    $account->bank_id = $data['bank_id'];
    $account->account_name = $data['account_name'];
    $account->account_number = $data['account_number'];

    $this->bankAccountRepo->save($account);

    if ($this->bankAccountRepo->save($account)) {
      $this->bankAccountRepo->normalizeFilters([
        'expand' => 'bank',
        'fields' => 'id,account_name,account_number,bank_id'
      ]);
      return $listener->response(201, $this->selector->_transforms($account, $this->bankAccountRepo));
    }
    return $listener->response(400, trans('errors.database'));
  }
}
