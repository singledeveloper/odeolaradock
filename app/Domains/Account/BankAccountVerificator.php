<?php
/**
 * Created by PhpStorm.
 * User: odeo
 * Date: 12/21/17
 * Time: 10:30 PM
 */

namespace Odeo\Domains\Account;

use Odeo\Domains\Account\Helper\BankAccountInquirer;
use Odeo\Domains\Account\Helper\BankAccountParser;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\PipelineListener;

class BankAccountVerificator {

  private $bankAccountInquirer;

  public function __construct() {
    $this->bankAccountInquirer = app()->make(BankAccountInquirer::class);
    $this->bankAccountParser = app()->make(BankAccountParser::class);
  }

  public function verify(PipelineListener $listener, $data) {
    if (!isProduction()) {
      return $listener->response(200, ['inquiry_account_name' => 'Test Account Name']);
    }

    $accountName = $data['account_name'] ?? 'dummy';
    list($bankInquiry, $status) = $this->bankAccountInquirer->inquire($data['bank_id'], $data['account_number'], $data['auth']['user_id'], [
      'timeout' => 10,
      'customer_name' => $accountName,
      'channel_type' => ArtajasaDisbursement::CHANNEL_MOBILE,
      'reference_id' => $data['auth']['user_id'],
      'reference_type' => ArtajasaDisbursement::INQUIRY_REFERENCE_TYPE_BANK_ACCOUNT,
    ]);

    switch ($status) {
      case BankAccountInquiryStatus::FAILED:
      case BankAccountInquiryStatus::FAILED_VENDOR_DOWN:
        return $listener->response(400, trans('errors.bank_account.failed_to_check_account'));

      case BankAccountInquiryStatus::FAILED_BANK_REJECTION:
      case BankAccountInquiryStatus::FAILED_CLOSED_BANK_ACCOUNT:
      case BankAccountInquiryStatus::FAILED_WRONG_ACCOUNT_NUMBER:
        return $listener->response(400, trans('errors.bank_account.invalid_account_number'));
    }

    /*if (!isVersionSatisfy(Platform::ANDROID, '3.1.1') && !isVersionSatisfy(Platform::IOS, '3.1.1')) {
      if (!$this->bankAccountParser->isNameValid($accountName, $bankInquiry->account_name)) {
        return $listener->response(400, trans('errors.bank_account.account_name_mismatch', [
          'hint' => $this->bankAccountParser->getMaskedName($bankInquiry->account_name),
        ]));
      }
    }*/

    return $listener->response(200, ['inquiry_account_name' => $bankInquiry->account_name]);
  }
}
