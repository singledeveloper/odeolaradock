<?php

namespace Odeo\Domains\Account;

use Lcobucci\JWT\Parser;
use Odeo\Domains\Account\Repository\UserTokenRepository;
use Odeo\Domains\Core\PipelineListener;

class DeviceChecker {

  private $userToken;

  public function __construct() {
    $this->userToken = app()->make(UserTokenRepository::class);
  }

  public function check(PipelineListener $listener, $data) {
    $data = getAuthorizationBearer();
    $token = (new Parser())->parse($data["bearer"]);
    $tokenId = $token->getClaim('dvt');

    if(!$this->userToken->findById($tokenId)) {
      return $listener->response(401, trans('errors.invalid_authorization'));
    }

    return $listener->response(200);
  }

}
