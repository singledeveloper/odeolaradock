<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 23-Jan-18
 * Time: 11:50 AM
 */

namespace Odeo\Domains\Account;

use Odeo\Domains\Constant\FavoriteNumber;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\Transportation;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Account\Repository\FavoriteNumberRepository;

class FavoriteNumberSelector implements SelectorListener {

  private $favoriteNumberRepo;

  public function __construct() {
    $this->favoriteNumberRepo = app()->make(FavoriteNumberRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $favoriteNumber = [];

    $favoriteNumber['id'] = $item->id;
    $favoriteNumber['user_id'] = $item->user_id;
    $favoriteNumber['number'] = $item->number;
    $favoriteNumber['name'] = $item->name;
    $favoriteNumber['service_id'] = $item->service_id;
    $favoriteNumber['category'] = $item->category;
    $favoriteNumber['is_private'] = $item->is_private;
    $favoriteNumber['type'] = $item->type;

    return $favoriteNumber;
  }

  public function getAll(PipelineListener $listener, $data) {
    $this->favoriteNumberRepo->normalizeFilters($data);

    $favoriteNumbers = [];
    foreach ($this->favoriteNumberRepo->gets($data['auth']['user_id']) as $item) {
      $favoriteNumbers[] = $this->_transforms($item, $this->favoriteNumberRepo);
    }

    if (sizeof($favoriteNumbers) > 0) {
      return $listener->response(200,
        ["favorites" => $favoriteNumbers]
      );
    }
    return $listener->response(204, ["favorites" => []]);
  }

  public function getByServiceId(PipelineListener $listener, $data) {
    $this->favoriteNumberRepo->normalizeFilters($data);

    $category = FavoriteNumber::getCategory($data['service_id']);

    $types = $this->parseType($data);

    $favoriteNumbers = [];
    foreach ($this->favoriteNumberRepo->getsByUserIdAndCategoryAndType($data['auth']['user_id'], $category, $types) as $item) {
      $favoriteNumbers[] = $this->_transforms($item, $this->favoriteNumberRepo);
    }

    if (sizeof($favoriteNumbers) > 0) {
      return $listener->response(200,
        ["favorites" => $favoriteNumbers]
      );
    }
    return $listener->response(204, ["favorites" => []]);
  }

  private function parseType($data) {
    $types = $data['type'] ?? null;
    if ($types) {
      switch ($data['service_id']) {
        case Service::TRANSPORTATION:
          $types = Transportation::getCategoriesByKey($data['type']);
          break;
        default:
          if (!is_array($types)) {
            $types = [$types];
          }
      }
    }

    return $types;
  }
}
