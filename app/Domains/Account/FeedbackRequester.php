<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Account\Jobs\SendFeedbackAlert;
use Odeo\Domains\Core\PipelineListener;

class FeedbackRequester {

  public function __construct() {
    $this->feedback = app()->make(\Odeo\Domains\Account\Repository\FeedbackRepository::class);
  }
  
  public function create(PipelineListener $listener, $data) {    
    $feedback = $this->feedback->getNew();
    if (isset($data['auth']) && isset($data['auth']['user_id'])) $feedback->user_id = $data['auth']['user_id'];
    if (isset($data["email"])) $feedback->email = $data["email"];
    if (isset($data["phone_number"])) $feedback->telephone = $data["phone_number"];
    $feedback->message = $data["message"];
    
    if (!$this->feedback->save($feedback)) {
      return $listener->response(400, "Failed to save feedback.");
    }

    $listener->pushQueue(new SendFeedbackAlert($feedback->toArray()));

    return $listener->response(201, ["feedback" => $feedback]);
  }
}
