<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/2/17
 * Time: 12:08 AM
 */

namespace Odeo\Domains\Account\Helper;

use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;

class AdminChecker {

  public function check(PipelineListener $listener, $data) {
    return $listener->response(200);
  }

  public function checkHasPermission(PipelineListener $listener, $data) {
    $userGroupRepo = app()->make(\Odeo\Domains\Account\Repository\UserGroupRepository::class);

    if ($data['task'] == UserType::GROUP_HAS_VERIFY_ORDER_PERMISSION) {
      $orderRepo = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
      $order = $orderRepo->findById($data['order_id']);
      $infoId = $order->payment->info_id;
  
      if (in_array($infoId, Payment::getBankTransferGroup())) {
        return $listener->response(200);
      }
    }

    if (!$userGroupRepo->findUserWithinGroup(getUserId(), $data['task']))
      return $listener->response(400, 'You don\'t have permission');

    return $listener->response(200);
  }

}
