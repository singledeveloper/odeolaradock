<?php

namespace Odeo\Domains\Account\Helper;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Repository\BankAccountInquiryRepository;
use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\BniDisbursement;
use Odeo\Domains\Constant\MandiriDisbursement;
use Odeo\Domains\Constant\PermataDisbursement;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Disbursement\ApiDisbursement\Artajasa\Helper\ArtajasaResponseMapper;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\TransferInquiry;
use Odeo\Domains\Vendor\Bni\Helper\ApiManager as BniApiManager;
use Odeo\Domains\Vendor\Mandiri\Helper\ApiManager as MandiriApiManager;
use Odeo\Domains\Vendor\Permata\Helper\ApiManager as PermataApiManager;

class BankAccountInquirer {

  /**
   * @var MandiriApiManager
   */
  private $mandiriApiManager;
  private $bniApiManager, $permataApiManager, $transferInquiry;
  private $bankAccountInquiryRepo, $bankRepo, $userRepo;
  private $artajasaResponseMapper, $redis;
  private $inquiryVendors;

  // vendors below can inquire to other banks
  const VENDOR_INQUIRE_PRIORITY = [
    1 => ApiDisbursement::DISBURSEMENT_VENDOR_BNI_API_DISBURSEMENT,
    ApiDisbursement::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT,
  ];

  public function __construct() {
    $this->bankAccountInquiryRepo = app()->make(BankAccountInquiryRepository::class);
    $this->bniApiManager = app()->make(BniApiManager::class);
    $this->permataApiManager = app()->make(PermataApiManager::class);
    $this->mandiriApiManager = app()->make(MandiriApiManager::class);
    $this->bankRepo = app()->make(BankRepository::class);
    $this->artajasaResponseMapper = app()->make(ArtajasaResponseMapper::class);
    $this->transferInquiry = app()->make(TransferInquiry::class);
    $this->userRepo = app()->make(UserRepository::class);
    $this->redis = Redis::connection();

    $this->inquiryVendors = [
      ApiDisbursement::DISBURSEMENT_VENDOR_BNI_API_DISBURSEMENT => [
        'maintenance' => false,
        // 'maintenance_from' => Carbon::parse('2018-09-21 22:00:00')->getTimestamp(),
        // 'maintenance_to' => Carbon::parse('2018-09-22 02:00:00')->getTimestamp(),
      ],
      ApiDisbursement::DISBURSEMENT_VENDOR_PERMATA_API_DISBURSEMENT => [
        'maintenance' => false,
      ],
      ApiDisbursement::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT => [
        'maintenance' => false,
      ],
      ApiDisbursement::DISBURSEMENT_VENDOR_MANDIRI_API_DISBURSEMENT => [
        'maintenance' => false,
      ]
    ];
  }

  public function inquire($bankId, $accountNumber, $userId, $opts) {
    if ($bankId == Bank::ODEO) {
      return $this->inquireOdeo($accountNumber);
    }

    $inquiry = $this->bankAccountInquiryRepo->findByBankIdAndAccountNumber($bankId, $accountNumber);
    if ($this->canUseCachedResult($inquiry)) {
      return [$inquiry, $inquiry->status, true];
    }

    $bank = $this->bankRepo->findById($bankId);

    if (!$bank || !$bank->aj_bank_code) {
      return [null, BankAccountInquiryStatus::FAILED, false];
    }

    $inquiry = $this->bankAccountInquiryRepo->getNew();
    $inquiry->bank_id = $bankId;
    $inquiry->user_id = $userId;
    $inquiry->account_number = $accountNumber;

    $currPriority = 0;
    $usedInquiryVendor = [];

    do {
      $inquiry->disbursement_vendor = $this->getVendor($accountNumber, $bankId, $currPriority);
      $currPriority++;

      if ($currPriority > count(self::VENDOR_INQUIRE_PRIORITY)) {
        return [null, BankAccountInquiryStatus::FAILED, false];
      }

      if (in_array($inquiry->disbursement_vendor, $usedInquiryVendor)) {
        continue;
      }

      array_push($usedInquiryVendor, $inquiry->disbursement_vendor);

      if ($this->isMaintenance($inquiry->disbursement_vendor)) {
        continue;
      }

      switch ($inquiry->disbursement_vendor) {
        case ApiDisbursement::DISBURSEMENT_VENDOR_BNI_API_DISBURSEMENT:
          list($accountName, $referenceId, $status) = $this->inquireBni($inquiry, $bank, $opts);
          break;
        case ApiDisbursement::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT:
          list($accountName, $referenceId, $status) = $this->inquireArtajasa($inquiry, $bank, $opts);
          break;
        case ApiDisbursement::DISBURSEMENT_VENDOR_PERMATA_API_DISBURSEMENT:
          list($accountName, $referenceId, $status) = $this->inquirePermata($inquiry);
          break;
        case ApiDisbursement::DISBURSEMENT_VENDOR_MANDIRI_API_DISBURSEMENT:
          list($accountName, $referenceId, $status) = $this->inquireMandiri($inquiry);
          break;
        default:
          return [null, BankAccountInquiryStatus::FAILED, false];
      }
    } while ($status == BankAccountInquiryStatus::FAILED);

    $inquiry->status = $status;
    $inquiry->account_name = $accountName;
    $inquiry->vendor_disbursement_reference_id = $referenceId;
    $this->bankAccountInquiryRepo->save($inquiry);

    return [$inquiry, $status, false];
  }

  private function canUseCachedResult($inquiry) {
    if (!$inquiry) {
      return false;
    }

    if ($inquiry->status == BankAccountInquiryStatus::COMPLETED) {
      return true;
    }

    return in_array($inquiry->status, [BankAccountInquiryStatus::FAILED_WRONG_ACCOUNT_NUMBER,
        BankAccountInquiryStatus::FAILED_CLOSED_BANK_ACCOUNT]) &&
      time() - strtotime($inquiry->created_at) < BankAccountInquiryStatus::INQUIRY_RESULT_EXPIRE_SEC;
  }

  private function getVendor($accountNumber, $bankId, $currPriority) {
    if (!isProduction()) {
      return ApiDisbursement::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT;
    }

    if (isset(self::VENDOR_INQUIRE_PRIORITY[$currPriority])) {
      return self::VENDOR_INQUIRE_PRIORITY[$currPriority];
    }

    switch ($bankId) {
      case Bank::BNI:
        return ApiDisbursement::DISBURSEMENT_VENDOR_BNI_API_DISBURSEMENT;
      case BANK::MANDIRI:
        return ApiDisbursement::DISBURSEMENT_VENDOR_MANDIRI_API_DISBURSEMENT;
      case Bank::PERMATA:
        return strlen($accountNumber) > 12
          ? ApiDisbursement::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT
          : ApiDisbursement::DISBURSEMENT_VENDOR_PERMATA_API_DISBURSEMENT;
    }

    return ApiDisbursement::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT;
  }

  private function isMaintenance($currVendor) {
    $now = time();
    $vendor = $this->inquiryVendors[$currVendor];
    if ($vendor['maintenance'] && !isset($vendor['maintenance_from'])) {
      return true;
    }

    return $vendor['maintenance'] && $now >= $vendor['maintenance_from'] && $now <= $vendor['maintenance_to'];
  }

  private function inquirePermata($bankInquiry) {
    try {
      $bankInquiry->cost = PermataDisbursement::INQUIRY_COST;
      $result = $this->permataApiManager->overbookingInquiry($bankInquiry->account_number);
      if (!$result) {
        return ['', null, BankAccountInquiryStatus::FAILED];
      }

      $header = $result['AcctInqRs']['MsgRsHdr'];
      if (in_array($header['StatusCode'], ['14', '90'])) {
        return ['', null, BankAccountInquiryStatus::FAILED_WRONG_ACCOUNT_NUMBER];
      }

      if ($header['StatusCode'] != '00') {
        return ['', null, BankAccountInquiryStatus::FAILED];
      }

      $accountName = $result['AcctInqRs']['InqInfo']['AccountName'];
      return [trim($accountName), null, BankAccountInquiryStatus::COMPLETED];

    } catch (\Exception $e) {
      return ['', null, BankAccountInquiryStatus::FAILED];
    }
  }

  private function inquireBni($bankInquiry, $bank, $opts) {
    try {
      $bankInquiry->cost = BniDisbursement::COST;
      // $this->redis->setex('odeo_core:disable_bni_inquiry', 300, 1);
      if ($bank->id == Bank::BNI) {
        $result = $this->bniApiManager->getInHouseInquiry($bankInquiry->account_number, $opts['timeout'] ?? 10);
      } else {
        list($refNumber) = $this->bniApiManager->generateCustomerRefAndValueDate();
        $result = $this->bniApiManager
          ->getInterBankInquiry($refNumber, explode('-', $bank->aj_bank_code)[0], $bankInquiry->account_number, $opts['timeout'] ?? 10);
      }

      if (!$result) {
        return ['', null, BankAccountInquiryStatus::FAILED];
      }

      if (in_array($result->responseCode, ['0110', '0169'])) {
        return ['', null, BankAccountInquiryStatus::FAILED_WRONG_ACCOUNT_NUMBER];
      }

      if ($result->responseCode != '0001') {
        return ['', null, BankAccountInquiryStatus::FAILED];
      }

      if ($bank->id == Bank::BNI && $result->accountStatus != 'BUKA') {
        return ['', null, BankAccountInquiryStatus::FAILED_CLOSED_BANK_ACCOUNT];
      }

      $accountName = $bank->id == Bank::BNI ?
        $result->customerName :
        $result->destinationAccountName;

      return [trim($accountName), null, BankAccountInquiryStatus::COMPLETED];
    } catch (\Exception $e) {
      \Log::error($e);
      return ['', null, BankAccountInquiryStatus::FAILED];
    }
  }

  private function inquireMandiri($bankInquiry) {

    try {
      $bankInquiry->cost = MandiriDisbursement::INQUIRY_COST;
      $result = $this->mandiriApiManager->getAccountInquiry($bankInquiry->account_number);

      if (!$result || !isset($result['responseHeader']) || !isset($result['body'])) {
        return ['', null, BankAccountInquiryStatus::FAILED];
      }

      if ($result['responseHeader']['responseCode'] != '1') {
        return ['', null, BankAccountInquiryStatus::FAILED_WRONG_ACCOUNT_NUMBER];
      }

      if ($result['responseHeader']['responseMessage'] != 'OK') {
        return ['', null, BankAccountInquiryStatus::FAILED];
      }

      $accountName = $result['body']['shortName'];
      return [trim($accountName), null, BankAccountInquiryStatus::COMPLETED];
    } catch (\Exception $e) {
      \Log::info($e);
      return ['', null, BankAccountInquiryStatus::FAILED];
    }
  }

  private function inquireArtajasa($bankInquiry, $bank, $opts) {
    $bankInquiry->cost = ArtajasaDisbursement::INQUIRY_COST;
    $customerName = $opts['customer_name'] ?? '';
    $userTelephone = $opts['user_telephone'] ?? '';

    if (!$customerName) {
      return ['', null, BankAccountInquiryStatus::FAILED];
    }

    if (!$userTelephone) {
      $user = $this->userRepo->findById($bankInquiry->user_id);
      $userTelephone = $user->telephone;
    }

    $amountList = [5, 8, 10, 12, 15];
    $amount = $amountList[random_int(0, count($amountList) - 1)] * 10000;

    $terminalId = preg_replace('/\\D/', '', $userTelephone);
    $terminalId = substr($terminalId, strlen($terminalId) - 8, 8);
    $terminalId = empty($terminalId) ? '1' : $terminalId;

    $transferInquiry = $this->transferInquiry->exec([
      'terminal_id' => $terminalId,
      'user_id' => $bankInquiry->user_id,
      'user_name' => $customerName,
      'sender_prefix' => $opts['sender_prefix'] ?? ArtajasaDisbursement::ODEO_SENDER_PREFIX,
      'bank_code' => explode('-', $bank->aj_bank_code)[0],
      'regency_code' => $bank->aj_regency_code,
      'transfer_to' => $bankInquiry->account_number,
      'amount' => $amount,
      'channel_type' => $opts['channel_type'] ?? ArtajasaDisbursement::CHANNEL_INTERNET,
      'timeout' => $opts['timeout'] ?? 10,
      'reference_id' => $opts['reference_id'] ?? null,
      'reference_type' => $opts['reference_type'] ?? null,
    ]);

    if (!$transferInquiry) {
      return ['', null, BankAccountInquiryStatus::FAILED];
    }

    $status = $this->artajasaResponseMapper->map($transferInquiry->response_code);

    return [
      trim($transferInquiry->transfer_to_name),
      $transferInquiry->id,
      BankAccountInquiryStatus::fromApiDisbursementStatus($status),
    ];
  }

  private function inquireOdeo($accountNumber) {
    $user = $this->userRepo->findByTelephone(purifyTelephone($accountNumber));
    $inquiry = $this->bankAccountInquiryRepo->getNew();
    $inquiry->cost = 0;

    if ($user && $user->type == 'seller' && in_array($user->status, [UserStatus::OK, UserStatus::RESET_PIN])) {
      $inquiry->status = BankAccountInquiryStatus::COMPLETED;
      $inquiry->account_name = $user->name;
    } else {
      $inquiry->status = BankAccountInquiryStatus::FAILED_WRONG_ACCOUNT_NUMBER;
    }

    return [$inquiry, $inquiry->status, false];
  }

}
