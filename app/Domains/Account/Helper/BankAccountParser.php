<?php
/**
 * Created by PhpStorm.
 * User: odeo
 * Date: 12/21/17
 * Time: 10:30 PM
 */

namespace Odeo\Domains\Account\Helper;

use Odeo\Domains\Account\Helper\BankAccountInquirer;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\PipelineListener;

class BankAccountParser {
 
  const SIMILARITY_THRESHOLD = 85;

  public function getValidity($nameFromInput, $nameFromBank) {
    $nameFromBank = $this->sanitizeName($nameFromBank);
    $nameFromInput = $this->sanitizeName($nameFromInput);
    $truncInput = substr($nameFromInput, 0, strlen($nameFromBank));

    if ($truncInput == $nameFromBank) {
      return 100;
    }

    similar_text($nameFromBank, $truncInput, $pct1);
    similar_text($truncInput, $nameFromBank, $pct2);

    return ($pct1 + $pct2) / 2;
  }

  public function isNameValid($nameFromInput, $nameFromBank, $threshold = self::SIMILARITY_THRESHOLD) {
    return $this->getValidity($nameFromInput, $nameFromBank) >= $threshold;
  }

  public function getMaskedName($name) {
    $name = $this->sanitizeName($name);

    $parts = explode(' ', $name);
    $parts = array_map(function ($str) {
      $maskLen = strlen($str) - 2;
      if ($maskLen < 0) {
        return $str;
      }

      return substr($str, 0, 2) . str_repeat("*", $maskLen);
    }, $parts);

    return implode(' ', $parts);
  }

  private function sanitizeName($name) {
    $name = strtoupper($name);

    // remove title
    $name = preg_replace("/^(SDRI?|BPK|IBU|PT)\.?\s/", "", $name);

    // remove non character
    $name = preg_replace("/[^A-Za-z ]/", "", $name);

    // remove adjacent whitespaces
    $parts = explode(' ', $name);
    $parts = array_filter($parts);
    $name = implode(' ', $parts);

    return strtoupper($name);
  }
}
