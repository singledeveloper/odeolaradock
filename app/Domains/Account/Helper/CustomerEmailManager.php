<?php

namespace Odeo\Domains\Account\Helper;

use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Core\PipelineListener;

class CustomerEmailManager {

  private $userRepo;

  public function __construct() {
    $this->userRepo = app()->make(UserRepository::class);
  }

  public function checkIsEmailVerified(PipelineListener $listener, $data) {
    $email = $data['email'] ?? null;

    $user = $email
      ? $this->userRepo->findByEmail($email)
      : $this->userRepo->findById(getUserId());

    if (!$user) {
      $listener->response(400, trans('user.email_not_set'));
    }

    if (!$user->is_email_verified) {
      return $listener->response(400, trans('user.email_not_verified'));
    }

    return $listener->response(200, ['email' => $user->email]);
  }

  public function pushJobIfEmailVerified(PipelineListener $listener, $data) {
    if (isset($data['email'])) {
      if ($user = $this->userRepo->findByEmail($data['email'])) {
        if ($user->is_email_verified) {
          $listener->pushQueue($data['job']);
        }
      }
    }
    return $listener->response(200);
  }

}
