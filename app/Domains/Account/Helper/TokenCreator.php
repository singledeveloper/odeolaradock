<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 10/13/16
 * Time: 12:46 PM
 */

namespace Odeo\Domains\Account\Helper;


use Lcobucci\JWT\Signer\Hmac\Sha256;

class TokenCreator {

  private $builder;

  public function __construct() {
    $this->builder = app()->make(\Lcobucci\JWT\Builder::class);
  }

  public function createToken(
    $userId, 
    $userType, 
    $platformId, 
    $version = null, 
    $deviceToken = null
  ) {
    $builder = $this->builder->unsign()
      ->setNotBefore(time())
      ->setExpiration(time() + 3600)
      ->set('uid', $userId)
      ->set('uty', $userType)
      ->set('upf', $platformId);

    if ($deviceToken) {
      $builder = $builder->set('dvt', $deviceToken);
    }

    return $builder->sign(new Sha256(), env('APP_KEY'))->getToken();
  }

  public function createTokenRefresh(
    $userId, 
    $userType, 
    $platformId, 
    $secret,
    $deviceToken = null
  ) {
    $builder = $this->builder->unsign()
      ->setNotBefore(time() + 60)
      ->setExpiration(time() + 3600 * 24 * 30)
      ->set('uid', $userId)
      ->set('uty', $userType)
      ->set('upf', $platformId)
      ->set('ref', $secret);

    if ($deviceToken) {
      $builder = $builder->set('dvt', $deviceToken);
    }

    return $builder->sign(new Sha256(), env('APP_KEY'))->getToken();
  }

}
