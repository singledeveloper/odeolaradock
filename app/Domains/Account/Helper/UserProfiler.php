<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/2/17
 * Time: 12:08 AM
 */

namespace Odeo\Domains\Account\Helper;

use Odeo\Domains\Account\Appidentifier\Repository\UserAndroidIdentifierRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\Platform;

class UserProfiler {

  private $userRepo;
  private $userAndroidIdentifiers;

  public function __construct() {
    $this->userRepo = app()->make(UserRepository::class);
    $this->userAndroidIdentifiers = app()->make(UserAndroidIdentifierRepository::class);
  }

  public function check($data) {

    $user = $this->userRepo->findById($data['auth']['user_id']);

    yield $this->checkHasRecordedAppIdentifier($data);
    yield $this->checkIsProfileHasCompletelyFilled($user, $data);
    yield $this->checkIsPasswordHasBeenSet($user);
    //yield another check function
  }


  public function checkIsPasswordHasBeenSet($user) {
    if ($user->password === $user->transaction_pin) {
      return 'require_password_change';
    }
    return null;
  }

  public function checkHasRecordedAppIdentifier($data) {

    if ($data['auth']['platform_id'] == Platform::ANDROID) {
      if (!$this->userAndroidIdentifiers->findByUserId($data['auth']['user_id'])) {
        return 'require_app_unique_identifier';
      }
    }
    return null;
  }


  public function checkIsProfileHasCompletelyFilled($user, $data) {

    $shouldCheckPlatform = [Platform::ANDROID, Platform::IOS, Platform::WEB_APP];

    if (in_array($data['auth']['platform_id'], $shouldCheckPlatform)) {
      if ($user && (!strlen($user->name) || !strlen($user->email))) {
        return 'require_profile_fill';
      } else if ($user && isset($user->email) && !$user->is_email_verified) {
        return 'require_email_verification';
      }
    }
    return null;
  }

}
