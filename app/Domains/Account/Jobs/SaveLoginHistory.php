<?php

namespace Odeo\Domains\Account\Jobs;

use Odeo\Jobs\Job;

class SaveLoginHistory extends Job {

  protected $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    $userLoginHistoryRepo = app()->make(\Odeo\Domains\Account\Repository\UserLoginHistoryRepository::class);
    $userRepo = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);

    $userId = $this->data['auth']['user_id'] ?? $this->data['user_id'];

    if ($userRepo->findById($userId)) {
      $platformId = $this->data['auth']['platform_id'] ?? $this->data['platform_id'];

      $userLoginHistory = $userLoginHistoryRepo->getNew();
      $userLoginHistory->user_id = $userId;
      $userLoginHistory->platform_id = $platformId;

      if (isset($this->data['version']))
        $userLoginHistory->client_version = $this->data['version'];

      $userLoginHistory->route = $this->data['route'];

      if (isset($this->data['ip']) && isProduction())
        $userLoginHistory->ip_address = $this->data['ip'];

      if (isset($this->data['user_agent']))
        $userLoginHistory->user_agent = $this->data['user_agent'];
      if (isset($this->data['lat_lang']))
        $userLoginHistory->lat_lang = $this->data['lat_lang'];

      $userLoginHistoryRepo->save($userLoginHistory);
    }
  }
}
