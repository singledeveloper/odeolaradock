<?php

namespace Odeo\Domains\Account\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendFeedbackAlert extends Job {

  protected $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }


  public function handle() {

    if (app()->environment() != 'production') {
      return;
    }

    Mail::send('emails.new_feedback_alert', [
      'data' => $this->data
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id');
      $m->to('cs@odeo.co.id', 'odeo')->subject('New Feedback Alert');
    });
  }
}
