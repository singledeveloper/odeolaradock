<?php

namespace Odeo\Domains\Account\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Constant\CashConfig;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Jobs\Job;

class SendOCashWarning extends Job {

  protected $userId, $currentOCash;

  public function __construct($userId, $currentOCash) {
    parent::__construct();
    $this->userId = $userId;
    $this->currentOCash = $currentOCash;
  }

  public function handle() {
    $notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $notificationBalanceRepo = app()->make(\Odeo\Domains\Notification\Repository\NotificationBalanceRepository::class);

    $formattedAmount = $currencyHelper->formatPrice($this->currentOCash)['formatted_amount'];
    $notification->setup($this->userId, NotificationType::WARNING, NotificationGroup::ACCOUNT);
    $notification->oCashAlmostSpentOutWarning($formattedAmount);
    dispatch($notification->queue());

    if ($setting = $notificationBalanceRepo->findByUserId($this->userId)) {
      Mail::send('emails.ocash_warning', ['amount' => $formattedAmount], function ($m) use ($setting) {
        $m->from('noreply@odeo.co.id', 'odeo.co.id');
        $m->to(explode(';', $setting->email))
          ->subject('[ODEO] Remaining Deposit Warning');
      });
    }
  }
}
