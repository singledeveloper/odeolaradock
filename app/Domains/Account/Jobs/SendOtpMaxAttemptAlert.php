<?php

namespace Odeo\Domains\Account\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendOtpMaxAttemptAlert extends Job {

  protected $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    if (app()->environment() != 'production') {
      return;
    }

    Mail::send('emails.otp_max_attempt_alert', [
      'data' => $this->data
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('security@odeo.co.id', 'odeo')->subject('Somebody has attempted OTP many times - ' . time());
    });
  }
}
