<?php

namespace Odeo\Domains\Account\Jobs;

use Illuminate\Contracts\Mail\Mailer;
use Odeo\Jobs\Job;

class SendPasswordResetEmail extends Job  {

  protected $url;
  protected $user;

  public function __construct($url, $user) {
    parent::__construct();
    $this->url = $url;
    $this->user = $user;
  }

  public function handle(Mailer $mailer) {

    $mailer->send('emails.password_reset', ['url' => $this->url], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id');

      $m->to($this->user->email, $this->user->name)->subject('Password Reset');
    });
  }
}
