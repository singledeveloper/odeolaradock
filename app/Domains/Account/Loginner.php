<?php

namespace Odeo\Domains\Account;

use Illuminate\Http\Request;
use Odeo\Domains\Account\Helper\UserChecker;
use Odeo\Domains\Account\Helper\UserProfiler;
use Odeo\Domains\Account\Helper\UserKtpReasonGenerator;
use Odeo\Domains\Account\Jobs\SaveLoginHistory;
use Odeo\Domains\Account\Repository\UserLoginHistoryRepository;
use Odeo\Domains\Account\Repository\UserPasswordHistoryRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Account\Repository\UserKtpRepository;
use Odeo\Domains\Constant\Header;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Constant\ErrorStatus;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Constant\UserType;

class Loginner {

  public function __construct() {
    $this->user = app()->make(UserRepository::class);
    $this->userLoginHistory = app()->make(UserLoginHistoryRepository::class);
    $this->userChecker = app()->make(UserChecker::class);
    $this->userPasswordHistories = app()->make(UserPasswordHistoryRepository::class);
    $this->userKtpRepo = app()->make(UserKtpRepository::class);
    $this->reasonGenerator = app()->make(UserKtpReasonGenerator::class);
    $this->userKtpValidator = app()->make(UserKtpValidator::class);
  }

  public function checkPin(PipelineListener $listener, $data) {
    if (!isset($data['pin'])) {
      return $listener->response(400, "Pin required");
    }

    $user = $this->user->findById($data["auth"]["user_id"]);

    list($isValid, $message, $errorStatus) = $this->userChecker->checkPin($user, $data['pin']);
    if (!$isValid) {
      if ($errorStatus) {
        return $listener->responseWithErrorStatus($errorStatus, $message);
      } else {
        return $listener->response(400, $message);
      }
    }

    return $listener->response(200);
  }

  public function checkPassword(PipelineListener $listener, $data) {
    if (!isset($data['pin'])) {
      return $listener->response(400, "Password required");
    }

    $user = $this->user->findById($data["auth"]["user_id"]);

    list($isValid, $user, $errorStatus) = $this->userChecker->checkPassword($user, $data['pin']);
    if (!$isValid) {
      $this->createLoginHistory($listener, array_merge($data, ['route' => 'check_password_fail']));
      if ($errorStatus) {
        return $listener->responseWithErrorStatus($errorStatus, $user);
      } else {
        return $listener->response(400, $user);
      }
    }

    $response = [
      "type" => $user->type,
      "user_id" => $user->id
    ];

    if (isset($data['profile_check']) && $data['profile_check']) {
      foreach (app()->make(UserProfiler::class)->check($data) as $requirement) {
        if ($requirement) $response['requirement'][] = $requirement;
      }
    }

    $this->createLoginHistory($listener, array_merge($data, ['route' => 'check_password']));

    return $listener->response(200, $response);
  }

  public function login(PipelineListener $listener, $data) {
    if (isset($data['email'])) {
      $user = $this->user->findByEmail($data["email"]);
    } else {
      $user = $this->user->findByTelephone($data["phone_number"]);
    }

    if (!$user) {
      return $listener->response(400, (isset($data['email']) ? trans('errors.invalid_email_or_password') : trans('errors.invalid_telephone_or_password')));
    } else {
      $userId = $user->id;
      if (in_array($data['platform_id'], [Platform::ADMIN, Platform::SUPPORT]) && $user->type != 'admin') {
        return $listener->response(400, trans('errors.invalid_email'));
      }

      list($isValid, $user, $errorStatus) = $this->userChecker->checkPassword($user, $data['password']);
      if (!$isValid) {
        $this->createLoginHistory($listener, array_merge($data, ['route' => 'login_fail', 'user_id' => $userId]));

        if ($errorStatus) {
          if ($errorStatus === ErrorStatus::ACCOUNT_BANNED){
            $userKtp = $this->userKtpRepo->findLastByUserId($userId);
            if ($this->userNeedVerification($userKtp)) {
              if ($userKtp->rejected_at === null) {
                $errorStatus = ErrorStatus::ACCOUNT_BANNED_UPLOADED;
              } else if ($userKtp->rejected_at !== null) {
                $errorStatus = ErrorStatus::ACCOUNT_BANNED_REJECTED;
                $user = $this->reasonGenerator->getReasonsArray(
                  $userKtp->nik_status,
                  $userKtp->name_status,
                  $userKtp->ktp_status,
                  $userKtp->ktp_selfie_status,
                  $userKtp->last_ocash_balance_status,
                  $userKtp->last_transaction_description_status,
                  true
                );
              }
            }
          }

          return $listener->responseWithErrorStatus($errorStatus, $user);
        } else {
          return $listener->response(400, $user);
        }
      }

      $clientIP = getClientIP();
      if ((in_array($data['platform_id'], [Platform::WEB_APP, Platform::WEB_STORE]) && $user->email != null)
        && ($clientIP && $this->userLoginHistory->getUserLoginCount($user->id))
      ) {
        if (app()->environment() === 'production' &&
          !$this->userLoginHistory->checkIp($user->id, $clientIP)) {
          $listener->addNext(new Task(LoginWarner::class, 'warn', [
            'email' => $user->email,
            'phone_number' => $user->telephone,
            'ip_address' => $clientIP,
            'platform_id' => $data['platform_id'],
          ]));
        }
      }

      $response = [
        'user_id' => $user->id,
        'name' => $user->name,
        'user_type' => $user->type,
      ];

      if ($user->status == UserStatus::RESET_PIN) $response['reset_pin'] = true;
      $response['email'] = $user->email;
      $response['phone_number'] = $user->telephone;
      $response['platform_id'] = $data['platform_id'];
      $response['has_disbursement'] = $user->disbursementApiUser ? true : false;
      $response['has_payment_gateway'] = $user->paymentGatewayUser ? true : false;
      $response['has_invoice'] = $user->businessInvoiceUser ? true : false;
      $response['is_email_verified'] = $user->is_email_verified;
      $response['is_ktp_verified'] = $this->userKtpValidator->isVerified($user->id);
      $response['is_forced_change_password'] = $user->status == UserStatus::FORCE_CHANGE_PASSWORD;
      $response['is_forced_change_pin'] = $user->status == UserStatus::FORCE_CHANGE_PIN;

      $userStores = app()->make(\Odeo\Domains\Subscription\Repository\UserStoreRepository::class);
      $response['has_supply_store'] = $userStores->findSupplyStore($user->id) ? true : false;

      $businessResellerRepo = app()->make(\Odeo\Domains\Marketing\Repository\BusinessResellerRepository::class);
      $response['is_reseller'] = $businessResellerRepo->checkExistingReseller($user->id) ? true : false;

      $approvalUserRepo = app()->make(\Odeo\Domains\Approval\Repository\ApprovalUserRepository::class);
      $response['is_approver'] = $approvalUserRepo->findApprover($user->id) ? true : false;

      $data = array_merge($data, [
        'auth' => [
          'user_id' => $user->id,
          'platform_id' => $data['platform_id']
        ]
      ]);
      if (isset($data['profile_check']) && $data['profile_check']) {
        foreach (app()->make(UserProfiler::class)->check($data) as $requirement) {
          if ($requirement) $response['requirement'][] = $requirement;
        }
      }

      $this->createLoginHistory($listener, array_merge($data, ['route' => 'login']));

      return $listener->response(201, $response);
    }

  }

  public function createLoginHistory(PipelineListener $listener, $data) {
    $request = app()->make(Request::class);

    $dispatchData = $data;

    $platformId = $data['auth']['platform_id'] ?? $data['platform_id'];
    $userIp = getClientIP();
    if ($userIp && isProduction()) $dispatchData['ip'] = $userIp;

    $version = $request->header(Header::getHeaderByPlatform($platformId));
    if ($version) $dispatchData['version'] = $version;

    if ($request->header('user-agent')) $dispatchData['user_agent'] = $request->header('user-agent');
    if ($request->header('lat-lang')) $dispatchData['lat_lang'] = $version = $request->header('lat-lang');

    dispatch(new SaveLoginHistory($dispatchData));

    return $listener->response(200);
  }

  public function guest(PipelineListener $listener, $data) {
    if (isset($data['user_id'])) {
      $user = $this->user->findById($data['user_id']);
    } else {
      $user = $this->user->getNew();
      $user->name = "";
      $user->type = UserType::GUEST;
      $user->status = UserStatus::OK;
      $this->user->save($user);
    }

    if (!$user || $user->type != UserType::GUEST) {
      return $listener->response(400, trans("errors.invalid_user_id"));
    }

    return $listener->response(201, ['user_id' => $user->id, 'user_type' => UserType::GUEST]);
  }

  private function userNeedVerification($userKtp) {
    return
      $userKtp &&
      $userKtp->last_ocash_balance !== null &&
      $userKtp->last_transaction_description !== null &&
      $userKtp->verified_at === null;
  }

}
