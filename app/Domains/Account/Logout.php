<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 02/08/18
 * Time: 15.53
 */

namespace Odeo\Domains\Account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Lcobucci\JWT\Parser;
use Odeo\Domains\Account\Repository\UserTokenRepository;
use Odeo\Domains\Constant\Header;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\PipelineListener;

class Logout {

  private $redis;
  private $userTokenRepo;

  public function __construct() {
    $this->redis = Redis::connection();
    $this->userTokenRepo = app()->make(UserTokenRepository::class);
  }

  private function getRedisKey($userId, $secret) {
    return "odeo_core:logout_{$userId}_{$secret}";
  }

  private function purifyToken($token) {
    $token = (new Parser())->parse($token);
    return $token;
  }

  public function logout(PipelineListener $listener, $data) {
    $token = $this->purifyToken($data['bearer']);

    if ($this->checkIfCanSkip($token)) {
      return $listener->response(200);
    }

    $deviceToken = $token->getClaim('dvt');

    $userToken = $this->userTokenRepo->findById($deviceToken);

    if (!$userToken) {
      return $listener->response(400, 'errors.invalid_authorization');
    }

    if ($userToken->user_id !== getUserId()) {
      return $listener->response(400, 'errors.invalid_authorization');
    }

    if (!$userToken->delete()) {
      return $listener->response(400, 'errors.invalid_authorization');
    }

    $this->redis->setex($this->getRedisKey(getUserId(), $deviceToken), 60 * 60, 1);

    return $listener->response(200);
  }

  public function validateHasLogout(PipelineListener $listener, $data) {
    $token = $this->purifyToken($data['bearer']);

    if (!$this->checkIfCanSkip($token)) {
      $deviceToken = $token->getClaim('dvt');

      if ($this->redis->get($this->getRedisKey(getUserId(), $deviceToken))) {
        return $listener->response(400, 'User has logout');
      }
    }

    return $listener->response(200);
  }

  private function checkIfCanSkip($token) {
    return !$token->hasClaim('dvt');
//    $headerPlatform = Header::getHeaderByPlatform(getPlatformId());
//    $version = app()->make(Request::class)->header($headerPlatform);
//    return !(in_array(getPlatformId(), [Platform::ANDROID, Platform::ADMIN, Platform::WEB_APP]) && !$version);
  }
}
