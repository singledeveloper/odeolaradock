<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 23-Jan-18
 * Time: 11:34 AM
 */

namespace Odeo\Domains\Account\Model;


use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;

class FavoriteNumber extends Entity {

  use SoftDeletes;

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
