<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/28/17
 * Time: 5:35 PM
 */

namespace Odeo\Domains\Account\Model;


use Odeo\Domains\Core\Entity;

class Province extends Entity {
  public $timestamps = false;
}