<?php

namespace Odeo\Domains\Account\Model;

use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\Entity;
use Odeo\Domains\Disbursement\Model\DisbursementApiUser;
use Odeo\Domains\Invoice\Model\BusinessInvoiceUser;
use Odeo\Domains\Order\Receipt\Model\UserReceiptConfig;
use Odeo\Domains\PaymentGateway\Model\PaymentGatewayUser;
use Odeo\Domains\Subscription\Model\UserStore;

class User extends Entity {

  protected $dates = ['deleted_at'];

  protected $fillable = ['name', 'email', 'telephone', 'type'];

  protected $hidden = ['password', 'deleted_at'];

  public function store(array $data) {
    $this->fill($data);
    if (!$this->type) {
      $this->type = UserType::USER;
    }
    if (isset($data["phone_number"])) {
      $this->telephone = $data["phone_number"];
    }
    if (isset($data["name"])) {
      $this->name = trim($data["name"]);
    }
    $this->status = UserStatus::PENDING;
  }

  public function forgot() {
    return $this->hasOne(UserForgot::class);
  }

  public function userStores() {
    return $this->hasMany(UserStore::class);
  }

  public function province() {
    return $this->belongsTo(Province::class);
  }

  public function city() {
    return $this->belongsTo(City::class);
  }

  public function token() {
    return $this->hasOne(UserToken::class);
  }

  public function verification() {
    return $this->hasOne(UserVerification::class);
  }

  public function fcmTokens() {
    return $this->hasMany(UserFcmToken::class);
  }

  public function receiptConfig() {
    return $this->hasOne(UserReceiptConfig::class);
  }

  public function disbursementApiUser() {
    return $this->hasOne(DisbursementApiUser::class);
  }

  public function paymentGatewayUser() {
    return $this->hasOne(PaymentGatewayUser::class);
  }

  public function businessInvoiceUser() {
    return $this->hasOne(BusinessInvoiceUser::class);
  }

}
