<?php

namespace Odeo\Domains\Account\Model;

use Odeo\Domains\Core\Entity;

class UserBusinessKyc extends Entity {

  public function user() {
    return $this->belongsTo(User::class);
  }

}
