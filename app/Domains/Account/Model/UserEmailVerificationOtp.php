<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 23-Jul-18
 * Time: 5:29 PM
 */

namespace Odeo\Domains\Account\Model;


use Odeo\Domains\Core\Entity;

class UserEmailVerificationOtp extends Entity {

  protected $hidden = ['created_at'];

  public $timestamps = false;

}