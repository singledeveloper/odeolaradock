<?php

namespace Odeo\Domains\Account\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;

class UserFcmToken extends Entity {

  protected $fillable = ['user_id', 'device_token'];

  public function user() {
    return $this->belongsTo(User::class);
  }

}
