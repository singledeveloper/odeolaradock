<?php

namespace Odeo\Domains\Account\Model;

use Odeo\Domains\Core\Entity;

class UserKtp extends Entity {

  public $timestamps = false;

  public static function boot() {
    parent::boot();

    static::creating(function ($model) {
      $timestamp = $model->freshTimestamp();
      $model->setCreatedAt($timestamp);
    });
  }
}
