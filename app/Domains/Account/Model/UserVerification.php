<?php

namespace Odeo\Domains\Account\Model;

use Odeo\Domains\Core\Entity;

class UserVerification extends Entity {

  protected $hidden = ['created_at'];

  public $timestamps = false;

  public function store($userId, $key_pair) {
    $this->user_id = $userId;
    $this->key_pair = $key_pair;
    $this->created_at = date('Y-m-d H:i:s');
  }

  public function user() {
    return $this->belongsTo(User::class);
  }
}
