<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 15/01/18
 * Time: 13.56
 */

namespace Odeo\Domains\Account;

use Carbon\Carbon;
use Odeo\Domains\Account\Helper\OtpGenerator;
use Odeo\Domains\Account\Helper\UserKtpReasonGenerator;
use Odeo\Domains\Account\Jobs\SendOtpEmail;
use Odeo\Domains\Account\Jobs\SendOtpMaxAttemptAlert;
use Odeo\Domains\Account\Repository\UserForgotRepository;
use Odeo\Domains\Account\Repository\UserOtpRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Account\Repository\UserKtpRepository;
use Odeo\Domains\Constant\ErrorStatus;
use Odeo\Domains\Constant\SMS;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\SMS\Job\SendSms;
use Odeo\Exceptions\FailException;

class OtpSender {

  private $userRepo, $userOtpRepo, $userForgotRepo, $otpGeneratorHelper;

  const SendTypeSMS = 'sms';
  const SendTypeEMail = 'email';

  public function __construct() {
    $this->userRepo = app()->make(UserRepository::class);
    $this->userOtpRepo = app()->make(UserOtpRepository::class);
    $this->userForgotRepo = app()->make(UserForgotRepository::class);
    $this->otpGeneratorHelper = app()->make(OtpGenerator::class);
    $this->userKtpRepo = app()->make(UserKtpRepository::class);
    $this->reasonGenerator = app()->make(UserKtpReasonGenerator::class);
  }

  public function sendOtp(PipelineListener $listener, $data) {
    try {
      $phoneNumber = $data['phone_number'];
      $pulsaPrefix = app()->make(\Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaPrefixRepository::class);
      if (!$pulsaPrefix->getNominal(['prefix' => substr(purifyTelephone($phoneNumber), 0, 5)]))
        return $listener->response(400, 'You need to use a valid Indonesian phone number. Please contact @odeo_cs for further information.');

      $userOtpBefore = $this->userOtpRepo->findAllTelephone($phoneNumber);
      list ($send, $token) = $this->generateToken($userOtpBefore, $phoneNumber, $data);

      if ($send) {
        isProduction() &&
        $listener->pushQueue(new SendSms($this->produceSendSmsData($userOtpBefore, $phoneNumber, $token)));
      }

    } catch (FailException $e) {
      return $listener->response(400, $e->getMessage());
    }

    return $listener->response(201, $this->produceSuccessData($userOtpBefore));
  }

  public function sendForgotByEmail(PipelineListener $listener, $data) {
    try {
      $user = $this->userRepo->findById($data['user_id']);

      $this->checkUserStatus($user);
      $phoneNumber = $user->telephone;

      $scenario = $data['scenario'] . '_' . $data['security_type'];

      $token = $this->newOtp($phoneNumber, false, $scenario);

      isProduction() &&
      $listener->pushQueue(new SendOtpEmail($user->email, $this->produceSendEmailData($token)));

      $this->storeForgotData($user->id, $token, $data['security_type'], self::SendTypeEMail);

    } catch (FailException $e) {
      if ($e->getErrorStatus()) {
        return $listener->responseWithErrorStatus($e->getErrorStatus(), $e->getMessage());
      } else {
        return $listener->response(400, $e->getMessage());
      }
    }

    return $listener->response(200);
  }

  public function sendForgotPasswordOtp(PipelineListener $listener, $data) {
    try {
      $user = $this->userRepo->findByTelephone($data['phone_number']);

      $this->checkUserStatus($user);

      $phoneNumber = $user->telephone;

      $userOtpBefore = $this->userOtpRepo->findAllTelephone($phoneNumber);
      list ($send, $token) = $this->generateToken($userOtpBefore, $phoneNumber, $data);

      if ($send) {
        isProduction() &&
        $listener->pushQueue(new SendSms($this->produceSendSmsData($userOtpBefore, $phoneNumber, $token)));

        $this->storeForgotData($user->id, $token, 'password');
      }

    } catch (FailException $e) {
      if ($e->getErrorStatus()) {
        $errorStatus = $e->getErrorStatus();
        $errorMessage = $e->getMessage();

        if ($errorStatus === ErrorStatus::ACCOUNT_BANNED){
          $user = $this->userRepo->findByTelephone($data['phone_number']);
          $userKtp = $this->userKtpRepo->findLastByUserId($user->id);
          if ($userKtp && $userKtp->verified_at === null && $userKtp->rejected_at === null) {
            $errorStatus = ErrorStatus::ACCOUNT_BANNED_UPLOADED;
          } else if ($userKtp && $userKtp->rejected_at !== null) {
            $errorStatus = ErrorStatus::ACCOUNT_BANNED_REJECTED;
            $errorMessage = $this->reasonGenerator->getReasonsArray(
              $userKtp->nik_status,
              $userKtp->name_status,
              $userKtp->ktp_status,
              $userKtp->ktp_selfie_status,
              $userKtp->last_ocash_balance_status,
              $userKtp->last_transaction_description_status,
              true
            );
          }
        }

        return $listener->responseWithErrorStatus($errorStatus, $errorMessage);
      } else {
        return $listener->response(400, $e->getMessage());
      }
    }

    return $listener->response(201, $this->produceSuccessData($userOtpBefore));
  }

  public function sendUpdatePinOtp(PipelineListener $listener, $data) {
    try {
      $user = $this->userRepo->findByid($data['auth']['user_id']);

      $this->checkUserStatus($user);

      $phoneNumber = $user->telephone;

      $userOtpBefore = $this->userOtpRepo->findAllTelephone($phoneNumber);
      list ($send, $token) = $this->generateToken($userOtpBefore, $phoneNumber, $data);

      if ($send) {
        isProduction() &&
        $listener->pushQueue(new SendSms($this->produceSendSmsData($userOtpBefore, $phoneNumber, $token)));

        $this->storeForgotData($user->id, $token, 'pin');
      }

    } catch (FailException $e) {
      if ($e->getErrorStatus()) {
        return $listener->responseWithErrorStatus($e->getErrorStatus(), $e->getMessage());
      } else {
        return $listener->response(400, $e->getMessage());
      }
    }

    return $listener->response(201, $this->produceSuccessData($userOtpBefore));
  }

  public function requestOtpEmail(PipelineListener $listener, $data) {
    $user = $this->userRepo->findByTelephone($data['phone_number']);
    if ($user) {
      $token = $this->newOtp($data['phone_number'], false, $data['scenario']);
      $this->storeForgotData($user->id, $token, 'password');

      $listener->pushQueue(new SendOtpEmail($user->email, $this->produceSendEmailData($token)));

      return $listener->response(200, [
        'message' => 'OTP has been sent to ' . maskEmail($user->email) . ', please check your email.'
      ]);
    }
    return $listener->response(400, "User not found");
  }

  private function storeForgotData($userId, $token, $securityType, $sendType = 'sms') {
    $this->userForgotRepo->deleteAllUserForgotByUserId($userId);

    $userForgot = $this->userForgotRepo->getNew();
    $userForgot->user_id = $userId;
    $userForgot->type = $sendType;
    $userForgot->token = $token;
    $userForgot->security_type = $securityType;
    $userForgot->created_at = date('Y-m-d H:i:s');

    $this->userForgotRepo->save($userForgot);
  }

  private function produceSuccessData($userOtpBefore) {
    return [
      "ttl" => 300000,
      "remains" => 3 - count($userOtpBefore)
    ];
  }


  private function generateToken($userOtpBefore, $phoneNumber, $data) {
    $lastUserOtp = $userOtpBefore->first();

    $now = Carbon::now();
    $lastUserOtpCreatedAt = $lastUserOtp ? Carbon::parse($lastUserOtp->created_at) : null;

    if (count($userOtpBefore) >= 3) {
      if ($lastUserOtpCreatedAt && $lastUserOtpCreatedAt->diffInHours($now) == 0) {
        throw new FailException('You have reached OTP request limit (3 times). Please try again in an hour.');
      } else {
        $this->userOtpRepo->archiveOtp($phoneNumber);
      }
    }

    $sendOtp = !$lastUserOtpCreatedAt || $lastUserOtpCreatedAt->diffInMinutes($now) >= 5;

    if ($sendOtp) {
      $force4DigitsOtp = isset($data['force_4_digits_otp']) && $data['force_4_digits_otp'];
      $scenario = $data['scenario'] ?? 'unknown';
      $token = $this->newOtp($phoneNumber, $force4DigitsOtp, $scenario);

      return [true, $token];
    }

    return [false, null];
  }

  private function newOtp($phoneNumber, $force4DigitsOtp = false, $scenario) {
    $token = $this->otpGeneratorHelper->generateOtp($force4DigitsOtp);

    $userOtp = $this->userOtpRepo->getNew();

    $userOtp->telephone = $phoneNumber;
    $userOtp->otp = $token;
    $userOtp->is_used = 0;
    $userOtp->created_at = date('Y-m-d H:i:s');
    $userOtp->scenario = $scenario;
    $userOtp->updated_at = date('Y-m-d H:i:s');

    $this->userOtpRepo->save($userOtp);

    return $token;
  }

  private function produceSendSmsData($userOtpBefore, $phoneNumber, $token) {
    /*return [
      'sms_destination_number' => $phoneNumber,
      'sms_text' => trans(isOcashApp() ? 'user.sms_otp' : 'user.sms_otp_shorten', [
        'otp' => $token
      ]),
      'sms_path' => $this->switchSmsVendor(count($userOtpBefore))
    ];*/
    $count = count($userOtpBefore);
    return [
      'sms_destination_number' => $phoneNumber,
      'sms_text' => trans('user.sms_otp', [
        'otp' => $token
      ]),
      'sms_path' => $this->switchSmsVendor($count)
    ];
  }

  private function produceSendEmailData($token) {
    return [
      'text' => trans('user.sms_otp', [
        'otp' => $token
      ]),
      'otp' => $token
    ];
  }

  private function switchSmsVendor($userOtpAttempt) {
    if (isOcashApp()) {
      return SMS::ZENZIVA;
    }

    /*switch ($userOtpAttempt) {
      case 0:
        return SMS::DARTMEDIA;
      case 1:
        return SMS::ZENZIVA;
      default:
        return SMS::NEXMO;
    }*/

    switch ($userOtpAttempt) {
      case 0:
        return SMS::DARTMEDIA;
      default:
        return SMS::ZENZIVA;
    }
  }

  private function checkUserStatus($user) {
    if (!$user) {
      throw new FailException('Telephone number is not existed in our data.');

    } else if (in_array($user->status, UserStatus::groupBanned())) {
      throw new FailException(trans('errors.account_banned'), ErrorStatus::ACCOUNT_BANNED);

    } else if ($user->status == UserStatus::DEACTIVATED) {
      throw new FailException(trans('errors.account_deactivated'));
    }
  }

}
