<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/28/17
 * Time: 5:34 PM
 */

namespace Odeo\Domains\Account;

use Odeo\Domains\Core\PipelineListener;

class ProvinceSelector {

  private $province, $city;

  public function __construct() {
    $this->province = app()->make(\Odeo\Domains\Account\Repository\ProvinceRepository::class);
    $this->city = app()->make(\Odeo\Domains\Account\Repository\CityRepository::class);
  }

  public function get(PipelineListener $listener, $data) {
    if($data = $this->province->gets()) return $listener->response(200, ['provinces' => $data]);
    return $listener->response(204, ['provinces' => []]);
  }

}