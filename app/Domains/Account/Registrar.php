<?php

namespace Odeo\Domains\Account;

use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Account\Appidentifier\Repository\UserAndroidIdentifierRepository;
use Odeo\Domains\Account\Jobs\SendNewBusinessUser;
use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Account\Repository\UserFcmTokenRepository;
use Odeo\Domains\Account\Repository\UserOtpRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Marketing\Repository\BusinessResellerRepository;
use Odeo\Exceptions\FailException;

class Registrar {

  private $userRepo, $userOtpRepo, $userFcmTokenRepo, $appIdentifierRepo;

  public function __construct() {
    $this->userRepo = app()->make(UserRepository::class);
    $this->userOtpRepo = app()->make(UserOtpRepository::class);
    $this->userFcmTokenRepo = app()->make(UserFcmTokenRepository::class);
    $this->appIdentifierRepo = app()->make(UserAndroidIdentifierRepository::class);
  }

  public function signUp(PipelineListener $listener, $data) {
    try {
      $this->checkIsDeviceHasReachLimit($data);
      $this->checkIsPhoneNumberHasOtp($data);
    } catch (FailException $e) {
      return $listener->response(400, $e->getMessage());
    }

    $user = $this->userRepo->getNew();

    if (isset($data['upgrade']) && $data['upgrade']) {
      $user = $this->userRepo->findById($data['auth']['user_id']);
    }

    $user->store($data);
    $user->password = Hash::make($data['password']);

    if (!isVersionSatisfy(Platform::ANDROID, '3.0.0') && !isVersionSatisfy(Platform::IOS, '3.0.0')) {
      if (isset($data['transaction_pin'])) {
        list($isValid, $message) = checkPinSecure($data['transaction_pin']);
        if (!$isValid) return $listener->response(400, $message);
      }
      $user->transaction_pin = isset($data['transaction_pin'])
        ? Hash::make($data['transaction_pin'])
        : $user->password;
    }

    if (isAdmin() && isset($data['email'])) {
      $user->is_email_verified = true;
      $user->mail_verified_at = date('Y-m-d H:i:s');
    }

    $user->status = UserStatus::OK;
    $this->userRepo->save($user);

    if (!isAdmin()) $this->userOtpRepo->archiveOtp($data['phone_number']);

    return $listener->response(200, [
      'user_id' => $user->id,
      'is_new_device' => $this->isNewDevice($data)
    ]);
  }

  public function signUpBusiness(PipelineListener $listener, $data) {

    if (isProduction()) {
      if (!isset($data['reseller_user_id']))
        return $listener->response(400, 'Need active reseller');
      if ($data['reseller_user_id'] != '0' && !$this->userRepo->findById($data['reseller_user_id']))
        return $listener->response(400, 'Reseller not exists');
    }

    $password = sprintf('%06d', rand('000000', '999999'));
    $pin = sprintf('%06d', rand('000000', '999999'));

    $user = $this->userRepo->getNew();
    $user->telephone = $this->userRepo->generateNewBusinessNumber();
    $user->email = $data['email'];
    $user->name = trim($data['name']);
    $user->password = Hash::make($password);
    $user->transaction_pin = Hash::make($pin);
    $user->type = UserType::SELLER;
    $user->status = UserStatus::FORCE_CHANGE_PASSWORD;
    $user->is_email_verified = true;
    $user->mail_verified_at = Carbon::now()->toDateTimeString();
    $this->userRepo->save($user);

    if (isProduction() && $data['reseller_user_id'] != '0') {
      $resellerRepo = app()->make(BusinessResellerRepository::class);
      $reseller = $resellerRepo->getNew();
      $reseller->user_id = $user->id;
      $reseller->reseller_user_id = $data['reseller_user_id'];
      $reseller->template_id = 2; // DEFAULT 10%
      $resellerRepo->save($reseller);
    }

    $ccEmails = [];
    if (!empty($data['cc'])) {
      $ccEmails = explode(';', $data['cc']);
    }

    dispatch(new SendNewBusinessUser($user->email, $user->name, $user->telephone, $password, $pin, $ccEmails));

    return $listener->response(200, [
      'name' => $user->name,
      'email' => $user->email,
      'telephone' => revertTelephone($user->telephone),
      'cc' => $data['cc'] ?? '-'
    ]);
  }

  public function autoSign(PipelineListener $listener, $data) {

    $telephone = $data['telephone'] ?? $data['phone_number'] ?? null;

    if (!$telephone) {
      return $listener->response(400, "User sudah pernah terdaftar.");
    }

    if (!$user = $this->userRepo->findByTelephone($telephone)) {

      $randomPass = rand(100000, 999999);

      $user = $this->userRepo->getNew();

      $user->name = $data['name'] ?? '';
      $user->street = $data['street'] ?? null;
      $user->telephone = $telephone;
      $user->password = Hash::make($randomPass);
      $user->transaction_pin = $user->password;
      $user->type = UserType::SELLER;
      $user->status = $data['status'] ?? UserStatus::RESET_PIN;

      $this->userRepo->save($user);

      $data['password'] = $randomPass;
    }

    $data['phone_number'] = $user->telephone;
    $data['user_id'] = $user->id;

    return $listener->response(200, $data);

  }

  private function isNewDevice($data) {
    return isset($data['device_token']) &&
      !$this->userFcmTokenRepo->findAllByDeviceToken($data['device_token']);
  }

  private function checkIsDeviceHasReachLimit($data) {
    if (isset($data['ssaid']) &&
      $this->appIdentifierRepo->getSSAIDCount($data['ssaid']) >= UserStatus::ANDROID_SSAID_LIMIT) {
      throw new FailException('Registrasi gagal');
    }
  }

  private function checkIsPhoneNumberHasOtp($data) {
    if (!isAdmin() && !$this->userOtpRepo->findUsedTelephone($data['phone_number'])) {
      throw new FailException(trans('errors.telephone_not_verified'));
    }
  }

}
