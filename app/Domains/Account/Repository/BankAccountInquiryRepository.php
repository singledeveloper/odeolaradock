<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Account\Model\BankAccountInquiry;
use Odeo\Domains\Core\Repository;

class BankAccountInquiryRepository extends Repository {

  public function __construct(BankAccountInquiry $bankTransferAccountInquiry) {
    $this->model = $bankTransferAccountInquiry;
  }

  public function findByBankIdAndAccountNumber($bankId, $accountNumber) {
    return $this->model
      ->where('bank_id', $bankId)
      ->where('account_number', $accountNumber)
      ->orderByDesc('id')
      ->first();
  }

}
