<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/28/17
 * Time: 6:28 PM
 */

namespace Odeo\Domains\Account\Repository;


use Odeo\Domains\Account\Model\City;
use Odeo\Domains\Core\Repository;

class CityRepository extends Repository {
  public function __construct(City $city) {
    $this->model = $city;
  }

  public function getByProvinceid($provinceId) {
    return $this->getModel()->where('province_id', '=', $provinceId)
      ->orderBy('name')
      ->select('id', 'name')
      ->get();
  }
}