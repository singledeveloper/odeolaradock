<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 23-Jan-18
 * Time: 11:36 AM
 */

namespace Odeo\Domains\Account\Repository;


use Odeo\Domains\Account\Model\FavoriteNumber;
use Odeo\Domains\Core\Repository;

class FavoriteNumberRepository extends Repository {

  public function __construct(FavoriteNumber $favoriteNumber) {
    $this->model = $favoriteNumber;
  }

  public function gets($userId) {
    $query = $this->getModel()
      ->where('user_id', $userId)
      ->orderBy('created_at', 'desc');

    return $this->getResult($query);
  }

  public function getsByUserIdAndCategoryAndType($userId, $category, $types) {
    $query = $this->getModel()
      ->where('user_id', $userId)
      ->where('category', $category);

    if ($types) {
      $query = $query->where(function ($query) use ($types) {
        $query->whereIn('type', $types)->orWhereNull('type');
      });
    }

    $query = $query->orderBy('created_at', 'desc');

    return $this->getResult($query);
  }

  public function deleteByUserId($userId, $ids = []) {
    $this->model
      ->where('user_id', $userId)
      ->whereIn('id', $ids)
      ->delete();
  }

}
