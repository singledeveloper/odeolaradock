<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/28/17
 * Time: 5:35 PM
 */

namespace Odeo\Domains\Account\Repository;


use Odeo\Domains\Account\Model\Province;
use Odeo\Domains\Core\Repository;

class ProvinceRepository extends Repository {
  public function __construct(Province $province) {
    $this->model = $province;
  }

  public function gets() {
    return $this->getModel()->select('id', 'name')->orderBy('name')->get();
  }
}