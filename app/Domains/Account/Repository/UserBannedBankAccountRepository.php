<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Account\Model\UserBannedBankAccount;
use Odeo\Domains\Core\Repository;

class UserBannedBankAccountRepository extends Repository {

  public function __construct(UserBannedBankAccount $userBannedBankAccount) {
    $this->model = $userBannedBankAccount;
  }

  public function findByAccountNumber($bankAccountNumber) {
    return $this->model->where('account_number', $bankAccountNumber)
      ->where('is_banned', true)->first();
  }

  public function gets() {
    $filters = $this->getFilters();
    $query = $this->model
    ->selectRaw('
      user_banned_bank_accounts.id,
      bank_id, 
      banks.name as bank_name, 
      account_number,
      is_banned, 
      banned_reason,
      user_banned_bank_accounts.created_at,
      user_banned_bank_accounts.updated_at
    ')
    ->join('banks', 'banks.id', '=', 'user_banned_bank_accounts.bank_id');

    if (isset($filters['search'])) {
      if (isset($filters['search']['account_number'])) {
        $query = $query->where('account_number', 'like', '%' . $filters['search']['account_number'] . '%');
      }
      if (isset($filters['search']['bank_id'])) {
        $query = $query->where('bank_id', '=', $filters['search']['bank_id']);
      }
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

}
