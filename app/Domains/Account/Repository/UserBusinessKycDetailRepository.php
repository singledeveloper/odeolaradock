<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Account\Model\UserBusinessKycDetail;
use Odeo\Domains\Constant\UserKycStatus;
use Odeo\Domains\Core\Repository;

class UserBusinessKycDetailRepository extends Repository {

  public function __construct(UserBusinessKycDetail $userBusinessKycDetail) {
    $this->model = $userBusinessKycDetail;
  }

  public function cancelAll($kycId) {
    return $this->getCloneModel()->where('kyc_id', $kycId)
      ->where('kyc_status', '<>', UserKycStatus::CANCELLED)->update(['kyc_status' => UserKycStatus::CANCELLED]);
  }

  public function getByKycId($kycId) {
    return $this->getCloneModel()->where('kyc_id', $kycId)
      ->where('kyc_status', '<>', UserKycStatus::CANCELLED)->get()->keyBy('option_id');
  }

  public function getStatusCount($kycId, $status) {
    return $this->getCloneModel()->where('kyc_id', $kycId)
      ->whereNotIn('option_id', UserKycStatus::SKIPPED_OPTIONS)
      ->where('kyc_status', $status)->count();
  }

}
