<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Account\Model\UserBusinessKycOption;
use Odeo\Domains\Constant\UserKycStatus;
use Odeo\Domains\Core\Repository;

class UserBusinessKycOptionRepository extends Repository {

  public function __construct(UserBusinessKycOption $userBusinessKycOption) {
    $this->model = $userBusinessKycOption;
  }

  public function getByBusinessType($businessType) {
    return $this->getCloneModel()->where('business_type', $businessType)
      ->orderBy('id', 'asc')->get();
  }

  public function getPassedOptionCount($businessType) {
    return $this->getCloneModel()->where('business_type', $businessType)
      ->whereNotIn('id', UserKycStatus::SKIPPED_OPTIONS)
      ->count();
  }

}
