<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Account\Model\UserBusinessKyc;
use Odeo\Domains\Constant\UserKycStatus;
use Odeo\Domains\Core\Repository;

class UserBusinessKycRepository extends Repository {

  public function __construct(UserBusinessKyc $userBusinessKyc) {
    $this->model = $userBusinessKyc;
  }

  public function checkCurrentKyc($userId) {
    return $this->getCloneModel()
      ->where('user_id', $userId)
      ->where('status', '<>', UserKycStatus::CANCELLED)
      ->first();
  }

  public function gets() {
    return $this->getResult($this->getCloneModel()
      ->join('users', 'users.id', '=', 'user_business_kycs.user_id')
      ->select('user_business_kycs.*', 'users.name as user_name')
      ->orderBy('user_business_kycs.updated_at', 'desc'));
  }

  public function checkMonitoringUsers($userIds) {
    return $this->getCloneModel()->whereIn('user_id', $userIds)
      ->where('status', '<>', UserKycStatus::CANCELLED)
      ->select('id', 'user_id', 'status')
      ->get()->keyBy('user_id');
  }

}
