<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Account\Model\UserDebt;
use Odeo\Domains\Core\Repository;

class UserDebtRepository extends Repository {

  public function __construct(UserDebt $userDebt) {
    $this->model = $userDebt;
  }

  public function getUnprocessed() {
    return $this->getCloneModel()->where('is_processed', false)->get();
  }
}
