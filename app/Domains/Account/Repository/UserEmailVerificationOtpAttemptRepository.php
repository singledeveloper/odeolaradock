<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 24-Jul-18
 * Time: 1:43 PM
 */

namespace Odeo\Domains\Account\Repository;


use Odeo\Domains\Account\Model\UserEmailVerificationOtpAttempt;
use Odeo\Domains\Core\Repository;

class UserEmailVerificationOtpAttemptRepository extends Repository {

  public function __construct(UserEmailVerificationOtpAttempt $userEmailVerificationOtpAttempt) {
    $this->model = $userEmailVerificationOtpAttempt;
  }

  public function getOTPAttemptCount($email) {
    return $this->model
      ->selectRaw('count(otp) as total_attempt, max(created_at) last_attempt_at')
      ->where('email', $email)
      ->where('is_attempted', true)
      ->first();
  }

  public function disableAttempt($email) {
    $this->model
      ->where('email', $email)
      ->where('is_attempted', true)
      ->update(['is_attempted' => false]);
  }
}