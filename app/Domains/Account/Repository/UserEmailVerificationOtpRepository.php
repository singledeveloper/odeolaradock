<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 23-Jul-18
 * Time: 6:23 PM
 */

namespace Odeo\Domains\Account\Repository;


use Carbon\Carbon;
use Odeo\Domains\Account\Model\UserEmailVerificationOtp;
use Odeo\Domains\Core\Repository;

class UserEmailVerificationOtpRepository extends Repository {

  public function __construct(UserEmailVerificationOtp $userEmailVerificationOtp) {
    $this->model = $userEmailVerificationOtp;
  }

  public function findExact($email, $otp) {
    return $this->model
      ->where('email', $email)
      ->where('otp', $otp)
      ->where('is_used', 0)
      ->orderBy('created_at', 'desc')
      ->first();
  }

  public function findAllEmail($email) {
    return $this->model
      ->where('email', $email)
      ->where('is_used', 0)
      ->where(\DB::raw('created_at'), '>', Carbon::now()->subHours(12)->toDateTimeString())
      ->orderBy('created_at', 'desc')
      ->get();
  }

  public function deleteByUserId($userId) {
    return $this->model->where('user_id', $userId)->delete();
  }

  public function deleteByEmail($email) {
    return $this->model->where('email', $email)->delete();
  }

}