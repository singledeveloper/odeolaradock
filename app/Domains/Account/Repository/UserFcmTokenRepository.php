<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Account\Model\UserFcmToken;

class UserFcmTokenRepository extends Repository {

  public function __construct(UserFcmToken $userFcmToken) {
    $this->model = $userFcmToken;
  }

  public function findByUserId($userId) {

    $query = $this->model;
    if (is_array($userId)) $query = $query->whereIn('user_id', $userId);
    else $query = $query->where('user_id', $userId);

    return $query->get();
  }

  public function findDeviceToken($userId, $tokenId) {
    return $this->model->where('user_id', $userId)->where('device_token', $tokenId)->first();
  }

  public function findAllByDeviceToken($tokenId) {
    return $this->model->where("device_token", $tokenId)->first();
  }

  public function removeDeviceToken($userId, $tokenId) {
    return $this->model
      ->where('user_id', $userId)
      ->orWhere('device_token', $tokenId)
      ->delete();
  }

}
