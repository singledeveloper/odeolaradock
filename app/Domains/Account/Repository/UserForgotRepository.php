<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Account\Model\UserForgot;

class UserForgotRepository extends Repository {

  public function __construct(UserForgot $userForgot) {
    $this->model = $userForgot;
  }

  public function findByEmailToken($userId, $token) {
    return $this->model
      ->where('user_id', $userId)
      ->where('type', 'email')
      ->where('token', $token)
      ->first();
  }

  public function findByUserId($userId, $securityType) {
    return $this->model
      ->where('user_id', $userId)
      ->where('security_type', $securityType)
      ->first();
  }

  public function deleteAllUserForgotByUserId($userId) {
    $this->model->where('user_id', $userId)->delete();
  }


}
