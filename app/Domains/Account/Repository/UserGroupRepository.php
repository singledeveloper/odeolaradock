<?php

namespace Odeo\Domains\Account\Repository;

use Carbon\Carbon;
use Odeo\Domains\Account\Model\UserGroup;
use Odeo\Domains\Core\Repository;

class UserGroupRepository extends Repository {

  public function __construct(UserGroup $userGroup) {
    $this->model = $userGroup;
  }

  public function findUserWithinGroup($userId, $groups) {
    if (!is_array($groups)) $groups = [$groups];
    return $this->getCloneModel()->where('user_id', $userId)
      ->whereIn('group_name', $groups)
      ->where(function($query){
        $query->whereNull('expired_at')
          ->orWhere('expired_at', '>', Carbon::now()->toDateTimeString());
      })
      ->first();
  }

  public function getUserIds($groups) {
    if (!is_array($groups)) $groups = [$groups];
    $userIds = [];
    foreach ($this->getCloneModel()->whereIn('group_name', $groups)->get() as $item) {
      if (!in_array($item->user_id, $userIds)) $userIds[] = $item->user_id;
    }
    return $userIds;
  }

}
