<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Account\Model\UserIpLocation;

class UserIpLocationRepository extends Repository {

  public function __construct(UserIpLocation $userIpLocation) {
    $this->model = $userIpLocation;
  }
  
  public function findLatestByIp($ip) {
    return $this->getCloneModel()->where('ip', $ip)->orderBy('created_at', 'desc')->first();
  }
}
