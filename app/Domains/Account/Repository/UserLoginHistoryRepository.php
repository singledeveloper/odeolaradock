<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Account\Model\UserLoginHistory;

class UserLoginHistoryRepository extends Repository {

  public function __construct(UserLoginHistory $userLoginHistory) {
    $this->model = $userLoginHistory;
  }

  public function getUserLoginCount($userId) {
    return $this->model->where('user_id', $userId)->count();
  }

  public function checkIp($userId, $ip) {
    return $this->model->where('user_id', $userId)->where('ip_address', $ip)->first();
  }

}
