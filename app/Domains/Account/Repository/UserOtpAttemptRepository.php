<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Account\Model\UserOtpAttempt;
use Odeo\Domains\Core\Repository;

class UserOtpAttemptRepository extends Repository {

  public function __construct(UserOtpAttempt $userOtpAttempt) {
    $this->model = $userOtpAttempt;
  }

  public function getIPCount($ip) {
    return $this->model->where('ip_address', $ip)->where('is_attempted', true)->count();
  }

  public function getLastOtpAttempt($telephone) {
    return $this->model
      ->where('telephone', $telephone)
      ->orderBy('id', 'desc')
      ->first();
  }

  public function getOTPAttemptCount($telephone) {
    return $this->model
      ->selectRaw('count(otp) as total_attempt, max(created_at) last_attempt_at')
      ->where('telephone', $telephone)
      ->where('is_attempted', true)
      ->first();
  }

  public function disableAttempt($telephone) {
    $this->model->where('telephone', $telephone)->where('is_attempted', true)->update(['is_attempted' => false]);
  }

  public function gets() {
    $filters = $this->getFilters();

    $query = $this->model;

    if (isset($filters['search'])) {
      if (isset($filters['search']['telephone'])) {
        $query = $query->where('telephone', 'like', '%' . $filters['search']['telephone'] . '%');
      }
      if (isset($filters['search']['ip_address'])) {
        $query = $query->where('ip_address', 'like', '%' . $filters['search']['ip_address'] . '%');
      }
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

}
