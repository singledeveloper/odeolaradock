<?php

namespace Odeo\Domains\Account\Repository;

use Carbon\Carbon;
use Odeo\Domains\Account\Model\UserOtp;
use Odeo\Domains\Core\Repository;

class UserOtpRepository extends Repository {

  public function __construct(UserOtp $userOtp) {
    $this->model = $userOtp;
  }
  
  public function findByTelephone($telephone) {
    return $this->model->where('telephone', $telephone)->where('is_archived', false)->orderBy('created_at', 'desc')->first();
  }
  
  public function findExact($telephone, $otp) {
    return $this->model->where('telephone', $telephone)->where('otp', $otp)->where('is_used', 0)->where('is_archived', false)->orderBy('created_at', 'desc')->first();
  }
  
  public function findAllTelephone($telephone) {
    return $this->model
      ->where('telephone', $telephone)
      ->where('is_used', false)
      ->where(\DB::raw('created_at'), '>', Carbon::now()->subHours(12)->toDateTimeString())
      ->where('is_archived', false)
      ->orderBy('created_at', 'desc')
      ->get();
  }

  public function archiveOtp($telephone) {
    return $this->getCloneModel()
      ->where('telephone', $telephone)
      ->where('is_archived', false)
      ->update(['is_archived' => true]);
  }
  
  public function findUsedTelephone($telephone) {
    return $this->model->where('telephone', $telephone)->where('is_used', 1)->orderBy('created_at', 'desc')->first();
  }
  
}
