<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Account\Model\UserPasswordHistory;
use Odeo\Domains\Core\Repository;

class UserPasswordHistoryRepository extends Repository {

  public function __construct(UserPasswordHistory $userPasswordHistory) {
    $this->model = $userPasswordHistory;
  }

  public function checkUserId($userId) {
    return $this->model->where('user_id', $userId)->first();
  }

}
