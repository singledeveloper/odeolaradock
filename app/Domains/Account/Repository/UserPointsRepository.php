<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Account\Model\UserPoint;

class UserPointsRepository extends Repository {

  public function __construct(UserPoint $userPoint) {
    $this->model = $userPoint;
  }

}
