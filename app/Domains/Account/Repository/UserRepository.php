<?php

namespace Odeo\Domains\Account\Repository;

use Carbon\Carbon;
use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\Repository;

class UserRepository extends Repository {

  public function __construct(User $user) {
    $this->model = $user;
  }

  public function getByContact() {
    $filters = $this->getFilters();
    $query = $this->model;

    if (isset($filters["telephone_list"])) {
      $telps = [];
      foreach (explode(",", $filters["telephone_list"]) as $item) {
        $telps[] = purifyTelephone($item);
      }
      $query = $query->whereIn("telephone", $telps);
    }

    return $this->getResult($query);
  }

  public function gets() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel();
    if (isset($filters['search'])) {
      if (isset($filters['search']['user_id'])) {
        $tempq = $query->where('id', '=', $filters['search']['user_id']);
        if (!$tempq->count()) {
          $tempq = $query->where('telephone', '=', purifyTelephone($filters['search']['user_id']));
        }
        $query = $tempq;
      }
      if (isset($filters['search']['name'])) {
        $query = $query->where('name', 'ilike', '%' . $filters['search']['name'] . '%');
      }
      if (isset($filters['search']['email'])) {
        $query = $query->where('email', 'like', '%' . $filters['search']['email'] . '%');
      }
      if (isset($filters['search']['user_type']) && $filters['search']['user_type'] != 'all') {
        $query = $query->where('type', 'like', '%' . $filters['search']['user_type'] . '%');
      }
      if (isset($filters['search']['status']) && $filters['search']['status'] == 'all-banned') {
        $query = $query->whereIn('status', UserStatus::groupBanned());
      } else if (isset($filters['search']['status']) && $filters['search']['status'] != 'all') {
        $query = $query->where('status', '=', $filters['search']['status']);
      }
    }
    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function findByEmail($email) {
    return $this->model->where("email", $email)->first();
  }

  public function findByTelephone($telephone) {
    return $this->model->where("telephone", $telephone)->first();
  }

  public function getUserActivity() {
    return $this->runCache('activity', 'activity.user_activity.', 60, function () {
      return [
        'registered_seller' => $this->model->where('status', '=', UserStatus::OK)->where('type', '=', UserType::SELLER)->count(),
        'registered_guest' => $this->model->where('status', '=', UserStatus::OK)->where('type', '=', UserType::GUEST)->count(),
        'registered_admin' => $this->model->where('status', '=', UserStatus::OK)->where('type', '=', UserType::ADMIN)->count(),
      ];
    });
  }

  public function registeredUserCountTodayByUserType($type) {
    $query = $this->getCloneModel();
    $date = Carbon::now()->format('Y-m-d');

    $query = $query->where('status', UserStatus::OK)
      ->where('type', $type)
      ->whereDate('created_at', '=', $date);

    return $query->count();
  }

  public function registeredUserCountLastNDayByUserType($type, $dayNum) {
    $query = $this->getCloneModel();
    $date = Carbon::now()->subDays($dayNum)->format('Y-m-d');

    $query = $query->where('status', UserStatus::OK)
      ->where('type', $type)
      ->whereDate('created_at', '=', $date);

    return $query->count();
  }


  public function getUserAgent($userIds = []) {

    $query = $this->model
      ->whereIn('id', $userIds)
      ->select('id', 'telephone', 'name');

    return $this->getResult($query->orderBy('id', 'asc'));
  }

  public function getRecentRegisteredUsers($date) {
    return $this->getCloneModel()
      ->whereNotNull('email')
      ->whereDate('created_at', '>=', $date)
      ->orWhereDate('updated_at', '>=', $date)
      ->select('email');
  }

  public function updateStatusDormantUser($whitelistedUser = []) {

    $this->model
      ->where('status', '=', UserStatus::OK)
      ->where('type', '=', 'seller')
      ->where('updated_at', '<', Carbon::now()->addDays(-90))
      ->whereNotIn('id', $whitelistedUser)
      ->update(['status' => UserStatus::RESET_PIN]);

  }

  public function getCsOdeoUsers() {
    return $this->model
      ->select('id')
      ->where('type', 'admin')
      ->where('has_qiscus_account', true)
      ->get();
  }

  public function getBusinessUsers($notUserIds = []) {
    $query = $this->getCloneModel()
      ->where('telephone', 'like', UserType::BUSINESS_PREFIX . '%')
      ->where('status', UserStatus::OK);

    if (count($notUserIds) > 0) $query->whereNotIn('id', $notUserIds);
    return $query->get();
  }

  public function generateNewBusinessNumber() {
    $result = $this->getCloneModel()->where('telephone', 'like', UserType::BUSINESS_PREFIX . '%')
      ->whereNotIn('telephone', ['628030078']) // TEMPORARY
      ->orderBy('telephone', 'desc')->first();
    $last = $result ? (intval(str_replace(UserType::BUSINESS_PREFIX, '', $result->telephone)) + 1) : 1;
    if ($last == 77 || $last == 78) $last = 79; // TEMPORARY
    return UserType::BUSINESS_PREFIX . sprintf("%04d", $last);
  }

}
