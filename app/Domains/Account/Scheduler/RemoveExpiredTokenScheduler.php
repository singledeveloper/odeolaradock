<?php

namespace Odeo\Domains\Account\Scheduler;

use Odeo\Domains\Account\Device\DeviceRemover;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\Pipeline;

class RemoveExpiredTokenScheduler {

  private $userToken;
  
  public function __construct() {
    $this->userToken = app()->make(\Odeo\Domains\Account\Repository\UserTokenRepository::class);
  }

  public function run() {
    $expiredUserTokens = $this->userToken->deleteExpiredTokens();
  }

}