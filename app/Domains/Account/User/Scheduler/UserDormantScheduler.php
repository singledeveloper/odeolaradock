<?php

namespace Odeo\Domains\Account\User\Scheduler;

use Odeo\Domains\Account\Repository\UserGroupRepository;
use Odeo\Domains\Constant\UserType;

class UserDormantScheduler {

  private $user, $userGroupRepo;
  
  public function __construct() {
    $this->user = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->userGroupRepo = app()->make(UserGroupRepository::class);
  }

  public function run() {
    $this->user->updateStatusDormantUser($this->userGroupRepo->getUserIds(UserType::GROUP_BYPASS_CASH_LIMIT));
  }

}