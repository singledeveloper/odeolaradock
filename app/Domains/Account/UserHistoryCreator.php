<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Account\Repository\UserHistoryRepository;

class UserHistoryCreator {

  private $userHistories;

  public function __construct() {
    $this->userHistories = app()->make(UserHistoryRepository::class);
  }

  public function create($user, $changedByUserId = null) {
    if (($dirty = $user->getDirty()) && !empty($dirty)) {
      array_walk($dirty, function(&$value, $key, $user) {
        $value = $user->getOriginal($key);
      }, $user);
      $this->userHistories->save(array_merge($dirty, [
        'user_id' => $user->id,
        'changed_by_user_id' => $changedByUserId
      ]));
    }
  }

}
