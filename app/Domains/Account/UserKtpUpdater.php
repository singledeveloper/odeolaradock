<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Account\Repository\UserHistoryRepository;
use Odeo\Domains\Account\Repository\UserKtpRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\UserKtpStatus;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Core\PipelineListener;
use Illuminate\Support\Facades\Storage;

class UserKtpUpdater {

  private $userKtpRepo, $userHistories, $userRepo;

  public function __construct() {
    $this->userKtpRepo = app()->make(UserKtpRepository::class);
    $this->userHistories = app()->make(UserHistoryRepository::class);
    $this->userRepo = app()->make(UserRepository::class);
  }

  public function updateStatus(PipelineListener $listener, $data) {
    $userKtp = $this->userKtpRepo->findById($data['id']);

    $notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);

    $notification->setup($userKtp->user_id, NotificationType::CHECK, NotificationGroup::ACCOUNT);

    switch ($data['status']) {
      case 'accept':
        $notification->userKtpAccepted();
        $userKtp->verified_at = date("Y-m-d H:i:s");
        $userKtp->verified_by_user_id = $data['auth']['user_id'];
        $userKtp->nik_status = UserKtpStatus::NIK_SUCCESS;
        $userKtp->name_status = UserKtpStatus::NAME_SUCCESS;
        $userKtp->ktp_status = UserKtpStatus::KTP_SUCCESS;
        $userKtp->ktp_selfie_status = UserKtpStatus::KTP_SELFIE_SUCCESS;
        
        if(isset($userKtp->last_ocash_balance)) $userKtp->last_ocash_balance_status = UserKtpStatus::LAST_OCASH_BALANCE_SUCCESS;
        if(isset($userKtp->last_transaction_description)) $userKtp->last_transaction_description_status = UserKtpStatus::LAST_TRANSACTION_DESCRIPTION_SUCCESS;

        $user = $this->userRepo->findById($userKtp->user_id);
        if (in_array($user->status, UserStatus::groupBanned()) && $userKtp->last_ocash_balance && $userKtp->last_transaction_description) {
          $user->status = UserStatus::OK;
          $this->userRepo->save($user);
        }
        break;
      case 'reject':
        $notification->userKtpRejected(
          $data['nik_status'], 
          $data['name_status'],
          $data['ktp_status'], 
          $data['ktp_selfie_status'],
          $data['last_ocash_balance_status'],
          $data['last_transaction_description_status']
        );
        $userKtp->rejected_at = date("Y-m-d H:i:s");
        $userKtp->rejected_by_user_id = $data['auth']['user_id'];
        $userKtp->nik_status = $data['nik_status'];
        $userKtp->name_status = $data['name_status'];
        $userKtp->ktp_status = $data['ktp_status'];
        $userKtp->ktp_selfie_status = $data['ktp_selfie_status'];

        if(isset($userKtp->last_ocash_balance)) $userKtp->last_ocash_balance_status = $data['last_ocash_balance_status'];
        if(isset($userKtp->last_transaction_description)) $userKtp->last_transaction_description_status = $data['last_transaction_description_status'];
        break;
    }

    $listener->pushQueue($notification->queue());

    $this->userKtpRepo->save($userKtp);

    $verificator = $this->userRepo->findById($data['auth']['user_id']);
    switch ($data['status']) {
      case 'accept':
        $userKtp->verified_by_user_name = $verificator->name;
        break;
      case 'reject':
        $userKtp->rejected_by_user_name = $verificator->name;
        break;
    }

    return $listener->response(200, app()->make(UserKtpSelector::class)->_transforms($userKtp, $this->userKtpRepo));

  }

  public function updateKtp(PipelineListener $listener, $data) {
    $userKtp = $this->userKtpRepo->findByUserIdWithoutReject($data["auth"]["user_id"]);

    if (!$userKtp && !isset($data['ktp_nik'])) {
      return $listener->response(400, 'NIK is required');
    } else if (isset($data['ktp_nik']) && !is_numeric($data['ktp_nik'])) {
      return $listener->response(400, 'NIK must be digit');
    } else if (!$userKtp && !isset($data['ktp_image'])) {
      return $listener->response(400, 'KTP is required');
    } else if (!$userKtp && !isset($data['ktp_selfie_image'])) {
      return $listener->response(400, 'KTP + Selfie is required');
    }

    // if the ktp is verified already, create a new one ( m-v3 behaviour )
    if (!$userKtp || $userKtp->verified_at) {
      $userKtp = $this->userKtpRepo->getNew();
      $userKtp->user_id = $data['auth']['user_id'];
    }

    if (isset($data['name'])) {
      $userKtp->name = $data['name'];
    }

    if (isset($data['ktp_nik'])) {
      $userKtp->ktp_nik = $data['ktp_nik'];
    }
    
    if (isset($data['last_ocash_balance'])) {
      $userKtp->last_ocash_balance = $data['last_ocash_balance'];
    }

    if (isset($data['last_transaction_description'])) {
      $userKtp->last_transaction_description = $data['last_transaction_description'];
    }

    $time = time();

    if (isset($data['ktp_image'])) {
      if (isset($data['string_ktp_image'])) {
        $userKtp->ktp_image_url = $data['ktp_image'];
      } else {
        $userKtp->ktp_image_url = 'ktp/' . base_convert($userKtp->id, 10, 36) . '_' . $time . '.' . $data['ktp_image']->getclientoriginalextension();
        Storage::disk('s3')->put($userKtp->ktp_image_url, file_get_contents($data['ktp_image']), 'public');
      }
    }
    if (isset($data['ktp_selfie_image'])) {
      if (isset($data['string_ktp_selfie_image'])) {
        $userKtp->ktp_selfie_image_url = $data['ktp_selfie_image'];
      } else {
        $userKtp->ktp_selfie_image_url = 'ktp_selfie/' . base_convert($userKtp->id, 10, 36) . '_' . $time . '.' . $data['ktp_selfie_image']->getclientoriginalextension();
        Storage::disk('s3')->put($userKtp->ktp_selfie_image_url, file_get_contents($data['ktp_selfie_image']), 'public');
      }
    }

    if (($dirty = $userKtp->getDirty()) && count($dirty)) {
      $this->userHistories->save(array_merge($userKtp->getDirty(), [
        'user_id' => $userKtp->user_id
      ]));
    }

    $this->userKtpRepo->save($userKtp);

    return $listener->response(200, app()->make(UserKtpSelector::class)->_transforms($userKtp, $this->userKtpRepo));
  }

}
