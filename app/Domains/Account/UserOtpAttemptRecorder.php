<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 10-Sep-18
 * Time: 12:57 PM
 */

namespace Odeo\Domains\Account;


use Carbon\Carbon;
use Odeo\Domains\Account\Jobs\SendOtpMaxAttemptAlert;
use Odeo\Domains\Account\Jobs\SendUserBannedAlert;
use Odeo\Domains\Account\Repository\UserEmailVerificationOtpAttemptRepository;
use Odeo\Domains\Account\Repository\UserEmailVerificationOtpRepository;
use Odeo\Domains\Account\Repository\UserOtpAttemptRepository;
use Odeo\Domains\Account\Repository\UserOtpRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Exceptions\FailException;

class UserOtpAttemptRecorder {

  private $userRepo, $userOtpRepo,
    $userOtpAttemptRepo, $userEmailVerificationOtpRepo, $userEmailVerificationOtpAttemptRepo;

  public function __construct() {
    $this->userRepo = app()->make(UserRepository::class);
    $this->userOtpRepo = app()->make(UserOtpRepository::class);
    $this->userOtpAttemptRepo = app()->make(UserOtpAttemptRepository::class);
    $this->userEmailVerificationOtpRepo = app()->make(UserEmailVerificationOtpRepository::class);
    $this->userEmailVerificationOtpAttemptRepo = app()->make(UserEmailVerificationOtpAttemptRepository::class);
  }

  public function recordEmailOtp($email, $verificationCode) {
    try {
      $user = $this->userRepo->findByEmail($email);
      $userId = $user->id ?? $email;

      if ($user && in_array($user->status, UserStatus::groupBanned())) {
        throw new FailException(trans('errors.account_banned'));
      }

      $attempt = $this->userEmailVerificationOtpAttemptRepo->getOTPAttemptCount($email);

      $this->checkHasReachedLimitAttempt(
        $attempt->total_attempt, $attempt->last_attempt_at, $userId, $email
      );

      $userOtp = $this->userEmailVerificationOtpRepo->findExact($email, $verificationCode);

      if ($userOtp) {
        $userOtp->is_used = true;
        $this->userEmailVerificationOtpRepo->save($userOtp);
        $this->userEmailVerificationOtpAttemptRepo->disableAttempt($email);

      } else {
        $currentAttempt = $this->saveEmailOtpAttempt($email, $verificationCode);

        $this->checkHasReachedLimitAttempt(
          $attempt->total_attempt + 1, $currentAttempt->created_at, $userId, $email
        );

        throw new FailException(trans('errors.invalid_otp_try_sgain', [
          'count' => UserStatus::OTP_ATTEMPTS_MAX - $attempt->total_attempt % UserStatus::OTP_ATTEMPTS_MAX - 1
        ]));

      }
    } catch (FailException $e) {
      return [false, $e->getMessage()];
    }

    return [true, ''];
  }

  public function recordPhoneOtp($telephone, $verificationCode) {
    try {
      $user = $this->userRepo->findByTelephone($telephone);
      $userId = $user->id ?? $telephone;

      if ($user && in_array($user->status, UserStatus::groupBanned())) {
        throw new FailException(trans('errors.account_banned'));
      }

      $attempt = $this->userOtpAttemptRepo->getOTPAttemptCount($telephone);

      $this->checkHasReachedLimitAttempt(
        $attempt->total_attempt, $attempt->last_attempt_at, $userId, $telephone
      );

      $userOtp = $this->userOtpRepo->findExact($telephone, $verificationCode);

      if ($userOtp) {
        $userOtp->is_used = true;
        $this->userOtpRepo->save($userOtp);
        $this->userOtpAttemptRepo->disableAttempt($telephone);

      } else {
        $currentAttempt = $this->savePhoneOtpAttempt($telephone, $verificationCode);

        $this->checkHasReachedLimitAttempt(
          $attempt->total_attempt + 1, $currentAttempt->created_at, $userId, $telephone
        );

        throw new FailException(trans('errors.invalid_otp_try_sgain', [
          'count' => UserStatus::OTP_ATTEMPTS_MAX - $attempt->total_attempt % UserStatus::OTP_ATTEMPTS_MAX - 1
        ]));

      }
    } catch (FailException $e) {
      return [false, $e->getMessage()];
    }

    return [true, ''];
  }

  private function checkHasReachedLimitAttempt($totalAttempt, $lastAttemptAt, $userId, $telephone) {
    if ($totalAttempt && $totalAttempt % UserStatus::OTP_ATTEMPTS_MAX == 0) {
      $attempt = $this->userOtpAttemptRepo->getLastOtpAttempt($telephone);
      
      if ($this->isNearLastAttempt($totalAttempt, $lastAttemptAt)) {
        if (!$attempt->is_sent_email) {
          $attempt->is_sent_email = true;
          $this->userOtpAttemptRepo->save($attempt);

          dispatch(new SendOtpMaxAttemptAlert([
            'user_id' => $userId,
            'telephone' => $telephone
          ]));
        }
        
        throw new FailException(trans('errors.otp_input_blocked_try_again', [
          'count' => $this->getThreshold($totalAttempt)
        ]));
      }
    }
  }

  private function isNearLastAttempt($totalAttempt, $lastAttemptAt) {
    if (!$lastAttemptAt) {
      return false;
    }

    return Carbon::now()->diffInHours($lastAttemptAt) < $this->getThreshold($totalAttempt);
  }

  private function getThreshold($totalAttempt) {
    $threshold = pow(2, $totalAttempt / UserStatus::OTP_ATTEMPTS_MAX - 1);
    return min($threshold, 24);
  }

  private function savePhoneOtpAttempt($telephone, $verificationCode) {
    $attempt = $this->userOtpAttemptRepo->getNew();

    $attempt->telephone = $telephone;
    $attempt->otp = $verificationCode;
    $attempt->ip_address = getClientIP();

    $this->userOtpAttemptRepo->save($attempt);

    return $attempt;
  }

  private function saveEmailOtpAttempt($email, $verificationCode) {
    $attempt = $this->userEmailVerificationOtpAttemptRepo->getNew();

    $attempt->email = $email;
    $attempt->otp = $verificationCode;
    $attempt->ip_address = getClientIP();

    $this->userEmailVerificationOtpAttemptRepo->save($attempt);

    return $attempt;
  }

}