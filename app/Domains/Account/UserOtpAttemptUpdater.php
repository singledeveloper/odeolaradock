<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Core\PipelineListener;

class UserOtpAttemptupdater {

  private $userOtpAttempt;

  public function __construct() {
    $this->userOtpAttempt = app()->make(\Odeo\Domains\Account\Repository\UserOtpAttemptRepository::class);
  }

  public function update(PipelineListener $listener, $data){
    if ($userOtpAttempts = $this->userOtpAttempt->findById($data["otp_attempt_id"])) {
      $userOtpAttempts->is_attempted = $data["is_attempted"];
      $this->userOtpAttempt->save($userOtpAttempts);
      return $listener->response(200);
    }
    return $listener->response(400);
  }

}
