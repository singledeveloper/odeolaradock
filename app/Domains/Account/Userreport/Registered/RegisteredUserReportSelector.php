<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/19/17
 * Time: 10:03 PM
 */

namespace Odeo\Domains\Account\Userreport\Registered;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;

class RegisteredUserReportSelector {

  private $registeredUsers;

  public function __construct() {

    $this->registeredUsers = app()->make(\Odeo\Domains\Account\Userreport\Registered\Repository\RegisteredUserReportRepository::class);

  }

  public function _transforms($data, Repository $repository) {

    $output = [];

    $output['registered_user'] = $data->registered_user;
    $output['registration_target'] = $data->registration_target;

    return $output;

  }

  public function getRegisteredUserReport(PipelineListener $listener, $data) {

    $this->registeredUsers->normalizeFilters($data);

    $dayNum = $data['day_num'];

    $response = [];

    foreach ($this->registeredUsers->getLastNDay($dayNum) as $registeredUser) {
      $date = $registeredUser->date;
      $type = $registeredUser->user_type;

      $response[$date][$type] = $this->_transforms($registeredUser, $this->registeredUsers);
    }

    return $listener->response(200, ['user' => $response]);

  }

}