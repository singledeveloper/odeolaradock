<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/19/17
 * Time: 11:07 PM
 */

namespace Odeo\Domains\Account\Userreport\Registered;


use Carbon\Carbon;
use Odeo\Domains\Account\Userreport\Helper\Initializer;

class RegisteredUserReportUpdater {

  private $registeredUser, $user;

  public function __construct() {

    $this->registeredUser = app()->make(\Odeo\Domains\Account\Userreport\Registered\Repository\RegisteredUserReportRepository::class);
    $this->user = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);

  }

  public function updateRegisteredUserReport($type, $day = Initializer::YESTERDAY) {

    $registeredUserLastNDay = $this->registeredUser->findRegisteredUserLastNDayByUserType($type, $day);

    if(!$registeredUserLastNDay) {

      $registeredCount = $this->user->registeredUserCountLastNDayByUserType($type, $day);
      $registeredUser = $this->registeredUser->getNew();
      $registeredUser->date = Carbon::now()->subDays($day)->format('Y-m-d');
      $registeredUser->user_type = $type;
      $registeredUser->registered_user = $registeredCount;
      $registeredUser->is_locked = FALSE;
      $registeredUser->registration_target = Initializer::DEFAULT_REGISTRATION_TARGET;

      $this->registeredUser->save($registeredUser);

    } else if (!$registeredUserLastNDay['is_locked']) {

      $registerCount = $this->user->registeredUserCountLastNDayByUserType($type, $day);
      $registeredUserLastNDay->registered_user = $registerCount;
      $registeredUserLastNDay->is_locked = TRUE;

      $this->registeredUser->save($registeredUserLastNDay);

    }

    $registeredUser = $this->registeredUser->findRegisteredUserTodayByUserType($type);

    if (!$registeredUser) {

      $registeredUser = $this->registeredUser->getNew();
      $registeredUser->date = Carbon::now()->format('Y-m-d');
      $registeredUser->user_type = $type;
      $registeredUser->registration_target = Initializer::DEFAULT_REGISTRATION_TARGET;

    }

    $registeredCount = $this->user->registeredUserCountTodayByUserType($type);
    $registeredUser->registered_user = $registeredCount;

    $this->registeredUser->save($registeredUser);

  }

}