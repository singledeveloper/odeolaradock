<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/19/17
 * Time: 10:12 PM
 */

namespace Odeo\Domains\Account\Userreport\Registered\Repository;


use Carbon\Carbon;
use Odeo\Domains\Account\Userreport\Registered\Model\RegisteredUserReport;
use Odeo\Domains\Core\Repository;

class RegisteredUserReportRepository extends Repository {

  public function __construct(RegisteredUserReport $registeredUserReport) {
    $this->model = $registeredUserReport;
  }

  public function getLockedReport($day) {
    $date = Carbon::now()->subDays($day)->format('Y-m-d');

    return $this->model->where('date', '=', $date)->where('is_locked', true)->get();
  }

  public function findRegisteredUserLastNDayByUserType($type, $dayNum) {

    $date = Carbon::today()->subDays($dayNum)->format('Y-m-d');

    return $this->model->where('user_type', $type)->where('date', '=', $date)->first();

  }

  public function findRegisteredUserTodayByUserType($type) {

    $date = Carbon::now()->format('Y-m-d');

    return $this->model->where('user_type', $type)->where('date', '=', $date)->first();

  }

  public function getLastNDay($dayNum) {

    $query = $this->getCloneModel();
    $filters = $this->getFilters();

    if (isset($filters['search']['start_date'])) {
      $date = Carbon::parse($filters['search']['start_date']);
    } else {
      $date = Carbon::today();
    }

    if (isset($filters['search']['cursor'])) {
      if($filters['search']['cursor'] < 0) $date->addDays($filters['search']['cursor'] * 7);
      else $date->addDays($filters['search']['cursor'] * 7);
    }

    $dateEnd = clone $date;
    $dateStart = clone $date->subDays($dayNum);

    return $query->where('date', '>', $dateStart)->where('date', '<=', $dateEnd)->orderBy('date', 'asc')->get();

  }

}