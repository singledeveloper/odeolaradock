<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/19/17
 * Time: 10:20 PM
 */

namespace Odeo\Domains\Account\Userreport\Scheduler;


use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;

class UserReportRefresher {


  private $registeredUser;

  public function __construct() {

    $this->registeredUser = app()->make(\Odeo\Domains\Account\Userreport\Registered\RegisteredUserReportUpdater::class);

  }

  public function run() {

    $pipeline = new Pipeline;

    $pipeline->add(new Task(__CLASS__, 'refreshRegisteredUserReport'));

    $pipeline->enableTransaction();
    $pipeline->execute([]);

  }

  public function refreshRegisteredUserReport(PipelineListener $listener, $data) {

    $userTypes = [
      UserType::GUEST,
      UserType::SELLER,
      UserType::ADMIN
    ];

    foreach ($userTypes as $type) {
      $this->registeredUser->updateRegisteredUserReport($type);
    }

    return $listener->response(200);

  }

}