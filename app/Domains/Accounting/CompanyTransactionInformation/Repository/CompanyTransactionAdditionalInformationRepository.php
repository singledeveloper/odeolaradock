<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 13/06/18
 * Time: 16.12
 */

namespace Odeo\Domains\Accounting\CompanyTransactionInformation\Repository;


use Odeo\Domains\Accounting\CompanyTransactionInformation\Model\CompanyTransactionAdditionalInformation;
use Odeo\Domains\Core\Repository;

class CompanyTransactionAdditionalInformationRepository extends Repository {

  public function __construct(CompanyTransactionAdditionalInformation $additionalInformation) {
    $this->model = $additionalInformation;
  }

  public function gets($cashTransactionId) {
    $query = $this->getCloneModel()->where('cash_transaction_id', $cashTransactionId);

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function findByAccountTransactionId($transactionId) {
    return $this->model
      ->join('company_cash_account_transactions', 'company_cash_account_transactions.id', '=', 'company_transaction_additional_informations.company_cash_account_transaction_id')
      ->join('company_cash_accounts', 'company_cash_accounts.id', '=', 'company_cash_account_transactions.company_cash_account_id')
      ->where('company_cash_account_transaction_id', '=', $transactionId)
      ->select('description', 'company_cash_accounts.company_name')
      ->first();
  }

  public function getInformationCountByTransactionIds($cashTransactionIds) {
    return $this->getCloneModel()->whereIn('cash_transaction_id', $cashTransactionIds)
      ->select('cash_transaction_id', \DB::raw('count(id) as total_count'))
      ->groupBy('cash_transaction_id')
      ->get()->keyBy('cash_transaction_id');
  }

  public function updateInformationByReference($cashTransactionId, $referenceType, $referenceId) {
    return $this->getCloneModel()->where('reference_type', $referenceType)
      ->where('reference_id', $referenceId)->update(['cash_transaction_id' => $cashTransactionId]);
  }

  public function getInformationByReferenceIds($ids, $referenceType) {
    return $this->getCloneModel()->where('reference_type', $referenceType)
      ->whereIn('reference_id', $ids)->get();
  }

}