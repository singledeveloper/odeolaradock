<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 12/12/17
 * Time: 17.30
 */

namespace Odeo\Domains\Accounting\InquiryInformation;


use Odeo\Domains\Constant\AwsConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class InquiryInformationFileImageSelector implements SelectorListener {

  private $inquiryFileImages, $inquiryInformation;

  public function __construct() {
    $this->inquiryInformation = app()->make(\Odeo\Domains\Accounting\InquiryInformation\Repository\BankInquiryAdditionalInformationRepository::class);
    $this->inquiryFileImages = app()->make(\Odeo\Domains\Accounting\InquiryInformation\Repository\BankInquiryAdditionalInformationFileImageRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    if ($item->image_url != '') {
      $item->image_url = AwsConfig::S3_BASE_URL . '/' . $item->image_url;
    }
    return $item;
  }

  public function getInquiryInformationFileImages(PipelineListener $listener, $data) {
    $this->inquiryFileImages->normalizeFilters($data);
    $this->inquiryFileImages->setSimplePaginate(true);

    $inquiryFileImages = [];
    foreach ($this->inquiryFileImages->gets() as $inquiryFileImage) {
      $inquiryFileImages[] = $this->_transforms($inquiryFileImage, $this->inquiryFileImages);
    }

    if (count($inquiryFileImages)) {
      return $listener->response(200, array_merge([
        'inquiry_file_images' => $inquiryFileImages
      ], $this->inquiryFileImages->getPagination()));
    }
    return $listener->response(204, ['inquiry_file_images' => '']);
  }

  public function getInquiryInformationDetail(PipelineListener $listener, $data) {
    if (!$inquiryDetails = $this->inquiryFileImages->getDetailByBankAndReferenceId($data['bank'], $data['reference_id'])) {
      return $listener->response(204, ['inquiry_information_details' => '']);
    }

    $details = [];
    foreach ($inquiryDetails as $detail) {
      $details[] = $this->_transforms($detail, $this->inquiryFileImages);
    }

    $inquiryInformation = $this->inquiryInformation->getInquiryInformationFromBankInquiryId($data['reference_id'], $data['bank']);

    return $listener->response(200, [
      'inquiry_information_details' => $details,
      'description' => $inquiryInformation->description ?? '',
    ]);
  }
}
