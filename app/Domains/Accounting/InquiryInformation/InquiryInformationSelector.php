<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/12/17
 * Time: 11.51
 */

namespace Odeo\Domains\Accounting\InquiryInformation;

use Odeo\Domains\Constant\AwsConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class InquiryInformationSelector implements SelectorListener {

  private $inquiryInformations;

  public function __construct() {
    $this->inquiryInformations = app()->make(\Odeo\Domains\Accounting\InquiryInformation\Repository\BankInquiryAdditionalInformationRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $data['id'] = $item->id;
    $data['description'] = $item->description;
    $data['reference_id'] = $item->reference_id;
    $data['type'] = $item->type;
    $data['bank_reference'] = $item->bank_reference;

    if(isset($item->pdf_url)) {
      $data['pdf_url'] =  AwsConfig::S3_BASE_URL . '/' . $item->pdf_url;
    }
    return $data;
  }

  public function getInquiryInformations(PipelineListener $listener, $data) {
    $this->inquiryInformations->normalizeFilters($data);
    $this->inquiryInformations->setSimplePaginate(true);
    $inquiryInformationLists = [];

    foreach ($this->inquiryInformations->gets() as $info) {
      $inquiryInformationLists[] = $this->_transforms($info, $this->inquiryInformations);
    }

    if(count($inquiryInformationLists)) {
      return $listener->response(200, array_merge(
        ["inquiry_informations" => $this->_extends($inquiryInformationLists, $this->inquiryInformations)],
        $this->inquiryInformations->getPagination()
      ));
    }

    return $listener->response(204, ["inquiry_informations" => []]);
  }
}
