<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/12/17
 * Time: 11.50
 */

namespace Odeo\Domains\Accounting\InquiryInformation;

use Carbon\Carbon;
use Odeo\Domains\Constant\AwsConfig;
use Odeo\Domains\Core\PipelineListener;

class InquiryInformationUpdater {

  public function __construct() {
    $this->inquiryInformations = app()->make(\Odeo\Domains\Accounting\InquiryInformation\Repository\BankInquiryAdditionalInformationRepository::class);
    $this->inquiryInformationFileImages = app()->make(\Odeo\Domains\Accounting\InquiryInformation\Repository\BankInquiryAdditionalInformationFileImageRepository::class);
  }

  public function updateInquiryInformation(PipelineListener $listener, $data) {
    $now = Carbon::now();
    if (!$inquiryInformation = $this->inquiryInformations->getInquiryInformationFromBankInquiryId($data['inquiry_id'], $data['bank'])) {
      $inquiryInformation = $this->inquiryInformations->getNew();
      $inquiryInformation->reference_id = $data['inquiry_id'];
      $inquiryInformation->bank_reference = $data['bank'];
    }

    if(isset($data['description']) && $data['description'] != ''){
      $inquiryInformation->description = $data['description'];
    }

    if (isset($data['pdf_url']) && $data['pdf_url'] != '') {
      $inquiryInformation->pdf_url = $data['pdf_url'];
    }

    $this->inquiryInformationFileImages->save($inquiryInformation);
    if (isset($data['files'])) {
      $informationFileImages = [];
      foreach ($data['files'] as $file) {
        $informationFileImages[] = [
          'bank_inquiry_additional_information_id' => $inquiryInformation->id,
          'image_url' => $file['file_url'],
          'type' => $file['type'],
          'created_at' => $now,
          'updated_at' => $now
        ];
      }

      $this->inquiryInformationFileImages->saveBulk($informationFileImages);
    }

    $response = [
      'inquiry_information_id' => $inquiryInformation->id,
    ];
    if (isset($inquiryInformation->pdf_url)) {
      $response['pdf_url'] = AwsConfig::S3_BASE_URL . '/' . $inquiryInformation->pdf_url;
    }

    return $listener->response(200, $response);
  }

}