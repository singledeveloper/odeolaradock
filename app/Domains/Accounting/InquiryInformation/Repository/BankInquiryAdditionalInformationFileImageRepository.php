<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/12/17
 * Time: 11.53
 */

namespace Odeo\Domains\Accounting\InquiryInformation\Repository;

use Odeo\Domains\Accounting\InquiryInformation\Model\BankInquiryAdditionalInformationFileImage;
use Odeo\Domains\Core\Repository;

class BankInquiryAdditionalInformationFileImageRepository extends Repository {

  public function __construct(BankInquiryAdditionalInformationFileImage $bankInquiryAdditionalInformationFileImage) {
    $this->model = $bankInquiryAdditionalInformationFileImage;
  }

  public function gets() {
    $filters = $this->getFilters();
    $query = $this->model->join('bank_inquiry_additional_informations', 'bank_inquiry_additional_informations.id', '=', 'bank_inquiry_additional_information_file_images.bank_inquiry_additional_information_id');

    if (isset($filters['search'])) {
      if (isset($filters['search']['type'])) {
        $query->where('type', '=', $filters['search']['type']);
      }
      if (isset($filters['search']['information_id'])) {
        $query->where('bank_inquiry_additional_information_file_images.bank_inquiry_additional_information_id', '=', $filters['search']['information_id']);
      }
      if (isset($filters['search']['bank'])) {
        $query->where('bank_inquiry_additional_informations.bank_reference', '=', $filters['search']['bank']);
      }
      if (isset($filters['search']['reference_id'])) {
        $query->where('bank_inquiry_additional_informations.reference_id', '=', $filters['search']['reference_id']);
      }
    }

    $query->select(
      'bank_inquiry_additional_informations.reference_id',
      'bank_inquiry_additional_informations.bank_reference',

      'bank_inquiry_additional_information_file_images.type',
      'bank_inquiry_additional_information_file_images.image_url',
      'bank_inquiry_additional_information_file_images.id',
      'bank_inquiry_additional_information_file_images.bank_inquiry_additional_information_id'
    )
      ->orderBy('bank_inquiry_additional_information_file_images.id', 'desc');

    return $this->getResult($query);
  }

  public function getDetailByBankAndReferenceId($bank, $reffId) {
    return $this->model
      ->join('bank_inquiry_additional_informations', 'bank_inquiry_additional_informations.id', '=', 'bank_inquiry_additional_information_file_images.bank_inquiry_additional_information_id')
      ->where('bank_inquiry_additional_informations.bank_reference', '=', $bank)
      ->where('bank_inquiry_additional_informations.reference_id', '=', $reffId)
      ->select('bank_inquiry_additional_information_file_images.*')
      ->orderBy('id', 'desc')
      ->get();
  }

}