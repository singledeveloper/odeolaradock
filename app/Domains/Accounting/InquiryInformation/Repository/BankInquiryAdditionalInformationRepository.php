<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/12/17
 * Time: 11.52
 */

namespace Odeo\Domains\Accounting\InquiryInformation\Repository;

use Odeo\Domains\Accounting\InquiryInformation\Model\BankInquiryAdditionalInformation;
use Odeo\Domains\Core\Repository;

class BankInquiryAdditionalInformationRepository extends Repository {

  public function __construct(BankInquiryAdditionalInformation $bankInquiryAdditionalInformation) {
    $this->model = $bankInquiryAdditionalInformation;
  }

  public function getInquiryInformationFromBankInquiryId($reffId, $bank) {
    return $this->model
      ->where('reference_id', '=', $reffId)
      ->where('bank_reference', '=', $bank)
      ->first();
  }

  public function gets() {
    $filters = $this->getFilters();
    $query = $this->model;

    if (isset($filters['search'])) {
      if ($filters['search']['bank']) {
        $query->where('bank_reference', $filters['search']['bank']);
      }
      if ($filters['search']['type']) {
        $query->where('type', $filters['search']['type']);
      }
      if ($filters['search']['reference_id']) {
        $query->where('reference_id', $filters['search']['reference_id']);
      }
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

}