<?php

namespace Odeo\Domains\Accounting\Jobs;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Account\Repository\UserGroupRepository;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Jobs\Job;

class GenerateAccountingReport extends Job  {

  public function handle() {
    $accountingReportRepo = app()->make(\Odeo\Domains\Accounting\Repository\AccountingReportRepository::class);

    $date = Carbon::today()->subMonth();
    $startDate = $date->copy()->startOfMonth()->format('Y-m-d');
    $endDate = $date->copy()->endOfMonth()->format('Y-m-d');

    $accountingReport = $accountingReportRepo->getNew();
    $accountingReport->period = $date->format('Y-m');

    $accountingReport->data = json_encode([
      'subscriptions' => $this->getSubscriptionReport($startDate, $endDate),
      'disbursements' => $this->getDisbursementReport($startDate, $endDate),
      'disbursement_inquiries' => $this->getDisbursementInquiryReport($startDate, $endDate),
      'withdraws' => $this->getWithdrawReport($startDate, $endDate),
      'bill_payments' => $this->getBillPaymentReport($startDate, $endDate),
      'payment_gateways' => $this->getPaymentGatewayReport($startDate, $endDate),
      'ppobs' => $this->getPPOBReport($startDate, $endDate),
      'marketing_expenses' => $this->getMarketingExpenseReport($startDate, $endDate),
      'biller_balances' => $this->getBillerBalanceReport($startDate, $endDate),
      'cashes' => $this->getCashReport($startDate, $endDate)
    ]);

    $accountingReportRepo->save($accountingReport);
  }

  private function getSubscriptionReport($startDate, $endDate) {
    $query = "select 
      to_char(o.closed_at, 'YYYY-MM') as period,
      count(od.id)                    as total_count,
      sum(od.base_price)              as total_order
    from order_details od
    left join orders o on o.id = od.order_id
    where 
      date(o.closed_at) >= :start_date 
      and date(o.closed_at) <= :end_date
      and o.status = :status
      and od.service_detail_id = :service_detail_id
    group by period
    order by period";

    return $this->processQuery($query, [
      'start_date' => $startDate,
      'end_date' => $endDate,
      'status' => OrderStatus::COMPLETED,
      'service_detail_id' => ServiceDetail::PLAN_ODEO
    ]);
  }

  private function getDisbursementReport($startDate, $endDate) {
    $query = "select 
      to_char(verified_at, 'YYYY-MM')               as period, 
      split_part(auto_disbursement_vendor, '_', 1)  as vendor, 
      count(*)                                      as count, 
      sum(amount)                                   as amount, 
      sum(fee)                                      as fee, 
      sum(cost)                                     as cost, 
      sum(fee - cost)                               as profit
    from api_disbursements
    where 
      status = :status 
      and date(verified_at) >= :start_date 
      and date(verified_at) <= :end_date
    group by period, auto_disbursement_vendor";

    return $this->processQuery($query, [
      'status' => ApiDisbursement::COMPLETED,
      'start_date' => $startDate,
      'end_date' => $endDate,
    ]);
  }

  private function getDisbursementInquiryReport($startDate, $endDate) {
    $statuses = implode(', ', array_map(function ($s) {
      return "'$s'";
    }, ApiDisbursement::NON_FREE_INQUIRY_STATUS));

    $query = "select 
      to_char(created_at, 'YYYY-MM')  as period, 
      count(id)                       as count, 
      sum(fee)                        as fee, 
      sum(cost)                       as cost, 
      sum(fee - cost)                 as profit
    from api_disbursement_inquiries
    where 
      status in ($statuses) 
      and date(created_at) >= :start_date 
      and date(created_at) <= :end_date 
      and fee > 0
    group by period
    order by period";
    
    return $this->processQuery($query, [
      'start_date' => $startDate,
      'end_date' => $endDate,
    ]);
  }

  private function getWithdrawReport($startDate, $endDate) {
    $disbursementApiUsers = app()->make(\Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository::class);
    $h2hManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper\PulsaH2HManager::class);
    $userGroupRepo = app()->make(UserGroupRepository::class);
  
    $withdrawB2BUserIdList = array_merge(
      $h2hManager->getUserH2H(),
      $userGroupRepo->getUserIds([
        UserType::GROUP_BYPASS_WITHDRAW_LIMIT,
        UserType::GROUP_BYPASS_CASH_LIMIT
      ]),
      array_keys(\Odeo\Domains\Transaction\Helper\CashLimiter::THROTTLED_USER),
      $disbursementApiUsers->getAll()->pluck('user_id')->toArray()
    );
  
    $userIds = implode(',', $withdrawB2BUserIdList);

    $query = "select 
      case when user_id in ($userIds) then 'B2B' else 'B2C' end as type, 
      to_char(verified_at, 'YYYY-MM')                           as period, 
      count(*)                                                  as count, 
      sum(amount)                                               as amount, 
      sum(fee)                                                  as fee, 
      sum(cost)                                                 as cost
    from user_withdraws
    where 
      status = :status 
      and date(verified_at) >= :start_date 
      and date(verified_at) <= :end_date
    group by period, type
    order by period, type";
  
    return $this->processQuery($query, [
      'status' => Withdraw::COMPLETED,
      'start_date' => $startDate,
      'end_date' => $endDate
    ]);
  }

  private function getBillPaymentReport($startDate, $endDate) {
    $query = "select 
      to_char(paid_at, 'YYYY-MM')                 as period, 
      uib.biller_name                             as biller_name, 
      case when mdr > 0 then 'EDC' else 'VA' end  as channel,
      count(*)                                    as count, 
      sum(total_paid)                             as total, 
      sum(biller_fee + mdr)                       as fee, 
      sum(payment_gateway_cost)                   as cost 
    from affiliate_user_invoices aui
    join user_invoice_billers uib ON uib.id = aui.biller_id
    where 
      status = :status 
      and date(paid_at) >= :start_date 
      and date(paid_at) <= :end_date
    group by 1, biller_name, channel
    order by period";
  
    return $this->processQuery($query, [
      'status' => UserInvoice::COMPLETED,
      'start_date' => $startDate,
      'end_date' => $endDate
    ]);
  }

  private function getPaymentGatewayReport($startDate, $endDate) {
    $query = "select 
      to_char(pgp.created_at, 'YYYY-MM')                                      as period, 
      poci.actual_name                                                        as vendor_name, 
      count(*)                                                                as count, 
      sum(pgp.amount)                                                         as total, 
      sum(pgp.fee + pgp.settlement_fee)                                       as fee, 
      sum(pgp.cost)                                                           as cost
    from payment_gateway_payments pgp
    join payment_odeo_payment_channel_informations poci ON poci.id = pgp.payment_group_id
    where 
      pgp.status = :status 
      and date(pgp.created_at) >= :start_date 
      and date(pgp.created_at) <= :end_date
    group by 1, vendor_name
    order by period";

    return $this->processQuery($query, [
      'status' => UserInvoice::COMPLETED,
      'start_date' => $startDate,
      'end_date' => $endDate
    ]);
  }

  private function getPPOBReport($startDate, $endDate) {
    $h2hPlatforms = implode(', ', Platform::getH2HPlatforms());
    $query = "
      SELECT
        CASE
        WHEN owner_store_id IS NOT NULL
          THEN
            'ppobC2C'
        WHEN platform_id IN ({$h2hPlatforms})
          THEN
            'ppobB2B'
        ELSE
            'ppobB2C'
        END                                                                                       AS type,
        services.displayed_name                                                                   AS service,
        to_char(requested_at, 'YYYY-MM')                                                          AS period,
        sum(order_details.sale_price)                                                             AS gmv,
        sum(order_details.base_price)                                                             AS revenue,
        sum(order_details.base_price - (first_base_price - current_base_price - subsidy_amount))  AS cogs,
        count(order_detail_pulsa_switchers.id)                                                    AS trx
      FROM order_detail_pulsa_switchers
        LEFT JOIN order_details ON order_detail_pulsa_switchers.order_detail_id = order_details.id
        LEFT JOIN orders ON order_details.order_id = orders.id
        LEFT JOIN pulsa_odeo_inventories ON pulsa_odeo_inventories.id = order_detail_pulsa_switchers.current_inventory_id
        LEFT JOIN pulsa_odeos ON pulsa_odeos.id = pulsa_odeo_inventories.pulsa_odeo_id
        LEFT JOIN service_details ON service_details.id = order_details.service_detail_id
        LEFT JOIN services ON services.id = service_details.service_id
      WHERE requested_at IS NOT NULL AND order_detail_pulsa_switchers.status = :status
        AND requested_at :: DATE >= :start_date AND requested_at :: DATE <= :end_date
      GROUP BY type, service, period
      ORDER BY period, type, service";
  
    return $this->processQuery($query, [
      'status' => SwitcherConfig::SWITCHER_COMPLETED,
      'start_date' => $startDate,
      'end_date' => $endDate
    ]);
  }

  private function getMarketingExpenseReport($startDate, $endDate) {
    $query = "select 
      to_char(transaction_date, 'YYYY-MM')  as period, 
      sum(total_marketing)                  as rush
    from order_reconciliations 
    where 
      date(transaction_date) >= :start_date 
      and date(transaction_date) <= :end_date
    group by period";
  
    return $this->processQuery($query, [
      'start_date' => $startDate,
      'end_date' => $endDate
    ]);
  }

  private function getBillerBalanceReport($startDate, $endDate) {
    $query = "select 
      'Artajasa Disbursement' as name,
      temp.period,
      temp.balance_after      as balance
    from 
      (select 
        balance_after,
        to_char(created_at, 'YYYY-MM')                                                            as period, 
        row_number() over (partition by to_char(created_at, 'YYYY-MM') order by created_at desc)  as rn 
      from disbursement_artajasa_mutations
      where date(created_at) >= :start_date and date(created_at) <= :end_date
      order by created_at desc) temp
    where temp.rn = 1
    order by period";
  
    $artajasaBalances = $this->processQuery($query, [
      'start_date' => $startDate,
      'end_date' => $endDate
    ]);
  
    $query = "select
      temp.vendor_switcher_id,
      vs.name,
      temp.period,
      temp.last_balance_recorded_from_biller as balance
    from 
      (select 
        vendor_switcher_id, 
        last_balance_recorded_from_biller, 
        to_char(created_at, 'YYYY-MM')                                                                                as period, 
        row_number() over (partition by to_char(created_at, 'YYYY-MM'), vendor_switcher_id order by created_at desc)  as rn 
      from vendor_switcher_reconciliation_mutations 
      where date(created_at) >= :start_date and date(created_at) <= :end_date 
      order by created_at desc) temp 
    left join vendor_switchers vs on vs.id = temp.vendor_switcher_id 
    where 
      temp.rn = 1 
      and vs.owner_store_id is null
    order by vendor_switcher_id, period";
  
    $billerBalances = $this->processQuery($query, [
      'start_date' => $startDate,
      'end_date' => $endDate
    ]);

    return array_merge($artajasaBalances, $billerBalances);
  }

  private function getCashReport($startDate, $endDate) {
    $query = "select 
      case when cash_type = 'cash' then 'oCash' else 'oDeposit' end as cash_type,
      to_char(date, 'YYYY-MM')                                      as period,
      sum
    from user_cash_snapshots 
    where 
      (cash_type = 'cash' or cash_type = 'deposit')
      and date = :date";
  
    return $this->processQuery($query, [
      'date' => $endDate,
    ]);
  }

  private function processQuery($query, $bindings) {
    $db = \DB::connection()->getPdo();

    $result = $db->prepare($query);
    $result->execute($bindings);
    
    $res = [];
    while ($temp = $result->fetch(\PDO::FETCH_ASSOC)) {
      $res[] = $temp;
    }

    return $res;
  }

}
