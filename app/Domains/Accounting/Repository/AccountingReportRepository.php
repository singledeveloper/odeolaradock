<?php

namespace Odeo\Domains\Accounting\Repository;

use Carbon\Carbon;
use Odeo\Domains\Accounting\Model\AccountingReport;
use Odeo\Domains\Core\Repository;

class AccountingReportRepository extends Repository {

  public function __construct(AccountingReport $accountingReport) {
    $this->model = $accountingReport;
  }

}