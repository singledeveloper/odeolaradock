<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 02/03/18
 * Time: 16.42
 */

namespace Odeo\Domains\Accounting\Xero\Helper;


class JournalHelper {

  const ACCOUNT_TYPE_BANK = 'BANK';
  const ACCOUNT_TYPE_CURRENT_ASSET = 'CURRENT';
  const ACCOUNT_TYPE_CURRENT_LIABILITY = 'CURRLIAB';
  const ACCOUNT_TYPE_DEPRECIATION = 'DEPRECIATN';
  const ACCOUNT_TYPE_DIRECT_COSTS = 'DIRECTCOSTS';
  const ACCOUNT_TYPE_EQUITY = 'EQUITY';
  const ACCOUNT_TYPE_EXPENSE = 'EXPENSE';
  const ACCOUNT_TYPE_FIXED_ASSET = 'FIXED';
  const ACCOUNT_TYPE_INVENTORY = 'INVENTORY';
  const ACCOUNT_TYPE_LIABILITY = 'LIABILITY';
  const ACCOUNT_TYPE_NONCURRENT_ASSET = 'NONCURRENT';
  const ACCOUNT_TYPE_NONCURRENT_LIABILITY = 'TERMLIAB';
  const ACCOUNT_TYPE_OTHER_INCOME = 'OTHERINCOME';
  const ACCOUNT_TYPE_OVERHEAD_ACCOUNT = 'OVERHEADS';
  const ACCOUNT_TYPE_PAYG_LIABILITY = 'PAYGLIABILITY';
  const ACCOUNT_TYPE_PREPAYMENT = 'PREPAYMENT';
  const ACCOUNT_TYPE_REVENUE = 'REVENUE';
  const ACCOUNT_TYPE_SALES = 'SALES';
  const ACCOUNT_TYPE_WAGES_EXPENSE = 'WAGESEXPENSE';

  const NARRATION_APP_RATE_BONUS = 'App Rate Bonus';
  const NARRATION_COMPANY_PAYMENT = 'Payment';
  const NARRATION_COMPANY_TRANSFER = 'Company Transfer';
  const NARRATION_COMPOUND_INTEREST = 'Compound Interest';
  const NARRATION_HUSTLER_SALES_COMMISSION = 'Hustler Sales Commission';
  const NARRATION_MENTOR_HUSTLER_ADS_CLICK = 'Mentor/Hustler ads click';
  const NARRATION_NEW_CUSTOMER_CASHBACK = 'New Customer Cashback';
  const NARRATION_NEW_USER_BONUS = 'New User Bonus';
  const NARRATION_OPAY_TRANSACTION = 'oPay Transaction';

  const NARRATION_ORDER_PLAN_AR = 'Server Plan Purchase by AR Group';
  const NARRATION_ORDER_PLAN_BANK_TRANSFER = 'Server Plan Purchase by Bank Transfer';
  const NARRATION_ORDER_PLAN_CC = 'Server Plan Purchase by Cc';
  const NARRATION_ORDER_PLAN_OCASH = 'Server Plan Purchase by oCash';
  const NARRATION_ORDER_PLAN_POSTPAID = 'Server Plan Purchase (Postpaid)';
  const NARRATION_ORDER_PLAN_SPONSOR_BONUS = 'Server Plan Sponsor Bonus';
  const NARRATION_ORDER_PULSA_AR_GROUP = 'Order Pulsa by AR Group';
  const NARRATION_ORDER_PULSA_BANK_TRANSFER = 'Order Pulsa by Bank Transfer';
  const NARRATION_ORDER_PULSA_CASH_ON_DELIVERY = 'Order Pulsa by CoD';
  const NARRATION_ORDER_PULSA_CC = 'Order Pulsa by Cc';
  const NARRATION_ORDER_PULSA_OCASH = 'Order Pulsa by oCash';

  const NARRATION_ORDER_BILL_PAYMENT_AR = 'Order Bill Payment by AR Group';
  const NARRATION_ORDER_BILL_PAYMENT_BANK_TRANSFER = 'Order Bill Payment by Bank Transfer';
  const NARRATION_ORDER_BILL_PAYMENT_CC = 'Order Bill Payment by Cc';
  const NARRATION_ORDER_BILL_PAYMENT_OCASH = 'Order Bill Payment by oCash';

  const NARRATION_PRESET_CASHBACK = 'Preset Cashback';
  const NARRATION_PROMO_CASHBACK = 'Promo Cashback';
  const NARRATION_REFERRAL_CLAIM = 'Referral Claim';
  const NARRATION_REFERRAL_CODE_REDEEM = 'Referral Code Redeem';
  const NARRATION_RETURN_DEPOSIT = 'Return Deposit';
  const NARRATION_RUSH_BONUS = 'Rush Bonus';
  const NARRATION_RUSH_BONUS_OCASH = 'Rush bonus (oCash)';
  const NARRATION_RUSH_BONUS_ODEPOSIT = 'Rush bonus (oDeposit)';
  const NARRATION_SHARE_REFERRAL_CODE = 'Share Referral Code';
  const NARRATION_TEAM_COMMISSION = 'Team Commission';
  const NARRATION_OVER_SETTLEMENT_REFUND = 'Over Settlement Refund';
  const NARRATION_FAULT_ORDER_REFUND = 'Fault Order Refund';

  const NARRATION_TOPUP_OCASH_AR_GROUP = 'Topup oCash by AR group';
  const NARRATION_TOPUP_OCASH_BANK_TRANSFER = 'Topup oCash by Bank Transfer';
  const NARRATION_TOPUP_ODEPOSIT = 'Topup oDeposit';
  const NARRATION_TOPUP_ODEPOSIT_AR_GROUP = 'Order Topup oDeposit by AR Group';
  const NARRATION_TOPUP_ODEPOSIT_BANK_TRANSFER = 'Order Topup oDeposit by Bank Transfer';
  const NARRATION_TOPUP_ODEPOSIT_CC = 'Order Topup oDeposit by Cc';
  const NARRATION_TOPUP_ODEPOSIT_OCASH = 'Order Topup oDeposit by oCash';
  const NARRATION_TOPUP_TOPUP_BILLER = 'Topup Biller';
  const NARRATION_TRANSFER_CASHBACK = 'Transfer Cashback';
  const NARRATION_WEEKDAYS_RESTOCK_CASHBACK = 'Weekdays Restock Cashback';
  const NARRATION_AGENT_BONUS_REQUEST = 'Agent Bonus Request';

  const NARRATION_WITHDRAW_ARTAJASA = 'Withdraw Artajasa';
  const NARRATION_WITHDRAW_BANK_BCA = 'Withdraw BCA';
  const NARRATION_WITHDRAW_BANK_CLEARING = 'Withdraw Bank Clearing';
  const NARRATION_WITHDRAW_FLIP = 'Withdraw Flip';
  const NARRATION_WITHDRAW_SUSPECT = 'Withdraw (SUSPECT)';

  const NARRATION_SUMMARY_VENDOR_SWITCHER_RECON_MUTATION = 'Biller Mutation Adjustment Summary';

  const PAYMENT_BANK_TRANSFER_BCA = 'Bank Transfer BCA';
  const PAYMENT_BANK_TRANSFER_MANDIRI = 'Bank Transfer Mandiri';
  const PAYMENT_BANK_TRANSFER_BRI = 'Bank Transfer BRI';
  const PAYMENT_CASH_ON_DELIVERY = 'Cash on Delivery';
  const PAYMENT_CIMB_CLICK = 'Cimb Clicks';
  const PAYMENT_CC = 'Credit Card';
  const PAYMENT_CC_INSTALLMENT = 'Cicilan Kartu Kredit';
  const PAYMENT_CC_TOKENIZATION = 'Credit Card Tokenization';
  const PAYMENT_AR_ALFA_GROUP = 'Alfa Group';
  const PAYMENT_AR_DOKU_WALLET = 'Doku Wallet';
  const PAYMENT_AR_KREDIVO = 'Kredivo';
  const PAYMENT_AR_MANDIRI_E_CASH = 'Mandiri E Cash';
  const PAYMENT_AR_BANK_TRANSFER_VA = 'ATM Transfer (Virtual Account)';
  const PAYMENT_AR_BRI_E_PAY = 'BRI Epay';
  const PAYMENT_AR_AKULAKU = 'Akulaku Pay';
  const PAYMENT_OCASH = 'Odeo Cash';

  const CHARGE_TYPE_PUNDI_FEE = 'pundi_fee';
  const CHARGE_TYPE_DOKU_FEE = 'doku_fee';
  const CHARGE_TYPE_SERVICE_COST = 'payment_service_cost';
  const CHARGE_GROUP_TYPE_COMPANY_PROFIT = 'company_profit';
  const CHARGE_GROUP_TYPE_CHARGE_TO_COMPANY = 'charge_to_company';

  const STATUS_FAIL = '90000';
  const STATUS_PROCESSING = '20000';
  const STATUS_SUCCESS = '50000';
  const STATUS_SUCCESS_WITH_WARNING = '50001';
  const STATUS_WAITING_UPDATE = '10000';

  const TYPE_ADS_CLICK = 'ads_click';
  const TYPE_APP_RATE_BONUS = 'app_rate_bonus';
  const TYPE_COMPANY_PAYMENT = 'company_payment';
  const TYPE_COMPANY_TRANSFER = 'company_transfer';
  const TYPE_COMPOUND_INTEREST = 'compound_interest';
  const TYPE_HUSTLER_SALES_COMMISSION = 'hustler_sales_commission';
  const TYPE_NEW_CUSTOMER_CASHBACK = 'new_customer_cashback';
  const TYPE_NEW_USER_BONUS = 'new_user_bonus';
  const TYPE_OPAY_TRANSACTION = 'opay_transaction';
  const TYPE_ORDER_BILL = 'bill_order';
  const TYPE_ORDER_PLAN = 'plan_order';
  const TYPE_ORDER_PULSA = 'pulsa_order';
  const TYPE_ORDER_PULSA_AR = 'pulsa_order_ar';
  const TYPE_ORDER_PULSA_BANK_TRANSFER = 'pulsa_order_bank_trf';
  const TYPE_ORDER_PULSA_CC = 'pulsa_order_cc';
  const TYPE_ORDER_PULSA_COD = 'pulsa_order_cod';
  const TYPE_ORDER_PULSA_OCASH = 'pulsa_order_ocash';
  const TYPE_PRESET_CASHBACK = 'preset_cashback';
  const TYPE_PROMO_CASHBACK = 'promo_cashback';
  const TYPE_REFERRAL_CLAIM = 'referral_claim';
  const TYPE_REFERRAL_CODE_REDEEM = 'referral_code_redeem';
  const TYPE_RESTOCK_CASHBACK = 'restock_cashback';
  const TYPE_RETURN_DEPOSIT = 'return_deposit';
  const TYPE_RUSH_BONUS = 'rush_bonus';
  const TYPE_RUSH_BONUS_OCASH = 'rush_bonus_ocash';
  const TYPE_RUSH_BONUS_ODEPOSIT = 'rush_bonus_odeposit';
  const TYPE_SHARE_REFERRAL_CODE = 'share_referral_code';
  const TYPE_SPONSOR_BONUS = 'sponsor_bonus';
  const TYPE_TEAM_COMMISSION = 'team_commission';
  const TYPE_TOPUP_BILLER = 'biller_topup';
  const TYPE_TOPUP_OCASH = 'ocash_topup';
  const TYPE_TOPUP_ODEPOSIT = 'odeposit_topup';
  const TYPE_TRANSFER_CASHBACK = 'transfer_cashback';
  const TYPE_VOID_ABUSE_DETECTED = 'void_abuse_detected';
  const TYPE_WITHDRAW = 'withdraws';
  const TYPE_WITHDRAW_ARTAJASA = 'withdraw_aj';
  const TYPE_WITHDRAW_BCA = 'withdraw_bca';
  const TYPE_WITHDRAW_CLEARING = 'withdraw_clearing';
  const TYPE_WITHDRAW_FLIP = 'withdraw_flip';
  const TYPE_WITHDRAW_SUSPECT = 'withdraw_suspect';
  const TYPE_AGENT_BONUS_REQUEST = 'agent_bonus_request';
  const TYPE_OVER_SETTLEMENT_REFUND = 'over_settlement_refund';
  const TYPE_FAULT_ORDER_REFUND = 'fault_order_refund';
  const TYPE_B2B_SALES_ORDER = 'b2b_sales_order';

  const WITHDRAW_VENDOR_ARTAJASA = 'artajasa_api_disbursement';
  const WITHDRAW_VENDOR_BCA = 'bca_api_disbursement';
  const WITHDRAW_VENDOR_CLEARING = 'bank_clearing';
  const WITHDRAW_VENDOR_FLIP = 'flip_api_disbursement';

  public static function isBankTransfer($paymentName) {
    return in_array($paymentName, [
      self::PAYMENT_BANK_TRANSFER_BCA,
      self::PAYMENT_BANK_TRANSFER_MANDIRI,
      self::PAYMENT_BANK_TRANSFER_BRI
    ]);
  }

  public static function isArGroup($paymentName) {
    return in_array($paymentName, [
      self::PAYMENT_AR_KREDIVO,
      self::PAYMENT_AR_DOKU_WALLET,
      self::PAYMENT_AR_BANK_TRANSFER_VA,
      self::PAYMENT_AR_ALFA_GROUP,
      self::PAYMENT_AR_MANDIRI_E_CASH,
      self::PAYMENT_AR_BRI_E_PAY,
      self::PAYMENT_AR_AKULAKU
    ]);
  }

  public static function isOcash($paymentName) {
    return $paymentName == self::PAYMENT_OCASH;
  }

  public static function isCoD($paymentName) {
    return $paymentName == self::PAYMENT_CASH_ON_DELIVERY;
  }

  public static function isKredivo($paymentName) {
    return $paymentName == self::PAYMENT_AR_KREDIVO;
  }

  public static function isCc($paymentName) {
    return in_array($paymentName, [
      self::PAYMENT_CC,
      self::PAYMENT_CC_TOKENIZATION,
      self::PAYMENT_CC_INSTALLMENT
    ]);
  }

  public static function getNarrationByJournalType($type) {
    switch ($type) {
      case self::TYPE_REFERRAL_CLAIM:
        return self::NARRATION_REFERRAL_CLAIM;
      case self::TYPE_APP_RATE_BONUS:
        return self::NARRATION_APP_RATE_BONUS;
      case self::TYPE_RESTOCK_CASHBACK:
        return self::NARRATION_WEEKDAYS_RESTOCK_CASHBACK;
      case self::TYPE_PRESET_CASHBACK:
        return self::NARRATION_PRESET_CASHBACK;
      case self::TYPE_PROMO_CASHBACK:
        return self::NARRATION_PROMO_CASHBACK;
      case self::TYPE_OVER_SETTLEMENT_REFUND:
        return self::NARRATION_OVER_SETTLEMENT_REFUND;
      default:
        return '-';
    }
  }

  public static function getPpobNarrationByPayment($paymentName) {
    switch ($paymentName) {
      case self::PAYMENT_BANK_TRANSFER_MANDIRI:
      case self::PAYMENT_BANK_TRANSFER_BCA:
        return self::NARRATION_ORDER_PULSA_BANK_TRANSFER;
      case self::PAYMENT_CC:
      case self::PAYMENT_CC_INSTALLMENT:
      case self::PAYMENT_CC_TOKENIZATION:
        return self::NARRATION_ORDER_PULSA_CC;
      case self::PAYMENT_CASH_ON_DELIVERY:
        return self::NARRATION_ORDER_PULSA_CASH_ON_DELIVERY;
      case self::PAYMENT_OCASH:
        return self::NARRATION_ORDER_PULSA_OCASH;
      case self::PAYMENT_AR_KREDIVO:
      case self::PAYMENT_AR_DOKU_WALLET:
      case self::PAYMENT_AR_MANDIRI_E_CASH:
      case self::PAYMENT_AR_BANK_TRANSFER_VA:
      case self::PAYMENT_AR_AKULAKU:
        return self::NARRATION_ORDER_PULSA_AR_GROUP;
      default:
        return 'Order Pulsa (unknown payment)';
    }
  }

  public static function getPlanOrderNarrationByPayment($paymentName) {
    switch ($paymentName) {
      case self::PAYMENT_BANK_TRANSFER_MANDIRI:
      case self::PAYMENT_BANK_TRANSFER_BCA:
        return self::NARRATION_ORDER_PLAN_BANK_TRANSFER;
      case self::PAYMENT_CC:
      case self::PAYMENT_CC_INSTALLMENT:
      case self::PAYMENT_CC_TOKENIZATION:
        return self::NARRATION_ORDER_PLAN_CC;
      case self::PAYMENT_CASH_ON_DELIVERY:
        return self::NARRATION_ORDER_PLAN_POSTPAID;
      case self::PAYMENT_OCASH:
        return self::NARRATION_ORDER_PLAN_OCASH;
      case self::PAYMENT_AR_KREDIVO:
      case self::PAYMENT_AR_DOKU_WALLET:
      case self::PAYMENT_AR_MANDIRI_E_CASH:
        return self::NARRATION_ORDER_PLAN_AR;
      default:
        return 'Server Plan Purchase (unknown payment)';
    }
  }

  public static function getBillPaymentNarrationByPayment($paymentName) {
    switch ($paymentName) {
      case self::PAYMENT_BANK_TRANSFER_MANDIRI:
      case self::PAYMENT_BANK_TRANSFER_BCA:
        return self::NARRATION_ORDER_BILL_PAYMENT_BANK_TRANSFER;
      case self::PAYMENT_CC:
      case self::PAYMENT_CC_INSTALLMENT:
      case self::PAYMENT_CC_TOKENIZATION:
        return self::NARRATION_ORDER_BILL_PAYMENT_CC;
      case self::PAYMENT_OCASH:
        return self::NARRATION_ORDER_BILL_PAYMENT_OCASH;
      case self::PAYMENT_AR_KREDIVO:
      case self::PAYMENT_AR_DOKU_WALLET:
      case self::PAYMENT_AR_MANDIRI_E_CASH:
      case self::PAYMENT_AR_BANK_TRANSFER_VA:
        return self::NARRATION_ORDER_BILL_PAYMENT_AR;
      default:
        return 'Order Bill Payment (unknown payment)';
    }
  }

  public static function getTopupOcashNarrationByPayment($paymentName) {
    switch ($paymentName) {
      case self::PAYMENT_BANK_TRANSFER_BCA:
      case self::PAYMENT_BANK_TRANSFER_MANDIRI:
        return self::NARRATION_TOPUP_OCASH_BANK_TRANSFER;
      case self::PAYMENT_AR_BANK_TRANSFER_VA:
      case self::PAYMENT_AR_KREDIVO:
      case self::PAYMENT_AR_MANDIRI_E_CASH:
      case self::PAYMENT_AR_DOKU_WALLET:
      case self::PAYMENT_AR_ALFA_GROUP:
        return self::NARRATION_TOPUP_OCASH_AR_GROUP;
      default:
        return 'Topup oCash (unknown payment)';
    }
  }

  public static function getTopupOdepositNarrationByPayment($paymentName) {
    switch ($paymentName) {
      case self::PAYMENT_BANK_TRANSFER_MANDIRI:
      case self::PAYMENT_BANK_TRANSFER_BCA:
        return self::NARRATION_TOPUP_ODEPOSIT_BANK_TRANSFER;
      case self::PAYMENT_CC:
      case self::PAYMENT_CC_INSTALLMENT:
      case self::PAYMENT_CC_TOKENIZATION:
        return self::NARRATION_TOPUP_ODEPOSIT_CC;
      case self::PAYMENT_OCASH:
        return self::NARRATION_TOPUP_ODEPOSIT_OCASH;
      case self::PAYMENT_AR_KREDIVO:
      case self::PAYMENT_AR_DOKU_WALLET:
      case self::PAYMENT_AR_MANDIRI_E_CASH:
      case self::PAYMENT_AR_ALFA_GROUP:
        return self::NARRATION_TOPUP_ODEPOSIT_AR_GROUP;
      default:
        return 'Order Topup Odeposit (unknown payment)';
    }
  }

}