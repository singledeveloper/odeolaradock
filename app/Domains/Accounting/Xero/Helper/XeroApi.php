<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/3/17
 * Time: 12:45 PM
 */

namespace Odeo\Domains\Accounting\Xero\Helper;


use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

class XeroApi {

  const CERT_FILE = 'xero-privatekey.pem';
//  const CONSUMER_KEY = 'HCOWJNVSCROWMJYKUTFCFUBRYUVJSI'; // febrianj.dev
//  const CONSUMER_SECRET = 'GNXOYEML18KFWWGV2TUVIBSWPPGJNZ'; // febrianj.dev
  const CONSUMER_KEY = '6D96VQR5LS21T64WIMVBBYYUTF6MGV'; // febrianjdev dev app
  const CONSUMER_SECRET = 'EP9FQK65STMUDB5BPARW1QWAABMH5N'; // febrianjdev dev app
  const CONSUMER_KEY_ODEO = 'VV7JR1VCIGRVFZOJVXCF1VMPGRE5LO';
  const CONSUMER_SECRET_ODEO = 'OF2IVOE8JPPEJZXOHJKDYW9APODE3Z';
  const MANUAL_JOURNAL_URL = 'https://api.xero.com/api.xro/2.0/manualjournals';

  public static function getClient() {
    $stack = HandlerStack::create();
    $middleware = new Oauth1([
//      'consumer_key' => self::CONSUMER_KEY,
//      'token' => self::CONSUMER_KEY,
      'consumer_key' => self::CONSUMER_KEY_ODEO,
      'token' => self::CONSUMER_KEY_ODEO,
      'token_secret' => self::CONSUMER_SECRET,
      'signature_method' => Oauth1::SIGNATURE_METHOD_RSA,
      'private_key_file' => __DIR__ . '/../certs/' . self::CERT_FILE,
      'private_key_passphrase' => ''
    ]);
    $stack->push($middleware);

    $client = new Client([
      'handler' => $stack,
      'auth' => 'oauth',
      'headers' => [
        'Accept' => 'application/json',
      ]
    ]);

    return $client;
  }

  public static function getNewAccountCodeXml($code, $name, $type) {
    return '<Account><Code>' . $code . '</Code><Name>' . $name . '</Name><Type>' . $type . '</Type><TaxType>NONE</TaxType></Account>';
  }

  public static function getJournalLineXml($data, $description) {
    $xml = '';
    foreach ($data as $key => $value) {
      if (is_array($value)) {
        foreach ($value as $val) {
          if (isset($val) && $val != '0' && $val != '-0') {
            $xml .= '<JournalLine>' . PHP_EOL;
            $xml .= '<Description>' . $description . '</Description>' . PHP_EOL;
            $xml .= '<LineAmount>' . $val . '</LineAmount><AccountCode>' . $key . '</AccountCode>' . PHP_EOL;
            $xml .= '</JournalLine>' . PHP_EOL;
          }
        }
      } else if (isset($value) && $value != '0' && $value != '-0') {
        $xml .= '<JournalLine>' . PHP_EOL;
        $xml .= '<Description>' . $description . '</Description>' . PHP_EOL;
        $xml .= '<LineAmount>' . $value . '</LineAmount><AccountCode>' . $key . '</AccountCode>' . PHP_EOL;
        $xml .= '</JournalLine>' . PHP_EOL;
      }
    }
    return $xml;
  }

  public static function getManualJournalXml($journalLineXmls, $narration, $date) {
    $date = Carbon::createFromFormat('Y-m-d G:i:s', $date)->toAtomString();
    $xml = '<ManualJournal><Date>' . $date . '</Date><Narration>' . $narration . '</Narration><JournalLines>';

    foreach ($journalLineXmls as $journalLineXml) {
      $xml .= $journalLineXml;
    }

    $xml .= '</JournalLines></ManualJournal>';
    return $xml;
  }

}