<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 02/04/18
 * Time: 14.43
 */

namespace Odeo\Domains\Accounting\Xero\Helper;


use Carbon\Carbon;

class XeroJsonFormatter {

  public static function wrapManualJournals($manualJournals) {
    return ['ManualJournals' => $manualJournals];
  }

  public static function generateManualJournal($date, $narration, $journalLines) {
    return [
      'Date' => Carbon::parse($date)->format('Y-m-d'),
      'Narration' => $narration,
      'Status' => 'DRAFT',
      'JournalLines' => $journalLines
    ];
  }

  public static function generateJournalLine($accountCode, $lineAmount) {
    $journalLines = [];

    if (is_array($lineAmount)) {
      foreach ($lineAmount as $amount) {
        if ($amount != '0' && $amount != '') {
          $journalLines[] = [
            'LineAmount' => $amount,
            'AccountCode' => $accountCode
          ];
        }
      }
    } else {
      $journalLines = [
        'LineAmount' => $lineAmount,
        'AccountCode' => $accountCode
      ];
    }

    return $journalLines;
  }

}