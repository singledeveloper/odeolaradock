<?php

namespace Odeo\Domains\Accounting\Xero\Repository;

use Odeo\Domains\Accounting\Xero\Model\XeroAccount;
use Odeo\Domains\Core\Repository;

class XeroAccountRepository extends Repository {

  function __construct(XeroAccount $xeroAccount) {
    $this->setModel($xeroAccount);
  }

}