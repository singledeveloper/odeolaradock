<?php

namespace Odeo\Domains\Accounting\Xero\Repository;

use Odeo\Domains\Accounting\Xero\Model\XeroJournalLine;
use Odeo\Domains\Core\Repository;

class XeroJournalLineRepository extends Repository {

  function __construct(XeroJournalLine $xeroJournalLine) {
    $this->setModel($xeroJournalLine);
  }

}