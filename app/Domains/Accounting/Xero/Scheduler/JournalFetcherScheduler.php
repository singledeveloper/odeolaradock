<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 18/09/18
 * Time: 12.16
 */

namespace Odeo\Domains\Accounting\Xero\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Accounting\Xero\Helper\XeroApi;

class JournalFetcherScheduler {

  // max 10222
  private $xeroJournalRepo, $startPage = 1, $maxPages = 1;

  public function initialize() {
    $this->xeroJournalRepo = app()->make('Odeo\Domains\Accounting\Xero\Repository\XeroJournalRepository');
    $this->xeroJournalLineRepo = app()->make('Odeo\Domains\Accounting\Xero\Repository\XeroJournalLineRepository');
    $this->xeroAccountRepo = app()->make('Odeo\Domains\Accounting\Xero\Repository\XeroAccountRepository');
  }

  public function run() {
    $this->initialize();

    $client = XeroApi::getClient();
    $now = Carbon::now();

    $journalRecords = [];
    $journalLineRecords = [];

    for ($i = $this->startPage; $i <= $this->maxPages; $i++) {
      // adjust start page and max page when running
      $q = [
        'page' => $i,
      ];

      $result = $client->request('GET', 'https://api.xero.com/api.xro/2.0/manualjournals', [
        'query' => $q,
      ]);

      $journals = json_decode($result->getBody()->getContents(), true)['ManualJournals'];

      if (count($journals) == 0) break;

      foreach ($journals as $journal) {
        preg_match('/(\d*)\+000/', $journal['Date'], $dateMatches);
        preg_match('/(\d*)\+000/', $journal['UpdatedDateUTC'], $updatedDateMatches);

        $journalRecords[] = [
          'date' => Carbon::createFromTimestampMs($dateMatches[1]),
          'status' => $journal['Status'],
          'line_amount_types' => $journal['LineAmountTypes'],
          'updated_date_utc' => Carbon::createFromTimestampMs($updatedDateMatches[1]),
          'manual_journal_id' => $journal['ManualJournalID'],
          'narration' => $journal['Narration'],
          'show_on_cash_basis_reports' => $journal['ShowOnCashBasisReports'] ?? true,
          'has_attachments' => $journal['HasAttachments'],
        ];

        foreach($journal['JournalLines'] as $journalLine) {
          $journalLineRecords[] = [
            'manual_journal_id' => $journal['ManualJournalID'],
            'tax_type' => $journalLine['TaxType'],
            'tax_amount' => $journalLine['TaxAmount'],
            'debit_amount' => $journalLine['LineAmount'] > 0 ? $journalLine['LineAmount'] : 0,
            'credit_amount' => $journalLine['LineAmount'] < 0 ? abs($journalLine['LineAmount']) : 0,
            'account_code' => $journalLine['AccountCode'],
            'tracking' => json_encode($journalLine['Tracking']),
            'account_id' => $journalLine['AccountID'],
            'is_blank' => $journalLine['IsBlank'],
          ];
        }
      }
    }
    $this->xeroJournalRepo->saveBulk($journalRecords);
    $this->xeroJournalLineRepo->saveBulk($journalLineRecords);
  }

  public function importAccount() {
    $this->initialize();

    $client = XeroApi::getClient();
    $now = Carbon::now();

    $accountRecords = [];

    $result = $client->request('GET', 'https://api.xero.com/api.xro/2.0/accounts');

    $accounts = json_decode($result->getBody()->getContents(), true)['Accounts'];

    foreach ($accounts as $account) {
      preg_match('/(\d*)\+000/', $account['UpdatedDateUTC'], $matches);

      $accountRecords[] = [
        'account_id' => $account['AccountID'],
        'code' => $account['Code'],
        'name' => $account['Name'],
        'status' => $account['Status'],
        'type' => $account['Type'],
        'tax_type' => $account['TaxType'],
        'description' => $account['Description'],
        'class' => $account['Class'],
        'enable_payments_to_account' => $account['EnablePaymentsToAccount'],
        'show_in_expense_claims' => $account['ShowInExpenseClaims'],
        'bank_account_type' => $account['BankAccountType'],
        'reporting_code' => $account['ReportingCode'],
        'reporting_code_name' => $account['ReportingCodeName'],
        'has_attachments' => $account['HasAttachments'],
        'updated_date_utc' => Carbon::createFromTimestampMs($matches[1]),
      ];
    }

    $this->xeroAccountRepo->saveBulk($accountRecords);
  }
}