<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/30/17
 * Time: 12:54 PM
 */

namespace Odeo\Domains\Accounting\Xero;

use Odeo\Domains\Accounting\Xero\Helper\XeroApi;
use Odeo\Domains\Accounting\Xero\Helper\XeroJsonFormatter;
use Odeo\Domains\Accounting\Xero\JournalProcessor\AccountReceivableJournalProcessor;
use Odeo\Domains\Accounting\Xero\JournalProcessor\BankTransferJournalProcessor;
use Odeo\Domains\Accounting\Xero\JournalProcessor\CashOnDeliveryJournalProcessor;
use Odeo\Domains\Accounting\Xero\JournalProcessor\OcashJournalProcessor;
use Odeo\Domains\Accounting\Xero\Repository\XeroJournalLineRecordRepository;
use Odeo\Domains\Constant\AccountingCode;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Repository\BankBriInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Mandiri\Repository\BankMandiriInquiryRepository;

class XeroTester {

  public function runTest(PipelineListener $listener, $data) {
    $journalLines[] = XeroJsonFormatter::generateJournalLine('6200000004', 7609605);
    $journalLines[] = XeroJsonFormatter::generateJournalLine('1300100000', -7609605);
    $manualJournal[] = XeroJsonFormatter::generateManualJournal('2016-12-31', 'Auditor Adjustment:Status oPay Device Prototype/Sample', $journalLines);
    $manualJournalJson = XeroJsonFormatter::wrapManualJournals($manualJournal);

    $client = XeroApi::getClient();
    try {
//      $response = $client->put(XeroApi::MANUAL_JOURNAL_URL, [
//        'json' => $manualJournalJson
//      ]);
//      \Log::info($response->getBody()->getContents());
    } catch (\Exception $e) {
      \Log::info($e->getMessage());
    }
    return $listener->response(200, $manualJournalJson );
  }
}