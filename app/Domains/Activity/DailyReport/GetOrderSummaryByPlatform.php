<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/4/17
 * Time: 16:58
 */

namespace Odeo\Domains\Activity\DailyReport;


use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository;

class GetOrderSummaryByPlatform {

  private $switcher;

  public function __construct() {
    $this->switcher = app()->make(OrderDetailPulsaSwitcherRepository::class);
  }

  public function get($isCommunity) {
    $result = $this->switcher
      ->getOrderSalesThisMonthByPlatform(1, $isCommunity)
      ->keyBy('platform_id');

    foreach ($result as &$summary) {
      $name = Platform::getConstKeyStringByValue($summary->platform_id);
      $summary->platform_name = $name;
    }

    $this->addGroupName($result, Platform::getAppPlatforms(), 'app');
    $this->addGroupName($result, Platform::getH2HPlatforms(), 'H2H');
    $this->addGroupName($result, Platform::getOthersPlatforms(), 'others');

    return $result->groupBy('group_name');
  }

  private function addGroupName(&$result, $platforms, $groupName) {
    foreach ($platforms as $platform) {
      if (isset($result[$platform])) {
        $result[$platform]->group_name = $groupName;
      } else {
        $result[$platform] = [
          'group_name' => $groupName,
          'platform_name' => Platform::getConstKeyStringByValue($platform),
          'gross_amount' => 0,
          'surplus' => 0,
          'order_total' => 0,
        ];
      }
    }
  }

}