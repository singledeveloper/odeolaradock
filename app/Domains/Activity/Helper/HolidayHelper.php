<?php

namespace Odeo\Domains\Activity\Helper;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;

class HolidayHelper {

  private $holidayRepo, $holidays;

  public function __construct() {
    $this->holidayRepo = app()->make(\Odeo\Domains\Activity\Repository\HolidayRepository::class);
    $this->holidays = $this->holidayRepo->getAllActive();
  }

  public function addWorkingDays($startDate, $dayNum = 1) {
    $endDate = $startDate->copy();
    $workingDayNum = 0;

    while ($workingDayNum < $dayNum) {
      $endDate->addDay();
      if (!$this->isHoliday($endDate)) {
        $workingDayNum++;
      }
    }

    return $endDate;
  }

  public function subWorkingDays($startDate, $dayNum = 1) {
    $endDate = $startDate->copy();
    $workingDayNum = 0;

    while ($workingDayNum < $dayNum) {
      $endDate->subDay();
      if (!$this->isHoliday($endDate)) {
        $workingDayNum++;
      }
    }

    return $endDate;
  }

  private function isHoliday($date) {
    return in_array($date->dayOfWeek, [0, 6]) || in_array($date->format('Y-m-d'), $this->holidays);
  }
}