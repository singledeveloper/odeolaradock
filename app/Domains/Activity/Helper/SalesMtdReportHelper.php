<?php
/**
 * Created by PhpStorm.
 * User: hs
 * Date: 10/9/18
 * Time: 2:49 PM
 */

namespace Odeo\Domains\Activity\Helper;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class SalesMtdReportHelper {

  private $redis;

  public function __construct() {
    $this->redis = Redis::connection();
  }

  public function calculate($sumByMonthMap, $yesterdayDate) {
    $mtdMap = [];
    $targetMap = [];
    $targetMtdMap = [];
    $predictionMap = [];
    $runRateMap = [];
    $athRunRateMap = [];
    $ytdMap = [];
    $prevMonthSumMap = [];
    $athMap = [];
    $startPeriod = $yesterdayDate->copy()->subYear()->format('Y-m');
    $endPeriod = $yesterdayDate->format('Y-m');
    $fxRate = $this->getFxRate('USD_IDR');

    foreach ($sumByMonthMap as $key => $rows) {
      list($mtd, $prevMonths) = $this->splitByMonth($rows, $yesterdayDate);
      $prevMonthSumMap[$key] = $prevMonths;
      $mtdMap[$key] = $mtd;
      $predictionMap[$key] = $this->toPrediction($mtd, $yesterdayDate);
    }

    foreach ($prevMonthSumMap as $key => $rows) {
      $athMap[$key] = $this->getMaxGmvOfProduct($rows);
    }

    $targetPeriod = $this->getMaxGmvPeriod($prevMonthSumMap);

    foreach ($prevMonthSumMap as $key => $rows) {
      $row = $this->getByPeriod($rows, $targetPeriod);
      $targetMap[$key] = $this->toTarget($row);
      $targetMtdMap[$key] = $this->toMtd($targetMap[$key], $yesterdayDate);
    }

    foreach ($mtdMap as $key => $mtd) {
      $runRateMap[$key] = $this->toRunRate($mtd, $targetMtdMap[$key]);
      $athRunRateMap[$key] = $this->toRunRate($mtd, $this->toMtd($athMap[$key], $yesterdayDate));
    }

    foreach ($sumByMonthMap as $key => $rows) {
      $ytdMap[$key] = $this->sum($this->getBetweenPeriod($rows, $startPeriod, $endPeriod));
    }

    $salesMtd = $this->sum($mtdMap);
    $salesAth = $this->sum($athMap);
    $salesTarget = $this->sum($targetMap);
    $salesTargetMtd = $this->toMtd($salesTarget, $yesterdayDate);
    $salesPrediction = $this->toPrediction($salesMtd, $yesterdayDate);
    $salesRunRate = $this->toRunRate($salesMtd, $salesTargetMtd);
    $salesAthRunRate = $this->toRunRate($salesMtd, $this->toMtd($salesAth, $yesterdayDate));
    $salesYtd = $this->sum($ytdMap);

    return [
      'mtdMap' => $mtdMap,
      'athMap' => $athMap,
      'ytdMap' => $ytdMap,
      'targetMap' => $targetMap,
      'targetMtdMap' => $targetMtdMap,
      'predictionMap' => $predictionMap,
      'runRateMap' => $runRateMap,
      'athRunRateMap' => $athRunRateMap,
      'salesMtd' => $salesMtd,
      'salesAth' => $salesAth,
      'salesYtd' => $salesYtd,
      'salesTarget' => $salesTarget,
      'salesTargetMtd' => $salesTargetMtd,
      'salesPrediction' => $salesPrediction,
      'salesRunRate' => $salesRunRate,
      'salesAthRunRate' => $salesAthRunRate,
      'fxRate' => $fxRate,
    ];
  }

  private function getFxRate($key) {
    $client = new Client([
      'http_errors' => false,
    ]);
    $response = $client->get("http://free.currencyconverterapi.com/api/v5/convert?q=$key&compact=y");

    if ($response->getStatusCode() == 200) {
      $body = $response->getBody()->getContents();
      $val = floor(json_decode($body)->{$key}->val);
    }
    else {
      $val = $this->redis->get("odeo_core:fx_$key");
    }

    if (!$val) {
      $val = 14617;
    }

    $this->redis->set("odeo_core:fx_$key", $val);
    return $val;
  }

  public function getByPeriod($rows, $period) {
    foreach ($rows as $row) {
      if ($row->period == $period) {
        return (object)[
          'gmv' => $row->gmv,
          'revenue' => $row->revenue,
          'trx' => $row->trx,
        ];
      }
    }

    return $this->newEmptyMtd();
  }

  private function getBetweenPeriod($rows, $startPeriod, $endPeriod) {
    $result = [];

    foreach ($rows as $row) {
      if ($startPeriod <= $row->period && $row->period <= $endPeriod) {
        $result[] = $row;
      }
    }

    return $result;
  }

  private function toTarget($row, $gmvMult = 1.05, $revenueMult = 0.005, $trxMult = 1.05) {
    return (object)[
      'gmv' => ceil($row->gmv * $gmvMult),
      'revenue' => ceil($row->gmv * $gmvMult * $revenueMult),
      'trx' => ceil($row->trx * $trxMult),
    ];
  }

  private function toMtd($row, $date) {
    $noOfDay = $date->format('t');
    $dayNo = $date->format('d');
    return (object)[
      'gmv' => ceil($dayNo / $noOfDay * $row->gmv),
      'revenue' => ceil($dayNo / $noOfDay * $row->revenue),
      'trx' => ceil($dayNo / $noOfDay * $row->trx),
    ];
  }

  private function toPrediction($row, $date) {
    $noOfDay = $date->format('t');
    $dayNo = $date->format('d');
    return (object)[
      'gmv' => ceil($row->gmv / $dayNo * $noOfDay),
      'revenue' => ceil($row->revenue / $dayNo * $noOfDay),
      'trx' => ceil($row->trx / $dayNo * $noOfDay),
    ];
  }

  private function toRunRate($mtd, $targetMtd) {
    return (object)[
      'gmv' => $targetMtd->gmv ? round($mtd->gmv / $targetMtd->gmv * 100, 2) : 0,
      'revenue' => $targetMtd->revenue ? round($mtd->revenue / $targetMtd->revenue * 100, 2) : 0,
      'trx' => $targetMtd->trx ? round($mtd->trx / $targetMtd->trx * 100, 2) : 0,
    ];
  }

  private function splitByMonth($sumByPeriod, $date) {
    $currentPeriod = $date->format('Y-m');
    $mtd = null;
    $result = [];

    foreach ($sumByPeriod as $row) {
      if ($row->period == $currentPeriod) {
        $mtd = $row;
      }
      else {
        $result[] = $row;
      }
    }


    if (!$mtd) {
      $mtd = (object)[
        'period' => $currentPeriod,
        'gmv' => 0,
        'revenue' => 0,
        'trx' => 0,
      ];
    }

    return [$mtd, $result];
  }

  private function getMaxGmvPeriod($sumByMonthMap) {
    $maxGmv = 0;
    $maxPeriod = '';
    $gmvMap = [];

    foreach ($sumByMonthMap as $rows) {
      foreach ($rows as $row) {
        if (!isset($gmvMap[$row->period])) {
          $gmvMap[$row->period] = 0;
        }
        $gmvMap[$row->period] += $row->gmv;

        if ($gmvMap[$row->period] > $maxGmv) {
          $maxGmv = $gmvMap[$row->period];
          $maxPeriod = $row->period;
        }
      }
    }

    return $maxPeriod;
  }

  private function getMaxGmvOfProduct($sumByMonth) {
    $max = $this->newEmptyMtd();

    foreach ($sumByMonth as $row) {
      if ($row->gmv > $max->gmv) {
        $max = $row;
      }
    }

    return $max;
  }

  public function sum($rows) {
    $result = $this->newEmptyMtd();

    foreach ($rows as $row) {
      $result->gmv += $row->gmv;
      $result->revenue += $row->revenue;
      $result->trx += $row->trx;
    }

    return $result;
  }

  public function newEmptyMtd() {
    return (object)[
      'gmv' => 0,
      'revenue' => 0,
      'trx' => 0,
    ];
  }


}