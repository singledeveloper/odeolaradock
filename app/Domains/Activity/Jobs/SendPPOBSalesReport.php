<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/24/17
 * Time: 8:30 PM
 */

namespace Odeo\Domains\Activity\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendPPOBSalesReport extends Job {

  private $data, $date;

  public function __construct($data, $date) {
    parent::__construct();
    $this->data = $data;
    $this->date = $date;
  }

  public function handle() {
    if (app()->environment() == 'production') {
      Mail::send('emails.email_send_ppob_sales_report', array_merge($this->data, ['date' => $this->date]), function ($m) {
        $m->from('noreply@odeo.co.id', 'odeo');
        $m->to('ppob@odeo.co.id')->subject('Daily PPOB Sales Report - ' . $this->date);
      });

    }

  }

}
