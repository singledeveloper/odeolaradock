<?php

namespace Odeo\Domains\Activity;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ForBusiness;
use Odeo\Domains\Constant\Gojek;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\UserKycStatus;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\PipelineListener;

class RealtimeDetailizer {

  private $vendorSwitchers, $pulsaSwitchers, $gojekUsers, $vendorDisbursements, $vendorMutationRepo,
    $users, $kycRepo, $userCashes, $businessResellerTransactionRepo, $paymentGatewayChannelRepo, $apiDisbursementRepo,
    $notificationInternalRepo, $dailyReportRepo, $currencyHelper, $paymentGatewayPaymentRepo, $redis;

  public function __construct() {
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->pulsaSwitchers = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->notificationInternalRepo = app()->make(\Odeo\Domains\Notification\Repository\NotificationInternalRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->gojekUsers = app()->make(\Odeo\Domains\Biller\Gojek\Repository\GojekUserRepository::class);
    $this->dailyReportRepo = app()->make(\Odeo\Domains\Activity\Repository\DailySalesReportRepository::class);
    $this->vendorDisbursements = app()->make(\Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->kycRepo = app()->make(\Odeo\Domains\Account\Repository\UserBusinessKycRepository::class);
    $this->userCashes = app()->make(\Odeo\Domains\Transaction\Repository\UserCashRepository::class);
    $this->businessResellerTransactionRepo = app()->make(\Odeo\Domains\Marketing\Repository\BusinessResellerTransactionRepository::class);
    $this->paymentGatewayChannelRepo = app()->make(\Odeo\Domains\PaymentGateway\Repository\PaymentGatewayChannelRepository::class);
    $this->paymentGatewayPaymentRepo = app()->make(\Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository::class);
    $this->vendorMutationRepo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherMutationRepository::class);
    $this->apiDisbursementRepo = app()->make(\Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository::class);
    $this->redis = Redis::connection();
  }

  public function get(PipelineListener $listener, $data) {

    $informations = ['order_queue_counts' => $this->pulsaSwitchers->getQueueCounts()];

    foreach ($this->notificationInternalRepo->getUnreads() as $item) {
      $informations['notifications'][] = [
        'id' => $item->id,
        'message' => '[' . date('Y/m/d H:i', strtotime($item->updated_at)) . '] ' .
          str_replace(NotificationType::PARSE_MULTIPLIER, $item->multiplier, $item->message)
      ];
    }

    $currentGojekBalance = 0;
    $informations['billers'] = [];
    if (isset($data['vendor_switcher_id_in'])) {
      $mutations = $this->vendorMutationRepo->findByVendorSwitcherIds(explode(',', $data['vendor_switcher_id_in']));

      foreach ($this->vendorSwitchers->getByIds(explode(',', $data['vendor_switcher_id_in'])) as $item) {
        $informations['billers'][] = [
          'name' => $item->name,
          'current_balance' => $item->minimum_topup == null ? intval($item->current_balance) : $this->currencyHelper->formatPrice($item->current_balance),
          'is_active' => $item->minimum_topup == null ? true : $item->is_active,
          'last_stable_at' => isset($mutations[$item->id]) && $mutations[$item->id]->last_zero_at ? Carbon::parse($mutations[$item->id]->last_zero_at)->diffForHumans() : ''
        ];
        if ($item->id == SwitcherConfig::INTERNAL_GOJEK) {
          $currentGojekBalance = $item->current_balance;
        }
      }
    }

    $gojekStoreTPS = 0;
    $gojekInternalTPS = 0;
    foreach ($this->gojekUsers->getAccountTPS() as $item) {
      if ($item->store_id == null) $gojekInternalTPS = round($item->tps, 2, PHP_ROUND_HALF_DOWN);
      else {
        $gojekStoreTPS+=round($item->tps, 2, PHP_ROUND_HALF_DOWN);
        $currentGojekBalance += $item->total;
      }
    }
    $informations['gojek_internal_tps'] = $gojekInternalTPS;
    $informations['gojek_store_tps'] = $gojekStoreTPS;

    $showTopup = false;
    foreach ($this->gojekUsers->getMonitoringAccounts() as $item) {
      $status = $item->token == null ? Gojek::PENDING_OTP : $item->status;
      if ($status == Gojek::ON_HOLD_PERMANENT && $item->current_balance == 0) continue;
      $informations['gojek_users'][] = [
        'name' => revertTelephone($item->telephone),
        'current_balance' => $this->currencyHelper->formatPrice($item->current_balance),
        'monthly_cash_in' => $this->currencyHelper->formatPrice($item->monthly_cash_in),
        'status' => $status
      ];
      if ($status == Gojek::CONNECTED) $showTopup = true;
    }

    $informations['suppliers'] = [];
    /*if (isset($data['vendor_switcher_with_store_id_in'])) {
      foreach ($this->vendorSwitchers->getByIdsWithStoreName(explode(',', $data['vendor_switcher_with_store_id_in'])) as $item) {
        $informations['suppliers'][] = [
          'name' => $item->name,
          'current_balance' => $this->currencyHelper->formatPrice($item->current_balance)
        ];
      }
    }*/

    $dailyVendors = [];
    foreach ($this->dailyReportRepo->getDisbursementVendorWeeks() as $item) {
      $total = $item->total;
      $key = strtoupper(explode('_', $item->group_type)[0]);
      if ($key == 'CIMB') $key = 'CIMBNIAGA';
      else if ($key == 'BCA') $total += 100000000;
      else if ($key == 'ARTAJASA') $total += 200000000;
      else if ($key == 'MANDIRI') $total += 500000000;
      if (date('N') == 1 && in_array($key, ['BRI', 'BNI'])) {
        $total = ceil($total * 2/3);
      }
      $dailyVendors[$key] = $this->currencyHelper->formatPrice($total);
    }

    foreach ($this->vendorDisbursements->getByIds([
      VendorDisbursement::ARTAJASA,
      VendorDisbursement::CIMB,
      VendorDisbursement::BNI,
      VendorDisbursement::PERMATA,
      VendorDisbursement::BCA,
      VendorDisbursement::BRI,
      VendorDisbursement::MANDIRI
    ]) as $item) {
      $bank = [
        'name' => $item->name . ($item->name == 'BCA' ? (' (H+1)') : ''),
        'current_balance' => $this->currencyHelper->formatPrice($item->balance)
      ];
      if (isset($dailyVendors[$item->name])) $bank['prediction_value'] = $dailyVendors[$item->name];
      $informations['banks'][] = $bank;
    }

    $informations['users'] = [];
    $informations['inactive_users'] = [];
    $currentGojekUserBalance = 0;

    if (isset($data['user_id_in'])) {
      $data['user_id_in'] = explode(',', $data['user_id_in']);
      $userNames = [];
      foreach ($this->users->getBusinessUsers([134974, 171700, 171652]) as $item) {
        if (!in_array($item->id, $data['user_id_in'])) {
          $data['user_id_in'][] = $item->id;
          $userNames[$item->id] = $item->name;
        }
      }

      $kycUsers = $this->kycRepo->checkMonitoringUsers($data['user_id_in']);
      $hasUserIdData = [];
      foreach ($this->userCashes->getUserCashByUserIds($data['user_id_in']) as $item) {
        $informations['users'][] = [
          'name' => $item->name,
          'current_balance' => $this->currencyHelper->formatPrice($item->amount),
          'kyc_status' => isset($kycUsers[$item->user_id]) ? UserKycStatus::getMonitoringIcon($kycUsers[$item->user_id]->status) : false
        ];
        $hasUserIdData[] = $item->user_id;
        if (in_array($item->user_id, [24352, 131313])) {
          $currentGojekUserBalance+= $item->amount;
        }
      }

      foreach ($data['user_id_in'] as $item) {
        if (!in_array($item, $hasUserIdData)) {
          $informations['inactive_users'][] = [
            'name' => $userNames[$item],
            'kyc_status' => isset($kycUsers[$item]) ? UserKycStatus::getMonitoringIcon($kycUsers[$item]->status) : false
          ];
        }
      }
    }

    $gojekBalanceNeeded = $currentGojekUserBalance - $currentGojekBalance;
    $informations['gojek_topup_multiplier'] = $this->currencyHelper->formatPrice(Gojek::TOPUP_MULTIPLIER);
    if ($showTopup && $gojekBalanceNeeded > 0) {
      $informations['gojek_topup_needed'] = ceil($gojekBalanceNeeded / Gojek::TOPUP_MULTIPLIER) + 2;
    }
    else $informations['gojek_topup_needed'] = 0;

    $pgLastDate = [];
    foreach ($this->paymentGatewayPaymentRepo->getLastDatePaymentGroup() as $item) {
      $pgLastDate[$item->payment_group_id] = Carbon::parse($item->last_created_at)->diffForHumans();
    }

    $informations['pg_suspect_counts'] = $this->paymentGatewayPaymentRepo->getSuspectCount();

    $informations['pg_usages'] = [];
    foreach ($this->paymentGatewayChannelRepo->getLastTenDaysPaymentGatewayUsages() as $item) {
      $informations['pg_usages'][] = [
        'id' => $item->id,
        'name' => $item->name,
        'short_name' => str_replace('Virtual Account', 'VA', $item->name),
        'total' => $this->currencyHelper->formatPrice($item->amount ? $item->amount : 0),
        'qty' => $item->qty ? $item->qty : 0,
        'last_created_at' => isset($pgLastDate[$item->id]) ? $pgLastDate[$item->id] : ''
      ];
    }


    $userUsages = [];
    $lastDateDisbursementUserIds = [];
    foreach ($this->businessResellerTransactionRepo->getLastTenDaysUsagesByUser() as $item) {
      if (!isset($userUsages[$item->user_id])) {
        $userUsages[$item->user_id] = [
          'name' => $item->user_name,
          ForBusiness::TRANSACTION_PAYMENT_GATEWAY => 0,
          ForBusiness::TRANSACTION_DISBURSEMENT => 0
        ];
      }
      $userUsages[$item->user_id][$item->type] = $item->amount;
      if ($item->type == ForBusiness::TRANSACTION_DISBURSEMENT && $item->amount > 0)
        $lastDateDisbursementUserIds[] = $item->user_id;
    }

    $lastDateDisbursements = [];
    if (count($lastDateDisbursementUserIds) > 0) {
      $lastDateDisbursements = $this->apiDisbursementRepo->getLastDisbursementDateByUserIds($lastDateDisbursementUserIds);
    }

    $informations['payment_users'] = [];
    foreach ($userUsages as $key => $item) {
      if ($item[ForBusiness::TRANSACTION_PAYMENT_GATEWAY] == 0) continue;
      $informations['payment_users'][] = [
        'name' => $item['name'],
        'last_disbursement_date' => isset($lastDateDisbursements[$key]) ? date('Y-m-d H:i', strtotime($lastDateDisbursements[$key]->last_date)) : '',
        ForBusiness::TRANSACTION_PAYMENT_GATEWAY => $this->currencyHelper->formatPrice($item[ForBusiness::TRANSACTION_PAYMENT_GATEWAY]),
        ForBusiness::TRANSACTION_DISBURSEMENT => $this->currencyHelper->formatPrice($item[ForBusiness::TRANSACTION_DISBURSEMENT])
      ];
    }

    $informations['heart_rate'] = ($this->redis->llen('queues:global') + $this->redis->zcard('queues:global:delayed'));

    //$route = app()->make(Request::class)->route();
    //$informations['check_route'] = isset($route[1]['as']) ? $route[1]['as'] : 'no-name';

    return $listener->response(200, ['informations' => $informations]);
  }

}
