<?php

namespace Odeo\Domains\Activity\Repository;

use Odeo\Domains\Activity\Model\DailySalesReport;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\Repository;

class DailySalesReportRepository extends Repository {

  public function __construct(DailySalesReport $dailySalesReport) {
    $this->model = $dailySalesReport;
  }

  public function getDisbursementVendorWeeks() {
    return $this->getCloneModel()
      ->where(function($query){
        $query->whereRaw("(cast(sales_date as date) >= date_trunc('day', CURRENT_TIMESTAMP - interval '7 days')
  and cast(sales_date as date) <= date_trunc('day', CURRENT_TIMESTAMP - interval '5 days'))")
          ->where('group_type', '<>', Withdraw::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT);
      })
      ->orWhere(function($query){
        $query->whereRaw("cast(sales_date as date) = date_trunc('day', CURRENT_TIMESTAMP - interval '7 days')")
          ->where('group_type', Withdraw::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT);
      })
      ->groupBy('group_type')
      ->select('group_type', \DB::raw('sum(value) as total'))
      ->get();
  }

  public function getByTransactionDate($date) {
    return $this->getCloneModel()->where('sales_date', $date)->get()->keyBy('group_type');
  }


}