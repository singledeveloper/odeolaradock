<?php

namespace Odeo\Domains\Activity\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Constant\Pulsa;

class AutoRecordSKUDataScheduler {

  private $pulsaOdeos, $notificationInternalRepo, $pulsaReports, $reports = [], $date;

  public function __construct() {
    $this->pulsaOdeos = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->notificationInternalRepo = app()->make(\Odeo\Domains\Notification\Repository\NotificationInternalRepository::class);
    $this->pulsaReports = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoReportRepository::class);
  }

  public function addReport($type, $total, $additionalData = null) {
    $this->reports[] = [
      'report_date' => $this->date,
      'report_type' => $type,
      'total' => $total,
      'additional_data' => $additionalData,
      'created_at' => Carbon::now()->toDateTimeString(),
      'updated_at' => Carbon::now()->toDateTimeString()
    ];
  }

  public function run() {

    $yesterdayDate = Carbon::now()->subDay(1);
    $this->date = $yesterdayDate->format('Y-m-d');

    $allOdeoSKU = $this->pulsaOdeos->getAllSKU();
    $this->addReport(Pulsa::REPORT_TYPE_SKU, count($allOdeoSKU));

    $allOdeoActiveSKU = $this->pulsaOdeos->getAllSKU(false, true);
    $disabledIds = [];
    $disabledSKUData = [];
    foreach ($allOdeoSKU as $item) if (!in_array($item, $allOdeoActiveSKU)) $disabledIds[] = $item;
    if (count($disabledIds) > 0) {
      foreach ($this->pulsaOdeos->getByIds($disabledIds) as $item)
        $disabledSKUData[] = $item->name;
      sort($disabledSKUData);
    }
    $this->addReport(Pulsa::REPORT_TYPE_SKU_DISABLED, count($disabledSKUData), json_encode($disabledSKUData));

    $allMarketplaceSKU = $this->pulsaOdeos->getAllSKU(true);
    $this->addReport(Pulsa::REPORT_TYPE_MARKETPLACE_SKU, count($allMarketplaceSKU));

    $allMarketplaceActiveSKU = $this->pulsaOdeos->getAllSKU(true, true);
    $disabledIds = [];
    $disabledSKUData = [];
    foreach ($allMarketplaceSKU as $item) if (!in_array($item, $allMarketplaceActiveSKU)) $disabledIds[] = $item;
    if (count($disabledIds) > 0) {
      foreach ($this->pulsaOdeos->getByIds($disabledIds) as $item)
        $disabledSKUData[] = $item->name;
      sort($disabledSKUData);
    }
    $this->addReport(Pulsa::REPORT_TYPE_MARKETPLACE_SKU_DISABLED, count($disabledSKUData), json_encode($disabledSKUData));

    $oosSKU = $this->pulsaOdeos->getAllOOS();
    $oosSKUData = [];
    foreach ($oosSKU as $item) $oosSKUData[] = $item->name;
    $this->addReport(Pulsa::REPORT_TYPE_OOS, count($oosSKUData), json_encode($oosSKUData));

    $problemSKU = [];
    $refundCount = 0;
    foreach ($this->notificationInternalRepo->getAllProblemInventories($this->date) as $item) {
      $refundCount+=$item->icount;
      $problemSKU[] =
        $item->icount . ' ' .
        ucwords(str_replace('_', ' ', $item->message_type)) . ' di ' .
        $item->name;
    }
    $this->addReport(Pulsa::REPORT_TYPE_REFUND, $refundCount, json_encode($problemSKU));

    $deadStockData = [];
    foreach ($this->pulsaOdeos->getDeadStock() as $item) {
      $deadStockData[] = $item->name . ' > ' . ($item->last_succeeded_at != null ? $item->last_succeeded_at : 'NO PURCHASE');
    }
    $this->addReport(Pulsa::REPORT_TYPE_DEAD_STOCK, count($deadStockData), json_encode($deadStockData));

    $this->pulsaReports->saveBulk($this->reports);

  }
}
