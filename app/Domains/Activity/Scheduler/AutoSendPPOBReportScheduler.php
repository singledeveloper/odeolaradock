<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/24/17
 * Time: 7:31 PM
 */

namespace Odeo\Domains\Activity\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Activity\Helper\SalesMtdReportHelper;
use Odeo\Domains\Activity\Jobs\SendPPOBSalesReport;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository;

class AutoSendPPOBReportScheduler {

  private $pulsaOdeoInventoryRepo, $switcherOrder, $mtdHelper;

  public function __construct() {
    $this->order = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->mtdHelper = app()->make(SalesMtdReportHelper::class);
    $this->pulsaOdeoInventoryRepo = app()->make(PulsaOdeoInventoryRepository::class);
  }

  public function run() {
    $yesterday = 1;
    $yesterdayDate = Carbon::now()->subDay($yesterday);
    $yesteryearDate = $yesterdayDate->copy()->subYear();
    $startOfMonthDate = $yesterdayDate->copy()->startOfMonth();
    $date = $yesterdayDate->format('d M Y');
    $period = $yesterdayDate->format('Y-m');

    $inventoryCounts = [];
    foreach ($this->pulsaOdeoInventoryRepo->getCountsByOwner() as $row) {
      if (!isset($inventoryCounts[$row->owner])) {
        $inventoryCounts[$row->owner] = [];
      }
      $inventoryCounts[$row->owner][$row->stock] = $row->count;
    }

    $channelLabelMap = [
      'ppobApp' => 'App',
      'ppobH2H' => 'H2H',
      'ppobOthers' => 'Others',
      'ppobMarketPlace' => 'MarketPlace',
    ];
    $keys = [];
    $sumByMonthMap = [];
    $refundedMtdMap = [];
    $completedPPOBMap = $this->switcherOrder->getSumByMonthAndService($yesteryearDate, $yesterdayDate, Service::PPOB);
    $refundedMtdPPOBMap = $this->switcherOrder
      ->getSumByMonthAndService($startOfMonthDate, $yesterdayDate, Service::PPOB, SwitcherConfig::SWITCHER_REFUNDED);

    foreach ($completedPPOBMap as $value) {
      $keys[$value->type][] = $value->service;
      $sumByMonthMap[$value->type . ' ' . $value->service][] = $value;
    }

    $refundedMtdPPOBMap = collect($refundedMtdPPOBMap)
      ->groupBy(function ($row) {
        return $row->type . ' ' . $row->service;
      })
      ->toArray();

    foreach ($refundedMtdPPOBMap as $key => $rows) {
      foreach ($rows as $row) {
        $keys[$row->type][] = $row->service;
      }
      $refundedMtdMap[$key] = $this->mtdHelper->getByPeriod($rows, $period);
    }

    foreach ($keys as $c => $services) {
      $keys[$c] = array_unique($services);
      foreach ($keys[$c] as $s) {
        $key = $c . ' ' . $s;

        if (empty($sumByMonthMap[$key])) {
          $empty = $this->mtdHelper->newEmptyMtd();
          $empty->period = $period;
          $sumByMonthMap[$key][] = $empty;
        }

        if (!isset($refundedMtdMap[$key])) {
          $refundedMtdMap[$key] = $this->mtdHelper->newEmptyMtd();
        }
      }
    }

    $calculatedData = $this->mtdHelper->calculate($sumByMonthMap, $yesterdayDate);
    $salesRefundedMtd = $this->mtdHelper->sum(array_values($refundedMtdMap));

    dispatch(new SendPPOBSalesReport(array_merge([
      'keys' => $keys,
      'inventoryCounts' => $inventoryCounts,
      'channelLabelMap' => $channelLabelMap,
      'orderValueReportsApp' => $this->order->getOrderValue($yesterday, Platform::getAppPlatforms()),
      'orderValueReportsH2H' => $this->order->getOrderValue($yesterday, Platform::getH2HPlatforms()),
      'orderValueReportsOthers' => $this->order->getOrderValue($yesterday, Platform::getOthersPlatforms()),
      'pulsaSkuCount' => $this->switcherOrder->getCountBySKU([], false, $date, 20),
      'pulsaSkuCountTotal' => $this->switcherOrder->getTotalOrderCount($yesterday),
      'pulsaFailed' => $this->switcherOrder->getFailedBySKU([], false, $date, 20),
      'pulsaFailedTotal' => $this->switcherOrder->getTotalFailed($yesterday),
      'pulsaSkuValue' => $this->switcherOrder->getOrderValueBySKU([], false, $date, 20),
      'pulsaSkuValueTotal' => $this->switcherOrder->getTotalOrderValue($yesterday),
      'refundedMtdMap' => $refundedMtdMap,
      'salesRefundedMtd' => $salesRefundedMtd,
    ], $calculatedData), $date));
  }

}
