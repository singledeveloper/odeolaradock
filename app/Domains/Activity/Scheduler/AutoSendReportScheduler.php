<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/24/17
 * Time: 7:31 PM
 */

namespace Odeo\Domains\Activity\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Account\Model\UserGroup;
use Odeo\Domains\Account\Repository\UserGroupRepository;
use Odeo\Domains\Activity\Helper\SalesMtdReportHelper;
use Odeo\Domains\Activity\Jobs\SendSalesReport;
use Odeo\Domains\Constant\CashConfig;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository;
use Odeo\Domains\Transaction\Helper\CashLimiter;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;

class AutoSendReportScheduler {

  private $userPurchases, $salesPlatformReport, $userAccessLogs,
    $registeredUserReport, $order, $cashTransactions, $switcherOrder,
    $mandiriInquiry, $bcaInquiry, $vendorSwitchers, $withdraws, $referral,
    $orderSummaryPlatform, $apiDisbursements, $apiDisbursementInquiries,
    $affiliateUserInvoices, $vendorSwitcherSelector, $userRate,
    $userWithdraws, $disbursementApiUsers, $h2hManager, $mtdHelper, $dailySalesRepo, $dataToBeSaved = [];

  public function __construct() {
    $this->userPurchases = app()->make(\Odeo\Domains\Order\Repository\UserPurchaseRepository::class);
    $this->salesPlatformReport = app()->make(\Odeo\Domains\Order\Salesreport\Platform\Repository\SalesPlatformRepository::class);
    $this->registeredUserReport = app()->make(\Odeo\Domains\Account\Userreport\Registered\Repository\RegisteredUserReportRepository::class);
    $this->order = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->cashTransactions = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->mandiriInquiry = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Mandiri\Repository\BankMandiriInquiryRepository::class);
    $this->bcaInquiry = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository::class);
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->vendorSwitcherSelector = app()->make(\Odeo\Domains\Biller\BillerSelector::class);
    $this->withdraws = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);
    $this->userRate = app()->make(\Odeo\Domains\Marketing\Rateapp\Repository\UserRateAppRepository::class);
    $this->referral = app()->make(\Odeo\Domains\Network\Repository\ReferralDetailRepository::class);
    $this->orderSummaryPlatform = app()->make(\Odeo\Domains\Activity\DailyReport\GetOrderSummaryByPlatform::class);
    $this->userAccessLogs = app()->make(\Odeo\Domains\Account\Repository\UserAccessLogRepository::class);
    $this->apiDisbursements = app()->make(\Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository::class);
    $this->apiDisbursementInquiries = app()->make(\Odeo\Domains\Disbursement\Repository\ApiDisbursementInquiryRepository::class);
    $this->paymentGatewayPayments = app()->make(\Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository::class);
    $this->affiliateUserInvoices = app()->make(AffiliateUserInvoiceRepository::class);
    $this->userWithdraws = app()->make(UserWithdrawRepository::class);
    $this->disbursementApiUsers = app()->make(DisbursementApiUserRepository::class);
    $this->h2hManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper\PulsaH2HManager::class);
    $this->mtdHelper = app()->make(SalesMtdReportHelper::class);
    $this->dailySalesRepo = app()->make(\Odeo\Domains\Activity\Repository\DailySalesReportRepository::class);
  }

  public function run() {
    $yesterday = 1;
    $yesterdayDate = Carbon::now()->subDay($yesterday);
    $yesteryearDate = $yesterdayDate->copy()->subYear();
    $startOfMonthDate = $yesterdayDate->copy()->startOfMonth();
    $date = $yesterdayDate->format('d M Y');
    $period = $yesterdayDate->format('Y-m');

    $this->vendorSwitchers->normalizeFilters(['is_active' => true]);
    $vendorSwitchers = $this->vendorSwitchers->gets();
    $vendorSwitcherIds = [];
    foreach ($vendorSwitchers as $item) {
      $vendorSwitcherIds[] = $item->id;
    }

    $billerReports = [];
    $sla = $this->vendorSwitcherSelector->getSLA($vendorSwitcherIds, $date);
    $leadTime = $this->vendorSwitcherSelector->getLeadTime($vendorSwitcherIds, $date);

    foreach ($vendorSwitchers as $item) {
      $billerReports[] = json_decode(json_encode([
        "name" => $item->name,
        "sla" => $sla[$item->id]['success_percentage'] * 100,
        "success_purchase_count" => $sla[$item->id]['success_purchase_count'],
        "average_lead_time" => $leadTime[$item->id]
      ]));
    }

    $userGroupRepo = app()->make(UserGroupRepository::class);
    $withdrawB2BUserIdList = array_merge(
      $this->h2hManager->getUserH2H(),
      $userGroupRepo->getUserIds([
        UserType::GROUP_BYPASS_WITHDRAW_LIMIT,
        UserType::GROUP_BYPASS_CASH_LIMIT
      ]),
      array_keys(CashLimiter::THROTTLED_USER),
      $this->disbursementApiUsers->getAll()->pluck('user_id')->toArray()
    );

    $products = [
      'ppobApp' => 'PPOB APP',
      'ppobH2H' => 'PPOB H2H',
      'ppobOthers' => 'PPOB Others',
      'ppobMarketPlace' => 'PPOB Market Place',
      'disb' => 'Disbursement',
      'disbBatch' => 'Disbursement Batch',
      'disbInquiry' => 'Disbursement Inquiry',
      'payment_gateway' => 'Payment Gateway',
      'invoice' => 'Invoicing',
      'invoiceEDC' => 'Invoicing (EDC)',
      'withdrawB2C' => 'Withdraw B2C',
      'withdrawB2B' => 'Withdraw B2B',
    ];

    $completedPPOBMap = $this->switcherOrder->getSumByMonth($yesteryearDate, $yesterdayDate);
    $refundedMtdPPOBMap = $this->switcherOrder->getSumByMonth($startOfMonthDate, $yesterdayDate, SwitcherConfig::SWITCHER_REFUNDED);

    $sumByMonthMap = [
      'ppobApp' => [],
      'ppobH2H' => [],
      'ppobOthers' => [],
      'ppobMarketPlace' => [],
      'disb' => $this->apiDisbursements->getSumByMonth($yesterdayDate),
      'disbBatch' => $this->apiDisbursements->getSumBatchByMonth($yesterdayDate),
      'disbInquiry' => $this->apiDisbursementInquiries->getSumByMonth($yesterdayDate),
      'payment_gateway' => $this->paymentGatewayPayments->getSumByMonth($yesterdayDate),
      'invoice' => $this->affiliateUserInvoices->getSumByMonthWithoutMdr($yesterdayDate),
      'invoiceEDC' => $this->affiliateUserInvoices->getSumByMonthWithMdr($yesterdayDate),
      'withdrawB2C' => $this->userWithdraws->getSumByMonthWithUserIdList($yesterdayDate, 'except', $withdrawB2BUserIdList),
      'withdrawB2B' => $this->userWithdraws->getSumByMonthWithUserIdList($yesterdayDate, 'only', $withdrawB2BUserIdList),
    ];
    foreach ($completedPPOBMap as $value) {
      $sumByMonthMap[$value->type][] = $value;
    }

    $refundedMtdMap = [
      'ppobApp' => $this->mtdHelper->newEmptyMtd(),
      'ppobH2H' => $this->mtdHelper->newEmptyMtd(),
      'ppobOthers' => $this->mtdHelper->newEmptyMtd(),
      'ppobMarketPlace' => $this->mtdHelper->newEmptyMtd(),
    ];

    $refundedMtdPPOBMap = collect($refundedMtdPPOBMap)
      ->groupBy('type')
      ->toArray();

    foreach ($refundedMtdPPOBMap as $type => $rows) {
      $refundedMtdMap[$type] = $this->mtdHelper->getByPeriod($rows, $period);
    }

    $calculatedData = $this->mtdHelper->calculate($sumByMonthMap, $yesterdayDate);
    $salesRefundedMtd = $this->mtdHelper->sum(array_values($refundedMtdMap));
//
//    dispatch(new SendReport([
//      'salesPlatformReports' => $this->salesPlatformReport->getLockedReport($yesterday),
//      'registeredUserReports' => $this->registeredUserReport->getLockedReport($yesterday),
//      'depositTransactions' => $this->cashTransactions->getTransactionData(TransactionType::CONVERT, CashType::ODEPOSIT, $yesterday),
//      'topupTransactions' => $this->cashTransactions->getTransactionData(TransactionType::TOPUP, CashType::OCASH, $yesterday),
//      'withdrawTransactions' => $this->withdraws->getWithdraws($yesterday),
//      'notCompletedOrderCount' => $this->order->getNotCompletedOrderStatusCount($yesterday),
//      'leadTimeAbove60Count' => $this->switcherOrder->getLeadTimeAbove60($yesterday),
//      'mandiriBalance' => $this->mandiriInquiry->getLastRecordedBalance(BankAccount::MANDIRI_ODEO),
//      'bcaBalance' => $this->bcaInquiry->getLastRecordedBalance(BankAccount::BCA_ODEO),
//      'billerReports' => $billerReports,
//      'billerData' => $vendorSwitchers,
//      'userRates' => $this->userRate->getRate($yesterday),
//      'agentReport' => $this->referral->getReferralCount($yesterday),
//      'userAccessLogs' => $this->userAccessLogs->getTodayFirstLog($yesterday, [6096, 5193, 90051, 25551])
//    ], $date));

    $this->addToDB($this->apiDisbursements->getSuccessfulDisbursement($yesterdayDate->toDateString(), false),
      $yesterdayDate->toDateString(), Disbursement::SALES_TYPE_VENDORS, 'auto_disbursement_vendor', 'gross_amount');

    if (count($this->dataToBeSaved) > 0) {
      $existingReports = $this->dailySalesRepo->getByTransactionDate($yesterdayDate->toDateString());
      if (count($existingReports) > 0) {
        $temp = [];
        foreach ($this->dataToBeSaved as $item) {
          if (isset($existingReports[$item['group_type']])) {
            $report = $existingReports[$item['group_type']];
            $report->value = $item['value'];
            $this->dailySalesRepo->save($report);
          }
          else $temp[] = $item;
        }
        $this->dataToBeSaved = $temp;
      }
      $this->dailySalesRepo->saveBulk($this->dataToBeSaved);
    }

    dispatch(new SendSalesReport(array_merge([
      'orderPerPlatformMP' => $this->orderSummaryPlatform->get(true),
      'orderPerPlatformAgent' => $this->orderSummaryPlatform->get(false),
      'salesOcommerceReports' => $this->userPurchases->getoCommerceServiceSales($yesterday),
      'salesByPaymentReports' => $this->userPurchases->getoCommercePaymentSales($yesterday),
      'apiDisbursementReports' => $this->apiDisbursements->getSuccessfulDisbursement($yesterdayDate->toDateString()),
      'apiDisbursementInquiryReports' => $this->apiDisbursementInquiries->getSuccessfulInquiry($yesterdayDate->toDateString()),
      'apiDisbursementRecipients' => $this->apiDisbursements->getSuccessfulDisbursementRecipients($yesterdayDate->toDateString()),
      'apiDisbursementTopUsers' => $this->apiDisbursements->getTopUsers($yesterdayDate->toDateString()),
      'apiDisbursementInquiryTopUsers' => $this->apiDisbursementInquiries->getTopUsers($yesterdayDate->toDateString()),
      'paymentGatewayReports' => $this->paymentGatewayPayments->getSuccessfulPayment($yesterdayDate->toDateString()),
      'paymentGatewayTopUsers' => $this->paymentGatewayPayments->getTopUsers($yesterdayDate->toDateString()),
      'products' => $products,
      'refundedMtdMap' => $refundedMtdMap,
      'salesRefundedMtd' => $salesRefundedMtd,
    ], $calculatedData), $date));
  }

  public function addToDB($result, $date, $type, $groupParam, $valueParam) {
    foreach ($result as $item) {
      $this->dataToBeSaved[] = [
        'type' => $type,
        'sales_date' => $date,
        'group_type' => $item->$groupParam,
        'value' => intval($item->$valueParam),
        'created_at' => Carbon::now()->toDateTimeString()
      ];
    }
  }
}
