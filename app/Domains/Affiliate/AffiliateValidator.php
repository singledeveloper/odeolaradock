<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/16/17
 * Time: 12:08 AM
 */

namespace Odeo\Domains\Affiliate;

use Odeo\Domains\Affiliate\Repository\AffiliateRepository;
use Odeo\Domains\Constant\Affiliate;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;

class AffiliateValidator {

  private $affiliateRepo;

  public function __construct() {
    $this->affiliateRepo = app()->make(AffiliateRepository::class);
  }

  public function guard(PipelineListener $listener, $data) {
    if (!($aff = $this->affiliateRepo->findByCredential($data['mid'], $data['secret_key']))) {
      return $listener->response(401, authDebug('MID not match with SECRET.', $data['mid']));
    }

    if (app()->environment() == 'production' && $aff->whitelist != null) {
      $whitelist = json_decode($aff->whitelist, true);
      if (count($whitelist) > 0 && !in_array($data['ip_address'], $whitelist))
        return $listener->response(401, "IP Address not registered, current IP is " . $data['ip_address']);
    }

    if (isset($data['api_type']) && $data['api_type'] != Affiliate::API_TYPE_GET && $aff->api_type != $data['api_type']) {
      return $listener->response(401, "You can't access this API, please check your current API type.");
    }

    $user = $aff->user;
    if ($user->status != UserStatus::OK || $user->login_counts == UserStatus::LOGIN_BLOCKED_COUNT) {
      return $listener->response(400, trans('errors.account_blocked'));
    }

    return $listener->response(200, [
      'user_id' => $aff->user_id,
      'type' => UserType::AFFILIATE,
      'api_type' => $aff->api_type,
      'purchase_preferred_store_id' => $user->purchase_preferred_store_id
    ]);
  }
}
