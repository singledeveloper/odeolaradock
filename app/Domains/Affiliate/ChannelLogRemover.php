<?php

namespace Odeo\Domains\Affiliate;

use Odeo\Domains\Core\PipelineListener;

class ChannelLogRemover {

  private $channelLogs;

  public function __construct() {
    $this->channelLogs = app()->make(\Odeo\Domains\Affiliate\Repository\ChannelLogRepository::class);
  }

  public function removeLog(PipelineListener $listener, $data) {
    $this->channelLogs->getModel();
    $this->channelLogs->deleteById($data['channel_log_id']);
    return $listener->response(200);
  }

}
