<?php

namespace Odeo\Domains\Affiliate;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class ChannelLogSelector implements SelectorListener {

  private $channelLogs;

  public function __construct() {
    $this->channelLogs = app()->make(\Odeo\Domains\Affiliate\Repository\ChannelLogRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $channelLog = [];
    if($item->id) $channelLog["channel_log_id"] = $item->id;
    if($item->from) $channelLog["from"] = $item->from;
    if($item->type) $channelLog["type"] = $item->type;
    if($item->description) $channelLog["description"] = $item->description;
    if($item->duplication_counts) $channelLog["duplication_counts"] = $item->duplication_counts;
    if($item->created_at) $channelLog["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    if($item->updated_at) $channelLog["updated_at"] = $item->updated_at->format("Y-m-d H:i:s");
    return $channelLog;
  }

  public function getAllLogs(PipelineListener $listener, $data) {
    $this->channelLogs->normalizeFilters($data);

    $channelLogs = [];
    foreach ($this->channelLogs->gets() as $item) {
      $channelLogs[] = $this->_transforms($item, $this->channelLogs);
    }

    if (sizeof($channelLogs) > 0) {
      return $listener->response(200, array_merge(
        ["channelLogs" => $this->_extends($channelLogs, $this->channelLogs)],
        $this->channelLogs->getPagination()
      ));
    }
    return $listener->response(204, ["channelLogs" => []]);
  }

  public function findLog(PipelineListener $listener, $data) {
    $this->channelLogs->normalizeFilters($data);
    
    if($channelLogs = $this->channelLogs->findSame($data["from"], $data["type"], $data["description"])){
      return $listener->response(200, ["channelLogs" => $channelLogs]);
    }
    return $listener->response(204, ["channelLogs" => []]);
  }

}
