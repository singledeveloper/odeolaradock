<?php

namespace Odeo\Domains\Affiliate;

use Odeo\Domains\Core\PipelineListener;

class ChannelLogUpdater {

  private $channelLogs;

  public function __construct() {
    $this->channelLogs = app()->make(\Odeo\Domains\Affiliate\Repository\ChannelLogRepository::class);
  }

  public function updateLog(PipelineListener $listener, $data){
    if ($channelLog = $this->channelLogs->findById($data["channel_log_id"])) {
      $channelLog->description = $data["description"];
      $this->channelLogs->save($channelLog);
      return $listener->response(200);
    }
    return $listener->response(204);
  }

}
