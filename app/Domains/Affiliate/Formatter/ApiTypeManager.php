<?php

namespace Odeo\Domains\Affiliate\Formatter;

use Odeo\Domains\Constant\Affiliate;

class ApiTypeManager {

  public function setPath($type) {
    switch ($type) {
      case Affiliate::API_TYPE_XMLRPC:
        return app()->make(\Odeo\Domains\Affiliate\Formatter\ApiType\AffiliateXmlRpcFormatter::class);
      case Affiliate::API_TYPE_XML:
        return app()->make(\Odeo\Domains\Affiliate\Formatter\ApiType\AffiliateXmlFormatter::class);
      case Affiliate::API_TYPE_GET:
        return app()->make(\Odeo\Domains\Affiliate\Formatter\ApiType\AffiliateGetFormatter::class);
      default:
        return '';
    }
  }

}
