<?php

namespace Odeo\Domains\Affiliate\Jobs;

use Odeo\Domains\Constant\Affiliate;
use Odeo\Domains\Constant\Supplier;
use Odeo\Jobs\Job;

class AffiliateNotifier extends Job  {

  private $data, $affiliates;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;

    $this->affiliates = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateRepository::class);
  }

  public function handle() {
    try {

      $aff = $this->affiliates->findByUserId($this->data['user_id']);

      $client = new \GuzzleHttp\Client([
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ],
        'headers' => [
          'User-Agent' => 'ODEO'
        ],
        'timeout' => 10

      ]);

      $signature = \hash("sha256", $this->data['order_id'] . $aff->user_id . $aff->secret_key . $this->data['status']);

      $request = [
        'order_id' => $this->data['order_id'],
        'message' => $this->data['message'],
        'signature' => $signature
      ];

      if ($aff->api_type != Affiliate::API_TYPE_JSON) {
        $apiTypeManager = app()->make(\Odeo\Domains\Affiliate\Formatter\ApiTypeManager::class)->setPath($aff->api_type);
        $request = $apiTypeManager->constructNotify($request, $this->data['status'] == 'REFUNDED' ? Supplier::RC_FAIL : Supplier::RC_OK);
      } else {
        if (isset($this->data['additional_data'])) {
          $request = array_merge($request, $this->data['additional_data']);
        }
        $request['status'] = $this->data['status'];
      }


      if ($aff->api_type == Affiliate::API_TYPE_GET)
        $response = $client->request('GET', $aff->notify_url . '?' . $request);
      else $response = $client->request('POST', $aff->notify_url, [
        ($aff->api_type == Affiliate::API_TYPE_JSON ? 'json' : 'body') => $request
      ]);

      $reason = 'SUCCESS';
      $statusCode = $response->getStatusCode();

      if ($statusCode != 200) {
        $reason = 'FAIL';
      }

    } catch (\Exception $exception) {
      if (!isset($request)) $request = null;
      $statusCode = $exception->getCode();
      $reason = $exception->getMessage();
    }

    if (!isset($this->data['save_log'])) {
      $affiliateNotifyLogs = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateNotifyLogRepository::class);
      $log = $affiliateNotifyLogs->getNew();
      $log->user_id = $this->data['user_id'];
      $log->order_id = $this->data['order_id'];
      $log->status = $this->data['status'];
      $log->response_status_code = $statusCode;
      $log->response_reason = $reason;
      if (isset($aff)) {
        if ($aff->api_type == Affiliate::API_TYPE_JSON) $log->request_data = json_encode($request);
        else if ($aff->api_type == Affiliate::API_TYPE_GET) $log->request_data = http_build_query($request);
        else $log->request_data = $request;
      }
      else $log->request_data = $request;
      $log->notify_url = isset($aff) && isset($aff->notify_url) ? $aff->notify_url : null;
      $affiliateNotifyLogs->save($log);
    } else {
      clog('affiliate_notify', $reason);
      clog('affiliate_notify', serialize($response));
      clog('affiliate_notify', serialize($response->getBody()->getContents()));
    }

  }
}
