<?php

namespace Odeo\Domains\Affiliate\Jobs;

use Odeo\Domains\Constant\Affiliate;
use Odeo\Jobs\Job;

class AffiliateNotifyBroadcaster extends Job  {

  private $apiType, $notifyUrl, $request;

  public function __construct($apiType, $notifyUrl, $request) {
    parent::__construct();
    $this->apiType = $apiType;
    $this->notifyUrl = $notifyUrl;
    $this->request = $request;

  }

  public function handle() {

    try {
      $client = new \GuzzleHttp\Client([
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ],
        'headers' => [
          'User-Agent' => 'ODEO'
        ]
      ]);

      if ($this->apiType == Affiliate::API_TYPE_GET)
        $response = $client->request('GET', $this->notifyUrl . '?' . $this->request['body']);
      else $response = $client->request('POST', $this->notifyUrl, $this->request);

      $reason = 'SUCCESS';
      $statusCode = $response->getStatusCode();

      if ($statusCode != 200) {
        $reason = 'FAIL';
      }
    }
    catch (\Exception $exception) {
      $statusCode = $exception->getCode();
      $reason = $exception->getMessage();
    }

    clog('affiliate_broadcast', $this->notifyUrl . ': ' . json_encode($this->request));
    clog('affiliate_broadcast', $statusCode . ': ' . $reason);

  }
}
