<?php

namespace Odeo\Domains\Affiliate\Jobs;

use Odeo\Domains\Constant\Affiliate;
use Odeo\Domains\Constant\InlineTelegram;
use Odeo\Jobs\Job;

class AffiliateTelegramBroadcaster extends Job  {

  private $chatId, $message;

  public function __construct($chatId, $message) {
    parent::__construct();
    $this->chatId = $chatId;
    $this->message = $message;
  }

  public function handle() {

    $telegramManager = app()->make(\Odeo\Domains\Vendor\Telegram\TelegramManager::class);
    $telegramManager->initialize($this->chatId, InlineTelegram::CORE_TOKEN);
    $telegramManager->reply($this->message);

  }
}
