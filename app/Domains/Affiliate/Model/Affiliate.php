<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/16/17
 * Time: 12:10 AM
 */

namespace Odeo\Domains\Affiliate\Model;


use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class Affiliate extends Entity {

  function user() {
    return $this->belongsTo(User::class);
  }

}