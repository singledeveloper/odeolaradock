<?php

namespace Odeo\Domains\Affiliate\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class AffiliateInquiryFee extends Entity {

  function user() {
    return $this->belongsTo(User::class);
  }

}