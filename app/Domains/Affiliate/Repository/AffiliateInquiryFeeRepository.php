<?php

namespace Odeo\Domains\Affiliate\Repository;

use Odeo\Domains\Affiliate\Model\AffiliateInquiryFee;
use Odeo\Domains\Core\Repository;

class AffiliateInquiryFeeRepository extends Repository {

  public function __construct(AffiliateInquiryFee $affiliateInquiryFee) {
    $this->model = $affiliateInquiryFee;
  }

  public function findByUserId($userId) {
    return $this->model->where('user_id', $userId)->first();
  }
}