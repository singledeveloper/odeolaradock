<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/16/17
 * Time: 12:11 AM
 */

namespace Odeo\Domains\Affiliate\Repository;


use Odeo\Domains\Affiliate\Model\Affiliate;
use Odeo\Domains\Affiliate\Model\AffiliateNotifyLog;
use Odeo\Domains\Core\Repository;

class AffiliateNotifyLogRepository extends Repository {

  public function __construct(AffiliateNotifyLog $affiliateNotifyLog) {
    $this->model = $affiliateNotifyLog;
  }

}