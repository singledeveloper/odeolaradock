<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/16/17
 * Time: 12:11 AM
 */

namespace Odeo\Domains\Affiliate\Repository;


use Odeo\Domains\Affiliate\Model\Affiliate;
use Odeo\Domains\Core\Repository;

class AffiliateRepository extends Repository {

  public function __construct(Affiliate $affiliate) {
    $this->model = $affiliate;
  }

  public function findByCredential($userId, $secretKey) {
    return $this->model
      ->where('user_id', $userId)
      ->where('secret_key', $secretKey)
      ->first();
  }

  public function findByUserId($userId) {
    return $this->model
      ->where('user_id', $userId)
      ->first();
  }

  public function getAllNotifies() {
    return $this->model->whereNotNull('notify_url')->orderBy('id', 'asc')->get();
  }

  public function gets() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel()->with(['user'])->whereNotNull('notify_url');

    $filters = isset($filters['search']) ? $filters['search'] : $filters;

    if (isset($filters['user_id'])) {
      $query = $query->where('user_id', $filters['user_id']);
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }
}