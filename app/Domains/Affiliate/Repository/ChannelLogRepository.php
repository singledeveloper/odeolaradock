<?php

namespace Odeo\Domains\Affiliate\Repository;

use Odeo\Domains\Affiliate\Model\ChannelLog;
use Odeo\Domains\Core\Repository;

class ChannelLogRepository extends Repository {

  public function __construct(ChannelLog $channelLog) {
    $this->model = $channelLog;
  }

  public function gets() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel();
    if (isset($filters['search'])) {
      if (isset($filters['search']['type'])) {
        $query = $query->where('type', 'like', '%' . $filters['search']['type'] . '%');
      }
      if (isset($filters['search']['from'])) {
        $query = $query->where('from', 'like', '%' . $filters['search']['from'] . '%');
      }
      if (isset($filters['search']['description'])) {
        $query = $query->where('description', 'like', '%' . $filters['search']['description'] . '%');
      }
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function findSame($from, $type, $description) {
    return $this->model->where('description', $description)
      ->where('from', $from)->where('type', $type)->first();
  }

  public function findLastByFromAndType($from, $type) {
    return $this->model->where('from', $from)->where('type', $type)->orderBy('id', 'desc')->first();
  }
}
