<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/30/17
 * Time: 5:54 PM
 */

namespace Odeo\Domains\Affiliate;

use GuzzleHttp\Client;
use Odeo\Domains\Constant\Affiliate;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Core\PipelineListener;
use Illuminate\Support\Facades\Hash;

class UserAffiliateConfigurator {

  private $affiliates;

  public function __construct() {
    $this->affiliates = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateRepository::class);
  }

  public function editFromAdminPanel(PipelineListener $listener, $data) {
    if ($affiliate = $this->affiliates->findById($data['affiliate_id'])) {
      $affiliate->broadcast_email = $data['email'];
      $this->affiliates->save($affiliate);

      return $listener->response(200);
    }
    return $listener->response(400, 'User not exist');
  }

  public function getWhiteList(PipelineListener $listener, $data) {

    $affiliate = $this->affiliates->findByUserId($data['auth']['user_id']);

    if (!$affiliate) return $listener->response(204);

    $whitelists = json_decode($affiliate->whitelist, true);

    if (!count($whitelists)) return $listener->response(204);

    $transformedWhitelists = [];

    foreach ($whitelists as $key => $ip) {
      $transformedWhitelists[] = [
        'whitelist_id' => ++$key,
        'ip_address' => $ip
      ];
    }

    return $listener->response(200, $transformedWhitelists);

  }

  public function addWhiteList(PipelineListener $listener, $data) {

    if (!($affiliate = $this->affiliates->findByUserId($data['auth']['user_id']))) {
      $affiliate = $this->affiliates->getNew();
      $affiliate->user_id = $data['auth']['user_id'];
    }

    $whitelists[] = $data['ip_address'];

    if ($affiliate->whitelist === null || $affiliate->whitelist === "") {
      $affiliate->whitelist = \json_encode([]);
    }

    $whitelists = array_merge(\json_decode($affiliate->whitelist, true), $whitelists);

    $affiliate->whitelist = \json_encode($whitelists);

    $this->affiliates->save($affiliate);

    return $listener->response(200);

  }


  public function removeWhiteList(PipelineListener $listener, $data) {

    $affiliate = $this->affiliates->findByUserId($data['auth']['user_id']);

    $whitelists = \json_decode($affiliate->whitelist, true);

    unset($whitelists[--$data['whitelist_id']]);

    $affiliate->whitelist = \json_encode($whitelists);

    $this->affiliates->save($affiliate);

    return $listener->response(200);

  }

  public function setApiType(PipelineListener $listener, $data) {
    if (!($affiliate = $this->affiliates->findByUserId($data['auth']['user_id']))) {
      $affiliate = $this->affiliates->getNew();
      $affiliate->user_id = $data['auth']['user_id'];
    }

    $affiliate->api_type = $data['api_type'];

    $this->affiliates->save($affiliate);
    return $listener->response(200);
  }

  public function getSecretKey(PipelineListener $listener, $data) {

    if (!($affiliate = $this->affiliates->findByUserId($data['auth']['user_id']))) {
      $affiliate = $this->affiliates->getNew();
      $affiliate->user_id = $data['auth']['user_id'];
      $data['regenerate_key'] = true;
    }

    if (isset($data['regenerate_key']) || $affiliate->secret_key == null) {
      $affiliate->secret_key = Hash::make($affiliate->user_id . microtime());
      $this->affiliates->save($affiliate);
    }

    return $listener->response(200, [
      'secret_key' => $affiliate->secret_key,
      'api_type' => $affiliate->api_type,
      'api_type_options' => [Affiliate::API_TYPE_JSON, Affiliate::API_TYPE_XML],
      'tnc' => [
        [
          '1_text' => trans('affiliate/conf.tnc_secret_key_1'),
        ],
        [
          '1_text' => trans('affiliate/conf.tnc_secret_key_2'),
        ],
        [
          '1_text' => trans('affiliate/conf.tnc_secret_key_3'),
        ],
      ]
    ]);

  }

  public function getNotifyUrl(PipelineListener $listener, $data) {

    $affiliate = $this->affiliates->findByUserId($data['auth']['user_id']);

    if (!$affiliate) return $listener->response(204);

    if ((!$affiliate->notify_url || strlen($affiliate->notify_url) == 0) && (!$affiliate->staging_notify_url || strlen($affiliate->staging_notify_url))) {
      return $listener->response(204);
    }

    return $listener->response(200, [
      'notify_url' => $affiliate->notify_url,
      'staging_notify_url' => $affiliate->staging_notify_url
    ]);

  }

  public function setNotifyUrl(PipelineListener $listener, $data) {

    if (!($affiliate = $this->affiliates->findByUserId($data['auth']['user_id']))) {
      $affiliate = $this->affiliates->getNew();
      $affiliate->user_id = $data['auth']['user_id'];
    }

    if (substr($data['notify_url'], 0, 4) !== "http") {
      $data['notify_url'] = (function () {

          if (app()->environment() == 'production') {
            return 'https://';
          }
          return 'http://';

        })() . $data['notify_url'];
    }

    if (substr($data['staging_notify_url'], 0, 4) !== "http") {
      $data['staging_notify_url'] = (function () {

          if (app()->environment() == 'production') {
            return 'https://';
          }
          return 'http://';

        })() . $data['staging_notify_url'];
    }

    $forbiddenUrls = [
      'odeo.co.id',
      'odeo.id',
      'odeotest.com',
      'staging.odeo.co.id',
      '52.74.36.212',
      '52.220.174.28',
      '54.169.4.192',
      '54.255.180.90',
      '54.255.237.244',
      '52.76.124.88',
      'jabber.id',
      '127.0.0.1',
      'localhost',
      'nginx',
    ];

    if (filter_var($data['notify_url'], FILTER_VALIDATE_URL) === false ||
      filter_var($data['staging_notify_url'], FILTER_VALIDATE_URL) === false) {
      return $listener->response(400, 'Invalid URL');
    }

    if (arrayContain(strtolower($data['notify_url']), $forbiddenUrls) ||
      arrayContain(strtolower($data['staging_notify_url']), $forbiddenUrls)) {
      return $listener->response(400, 'URL not allowed');
    }

    $affiliate->notify_url = $data['notify_url'];
    $affiliate->staging_notify_url = $data['staging_notify_url'];

    $this->affiliates->save($affiliate);

    return $listener->response(200);

  }

  public function testSuccessNotify(PipelineListener $listener, $data) {

    $affiliate = $this->affiliates->findByUserId($data['auth']['user_id']);

    if (!$affiliate || !$affiliate->staging_notify_url) return $listener->response(400, 'Staging notify url must be set');

    $client = new Client([
      'headers' => [
        'User-Agent' => 'ODEO'
      ]
    ]);

    try {

      $variant = [trans('pulsa.inline.success', [
        'order_id' => InlineJabber::EXAMPLE_ORDER_ID,
        'item_name' => InlineJabber::EXAMPLE_ITEM_NAME,
        'number' => InlineJabber::EXAMPLE_NUMBER,
        'sn' => InlineJabber::EXAMPLE_SN,
        'sisa_saldo' => InlineJabber::EXAMPLE_REMAIN_OCASH_SUCCESS
      ]), trans('pulsa.inline.success', [
        'order_id' => InlineJabber::EXAMPLE_ORDER_ID,
        'item_name' => InlineJabber::EXAMPLE_ITEM_NAME_PLN,
        'number' => InlineJabber::EXAMPLE_NUMBER_PLN,
        'sn' => InlineJabber::EXAMPLE_SN_PLN,
        'sisa_saldo' => InlineJabber::EXAMPLE_REMAIN_OCASH_SUCCESS
      ])];

      $client->post($affiliate->staging_notify_url, [
        'json' => [
          'order_id' => 1,
          'status' => 'COMPLETED',
          'message' => $variant[array_rand($variant)],
          'signature' => \hash("sha256", 1 . $affiliate->user_id . $affiliate->secret_key . 'COMPLETED')
        ]
      ]);

    } catch (\Exception $exception) {
      return $listener->response(400, 'Failed to notify ' . $affiliate->staging_notify_url . ' Log: ' . $exception->getMessage());
    }
    return $listener->response(200);

  }

  public function testFailNotify(PipelineListener $listener, $data) {

    $affiliate = $this->affiliates->findByUserId($data['auth']['user_id']);

    if (!$affiliate || !$affiliate->staging_notify_url) return $listener->response(400, 'Staging notify url must be set');

    $client = new Client([
      'headers' => [
        'User-Agent' => 'ODEO'
      ]
    ]);

    try {

      $client->post($affiliate->staging_notify_url, [
        'json' => [
          'order_id' => 1,
          'status' => 'REFUNDED',
          'message' => trans('pulsa.inline.refund', [
            'order_id' => InlineJabber::EXAMPLE_ORDER_ID,
            'item_name' => InlineJabber::EXAMPLE_ITEM_NAME,
            'number' => InlineJabber::EXAMPLE_NUMBER,
            'sisa_saldo' => InlineJabber::EXAMPLE_REMAIN_OCASH_REFUND
          ]),
          'signature' => \hash("sha256", 1 . $affiliate->user_id . $affiliate->secret_key . 'REFUNDED')
        ]
      ]);

    } catch (\Exception $exception) {
      return $listener->response(400, 'Failed to notify ' . $affiliate->staging_notify_url . ' Log: ' . $exception->getMessage());
    }
    return $listener->response(200);

  }


  public function testNewPriceNotify(PipelineListener $listener, $data) {

    $affiliate = $this->affiliates->findByUserId($data['auth']['user_id']);

    if (!$affiliate || !$affiliate->staging_notify_url) return $listener->response(400, 'staging notify url must be set');

    $client = new Client([
      'headers' => [
        'User-Agent' => 'ODEO'
      ]
    ]);

    try {
      $client->post($affiliate->staging_notify_url, [
        'json' => [
          'status' => 'BROADCAST_NEW_PRICE',
          'messages' => 'XXX',
          'new_prices' => [
            'TSEL5' => 5000,
            'TSEL10' => 10000
          ],
          'signature' => \hash("sha256", $affiliate->user_id . $affiliate->secret_key . 'BROADCAST_NEW_PRICE')
        ]
      ]);

    } catch (\Exception $exception) {
      return $listener->response(400, 'Failed to notify ' . $affiliate->staging_notify_url . ' Log: ' . $exception->getMessage());

    }
    return $listener->response(200);

  }

}
