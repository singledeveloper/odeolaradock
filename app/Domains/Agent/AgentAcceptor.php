<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/9/17
 * Time: 2:55 PM
 */

namespace Odeo\Domains\Agent;


use Odeo\Domains\Constant\AgentConfig;
use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Subscription\ManageInventory\Jobs\PopulateUserGroupPrice;
use Odeo\Domains\Supply\SupplyInventoryGroupEditor;

class AgentAcceptor {

  private $agents, $notifications, $users;

  public function __construct() {
    $this->agents = app()->make(\Odeo\Domains\Agent\Repository\AgentRepository::class);
    $this->agentHistories = app()->make(\Odeo\Domains\Agent\Repository\AgentHistoryRepository::class);
    $this->notifications = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->userReferralCodes = app()->make(\Odeo\Domains\Marketing\Userreferral\Repository\UserReferralCodeRepository::class);
  }

  public function accept(PipelineListener $listener, $data) {

    $agent = $this->agents->findById($data['agent_id']);
    $storePreffered = $this->stores->findById($agent->store_referred_id);
    $bonus = 0;
    $isAuto = isset($data['is_auto']) && $data['is_auto'];

    if (!$isAuto && $agent->user_referred_id != $data['auth']['user_id']) {
      return $listener->response(400, 'you are not authorized to accept this agent.');
    }

    if ($agent->status != AgentConfig::PENDING) {
      return $listener->response(400, 'this agent has either accepted or removed');
    }

    $agent->status = AgentConfig::ACCEPTED;
    $agent->joined_at = datenow();
    $this->agents->save($agent);

    $user = $this->users->findById($agent->user_id);
    $user->purchase_preferred_store_id = $agent->store_referred_id;
    $this->users->save($user);

    $storeOwner = $this->stores->findOwner($agent->store_referred_id);

    if (!$this->userReferralCodes->findByUserId($agent->user_id)) {
      $userReferralCode = $this->userReferralCodes->getNew();
      $userReferralCode->user_id = $agent->user_id;
      $userReferralCode->user_referred_id = $agent->user_referred_id;
      $userReferralCode->store_referred_id = $agent->store_referred_id;
      $userReferralCode->referral_code = $agent->inputted_master_code;

      $this->userReferralCodes->save($userReferralCode);
    }

    $listener->pushQueue(new PopulateUserGroupPrice([
      'plan_id' => $storePreffered->plan_id,
      'user_id' => $agent->user_id,
      'store_id' => $agent->store_referred_id,
      'group_price_type' => $storeOwner->user_id == $agent->user_id ? InventoryGroupPrice::SELF : InventoryGroupPrice::AGENT_DEFAULT
    ]));

    $this->notifications->setup($agent->user_id, NotificationType::CHECK);
    $this->notifications->agentAccepted($agent->inputted_master_code, $bonus);

    $listener->pushQueue($this->notifications->queue());

    $agentSupplyDefaults = app()->make(\Odeo\Domains\Agent\Repository\AgentSupplyDefaultRepository::class);
    if ($default = $agentSupplyDefaults->findByStoreId($agent->store_referred_id)) {
      $listener->addNext(new Task(SupplyInventoryGroupEditor::class, 'editAgentGroup', [
        'supply_group_id' => $default->supply_group_id,
        'set_as_default' => true
      ]));
    }

    return $listener->response(200, [
      'bonus' => app()->make(\Odeo\Domains\Transaction\Helper\Currency::class)->formatPrice($bonus)
    ]);
  }


  public function reject(PipelineListener $listener, $data) {

    $agent = $this->agents->findById($data['agent_id']);


    if ($agent->referred_user_id == $data['auth']['user_id']) {
      return $listener->response(400, 'you are not authorized to accept this agent.');
    }

    if ($agent->status != AgentConfig::PENDING) {
      return $listener->response(400, 'this agent has either accepted or removed');
    }

    $agent->status = AgentConfig::AVAILABLE;
    $this->agents->save($agent);

    $this->notifications->setup($agent->user_id, NotificationType::CHECK, NotificationGroup::ACCOUNT);
    $this->notifications->agentRejected($agent->inputted_master_code);

    $listener->pushQueue($this->notifications->queue());

    return $listener->response(200);
  }

}
