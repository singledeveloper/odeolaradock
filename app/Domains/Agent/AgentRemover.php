<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/9/17
 * Time: 4:44 PM
 */

namespace Odeo\Domains\Agent;

use Carbon\Carbon;
use Odeo\Domains\Constant\AgentConfig;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Core\PipelineListener;

class AgentRemover {

  private $agents, $notifications, $users, $agentHistories, $agentPriorities;

  public function __construct() {
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->agents = app()->make(\Odeo\Domains\Agent\Repository\AgentRepository::class);
    $this->agentHistories = app()->make(\Odeo\Domains\Agent\Repository\AgentHistoryRepository::class);
    $this->agentPriorities = app()->make(\Odeo\Domains\Agent\Repository\AgentPriorityRepository::class);
    $this->notifications = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
  }

  public function remove(PipelineListener $listener, $data) {

    if ($data['agent_id'] >= 9000000) {
      return $listener->response(400, 'Could not remove agent, the agent should remove himself');
    }

    $agent = $this->agents->findById($data['agent_id']);


    if ($agent->referred_user_id == $data['auth']['user_id']) {
      return $listener->response(400, 'you are not authorized to remove this agent.');
    }

    if ($agent->status != AgentConfig::ACCEPTED) {
      return $listener->response(400, 'this agent has already removed');
    }

    $this->leaveAgencyAndSaveToHistory($agent);

    $me = $this->users->findById($data['auth']['user_id']);
    $this->notifications->setup($me->id, NotificationType::CHECK, NotificationGroup::ACCOUNT);
    $this->notifications->removeAgent($me->name);

    $listener->pushQueue($this->notifications->queue());

    return $listener->response(200);

  }


  public function agentRemoveMaster(PipelineListener $listener, $data) {
    if (!($agent = $this->agents->findByUserId($data['auth']['user_id']))) {
      return $listener->response(400, 'Could not found agent');
    }

    $this->updatePriorities($data['auth']['user_id'], $agent->store_referred_id);

    return $this->removeMaster($listener, $agent);
  }

  public function agentDeleteMaster(PipelineListener $listener, $data) {
    if (!($agent = $this->agents->findByUserIdAndStoreId($data['auth']['user_id'], $data['store_id']))) {
      return $listener->response(400, 'Could not found agent');
    }  

    $this->updatePriorities($data['auth']['user_id'], $data['store_id']);

    return $this->removeMaster($listener, $agent);
  }

  public function removeMaster($listener, $agent) {
    $this->leaveAgencyAndSaveToHistory($agent);

    $master = $this->users->findById($agent->user_id);
    $this->notifications->setup($agent->user_referred_id, NotificationType::CHECK, NotificationGroup::ACCOUNT);

    if ($agent->status == AgentConfig::ACCEPTED) {
      $this->notifications->agentRemoveMaster($master->name);
    } else {
      $this->notifications->agentCancelRequestMaster($master->name);
    }

    $listener->pushQueue($this->notifications->queue());

    return $listener->response(200);
  }

  private function updatePriorities($userId, $storeId) {
    $updatedAgentPriorities = array();
    $updatedPriorities = array();
    $now = Carbon::now();

    $agentPriorities = $this->agentPriorities->getPreferredPrioritiesByUserId($userId);

    // Set priorities related to the corresponding storeId to 0
    foreach($agentPriorities->where('store_referred_id', $storeId) as $agentPriority) {
      $updatedAgentPriorities[] = [
        'id' => $agentPriority->id,
        'priority' => 0,
        'is_selected' => false,
        'updated_at' => $now
      ];
      $updatedPriorities[$agentPriority->service_id] = $agentPriority->priority;
    }

    // Decrement all other priorities that is currently behind the to-be-removed store priority
    foreach ($agentPriorities as $agentPriority) {
      if (
        isset($updatedPriorities[$agentPriority->service_id]) && 
        $agentPriority->priority > $updatedPriorities[$agentPriority->service_id]
      ) {
        $updatedAgentPriorities[] = [
          'id' => $agentPriority->id,
          'priority' => $agentPriority->priority - 1,
          'is_selected' => $agentPriority->is_selected,
          'updated_at' => $now
        ];
      }
    }

    if (sizeof($updatedAgentPriorities) > 0) {
      $this->agentPriorities->updateBulk($updatedAgentPriorities);
    }
  }

  private function leaveAgencyAndSaveToHistory($agent) {

    $agentHistory = $this->agentHistories->getNew();
    $agentHistory->agent_id = $agent->id;
    $agentHistory->user_referred_id = $agent->user_referred_id;
    $agentHistory->store_referred_id = $agent->store_referred_id;
    $agentHistory->requested_at = $agent->requested_at;
    $agentHistory->joined_at = $agent->joined_at;
    $agentHistory->inputted_master_code = $agent->inputted_master_code;

    $this->agentHistories->save($agentHistory);

    $agent->status = AgentConfig::AVAILABLE;
    $agent->joined_at = null;
    $agent->requested_at = null;

    $this->agents->save($agent);

    $user = $this->users->findById($agent->user_id);
    $user->purchase_preferred_store_id = null;
    $this->users->save($user);
  }


}