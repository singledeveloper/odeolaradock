<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/7/17
 * Time: 3:28 PM
 */

namespace Odeo\Domains\Agent\Model;


use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Store;

class Agent extends Entity {

  public function user() {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function referredUser() {
    return $this->belongsTo(User::class, 'user_referred_id');
  }

  public function referredStore() {
    return $this->belongsTo(Store::class);
  }

}