<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/7/17
 * Time: 3:28 PM
 */

namespace Odeo\Domains\Agent\Repository;

use Carbon\Carbon;
use Odeo\Domains\Agent\Model\Agent;
use Odeo\Domains\Constant\AgentConfig;
use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Core\Repository;

class AgentRepository extends Repository {

  public function __construct(Agent $agent) {
    $this->model = $agent;
  }

  public function getMyAgents() {

    $filters = $this->getFilters();

    $query = function ($selectRaw) use ($filters) {
      return
        "WITH user_servers AS (
        SELECT
          user_stores.user_id,
          min(store_inventory_vendors.created_at) as server_joined_at
        FROM store_inventory_vendors
          JOIN store_inventories ON store_inventory_vendors.store_inventory_id = store_inventories.id
          JOIN user_stores ON user_stores.store_id = store_inventories.store_id
        WHERE store_inventory_vendors.vendor_id = " . (int)$filters['store_id'] . " AND
              user_stores.type = 'owner'
        GROUP BY user_id
      ), user_agents AS (
        SELECT
          user_id,
          id as agent_id,
          joined_at as agent_joined_at
        FROM agents
        WHERE store_referred_id = " . (int)$filters['store_id'] . " and
          status = '50000'
      )
      SELECT " . implode(',', $selectRaw) . "
      FROM users
        LEFT JOIN user_agents on user_agents.user_id = users.id
        LEFT JOIN user_servers ON user_servers.user_id = users.id
        WHERE user_agents.agent_joined_at is not null or user_servers.server_joined_at is not null";
    };

    $total = array_first(\DB::select(\DB::raw($query([
      'count(users.id) as total'
    ]))))->total;

    $result = \DB::select(\DB::raw($query([
        'users.id as user_id',
        'users.telephone',
        'users.name',
        'user_agents.agent_id',
        'user_agents.agent_joined_at',
        'user_servers.server_joined_at'
      ]) . " ORDER BY user_agents.agent_joined_at desc, user_servers.server_joined_at desc
        LIMIT " . $filters['limit'] . " OFFSET " . $filters['offset']));

    return [
      'pagination' => [
        'count' => $total,
        'limit' => (int)$filters['limit'],
        'offset' => (int)$filters['offset'],
      ],
      'result' => $result
    ];
  }

  public function getRequestedAgents() {

    $filters = $this->getFilters();

    $query = $this->model->with(['user'])
      ->where('status', AgentConfig::PENDING);

    if (!isAdmin()) {
      $query->where('store_referred_id', $filters['store_id']);
    }

    return $this->getResult($query->orderBy('requested_at', 'desc'));

  }


  public function isAccepted($userId) {
    return $this->model
      ->where('user_id', $userId)
      ->where('status', AgentConfig::ACCEPTED)
      ->first();
  }

  public function getExpired() {
    return $this->model
      ->where('status', AgentConfig::PENDING)
      ->where('requested_at', '<', Carbon::now()->subDay(1))
      ->get();
  }

  public function findByUserId($userId) {
    return $this->model
      ->where('user_id', $userId)
      ->orderBy('created_at', 'desc')
      ->first();
  }

  public function findByUserIdAndStoreId($userId, $storeId) {
    return $this->model
      ->where('user_id', $userId)
      ->where('store_referred_id', $storeId)
      ->whereIn('status', [AgentConfig::PENDING, AgentConfig::ACCEPTED])
      ->first();
  }

  public function findByUserIdAndMasterCode($userId, $masterCode) {
    return $this->model
      ->where('user_id', $userId)
      ->where('inputted_master_code', 'ilike', $masterCode)
      ->first();
  }

  public function findAgentFromStore($userId, $storeId) {
    return $this->model
      ->where('user_id', $userId)
      ->where('store_referred_id', $storeId)
      ->where('status', AgentConfig::ACCEPTED)
      ->first();
  }

  public function getAgentSupplyGroup($agentIds) {
    return
      $this->model->join('store_inventory_group_price_users', 'store_inventory_group_price_users.user_id', '=', 'agents.user_id')
      ->join('store_inventory_group_prices', 'store_inventory_group_prices.id', '=', 'store_inventory_group_price_users.store_inventory_group_price_id')
      ->leftJoin('store_inventory_group_prices as new_group_prices', 'new_group_prices.id', '=', 'store_inventory_group_price_users.new_store_inventory_group_price_id')
      ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_group_prices.store_inventory_id')
      ->whereNotNull('store_inventory_group_prices.supply_group_id')
      ->whereIn('agents.id', $agentIds)
      ->where('store_inventory_group_prices.type', InventoryGroupPrice::CUSTOM)
      ->select('agents.id', 'store_inventory_group_prices.name', 'new_group_prices.name as new_name')
      ->get();
  }

  public function getMastersByUserId($userId) {
    return $this->model
      ->select('store_referred_id', 'name', 'inputted_master_code', 'agents.status', 'stores.status as store_status')
      ->join('stores', 'stores.id', '=', 'store_referred_id')
      ->where('user_id', $userId)
      ->whereIn('agents.status', [AgentConfig::PENDING, AgentConfig::ACCEPTED])
      ->orderBy('agents.updated_at', 'desc')
      ->get();
  }
}