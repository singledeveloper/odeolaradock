<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/7/17
 * Time: 3:28 PM
 */

namespace Odeo\Domains\Agent\Repository;

use Odeo\Domains\Agent\Model\StoreAgentMasterSetting;
use Odeo\Domains\Core\Repository;

class StoreAgentMasterSettingRepository extends Repository {

  public function __construct(StoreAgentMasterSetting $storeAgentMasterSetting) {
    $this->model = $storeAgentMasterSetting;
  }

  public function findByStoreId($storeId) {
    return $this->model
      ->where('store_id', $storeId)
      ->first();
  }

}