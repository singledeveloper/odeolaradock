<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/9/17
 * Time: 4:10 PM
 */

namespace Odeo\Domains\Agent\Scheduler;


use Odeo\Domains\Constant\AgentConfig;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;

class AgentRequestCanceller {


  private $agents, $notifications;

  public function __construct() {
    $this->agents = app()->make(\Odeo\Domains\Agent\Repository\AgentRepository::class);
    $this->notifications = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);

  }

  public function run() {

    $count = 0;

    foreach ($this->agents->getExpired() as $agent) {

      ++$count;

      $agent->status = AgentConfig::AVAILABLE;
      $this->agents->save($agent);

      $this->notifications->setup($agent->user_id, NotificationType::CHECK, NotificationGroup::ACCOUNT);
      $this->notifications->requestExpired($agent->inputted_master_code);

    }

    $count && dispatch($this->notifications->queue());

  }
}