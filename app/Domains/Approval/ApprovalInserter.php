<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/11/19
 * Time: 16.13
 */

namespace Odeo\Domains\Approval;

use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Approval\Repository\ApprovalPendingRequestRepository;
use Odeo\Domains\Core\PipelineListener;

class ApprovalInserter {

  function __construct() {
    $this->approvalPendingRequest = app()->make(ApprovalPendingRequestRepository::class);
  }

  public function createRequest(PipelineListener $listener, $data) {
    if (!Approval::isValidPath($data['path']))
      return $listener->response(400, 'Approval not valid');

    $request = $this->approvalPendingRequest->getNew();
    $request->raw_data = json_encode($data['raw_data']);
    $request->path = $data['path'];
    $request->requested_by = $data['auth']['user_id'];
    $request->status = Approval::CREATED;
    $this->approvalPendingRequest->save($request);

    return $listener->response(200, ['message' => 'Request created']);
  }

}