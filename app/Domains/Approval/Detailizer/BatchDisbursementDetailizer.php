<?php

namespace Odeo\Domains\Approval\Detailizer;

use Odeo\Domains\Constant\BulkPurchase;
use Odeo\Domains\Order\Repository\PulsaBulkRepository;

class BatchDisbursementDetailizer implements DetailizerContract {

  public function format($data) {
    return [];
  }

  public function description($data) {
    return $data->description;
  }

  public function cancel($data) {

  }

}