<?php

namespace Odeo\Domains\Approval\Detailizer;

interface DetailizerContract {

  public function format($data);

  public function description($data);

  public function cancel($data);

}