<?php

namespace Odeo\Domains\Approval\Detailizer;

use Odeo\Domains\Account\Repository\UserBankAccountRepository;
use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Transaction\Helper\Currency;

class WithdrawDetailizer implements DetailizerContract {

  private $currencyHelper;

  public function __construct() {
    $this->currencyHelper = app()->make(Currency::class);
  }

  private function formatRecurring($data) {
    return isset($data->transaction_type) && $data->transaction_type != 'immediate' ?
      Recurring::getTypeName($data->recurring_type .
        ($data->recurring_type == Recurring::TYPE_MONTH ? ('_' . $data->recurring_month) : '') .
        '_' . $data->recurring_time) : 'No Recurring';
  }

  public function format($data) {
    return [];
  }

  public function description($data) {
    $bankAccountRepo = app()->make(UserBankAccountRepository::class);
    $bankAccount = $bankAccountRepo->findById($data->bank_account_id);
    return $this->currencyHelper->formatPrice($data->cash->amount)['formatted_amount']
      . ' - ' . $bankAccount->bank->name
      . ' - ' . $bankAccount->account_name
      . ' - ' . $bankAccount->account_number
      . ' - ' . $this->formatRecurring($data);
  }

  public function cancel($data) {

  }

}