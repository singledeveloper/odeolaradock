<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/11/19
 * Time: 18.03
 */

namespace Odeo\Domains\Approval\Helper;


class Approval {

  const CREATED = '10000';
  const APPROVED = '20000';
  const FAILED = '40000';
  const PROCESSED = '50000';
  const CANCELLED = '90000';

  const PARAM_FILE = 'file';
  const PARAM_FILE_TYPE = 'file_type';

  const PATH_BANK_INQUIRY = 'bank_inquiry';
  const PATH_WITHDRAW = 'withdraw';
  const PATH_OCASH_TRANSFER = 'ocash_transfer';
  const PATH_BULK_PULSA = 'pulsa_bulk';
  const PATH_BATCH_DISBURSEMENT = 'batch_disbursement';

  const STATUS_CODE_TO_MESSAGE = [
    self::CREATED => 'api_pending',
    self::APPROVED => 'api_approved',
    self::FAILED => 'api_failed',
    self::PROCESSED => 'api_completed',
    self::CANCELLED => 'api_cancelled'
  ];

  public static function isValidPath($string) {
    return in_array(explode(':', $string)[0], [
      self::PATH_BANK_INQUIRY,
      self::PATH_WITHDRAW,
      self::PATH_OCASH_TRANSFER
    ]);
  }

  public static function isPathNeedApprover($string) {
    return in_array(explode(':', $string)[0], [
      self::PATH_OCASH_TRANSFER,
      self::PATH_WITHDRAW
    ]);
  }

  public static function getStatusMessage($statusCode) {
    if (isset(self::STATUS_CODE_TO_MESSAGE[$statusCode])) {
      return trans('status.' . self::STATUS_CODE_TO_MESSAGE[$statusCode]);
    }

    return trans('status.api_unknown') . ': ' . $statusCode;
  }

  public static function getPathName($path) {
    $names = [
      self::PATH_BANK_INQUIRY => 'Bank Inquiry',
      self::PATH_WITHDRAW => 'Bank Transfer',
      self::PATH_BULK_PULSA => 'Pulsa Bulk',
      self::PATH_OCASH_TRANSFER => 'OCash Transfer',
      self::PATH_BATCH_DISBURSEMENT => 'Batch Disbursement'
    ];
    if (isset($names[explode(':', $path)[0]]))
      return $names[explode(':', $path)[0]];
    return 'Unknown';
  }

}