<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 06/11/19
 * Time: 12.26
 */

namespace Odeo\Domains\Approval\Job;


use Carbon\Carbon;
use Odeo\Domains\Account\Registrar;
use Odeo\Domains\Accounting\CompanyTransactionInformation\Repository\CompanyTransactionAdditionalInformationRepository;
use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Approval\Repository\ApprovalPendingRequestRepository;
use Odeo\Domains\Constant\BankAccount;
use Odeo\Domains\Constant\BankTransferInquiries;
use Odeo\Domains\Constant\BulkPurchase;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\BatchDisbursementApprover;
use Odeo\Domains\Order\BulkPurchaseUpdater;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Maybank\Repository\BankMaybankInquiryRepository;
use Odeo\Domains\Transaction\CashTransfer;
use Odeo\Jobs\Job;

class ApproveBatchDisbursement extends Job {

  private $approvalPendingId, $approvalPendingRepo;

  public function __construct($approvalPendingId) {
    parent::__construct();
    $this->approvalPendingId = $approvalPendingId;
  }

  public function init() {
    $this->approvalPendingRepo = app()->make(ApprovalPendingRequestRepository::class);
  }

  public function handle() {
    $this->init();
    $pending = $this->approvalPendingRepo->findById($this->approvalPendingId);
    $pipeline = new Pipeline();
    $pipeline->add(new Task(BatchDisbursementApprover::class, 'approve'));
    $pipeline->enableTransaction();
    $pipeline->execute([
      'batch_disbursement_id' => $pending->reference_id,
      'auth' => [
        'user_id' => $pending->need_approver_from,
        'type' => 'seller'
      ]
    ]);

    $pending->processed_at = Carbon::now();
    if ($pipeline->fail()) {
      $pending->status = Approval::FAILED;
      $pending->user_message = $pipeline->errorMessage;
    }
    else $pending->status = Approval::PROCESSED;

    $this->approvalPendingRepo->save($pending);
  }

}