<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 06/11/19
 * Time: 12.26
 */

namespace Odeo\Domains\Approval\Job;


use Carbon\Carbon;
use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Approval\Repository\ApprovalPendingRequestRepository;
use Odeo\Domains\Constant\BankAccount;
use Odeo\Domains\Constant\BankTransferInquiries;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Maybank\Repository\BankMaybankInquiryRepository;
use Odeo\Jobs\Job;

class ManualInsertBankInquiry extends Job {

  private $approvalPendingId, $bank, $approvalPendingRepo;
  public function __construct($approvalPendingId, $bank) {
    parent::__construct();
    $this->approvalPendingId = $approvalPendingId;
    $this->bank = $bank;
  }

  public function init() {
    $this->approvalPendingRepo = app()->make(ApprovalPendingRequestRepository::class);
  }

  public function handle() {
    $this->init();
    $pending = $this->approvalPendingRepo->findById($this->approvalPendingId);

    switch ($this->bank) {
      case 'mandiri_giro':
        $this->insertMandiriGiroMutation($pending);
        break;
      case 'maybank':
        $this->insertMaybankMutation($pending);
        break;
    }
  }

  public function insertMandiriGiroMutation($approvalPending) {
    $mandiriGiroRepo = app()->make(BankMandiriGiroInquiryRepository::class);
    $rows = json_decode($approvalPending->raw_data);

    $newInquiries = [];
    foreach ($rows as $row) {
      $isCredit = $row->credit > 0;
      $referenceType = $isCredit ? 'note' : null;
      $reference = $isCredit ? BankTransferInquiries::REFERENCE_TYPE_UNRECOGNIZED : null;
      $newInquiries[] = [
        'date' => Carbon::parse($row->date),
        'bank_scrape_account_number_id' => BankAccount::MANDIRI_GIRO,
        'date_value' => Carbon::parse($row->date),
        'description' => $row->desc,
        'credit' => $row->credit,
        'debit' => $row->debit,
        'balance' => 0,
        'reference_type' => $referenceType,
        'reference' => $reference,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ];
    }

    $mandiriGiroRepo->saveBulk($newInquiries);

    $this->completeApprovalPending($approvalPending);
  }

  public function insertMaybankMutation($approvalPending) {
    $maybankInquiryRepo = app()->make(BankMaybankInquiryRepository::class);
    $rows = json_decode($approvalPending->raw_data, true);

    $newInquiries = [];
    $now = Carbon::now();
    foreach ($rows as $row) {
      $postDate = Carbon::parse($row['postingDate']);
      $newInquiries[] = [
        'transaction_date' => $row['transactionDate'],
        'transaction_time' => $row['transactionTime'],
        'posting_date' => $postDate,
        'date' => $postDate,
        'processing_time' => $row['processingTime'],
        'description' => $row['description'],
        'transaction_ref' => $row['transactionRef'],
        'debit' => $row['debit'],
        'credit' => $row['credit'],
        'end_balance' => $row['balance'],
        'source_code' => $row['sourceCode'],
        'teller_id' => $row['tellerId'],
        'transaction_code' => $row['transactionCode'],
        'bank_scrape_account_number_id' => BankAccount::MAYBANK,
        'created_at' => $now,
        'updated_at' => $now
      ];
    }

    $maybankInquiryRepo->saveBulk($newInquiries);
    $this->completeApprovalPending($approvalPending);
  }

  private function completeApprovalPending($approvalPending) {
    $approvalPending->processed_at = Carbon::now();
    $approvalPending->status = Approval::PROCESSED;
    $this->approvalPendingRepo->save($approvalPending);
  }

  private function removeCommas($text) {
    return str_replace(',', '', $text);
  }
}