<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/11/19
 * Time: 16.20
 */

namespace Odeo\Domains\Approval\Model;


use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class ApprovalPendingRequest extends Entity {

  public function requester() {
    return $this->belongsTo(User::class, 'requested_by');
  }

}