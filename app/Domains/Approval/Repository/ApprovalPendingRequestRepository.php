<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/11/19
 * Time: 16.20
 */

namespace Odeo\Domains\Approval\Repository;


use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Approval\Model\ApprovalPendingRequest;
use Odeo\Domains\Core\Repository;

class ApprovalPendingRequestRepository extends Repository {

  public function __construct(ApprovalPendingRequest $request) {
    $this->model = $request;
  }

  public function get($userId = null) {
    $filters = $this->getFilters();
    $q = $this->getCloneModel()->with(['requester']);

    if ($userId) $q = $q->where('need_approver_from', $userId);
    else $q = $q->whereNull('need_approver_from');

    if (isset($filters['search'])) {
      $search = $filters['search'];

      if (isset($search['id'])) {
        $q = $q->whereId($search['id']);
      }

      if (isset($search['status'])) {
        $q = $q->where('status', $search['status']);
      }
    }

    $q = $q->orderBy('id', 'DESC');

    return $this->getResult($q);
  }

  public function getApprovedPendingApproval() {
    $q = $this->getModel()
      ->where('status', Approval::APPROVED)
      ->get();

    return $q;
  }

  public function findPathReferenceId($path, $referenceId) {
    return $this->getCloneModel()
      ->where('path', $path)->where('reference_id', $referenceId)
      ->orderBy('id', 'desc')->first();
  }

}