<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/5/17
 * Time: 11:45 PM
 */

namespace Odeo\Domains\Banner\Mobile;

use Odeo\Domains\Core\PipelineListener;

class BannerInserter {

  private $banners;

  public function __construct() {
    $this->banners = app()->make(\Odeo\Domains\Banner\Mobile\Repository\MobileBannerRepository::class);
  }

  public function insert(PipelineListener $listener, $data) {

    $banner = $this->banners->getNew();

    $banner->trigger = $data['trigger'];
    $banner->priority = $data['priority'];
    $banner->url = $data['url'];

    if (isset($data['additional'])) {
      $banner->additional = json_encode($data['additional']);
    }

    $this->banners->save($banner);

    return $listener->response(200);

  }

}