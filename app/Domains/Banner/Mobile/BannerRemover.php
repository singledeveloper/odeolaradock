<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/5/17
 * Time: 11:45 PM
 */

namespace Odeo\Domains\Banner\Mobile;


use Odeo\Domains\Core\PipelineListener;

class BannerRemover {

  private $banners;

  public function __construct() {
    $this->banners = app()->make(\Odeo\Domains\Banner\Mobile\Repository\MobileBannerRepository::class);
  }

  public function remove(PipelineListener $listener, $data) {

    $this->banners->deleteById($data['banner_id']);

    return $listener->response(200);
  }


}