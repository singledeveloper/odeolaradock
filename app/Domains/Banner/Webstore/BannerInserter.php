<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/5/17
 * Time: 11:45 PM
 */

namespace Odeo\Domains\Banner\Webstore;

use Odeo\Domains\Banner\Webstore\Repository\BannerRepository;
use Odeo\Domains\Core\PipelineListener;

class BannerInserter {

  private $bannerRepo;

  public function __construct() {
    $this->bannerRepo = app()->make(BannerRepository::class);
  }

  public function insert(PipelineListener $listener, $data) {

    $banner = $this->bannerRepo->getNew();

    $banner->owner_id = $data['owner_id'];
    $banner->owner_type = $data['owner_type'];
    $banner->trigger = $data['trigger'];
    $banner->priority = $data['priority'];

    $this->insertTriggerData($banner, $data);

    $banner->desktop_url = $data['desktop_url'] ?? null;
    $banner->mobile_url = $data['mobile_url'] ?? null;

    $this->bannerRepo->save($banner);

    return $listener->response(200);

  }

  private function insertTriggerData($banner, $data) {

    switch ($data['trigger']) {
      case 'only_redirect':
        $banner->redirect_url = $data['url'];
        break;
    }

  }

}
