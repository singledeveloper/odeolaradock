<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/6/17
 * Time: 4:51 PM
 */

namespace Odeo\Domains\Banner\Webstore;

use Odeo\Domains\Constant\AwsConfig;
use Odeo\Domains\Constant\Banner;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class BannerSelector implements SelectorListener {

  private $banners, $bannerHelper;

  public function __construct() {
    $this->banners = app()->make(\Odeo\Domains\Banner\Webstore\Repository\BannerRepository::class);
    $this->bannerHelper = app()->make(\Odeo\Domains\Banner\Webstore\Helper\BannerHelper::class);
  }

  public function _transforms($item, Repository $repository) {

    $response = $item->toArray();

    if ($response['desktop_url']) $response['desktop_url'] = AwsConfig::S3_BASE_URL . $response['desktop_url'];
    if ($response['mobile_url']) $response['mobile_url'] = AwsConfig::S3_BASE_URL . $response['mobile_url'];

    $response['owner'] = $this->bannerHelper->getOwner($response['owner_type'], $response['owner_id']);

    return $response;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {

    $this->banners->normalizeFilters($data);

    $banners = [];

    foreach ($this->banners->getAll() as $banner) {
      $banners[] = $this->_transforms($banner, $this->banners);
    }
    if (sizeof($banners) > 0)
      return $listener->response(200, array_merge(
        ["banners" => $this->_extends($banners, $this->banners)],
        $this->banners->getPagination()
      ));
    return $listener->response(204, ["banners" => []]);
  }

}
