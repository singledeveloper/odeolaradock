<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/6/17
 * Time: 11:14 PM
 */

namespace Odeo\Domains\Banner\Webstore\Helper;

use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Service;

class BannerHelper {

  public function getOwner($ownerType, $ownerId) {
    switch ($ownerType) {
      case 'service':
        return Service::getConstKeyByValue($ownerId);
        break;
      case 'payment':
        return Payment::getConstKeyByValue($ownerId);
        break;
    }
    return null;
  }

}