<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/5/17
 * Time: 11:02 PM
 */

namespace Odeo\Domains\Banner\Webstore\Repository;

use Odeo\Domains\Banner\Webstore\Model\Banner;
use Odeo\Domains\Core\Repository;

class BannerRepository extends Repository {

  public function __construct(Banner $banner) {
    $this->model = $banner;
  }

  public function getActive() {
    return $this->model
      ->where('active', true)
      ->orderBy('priority', 'asc')
      ->get();
  }

}