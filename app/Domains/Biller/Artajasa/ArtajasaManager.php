<?php

namespace Odeo\Domains\Biller\Artajasa;

use Odeo\Domains\Biller\Artajasa\Purchaser\IndosatDataPurchaser;
use Odeo\Domains\Biller\Artajasa\Purchaser\IndosatPostpaidPurchaser;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\Contract\BillerContract;

class ArtajasaManager implements BillerContract {

  private $purchaser, $postpaidPurchaser, $inventoryValidator, $inquirer;

  public function __construct() {
    $this->inventoryValidator = app()->make(InventoryValidator::class);
    $this->postpaidPurchaser = app()->make(IndosatPostpaidPurchaser::class);
    $this->inquirer = app()->make(IndosatPostpaidInquirer::class);
    $this->purchaser = app()->make(IndosatDataPurchaser::class);
  }

  public function purchase(PipelineListener $listener, $data) {
    return $this->purchaser->purchase($listener, $data);
  }

  public function purchasePostpaid(PipelineListener $listener, $data) {
    return $this->postpaidPurchaser->purchase($listener, $data);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    return $this->inventoryValidator->validateInventory($listener, $data);
  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {
    return $this->inventoryValidator->validatePostpaidInventory($listener, $data);
  }

  public function inquiryPostpaid(PipelineListener $listener, $data) {
    return $this->inquirer->inquiry($listener, $data);
  }

}