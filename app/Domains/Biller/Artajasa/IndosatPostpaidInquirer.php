<?php

namespace Odeo\Domains\Biller\Artajasa;

use Odeo\Domains\Constant\BillerArtajasa;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Supply\Biller\Inquirer;

class IndosatPostpaidInquirer {

  private $inquirer, $inquiryHistories;

  public function __construct() {
    $this->inquirer = app()->make(\Odeo\Domains\Supply\Biller\Inquirer::class);
    $this->inquiryHistories = app()->make(\Odeo\Domains\Supply\Repository\PostpaidInquiryHistoryRepository::class);
  }

  public function inquiry(PipelineListener $listener, $data) {
    $data['number'] = isset($data['item_detail']) ? $data['item_detail']['number'] : $data['number'];

    try {
      $client = new \GuzzleHttp\Client([
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ],
        'timeout' => 20,
        'headers' => [
          'Pass-Key' => env('ARTAJASA_BILLER_PASS_KEY', ''),
          'Client-Id' => env('ARTAJASA_BILLER_CLIENT_ID', '')
        ]
      ]);

      $dateISO = date('Ymd\THis+07');
      $stanId = BillerArtajasa::generateSTAN();
      $acquireId = env('ARTAJASA_BILLER_ACQUIRE_ID');
      $id = $this->inquiryHistories->generateLastId();
      $request = [
        'billerId' => '',
        'signature' => strtoupper(sha1($stanId . $id . $dateISO .
          $data['number'] . $data['number'] . $acquireId .
          env('ARTAJASA_BILLER_SECRET_KEY', ''))),
        'transId' => [
          'STAN' => $stanId,
          'RRN' => (string)$id,
          'Timestamp' => $dateISO,
          'termId' => $data['number'],
          'acquireId' => $acquireId
        ],
        'transData' => [
          'idCustomer' => $data['number']
        ]
      ];

      $response = $client->request('POST', BillerArtajasa::INDOSAT_JSON_SERVER . 'api/inquiry', [
        'json' => $request
      ]);
      $inquiry = json_decode($response->getBody()->getContents());

      if (isset($response->transId) && isset($response->transId->respCode)) {
        if ($response->transId->respCode == '00') {
          $result[Supplier::SF_PARAM_PRODUCT_NAME] = PostpaidType::ISAT_POSTPAID;
          $result[Supplier::SF_PARAM_NUMBER] = $inquiry->transData->idCustomer;
          $result[Supplier::SF_PARAM_OWNER_NAME] = $inquiry->transData->custName;
          $result[Supplier::SF_PARAM_SN] = $inquiry->transData->authKey;
          $result[Supplier::SF_PARAM_POSTPAID_PRICE] = $inquiry->transData->billAmount;
          $billerStatus = SwitcherConfig::BILLER_SUCCESS;
          $data['result'] = json_encode($result);
        } else {
          $billerStatus = SwitcherConfig::BILLER_FAIL;
          $data['error_message'] = Inquirer::DEFAULT_ERROR . '. Kode error: ' . $response->transId->respCode;
        }
      }
      else {
        $data['error_message'] = Inquirer::DEFAULT_ERROR;
      }

      $data['response'] = json_encode($inquiry);
      $data['status'] = $billerStatus ?? SwitcherConfig::BILLER_FAIL;
    }
    catch (\Exception $e) {
      $data['error_message'] = 'Error timeout. Please try again later.' . (isset($inquiry) ? (', res: ' . json_encode($inquiry)) : '');
      $data['status'] =  SwitcherConfig::BILLER_FAIL;
    }

    $data['request'] = isset($request) ? json_encode($request) : '-';

    return $this->inquirer->saveManually($listener, $data);
  }
}
