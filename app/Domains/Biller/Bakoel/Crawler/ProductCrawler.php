<?php

namespace Odeo\Domains\Biller\Bakoel\Crawler;

use Odeo\Domains\Constant\BillerBakoel;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository;

class ProductCrawler {

  public function __construct() {
    $this->pulsaInventories = app()->make(PulsaOdeoInventoryRepository::class);
  }

  public function crawl() {
    if ($products = BillerBakoel::getProductList()) {
      $response = [];
      foreach ($products as $item) {
        $response[$item['produk_id']] = $item;
      }

      $inventories = $this->pulsaInventories->findByVendorSwitcherId(SwitcherConfig::BAKOEL);

      $codes = [];
      foreach ($inventories as $item) {
        if (isset($response[$item->code])) {
          $item->base_price = $response[$item->code]['harga'];
          $item->is_active = true;
          $this->pulsaInventories->save($item);

          $codes[] = $item->code;
        }
      }

      $this->pulsaInventories->finalizeProducts(SwitcherConfig::BAKOEL, $codes, [Pulsa::OPERATOR_PLN]);
    }
  }

}