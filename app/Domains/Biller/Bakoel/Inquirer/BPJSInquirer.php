<?php

namespace Odeo\Domains\Biller\Bakoel\Inquirer;

use Odeo\Domains\Constant\BillerBakoel;
use Odeo\Domains\Constant\BPJS;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Supply\Biller\Inquirer;

class BPJSInquirer {

  private $inquirer, $pulsaInventories;

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->inquirer = app()->make(\Odeo\Domains\Supply\Biller\Inquirer::class);
  }

  public function inquiryKes(PipelineListener $listener, $data) {

    $data['number'] = isset($data['item_detail']) ? $data['item_detail']['number'] : $data['number'];

    $monthCounts = isset($data['item_detail']) ?
      (isset($data['item_detail']['month_counts']) ? $data['item_detail']['month_counts'] : 1) :
      (isset($data['month_counts']) ? $data['month_counts'] : 1); // TO DO BPJS DELETE LATER
    if ($monthCounts == 1) {
      if (isset($data['inventory_code'])) $code = $data['inventory_code'];
      else {
        $inventory = $this->pulsaInventories->findById($data['inventory_id']);
        $code = $inventory->code;
      }
      $temp = explode('.', $code);
      $monthCounts = isset($temp[1]) ? $temp[1] : 1;
    }

    $request = [
      'productCode' => BillerBakoel::BPJS_KES,
      'idPel' => $data['number'],
      'idPel2' => $monthCounts,
      'miscData' => json_encode([
        'namaLoket' => BPJS::LOCKET_NAME,
        'alamatLoket' => BPJS::LOCKET_ADDRESS,
        'kotaLoket' => BPJS::LOCKET_CITY,
        'kodepos' => BPJS::LOCKET_POSTAL,
        'noTelp' => BPJS::LOCKET_TELEPHONE,
        'kodeKab' => BPJS::LOCKET_AREA_CODE
      ])
    ];

    try {

      $inquiry = BillerBakoel::setClient(BillerBakoel::PP_SERVER)->call(BillerBakoel::CMD_PP_INQUIRY, $request);

      if (isset($inquiry['responseCode'])) {
        if ($inquiry['responseCode'] == '00') {
          $result[Supplier::SF_PARAM_OWNER_NAME] = $inquiry['namaPeserta'];
          $result[Supplier::SF_PARAM_SN] = $inquiry['refID'];
          $result[Supplier::SF_PARAM_POSTPAID_PRICE] = $inquiry['tagihan'][0]['nilaiTagihan'];
          $result[Supplier::SF_PARAM_POSTPAID_ADMIN] = $inquiry['tagihan'][0]['admin'];
          $result[Supplier::SF_PARAM_BPJS_BRANCH_CODE] = $inquiry['kodeCabang'];
          $result[Supplier::SF_PARAM_BPJS_BRANCH_NAME] = $inquiry['namaCabang'];
          $result[Supplier::SF_PARAM_POSTPAID_MONTH_COUNT] = intval($inquiry['jumlahBulan']);
          $result[Supplier::SF_PARAM_BPJS_PERSON_COUNT] = intval($inquiry['jumlahPeserta']);
          $result[Supplier::SF_PARAM_POSTPAID_BILL_REST] = intval($inquiry['tagihan'][0]['sisaSebelumnya']);
          $billerStatus = SwitcherConfig::BILLER_SUCCESS;
          $data['result'] = json_encode($result);
        }
        else {
          list($billerStatus, $data['error_message']) = BillerBakoel::translatePostpaidResponse($inquiry['responseCode'],
            isset($inquiry['message']) ? $inquiry['message'] : '');
        }

        $data['response'] = json_encode($inquiry);
        $data['status'] = $billerStatus ?? SwitcherConfig::BILLER_FAIL;
      }
      else {
        $data['error_message'] = Inquirer::DEFAULT_ERROR;
        $data['status'] = $billerStatus ?? SwitcherConfig::BILLER_FAIL;
      }
    }
    catch(\Exception $e) {
      $data['error_message'] = 'Error timeout. Please try again later.';
      $data['status'] =  SwitcherConfig::BILLER_FAIL;
    }

    $data['request'] = json_encode($request);

    return $this->inquirer->saveManually($listener, $data);
  }

  public function inquiryKtg(PipelineListener $listener, $data) {

    $inquiry = BillerBakoel::setClient(BillerBakoel::BPJS_TK_SERVER)->call(BillerBakoel::CMD_BPJS_TK_INQUIRY, [
      'nik' => isset($data['item_detail']) ? $data['item_detail']['number'] : $data['nik']
    ]);

    if (isset($inquiry['status']) && $inquiry['status'] == '00') {

      $inquiry = BillerBakoel::setClient(BillerBakoel::BPJS_TK_SERVER)->call(BillerBakoel::CMD_BPJS_TK_IURAN_INQUIRY, [
        'kodeIuran' => $inquiry['data'][0]['kodeIuran']
      ]);

      if (isset($inquiry['status']) && $inquiry['status'] == '00') {
        return [
          'name' => strtoupper(PostpaidType::BPJS_TK),
          'subscriber_id' => $inquiry['nik'],
          'subscriber_name' => $inquiry['nama'],
          'multiplier' => 1,
          'ref_id' => $inquiry['kodeIuran'],
          'biller_price' => $inquiry['totalPembayaran'],
          'details' => [
            'program_code' => $inquiry['kodeProgram'],
            'base_price' => $inquiry['jumlahBayar'],
            'registration_fee' => $inquiry['biayaRegistrasi'],
            'admin' => $inquiry['biayaAdmin']
          ]
        ];
      }

    }

    return [
      'status' => SwitcherConfig::BILLER_FAIL,
      'error_message' => $inquiry['msg']
    ];
  }
}