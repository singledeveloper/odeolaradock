<?php

namespace Odeo\Domains\Biller\Bakoel\Inquirer;

use Odeo\Domains\Constant\BillerBakoel;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Supply\Biller\Inquirer;

class PDAMInquirer {

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->inquirer = app()->make(\Odeo\Domains\Supply\Biller\Inquirer::class);
  }

  public function inquiry(PipelineListener $listener, $data) {

    if (isset($data['inventory_code'])) $code = $data['inventory_code'];
    else {
      $inventory = $this->pulsaInventories->findById($data['inventory_id']);
      $code = $inventory->code;
    }

    $data['number'] = isset($data['item_detail']) ? $data['item_detail']['number'] : $data['number'];
    $request = [
      'productCode' => $code,
      'idPel' => $data['number'],
      'idPel2' => '',
      'miscData' => ''
    ];

    try {
      $inquiry = BillerBakoel::setClient(BillerBakoel::PP_SERVER)->call(BillerBakoel::CMD_PP_INQUIRY, $request);

      if (isset($inquiry['responseCode'])) {
        if ($inquiry['responseCode'] == '00') {
          $result[Supplier::SF_PARAM_DETAILS_LOOP] = [];

          foreach ($inquiry['tagihan'] as $item) {
            $result[Supplier::SF_PARAM_DETAILS_LOOP][] = [
              Supplier::SF_PARAM_POSTPAID_PERIODE => trim($item['periode']),
              Supplier::SF_PARAM_POSTPAID_PRICE => $item['nilaiTagihan'],
              Supplier::SF_PARAM_POSTPAID_FINE => $item['penalty'],
              Supplier::SF_PARAM_POSTPAID_BILL_REST => intval($item['tagihanLain']),
              Supplier::SF_PARAM_POSTPAID_METER_CHANGES => trim($item['meterAwal']) != 0 && trim($item['meterAwal']) != '' ? ($item['meterAwal'] . '-' . $item['meterAkhir']) : '-',
              Supplier::SF_PARAM_POSTPAID_ADMIN => $item['admin']
            ];
          }

          $result[Supplier::SF_PARAM_PRODUCT_NAME] = isset($inventory) ? strtoupper($inventory->pulsa->category) : PostpaidType::PDAM;
          $result[Supplier::SF_PARAM_OWNER_NAME] = $inquiry['nama'];
          $result[Supplier::SF_PARAM_SN] = $inquiry['refID'];
          $result[Supplier::SF_PARAM_POSTPAID_PRICE_TOTAL] = $inquiry['totalTagihan'];
          $billerStatus = SwitcherConfig::BILLER_SUCCESS;
          $data['result'] = json_encode($result);
        }
        else {
          list($billerStatus, $data['error_message']) = BillerBakoel::translatePostpaidResponse($inquiry['responseCode'],
            isset($inquiry['message']) ? $inquiry['message'] : '');
        }

        $data['response'] = json_encode($inquiry);
        $data['status'] = $billerStatus ?? SwitcherConfig::BILLER_FAIL;
      }
      else {
        $data['error_message'] = Inquirer::DEFAULT_ERROR;
        $data['status'] = $billerStatus ?? SwitcherConfig::BILLER_FAIL;
      }
    }
    catch(\Exception $e) {
      $data['error_message'] = 'Error timeout. Please try again later.';
      $data['status'] =  SwitcherConfig::BILLER_FAIL;
    }

    $data['request'] = json_encode($request);

    return $this->inquirer->saveManually($listener, $data);
  }
}