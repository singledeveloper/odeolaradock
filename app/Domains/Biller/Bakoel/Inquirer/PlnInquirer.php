<?php

namespace Odeo\Domains\Biller\Bakoel\Inquirer;

use Odeo\Domains\Constant\Affiliate;
use Odeo\Domains\Constant\BillerBakoel;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Supply\Biller\Inquirer;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;

class PlnInquirer {

  private $inquirer;

  public function __construct() {
    $this->inquirer = app()->make(\Odeo\Domains\Supply\Biller\Inquirer::class);
  }

  public function inquiry(PipelineListener $listener, $data) {
    $result = [];
    try {
      $inquiry = BillerBakoel::getPlnPrepaidInquiry($data['number']);
      if (count($inquiry) > 0)
        $result = [
          'number' => $inquiry['data']['msn'],
          'subscriber_id' => $inquiry['data']['subscriberID'],
          'name' => $inquiry['data']['nama'],
          'tariff' => $inquiry['data']['tarif'],
          'power' => $inquiry['data']['daya']
        ];
    }
    catch(\Exception $e) {
      clog('bakoel_timeout', $e->getMessage());
    }

    if (count($result) > 0) {
      if (isset($data['auth']) && isset($data['is_error'])) {
        $inquiryFees = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateInquiryFeeRepository::class);
        $fee = $inquiryFees->findByUserId($data['auth']['user_id']);
        $fee = $fee && $fee->pln_inquiry_fee != null ? $fee->pln_inquiry_fee : Affiliate::DEFAULT_PLN_INQUIRY_FEE;
        if ($fee > 0) {
          try {
            $cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
            $cashInserter->add([
              'user_id' => $data['auth']['user_id'],
              'trx_type' => TransactionType::AFFILIATE_INQUIRY_FEE,
              'cash_type' => CashType::OCASH,
              'amount' => -$fee,
              'data' => json_encode([
                'number' => $data['number'],
                'type' => 'pln'
              ]),
              'reference_type' => TransactionType::AFFILIATE_PLN_INQUIRY
            ]);
            $cashInserter->run();
          }
          catch (InsufficientFundException $e) {
            return $listener->response(400, 'Saldo Anda tidak mencukupi.');
          }
        }
      }
      return $listener->response(200, ['inquiry' => $result]);
    }
    if (isset($data['bypass'])) {
      return $listener->response(200, ['inquiry' => [
        'number' => $data['number'],
        'subscriber_id' => '-',
        'name' => '-',
        'tariff' => '-',
        'power' => '-'
      ]]);
    }
    return $listener->response(400, 'Invalid PLN number.');
  }

  public function tempInquiry(PipelineListener $listener, $data) {
    return $listener->response(200, ['inquiry' => [
      'number' => $data['number'],
      'subscriber_id' => '-',
      'name' => '-',
      'tariff' => '-',
      'power' => '-'
    ]]);
  }

  public function inquiryPostpaid(PipelineListener $listener, $data) {
    $data['number'] = isset($data['item_detail']) ? $data['item_detail']['number'] : $data['number'];
    $request = [
      'productCode' => BillerBakoel::PLN_POSTPAID,
      'idPel' => $data['number'],
      'idPel2' => '',
      'miscData' => ''
    ];

    try {
      $inquiry = BillerBakoel::setClient(BillerBakoel::PP_SERVER, 10, 10)->call(BillerBakoel::CMD_PP_INQUIRY, $request);

      if (isset($inquiry['responseCode'])) {
        if (in_array($inquiry['responseCode'], ['00', '0'])) {
          $result[Supplier::SF_PARAM_DETAILS_LOOP] = [];
          foreach ($inquiry['detilTagihan'] as $item) {
            $result[Supplier::SF_PARAM_DETAILS_LOOP][] = [
              Supplier::SF_PARAM_POSTPAID_PERIODE => wordwrap($item['periode'], 4, '-', true),
              Supplier::SF_PARAM_POSTPAID_PRICE => intval($item['nilaiTagihan']),
              Supplier::SF_PARAM_POSTPAID_FINE => intval($item['denda']),
              Supplier::SF_PARAM_POSTPAID_ADMIN => intval($item['admin'])
            ];
          }

          $result[Supplier::SF_PARAM_OWNER_NAME] = $inquiry['nama'];
          $result[Supplier::SF_PARAM_PLN_POWER] = $inquiry['daya'];
          $result[Supplier::SF_PARAM_PLN_TARIFF] = $inquiry['tarif'];
          $result[Supplier::SF_PARAM_SN] = $inquiry['refID'];
          $result[Supplier::SF_PARAM_POSTPAID_PRICE_TOTAL] = intval($inquiry['totalTagihan']);
          $billerStatus = SwitcherConfig::BILLER_SUCCESS;
          $data['result'] = json_encode($result);
        }
        else {
          list($billerStatus, $data['error_message']) = BillerBakoel::translatePostpaidResponse($inquiry['responseCode'],
            isset($inquiry['message']) ? $inquiry['message'] : '');
        }

        $data['response'] = json_encode($inquiry);
        $data['status'] = $billerStatus ?? SwitcherConfig::BILLER_FAIL;
      } else {
        $data['error_message'] = Inquirer::DEFAULT_ERROR;
        $data['status'] = $billerStatus ?? SwitcherConfig::BILLER_FAIL;
      }
    }
    catch(\Exception $e) {
      $data['error_message'] = 'Error timeout. Please try again later.';
      $data['status'] =  SwitcherConfig::BILLER_FAIL;
    }

    $data['request'] = json_encode($request);

    return $this->inquirer->saveManually($listener, $data);
  }
}
