<?php

namespace Odeo\Domains\Biller\Bakoel;

use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class InventoryValidator {

  private $pulsaInventories, $marginFormatter, $inquirer;

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->inquirer = app()->make(\Odeo\Domains\Supply\Biller\Inquirer::class);
  }

  public function validateInventory(PipelineListener $listener, $data) {

    if ($inventory = $this->pulsaInventories->findById($data['inventory_id'])) {
      $pulsa = $inventory->pulsa;
      $formatted = $this->marginFormatter->formatMargin($pulsa->price, $data);

      return $listener->response(200, array_merge($formatted, [
        'inventory_id' => $inventory->id,
        'name' => $pulsa->name,
      ]));
    }

    return $listener->response(400, trans('pulsa.cant_purchase_no_backup'));

  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {
    $inquiryHistories = app()->make(\Odeo\Domains\Supply\Repository\PostpaidInquiryHistoryRepository::class);
    if ($inquiry = $inquiryHistories->findRecentInquiry(isset($data['item_detail']) ? $data['item_detail']['number'] : $data['number'],
      $data['auth']['user_id'], $data['inventory_id'], Supplier::POSTPAID_INQ_TYPE_CHECK))
      return $this->inquirer->format($listener, $data, $inquiry);

    $inventory = $this->pulsaInventories->findById($data['inventory_id']);
    $manager = SwitcherConfig::getVendorSwitcherManagerById($inventory->vendor_switcher_id);
    $inquiry = $manager->inquiryPostpaid($listener, $data);

    if ($inquiry->status == SwitcherConfig::BILLER_FAIL)
      return $listener->response(400, $inquiry->error_message);
    return $this->inquirer->format($listener, $data, $inquiry);
  }
}
