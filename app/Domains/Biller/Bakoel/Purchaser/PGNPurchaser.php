<?php

namespace Odeo\Domains\Biller\Bakoel\Purchaser;

use Odeo\Domains\Constant\BillerBakoel;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class PGNPurchaser extends BillerManager {

  private $inquirer;

  public function __construct() {
    parent::__construct(SwitcherConfig::BAKOEL);
    $this->inquirer = app()->make(\Odeo\Domains\Biller\Bakoel\Inquirer\PGNInquirer::class);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);

    try {
      $data['user_id'] = $this->currentOrder->user_id;
      $inquiry = $this->inquirer->inquiry($listener, $data);
      if ($inquiry->status != SwitcherConfig::BILLER_SUCCESS) {
        $this->billerOrder->status = $inquiry->status;
        $this->billerOrder->log_request = '';
        $this->billerOrder->log_response = $inquiry->response ? $inquiry->response : $inquiry->error_message;
      }
      else {
        $inquiry = $this->inquiryFormatter->setPath($this->currentPulsa->service_detail_id)->toJsonInquiry(json_decode($inquiry->result, true));
        $latestInquiryPrice = $inquiry['biller_price'] + ($inquiry['multiplier'] * $this->currentPulsaInventory->base_price);
        if ($latestInquiryPrice != $this->currentSwitcher->current_base_price) {
          $this->billerOrder->log_response = json_encode($inquiry);
          $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
        }
        else {
          $this->currentSwitcher->current_base_price = $latestInquiryPrice;

          $request = [
            'productCode' => $this->currentPulsaInventory->code,
            'refID' => $inquiry["ref_id"],
            'nominal' => $inquiry['biller_price'],
            'miscData' => ''
          ];
          $response = BillerBakoel::setClient(BillerBakoel::PP_SERVER)->call(BillerBakoel::CMD_PP_PAYMENT, $request);

          if (isset($response['responseCode'])) {
            if ($response['responseCode'] == '00') {
              $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;
              $this->currentSwitcher->serial_number = $response['ref'];
              $this->currentRecon->biller_transaction_id = $response['ref'];
            }
            else if ($response['responseCode'] == '68') $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;
            else {
              if ($response['responseCode'] == '22') {
                $internalNoticer = app()->make(\Odeo\Domains\Notification\Helper\InternalNoticer::class);
                $internalNoticer->saveMessage('Refund ID ' . $this->currentOrder->id . ', saldo tidak cukup di Bakoel', NotificationType::NOTICE_REFUND, $this->currentPulsa->id);
              }
              $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
            }
          }
          else $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;

          $this->billerOrder->log_request = json_encode($request);
          $this->billerOrder->log_response = json_encode($response);
        }
      }
    }
    catch (\Exception $e) {
      $this->billerOrder->log_response = 'Error: ' . $e->getMessage();
      $this->billerOrder->status = SwitcherConfig::BILLER_TIMEOUT;
    }

    try {
      $info = BillerBakoel::setClient(BillerBakoel::PULSA_SERVER)->call(BillerBakoel::CMD_MITRA_INFO);
      $this->currentBiller->current_balance = $info['quota'];
    }
    catch(\Exception $e) {}

    return $this->finalize($listener);
  }
}