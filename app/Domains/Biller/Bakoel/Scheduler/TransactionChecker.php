<?php

namespace Odeo\Domains\Biller\Bakoel\Scheduler;

use Odeo\Domains\Constant\BillerBakoel;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class TransactionChecker extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::BAKOEL);
  }

  public function run() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(__CLASS__, 'check'));
    $pipeline->execute([]);
  }
  
  public function check(PipelineListener $listener, $data) {
    $orders = $this->billerRepository->getEmptySNAndPendingTransaction();
    foreach ($orders as $output) {
      $this->load($output);

      $json = json_decode($this->billerOrder->log_response);
      $check = BillerBakoel::setClient(BillerBakoel::PULSA_SERVER)->call(BillerBakoel::CMD_CHECK_STATUS, [
        'msisdn' => $json->msisdn,
        'trxID' => $json->trxID
      ]);
      if (isset($check['transactionStatus'])) {
        $this->billerOrder->log_refund = json_encode($check);
        if ($check['transactionStatus'] == "REFUND" || $check['transactionStatus'] == "FAILED") {
          $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
        }
        else if ($check['transactionStatus'] == 'SUCCESS') {
          $this->billerOrder->timestamps = false;
          $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;

          if ((isset($check['VoucherSN']) && $check['VoucherSN'] != null) ||
            time() - strtotime($this->billerOrder->updated_at) > 2 * 24 * 3600) {
            $this->currentSwitcher->serial_number = (isset($check['VoucherSN']) && $check['VoucherSN'] != null) ? $check['VoucherSN'] : 'N/A';
          }
        }
        $this->currentRecon->notify = $this->billerOrder->log_refund;
        $this->finalize($listener, false);
      }
    }
  }
}
