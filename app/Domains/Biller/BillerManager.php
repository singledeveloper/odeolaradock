<?php

namespace Odeo\Domains\Biller;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Biller\Jobs\ChangeBiller;
use Odeo\Domains\Biller\Jobs\InsertMutation;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Pln\Jobs\SendPLNToken;
use Odeo\Domains\Order\OrderVerificator;
use Odeo\Domains\Subscription\ManageInventory\Jobs\StabilizeAllStoreProfit;
use Odeo\Domains\Vendor\SMS\Job\SendSms;

class BillerManager {

  //repository
  protected $pulsaSwitcherRepository, $reconRepository, $vendorSwitcherRepository, $pulsaDuplicates,
    $pulsaInventoryRepository, $billerRepository, $orderRepository, $pulsaOdeoRepository;

  //helper
  protected $switcher, $currencyHelper, $notification, $gatewayCallback, $inquiryFormatter;

  //variable
  protected $currentSwitcher, $currentRecon, $currentOrderDetail,
    $currentOrder, $currentBiller, $currentPulsa, $currentPulsaInventory, $currentPulsaSupply,
    $billerOrder = false, $currentRoute, $statusBefore, $timeoutMessage = '',
    $isCheckSN = true, $supplySettlementAt = '', $externalData, $redis;

  public function __construct($billerId = '') {
    $this->vendorSwitcherRepository = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->pulsaSwitcherRepository = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->pulsaInventoryRepository = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->reconRepository = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository::class);
    $this->pulsaOdeoRepository = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->orderRepository = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->pulsaDuplicates = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaDuplicateRepository::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->gatewayCallback = app()->make(\Odeo\Domains\Order\Helper\GatewayCallbacker::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->switcher = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaSwitcher::class);
    $this->inquiryFormatter = app()->make(\Odeo\Domains\Supply\Formatter\InquiryManager::class);
    $this->redis = Redis::connection();
    if ($billerId != '') $this->billerRepository = SwitcherConfig::getVendorSwitcherRepositoryById($billerId);

    $temp = explode('\\', get_class($this));
    $this->currentRoute = strtolower((count($temp) > 1 ? ($temp[count($temp) - 2] . '_') : '') . $temp[count($temp) - 1]);
  }

  public function getCurrentRecon() {
    return $this->currentRecon;
  }

  public function getCurrentSwitcher() {
    return $this->currentSwitcher;
  }

  public function expandSwitcher($switcher, $data = []) {
    $this->currentSwitcher = $switcher;
    $this->currentOrderDetail = $this->currentSwitcher->orderDetail;
    $this->currentOrder = $this->currentOrderDetail->order;
    $this->currentPulsaInventory = $this->currentSwitcher->currentInventory;
    $this->currentPulsa = $this->currentPulsaInventory->pulsa;
    $this->currentBiller = $this->currentSwitcher->vendorSwitcher;
    if ($this->currentPulsaInventory->supply_pulsa_id != null)
      $this->currentPulsaSupply = $this->pulsaOdeoRepository->findById($this->currentPulsaInventory->supply_pulsa_id);
    $this->externalData = $data;
    if (count($data) > 0) $this->isCheckSN = false;

    if (isset($data['check_duplicate']) && $duplicate = $this->pulsaDuplicates->findSame($switcher->number, $this->currentPulsa->id)) {
      $this->currentRecon = $this->reconRepository->findById($duplicate->vendor_switcher_reconciliation_id);
      if ($this->currentRecon) {
        $duplicate->new_order_id = $this->currentOrder->id;
        $this->pulsaDuplicates->save($duplicate);

        $this->currentSwitcher->serial_number = $this->currentRecon->reference_number;
        $this->currentSwitcher->current_base_price = $this->currentRecon->base_price;

        $this->currentRecon->order_detail_pulsa_switcher_id = $switcher->id;
        $this->reconRepository->save($this->currentRecon);

        return false;
      }
    }

    if (!$this->currentRecon && !isset($data['no_recon'])) {
      if (!isset($data['initiate'])) {
        if (!$this->billerRepository) $this->billerRepository = SwitcherConfig::getVendorSwitcherRepositoryById($this->currentBiller->id);
        if ($this->billerRepository != '' && $transaction = $this->billerRepository->findBySwitcherId($switcher->id)) {
          $this->currentRecon = $this->reconRepository->findByTransactionId($transaction->id, $this->currentBiller->id);
        }
        else $this->currentRecon = $this->reconRepository->findBySwitcherId($switcher->id, $this->currentBiller->id);
      }

      if (!$this->currentRecon) {
        $this->currentRecon = $this->reconRepository->getNew();
        $this->currentRecon->order_detail_pulsa_switcher_id = $data["order_detail_pulsa_switcher_id"];
        $this->currentRecon->vendor_switcher_id = $this->currentBiller->id;
        if ($this->billerOrder) $this->currentRecon->transaction_id = $this->billerOrder->id;

        if ($this->currentPulsa->owner_store_id) $this->currentRecon->base_price =  $this->currentPulsa->price;
        else if ($this->currentPulsaSupply) $this->currentRecon->base_price = $this->currentPulsaSupply->price;
        else $this->currentRecon->base_price = $this->currentSwitcher->current_base_price;
        $this->currentRecon->trx_month_year = date('Y-m');
        $this->currentRecon->status = SwitcherConfig::BILLER_CREATED;
        $this->reconRepository->save($this->currentRecon);
      }
    }

    return true;
  }

  public function initiate(PipelineListener $listener, $data) {

    if ($this->billerRepository) {
      $this->billerOrder = $this->billerRepository->getNew();
      $this->billerOrder->switcher_reference_id = $data["order_detail_pulsa_switcher_id"];
      $switcher = $this->billerOrder->switcher;

      if (isset($data['route'])) $this->billerOrder->route = $data['route'];
      else {
        $switcher->status = SwitcherConfig::SWITCHER_FAIL_UNDEFINED_ROUTE;
        $this->pulsaSwitcherRepository->save($switcher);
        return false;
      }
    }
    else $switcher = $this->pulsaSwitcherRepository->findById($data["order_detail_pulsa_switcher_id"]);

    if ($switcher->status == SwitcherConfig::SWITCHER_PENDING) {
      $switcher->status = SwitcherConfig::SWITCHER_IN_QUEUE;
      $switcher->requested_at = date("Y-m-d H:i:s");
      $this->pulsaSwitcherRepository->save($switcher);
    } else if ($this->billerOrder && $this->billerOrder->route == SwitcherConfig::ROUTE_ORDER_VERIFICATOR) {
      return false;
    }

    if ($this->billerOrder) $this->billerRepository->save($this->billerOrder);

    if (!isset($data['no_initiate'])) $data['initiate'] = true;
    $data['check_duplicate'] = true;

    return $this->expandSwitcher($switcher, $data);
  }

  public function load($item) {
    $this->billerOrder = $item;
    $this->expandSwitcher($item->switcher);
    $this->statusBefore = $this->billerOrder->status;
    $this->currentRecon = $this->reconRepository->findByTransactionId($item->id, $this->currentBiller->id);
  }

  public function sendPlnNotification(PipelineListener $listener, $token) {
    if (!in_array($this->currentOrder->gateway_id, [Payment::JABBER, Payment::TELEGRAM, Payment::SMS_CENTER, Payment::AFFILIATE])) {
      if ($this->currentOrder->platform_id == Platform::WEB_STORE || $this->currentOrder->platform_id == Platform::WEB_COMPANY) {
        $listener->pushQueue(new SendSms([
          'sms_destination_number' => $this->currentOrder->phone_number,
          'sms_text' => trans('user.sms_pln_token', [
            'meter_number' => $this->currentSwitcher->number,
            'token' => $token
          ])
        ]));
      } else {
        $this->notification->setup($this->currentOrder->user_id, NotificationType::CHECK, NotificationGroup::TRANSACTION);
        $this->notification->pln_success($this->currentSwitcher->number, $token);
        $listener->pushQueue($this->notification->queue());
      }

      $listener->pushQueue(new SendPLNToken($this->currentOrder, []));
    }
  }

  public function updateStatusBasedOnConditions() {

    if ($this->currentRecon->status == SwitcherConfig::BILLER_DUPLICATE_SUCCESS) {
      if ($this->reconRepository->checkDuplicateDifferentSwitcher($this->currentSwitcher->number, $this->currentRecon->order_detail_pulsa_switcher_id,
        $this->currentSwitcher->serial_number, $this->currentBiller->duplicate_check_type)) {
        $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;
        $this->currentSwitcher->current_response_status_code = Supplier::RC_FAIL;
      }
      else if (!$this->reconRepository->checkDuplicateDifferentTransaction($this->currentRecon->id, $this->currentRecon->order_detail_pulsa_switcher_id)) {
        $this->currentRecon->status = SwitcherConfig::BILLER_SUCCESS;
        $this->currentSwitcher->current_response_status_code = Supplier::RC_OK;
      }
    }

    if ($this->currentRecon->status == SwitcherConfig::BILLER_SUCCESS && $this->isCheckSN && app()->environment() == 'production') {
      $this->currentSwitcher->serial_number = str_replace('\/', '/', $this->currentSwitcher->serial_number);
      if (strpos(strtoupper($this->currentSwitcher->serial_number), 'N/A') !== false ||
        strpos(strtolower($this->currentSwitcher->serial_number), 'update') !== false ||
        strpos(strtolower($this->currentSwitcher->serial_number), 'NULL') !== false ||
        in_array(trim($this->currentSwitcher->serial_number), ['', '-']))
        $this->currentRecon->status = SwitcherConfig::BILLER_SUSPECT;
      else if ($this->currentSwitcher->serial_number != null) {
        if ($this->currentSwitcher->operator_id == Pulsa::OPERATOR_PLN
          && strlen(str_replace('-', '', explode('/', $this->currentSwitcher->serial_number)[0])) != 20)
          $this->currentRecon->status = SwitcherConfig::BILLER_SUSPECT;
        else if (strpos($this->currentSwitcher->number, $this->currentSwitcher->serial_number) !== false)
          $this->currentRecon->status = SwitcherConfig::BILLER_SUSPECT;
      }
    }

    if (in_array($this->currentSwitcher->current_response_status_code, Supplier::getWrongNumberGroup()))
      $this->currentSwitcher->status = SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER;

    if ($this->currentRecon->order_id == null) {
      $this->currentRecon->order_id = $this->currentOrder->id;
      $this->currentRecon->inventory_name = $this->currentOrderDetail->name;
      $this->currentRecon->inventory_id = $this->currentPulsa->id;
      $this->currentRecon->inventory_code = $this->currentPulsaInventory->code;
      $this->currentRecon->number = $this->currentSwitcher->number;
      $this->currentRecon->vendor_switcher_name = $this->currentBiller->name;
      $this->currentRecon->vendor_switcher_owner_store_id = $this->currentBiller->owner_store_id;
      $this->currentRecon->order_user_id = $this->currentOrder->user_id;
      if ($this->currentPulsa->owner_store_id == $this->currentBiller->owner_store_id) {
        $key = UserStatus::REDIS_USER_DATA . $this->currentOrder->user_id;
        if (!$name = $this->redis->get($key)) {
          $name = $this->currentOrder->user->name;
          if ($this->redis->setnx($key, $name)) $this->redis->expire($key, 24 * 60 * 60);
        }
        $this->currentRecon->order_user_name = $name;
      }
    }
  }

  public function finalizeSwitcher(PipelineListener $listener) {

    $externalUserId = isset($this->externalData['auth']) && isset($this->externalData['auth']['user_id'])
      ? $this->externalData['auth']['user_id'] : '';

    if ($externalUserId != '') {
      $user = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class)->findById($externalUserId);
      $note = ($this->currentRecon->status == SwitcherConfig::BILLER_FAIL ? 'Refund' : 'Sukses') .
        ' manual oleh ' . $user->name . ' [' . $user->id . '] @' . date('d/m/Y H:i:s');
      $this->currentSwitcher->admin_note = $note;
    }

    if ($this->currentRecon->status == SwitcherConfig::BILLER_SUSPECT) {
      $this->currentOrder->status = OrderStatus::WAITING_SUSPECT;
      $this->orderRepository->save($this->currentOrder);
    } else if ($this->currentRecon->status == SwitcherConfig::BILLER_FAIL) {
      $this->currentSwitcher->serial_number = null;
      $this->currentRecon->reference_number = null;
      $this->pulsaSwitcherRepository->save($this->currentSwitcher);

      if ($this->statusBefore != SwitcherConfig::BILLER_FAIL) {
        $listener->pushQueue(new ChangeBiller([
          'order_detail_pulsa_switcher_id' => $this->currentRecon->order_detail_pulsa_switcher_id,
          'route' => $this->currentRoute,
          'external_user_id' => $externalUserId
        ]));
      }
    } else if ($this->currentRecon->status == SwitcherConfig::BILLER_VOIDED) {
      $this->currentSwitcher->status = SwitcherConfig::SWITCHER_VOIDED;
      $this->pulsaSwitcherRepository->save($this->currentSwitcher);

      $listener->pushQueue(new ChangeBiller([
        'order_detail_pulsa_switcher_id' => $this->currentRecon->order_detail_pulsa_switcher_id,
        "route" => $this->currentRoute
      ]));
    } else if ($this->currentRecon->status == SwitcherConfig::BILLER_VOID_FAIL) {
      $this->currentSwitcher->status = SwitcherConfig::SWITCHER_VOID_FAIL;
      $this->pulsaSwitcherRepository->save($this->currentSwitcher);
    } else if ($this->currentRecon->status == SwitcherConfig::BILLER_SUCCESS) {
      $this->currentSwitcher->status = SwitcherConfig::SWITCHER_COMPLETED;
      $this->pulsaSwitcherRepository->save($this->currentSwitcher);

      if ($this->currentSwitcher->serial_number != null &&
        $this->currentSwitcher->operator_id == Pulsa::OPERATOR_PLN)
        $this->sendPlnNotification($listener, $this->currentSwitcher->serial_number);

      $this->success($listener);
    }

    if ($this->currentRecon->status != SwitcherConfig::BILLER_IN_QUEUE && PostpaidType::getConstKeyByValue($this->currentPulsa->category) != '') {
      $inquiryHistories = app()->make(\Odeo\Domains\Supply\Repository\PostpaidInquiryHistoryRepository::class);
      $inquiryHistories->updateInquiry($this->currentSwitcher->number, $this->currentOrder->user_id);
    }
  }

  public function finalize(PipelineListener $listener, $return = true) {
    if ($this->timeoutMessage != '') {
      $this->currentRecon->response = $this->timeoutMessage;
      if ($this->currentBiller->id != SwitcherConfig::BLACKHAWK && (
          strpos($this->currentRecon->response, 'Failed to connect to') !== false ||
          strpos($this->currentRecon->response, 'Could not resolve host') !== false))
        $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
    }

    if (isset($this->externalData['mod_request'])) $this->currentRecon->request = $this->externalData['mod_request'];
    if (isset($this->externalData['destination'])) $this->currentRecon->destination = $this->externalData['destination'];
    if (isset($this->externalData['status'])) $this->currentRecon->status = $this->externalData['status'];
    else if ($this->billerOrder) $this->currentRecon->status = $this->billerOrder->status;

    $this->updateStatusBasedOnConditions();

    if ($this->billerOrder) {
      $this->billerOrder->status = $this->currentRecon->status;
      $this->billerRepository->save($this->billerOrder);
    }

    if ($this->currentRecon->base_price != $this->currentSwitcher->current_base_price && $this->currentPulsaInventory->supply_pulsa_id == null) {
      $this->currentRecon->base_price = $this->currentSwitcher->current_base_price;
      if ($this->currentSwitcher->operator_id != null) {
        $this->currentPulsaInventory->base_price = $this->currentSwitcher->current_base_price;
      }
    }
    $this->currentRecon->reference_number = $this->currentSwitcher->serial_number;
    if ($this->billerOrder) {
      if ($this->currentRecon->request == null && isset($this->billerOrder->log_request))
        $this->currentRecon->request = $this->billerOrder->log_request;
      if ($this->currentRecon->response == null && isset($this->billerOrder->log_response))
        $this->currentRecon->response = $this->billerOrder->log_response;
      if (isset($this->billerOrder->log_notify)) $this->currentRecon->notify = $this->billerOrder->log_notify;
      $this->currentRecon->status = $this->billerOrder->status;
    }
    $this->reconRepository->save($this->currentRecon);

    $this->finalizeSwitcher($listener);

    if ($this->currentRecon && $this->currentRecon->status == SwitcherConfig::BILLER_SUCCESS
      && !$this->currentPulsaInventory->is_unit) {
      $mutationData = [
        'order_detail_pulsa_switcher_id' => $this->currentSwitcher->id,
        'vendor_switcher_id' => $this->currentBiller->id,
        'transaction_id' => $this->billerOrder ? $this->billerOrder->id : $this->currentRecon->id,
        'transaction_status' => $this->currentRecon->status,
        'transaction_status_prior' => SwitcherConfig::BILLER_FAIL,
        'amount' => ('-' . $this->currentRecon->base_price),
        'mutation_route' => $this->currentRoute
      ];
      if ($this->currentBiller->current_balance) $mutationData['current_balance'] = $this->currentBiller->current_balance;
      if (isset($this->externalData['auth']) && isset($this->externalData['auth']['user_id']))
        $mutationData['user_id'] = $this->externalData['auth']['user_id'];
      $listener->pushQueue(new InsertMutation($mutationData));
    }

    if ($return) return $listener->response(200, ['status' => $this->currentRecon->status]);
  }

  public function success(PipelineListener $listener) {
    $listener->addNext(new Task(OrderVerificator::class, 'verifyDetailAndCompleteOrder', [
      'order_detail_id' => $this->currentSwitcher->order_detail_id
    ]));

    $this->currentOrder->status = OrderStatus::COMPLETED;

    $this->currentPulsaInventory->last_purchased_at = date('Y-m-d H:i:s');
    $this->currentPulsa->last_succeeded_at = date('Y-m-d H:i:s');

    $temp = explode('/', $this->currentOrderDetail->name);
    $cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $cashTransaction = $cashManager->findOrderHistory($this->currentOrder, TransactionType::PAYMENT, true);
    $callbackMessage = trans('pulsa.inline.success', [
      'order_id' => $this->currentOrder->id,
      'item_name' => $temp[0],
      'number' => revertTelephone($this->currentSwitcher->number),
      'sn' => $this->currentSwitcher->serial_number != null ? $this->currentSwitcher->serial_number : 'N/A',
      'sisa_saldo' => $cashTransaction['message']
    ]);

    if ($this->currentOrder->gateway_id != Payment::SMS_CENTER) {
      $callbackMessage .= ' @' . date('d/m/Y H:i:s', strtotime($this->currentOrder->paid_at)) . '.';
    }

    $this->currentSwitcher->h2h_message = $callbackMessage;

    $this->gatewayCallback->callback($listener, $this->currentOrder, $callbackMessage,
      $this->currentSwitcher->current_response_status_code, [
        'number' => revertTelephone($this->currentSwitcher->number),
        'sn' => $this->currentSwitcher->serial_number != null ? $this->currentSwitcher->serial_number : 'N/A',
        'saldo' => intval($cashTransaction['balance_after'])
      ]);

    if ($this->currentSwitcher->sms_to && $this->currentSwitcher->sms_to != '') {
      $smsCenterManager = app()->make(\Odeo\Domains\Vendor\SMS\CenterManager::class);
      $smsCenterManager->reply(trans('pulsa.inline.success_for_customer', [
        'item_name' => $temp[0],
        'number' => revertTelephone($this->currentSwitcher->number),
        'sn' => $this->currentSwitcher->serial_number != null ? explode($this->currentSwitcher->serial_number, '/!/')[0] : 'N/A'
      ]), $this->currentSwitcher->sms_to);
    }

    if ($this->currentPulsaInventory->supply_pulsa_id != null) {
      if ($this->statusBefore != SwitcherConfig::BILLER_SUCCESS) {
        $userStores = app()->make(\Odeo\Domains\Subscription\Repository\UserStoreRepository::class);
        $cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
        $owner = $userStores->findSingleOwner($this->currentRecon->vendor_switcher_owner_store_id);

        $oldInventoryPrice = $this->currentPulsaInventory->base_price;
        $oldSupplyPulsaPrice = $this->currentPulsaSupply->price;

        if ($this->currentPulsaSupply->price != $this->currentRecon->base_price) {
          clog('marketplace_log', 'Supposed to be changed: ' . $this->currentPulsaSupply->id .
            ' from ' . $this->currentPulsaSupply->price . ' to ' . $this->currentRecon->base_price);
          $listener->pushQueue(new StabilizeAllStoreProfit([
            'inventories' => [
              $this->currentPulsaSupply->service_detail_id => [
                [
                  'inventory_id' => $this->currentPulsaSupply->id,
                  'price_before' => $this->currentPulsaSupply->price,
                  'price_after' => $this->currentRecon->base_price
                ]
              ]
            ]
          ]));
          $this->currentPulsaInventory->base_price += $this->currentRecon->base_price - $this->currentPulsaSupply->price;
          $this->currentPulsaSupply->price = $this->currentRecon->base_price;
          $this->currentSwitcher->current_base_price = $this->currentPulsaInventory->base_price;
          $this->pulsaOdeoRepository->save($this->currentPulsaSupply);
        }

        if ($this->currentRecon->sale_amount != null)
          $this->currentRecon->base_price = $oldSupplyPulsaPrice - $this->currentRecon->sale_amount;
        $this->currentRecon->sale_amount = $oldInventoryPrice;

        if ($this->currentBiller->status == SwitcherConfig::BILLER_STATUS_OK && $this->supplySettlementAt == '')
          $this->supplySettlementAt = Carbon::now()->addHour()->toDateTimeString();
        else if ($this->currentBiller->status != SwitcherConfig::BILLER_STATUS_OK)
          $this->supplySettlementAt = '';

        $supplySalesInserter = app()->make(\Odeo\Domains\Supply\Formatter\SupplySalesInserter::class);
        $supplySalesData = $supplySalesInserter->getCashInserterData([
          'user_id' => $owner->user_id,
          'order_id' => $this->currentRecon->order_id,
          'amount' => $this->currentPulsaInventory->base_price,
          'settlement_at' => $this->supplySettlementAt
        ]);
        foreach ($supplySalesData as $item) $cashInserter->add($item);

        $cashInserter->run();
      }
      else {
        clog('supply_log', 'ORDER ID HIT MORE THAN ONCE: ' . $this->currentOrder->id);
      }
    }

    $this->pulsaSwitcherRepository->save($this->currentSwitcher);
    $this->pulsaInventoryRepository->save($this->currentPulsaInventory);
    $this->pulsaOdeoRepository->save($this->currentPulsa);
  }
}
