<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:54 PM
 */

namespace Odeo\Domains\Biller;

use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Supply\Jobs\SendTransactionList;

class BillerReconciliationSelector implements SelectorListener {

  private $currencyHelper, $vendorRecons;

  public function __construct() {
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->vendorRecons = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    $output = [];
    $output['transaction_id'] = $item->transaction_id;
    $output['biller_transaction_id'] = $item->biller_transaction_id;
    $output['biller_name'] = $item->vendor_switcher_name;
    $output['name'] = $item->inventory_name;
    if (strpos($output['name'], PulsaCode::PLN) !== false) {
      $temp = explode('/', $output['name']);
      $output['name'] = $temp[0];
    }
    $output['number'] = revertTelephone($item->number);
    $output['price'] = $this->currencyHelper->formatPrice($item->base_price);
    $output['reference_number'] = $item->reference_number;
    $output['created_at'] = $item->created_at->format('Y-m-d H:i:s');
    return $output;

  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getRecon(PipelineListener $listener, $data) {
    $recons = [];

    $this->vendorRecons->normalizeFilters($data);
    $this->vendorRecons->setSimplePaginate(true);

    foreach ($this->vendorRecons->gets() as $order) {
      $recons[] = $this->_transforms($order, $this->vendorRecons);
    }

    if (sizeof($recons) > 0)
      return $listener->response(200, array_merge(
        ["recons" => $this->_extends($recons, $this->vendorRecons)],
        $this->vendorRecons->getPagination()
      ));

    return $listener->response(204, ["recons" => []]);
  }

  public function getReconForSupply(PipelineListener $listener, $data) {
    $supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    list ($isValid, $message) = $supplyValidator->checkStore($data['store_id'], $data);
    if (!$isValid) return $listener->response(400, $message);

    $recons = [];

    $this->vendorRecons->normalizeFilters($data);
    $this->vendorRecons->setSimplePaginate(true);

    foreach ($this->vendorRecons->getsForSupply() as $recon) {
      if ($recon->order_status == SwitcherConfig::SWITCHER_COMPLETED) $status = "SUCCESS";
      else if ($recon->order_status == SwitcherConfig::SWITCHER_REFUNDED) $status = "REFUNDED";
      else if (in_array($recon->order_status, [
        SwitcherConfig::SWITCHER_FAIL,
        SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER
      ])) $status = "FAILED";
      else $status = 'PENDING';

      $billerName = '';
      $pulsaName = $recon->inventory_name;
      $pulsaCode = null;
      if (strpos($recon->vendor_switcher_name, 'GOJEK') !== false) {
        if ($recon->destination != null) $pulsaName = $recon->inventory_name . ' [' . $recon->destination . ']';
        else {
          $temp = explode('number', $recon->request);
          if (isset($temp[1])) {
            $temp = explode('[', $temp[1]);
            $pulsaName = $recon->inventory_name . ' [' . trim($temp[0]) . ']';
          }
        }
      }
      else {
        $billerName = $recon->vendor_switcher_name;
        $pulsaCode = $recon->inventory_code;
      }

      $recons[] = [
        'id' => $recon->id,
        'order_id' => $recon->order_id,
        'biller' => [
          'id' => $recon->vendor_switcher_id,
          'name' => $billerName
        ],
        'pulsa' => [
          'id' => $recon->inventory_id,
          'name' => $pulsaName,
          'code' => $pulsaCode
        ],
        'user' => [
          'id' => $recon->order_user_id,
          'name' => $recon->order_user_name
        ],
        'number' => revertTelephone($recon->number),
        'base_price' => $this->currencyHelper->formatPrice($recon->base_price),
        'sale_price' => $recon->sale_amount != null ? $this->currencyHelper->formatPrice($recon->sale_amount < $recon->base_price ? $recon->base_price : $recon->sale_amount) : null,
        'profit' => $recon->sale_amount != null && $recon->sale_amount > 0 ? $this->currencyHelper->formatPrice($recon->sale_amount - $recon->base_price) : null,
        'reference_number' => $recon->reference_number,
        'logs' => [
          'to' => revertTelephone($recon->destination),
          'request' => $recon->request,
          'response' => $recon->response,
          'notify' => $recon->notify
        ],
        'created_at' => $recon->created_at->format('Y-m-d H:i:s'),
        'status' => [
          'code' => $recon->status,
          'label' => $status
        ]
      ];
    }

    if (sizeof($recons) > 0)
      return $listener->response(200, array_merge(
        ["orders" => $this->_extends($recons, $this->vendorRecons)],
        $this->vendorRecons->getPagination()
      ));

    return $listener->response(204, ["recons" => []]);
  }

  public function exportOrder(PipelineListener $listener, $data) {
    $supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    list ($isValid, $message) = $supplyValidator->checkStore($data['store_id'], $data);
    if (!$isValid) return $listener->response(400, $message);

    dispatch(new SendTransactionList($data));

    return $listener->response(200);
  }

}
