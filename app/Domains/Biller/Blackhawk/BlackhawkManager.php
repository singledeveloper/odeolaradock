<?php

namespace Odeo\Domains\Biller\Blackhawk;

use Odeo\Domains\Biller\Contract\BillerContract;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class BlackhawkManager implements BillerContract {

  private $purchaser, $inventoryValidator, $voidRequester;

  public function __construct() {
    $this->purchaser = app()->make(Purchaser::class);
    $this->inventoryValidator = app()->make(InventoryValidator::class);
    $this->voidRequester = app()->make(VoidRequester::class);
  }

  public function purchase(PipelineListener $listener, $data) {
    return $this->purchaser->purchase($listener, $data);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    $this->inventoryValidator->validateInventory($listener, $data);
  }

  public function void(PipelineListener $listener, $data) {
    return $this->voidRequester->void($listener, $data);
  }

}