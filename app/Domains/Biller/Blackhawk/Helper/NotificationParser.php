<?php

namespace Odeo\Domains\Biller\Blackhawk\Helper;

use Odeo\Domains\Constant\BillerBlackhawk;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Core\PipelineListener;

class NotificationParser {

  private $notifications;

  public function __construct() {
    $this->notifications = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
  }

  public function failureNotify(PipelineListener $listener, $order, $statusCode) {
    $this->notifications->setup($order->user_id, NotificationType::CHECK, NotificationGroup::TRANSACTION);

    if (in_array($statusCode, BillerBlackhawk::STATUS_REVERSAL) || $statusCode == '99')
      $this->notifications->googlePlaySoftDecline($order->id);
    else $this->notifications->googlePlayHardDecline($order->id);

    return $listener->pushQueue($this->notifications->queue());
  }
}
