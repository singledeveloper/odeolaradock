<?php

namespace Odeo\Domains\Biller\Blackhawk;

use Odeo\Domains\Biller\BillerManager;
use Odeo\Domains\Constant\BillerBlackhawk;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\GooglePlay\Jobs\SendGooglePlayVoucherCode;

class Purchaser extends BillerManager {

  private $client, $notificationParser, $statusBeforeReversal = '00', $urlManager;

  public function __construct() {
    parent::__construct(SwitcherConfig::BLACKHAWK);
    $this->client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ]
    ]);
    $this->notificationParser = app()->make(\Odeo\Domains\Biller\Blackhawk\Helper\NotificationParser::class);
    $this->urlManager = app()->make(\Odeo\Domains\Biller\Blackhawk\Helper\UrlManager::class);
  }

  public function purchase(PipelineListener $listener, $data) {
    if (!$this->initiate($listener, $data)) return $listener->response(200);

    $time = time();

    $json = [
      'request' => [
        'header' => [
          'signature' => BillerBlackhawk::SIGNATURE,
          'details' => [
            'productCategoryCode' => BillerBlackhawk::PCC,
            'specVersion' => BillerBlackhawk::VERSION
          ]
        ],
        'transaction' => [
          'primaryAccountNumber' => BillerBlackhawk::getPAN(),
          'processingCode' => BillerBlackhawk::PROCESSING_CODE,
          'transactionAmount' => sprintf('%012d', $this->currentSwitcher->currentInventory->code . '00000'),
          'transmissionDateTime' => date("ymdHis", $time),
          'systemTraceAuditNumber' => (string)rand('000000', '999999'),
          'localTransactionTime' => date("His", $time),
          'localTransactionDate' => date("ymd", $time),
          'merchantCategoryCode' => BillerBlackhawk::MCC,
          'pointOfServiceEntryMode' => BillerBlackhawk::POSEM,
          'acquiringInstitutionIdentifier' => BillerBlackhawk::MID,
          'retrievalReferenceNumber' => sprintf('%012d', $this->billerOrder->id),
          'merchantTerminalId' => BillerBlackhawk::getTerminalId(),
          'merchantIdentifier' => str_pad(BillerBlackhawk::MID, 15),
          'merchantLocation' => BillerBlackhawk::MERCHANT_LOC,
          'transactionCurrencyCode' => BillerBlackhawk::CURRENCY_CODE,
          'additionalTxnFields' => [
            'productId' => BillerBlackhawk::getUPC()
          ]
        ]
      ]
    ];

    $this->billerOrder->log_request = json_encode($json);

    try {
      $response = $this->client->request('POST', $this->urlManager->get(), ["json" => $json]);
      $this->billerOrder->log_response = $response->getBody()->getContents();

      $content = json_decode($this->billerOrder->log_response);
      try {
        if (in_array($content->response->transaction->responseCode, BillerBlackhawk::STATUS_SUCCESS)) {
          $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;
          $this->currentSwitcher->serial_number = $content->response->transaction->additionalTxnFields->activationAccountNumber
            . '#' . $content->response->transaction->additionalTxnFields->redemptionAccountNumber;
          $this->currentRecon->biller_transaction_id = $content->response->transaction->additionalTxnFields->transactionUniqueId;

          $order = $this->currentSwitcher->orderDetail->order;
          $listener->pushQueue(new SendGooglePlayVoucherCode($order, []));
        } else if (in_array($content->response->transaction->responseCode, BillerBlackhawk::STATUS_REVERSAL)) {
          $this->statusBeforeReversal = $content->response->transaction->responseCode;
          $this->reversal($listener, $data);
        } else {
          $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
          $this->notificationParser->failureNotify($listener, $this->currentSwitcher->orderDetail->order, $content->response->transaction->responseCode);
        }
      } catch (\Exception $e) {
        $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;
      }
    } catch (\Exception $e) {
      $this->timeoutMessage = $e->getMessage();
      $this->urlManager->change();
      $this->reversal($listener, $data);
    }

    return $this->finalize($listener);
  }

  public function reversal(PipelineListener $listener, $data) {
    if (isset($data['item'])) $this->load($data['item']);

    $json = json_decode($this->billerOrder->log_request, true);

    $json['request']['transaction']['systemTraceAuditNumber'] = (string)rand('000000', '999999');
    $json['request']['transaction']['transmissionDateTime'] = date("ymdHis", time());

    $this->billerOrder->log_reversal_request = json_encode($json);

    try {
      $response = $this->client->request('POST', $this->urlManager->get() . '/reverse', ["json" => $json]);
      $this->billerOrder->log_reversal_response = $response->getBody()->getContents();

      $content = json_decode($this->billerOrder->log_reversal_response);
      if (in_array($content->response->transaction->responseCode, BillerBlackhawk::STATUS_SUCCESS)) {
        $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
        $this->notificationParser->failureNotify($listener, $this->currentSwitcher->orderDetail->order, $this->statusBeforeReversal);
      } else {
        $this->billerOrder->status = SwitcherConfig::BILLER_IN_QUEUE;
      }
    } catch (\Exception $e) {
      $this->billerOrder->status = SwitcherConfig::BILLER_IN_QUEUE;
    }

    return $this->finalize($listener);

  }
}
