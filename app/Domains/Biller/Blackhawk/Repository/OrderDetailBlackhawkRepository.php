<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/19/17
 * Time: 5:56 PM
 */

namespace Odeo\Domains\Biller\Blackhawk\Repository;

use Odeo\Domains\Biller\Blackhawk\Model\OrderDetailBlackhawk;
use Odeo\Domains\Constant\BillerBlackhawk;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\BillerRepository;

class OrderDetailBlackhawkRepository extends BillerRepository {

  public function __construct(OrderDetailBlackhawk $orderDetailBlackhawk) {
    $this->setModel($orderDetailBlackhawk);
  }

  public function getPendingTransactions() {
    return $this->model->where('status', SwitcherConfig::BILLER_IN_QUEUE)->get();
  }

  public function findNormalTransaction($switcherId) {
    return $this->model->where('switcher_reference_id', $switcherId)
      ->where('transaction_type', BillerBlackhawk::TRX_TYPE_NORMAL)
      ->orderBy('id', 'desc')->first();
  }

}