<?php

namespace Odeo\Domains\Biller\Blackhawk;

use Odeo\Domains\Biller\BillerManager;
use Odeo\Domains\Constant\BillerBlackhawk;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class VoidRequester extends BillerManager {

  private $client, $urlManager;

  public function __construct() {
    parent::__construct(SwitcherConfig::BLACKHAWK);
    $this->client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ]
    ]);
    $this->urlManager = app()->make(\Odeo\Domains\Biller\Blackhawk\Helper\UrlManager::class);
  }

  public function void(PipelineListener $listener, $data) {

    if ($this->currentRecon = $this->reconRepository->findBySwitcherId($data['order_detail_pulsa_switcher_id'], $this->currentBiller->id)) {
      $data['no_recon'] = true;
      $data['route'] = $this->currentRoute;
      $this->initiate($listener, $data);

      $time = time();
      $pan = BillerBlackhawk::getPAN();
      $trxId = $this->currentRecon->biller_transaction_id;

      $json = [
        'request' => [
          'header' => [
            'signature' => BillerBlackhawk::SIGNATURE,
            'details' => [
              'productCategoryCode' => BillerBlackhawk::PCC,
              'specVersion' => BillerBlackhawk::VERSION
            ]
          ],
          'transaction' => [
            'primaryAccountNumber' => $pan,
            'processingCode' => BillerBlackhawk::VOID_PROCESSING_CODE,
            'transactionAmount' => sprintf('%012d', $this->currentSwitcher->currentInventory->code . '00000'),
            'transmissionDateTime' => date("ymdHis", $time),
            'systemTraceAuditNumber' => (string) rand('000000', '999999'),
            'localTransactionTime' => date("His", $time),
            'localTransactionDate' => date("ymd", $time),
            'merchantCategoryCode' => BillerBlackhawk::MCC,
            'pointOfServiceEntryMode' => BillerBlackhawk::POSEM,
            'acquiringInstitutionIdentifier' => BillerBlackhawk::MID,
            'retrievalReferenceNumber' => sprintf('%012d', $this->billerOrder->id),
            'merchantTerminalId' => BillerBlackhawk::getTerminalId(),
            'merchantIdentifier' => str_pad(BillerBlackhawk::MID, 15),
            'merchantLocation' => BillerBlackhawk::MERCHANT_LOC,
            'transactionCurrencyCode' => BillerBlackhawk::CURRENCY_CODE,
            'additionalTxnFields' => [
              'productId' => BillerBlackhawk::getUPC(),
              'referenceTransactionId' => $trxId
            ]
          ]
        ]
      ];

      $this->billerOrder->log_request = json_encode($json);
      $this->billerOrder->transaction_type = BillerBlackhawk::TRX_TYPE_VOID;

      try {
        $response = $this->client->request('POST', $this->urlManager->get(), ["json" => $json]);
        $this->billerOrder->log_response = $response->getBody()->getContents();

        $content = json_decode($this->billerOrder->log_response);
        try {
          if (in_array($content->response->transaction->responseCode, BillerBlackhawk::STATUS_SUCCESS)) {
            $this->billerOrder->status = SwitcherConfig::BILLER_VOIDED;
            $this->currentRecon->biller_transaction_id = $content->response->transaction->additionalTxnFields->transactionUniqueId;
          }
          else if (in_array($content->response->transaction->responseCode, BillerBlackhawk::STATUS_REVERSAL))
            $this->reversal($listener, $data);
          else $this->billerOrder->status = SwitcherConfig::BILLER_VOID_FAIL;
        }
        catch (\Exception $e) {
          $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;
        }
      }
      catch (\Exception $e) {
        $this->timeoutMessage = $e->getMessage();
        $this->reversal($listener, $data);
      }

      return $this->finalize($listener);
    }

    return $listener->response(400, "Can't void the transaction.");

  }

  public function reversal(PipelineListener $listener, $data) {
    if (isset($data['item'])) $this->load($data['item']);
    if (!$this->currentRecon) $this->currentRecon = $this->reconRepository->findBySwitcherId($this->billerOrder->switcher_reference_id, $this->currentBiller->id);

    $json = json_decode($this->billerOrder->log_request, true);

    $json['request']['transaction']['systemTraceAuditNumber'] = (string) rand('000000', '999999');
    $json['request']['transaction']['transmissionDateTime'] = date("ymdHis", time());

    $this->billerOrder->log_reversal_request = json_encode($json);

    try {
      $response = $this->client->request('POST', $this->urlManager->get() . '/reverse', ["json" => $json]);
      $this->billerOrder->log_reversal_response = $response->getBody()->getContents();

      $content = json_decode($this->billerOrder->log_reversal_response);
      if (in_array($content->response->transaction->responseCode, BillerBlackhawk::STATUS_SUCCESS)) {
        $this->billerOrder->status = SwitcherConfig::BILLER_VOID_FAIL;
      }
      else $this->billerOrder->status = SwitcherConfig::BILLER_IN_QUEUE;
    }
    catch (\Exception $e) {
      $this->billerOrder->status = SwitcherConfig::BILLER_IN_QUEUE;
    }

    return $this->finalize($listener);

  }
}