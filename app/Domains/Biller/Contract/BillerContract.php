<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 5:25 PM
 */

namespace Odeo\Domains\Biller\Contract;

use Odeo\Domains\Core\PipelineListener;

interface BillerContract {

  public function purchase(PipelineListener $listener, $data);

  public function validateInventory(PipelineListener $listener, $data);

}