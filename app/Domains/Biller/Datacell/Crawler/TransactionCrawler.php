<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 9:41 PM
 */

namespace Odeo\Domains\Biller\Datacell\Crawler;

use Odeo\Domains\Constant\BillerDatacell;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;
use TesseractOCR;

class TransactionCrawler extends BillerManager {

  private $client;

  public function __construct() {
    parent::__construct(SwitcherConfig::DATACELL);
    $this->client = new \GuzzleHttp\Client(['cookies' => true]);
  }

  public function crawl() {

    $pipeline = new Pipeline;
    $pipeline->add(new Task(__CLASS__, 'check'));
    $pipeline->execute([]);

  }

  public function check(PipelineListener $listener, $data) {
    $mappingOperator = [
      Pulsa::OPERATOR_TELKOMSEL => "SIMPATI/AS",
      Pulsa::OPERATOR_INDOSAT => "MENTARI/IM3/STARONE",
      Pulsa::OPERATOR_XL => "XL",
      Pulsa::OPERATOR_THREE => "(3)THREE",
      Pulsa::OPERATOR_AXIS => "XL",
      Pulsa::OPERATOR_SMARTFREN => "SMARTFREN",
      Pulsa::OPERATOR_ESIA => "ESIA",
      Pulsa::OPERATOR_PLN => "PLN-PREPAID",
      Pulsa::OPERATOR_BOLT => "BOLT"
    ];

    try {
      $this->client->request('GET', BillerDatacell::WEB_TRANSACTION_URL . "cek.transaksi/");

      $datacellOrder = $this->billerRepository->getPendingTransaction();

      if (count($datacellOrder) > 0) {
        $this->client->get(BillerDatacell::WEB_TRANSACTION_URL . 'libs/captcha.php', [
          'save_to' => 'datacell_captcha.jpg'
        ]);

        $captcha = (new TesseractOCR('datacell_captcha.jpg'))->recognize();
        @unlink("datacell_captcha.jpg");

        foreach ($datacellOrder as $output) {
          $this->load($output);

          $log_response = simplexml_load_string($this->billerOrder->log_response);
          $temp = explode('SN Kami :', $log_response->message);
          $temp = explode('.', $temp[1]);
          $refID = trim($temp[0]);

          $log_request = simplexml_load_string($this->billerOrder->log_request);

          list($code, $denom) = explode('.', $log_request->oprcode);

          $response = $this->client->request('POST', BillerDatacell::WEB_TRANSACTION_URL . "cek.transaksi/postdata.php", [
            'form_params' => [
              "operator" => $mappingOperator[$this->currentSwitcher->operator_id],
              "nohp" => $this->currentSwitcher->number,
              "nominal" => $denom . '000',
              "tanggal" => date('Y-m-d', strtotime($this->billerOrder->created_at)),
              "captcha" => $captcha
            ]
          ]);

          preg_match_all('/<tr.*?>(.*?)<\/tr>/si', $response->getBody()->getContents(), $matches);
          foreach ($matches[1] as $item) {
            preg_match_all('/<td.*?>(.*?)<\/td>/si', $item, $output);

            if (sizeof($output[1]) > 0) {
              list($idTrx, $date, $code, $number, $message) = $output[1];
              if ($idTrx == $refID) {
                $this->billerOrder->log_notify = $message;

                if (strpos($message, 'GAGAL,') !== false || strpos($message, 'The process failed') !== false
                  || strpos($message, ': GAGAL') !== false) {
                  $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
                }
                else if (strpos($message, 'BERHASIL,') !== false || strpos($message, '00:BERHASIL') !== false) {
                  $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;
                }

                if ($this->billerOrder->status == SwitcherConfig::BILLER_SUCCESS) {
                  try {
                    if ($this->currentSwitcher->operator_id == Pulsa::OPERATOR_PLN) {
                      $temp = explode('TOKEN:', $message);
                      $temp = explode('/', $temp[1]);
                      $this->currentSwitcher->serial_number = trim($temp[0]);
                    }
                    else {
                      $temp = explode('SN Operator:', $message);
                      $this->currentSwitcher->serial_number = trim($temp[1]);
                    }
                  }
                  catch (\Exception $e) {
                    $temp = explode('00:BERHASIL ', $message);
                    $this->currentSwitcher->serial_number = trim(str_replace(['<', '>'], '', $temp[1]));
                  }
                  if (strpos(strtolower($this->currentSwitcher->serial_number), 'berhasil') !== false)
                    $this->currentSwitcher->serial_number = 'N/A';
                }
                $this->finalize($listener, false);
                break;
              }
            }
          }
        }
      }
    }
    catch(\Exception $e) {
      clog($this->currentRoute, $e->getMessage());
    }

  }
}