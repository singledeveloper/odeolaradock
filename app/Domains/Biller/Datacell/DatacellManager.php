<?php

namespace Odeo\Domains\Biller\Datacell;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\Contract\BillerContract;

class DatacellManager implements BillerContract {

  private $purchaser, $inventoryValidator, $repository;

  public function __construct() {
    $this->purchaser = app()->make(Purchaser::class);
    $this->inventoryValidator = app()->make(InventoryValidator::class);
    $this->repository = app()->make(\Odeo\Domains\Biller\Datacell\Repository\OrderDetailDatacellRepository::class);
  }

  public function purchase(PipelineListener $listener, $data) {
    if (app()->environment() != 'production') return $listener->response(200);
    return $this->purchaser->purchase($listener, $data);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    return $this->inventoryValidator->validateInventory($listener, $data);
  }

}