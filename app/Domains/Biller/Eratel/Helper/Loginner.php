<?php

namespace Odeo\Domains\Biller\Eratel\Helper;

use GuzzleHttp\Client;

class Loginner {

  const WEB_REPORT_AUTH_URL = 'https://report.narindo.com/login.php';

  public function login($userId, $userName, $password) {

    $client = new \Goutte\Client();
    $client->setClient(new Client(['cookies' => true, 'timeout' => 60]));
    $form = $client->request('GET', self::WEB_REPORT_AUTH_URL)->selectButton('Login')->form();
    $form['username'] = $userId;
    $form['msisdn'] = $userName;
    $form['password'] = $password;
    $client->submit($form);

    return $client;

  }

}

