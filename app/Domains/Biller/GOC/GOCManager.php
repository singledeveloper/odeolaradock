<?php

namespace Odeo\Domains\Biller\GOC;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\Contract\BillerContract;

class GOCManager implements BillerContract {

  private $purchaser, $inventoryValidator;

  public function __construct() {
    $this->inventoryValidator = app()->make(InventoryValidator::class);
    $this->purchaser = app()->make(Purchaser::class);
  }

  public function purchase(PipelineListener $listener, $data) {
    return $this->purchaser->purchase($listener, $data);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    return $this->inventoryValidator->validateInventory($listener, $data);
  }

}