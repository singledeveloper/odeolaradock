<?php

namespace Odeo\Domains\Biller\Gojek\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Store;

class GojekUser extends Entity
{
  public function store() {
    return $this->belongsTo(Store::class);
  }
}
