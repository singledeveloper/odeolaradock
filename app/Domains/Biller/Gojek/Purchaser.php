<?php

namespace Odeo\Domains\Biller\Gojek;

use Illuminate\Support\Facades\Crypt;
use Odeo\Domains\Biller\Gojek\Helper\GojekManager;
use Odeo\Domains\Biller\Gojek\Jobs\UpdatePurchaserToAdminNote;
use Odeo\Domains\Constant\Gojek;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class Purchaser extends GojekManager {

  private $redisLockName, $redisDepositCounterName;

  public function __construct() {
    parent::__construct();
  }

  public function purchase(PipelineListener $listener, $data) {

    \DB::beginTransaction();

    $this->redisLockName = Gojek::REDIS_KEY_USER_TRANSACTION_LOCK . $data['store_id'];

    try {
      if (isset($data['gojek_user_id']))
        $gojekUser = $this->gojekUsers->findByIdLocked($data['gojek_user_id']);
      else $gojekUser = $this->gojekUsers->findBiggestBalance($data['store_id'], $data['amount'], $this->getLock());
    }
    catch (\PDOException $e) {
      \DB::commit();
      return [
        'status' => SwitcherConfig::BILLER_FAIL,
        'response' => $e->getMessage()
      ];
    }

    if ($gojekUser) {
      $this->addLock($gojekUser->id);
      $this->redisDepositCounterName = Gojek::REDIS_KEY_DEPOSIT_COUNTER_LOCK . $gojekUser->id;
      $request = ' using number ' . revertTelephone($gojekUser->telephone);

      if (!isProduction()) {
        $this->removeLock($gojekUser->id);
        return [
          'destination' => $gojekUser->telephone,
          'request' => 'Test' . $request,
          'ref' => rand('00000000', '99999999'),
          'status' => SwitcherConfig::BILLER_SUCCESS,
          'response' => '',
          'balance' => 0
        ];
      }

      $this->setCurrentId($gojekUser->id);
      $this->setToken($gojekUser->token);
      $this->setUniqueId($gojekUser->unique_id);

      $response = $this->requestWithRetry('GET', '/wallet/qr-code?phone_number=%2B' . purifyTelephone($data['msisdn']));
      if ($this->errorMessage != '') {
        $this->removeLock($gojekUser->id);
        $result = [
          'destination' => $gojekUser->telephone,
          'request' => 'Get QR-Code' . $request,
          'status' => SwitcherConfig::BILLER_FAIL,
          'response' => $this->errorMessage
        ];
        if (strpos($this->errorMessage, 'Entity Not Found') !== false ||
          strpos($this->errorMessage, "couldn't find your wallet") !== false)
          $result['response_code'] = Supplier::RC_FAIL_UNREGISTERED_NUMBER;
        return $result;
      }

      try {
        $qrId = $response->data->qr_id;
      }
      catch(\Exception $e) {
        $this->removeLock($gojekUser->id);
        return [
          'destination' => $gojekUser->telephone,
          'request' => 'Get QR-Code' . $request,
          'status' => SwitcherConfig::BILLER_FAIL,
          'response' => 'QR NULL'
        ];
      }

      dispatch(new UpdatePurchaserToAdminNote($data['order_detail_pulsa_switcher_id'], revertTelephone($gojekUser->telephone)));

      $response = $this->requestWithRetry('POST', '/v2/fund/transfer', [
        'qr_id' => $qrId,
        'amount' => $data['amount'],
        'description' => ''
      ], [
        'pin' => Crypt::decrypt($gojekUser->pin)
      ]);

      if ($this->errorMessage != '') {
        $this->removeLock($gojekUser->id);
        $result = [
          'destination' => $gojekUser->telephone,
          'request' => 'Transfer' . $request,
          'status' => $this->responseStatus,
          'response' => $this->errorMessage
        ];
        if (strpos($this->errorMessage, 'Cannot transfer money') !== false ||
          strpos($this->errorMessage, "couldn't find your wallet") !== false)
          $result['response_code'] = Supplier::RC_FAIL_UNREGISTERED_NUMBER;
        else if (strpos($this->errorMessage, 'balance is not enough') !== false) {
          $this->errorMessage = '';
          $response = $this->request('GET', '/wallet/profile/detailed');
          if ($this->errorMessage == '') {
            try {
              $gojekUser->current_balance = $response->data->balance;
            }
            catch(\Exception $e) {}
          }
        }
        $this->gojekUsers->save($gojekUser);
        return $result;
      }

      $transferResponse = $response;

      $depoCounter = $this->getDepositCounter();
      if ($depoCounter >= Gojek::MAX_DEPOSIT_COUNTER || $gojekUser->current_balance > 10000000) {
        $this->errorMessage = '';
        $response = $this->request('GET', '/wallet/profile/detailed');
        if ($this->errorMessage == '') {
          try {
            $gojekUser->current_balance = $response->data->balance;
          }
          catch(\Exception $e) {}
        }
        $this->setDepositCounter(1);
      }
      else {
        $gojekUser->current_balance -= $data['amount'];
        $this->setDepositCounter($depoCounter + 1);
      }

      if ($gojekUser->store_id != null)
        $gojekUser->monthly_cash_in += $data['amount'];

      $this->gojekUsers->save($gojekUser);

      $balance = $this->gojekUsers->getTotalBalance($gojekUser->store_id);
      if (!$balance) $balance = 0;
      else $balance = $balance->total_balance;

      $this->removeLock($gojekUser->id);
      if (isset($data['store_id'])) $this->redis->hdel(Gojek::REDIS_GOJEK_NO_ACC_COUNT, $data['store_id']);
      return [
        'destination' => $gojekUser->telephone,
        'request' => 'Transfer' . $request . ' [' . $gojekUser->unique_id . '-' . $this->getProxyData() . ']',
        'ref' => isset($transferResponse->data) && isset($transferResponse->data->transaction_ref) ? $transferResponse->data->transaction_ref : 'N/A',
        'status' => SwitcherConfig::BILLER_SUCCESS,
        'response' => json_encode($transferResponse),
        'balance' => $balance
      ];
    }

    \DB::commit();

    if (isset($data['store_id'])) {
      $supps = Gojek::GOJEK_SUPPS;
      $count = $this->redis->hincrby(Gojek::REDIS_GOJEK_NO_ACC_COUNT, $data['store_id'], 1);
      if (isset($supps[$data['store_id']])) {
        if ($count % 10 == 0) {
          $notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
          $notification->pushOnly();
          $notification->setup($supps[$data['store_id']], NotificationType::WARNING, NotificationGroup::TRANSACTION);
          $notification->gojekSupplierNoAccount();
          $listener->pushQueue($notification->queue());
        }
      }
      if ($count >= 20 && $data['store_id'] != 5239) {
        $pulsaOdeoInventoryRepo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
        $pulsaOdeoRepo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
        foreach ($pulsaOdeoInventoryRepo->findByVendorSwitcherId($data['vendor_switcher_id']) as $item) {
          if ($item->is_active) {
            $item->is_active = false;
            $pulsaOdeoInventoryRepo->save($item);
            if ($item->supply_pulsa_id) {
              $pulsa = $pulsaOdeoRepo->findById($item->supply_pulsa_id);
              $pulsa->is_active = false;
              $pulsaOdeoRepo->save($pulsa);
            }
          }
        }
        $this->redis->hdel(Gojek::REDIS_GOJEK_NO_ACC_COUNT, $data['store_id']);
      }
    }

    return [
      'status' => SwitcherConfig::BILLER_FAIL,
      'response' => 'No Gojek account exist'
    ];

  }

  private function getLock() {
    if (!$lockIds = $this->redis->hget(Gojek::REDIS_GOJEK, $this->redisLockName))
      return [];
    return explode(',', $lockIds);
  }

  private function addLock($id) {
    $lockIds = $this->getLock();
    $lockIds[] = $id;
    $this->redis->hset(Gojek::REDIS_GOJEK, $this->redisLockName, implode(',', $lockIds));
  }

  private function removeLock($id) {
    $lockIds = $this->getLock();
    $newLockIds = [];
    foreach ($lockIds as $item) {
      if ($item != $id) $newLockIds[] = $item;
    }
    $this->redis->hset(Gojek::REDIS_GOJEK, $this->redisLockName, implode(',', $newLockIds));
    \DB::commit();
  }

  private function getDepositCounter() {
    if (!$depoCounter = $this->redis->hget(Gojek::REDIS_GOJEK, $this->redisDepositCounterName))
      $depoCounter = 1;
    return $depoCounter;
  }

  private function setDepositCounter($depoCounter) {
    $this->redis->hset(Gojek::REDIS_GOJEK, $this->redisDepositCounterName, $depoCounter);
  }

}

