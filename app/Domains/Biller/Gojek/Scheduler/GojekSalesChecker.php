<?php

namespace Odeo\Domains\Biller\Gojek\Scheduler;

use Odeo\Domains\Constant\SwitcherConfig;

class GojekSalesChecker {

  public function __construct() {
    clog('debug_scheduler', __CLASS__ . 'constructor called');
  }

  public function run() {
    clog('debug_scheduler', __CLASS__ . 'run called');
    $vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $vendorRecons = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository::class);
    $gojekUsers = app()->make(\Odeo\Domains\Biller\Gojek\Repository\GojekUserRepository::class);

    $ownerStoreIds = [];
    foreach ($vendorSwitchers->getByActiveStatus(SwitcherConfig::BILLER_STATUS_HIDDEN_GOJEK) as $item) {
      if ($item->owner_store_id != null) $ownerStoreIds[] = $item->owner_store_id;
    }

    $result = $vendorRecons->getMonthRecapByStatus(SwitcherConfig::BILLER_STATUS_HIDDEN_GOJEK, $ownerStoreIds);

    foreach ($gojekUsers->getAllSuppliers() as $item) {
      $actualSales = isset($result[$item->telephone]) ? $result[$item->telephone]->total_sales : 0;
      if ($actualSales != $item->monthly_cash_in) {
        $item->monthly_cash_in = $actualSales;
        $gojekUsers->save($item);
      }
    }

  }

}
