<?php

namespace Odeo\Domains\Biller\Gojek\Scheduler;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Gojek;

class GojekVersionUpdater {

  private $redis;

  public function __construct() {
    clog('debug_scheduler', __CLASS__ . 'constructor called');
    $this->redis = Redis::connection();
  }

  public function run() {
    clog('debug_scheduler', __CLASS__ . 'run called');
    try {
      $client = new \GuzzleHttp\Client([
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ]
      ]);

      $response = json_decode($client->request('GET', 'https://api.appmonsta.com/v1/stores/android/details/com.gojek.app.json?country=US', [
        'auth' => ['2206d3d49e65ac4b51b75d032615feb75cd21ff9', 'X']
      ])->getBody()->getContents());

      $this->redis->hset(Gojek::REDIS_GOJEK, Gojek::REDIS_KEY_GOJEK_VERSION, $response->version);
    }
    catch (\Exception $e) {}
  }

}
