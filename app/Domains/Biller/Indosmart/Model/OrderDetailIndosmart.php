<?php

namespace Odeo\Domains\Biller\Indosmart\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\OrderDetailPulsaSwitcher;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\PulsaOdeoInventory;

class OrderDetailIndosmart extends Entity
{

  public function inventory() {
    return $this->belongsTo(PulsaOdeoInventory::class, 'inventory_id');
  }

  public function switcher() {
    return $this->belongsTo(OrderDetailPulsaSwitcher::class, 'switcher_reference_id');
  }
}
