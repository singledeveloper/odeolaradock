<?php

namespace Odeo\Domains\Biller\Jatelindo;

use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class InventoryValidator {

  private $pulsaInventories, $marginFormatter, $orderDetail;

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->orderDetail = app()->make(\Odeo\Domains\Biller\Jatelindo\Repository\OrderDetailJatelindoRepository::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
  }

  public function validateInventory(PipelineListener $listener, $data) {

    if ($inventory = $this->pulsaInventories->findById($data['inventory_id'])) {
      $pulsa = $inventory->pulsa;
      $formatted = $this->marginFormatter->formatMargin($pulsa->price, $data);

      return $listener->response(200, array_merge($formatted, [
        'inventory_id' => $inventory->id,
        'name' => $pulsa->name
      ]));
    }

    return $listener->response(400, trans('pulsa.cant_purchase_no_backup'));
  }
}
