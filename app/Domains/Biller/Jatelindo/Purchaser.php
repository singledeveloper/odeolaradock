<?php

namespace Odeo\Domains\Biller\Jatelindo;

use Odeo\Domains\Constant\BillerJatelindo;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class Purchaser extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::JATELINDO);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);

    $this->billerOrder->transaction_id = sprintf("%06d", $this->billerRepository->getTodayLatestId());

    $number = revertTelephone($this->currentSwitcher->number);
    
    list($pan, $invCode, $denom) = explode(".", $this->currentSwitcher->currentInventory->code);
    
    $time = time();
    
    $bits = [
      "2" => $pan,
      "3" => BillerJatelindo::PURCHASE_PROCESSING_CODE,
      "4" => sprintf('%012d', $denom . '000'),
      "7" => date("mdGis", $time),
      "11" => $this->billerOrder->transaction_id,
      "12" => date("Gis", $time),
      "13" => date("md", $time),
      "15" => date("md", $time + (60 * 60 * 24)),
      "18" => BillerJatelindo::MERCHANT_TYPE,
      "32" => BillerJatelindo::AIID,
      "37" => sprintf('%012d', $this->billerOrder->transaction_id),
      "41" => BillerJatelindo::getTerminalId(),
      "42" => BillerJatelindo::getAcceptorId(),
      "48" => str_pad($invCode . $number, 18),
      "49" => BillerJatelindo::CURRENCY_CODE
    ];

    $purchaseISO = BillerJatelindo::encodeISO(BillerJatelindo::PURCHASE_MTI, $bits);
    $this->billerOrder->log_purchase_request = json_encode($bits);
    
    list($ip, $port) = explode(":", BillerJatelindo::getHost());

    $adviceReq = ''; $adviceRes = '';

    try {
      $sock = socket_create(AF_INET, SOCK_STREAM, 0);
      socket_connect($sock, $ip, $port);
      socket_set_option($sock, SOL_SOCKET, SO_RCVTIMEO, ["sec"=> 60, "usec" => 0]);
      socket_write($sock, BillerJatelindo::formatRequest($purchaseISO));
      
      if (false === ($result = socket_read($sock, 1024))) {
        list($statusTransaction, $adviceReq, $adviceRes) = $this->advice($bits);
      }
      else {
        try {
          $response = BillerJatelindo::decodeISO(preg_replace("/[^A-Za-z0-9 -]/", '', $result));
          $this->billerOrder->log_purchase_response = json_encode($response);
          
          if (isset($response["39"]) && $response["39"] == "00") {
            $statusTransaction = SwitcherConfig::BILLER_SUCCESS;
            list($code, $operator, $msisdn, $nominal, $admin, $expire, $sn, $no_ref, $datetime) =
              BillerJatelindo::getParts($response['48'], [4, 4, 10, 12, 8, 8, 16, 32, 14]);
            $this->currentSwitcher->serial_number = $sn;
            $this->currentRecon->biller_transaction_id = $no_ref;
            $this->currentSwitcher->current_base_price = intval($nominal);
          }
          else if (isset($response["39"]) && ($response["39"] == "18" || $response["39"] == "06" || $response["39"] == "09"))
            list($statusTransaction, $adviceReq, $adviceRes) = $this->advice($bits);
          else $statusTransaction = SwitcherConfig::BILLER_FAIL;
        }
        catch (\Exception $e) {
          $statusTransaction = SwitcherConfig::BILLER_SUSPECT;
        }
      }
      
      socket_close($sock);
    }
    catch (\Exception $e) {
      $statusTransaction = SwitcherConfig::BILLER_SUSPECT;
    }

    if ($adviceReq != "") $this->billerOrder->log_advice_request = $adviceReq;
    if ($adviceRes != "") $this->billerOrder->log_advice_response = $adviceRes;
    $this->billerOrder->status = $statusTransaction;

    $this->currentRecon->request = $this->billerOrder->log_purchase_request;
    $this->currentRecon->response = $this->billerOrder->log_purchase_response;
    $this->currentRecon->notify = $this->billerOrder->log_advice_response;

    return $this->finalize($listener);
  }
  
  public function advice($bits) {
    $adviceRes = '';
    $bits["3"] = BillerJatelindo::ADVICE_PROCESSING_CODE;
    $adviceISO = BillerJatelindo::encodeISO(BillerJatelindo::ADVICE_MTI, $bits);
    $adviceReq = json_encode($bits);
    list($ip, $port) = explode(":", BillerJatelindo::getHost());
    
    try {
      $sock = socket_create(AF_INET, SOCK_STREAM, 0);
      socket_connect($sock, $ip, $port);
      socket_set_option($sock, SOL_SOCKET, SO_RCVTIMEO, ["sec"=> 60, "usec" => 0]);
      socket_write($sock, BillerJatelindo::formatRequest($adviceISO));
      
      if (false === ($result = socket_read($sock, 1024))) {
        $statusTransaction = SwitcherConfig::BILLER_TIMEOUT;
      }
      else {
        try {
          $response = BillerJatelindo::decodeISO(preg_replace("/[^A-Za-z0-9 ]/", '', $result));
          $adviceRes = json_encode($response);
          
          if (isset($response["39"]) && $response["39"] == "00") {
            $statusTransaction = SwitcherConfig::BILLER_SUCCESS;
            list($code, $operator, $msisdn, $nominal, $admin, $expire, $sn, $no_ref, $datetime) =
              BillerJatelindo::getParts($response['48'], [4, 4, 10, 12, 8, 8, 16, 32, 14]);
            $this->currentSwitcher->serial_number = $sn;
            $this->currentRecon->biller_transaction_id = $no_ref;
            $this->currentSwitcher->current_base_price = intval($nominal);
          }
          else if (isset($response["39"]) && ($response["39"] != "18" && $response["39"] != "06" && $response["39"] != "09"))
            $statusTransaction = SwitcherConfig::BILLER_FAIL;
          else $statusTransaction = SwitcherConfig::BILLER_IN_QUEUE;
        }
        catch (\Exception $e) {
          $statusTransaction = SwitcherConfig::BILLER_SUSPECT;
        }
      }
      
      socket_close($sock);
    }
    catch (\Exception $e) {
      $statusTransaction = SwitcherConfig::BILLER_IN_QUEUE;
    }
    
    return [$statusTransaction, $adviceReq, $adviceRes];
  }
}