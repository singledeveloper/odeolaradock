<?php

namespace Odeo\Domains\Biller\Jatelindo\Scheduler;

use Odeo\Domains\Biller\Jatelindo\Purchaser;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class TransactionChecker extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::JATELINDO);
  }

  public function run() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(__CLASS__, 'check'));
    $pipeline->execute([]);
  }
  
  public function check(PipelineListener $listener, $data) {
    $orders = $this->billerRepository->getPendingTransaction();
    foreach ($orders as $output) {
      $this->load($output);

      list($statusTransaction, $adviceReq, $adviceRes) =
        app()->make(Purchaser::class)->advice(json_decode($this->billerOrder->log_purchase_request, true));

      if ($adviceReq != "") $this->billerOrder->log_advice_request = $adviceReq;
      if ($adviceRes != "") $this->billerOrder->log_advice_response = $adviceRes;
      $this->billerOrder->status = $statusTransaction;

      $this->finalize($listener, false);
    }
  }
}
