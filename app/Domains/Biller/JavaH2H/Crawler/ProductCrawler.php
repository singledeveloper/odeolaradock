<?php

namespace Odeo\Domains\Biller\JavaH2H\Crawler;

use Goutte\Client;
use GuzzleHttp\Exception\ConnectException;
use Odeo\Domains\Constant\BillerJavaH2H;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository;

class ProductCrawler {
  private $client, $codes = [];

  public function __construct() {
    $this->pulsaInventories = app()->make(PulsaOdeoInventoryRepository::class);
    $this->client = new Client();
    $this->client->setClient(new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ]
    ]));
  }

  public function crawl() {

    try {
      $crawler = $this->client->request('GET', BillerJavaH2H::WEB_PRICELIST_PULSA);

      $inventories = $this->pulsaInventories->findByVendorSwitcherId(SwitcherConfig::JAVAH2H);
      $listInventories = [];

      foreach ($inventories as $item) {
        if (!isset($listInventories[$item->code])) $listInventories[$item->code] = [];
        $listInventories[$item->code][] = $item;
      }
      $tr = $crawler->filter('table > tbody > tr');

      $tr->each(function ($node) use ($listInventories) {
        $td = $node->filter('td');

        $td->each(function ($node, $i) use (&$data) {
          switch ($i) {
            case 1:
              $data['code'] = $node->text();
              break;
            case 2:
              $data['price'] = str_replace('.', '', $node->text());
              break;
            case 3:
              $data['status'] = $node->text();
          }
        });

        if (isset($listInventories[$data['code']]) && $data['status'] != 'gangguan') {
          $this->codes[] = $data['code'];
          foreach ($listInventories[$data['code']] as $item) {
            $item->base_price = $data['price'];
            $item->is_active = true;
            $this->pulsaInventories->save($item);
          }
        }

      });

      $this->pulsaInventories->finalizeProducts(SwitcherConfig::JAVAH2H, $this->codes);

    } catch (ConnectException $e) {
      return [false, 'Error while scraping from JavaH2H, connection error'];
    }

  }

}