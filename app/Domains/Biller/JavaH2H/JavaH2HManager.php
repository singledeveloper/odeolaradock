<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/15/17
 * Time: 12:00 PM
 */

namespace Odeo\Domains\Biller\JavaH2H;

use Odeo\Domains\Biller\Contract\BillerContract;
use Odeo\Domains\Core\PipelineListener;

class JavaH2HManager implements BillerContract {

  private $purchaser, $inventoryValidator;

  public function __construct() {
    $this->inventoryValidator = app()->make(InventoryValidator::class);
    $this->purchaser = app()->make(Purchaser::class);
  }

  public function purchase(PipelineListener $listener, $data) {
    return $this->purchaser->purchase($listener, $data);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    $this->inventoryValidator->validateInventory($listener, $data);
  }

}