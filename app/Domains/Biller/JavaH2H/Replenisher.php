<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/16/17
 * Time: 3:23 PM
 */

namespace Odeo\Domains\Biller\JavaH2H;

use Odeo\Domains\Biller\Contract\ReplenishmentContract;
use Odeo\Domains\Constant\BillerJavaH2H;
use Odeo\Domains\Core\PipelineListener;

class Replenisher implements ReplenishmentContract {

  private $headers, $client;

  public function __construct() {

    $this->client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ],
      'timeout' => 90
    ]);
    $this->headers = [
      "h2h-userid" => BillerJavaH2H::USER_ID,
      "h2h-key" => BillerJavaH2H::PROD_KEY,
      "h2h-secret" => BillerJavaH2H::PROD_SECRET
    ];

  }

  public function request(PipelineListener $listener, $data) {

    $response = $this->client->request('POST', BillerJavaH2H::PATH, [
      "form_params" => [
        "inquiry" => BillerJavaH2H::INQUIRY_DEPOSIT,
        "bank" => isset($data['bank']) ? $data['bank'] : 'bca',
        "nominal" => $data['amount']
      ],
      "headers" => $this->headers
    ]);

    $response = json_decode($response->getBody()->getContents(), true);

    if (isset($data['in_pipeline'])) return $listener->response(200, $response);

    preg_match("/.* transfer sebesar Rp (.*),- .*/", $response['message'], $matches);
    list($a, $amount) = $matches;

    return ['amount' => str_replace('.', '', $amount)];
  }

  public function complete(PipelineListener $listener, $data) {
    return [];
  }

}