<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/15/17
 * Time: 12:30 PM
 */

namespace Odeo\Domains\Biller\JavaH2H\Repository;

use Odeo\Domains\Biller\JavaH2H\Model\OrderDetailJavaH2h;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\BillerRepository;

class OrderDetailJavaH2hRepository extends BillerRepository {

  public function __construct(OrderDetailJavaH2h $orderDetailJava) {
    $this->setModel($orderDetailJava);
  }

}