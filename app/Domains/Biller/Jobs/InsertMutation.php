<?php

namespace Odeo\Domains\Biller\Jobs;

use Carbon\Carbon;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Disbursement\BillerReplenishment\Jobs\BeginReplenishment;
use Odeo\Jobs\Job;

class InsertMutation extends Job  {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    $billerMutationRecons = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationMutationRepository::class);
    $billerMutations = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherMutationRepository::class);
    $billers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $data = $this->data;

    \DB::beginTransaction();

    try {
      $billerMutation = $billerMutations->findByVendorSwitcherIdLocked($data['vendor_switcher_id']);
      $biller = $billers->findById($data['vendor_switcher_id']);

      $continue = true;
      if (isset($data['transaction_id']) && isset($data['transaction_status']) && isset($data['transaction_status_prior'])) {
        $diff = $billerMutationRecons->findDifferenceInSameTransaction($data['vendor_switcher_id'], $data['transaction_id']);

        if ($data['transaction_status_prior'] == SwitcherConfig::BILLER_SUCCESS)
          $continue = ($diff->total == 0 && $data['transaction_status'] == SwitcherConfig::BILLER_SUCCESS) ||
            ($diff->total < 0 && $data['transaction_status'] != SwitcherConfig::BILLER_SUCCESS);
        else
          $continue = ($diff->total == 0 && $data['transaction_status'] != SwitcherConfig::BILLER_FAIL) ||
            ($diff->total < 0 && $data['transaction_status'] == SwitcherConfig::BILLER_FAIL);
      }

      if ($continue) {
        $billerRecon = $billerMutationRecons->getNew();
        $billerRecon->order_detail_pulsa_switcher_id = isset($data['order_detail_pulsa_switcher_id']) ? $data['order_detail_pulsa_switcher_id'] : null;
        $billerRecon->vendor_switcher_id = $data['vendor_switcher_id'];
        $billerRecon->transaction_id = isset($data['transaction_id']) ? $data['transaction_id'] : null;

        $billerRecon->amount = trim($data['amount']);
        if (isset($data['current_balance']) && intval($data['current_balance'])) {
          $biller->current_balance = trim($data['current_balance']);
          $billers->save($biller);
        }

        if (!$billerMutation) {
          $billerMutation = $billerMutations->getNew();
          $billerMutation->vendor_switcher_id = $data['vendor_switcher_id'];
          $billerRecon->balance_after = intval($biller->current_balance);
        } else {
          $billerRecon->balance_before = $billerMutation->current_mutation;
          $billerRecon->balance_after = $billerMutation->current_mutation + $data['amount'];
        }
        $billerRecon->last_balance_recorded_from_biller = intval($biller->current_balance);
        $billerRecon->route = isset($data['mutation_route']) ? $data['mutation_route'] : null;
        $billerRecon->user_id = isset($data['user_id']) ? $data['user_id'] : null;
        $billerMutationRecons->save($billerRecon);

        $billerMutation->current_mutation = $billerRecon->balance_after;
        if ($billerRecon->balance_after == $billerRecon->last_balance_recorded_from_biller) {
          $billerMutation->last_zero_at = date('Y-m-d H:i:s');
        }
        $billerMutations->save($billerMutation);

        if ($biller->current_balance <= $biller->minimum_topup && $biller->is_auto_replenish) {
          dispatch(new BeginReplenishment([
            'vendor_switcher_id' => $biller->id,
            'start_time' => Carbon::now()->format('Y-m-d') . ' 00:00:00'
          ]));
        }
      }
    }
    catch (\Exception $e) {
      \DB::rollback();
      clog('mutation_failure', $e->getMessage());
      throw $e;
    }

    \DB::commit();

  }

}


