<?php

namespace Odeo\Domains\Biller\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SuspectDoublePurchase extends Job  {

  private $switcherOrder, $criminalName;

  public function __construct($switcherOrder, $criminalName) {
    parent::__construct();
    $this->switcherOrder = $switcherOrder;
    $this->criminalName = $criminalName;
  }

  public function handle() {

    if (app()->environment() == 'production') {
      Mail::send('emails.email_notice_double_purchase', ["switcherOrder" => $this->switcherOrder, "criminalName" => $this->criminalName], function ($m) {

        $m->from('noreply@odeo.co.id', 'odeo');
        $m->to('ops@odeo.co.id')->subject('Double Purchase Suspect - ' . time());
        //$m->to('brian.japutra@gmail.com')->subject('Double Purchase Suspect - ' . time());
      });

    }

  }

}


