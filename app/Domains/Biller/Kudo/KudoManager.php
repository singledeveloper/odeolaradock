<?php

namespace Odeo\Domains\Biller\Kudo;

use Odeo\Domains\Biller\Contract\BillerContract;
use Odeo\Domains\Core\PipelineListener;

class KudoManager implements BillerContract {
  private $purchaser, $inventoryValidator;

  public function __construct() {
    $this->purchaser = app()->make(Purchaser::class);
    $this->inventoryValidator = app()->make(InventoryValidator::class);
  }

  public function purchase(PipelineListener $listener, $data) {
    return $this->purchaser->purchase($listener, $data);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    $this->inventoryValidator->validateInventory($listener, $data);
  }

}