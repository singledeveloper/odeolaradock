<?php

namespace Odeo\Domains\Biller\Kudo\Repository;

use Odeo\Domains\Biller\Kudo\Model\OrderDetailKudo;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\BillerRepository;

class OrderDetailKudoRepository extends BillerRepository {

  public function __construct(OrderDetailKudo $orderDetailKudo) {
    $this->setModel($orderDetailKudo);
  }

  public function getPendingTransaction() {
    return $this->model->where('status', SwitcherConfig::BILLER_IN_QUEUE)
      ->orWhere('status', SwitcherConfig::BILLER_SUSPECT)->get();
  }

}