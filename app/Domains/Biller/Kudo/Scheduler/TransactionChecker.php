<?php

namespace Odeo\Domains\Biller\Kudo\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Biller\Kudo\Helper\KudoParser;
use Odeo\Domains\Constant\BillerKudo;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;

class TransactionChecker extends KudoParser {

  public function __construct() {
    parent::__construct();
  }

  public function run() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(__CLASS__, 'check'));
    $pipeline->execute([]);
  }

  public function check(PipelineListener $listener, $data) {
    $orders = $this->billerRepository->getPendingTransaction();
    if (sizeof($orders) > 0) {
      $this->biller = $this->vendorSwitchers->findById(SwitcherConfig::KUDO);

      if ($profile = $this->get(BillerKudo::SERVER . '/users/profile')) {
        $this->currentBalance = (isset($profile->message->original_balance) ? $profile->message->original_balance : $profile->message->balance) + $profile->message->commission;
      }

      $response = $this->postForm(BillerKudo::SERVER . '/order/history-transaction', [
        'page' => 1,
        'size' => 50,
        'order' => 'desc',
        'date_from' => Carbon::now()->subDays(6)->toDateString(),
        'date_to' => date('Y-m-d'),
        'status' => 0
      ]);

      $bulkData = [];
      foreach($response->message->orders as $item) {
        $bulkData[$item->order_id] = $item;
      }

      foreach ($orders as $output) {
        $this->load($output);

        $request = json_decode($this->billerOrder->log_response)->message;
        if (!isset($bulkData[$request->id])) {
          $responseDetail = $this->get(BillerKudo::SERVER . '/order/detail?order_split_id=' . $request->order_split_id);
          $bulkData[$request->id] = $responseDetail->message;
        }

        $this->billerOrder->log_notify = json_encode($bulkData[$request->id]);
        $orderDetail = $bulkData[$request->id]->order_items[0];
        $parser = json_decode($orderDetail->attributes);

        try {
          preg_match('/SN:(.*),.*;Trx NARINDO_.* BERHASIL,.*/', $parser->data_return->message, $matches);
          list(, $sn) = $matches;
          $this->currentSwitcher->serial_number = trim($sn);
        }
        catch(\Exception $e) {
          try {
            preg_match('/SN:(.*);Trx GEMILANG_.* BERHASIL,.*/', $parser->data_return->message, $matches);
            list(, $sn) = $matches;
            $this->currentSwitcher->serial_number = trim($sn);
          }
          catch(\Exception $e) {
            try {
              preg_match('/SUCCESS, .*, .*, SN: (.*)/', $parser->data_return->message, $matches);
              list(, $sn) = $matches;
              $this->currentSwitcher->serial_number = trim($sn);
            }
            catch(\Exception $e) {
              try {
                preg_match('/SN:(.*);Trx JKIOS_.* BERHASIL,.*/', $parser->data_return->message, $matches);
                list(, $sn) = $matches;
                $this->currentSwitcher->serial_number = trim($sn);
              }
              catch(\Exception $e) {
                try {
                  preg_match('/.*;Trx SAKALAGUNA_.* BERHASIL,.* SN: (.*) MSG=.*/', $parser->data_return->message, $matches);
                  list(, $sn) = $matches;
                  $this->currentSwitcher->serial_number = trim($sn);
                }
                catch(\Exception $e) {
                  try {
                    preg_match('/SN:(.*);/', $parser->data_return->message, $matches);
                    list(, $sn) = $matches;
                    $this->currentSwitcher->serial_number = trim($sn);
                  }
                  catch(\Exception $e) {
                    if (strpos($parser->data_return->message, ' GAGAL.') !== false ||
                      strpos($parser->data_return->message, 'Duplicate transaction') !== false ||
                      strpos($parser->data_return->message, 'this item has reach the limit') !== false) {
                      $refundResponse = $this->postForm(BillerKudo::SERVER . '/order/refund-cashier', [
                        'order_split_id' => $request->order_split_id,
                        'pin' => hash('sha256', BillerKudo::PIN)
                      ]);
                      $this->billerOrder->log_notify = json_encode($refundResponse);
                      $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
                    }
                  }
                }
              }
            }
          }
        }

        if (isset($sn)) {
          $this->currentSwitcher->current_base_price = intval($orderDetail->price - $orderDetail->item_komisi);
          $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;
        }

        $this->finalize($listener, false);
      }
    }
  }
}