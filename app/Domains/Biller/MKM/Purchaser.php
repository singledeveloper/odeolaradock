<?php

namespace Odeo\Domains\Biller\MKM;

use Odeo\Domains\Constant\BillerMKM;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class Purchaser extends BillerManager {
  
  private $log_advice_req = [], $log_advice_res = "", $crawler;

  public function __construct() {
    parent::__construct(SwitcherConfig::MKM);
    $this->crawler = app()->make(\Odeo\Domains\Biller\MKM\Crawler\ProductCrawler::class);
  }
  
  private function advice($json, $sleep, $count) {
    sleep($sleep);
    
    try {
      $client = new \GuzzleHttp\Client([
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ]
      ]);
      
      $this->log_advice_req[] = [
        "counter_req" => ($count + 1),
        "datetime" => date("Y-m-d H:i:s"),
        "json" => $json
      ];

      $response = $client->request('POST', BillerMKM::getServer(), [
        "json" => $json,
        "timeout" => 100
      ]);
      
      $this->log_advice_res = $response->getBody()->getContents();
      $body = json_decode($this->log_advice_res, true);
      
      if ($body['Status'] == '0000') return SwitcherConfig::BILLER_SUCCESS;
      else if ($body['Status'] != '0184') return SwitcherConfig::BILLER_FAIL;
    }
    catch(\Exception $e) {}
    
    $count = $count + 1;
    if ($count == BillerMKM::MAX_ADVICE_COUNT) return SwitcherConfig::BILLER_FAIL;
    return $this->advice($json, $sleep, $count);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);
    
    $number = revertTelephone($this->currentSwitcher->number);
    
    list($inquery_request, $inquery_response) = $this->crawler->getInquiry($this->currentSwitcher->currentInventory->code, $number);

    $this->currentSwitcher->current_base_price = $inquery_response['TotalTagihan'];

    $this->billerOrder->log_inquiry_request = json_encode($inquery_request);
    $this->billerOrder->log_inquiry_response = $inquery_response;
    
    $inquery_response = json_decode($inquery_response, true);
    
    if (($inquery_response && isset($inquery_response['Status'])) && $inquery_response['Status'] == '0000') {
      $client = new \GuzzleHttp\Client([
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ]
      ]);
      
      $temptime = time();
      
      try {
        $json = $inquery_request;
        $json['SessionId'] = $inquery_response['SessionId'];
        $json['Tagihan'] = $inquery_response['Tagihan'];
        $json['TotalAdmin'] = 0;
        $json['Action'] = BillerMKM::ACTION_PAYMENT;
        
        $this->billerOrder->log_payment_request = json_encode($json);
        
        $response = $client->request('POST', BillerMKM::getServer(), [
          "json" => $json,
          "timeout" => 100
        ]);

        $this->billerOrder->log_payment_response = $response->getBody()->getContents();
        $body = json_decode($this->billerOrder->log_payment_response, true);

        if ($body['Status'] == '0000') $statusTransaction = SwitcherConfig::BILLER_SUCCESS;
        else if (BillerMKM::isFail($body['Status'])) {
          $statusTransaction = SwitcherConfig::BILLER_FAIL;
          if (BillerMKM::checkWrongNumberPattern($body['ErrorMessage'])) {
            $this->currentSwitcher->status = SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER;
          }
        }
        else $statusTransaction = SwitcherConfig::BILLER_IN_QUEUE;
      }
      catch (\Exception $e) {
        $json['Action'] = BillerMKM::ACTION_ADVICE;
        $statusTransaction = $this->advice($json, time() - $temptime, 0);
      }
    }
    else $statusTransaction = SwitcherConfig::BILLER_FAIL;

    if (count($this->log_advice_req) > 0) $this->billerOrder->log_advice_request = json_encode($this->log_advice_req);
    if ($this->log_advice_res != "") $this->billerOrder->log_advice_response = $this->log_advice_res;
    
    $this->billerOrder->status = $statusTransaction;

    if ($this->billerOrder->status == SwitcherConfig::BILLER_SUCCESS) {
      $this->currentRecon->biller_transaction_id = $body["SessionId"];
      if (trim($body['SerialNumber']) != '' && trim($body['SerialNumber']) != 'N/A')
        $this->currentSwitcher->serial_number = trim($body['SerialNumber']);
      $this->currentSwitcher->current_base_price = $inquery_response["Tagihan"][0]["Total"];

      try {
        $client = new \GuzzleHttp\Client([
          'curl' => [
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false
          ]
        ]);

        $response = $client->request('POST', BillerMKM::getServer(), [
          "json" => [
            'Action' => BillerMKM::ACTION_BALANCE,
            'ClientId' => BillerMKM::getClientId(),
            'MCC' => BillerMKM::MCC,
            'KodeProduk' => BillerMKM::VOUCHER_ID
          ],
          "timeout" => 100
        ]);
        $response = json_decode($response->getBody()->getContents(), true);

        if (isset($response['Balance'])) $this->currentBiller->current_balance = $response['Balance'];
      }
      catch (\Exception $e) {}
    }

    $this->currentRecon->request = $this->billerOrder->log_payment_request;
    $this->currentRecon->response = $this->billerOrder->log_payment_response;
    $this->currentRecon->notify = $this->billerOrder->log_advice_response;

    return $this->finalize($listener);
  }
}