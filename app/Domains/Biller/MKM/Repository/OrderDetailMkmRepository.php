<?php

namespace Odeo\Domains\Biller\MKM\Repository;

use Carbon\Carbon;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\BillerRepository;
use Odeo\Domains\Biller\MKM\Model\OrderDetailMkm;
use Odeo\Domains\Constant\SwitcherConfig;

class OrderDetailMkmRepository extends BillerRepository {

  public function __construct(OrderDetailMkm $orderDetailMkm) {
    $this->setModel($orderDetailMkm);
  }

  public function getIncompleteTransaction() {
    return $this->model->whereHas('switcher', function ($query) {
      $query->whereNull('serial_number');
    })->where('status', SwitcherConfig::BILLER_SUCCESS)
      ->orWhere('status', SwitcherConfig::BILLER_IN_QUEUE)->get();
  }

}
