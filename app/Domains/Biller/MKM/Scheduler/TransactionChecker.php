<?php

namespace Odeo\Domains\Biller\MKM\Scheduler;

use Odeo\Domains\Constant\BillerMKM;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Biller\BillerManager;

class TransactionChecker extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::MKM);
  }

  public function run() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(__CLASS__, 'check'));
    $pipeline->execute([]);
  }
  
  public function check(PipelineListener $listener, $data) {
    $orders = $this->billerRepository->getIncompleteTransaction();
    foreach ($orders as $output) {
      try {
        $this->load($output);
        $json = json_decode($this->billerOrder->log_payment_request, true);
        $json['Action'] = BillerMKM::ACTION_ADVICE;

        $client = new \GuzzleHttp\Client([
          'curl' => [
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false
          ]
        ]);


        $this->billerOrder->log_advice_request = json_encode($json);

        $response = $client->request('POST', BillerMKM::getServer(), [
          "json" => $json,
          "timeout" => 100
        ]);
        $this->billerOrder->log_advice_response = $response->getBody()->getContents();
        $body = json_decode($this->billerOrder->log_advice_response, true);

        if ($body['Status'] == '0000' && $this->billerOrder->status == SwitcherConfig::BILLER_IN_QUEUE) {
          $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;
        }
        else if (BillerMKM::isFail($body['Status'])) {
          $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;

          if ($this->currentSwitcher->vendor_switcher_id == SwitcherConfig::MKM) {
            $this->currentSwitcher->status = SwitcherConfig::SWITCHER_IN_QUEUE;
          }
        }
        else $this->billerOrder->timestamps = false;

        if ((isset($body['SerialNumber']) && ($body['SerialNumber'] != '' && $body['SerialNumber'] != '0' && $body['SerialNumber'] != 'N/A')) ||
          time() - strtotime($this->billerOrder->updated_at) > 24 * 3600) {
          $this->currentSwitcher->serial_number = isset($body['SerialNumber']) && ($body['SerialNumber'] != '' && $body['SerialNumber'] != '0' && $body['SerialNumber'] != 'N/A') ? $body['SerialNumber'] : 'N/A';
        }

        $this->currentRecon->notify = $this->billerOrder->log_advice_response;
        $this->finalize($listener, false);
      }
      catch (\Exception $e) {}
    }
  }
}
