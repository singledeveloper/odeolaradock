<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/10/17
 * Time: 3:14 PM
 */

namespace Odeo\Domains\Biller\MobilePulsa;

use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class InventoryValidator {

  private $pulsaInventories, $margnFormatter, $inquirer;

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->margnFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->inquirer = app()->make(\Odeo\Domains\Supply\Biller\Inquirer::class);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    if ($inventory = $this->pulsaInventories->findById($data['inventory_id'])) {
      $pulsa = $inventory->pulsa;
      $formatted = $this->margnFormatter->formatMargin($pulsa->price, $data);

      return $listener->response(200, array_merge($formatted, [
        'inventory_id' => $inventory->id,
        'name' => $pulsa->name
      ]));
    }

    return $listener->response(400, trans('pulsa.cant_purchase_no_backup'));
  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {
    $inventory = $this->pulsaInventories->findById($data['inventory_id']);
    $manager = SwitcherConfig::getVendorSwitcherManagerById($inventory->vendor_switcher_id);
    $inquiry = $manager->inquiryPostpaid($listener, $data);

    if ($inquiry->status == SwitcherConfig::BILLER_FAIL)
      return $listener->response(400, $inquiry->error_message);
    return $this->inquirer->format($listener, $data, $inquiry);
  }
}
