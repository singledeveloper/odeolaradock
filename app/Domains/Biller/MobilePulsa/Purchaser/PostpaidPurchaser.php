<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/14/17
 * Time: 5:32 PM
 */

namespace Odeo\Domains\Biller\MobilePulsa\Purchaser;

use GuzzleHttp\Client;
use Odeo\Domains\Biller\BillerManager;
use Odeo\Domains\Biller\MobilePulsa\Inquirer\BroadbandLandlineInquirer;
use Odeo\Domains\Biller\MobilePulsa\Inquirer\PlnPostpaidInquirer;
use Odeo\Domains\Biller\MobilePulsa\Inquirer\PulsaPostpaidInquirer;
use Odeo\Domains\Constant\BillerMobilePulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class PostpaidPurchaser extends BillerManager {


  public function __construct() {
    parent::__construct(SwitcherConfig::MOBILEPULSA);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);
    try {
      $pulsaInventory = $this->currentSwitcher->currentInventory;
      $inquiryData = [
        'number' => $this->currentSwitcher->number,
        'inventory_code' => $pulsaInventory->code,
        'ref_id' => $this->billerOrder->id
      ];

      switch ($pulsaInventory->pulsa->service_detail_id) {
        case ServiceDetail::PLN_POSTPAID_ODEO:
          $inquiryResult = app()->make(PlnPostpaidInquirer::class)->inquiryPostpaid($listener, $inquiryData);
          break;
        case ServiceDetail::BROADBAND_ODEO:
        case ServiceDetail::LANDLINE_ODEO:
          $inquiryResult =  app()->make(BroadbandLandlineInquirer::class)->inquiry($listener, $inquiryData);
          break;
        case ServiceDetail::PULSA_POSTPAID_ODEO:
          $inquiryResult = app()->make(PulsaPostpaidInquirer::class)->inquiry($listener, $inquiryData);
      }

      if (isset($inquiryResult['error_message'])) $this->billerOrder->status = $inquiryResult['status'];
      else {
        $latestInquiryPrice = $inquiryResult['biller_price'] + ($inquiryResult['multiplier'] * $pulsaInventory->base_price);
        if ($latestInquiryPrice != $this->currentSwitcher->current_base_price) {
          $this->billerOrder->log_response = json_encode($inquiryResult);
          $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;
        }
        else {
          $this->currentSwitcher->current_base_price = $latestInquiryPrice;
          $xml = new \SimpleXMLElement('<mp/>');
          $client = new Client();
          $query = ['commands' => BillerMobilePulsa::COMMAND_POSTPAID_PAY, 'username' => BillerMobilePulsa::USERNAME, 'tr_id' => $inquiryResult['tr_id'], 'sign' => md5(BillerMobilePulsa::USERNAME . BillerMobilePulsa::getKey() . $inquiryResult['tr_id'])];

          array_walk_recursive($query, function ($value, $key) use ($xml) {
            $xml->addChild($key, $value);
          });
          $this->billerOrder->log_request = $xml->asXML();

          $response = $client->request('POST', BillerMobilePulsa::getPostpaidUrl(), ['body' => $xml->asXML()]);
          $this->billerOrder->log_response = $response->getBody()->getContents();
          $dom = new \DOMDocument();

          if (@$dom->loadXML($this->billerOrder->log_response)) {
            if (count($dom->getElementsByTagName('response_code'))) {
              $status = $dom->getElementsByTagName('response_code')[0]->nodeValue;

              if (array_key_exists($status, BillerMobilePulsa::POSTPAID_STATUS_SUCCESS)) {
                $this->isCheckSN = false;
                $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;
                $this->currentRecon->biller_transaction_id = $dom->getElementsByTagName('tr_id')[0]->nodeValue;
                $this->currentBiller->current_balance = $dom->getElementsByTagName('balance')[0]->nodeValue;
              }
              else if (array_key_exists($status, BillerMobilePulsa::POSTPAID_STATUS_PENDING))
                $this->billerOrder->status = SwitcherConfig::BILLER_IN_QUEUE;
              else $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
            }
            else $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;
          }
          else $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;
        }
      }
    }
    catch (\Exception $e) {
      $this->timeoutMessage = 'Error timeout: ' . $e->getMessage();
      $this->billerOrder->status = SwitcherConfig::BILLER_TIMEOUT;
    }

    return $this->finalize($listener);
  }

}