<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/18/17
 * Time: 3:44 PM
 */

namespace Odeo\Domains\Biller\MobilePulsa\Purchaser;


use GuzzleHttp\Client;
use Odeo\Domains\Biller\BillerManager;
use Odeo\Domains\Constant\BillerMobilePulsa;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class PrepaidPurchaser extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::MOBILEPULSA);
  }

  public function purchase(PipelineListener $listener, $data) {
    if (!$this->initiate($listener, $data)) return $listener->response(200);

    $query = [
      'commands' => BillerMobilePulsa::COMMAND_TOPUP,
      'username' => BillerMobilePulsa::USERNAME,
      'ref_id' => $this->billerOrder->id,
      'hp' => BillerMobilePulsa::USERNAME,
      'pulsa_code' => $this->currentSwitcher->currentInventory->code,
      'sign' => md5(BillerMobilePulsa::USERNAME . BillerMobilePulsa::getKey() . $this->billerOrder->id)
    ];

    try {
      $xml = new \SimpleXMLElement('<mp/>');
      array_walk_recursive($query, function ($val, $key) use ($xml) {
        $xml->addChild($key, $val);
      });
      $this->billerOrder->log_request = $xml->asXML();

      $client = new Client();
      $response = $client->post(BillerMobilePulsa::getPrepaidUrl(), ['body' => $xml->asXML()]);
      $this->billerOrder->log_response = $response->getBody()->getContents();

      $dom = new \DOMDocument();
      if (@$dom->loadXML($this->billerOrder->log_response)) {
        if (count($dom->getElementsByTagName('status'))) {
          $status = $dom->getElementsByTagName('status')[0]->nodeValue;

          if ($status == BillerMobilePulsa::PREPAID_TRANSACTION_STATUS_SUCCESS) {
            $rc = $dom->getElementsByTagName('rc')[0]->nodeValue;

            if ($rc == BillerMobilePulsa::PREPAID_RC_STATUS_SUCCESS) {
              $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;
              $this->currentRecon->biller_transaction_id = $dom->getElementsByTagName('tr_id')[0]->nodeValue;
              $this->currentBiller->current_balance = $dom->getElementsByTagName('balance')[0]->nodeValue;
              $this->currentSwitcher->serial_number = $dom->getElementsByTagName('sn')[0]->nodeValue;
              try {
                $this->currentSwitcher->serial_number .= '/PIN:' . $dom->getElementsByTagName('pin')[0]->nodeValue;
              }
              catch (\Exception $e) {}
            }
            else if ($rc == BillerMobilePulsa::PREPAID_RC_STATUS_PENDING)
              $this->billerOrder->status = SwitcherConfig::BILLER_IN_QUEUE;
            else if (array_key_exists($rc, BillerMobilePulsa::PREPAID_RC_STATUS_FAIL))
              $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
            else $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;
          }
          else if ($status == BillerMobilePulsa::PREPAID_TRANSACTION_STATUS_PROCESS)
            $this->billerOrder->status = SwitcherConfig::BILLER_IN_QUEUE;
          else if ($status == BillerMobilePulsa::PREPAID_TRANSACTION_STATUS_FAIL)
            $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
          else $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;
        }
        else $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;
      }
      else $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;
    }
    catch (\Exception $e) {
      $this->timeoutMessage = 'Error timeout: ' . $e->getMessage();
      $this->billerOrder->status = SwitcherConfig::BILLER_TIMEOUT;
    }

    return $this->finalize($listener);
  }
}