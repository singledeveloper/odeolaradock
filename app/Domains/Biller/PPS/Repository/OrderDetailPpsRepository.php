<?php

namespace Odeo\Domains\Biller\PPS\Repository;

use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\BillerRepository;
use Odeo\Domains\Biller\PPS\Model\OrderDetailPps;

class OrderDetailPpsRepository extends BillerRepository {

  public function __construct(OrderDetailPps $orderDetailPps) {
    $this->setModel($orderDetailPps);
  }

  public function getPendingTransaction() {
    return $this->model->where('status', SwitcherConfig::BILLER_IN_QUEUE)->get();
  }

}
