<?php

namespace Odeo\Domains\Biller\Pringsewu;

use GuzzleHttp\Client;
use Odeo\Domains\Biller\Contract\ReplenishmentContract;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class Replenisher implements ReplenishmentContract {

  const MEMBER_ID = 'H2H2557';
  const PHONE_NO = '085624244787';
  const WEB_REPORT_AUTH_URL = 'http://pringsewucell.com/';
  const WEB_PASSWORD = 'Pr!ng1000';

  private $billerReplenishments;

  public function __construct() {
    $this->billerReplenishments = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository::class);
  }

  public function request(PipelineListener $listener, $data) {

    if (!isset($data['bank']) && !BcaDisbursement::isOperational()) {
      if (isset($data['in_pipeline'])) return $listener->response(200, [
        'amount' => $data['amount'],
        'status' => BillerReplenishment::PENDING_PROGRESS_AUTO_TRANSFER
      ]);
      return [
        'amount' => $data['amount'],
        'status' => BillerReplenishment::PENDING_PROGRESS_AUTO_TRANSFER
      ];
    }

    $count = $this->billerReplenishments->getCompletedReplenishmentCount(SwitcherConfig::PRINGSEWU);
    $data['amount'] = $data['amount'] + ($count * BillerReplenishment::SAME_DAY_MULTIPLIER);

    $client = new \Goutte\Client();
    $client->setClient(new Client(['cookies' => true, 'timeout' => 60]));
    $client->request('POST', self::WEB_REPORT_AUTH_URL, [
      'usernm' => self::PHONE_NO,
      'passwd' => self::WEB_PASSWORD,
      'tp' => 'ps',
      'login' => 'Login',
      'log' => ''
    ]);
    $crawler = $client->request('POST', self::WEB_REPORT_AUTH_URL . 'member/tiketdeposit', [
      'usernm' => self::MEMBER_ID,
      'amount' => $data['amount'],
      'action' => 'tiketdeposit'
    ]);

    $elements = $crawler->filter('#pesanreply .modal-body')->each(function($node){
      return $node->text();
    });

    if (isset($elements[0])) {
      try {
        preg_match('/trf rp. (.*) ke/', strtolower($elements[0]), $matches);
        list(, $amount) = $matches;
        $amount = str_replace('.', '', $amount);
        if (isset($data['in_pipeline'])) return $listener->response(200, ['amount' => $amount]);
        return ['amount' => $amount];
      }
      catch(\Exception $e) {}
    }
    if (isset($data['in_pipeline'])) return $listener->response(400, 'Pringsewu unknown error');
    return ['is_error' => true];
  }

  public function complete(PipelineListener $listener, $data) {
    return [];
  }
}