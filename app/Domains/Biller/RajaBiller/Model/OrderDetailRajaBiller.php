<?php

namespace Odeo\Domains\Biller\RajaBiller\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\OrderDetailPulsaSwitcher;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\PulsaOdeoInventory;

class OrderDetailRajaBiller extends Entity
{

  public function inventory() {
    return $this->belongsTo(PulsaOdeoInventory::class, 'inventory_id');
  }

  public function switcher() {
    return $this->belongsTo(OrderDetailPulsaSwitcher::class, 'switcher_reference_id');
  }
}
