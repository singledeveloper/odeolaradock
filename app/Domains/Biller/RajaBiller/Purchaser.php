<?php

namespace Odeo\Domains\Biller\RajaBiller;

use Odeo\Domains\Constant\BillerRaja;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class Purchaser extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::RAJABILLER);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);
    
    $number = revertTelephone($this->currentSwitcher->number);
    $code = $this->currentSwitcher->currentInventory->code;
    
    $xml = BillerRaja::formatXML("rajabiller.pulsa", [$code, $number, BillerRaja::USER_ID, BillerRaja::PASSWORD, $this->billerOrder->id]);
    
    $this->billerOrder->log_request = $xml;
    
    $client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ]
    ]);
    
    try {
      $response = $client->request('POST', BillerRaja::PROD_SERVER, [
        "body" => $xml,
        "timeout" => 100
      ]);
      $this->billerOrder->log_response = $response->getBody()->getContents();
      
      $dom = new \DOMDocument;
      if (@$dom->loadXML($this->billerOrder->log_response)) {
        $arrays = $dom->getElementsByTagName('string');

        $key = ['code', 'date', 'number', 'id', 'pin', 'sn', 'ref1', 'ref2', 'status', 'keterangan', 'price', 'sisa_saldo'];

        $i = 0; foreach ($arrays as $array) $a[$key[$i++]] = $array->nodeValue;

        if (isset($a['status']) && $a['status'] == '00') $statusTransaction = SwitcherConfig::BILLER_SUCCESS;
        else if (isset($a['status']) && $a['status'] != '') $statusTransaction = SwitcherConfig::BILLER_FAIL;
        else if (isset($a['keterangan']) && BillerRaja::checkWrongNumberPattern($a['keterangan'])) {
          $statusTransaction = SwitcherConfig::BILLER_FAIL;
          $this->currentSwitcher->status = SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER;
        }
        else $statusTransaction = SwitcherConfig::BILLER_IN_QUEUE;

        if (isset($a['ref2'])) $this->currentRecon->biller_transaction_id = $a['ref2'];
        if (isset($a['price']) && trim($a['price']) != '0') $this->currentSwitcher->current_base_price = $a['price'];
      }
      else $statusTransaction = SwitcherConfig::BILLER_SUSPECT;
    }
    catch (\Exception $e) {
      $statusTransaction = SwitcherConfig::BILLER_TIMEOUT;
    }

    $this->billerOrder->status = $statusTransaction;

    if ($this->billerOrder->status == SwitcherConfig::BILLER_SUCCESS) {
      if (isset($a['sn'])) $this->currentSwitcher->serial_number = (trim($a['sn']) != '') ? trim($a['sn']) : 'N/A';
      if (isset($a['sisa_saldo'])) $this->currentBiller->current_balance = $a['sisa_saldo'];
    }

    return $this->finalize($listener);
  }
}