<?php

namespace Odeo\Domains\Biller\RajaBiller\Repository;

use Carbon\Carbon;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\BillerRepository;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Biller\RajaBiller\Model\OrderDetailRajaBiller;

class OrderDetailRajaBillerRepository extends BillerRepository {

  public function __construct(OrderDetailRajaBiller $orderDetailRajaBiller) {
    $this->setModel($orderDetailRajaBiller);
  }
  
  public function getPendingTransaction() {
    return $this->model->where('status', SwitcherConfig::BILLER_IN_QUEUE)->get();
  }

  public function getRecon() {
    return $this->model->where('log_transaction', 'not like', '% berhasil %')->get();
  }

}
