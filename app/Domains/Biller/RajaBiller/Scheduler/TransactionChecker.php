<?php

namespace Odeo\Domains\Biller\RajaBiller\Scheduler;

use Odeo\Domains\Constant\BillerRaja;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class TransactionChecker extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::RAJABILLER);
  }

  public function run() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(__CLASS__, 'check'));
    $pipeline->execute([]);
  }
  
  public function check(PipelineListener $listener, $data) {
    $pendingOrders = $this->billerRepository->getPendingTransaction();
    foreach ($pendingOrders as $output) {
      $this->load($output);

      try {
        $dom = new \DOMDocument;
        if (@$dom->loadXML($this->billerOrder->log_response)) {
          $arrays = $dom->getElementsByTagName('string');

          $key = array('code', 'date', 'number', 'id', 'pin', 'sn', 'ref1', 'ref2', 'status', 'keterangan', 'price', 'sisa_saldo');

          $i = 0; foreach ($arrays as $array) $a[$key[$i++]] = $array->nodeValue;

          if (!isset($a['ref2']) || (isset($a['ref2']) && $a['ref2'] == '')) continue;

          $xml = BillerRaja::formatXML('rajabiller.datatransaksi', [date('Ymd', time()) . '000000', date('Ymd', time() + (60 * 60 * 24)) . '000000', $a['ref2'], '', '', '', BillerRaja::USER_ID, BillerRaja::PASSWORD]);

          $client = new \GuzzleHttp\Client([
            'curl' => [
              CURLOPT_SSL_VERIFYPEER => false,
              CURLOPT_SSL_VERIFYHOST => false
            ]
          ]);

          $response = $client->request('POST', BillerRaja::PROD_SERVER, ["body" => $xml]);
          $this->billerOrder->log_transaction = $response->getBody()->getContents();

          $dom = new \DOMDocument;
          if (@$dom->loadXML($this->billerOrder->log_transaction)) {
            $arrays = $dom->getElementsByTagName('string');

            $key = array('datestart', 'datefinish', 'a', 'b', 'c', 'user_id', 'pin', 'status', 'keterangan', 'data');

            $i = 0; foreach ($arrays as $array) $a[$key[$i++]] = $array->nodeValue;

            if (isset($a['sisa_saldo'])) $sisa_saldo = $a['sisa_saldo'];
            $temp = explode("#", $a['data']);

            if (isset($a['data']) && (
                strpos(strtolower($a['data']), ' gagal.') !== false ||
                strpos(strtolower($a['data']), ' gagal ') !== false ||
                strpos(strtolower($a['data']), '#gagal ') !== false ||
                strpos(strtolower($a['data']), '#ext: fail#') !== false))
              $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
            else if (isset($a['data']) &&
              strpos(strtolower($a['data']), '#sukses oleh') !== false) {
              $this->currentSwitcher->status = SwitcherConfig::BILLER_SUSPECT;
            }
            else if (isset($a['data']) &&
              strpos(strtolower($a['data']), 'sedang diproses') === false)
              $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;

            if ($this->billerOrder->status == SwitcherConfig::BILLER_SUCCESS) {
              $sn = (trim($temp[count($temp) - 1]) != '') ? trim($temp[count($temp) - 1]) : "N/A";
              $this->currentSwitcher->serial_number = strpos($sn, 'tlhditambahkan') === false && strpos($sn, 'xtracombo') === false ? $sn : "N/A";

              if (isset($sisa_saldo)) $this->currentBiller->current_balance = $sisa_saldo;
            }
            $this->currentRecon->notify = $this->billerOrder->log_transaction;
            $this->finalize($listener, false);
          }
        }
      }
      catch (\Exception $e) {}
    }
    
  }
}
