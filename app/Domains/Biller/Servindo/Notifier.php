<?php

namespace Odeo\Domains\Biller\Servindo;

use Odeo\Domains\Biller\BillerManager;
use Odeo\Domains\Constant\BillerServindo;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class Notifier extends BillerManager {

  private $request;

  public function __construct() {
    parent::__construct(SwitcherConfig::SERVINDO);
    $this->request = app()->make(\Illuminate\Http\Request::class);
  }

  public function notify(PipelineListener $listener, $data) {
    if (isProduction() && !in_array(getClientIP(), BillerServindo::IP_WHITELIST)) {
      echo 'IP MISMATCH';
    }
    else {
      $data = json_decode(BillerServindo::hardFixRes($this->request->getContent()));
      try {
        if ($transaction = $this->billerRepository->findById($data->reg_id)) {
          $this->load($transaction);
          if ($this->billerOrder->status == SwitcherConfig::BILLER_IN_QUEUE) {
            $this->billerOrder->log_notify = json_encode($data);
            $isValidSN = isset($data->sn) && trim($data->sn) != '-' && trim($data->sn) != '';

            if ($data->status == '1') {
              if ($isValidSN) $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;
            }
            else if ($data->status == '5') {
              if ($isValidSN) $this->billerOrder->status = SwitcherConfig::BILLER_DUPLICATE_SUCCESS;
            }
            else if ($data->status == '2' || $data->status == '7')
              $this->billerOrder->status = SwitcherConfig::BILLER_IN_QUEUE;
            else {
              $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;

              if (BillerServindo::checkWrongNumberPattern($data->message))
                $this->currentSwitcher->status = SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER;
              else if ($this->currentSwitcher->vendor_switcher_id == SwitcherConfig::SERVINDO)
                $this->currentSwitcher->status = SwitcherConfig::SWITCHER_IN_QUEUE;
            }

            if (in_array($this->billerOrder->status, [
              SwitcherConfig::BILLER_SUCCESS,
              SwitcherConfig::BILLER_DUPLICATE_SUCCESS
            ]) && $this->currentSwitcher->serial_number == null) {
              $this->currentSwitcher->serial_number = $data->sn;

              if (isset($data->harga) && trim($data->harga) != '0')
                $this->currentSwitcher->current_base_price = $data->harga;

              if (isset($data->saldo)) $this->currentBiller->current_balance = $data->saldo;
            }

            if (isset($data->tn)) $this->currentRecon->biller_transaction_id = trim($data->tn);

            $this->finalize($listener, false);
          }

          echo 'OK';
        }
        else echo 'ERR PARAMETER NOT VALID';
      }
      catch (\Exception $e) {
        echo 'ERR PARAMETER NOT VALID';
      }
    }
  }

}
