<?php

namespace Odeo\Domains\Biller\Servindo\Purchaser;

use Odeo\Domains\Constant\BillerServindo;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class PulsaPurchaser extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::SERVINDO);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);

    $number = revertTelephone($this->currentSwitcher->number);

    $query = [
      'regid' => $this->billerOrder->id,
      'userid' => BillerServindo::USER_ID,
      'passwd' => BillerServindo::PASSWORD,
      'msisd' => $number,
      'denom' => $this->currentSwitcher->currentInventory->code
    ];

    $this->billerOrder->log_request = json_encode($query);

    $client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ],
      'timeout' => 90
    ]);

    try {
      $response = $client->request('GET', BillerServindo::getServer(), ["query" => $query]);
      $this->billerOrder->log_response = BillerServindo::hardFixRes($response->getBody()->getContents());
      if ($content = json_decode($this->billerOrder->log_response)) {
        if ($content->status == '1') $statusTransaction = SwitcherConfig::BILLER_SUCCESS;
        else if ($content->status == '5') $statusTransaction = SwitcherConfig::BILLER_DUPLICATE_SUCCESS;
        else if ($content->status == '2' || $content->status == '7') $statusTransaction = SwitcherConfig::BILLER_IN_QUEUE;
        else {
          $statusTransaction = SwitcherConfig::BILLER_FAIL;
          if (BillerServindo::checkWrongNumberPattern($content->message))
            $this->currentSwitcher->status = SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER;
        }

        if (($statusTransaction == SwitcherConfig::BILLER_SUCCESS || $statusTransaction == SwitcherConfig::BILLER_DUPLICATE_SUCCESS)) {
          $this->currentSwitcher->serial_number = $content->sn;

          if (isset($content->harga) && trim($content->harga) != '0')
            $this->currentSwitcher->current_base_price = $content->harga;
        }

        if (isset($content->tn) && $content->tn != '0') $this->currentRecon->biller_transaction_id = trim($content->tn);

      }
      else $statusTransaction = SwitcherConfig::BILLER_SUSPECT;
    }
    catch (\Exception $e) {
      $this->timeoutMessage = 'Error timeout: ' . $e->getMessage();
      $statusTransaction = SwitcherConfig::BILLER_TIMEOUT;
    }

    $this->billerOrder->status = $statusTransaction;

    if ($this->billerOrder->status == SwitcherConfig::BILLER_SUCCESS && isset($content)) {
      $this->currentBiller->current_balance = $content->saldo;
    }

    return $this->finalize($listener);
  }
}