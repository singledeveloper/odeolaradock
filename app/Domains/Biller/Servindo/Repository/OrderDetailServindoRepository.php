<?php

namespace Odeo\Domains\Biller\Servindo\Repository;

use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\BillerRepository;
use Odeo\Domains\Biller\Servindo\Model\OrderDetailServindo;

class OrderDetailServindoRepository extends BillerRepository {

  public function __construct(OrderDetailServindo $orderDetailServindo) {
    $this->setModel($orderDetailServindo);
  }

  public function getPendingTransaction() {
    return $this->model->where(\DB::raw('date(created_at)'), date('Y-m-d'))
      ->where('status', SwitcherConfig::BILLER_IN_QUEUE)->get();
  }

}
