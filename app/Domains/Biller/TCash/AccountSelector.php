<?php

namespace Odeo\Domains\Biller\TCash;

use Odeo\Domains\Constant\Gojek;
use Odeo\Domains\Constant\TCash;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class AccountSelector implements SelectorListener {

  private $tcashUsers, $currencyHelper, $supplyValidator;

  public function __construct() {
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    $this->tcashUsers = app()->make(\Odeo\Domains\Biller\TCash\Repository\TcashUserRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    if ($item->status == TCash::CONNECTED) $status = 'CONNECTED';
    else if ($item->status == TCash::PENDING_OTP) $status = 'NEED OTP';
    else if ($item->status == TCash::PENDING_PIN) $status = 'NEED PIN';
    else $status = 'UNKNOWN';

    return [
      'id' => $item->id,
      'telephone' => revertTelephone($item->telephone),
      'account_type' => $item->account_type,
      'current_balance' => $this->currencyHelper->formatPrice($item->current_balance),
      'status' => $status,
      'is_need_pin_input' => $item->pin == null,
      'today_purchase_counts' => $item->purchase_counts,
      'last_error_message' => $item->last_error_message
    ];
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {
    if (!isAdmin($data) && !isset($data['store_id']))
      return $listener->response(400, 'Access error.');

    list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
    if (!$isValid) return $listener->response(400, $message);

    $tcashUsers = [];

    $this->tcashUsers->normalizeFilters($data);
    $this->tcashUsers->setSimplePaginate(true);

    foreach($this->tcashUsers->gets() as $item) {
      $tcashUsers[] = $this->_transforms($item, $this->tcashUsers);
    }

    if (sizeof($tcashUsers) > 0)
      return $listener->response(200, array_merge(
        ["tcash_users" => $this->_extends($tcashUsers, $this->tcashUsers)],
        $this->tcashUsers->getPagination()
      ));

    return $listener->response(204, ["tcash_users" => []]);
  }

}