<?php

namespace Odeo\Domains\Biller\TCash;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Biller\TCash\Helper\TCashManager;
use Odeo\Domains\Biller\TCash\Jobs\DistributeDeposit;
use Odeo\Domains\Constant\TCash;
use Odeo\Domains\Core\PipelineListener;

class DepositDistributor extends TCashManager {

  private $tcashTransferHistories, $transferrer, $supplyValidator, $redis;

  public function __construct() {
    parent::__construct();
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    $this->tcashTransferHistories = app()->make(\Odeo\Domains\Biller\TCash\Repository\TcashTransferHistoryRepository::class);
    $this->transferrer = app()->make(\Odeo\Domains\Biller\TCash\Helper\Transferrer::class);
    $this->redis = Redis::connection();
  }

  public function distribute(PipelineListener $listener, $data) {
    if (isset($data['store_id'])) {
      list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
      if (!$isValid) return $listener->response(400, $message);
    }
    else $data['store_id'] = 0;

    if ($this->redis->hget(TCash::REDIS_TCASH, TCash::REDIS_KEY_TCASH_DISTRIBUTE_LOCK . $data['store_id']))
      return $listener->response(400, 'Distribution under process. Please wait.');

    dispatch(new DistributeDeposit($data['store_id']));

    return $listener->response(200);

  }

}

