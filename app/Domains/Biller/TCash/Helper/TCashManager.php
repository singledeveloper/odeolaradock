<?php

namespace Odeo\Domains\Biller\TCash\Helper;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Biller\TCash\Jobs\UpdateTCashUser;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\TCash;

class TCashManager {

  const BASE_URL = 'https://mwallet.telkomsel.com/telkomsel21/ci/';
  const DEFAULT_ERROR = 'Gagal. Silakan coba lagi.';

  private $client, $sessionKey, $msisdn, $currentId = '', $cookieJar = '', $customerId = '';

  protected $tcashUsers, $redis, $errorMessage = '', $responseStatus = '';

  public function __construct() {
    $this->tcashUsers = app()->make(\Odeo\Domains\Biller\TCash\Repository\TcashUserRepository::class);
    $this->redis = Redis::connection();
    $this->client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ],
      'timeout' => 90,
      'headers' => [
        'Accept-Language' => 'id-ID'
      ]
    ]);
  }

  protected function setCommonHeader($tcashUser) {
    $this->currentId = $tcashUser->id;
    $this->sessionKey = $tcashUser->session_key;
    $this->setMSISDN(revertTelephone($tcashUser->telephone));
    $this->setCookieJar(json_decode($tcashUser->cookie_jar, true));
    if ($tcashUser->customer_id != null) $this->setCustomerID($tcashUser->customer_id);
  }

  protected function setMSISDN($msisdn) {
    $this->msisdn = $msisdn;
  }

  protected function setCustomerID($customerId) {
    $this->customerId = $customerId;
  }

  protected function setCookieJar($data) {
    $this->cookieJar = new \GuzzleHttp\Cookie\CookieJar();
    foreach ($data as $value) {
      $this->cookieJar->setCookie(new \GuzzleHttp\Cookie\SetCookie($value));
    }
  }

  protected function getCookieJar() {
    return $this->cookieJar;
  }

  protected function deleteCookieJar() {
    $this->cookieJar = '';
  }

  protected function getVersion() {
    if (!$version = $this->redis->hget(TCash::REDIS_TCASH, TCash::REDIS_KEY_TCASH_VERSION))
      $version = TCash::DEFAULT_VERSION;
    return $version;
  }

  protected function getToken($tcashUser) {
    return TCash::simpleEncrypt(revertTelephone($tcashUser->telephone) . $tcashUser->imei . $tcashUser->imsi);
  }

  protected function getTransactionPin($tcashUser) {
    return openssl_encrypt(Crypt::decrypt($tcashUser->pin), 'aes-128-ecb', $this->sessionKey);
  }

  protected function getLoginPin($pin) {
    $walletCert = $this->requestWithRetry('GET', '/user/getWalletCertificate');
    if ($this->errorMessage == '') {
      openssl_public_encrypt($pin, $encrypted, openssl_pkey_get_public($walletCert));
      return base64_encode($encrypted);
    }
    return false;
  }

  protected function getMobileInfo($tcashUser) {
    return [
      'imei' => $tcashUser->imei,
      'imsi' => $tcashUser->imsi,
      'modelNo' => TCash::getModelNo(),
      'pushId' => '',
      'pushType' => TCash::DEFAULT_PUSH_TYPE,
      'walletVersion' => $this->getVersion(),
      'osVer' => TCash::DEFAULT_OS_VERSION
    ];
  }

  protected function request($method, $url, $title = '', $json = [], $loop = true) {

    $jsonBody = [];
    if ($title != '') {
      $commonHeader = [
        'msisdn' => $this->msisdn,
        'tid' => 'v' . $this->getVersion() . '_' . $this->msisdn . '_A_' . time(),
        'locale' => 'id'
      ];
      if ($this->customerId != '') $commonHeader['customerId'] = $this->customerId;

      $jsonBody = [$title => array_merge(['commonHeader' => $commonHeader], $json)];
    }

    try {
      $body = ['headers' => [
        'User-Agent' => $this->redis->hget(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_PLN_USER_AGENT)
      ]];
      if (count($jsonBody) > 0) $body['json'] = $jsonBody;
      if ($this->cookieJar != '') $body['cookies'] = $this->cookieJar;

      $response = $this->client->request($method, self::BASE_URL . ltrim($url, '/'), $body);

      if ($this->cookieJar == '') {
        $setCookies = $response->getHeader('set-cookie');
        $this->cookieJar = new \GuzzleHttp\Cookie\CookieJar();
        foreach ($setCookies as $value) {
          list($cookie, $cookiePath) = explode(';', $value);
          list($cookieName, $cookieValue) = explode('=', $cookie);
          list(, $cookiePathValue) = explode('=', $cookiePath);

          $this->cookieJar->setCookie(new \GuzzleHttp\Cookie\SetCookie([
            'Domain'  => '.mwallet.telkomsel.com',
            'Name'    => $cookieName,
            'Value'   => $cookieValue,
            'Path'    => $cookiePathValue,
            'Discard' => false
          ]));
        }
      }

      $rawResponse = $response->getBody()->getContents();

      if ($response = json_decode($rawResponse, true)) {
        $response = $response[array_keys($response)[0]];

        if ($response['result']['code'] != '0') {
          $this->errorMessage = $response['result']['message'];
          if (in_array($response['result']['code'], ['90001']))
            $this->responseStatus = SwitcherConfig::BILLER_SUSPECT;
          else $this->responseStatus = SwitcherConfig::BILLER_FAIL;
        }
        else {
          $this->responseStatus = SwitcherConfig::BILLER_SUCCESS;
          return $response;
        }
      }
      else {
        $this->responseStatus = SwitcherConfig::BILLER_SUSPECT;
        return $rawResponse;
      }
    }
    catch (\GuzzleHttp\Exception\ClientException $e) {
      if ($loop && $e->getCode() == '401') {
        $tcashUser = $this->tcashUsers->findById($this->currentId);
        $this->errorMessage = '';
        $this->responseStatus = '';
        $this->cookieJar = '';
        $tcashUserLogin = $this->requestLogin($tcashUser);
        if ($this->errorMessage == '') {
          $tcashUser = $tcashUserLogin;
          dispatch(new UpdateTCashUser($tcashUser->id, [
            'session_key' => $tcashUser->session_key,
            'cookie_jar' => json_encode($this->cookieJar->toArray())
          ]));
          $this->sessionKey = $tcashUser->session_key;
          return $this->request($method, $url, $title, $json, false);
        }
        else {
          clog('tcash_error', 'login fail: ' . $this->errorMessage);
          $dispatchData = [];
          if (strpos($this->errorMessage, 'tidak diketahui') === false
            && strpos($this->errorMessage, 'pemeliharaan sistem') === false) // Kesalahan server tidak diketahui >> belum tentu gagal login
            $dispatchData['status'] = TCash::PENDING_OTP;
          //$this->errorMessage = 'Login gagal, mungkin perlu ulangi login dengan OTP';
          $this->responseStatus = SwitcherConfig::BILLER_FAIL;

          $dispatchData['last_error_message'] = $this->errorMessage;
          dispatch(new UpdateTCashUser($tcashUser->id, $dispatchData));
          return false;
        }
      }
      else {
        $this->errorMessage = $e->getResponse()->getBody()->getContents();
        clog('tcash_error', 'req: ' . json_encode($jsonBody));
        clog('tcash_error', 'res: ' . $this->errorMessage);
        $this->responseStatus = SwitcherConfig::BILLER_FAIL;
      }
    }
    catch (\GuzzleHttp\Exception\ServerException $e) {
      $this->errorMessage = 'Timeout. ' . self::DEFAULT_ERROR;
      $this->responseStatus = SwitcherConfig::BILLER_SUSPECT;
    }
    catch (\Exception $e) {
      $this->errorMessage = $e->getMessage();
      if (strpos($this->errorMessage, 'Failed to connect to') !== false)
        $this->errorMessage = SwitcherConfig::BILLER_FAIL;
      else $this->responseStatus = SwitcherConfig::BILLER_SUSPECT;
    }

    if ($this->errorMessage != '') {
      $this->errorMessage = 'TCASH error: ' . $this->errorMessage;
      if (strpos($this->errorMessage, 'internal error') !== false)
        $this->responseStatus = SwitcherConfig::BILLER_SUSPECT;
      if ($this->currentId != '') {
        $params = ['last_error_message' => $this->errorMessage];
        if (strpos($this->errorMessage, 'Rule Limited') !== false ||
          strpos($this->errorMessage, 'Balance insufficient') !== false)
          $params['purchase_counts'] = TCash::MAX_PURCHASE_LIMIT;
        dispatch(new UpdateTCashUser($this->currentId, $params));
      }
    }

    return false;
  }

  protected function requestWithRetry($method, $url, $title = '', $json = [], $loopLimit = 3) {
    $loop = 0;
    $isTryAgain = true;
    do {
      $this->errorMessage = '';
      $response = $this->request($method, $url, $title, $json);
      if ($this->errorMessage != ''){
        if (strpos($this->errorMessage, 'Could not resolve host') === false || $loop >= $loopLimit)
          $isTryAgain = false;
        $loop++;
      } else $isTryAgain = false;
    } while($isTryAgain);

    return $response;
  }

  protected function requestTimestamp() {
    $response = $this->requestWithRetry('POST', '/user/checkWalletUpdate', 'checkWalletUpdateRq', [
      'osType' => TCash::LOGIN_OS_TYPE,
      'version' => $this->getVersion(),
      'walletId' => TCash::LOGIN_WALLET_ID
    ]);

    if (isset($response['version']))
      $this->redis->hset(TCash::REDIS_TCASH, TCash::REDIS_KEY_TCASH_VERSION, $response['version']);

    return $response;
  }

  protected function requestLogin($tcashUser) {
    if ($checkUpdateRes = $this->requestTimestamp()) {
      $tokenKey = openssl_decrypt($tcashUser->enc_token_key, 'aes-128-ecb', Crypt::decrypt($tcashUser->pin));
      $cMac = openssl_encrypt($checkUpdateRes['timestamp'], 'aes-128-ecb', $tokenKey);

      $tcashUser->session_key = openssl_encrypt(TCash::simpleEncrypt($checkUpdateRes['timestamp']), 'aes-128-ecb', $tokenKey);

      $loginRes = $this->requestWithRetry('POST', '/user/login', 'loginRq', [
        'imei' => $tcashUser->imei,
        'imsi' => $tcashUser->imsi,
        'cMac' => $cMac,
        'token' => $this->getToken($tcashUser)
      ]);

      if (isset($loginRes['tcashStatus']))
        $tcashUser->account_type = strtolower(explode('_', $loginRes['tcashStatus'])[0]);

      return $tcashUser;
    }
    return false;
  }

}

