<?php

namespace Odeo\Domains\Biller\TCash\Helper;

use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\TCash;

class Transferrer extends TCashManager {

  public function __construct() {
    parent::__construct();
  }

  public function getBalance($tcashUser) {
    $this->setCommonHeader($tcashUser);
    $balanceRes = $this->requestWithRetry('POST', '/user/getTcashBalance', 'getTcashBalanceRq');
    if ($this->errorMessage == '') return $balanceRes;
    return [];
  }

  public function transfer($data) {

    $tcashUser = $this->tcashUsers->findById($data['tcash_user_id']);
    $this->setCommonHeader($tcashUser);

    $resultData = [];

    foreach ($data['transfers'] as $key => $item) {
      $this->responseStatus = '';
      $this->errorMessage = '';

      $transferInfoRes = $this->request('POST', '/tcash/getTransferInfo', 'getTransferInfoRq', [
        'tcashTransactionRequest' => [
          'menuId' => TCash::MENU_ID_TRANSFER,
          'transferTypeId' => TCash::MENU_ID_TRANSFER,
          'destinationNo' => revertTelephone(trim($key)),
          'amount' => $item
        ]
      ]);
      if ($this->errorMessage != '') {
        $resultData[$key] = [
          'status' => $this->responseStatus,
          'response' => $this->errorMessage
        ];
        continue;
      }

      $response = $this->request('POST', '/tcash/transferWithTcash', 'transferWithTcashRq', [
        'tcashTransactionRequest' => [
          'menuId' => TCash::MENU_ID_TRANSFER,
          'transferTypeId' => TCash::MENU_ID_TRANSFER,
          'tcashPin' => $this->getTransactionPin($tcashUser),
          'tcashTransactionId' => $transferInfoRes['tcashTransaction']['transactionId'],
          'destinationNo' => revertTelephone(trim($key)),
          'amount' => $item
        ]
      ]);
      if ($this->errorMessage != '') {
        $resultData[$key] = [
          'status' => $this->responseStatus,
          'response' => $this->errorMessage
        ];
        continue;
      }

      $resultData[$key] = [
        'ref' => $response['tcashTransaction']['transactionId'],
        'status' => SwitcherConfig::BILLER_SUCCESS,
        'response' => json_encode($response)
      ];
    }

    return $resultData;
  }

}

