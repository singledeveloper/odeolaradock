<?php

namespace Odeo\Domains\Biller\TCash\Jobs;

use Illuminate\Support\Facades\Crypt;
use Odeo\Jobs\Job;

class UpdateTCashUser extends Job  {

  private $id, $data;

  public function __construct($id, $data) {
    parent::__construct();
    $this->id = $id;
    $this->data = $data;
  }

  public function handle() {

    $tcashUsers = app()->make(\Odeo\Domains\Biller\TCash\Repository\TcashUserRepository::class);
    if ($tcashUser = $tcashUsers->findById($this->id)) {
      if (isset($this->data['pin'])) $tcashUser->pin = Crypt::encrypt($this->data['pin']);
      if (isset($this->data['enc_token_key'])) $tcashUser->enc_token_key = $this->data['enc_token_key'];
      if (isset($this->data['session_key'])) $tcashUser->session_key = $this->data['session_key'];
      if (isset($this->data['cookie_jar'])) $tcashUser->cookie_jar = $this->data['cookie_jar'];
      if (isset($this->data['last_error_message'])) $tcashUser->last_error_message = $this->data['last_error_message'];
      if (isset($this->data['status'])) $tcashUser->status = $this->data['status'];
      if (isset($this->data['purchase_counts'])) $tcashUser->purchase_counts = $this->data['purchase_counts'];

      $tcashUsers->save($tcashUser);
    }

  }

}


