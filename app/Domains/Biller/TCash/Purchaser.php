<?php

namespace Odeo\Domains\Biller\TCash;

use Carbon\Carbon;
use Odeo\Domains\Biller\TCash\Helper\TCashManager;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\TCash;
use Odeo\Domains\Core\PipelineListener;

class Purchaser extends TCashManager {

  private $menuId, $vendorSwitchers, $pulsaInventories;

  public function __construct() {
    parent::__construct();
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
  }

  public function purchase(PipelineListener $listener, $data) {

    \DB::beginTransaction();

    try {
      if ($tcashUser = $this->tcashUsers->findBiggestBalance($data['store_id'])) {
        $request = ' using number ' . revertTelephone($tcashUser->telephone);
        $this->setCommonHeader($tcashUser);

        try {
          list($menuId, $data['amount']) = explode('.', $data['amount']);
        }
        catch (\Exception $e) {
          \DB::commit();
          return [
            'destination' => $tcashUser->telephone,
            'request' => 'Try parse inventory' . $request,
            'status' => SwitcherConfig::BILLER_FAIL,
            'response' => 'Error. Mohon cek kembali format kode produk.'
          ];
        }

        $this->menuId = $menuId;

        if ($menuId == TCash::MENU_ID_TRANSFER) {
          $transferrer = app()->make(\Odeo\Domains\Biller\TCash\Helper\Transferrer::class);
          $result = $transferrer->transfer([
            'tcash_user_id' => $tcashUser->id,
            'transfers' => [
              $data['msisdn'] => $data['amount']
            ]
          ]);
          $result = $result[$data['msisdn']];
          $result['request'] = 'Transfer';
        }
        else if (in_array($menuId, TCash::MENU_ID_AIRTIME_DENOMS))
          $result = $this->purchaseDenom($tcashUser, $data);
        else {
          \DB::commit();
          return [
            'destination' => $tcashUser->telephone,
            'request' => 'Check menu ID' . $request,
            'status' => SwitcherConfig::BILLER_FAIL,
            'response' => 'Error. Mohon cek kembali kode menu ID.'
          ];
        }

        $result['request'] = (isset($result['request']) ? $result['request'] : '') . $request;

        if (in_array($result['status'], [SwitcherConfig::BILLER_SUCCESS, SwitcherConfig::BILLER_SUSPECT])) {
          if ($result['status'] == SwitcherConfig::BILLER_SUCCESS || strpos($this->errorMessage, 'Sistem eksternal tidak') !== false) {
            if ($menuId != TCash::MENU_ID_TRANSFER) {
              $tcashUser->last_purchased_at = Carbon::now()->toDateTimeString();
              $tcashUser->purchase_counts = $tcashUser->purchase_counts + 1;
            }
            $tcashUser->current_balance = $tcashUser->current_balance - $data['amount'];
          }
          else {
            $balanceRes = $this->request('POST', '/user/getTcashBalance', 'getTcashBalanceRq');
            if ($this->errorMessage == '') {
              $tcashUser->current_balance = $balanceRes['tcashBalance'];
            }
          }
          $this->tcashUsers->save($tcashUser);

          $balance = $this->tcashUsers->getTotalBalance($tcashUser->store_id);
          if (!$balance) $balance = 0;
          else $balance = $balance->total_balance;

          $result['balance'] = $balance;
        }

        \DB::commit();
        return $result;
      }

      if ($biller = $this->vendorSwitchers->findByStatus(SwitcherConfig::BILLER_STATUS_HIDDEN_TCASH, $data['store_id']))
        $this->pulsaInventories->disableInventory($biller->id);

      \DB::commit();
      return [
        'status' => SwitcherConfig::BILLER_FAIL,
        'response' => 'No TCash account exist/All TCash accounts are overlimit'
      ];
    }
    catch (\Exception $e) {
      \DB::commit();
      return [
        'status' => SwitcherConfig::BILLER_SUSPECT,
        'response' => 'Error. Cek manual ke sistem (odeo_it).'
      ];
    }

  }

  private function purchaseDenom($tcashUser, $data) {
    $transferInfoRes = $this->request('POST', '/tcash/getPurchaseInfo', 'getPurchaseInfoRq', [
      'tcashTransactionRequest' => [
        'menuId' => $this->menuId,
        'destinationNo' => revertTelephone($data['msisdn']),
        'amount' => $data['amount']
      ]
    ]);
    if ($this->errorMessage != '') return [
      'destination' => $tcashUser->telephone,
      'request' => 'Purchase Info',
      'status' => $this->responseStatus,
      'response' => $this->errorMessage
    ];

    $response = $this->request('POST', '/tcash/purchaseWithTcash', 'purchaseWithTcashRq', [
      'tcashTransactionRequest' => [
        'menuId' => $this->menuId,
        'tcashPin' => $this->getTransactionPin($tcashUser),
        'tcashTransactionId' => $transferInfoRes['tcashTransaction']['transactionId'],
        'destinationNo' => revertTelephone($data['msisdn']),
        'amount' => $data['amount']
      ]
    ]);
    if ($this->errorMessage != '') return [
      'destination' => $tcashUser->telephone,
      'request' => 'Purchase',
      'status' => $this->responseStatus,
      'response' => $this->errorMessage
    ];

    return [
      'destination' => $tcashUser->telephone,
      'ref' => $response['tcashTransaction']['transactionId'],
      'request' => 'Purchase',
      'status' => SwitcherConfig::BILLER_SUCCESS,
      'response' => json_encode($response),
    ];
  }

}

