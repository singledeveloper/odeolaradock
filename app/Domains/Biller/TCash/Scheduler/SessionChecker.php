<?php

namespace Odeo\Domains\Biller\TCash\Scheduler;

use Odeo\Domains\Biller\TCash\Helper\TCashManager;

class SessionChecker extends TCashManager {

  public function __construct() {
    parent::__construct();
  }

  public function run() {
    if (!isProduction()) return;

    if ($tcashUser = $this->tcashUsers->findLeastConnected()) {
      $this->setCommonHeader($tcashUser);
      $balanceRes = $this->request('POST', '/user/getTcashBalance', 'getTcashBalanceRq');
      if ($this->errorMessage == '')  {
        $tcashUser->current_balance = $balanceRes['tcashBalance'];
        $this->tcashUsers->save($tcashUser);
      }
      else clog('tcash_scheduler_debug', 'Error user . ' . revertTelephone($tcashUser->telephone) . ': ' . $this->errorMessage);
    }
  }

}
