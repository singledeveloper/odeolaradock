<?php

namespace Odeo\Domains\Biller\TCash\Scheduler;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\TCash;

class TCashVaInquiryChecker {

  private $tcashUsers, $bcaInquiries, $billerReplenishments, $redis;

  public function __construct() {
    $this->tcashUsers = app()->make(\Odeo\Domains\Biller\TCash\Repository\TcashUserRepository::class);
    $this->bcaInquiries = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository::class);
    $this->billerReplenishments = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository::class);
    $this->redis = Redis::connection();
  }

  public function run() {

    if (!$lastInquiryCheckDate = $this->redis->hget(TCash::REDIS_TCASH, TCash::REDIS_KEY_TCASH_LAST_INQUIRY_CHECK_DATE))
      $lastInquiryCheckDate = Carbon::now()->toDateTimeString();

    $tCashInquiries = $this->bcaInquiries->getModel()->where('description', 'like', '%/TCASH%')
      ->where('created_at', '>', $lastInquiryCheckDate)->get();

    foreach($tCashInquiries as $item) {
      if ($item->reference_type == null) {

        $temp = explode('-', $item->description);
        $cashIn = intval($item->debit);

        if ($tcashUser = $this->tcashUsers->findByTelephone(purifyTelephone(trim($temp[count($temp) - 1])))) {
          $tcashUser->monthly_cash_in += $cashIn;
          $tcashUser->current_balance += $cashIn;
          $this->tcashUsers->save($tcashUser);
        }

        if ($replenishment = $this->billerReplenishments->findByAmount(120, $cashIn, BillerReplenishment::DISBURSEMENT_VENDOR_BCA_MANUAL)) {
          $item->reference_type = 'biller_replenishment_id';
          $item->reference = \json_encode(["" . $replenishment->id]);
          $this->bcaInquiries->save($item);

          $replenishment->reconciled_at = Carbon::now()->toDateTimeString();
          $this->billerReplenishments->save($replenishment);
        }
      }
    }

    $this->redis->hset(TCash::REDIS_TCASH, TCash::REDIS_KEY_TCASH_LAST_INQUIRY_CHECK_DATE, Carbon::now()->toDateTimeString());
  }

  public function reset() {
    $this->tcashUsers->getModel()->where('account_type', TCash::ACCOUNT_TYPE_FULL)
      ->update(['monthly_cash_in' => 0]);
  }

}
