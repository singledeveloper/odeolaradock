<?php

namespace Odeo\Domains\Biller\Unipin;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\Contract\BillerContract;

class UnipinManager implements BillerContract {

  private $purchaser, $inventoryValidator;

  public function __construct() {
    $this->inventoryValidator = app()->make(InventoryValidator::class);
    $this->purchaser = app()->make(\Odeo\Domains\Biller\Unipin\Purchaser\VoucherPurchaser::class);
  }

  public function purchase(PipelineListener $listener, $data) {
    return $this->purchaser->purchase($listener, $data);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    return $this->inventoryValidator->validateInventory($listener, $data);
  }

  public function inquiry(PipelineListener $listener, $data) {

    if (app()->environment() == 'production') return $listener->response(400, 'No access.');

    $client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ],
      'timeout' => 100
    ]);

    $response = [];

    //MLBBD_ID, MLBBS_ID, AOVV_ID, LMR_ID, RO_COIN_ID, MLBBSP_ID, BLEACHK_ID, DNMB_ID, EOCD_ID, LND_ID
    $gameCode = 'RO_COIN_ID';

    /*$url = 'api/v1/getGameList';
    $response = $client->request('GET', BillerUnipin::getURL($url), [
      'headers' => BillerUnipin::getTopupHeader($url)
    ]);*/

    /*$url = 'api/v1/getGameDetail';
    $response = $client->request('POST', BillerUnipin::getURL($url), [
      'headers' => BillerUnipin::getTopupHeader($url),
      'json' => ['gameCode' => $gameCode]
    ]);*/

    /*$url = 'api/v1/validateUserDetail';
    $response = $client->request('POST', BillerUnipin::getURL($url), [
      'headers' => BillerUnipin::getTopupHeader($url),
      'json' => [
        'gameCode' => $gameCode,
        '...fields' // depends from getGameDetail response
      ]
    ]);*/

    /*$url = 'api/v1/​orderFulfillment';
    $response = $client->request('POST', BillerUnipin::getURL($url), [
      'headers' => BillerUnipin::getTopupHeader($url),
      'json' => [
        'gameCode' => $gameCode,
        'validationLogId' => 'XXX', // response from validateUserDetail, expire 12 hours
        'referenceNo' => '%trxid%',
        'denominationId' => 'XXX' // response from getGameDetail
      ]
    ]);*/

    /*$url = 'api/v1/​fulfillmentInquiry'; // only if disconnected, recheck status of orderFulfillment
    $response = $client->request('POST', BillerUnipin::getURL($url), [
      'headers' => BillerUnipin::getTopupHeader($url),
      'json' => ['referenceNo' => '%trxid%']
    ]);*/
    // status >=0 sukses, else fail

    return $listener->response(200, $response ? $response : []);
  }
}