<?php

namespace Odeo\Domains\Campaign\DepositCashback\Helper;

use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Transaction\Repository\StoreDepositRepository;
use Odeo\Domains\Transaction\Repository\UserCashRepository;

class PresetGenerator {

  public $mapping = [
    '0.8' => 0.2,
    '1' => 0.3,
    '1.2' => 0.4,
    '1.4' => 0.5,
    '1.6' => 0.6,
    '1.8' => 0.8,
    '2' => 1,
    '2.5' => 2,
    '3' => 3
  ];

  private $storeDeposits, $userCashes;

  public function __construct() {
    $this->userCashes = app()->make(UserCashRepository::class);
    $this->storeDeposits = app()->make(StoreDepositRepository::class);
  }

  public function presetAmount($storeId, $index, $cash = null) {

    if (!$cash) {
      $cash = $this->userCashes->findByStoreId(null, $storeId, CashType::OCASHBACK);
    }

    $threshold = $cash->amount ?? 0;
    $threshold = floor($threshold / 500) * 500;

    $isThreshold = false;

    $avg = $this->storeDeposits->getAverageLastDeposit($storeId);
    $avg = floor($avg / 100000) * 100000;

    $avg2 = $this->storeDeposits->getAverageAllDeposit();
    $avg2 = floor($avg2 / 100000) * 100000;

    $avg = ($avg + $avg2) / 2;
    $avg = floor($avg / 100000) * 100000;

    $key = array_keys($this->mapping)[$index];
    $topupAmount = $key * $avg;
    $topupAmount = floor($topupAmount);

    $bonus = $this->mapping[$key] / 100 * $topupAmount;
    $bonus = floor($bonus / 500) * 500;

    if ($threshold < $bonus) {
      $bonus = $threshold;
      $isThreshold = true;
    }
    $getAmount = $topupAmount + $bonus;

    return [
      'bonus' => $bonus,
      'topup_amount' => $topupAmount,
      'get_amount' => $getAmount,
      'is_threshold' => $isThreshold,
      'preset_index' => $index,
    ];
  }
}
