<?php

namespace Odeo\Domains\Campaign\DepositCashback\Model;

use Odeo\Domains\Core\Entity;

class PresetCashback extends Entity {

  protected $hidden = ['created_at', 'updated_at'];
}
