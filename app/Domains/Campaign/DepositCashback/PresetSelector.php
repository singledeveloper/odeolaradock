<?php

namespace Odeo\Domains\Campaign\DepositCashback;

use Carbon\Carbon;
use Odeo\Domains\Campaign\DepositCashback\Helper\PresetGenerator;
use Odeo\Domains\Campaign\DepositCashback\Repository\PresetCashbackRepository;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Subscription\Repository\StoreRepository;
use Odeo\Domains\Transaction\Repository\StoreDepositRepository;
use Odeo\Domains\Transaction\Repository\UserCashRepository;

class PresetSelector {

  private $storeDeposits, $stores, $userCashes, $presetCashbacks, $generator;

  public function __construct() {
    $this->userCashes = app()->make(UserCashRepository::class);
    $this->storeDeposits = app()->make(StoreDepositRepository::class);
    $this->presetCashbacks = app()->make(PresetCashbackRepository::class);
    $this->stores = app()->make(StoreRepository::class);
    $this->generator = app()->make(PresetGenerator::class);
  }

  public function get(PipelineListener $listener, $data) {

    $day = Carbon::now()->dayOfWeek;

    if ($day != Carbon::MONDAY && $day != Carbon::WEDNESDAY && $day != Carbon::FRIDAY) {
      return $listener->response(400, "not today");
    }

    $storeId = $data['storeId'];

    $cash = $this->userCashes->findByStoreId(null, $storeId, CashType::OCASHBACK);
    $threshold = $cash->amount ?? 0;
    $threshold = floor($threshold / 500) * 500;

    if ($threshold == 0) {
      return $listener->response(400, "0 threshold");
    }

    if ($this->presetCashbacks->hasPresetToday($storeId)) {
      return $listener->response(400, "already used");
    }

    $presets = [];
    for ($i = 0; $i < count($this->generator->mapping); $i++) {
      $preset = $this->generator->presetAmount($storeId, $i, $cash);
      if ($preset['bonus'] == 0) continue;

      $isThreshold = $preset['is_threshold'];
      if ($isThreshold) break;
      
      $presets[] = array_except($preset, 'is_threshold');
    }

    if (count($presets) == 0) {
      return $listener->response(400, "no preset generated");
    }

    return $listener->response(200, [
      'presets' => $presets
    ]);
  }

  public function summary(PipelineListener $listener, $data) {

    $startDate = $data['start_date'] ?? null;
    $endDate = $data['end_date'] ?? null;

    $result = $this->presetCashbacks->summary($startDate, $endDate);

    return $listener->response(200, [
      'data' => $result
    ]);
  }

}

