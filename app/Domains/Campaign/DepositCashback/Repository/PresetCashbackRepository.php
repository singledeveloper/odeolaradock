<?php

namespace Odeo\Domains\Campaign\DepositCashback\Repository;

use Odeo\Domains\Campaign\DepositCashback\Model\PresetCashback;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\Repository;
use Carbon\Carbon;

class PresetCashbackRepository extends Repository {

  public function __construct(PresetCashback $preset) {
    $this->model = $preset;
  }

  public function findByOrderId($orderId) {
    return $this->model->where('order_id', $orderId)->first();
  }

  public function todayPreset($storeId) {
    return $this->model
      ->whereDate('created_at', '=', Carbon::now())
      ->where('store_id', $storeId)
      ->first();
  }

  public function hasPresetToday($storeId) {
    return $this->model
      ->join('orders', 'orders.id', '=', 'preset_cashbacks.order_id')
      ->where(function ($q) {
        $q->where('orders.status', '>=', OrderStatus::VERIFIED)
          ->where('orders.status', '<>', OrderStatus::CANCELLED);
      })
      ->whereDate('preset_cashbacks.created_at', '=', Carbon::now())
      ->where('store_id', $storeId)
      ->first();
  }

  public function summary($startDate = null, $endDate = null) {

    $query = $this->model->selectRaw('
      preset,
      count(*),
      sum(topup_amount) AS total_topup,
      avg(topup_amount) AS average_topup,
      min(topup_amount) AS min_topup,
      max(topup_amount) AS max_topup,
      sum(bonus)        AS total_bonus,
      avg(bonus)        AS average_bonus,
      min(bonus)        AS min_bonus,
      max(bonus)        AS max_bonus 
    ');

    if ($startDate) $query->whereDate('created_at', '>=', $startDate);
    if ($endDate) $query->whereDate('created_at', '<=', $endDate);

    return $query
      ->groupBy('preset')
      ->orderBy('preset')
      ->get();
  }

}