<?php

namespace Odeo\Domains\Campaign;

use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Campaign\Repository\CampaignMemberRepository;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Order\Repository\OrderRepository;
use Odeo\Domains\Transaction\Helper\CashInserter;

class ExecuteCashback {

  private $orders, $members, $users, $cashInserter;

  private $cashback = [
    10 => 1000,
    50 => 5000,
    100 => 10000,
    250 => 30000,
    500 => 60000,
    1000 => 130000,
    2500 => 325000,
    5000 => 700000,
    7500 => 1050000,
    10000 => 1500000,
  ];

  public function __construct() {
    $this->orders = app()->make(OrderRepository::class);
    $this->members = app()->make(CampaignMemberRepository::class);
    $this->users = app()->make(UserRepository::class);
    $this->cashInserter = app()->make(CashInserter::class);
  }

  public function run() {

    if (app()->environment() == 'production') return;

    $members = \DB::select(\DB::raw("
        SELECT
          users.id AS user_id,
          users.telephone,
          users.name,
          count(orders.id) AS count
        FROM campaign_members
          JOIN users ON campaign_members.user_id = users.id
          JOIN user_ktps ON users.id = user_ktps.user_id
          JOIN orders ON users.id = orders.user_id
          JOIN order_details ON orders.id = order_details.order_id
        WHERE campaign = 'cashback_sept_oct_2017' AND
              users.email IS NOT NULL AND
              user_ktps.verified_at IS NOT NULL AND
              users.status = '50000' AND
              orders.status = '50000' AND
              orders.created_at :: DATE >= '2017-09-20' AND orders.created_at::DATE <= '2017-10-03' AND
              service_detail_id IN (16, 17, 18, 19, 20, 4, 15, 11, 10)
        GROUP BY users.id, users.email, users.name
        HAVING count(orders.id) >= 10
        ORDER BY count DESC
    "));

    $this->cashInserter->clear();

    \DB::beginTransaction();

    foreach ($members as $member) {

      $userId = $member->user_id;

      $cash = $this->getCashback($member->count);

      $this->cashInserter->add([
        'user_id' => 811,
        'trx_type' => TransactionType::PROMO_CASHBACK,
        'cash_type' => CashType::OCASH,
        'amount' => -$cash,
        'data' => json_encode([
          'notes' => 'Campaign sept oct 2017',
          'to' => $member->name ? $member->name : $member->telephone,
          "event" => "Campaign sept oct 2017",
          "start_date" => '2017-09-20',
          "end_date" => '2017-10-03'
        ])
      ]);

      $this->cashInserter->add([
        'user_id' => $userId,
        'trx_type' => TransactionType::PROMO_CASHBACK,
        'cash_type' => CashType::OCASH,
        'amount' => $cash,
        'data' => json_encode([
          'notes' => 'Campaign sept oct 2017',
          'to' => $member->name ? $member->name : $member->telephone,
          "event" => "Campaign sept oct 2017",
          "start_date" => '2017-09-20',
          "end_date" => '2017-10-03'
        ])
      ]);
    }

     $this->cashInserter->run();

    \DB::commit();

  }

  public function getCashback($count) {

    $current = 0;

    foreach ($this->cashback as $target => $cash) {
      if ($count >= $target) {
        $current = $target;
      }
    }

    return $this->cashback[$current] ?? 0;
  }
}