<?php
/**
 * Created by PhpStorm.
 * User: mrp130
 * Date: 9/20/17
 * Time: 15:11
 */

namespace Odeo\Domains\Campaign\Jobs;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendDailySummaryEmail extends Job {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    $data = $this->data;
    $yesterdayDate = Carbon::now()->subDay(1)->format('d M Y');

    if (app()->environment() == 'production') {
      Mail::send('emails.campaign.cashback_sept_oct_2017',
        array_merge($data, ['date' => $yesterdayDate]),
        function ($m) use ($yesterdayDate, $data) {
          $m->from('noreply@odeo.co.id', 'ODEO');
          $m->to($data['member']['email'], $data['member']['name'])->subject('Promo Cashback - ' . $yesterdayDate);
        });
    }

  }

}
