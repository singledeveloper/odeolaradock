<?php

namespace Odeo\Domains\Campaign\Script;

use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Campaign\Repository\CampaignMemberRepository;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Order\Repository\OrderRepository;
use Odeo\Domains\Transaction\Helper\CashInserter;

class TransferCampaignCashbackSeptOct2017 {
  private $orders, $members, $users, $cashInserter;

  private $cashback = [
    10 => 1000,
    50 => 5000,
    100 => 10000,
    250 => 30000,
    500 => 60000,
    1000 => 130000,
    2500 => 325000,
    5000 => 700000,
    7500 => 1050000,
    10000 => 1500000,
  ];

  public function __construct() {
    $this->orders = app()->make(OrderRepository::class);
    $this->members = app()->make(CampaignMemberRepository::class);
    $this->users = app()->make(UserRepository::class);
    $this->cashInserter = app()->make(CashInserter::class);
  }

  public function run() {
    $members = $this->members->getSeptOct2017members();
    foreach ($members as $member) {
      $userId = $member->user_id;
      $cash = $this->getCashback($userId);

      $toUser = $this->users->findById($userId);

      $this->cashInserter->clear();
      $this->cashInserter->add([
        'user_id' => $userId,
        'trx_type' => TransactionType::PROMO_CASHBACK,
        'cash_type' => CashType::OCASHBACK,
        'amount' => $cash,
        'data' => json_encode([
          'notes' => 'campaign sept oct 2017',
          'to' => $toUser->name ? $toUser->name : $toUser->telephone
        ])
      ]);
    }
  }

  public function getCashback($userId) {
    $count = $this->orders->countUserTransactionBetweenDate(
      $userId,
      '2017-09-20',
      '2017-10-03'
    );

    $current = 0;

    foreach ($this->cashback as $target => $cash) {
      if ($count >= $target) {
        $current = $target;
      } else {
        break;
      }
    }

    return $this->cashback[$current];
  }
}