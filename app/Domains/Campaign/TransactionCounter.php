<?php

/**
 * Created by PhpStorm.
 * User: mrp130
 * Date: 9/15/2017
 * Time: 12:03 PM
 */

namespace Odeo\Domains\Campaign;

use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Order\Repository\OrderRepository;

class TransactionCounter {

  private $users;
  private $orders;

  private $cashback = [
    10 => 'Rp1.000',
    50 => 'Rp5.000',
    100 => 'Rp10.000',
    250 => 'Rp30.000',
    500 => 'Rp60.000',
    1000 => 'Rp130.000',
    2500 => 'Rp325.000',
    5000 => 'Rp700.000',
    7500 => 'Rp1.050.000',
    10000 => 'Rp1.500.000',
  ];

  public function __construct() {
    $this->users = app()->make(UserRepository::class);
    $this->orders = app()->make(OrderRepository::class);
  }

  public function septOct2017CampaignCounter($userId) {

    $count = $this->orders->countUserTransactionBetweenDate(
      $userId,
      '2017-09-20',
      '2017-10-03'
    );

    $current = 0;

    foreach ($this->cashback as $target => $cash) {
      if ($count >= $target) {
        $current = $target;
      } else {
        break;
      }
    }

    return [
      'top_info' => 'Jumlah transaksi Anda sejak 20 September 2017 hingga sekarang:',
      'count' => $count,
      'bottom_info' => 'Kumpulkan transaksi hingga 3 Oktober 2017, Raih cashback hingga 1.500.000 rupiah!',
      'cashback' => $this->cashback,
      'current' => $current,
    ];
  }

  public function referralCampaignCounter($userId) {
    $count = $this->orders->countUserTransaction($userId);
    return [
      'count' => $count,
    ];
  }

}