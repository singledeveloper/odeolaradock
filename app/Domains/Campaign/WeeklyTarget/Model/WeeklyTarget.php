<?php

namespace Odeo\Domains\Campaign\WeeklyTarget\Model;

use Odeo\Domains\Core\Entity;

class WeeklyTarget extends Entity {
  protected $hidden = ['created_at', 'updated_at'];

}
