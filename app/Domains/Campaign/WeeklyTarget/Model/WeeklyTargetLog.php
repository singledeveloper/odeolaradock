<?php

namespace Odeo\Domains\Campaign\WeeklyTarget\Model;

use Odeo\Domains\Core\Entity;

class WeeklyTargetLog extends Entity {
  protected $hidden = ['created_at', 'updated_at'];
  protected $dates = ['start_period'];
}
