<?php

namespace Odeo\Domains\Campaign\WeeklyTarget\Scheduler;

use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Campaign\WeeklyTarget\WeeklyTargetUpdater;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Subscription\Repository\StoreRepository;
use Odeo\Domains\Subscription\Repository\UserStoreRepository;
use Odeo\Domains\Transaction\Helper\CashInserter;


class WeeklyTargetScheduler {
  private $stores, $cashInserter, $users, $userStores, $weeklyTargets;

  public function __construct() {
    $this->stores = app()->make(StoreRepository::class);
    $this->cashInserter = app()->make(CashInserter::class);
    $this->users = app()->make(UserRepository::class);
    $this->userStores = app()->make(UserStoreRepository::class);
  }

  public function run() {
    $stores = $this->stores->getAllActiveStore();
    foreach ($stores as $store) {
      $this->weeklyStoreCampaignUpdateTarget($store->id);
    }
  }

  public function weeklyStoreCampaignUpdateTarget($storeId) {
    $data = $this->getRequestData();
    $data['storeId'] = $storeId;

    $this->pipeline->add(new Task(WeeklyTargetUpdater::class, 'update'));

    return $this->executeAndResponse($data);
  }

  public function sendCash($storeId) {
    $userId = $this->userStores->findOwner($storeId)[0]['user_id'];
    $user = $this->users->findById($userId);
    $cash = $this->weeklyTargets->getTarget($storeId)->reward;

    $this->cashInserter->add([
      'user_id' => 811,
      'trx_type' => TransactionType::PROMO_CASHBACK,
      'cash_type' => CashType::OCASH,
      'amount' => -$cash,
      'data' => json_encode([
        'notes' => 'Campaign sept oct 2017',
        'to' => $user->name ? $user->name : $user->telephone,
        "event" => "weekly target bonus",
      ])
    ]);

    $this->cashInserter->add([
      'user_id' => $userId,
      'trx_type' => TransactionType::PROMO_CASHBACK,
      'cash_type' => CashType::OCASH,
      'amount' => $cash,
      'data' => json_encode([
        'notes' => 'Campaign sept oct 2017',
        'to' => $user->name ? $user->name : $user->telephone,
        "event" => "weekly target bonus",
      ])
    ]);
  }
}