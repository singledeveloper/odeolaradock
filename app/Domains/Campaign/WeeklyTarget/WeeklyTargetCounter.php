<?php
/**
 * Created by PhpStorm.
 * User: mrp130
 * Date: 10/4/2017
 * Time: 3:31 AM
 */

namespace Odeo\Domains\Campaign\WeeklyTarget;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Marketing\Repository\StoreRevenueRepository;

class WeeklyTargetCounter
{
  private $revenues;

  public function __construct() {
    $this->revenues = app()->make(StoreRevenueRepository::class);
  }

  public function get($storeId) {
    $orderCount = $this->revenues->getWeekStoreOrder($storeId);
    if (!$orderCount)
      return ["order" => []];

    return ["order" => $orderCount];

  }
}