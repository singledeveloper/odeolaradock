<?php

namespace Odeo\Domains\Campaign\WeeklyTarget;


use Odeo\Domains\Campaign\WeeklyTarget\Repository\WeeklyTargetRepository;
use Odeo\Domains\Core\PipelineListener;

class WeeklyTargetInserter {

  private $targets;

  public function __construct() {
    $this->targets = app()->make(WeeklyTargetRepository::class);
  }

  public function insert($data) {

    $target = $this->targets->getNew();

    $target->store_id = $data['storeId'];
    $target->target = $data['target'];
    $target->reward = $data['reward'];

    $this->targets->save($target);

    return ["target" => $target];

  }

}