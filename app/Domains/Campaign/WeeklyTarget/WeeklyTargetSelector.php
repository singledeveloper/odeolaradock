<?php

namespace Odeo\Domains\Campaign\WeeklyTarget;

use Odeo\Domains\Campaign\WeeklyTarget\Repository\WeeklyTargetRepository;
use Odeo\Domains\Core\PipelineListener;

class WeeklyTargetSelector
{

  private $targets, $inserter, $counter;

  public function __construct() {
    $this->counter = app()->make(WeeklyTargetCounter::class);
    $this->targets = app()->make(WeeklyTargetRepository::class);
    $this->inserter = app()->make(WeeklyTargetInserter::class);
  }

  public function get(PipelineListener $listener, $data) {
    $storeId = $data['storeId'];

    return $listener->response(200, array_merge(
        $this->counter->get($storeId),
        $this->getByStoreId($storeId)
      )
    );
  }

  public function getByStoreId($storeId) {
    $target = $this->targets->getTarget($storeId);
    if (count($target['target']) == 0) {
      $target = $this->createTarget($storeId);
    }

    return ["target" => $target];
  }

  private function createTarget($storeId) {
    $data['storeId'] = $storeId;
    $data['target'] = 50;
    $data['reward'] = 50 * 100;
    return $this->inserter->insert($data);
  }

}
