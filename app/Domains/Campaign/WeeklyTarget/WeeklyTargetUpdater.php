<?php

namespace Odeo\Domains\Campaign\WeeklyTarget;

use Odeo\Domains\Campaign\WeeklyTarget\Repository\WeeklyTargetLogRepository;
use Odeo\Domains\Campaign\WeeklyTarget\Repository\WeeklyTargetRepository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Marketing\Repository\StoreRevenueRepository;

class WeeklyTargetUpdater
{

  private $targets;
  private $revenues;
  private $logs;

  public function __construct() {
    $this->revenues = app()->make(StoreRevenueRepository::class);
    $this->targets = app()->make(WeeklyTargetRepository::class);
    $this->logs = app()->make(WeeklyTargetLogRepository::class);
  }

  public function update(PipelineListener $listener, $data) {
    $storeId = $data['storeId'];

    $target = $this->targets->getTarget($storeId);
    if (!$target)
      return $listener->response(404);

    $orderCount = $this->revenues->getWeekStoreOrder($storeId);
    $this->log($storeId);
    $count = $orderCount['count'];
    $target_order = $target->target;

    if ($count >= $target_order) {
      $target->target = $target_order + 10;
      $target->reward = ($target_order + 10) * 100;
    }

    $this->targets->save($target);
    return $listener->response(200, [
      'target' => $target
    ]);
  }

  public function log($storeId) {
    $target = $this->targets->getTarget($storeId);
    if (!$target)
      throw new \Exception('input weekly target log failed');

    $orderCount = $this->revenues->getWeekStoreOrder($storeId);

    $log = $this->logs->getNew();
    $log->store_id = $target->store_id;
    $log->target = $target->target;
    $log->reward = $target->reward;
    $log->start_period = $orderCount['start_date'];
    $log->order_count = $orderCount['count'];

    $this->logs->save($log);

  }

}