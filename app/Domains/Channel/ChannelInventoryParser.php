<?php

namespace Odeo\Domains\Channel;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Helper\Currency;

class ChannelInventoryParser {

  public function __construct() {
    $this->service = app()->make(\Odeo\Domains\Inventory\Repository\ServiceRepository::class);
    $this->user = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function getServiceDetailIdFromInventory(PipelineListener $listener, $data) {
    $data['service_detail_id'] = 0;
    if(isset($data['inventory']) && isset($data['service_id'])) {
      $service = $this->service->findById($data['service_id']);
      foreach($inventories = $data['inventory'] as $inventory) {
        if($inventory['type'] == $service->name) {
          foreach($inventory['providers'] as $provider) {
            if($provider['is_currently_used']) {
              $data['service_detail_id'] = $provider['service_detail_id'];
            }
          }
        } else {
          continue;
        }
      }
      unset($data['inventory']);
    }
    return $listener->response(200, $data, true);
  }

  public function getItemIdFromInventory(PipelineListener $listener, $data) {
    $data['item_id'] = 0;
    if(isset($data['inventory']) && count($data['inventory'])) {
      $item = $data['inventory'][0]['details'][0];
      $data['item_id'] = $item['item_id'];
      $data['item_name'] = $item['name'];
      $data['amount'] = $item['price'];
      $data['price'] = $item['price'];
      $data['price']['amount'] = -$data['price']['amount'];
      $data['item_detail'] = [
        'operator' => $data['operator'],
        'number' => $this->user->findById($data['auth']['user_id'])->telephone
      ];
      unset($data['inventory']);
    }
    return $listener->response(200, $data, true);
  }
}
