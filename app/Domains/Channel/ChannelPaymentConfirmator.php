<?php

namespace Odeo\Domains\Channel;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\CashConfig;
use Odeo\Domains\Constant\ChannelStatus;
use Odeo\Domains\Constant\ChannelType;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Channel\ChannelPaymentProcessor;
use Odeo\Domains\Network\OrderBonusRequester;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\OrderVerificator;
use Odeo\Domains\Inventory\Pulsa\PulsaManager;
use Odeo\Domains\Marketing\BudgetConverter;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Domains\Subscription\StoreSelector;
use Odeo\Domains\Transaction\CashUpdater;
use Odeo\Domains\Transaction\DepositRequester;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Transaction\TempDepositRequester;

class ChannelPaymentConfirmator {

  public function __construct() {
    $this->channel = app()->make(\Odeo\Domains\Channel\Repository\StoreChannelRepository::class);
    $this->user = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->userCash = app()->make(\Odeo\Domains\Transaction\Repository\UserCashRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->selector = app()->make(\Odeo\Domains\Channel\ChannelSelector::class);
  }

  public function confirm(PipelineListener $listener, $data) {
    $response = [];

    $pinRequiredAmount = CashConfig::MIN_PAYMENT_REQUIRE_PIN;
    $skipConfirmation = isset($data['skip_confirmation']) ? $data['skip_confirmation'] : true;

    if(isset($data['amount']['amount']) && $data['amount']['amount'] > $pinRequiredAmount) {
      $skipConfirmation = false;
    }

    switch($data['channel_type']) {
      case ChannelType::VARIABLE: {
        $skipConfirmation = false;
        $response['amount'] = $this->currency->formatPrice(0);
        break;
      }
      case ChannelType::FIXED: {
        $response['amount'] = $data['amount'];
        if($skipConfirmation) {
          $listener->addNext(new Task(CashUpdater::class, 'pay', [
            'amount' => $data['amount']['amount'],
            'currency' => $data['amount']['currency'],
          ]));
          $listener->addNext(new Task(ChannelPaymentProcessor::class, 'newUserCashback'));
          $listener->addNext(new Task(ChannelPaymentProcessor::class, 'MDR'));
        }
        break;
      }
      case ChannelType::PULSA: {
        $listener->addNext(new Task(ChannelInventoryParser::class, 'getServiceDetailIdFromInventory', [
          'service_id' => Service::PULSA
        ]));
        $listener->addNext(new Task(PulsaManager::class, 'searchNominal', [
          'prefix' => $this->user->findById($data['auth']['user_id'])->telephone,
          'with_nominal' => $data['amount']['amount']
        ]));
        $listener->addNext(new Task(ChannelInventoryParser::class, 'getItemIdFromInventory'));
        if($skipConfirmation) {
          $listener->addNext(new Task(CartRemover::class, 'clear'));
          $listener->addNext(new Task(CartInserter::class, 'addToCart'));
          $listener->addNext(new Task(CartInserter::class, 'addCharge', [
            'type' => 'ocash',
          ]));
          $listener->addNext(new Task(CartCheckouter::class, 'checkout', [
            'signature' => NULL,
            'gateway_id' => Payment::ANDROID_OCASH,
            'platform_id' => 0,
          ]));
          $listener->addNext(new Task(TempDepositRequester::class, 'tempDeposit'));
          $listener->addNext(new Task(CashUpdater::class, 'deduct', [
            'amount' => $data['amount']['amount'],
            'currency' => $data['amount']['currency'],
          ]));
          $listener->addNext(new Task(PaymentRequester::class, 'request', [
            'opc' => 45
          ]));
          $listener->addNext(new Task(OrderVerificator::class, 'verify'));
          $listener->addNext(new Task(OrderBonusRequester::class, 'create'));
          $listener->addNext(new Task(BudgetConverter::class, 'rush'));
          $listener->addNext(new Task(OrderVerificator::class, 'completeOrder'));
        }
        break;
      }
    }

    $response['pin_required_amount'] = $pinRequiredAmount;
    $response['skip_confirmation'] = $skipConfirmation;

    return $listener->response(200, $response);
  }
}
