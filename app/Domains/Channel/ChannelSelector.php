<?php

namespace Odeo\Domains\Channel;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Constant\ChannelStatus;
use Odeo\Domains\Core\Repository;

class ChannelSelector implements SelectorListener {

  public function __construct() {
    $this->channel = app()->make(\Odeo\Domains\Channel\Repository\StoreChannelRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function _transforms($item, Repository $repository) {
    $channel = [];
    if ($item->id) $channel["channel_id"] = $item->id;
    if ($repository->hasExpand('channel_code')) {
      $channel['channel_code'] = $item->code;
    }
    if ($repository->hasExpand('user') && $user = $item->user) {
      $channel['user_id'] = $item->user->id;
      $channel['user_name'] = $item->user->name;
    }
    if ($repository->hasExpand('store') && $store = $item->store) {
      $channel['store_id'] = $item->store->id;
      $channel['store_name'] =  $item->store->name;
    }
    if ($repository->hasExpand('mdr') && $store = $item->store && $mdr = $store->mdr) {
      $channel['mdr'] = $mdr->base_mdr;
    }
    if ($item->channel_data && $channelData = json_decode($item->channel_data)) {
      if($repository->hasExpand('channel_data')) {
        $channel['channel_data'] = [
          'name' => $channelData->name,
          'phone_number' => $channelData->phone_number,
          'address' => $channelData->address,
          'city' => $channelData->city,
          'province' => $channelData->province,
          'zip_code' => $channelData->zip_code
        ];
      }
      $channel['channel_name'] = isset($channel['store_name']) ? $channel['store_name'] . ' - ' . $channelData->name : $channelData->name;
      if(isset($channelData->device_id)) $channel['device_id'] = $channelData->device_id;
    }
    if ($repository->hasExpand('channel_settings') && $item->channel_settings && $channelSettings = json_decode($item->channel_settings)) {
      $channel['channel_type'] = $channelSettings->type;
      if(isset($channelSettings->amount)) {
        $channel['amount'] = $this->currency->formatPrice($channelSettings->amount, $channelSettings->currency);
      }
    }
    if ($item->status) {
      if ($item->status == ChannelStatus::PENDING) $status = 'PENDING';
      else if ($item->status == ChannelStatus::ACTIVE) $status = 'ACTIVE';
      else if ($item->status == ChannelStatus::SUSPENDED) $status = 'SUSPENDED';
      else $status = 'UNKNOWN';
      $channel['status'] = $status;
    }
    if ($item->daily_payment_limit) $channel['daily_payment_limit'] = $this->currency->formatPrice($item->daily_payment_limit);

    return $channel;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {
    $this->channel->normalizeFilters($data);

    $channels = [];
    foreach($this->channel->get() as $item) {
      $channels[] = $this->_transforms($item, $this->channel);
    }

    if (sizeof($channels) > 0)
      return $listener->response(200, array_merge(
        ["channels" => $this->_extends($channels, $this->channel)],
        $this->channel->getPagination()
      ));

    return $listener->response(204, ["channels" => []]);
  }

  public function getDetail(PipelineListener $listener, $data) {
    $this->channel->normalizeFilters($data);

    if (isset($data['channel_id'])) $channel = $this->channel->findById($data['channel_id']);
    else if (isset($data['channel_code'])) $channel = $this->channel->findByAttributes('code', $data['channel_code']);

    if (isset($channel) && $channel) {
      $channel = $this->_transforms($channel, $this->channel);
      if($channel['status'] == 'SUSPENDED') {
        return $listener->response(403, $channel);
      }
      return $listener->response(200, $this->_extends($channel, $this->channel));
    }

    return $listener->response(204);
  }
}
