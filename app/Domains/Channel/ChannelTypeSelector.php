<?php

namespace Odeo\Domains\Channel;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Constant\ChannelType;

class ChannelTypeSelector {

  public function __construct() {
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function get(PipelineListener $listener, $data) {
    $types = array();

    $types[] = array(
      'value' => ChannelType::FIXED,
      'name' => 'Fixed Price',
      'field' => [
        'type' => 'text',
      ]
    );
    $types[] = array(
      'value' => ChannelType::VARIABLE,
      'name' => 'Variable Price',
      'field' => [
        'type' => 'none'
      ]
    );

    $store = $this->store->findById($data['store_id']);

    if($store && $store->plan->name != 'FREE') {
      $available_amounts = array(5000, 10000, 25000, 50000, 100000);
      $pulsa_amounts = [];
      foreach($available_amounts as $key => $value) {
        $pulsa_amounts[] = $this->currency->formatPrice($value);
      }
      $types[] = array(
        'value' => ChannelType::PULSA,
        'name' => 'Beli Pulsa',
        'field' => [
          'type' => 'select',
          'values' => $pulsa_amounts
          ]
        );
    }

    return $listener->response(200, $types);
  }
}
