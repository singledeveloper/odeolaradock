<?php

namespace Odeo\Domains\Channel\Jobs;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Odeo\Domains\Constant\QRCodeConfig;
use Odeo\Jobs\Job;

class GenerateQRCodeImage extends Job  {

  use InteractsWithQueue, SerializesModels;

  protected $amount;
  protected $channelCodes;
  protected $existingCodes;

  public function __construct($amount) {
    parent::__construct();
    $this->amount = $amount;
  }

  public function handle() {
    $this->channelCodes = app()->make(\Odeo\Domains\Channel\Repository\ChannelCodeRepository::class);
    $this->existingCodes = $this->channelCodes->getAll()->pluck('id', 'code');
    $channelCodes = array();
    list($channelCode, $maxAmount) = $this->createAndSaveQR();
    $channelCodes[] = $channelCode;
    $amount = $maxAmount > 0 ? min($this->amount, $maxAmount) : $this->amount;
    for ($i = 1; $i < $amount; $i++) {
      list($channelCode) = $this->createAndSaveQR();
      $channelCodes[] = $channelCode;
    }
    $this->channelCodes->saveBulk($channelCodes);
  }

  private function createAndSaveQR() {
    $channelCode = array();
    $length = 2;

    $hashData = [
      "salt" => mt_rand(),
      "channel_id" => uniqid(),
    ];
    $hash = sha1(json_encode($hashData));
    do {
      if ($length == 40) {
        $hashData = [
          "salt" => mt_rand(),
          "channel_id" => uniqid(),
        ];
        $hash = sha1(json_encode($hashData));
        $length = 2;
      }
      $code = substr($hash, 0, ++$length);
    } while (isset($this->existingCodes[$code]));
    $this->existingCodes[$code] = 1;

    $channelCode['code'] = $code;
    $channelCode['hash'] = $hash;

    $client = new Client([
      'base_uri' => QRCodeConfig::BASE_URI,
      'http_errors' => false,
      'verify' => false,
      'headers' => [
        'X-Mashape-Key' => app()->environment() == 'production' ? QRCodeConfig::API_KEY_PRODUCTION : QRCodeConfig::API_KEY_DEVELOPMENT
      ],
    ]);

    $response = $client->request('GET', '', [
      'query' => [
        'data' => '{"TYPE":"text","DATA":{"TEXT":"' . $code . '"}}',
        'setting' => '{"LAYOUT":{"COLORBG":"transparent","GRADIENT_TYPE":"NO_GR","COLOR1":"0A2528"},"EYES":{"EYE_TYPE":"ER_IR"},"E":"H","BODY_TYPE":5}',
        'T' => 'SVG'
      ]
    ]);

    $rateLimit = $response->getHeader('X-Ratelimit-Requests-Remaining');
    $limit = intval(array_pop($rateLimit));
    $body = $response->getBody();

    $filepath = "/qr/" . (app()->environment() == 'production' ? '' : 'test/') . $code . ".svg";

    $channelCode['qr_url'] = $filepath;

    Storage::disk('s3')->put($filepath, (string)$response->getBody()->getContents(), 'public');

    $channelCode['created_at'] = Carbon::now();
    return [$channelCode, $limit];
  }
}
