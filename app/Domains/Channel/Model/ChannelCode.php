<?php

namespace Odeo\Domains\Channel\Model;

use Odeo\Domains\Core\Entity;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChannelCode extends Entity
{
    use softDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'deleted_at'];

    protected $fillable = ['code', 'hash', 'qr_url'];

    public $timestamps = false;

    public static function boot() {
      parent::boot();

      static::creating(function($model) {
        $timestamp = $model->freshTimestamp();
        $model->setCreatedAt($timestamp);
      });
    }

}
