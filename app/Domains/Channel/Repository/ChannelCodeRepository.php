<?php

namespace Odeo\Domains\Channel\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Channel\Model\ChannelCode;

class ChannelCodeRepository extends Repository {

  public function __construct(ChannelCode $code) {
    $this->model = $code;
  }

  public function getChannelCodes($amount, $id = 0) {
    $query = $this->model->withTrashed()->orderBy('id', 'desc')->limit($amount);

    if($id) $query = $query->where('id', '<=', $id);

    return $query->get();
  }
}
