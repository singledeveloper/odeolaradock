<?php

namespace Odeo\Domains\Constant;


class Affiliate {

  const DEFAULT_PLN_INQUIRY_FEE = 50;
  const DEFAULT_PRODUCT_CATEGORY_FEE = 2500;
  const DEFAULT_PRODUCT_CODE_FEE = 2500;
  const DEFAULT_PRODUCT_DESCRIPTION_FEE = 2000;
  const DEFAULT_PRODUCT_PRICE_FEE = 200;
  const DEFAULT_PRODUCT_SMS_FEE = 600;

  const API_TYPE_JSON = 'json';
  const API_TYPE_XML = 'xml2';
  const API_TYPE_GET = 'get';
  const API_TYPE_XMLRPC = 'xml';

  public static function groupXml() {
    return [
      self::API_TYPE_XML,
      self::API_TYPE_XMLRPC
    ];
  }

  public static function allowedType() {
    return [
      self::API_TYPE_JSON,
      self::API_TYPE_XML,
      self::API_TYPE_GET,
      self::API_TYPE_XMLRPC
    ];
  }

  public static function finalizeErrorMessage($number, $err) {
    $number = revertTelephone($number);
    $number = explode('/', $number)[0];
    $number = explode('#', $number)[0];

    $err = is_array($err) ? implode(', ', $err) : $err;

    if (strpos($err, 'Trx sdh pernah') === false)
      return 'GAGAL Trx ke ' . $number . '. ' . $err;
    return $err;
  }

}