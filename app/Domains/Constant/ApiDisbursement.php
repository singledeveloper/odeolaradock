<?php

namespace Odeo\Domains\Constant;

class ApiDisbursement {

  const PENDING = '10000';
  const PENDING_NOT_ENOUGH_DEPOSIT = '10001';
  const ON_PROGRESS = '30000';
  const ON_PROGRESS_WAIT_FOR_NOTIFY = '30001';
  const FAIL_PROCESSING = '40000';

  const VALIDATION_ON_PROGRESS = '10010';
  const VALIDATION_SUCCESS = '10050';
  const VALIDATION_MANUAL_SUCCESS = '10051';
  const VALIDATION_FAILED = '10090';
  const VALIDATION_FAILED_WRONG_ACCOUNT_NUMBER = '10091';
  const VALIDATION_FAILED_NAME_MISMATCH = '10092';
  const VALIDATION_FAILED_CLOSED_BANK_ACCOUNT = '10093';

  const COMPLETED = '50000';
  const SUSPECT = '80000';
  const SUSPECT_EVENTUALLY_SUCCESS = '80001';

  const FAILED = '90000';
  const FAILED_WRONG_ACCOUNT_NUMBER = '90001';
  const FAILED_CLOSED_BANK_ACCOUNT = '90003';
  const FAILED_BANK_REJECTION = '90004';
  const FAILED_VENDOR_DOWN = '90005';
  const FAILED_DUPLICATE_REQUEST = '90006';
  const CANCELLED = '90007';

  const FAILED_STATUS_LIST = [self::FAILED, self::FAILED_WRONG_ACCOUNT_NUMBER, self::FAILED_CLOSED_BANK_ACCOUNT,
    self::FAILED_BANK_REJECTION, self::FAILED_VENDOR_DOWN, self::FAILED_DUPLICATE_REQUEST];

  const STATUS_FILTERS = [
    self::PENDING, self::ON_PROGRESS, self::COMPLETED, self::SUSPECT, self::FAILED, self::FAILED_WRONG_ACCOUNT_NUMBER,
    self::FAILED_CLOSED_BANK_ACCOUNT, self::FAILED_BANK_REJECTION, self::FAILED_VENDOR_DOWN, self::FAILED_DUPLICATE_REQUEST,

    self::VALIDATION_ON_PROGRESS, self::VALIDATION_MANUAL_SUCCESS, self::VALIDATION_SUCCESS,
    self::VALIDATION_FAILED_NAME_MISMATCH, self::VALIDATION_FAILED, self::VALIDATION_FAILED_WRONG_ACCOUNT_NUMBER,
    self::VALIDATION_FAILED_CLOSED_BANK_ACCOUNT,
  ];

  const VALIDATION_STATUS_LIST = [
    self::VALIDATION_ON_PROGRESS, self::VALIDATION_SUCCESS, self::VALIDATION_MANUAL_SUCCESS,
    self::VALIDATION_FAILED_NAME_MISMATCH, self::VALIDATION_FAILED, self::VALIDATION_FAILED_WRONG_ACCOUNT_NUMBER,
    self::VALIDATION_FAILED_CLOSED_BANK_ACCOUNT,
  ];

  const GENERAL_STATUSES = [
    self::ON_PROGRESS,
    self::VALIDATION_ON_PROGRESS,
    self::VALIDATION_FAILED,
    self::VALIDATION_SUCCESS,
    self::COMPLETED,
    self::SUSPECT,
    self::FAILED,
  ];

  const COMPLETED_VALIDATION_STATUS_LIST = [
    self::VALIDATION_SUCCESS, self::VALIDATION_FAILED_NAME_MISMATCH,
    self::VALIDATION_FAILED, self::VALIDATION_FAILED_WRONG_ACCOUNT_NUMBER,
    self::VALIDATION_FAILED_CLOSED_BANK_ACCOUNT, self::VALIDATION_MANUAL_SUCCESS,
  ];

  const COMPLETED_STATUS_LIST = [
    self::FAILED, self::FAILED_WRONG_ACCOUNT_NUMBER, self::FAILED_CLOSED_BANK_ACCOUNT,
    self::FAILED_BANK_REJECTION, self::FAILED_VENDOR_DOWN, self::FAILED_DUPLICATE_REQUEST,
    self::COMPLETED,
  ];

  const GENERALIZED_STATUS_LIST = [
    self::PENDING,
    self::SUSPECT,
    self::COMPLETED,
    self::FAILED,
  ];

  const GENERALIZED_STATUS_LIST_BY_BATCH_STATUS = [
    BatchDisbursementStatus::PENDING => [
      self::VALIDATION_ON_PROGRESS,
      self::VALIDATION_SUCCESS,
      self::VALIDATION_FAILED,
    ],
    BatchDisbursementStatus::ON_PROGRESS => [
      self::ON_PROGRESS,
      self::SUSPECT,
      self::COMPLETED,
      self::FAILED,
    ],
    BatchDisbursementStatus::COMPLETED => [
      self::COMPLETED,
      self::FAILED,
    ],
    BatchDisbursementStatus::CANCELLED => [
      self::CANCELLED,
    ],
  ];

  const FEE = 5500;
  //const FEE_ODEO = 250;
  const FEE_ODEO = 0;
  const EXTRA_FEE = 6000;
  const EXTRA_FEE_AMOUNT_THRESHOLD = 1500000;
  const INQUIRY_FEE = 100;
  const INQUIRY_VALIDATION_FEE = 100;
  const MIN_AMOUNT = 35000;
  const MIN_AMOUNT_ODEO = 1;
  const MAX_AMOUNT = 50000000;
  const MAX_DESCRIPTION_LENGTH = 50;

  const NO_ACCESS = 'no-access';

  const ERROR_INVALID_REQUEST = '40000';
  const ERROR_INVALID_TIMESTAMP = '40001';
  const ERROR_INVALID_BANK = '40002';
  const ERROR_AMOUNT_BELOW_MINIMUM_LIMIT = '40003';
  const ERROR_AMOUNT_EXCEEDS_MAXIMUM_LIMIT = '40004';
  const ERROR_INVALID_DESCRIPTION = '40005';
  const ERROR_INSUFFICIENT_BALANCE = '40011';
  const ERROR_UNAUTHORIZED = '40100';
  const ERROR_SIGNATURE_MISMATCH = '40101';
  const ERROR_TIMESTAMP_EXPIRED = '40102';
  const ERROR_GENERAL_ERROR = '40400';

  const DISBURSEMENT_VENDOR_FLIP_API_DISBURSEMENT = 'flip_api_disbursement';
  const DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT = 'bca_api_disbursement';
  const DISBURSEMENT_VENDOR_BNI_API_DISBURSEMENT = 'bni_api_disbursement';
  const DISBURSEMENT_VENDOR_BRI_API_DISBURSEMENT = 'bri_api_disbursement';
  const DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT = 'artajasa_api_disbursement';
  const DISBURSEMENT_VENDOR_CIMB_API_DISBURSEMENT = 'cimb_api_disbursement';
  const DISBURSEMENT_VENDOR_PERMATA_API_DISBURSEMENT = 'permata_api_disbursement';
  const DISBURSEMENT_VENDOR_MANDIRI_API_DISBURSEMENT = 'mandiri_api_disbursement';
  const DISBURSEMENT_VENDOR_REDIG_API_DISBURSEMENT = 'redig_api_disbursement';
  const DISBURSEMENT_VENDOR_ODEO_DISBURSEMENT = 'odeo_disbursement';

  const CACHED_INQUIRY_RESULT = 'cached_inquiry';

  const NON_FREE_INQUIRY_STATUS = [
    self::COMPLETED,
    self::FAILED_WRONG_ACCOUNT_NUMBER,
    self::FAILED_CLOSED_BANK_ACCOUNT,
  ];

  const ERROR_CODE_TO_MESSAGE = [
    self::ERROR_INVALID_REQUEST => 'INVALID REQUEST',
    self::ERROR_INVALID_TIMESTAMP => 'INVALID TIMESTAMP',
    self::ERROR_INVALID_BANK => 'INVALID BANK',
    self::ERROR_AMOUNT_BELOW_MINIMUM_LIMIT => 'AMOUNT BELOW MINIMUM LIMIT',
    self::ERROR_AMOUNT_EXCEEDS_MAXIMUM_LIMIT => 'AMOUNT EXCEED MAXIMUM LIMIT',
    self::ERROR_INVALID_DESCRIPTION => 'INVALID DESCRIPTION',
    self::ERROR_INSUFFICIENT_BALANCE => 'INSUFFICIENT BALANCE',
    self::ERROR_UNAUTHORIZED => 'UNAUTHORIZED',
    self::ERROR_SIGNATURE_MISMATCH => 'SIGNATURE MISMATCH',
    self::ERROR_TIMESTAMP_EXPIRED => 'TIMESTAMP EXPIRED',
    self::ERROR_GENERAL_ERROR => 'GENERAL ERROR',
  ];

  const STATUS_CODE_TO_MESSAGE = [
    self::PENDING => 'api_pending',
    self::PENDING_NOT_ENOUGH_DEPOSIT => 'api_pending_not_enough_deposit',
    self::VALIDATION_ON_PROGRESS => 'api_validation_on_progress',
    self::VALIDATION_SUCCESS => 'api_validation_success',
    self::VALIDATION_MANUAL_SUCCESS => 'api_validation_manual_success',
    self::VALIDATION_FAILED => 'api_validation_failed',
    self::VALIDATION_FAILED_NAME_MISMATCH => 'api_validation_failed_name_mismatch',
    self::VALIDATION_FAILED_WRONG_ACCOUNT_NUMBER => 'api_validation_failed_wrong_number',
    self::VALIDATION_FAILED_CLOSED_BANK_ACCOUNT => 'api_validation_failed_closed_account',
    self::ON_PROGRESS => 'api_on_progress',
    self::ON_PROGRESS_WAIT_FOR_NOTIFY => 'api_on_progress_wait_notify',
    self::FAIL_PROCESSING => 'api_fail_processing',
    self::COMPLETED => 'api_completed',
    self::SUSPECT => 'api_suspect',
    self::SUSPECT_EVENTUALLY_SUCCESS => 'api_suspect_even_success',
    self::FAILED => 'api_failed',
    self::FAILED_WRONG_ACCOUNT_NUMBER => 'api_failed_wrong_number',
    self::FAILED_CLOSED_BANK_ACCOUNT => 'api_failed_closed_account',
    self::FAILED_BANK_REJECTION => 'api_failed_bank_rejection',
    self::FAILED_VENDOR_DOWN => 'api_failed_vendor_down',
    self::FAILED_DUPLICATE_REQUEST => 'api_failed_duplicate',
    self::CANCELLED => 'api_cancelled',
  ];

  static function getStatusMessage($statusCode) {
    if (isset(self::STATUS_CODE_TO_MESSAGE[$statusCode])) {
      return trans('status.' . self::STATUS_CODE_TO_MESSAGE[$statusCode]);
    }

    return trans('status.api_unknown') . ': ' . $statusCode;
  }

  static function getErrorMessage($errorCode) {
    $message = 'Unknown error: ' . $errorCode;

    if (isset(self::ERROR_CODE_TO_MESSAGE[$errorCode])) {
      $message = self::ERROR_CODE_TO_MESSAGE[$errorCode];
    }

    return $message;
  }

  static function getErrorObject($errorCode) {
    return [
      'error_code' => $errorCode,
      'message' => self::getErrorMessage($errorCode),
    ];
  }

  static function generalizeStatus($status) {
    switch ($status) {
      case self::PENDING:
      case self::PENDING_NOT_ENOUGH_DEPOSIT:
      case self::ON_PROGRESS:
      case self::ON_PROGRESS_WAIT_FOR_NOTIFY:
      case self::FAIL_PROCESSING:
        return self::ON_PROGRESS;

      case self::VALIDATION_ON_PROGRESS;
        return self::VALIDATION_ON_PROGRESS;

      case self::VALIDATION_SUCCESS;
      case self::VALIDATION_MANUAL_SUCCESS:
        return self::VALIDATION_SUCCESS;

      case self::VALIDATION_FAILED;
      case self::VALIDATION_FAILED_WRONG_ACCOUNT_NUMBER;
      case self::VALIDATION_FAILED_NAME_MISMATCH;
      case self::VALIDATION_FAILED_CLOSED_BANK_ACCOUNT;
        return self::VALIDATION_FAILED;

      case self::COMPLETED:
        return self::COMPLETED;

      case self::SUSPECT:
      case self::SUSPECT_EVENTUALLY_SUCCESS:
        return self::SUSPECT;

      case self::FAILED:
      case self::FAILED_WRONG_ACCOUNT_NUMBER:
      case self::FAILED_CLOSED_BANK_ACCOUNT:
      case self::FAILED_BANK_REJECTION:
      case self::FAILED_VENDOR_DOWN:
      case self::FAILED_DUPLICATE_REQUEST:
        return self::FAILED;

      case self::CANCELLED:
        return self::CANCELLED;

      default:
        return $status;
    }
  }
}
