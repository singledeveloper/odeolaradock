<?php

namespace Odeo\Domains\Constant;

use Carbon\Carbon;

class ArtajasaDisbursement {

  const PENDING = '10000';
  const SUSPECT = '20000';
  const TIMEOUT = '30000';
  const SUCCESS = '50000';
  const FAIL = '90000';

  const INQUIRY_COST = 0;
  const COST = 3500;

  const CHANNEL_INTERNET = 6014;
  const CHANNEL_MOBILE = 6017;

  const INQUIRY_REFERENCE_TYPE_DISBURSEMENT = 'disbursement';
  const INQUIRY_REFERENCE_TYPE_API_DISBURSEMENT = 'api_disbursement';
  const INQUIRY_REFERENCE_TYPE_API_DISBURSEMENT_INQUIRY = 'api_disbursement_inquiry';
  const INQUIRY_REFERENCE_TYPE_DISBURSEMENT_ARTAJASA_DISBURSEMENT = 'disbursement_artajasa_disbursement';
  const INQUIRY_REFERENCE_TYPE_BANK_ACCOUNT = 'bank_account';

  const ODEO_SENDER_PREFIX = 'ODEO';

  const RESPONSE_CODE_RTO = '_RTO';
  const RESPONSE_CODE_EXCEPTION = '_EXC';

  public static function isOperational() {

    $now = Carbon::now();

    if ($now->hour <= 20 && $now->hour >= 5) {

      if ($now->hour == 20 && $now->minute > 40) return false;

      return true;

    }

    return false;

  }

}