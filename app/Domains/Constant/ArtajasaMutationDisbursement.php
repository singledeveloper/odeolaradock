<?php

namespace Odeo\Domains\Constant;


class ArtajasaMutationDisbursement {

  const TYPE_REFUND = 'refund';
  const TYPE_TRANSFER = 'transfer';
  const TYPE_TOPUP = 'topup';

}