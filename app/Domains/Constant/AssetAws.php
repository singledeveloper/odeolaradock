<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 07-Aug-18
 * Time: 5:38 PM
 */

namespace Odeo\Domains\Constant;


class AssetAws {
  const AWS_LINK = 'https://s3-ap-southeast-1.amazonaws.com/odeo/';
  const BUSINESS_LOGO = 'business-logo/';
  const PPOB_LOGO = 'ppob-logo/';
  const PAYMENT = 'payment/';
  const BANK = 'bank/';

  const TOP_STORE_LOGO = 'top.png';
  const DEFAULT_STORE_LOGO = 'default.png';

  public static function getAssetPath($folderKey, $assetName) {
    return self::AWS_LINK . $folderKey . $assetName;
  }

}
