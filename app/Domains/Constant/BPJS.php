<?php

namespace Odeo\Domains\Constant;

class BPJS {

  const LOCKET_NAME = 'ODEO';
  const LOCKET_ADDRESS = 'Komplek Business Park Kebon Jeruk, Ruko AB-6 Lt. 6, Jalan Meruya Ilir No. 88, Kembangan';
  const LOCKET_CITY = 'Jakarta Barat';
  const LOCKET_POSTAL = '11620';
  const LOCKET_TELEPHONE = '(021) 2254-2690';
  const LOCKET_AREA_CODE = '31.73';

}