<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/10/17
 * Time: 11:58 PM
 */

namespace Odeo\Domains\Constant;


class Bank {

  const BCA = 1;
  const BNI = 2;
  const BRI = 3;
  const MANDIRI = 4;
  const CIMBNIAGA = 6;
  const PERMATA = 11;
  const MANDIRI_SYARIAH = 13;
  const MUAMALAT = 20;
  const GOPAY = 144;
  const ODEO = 149;

}
