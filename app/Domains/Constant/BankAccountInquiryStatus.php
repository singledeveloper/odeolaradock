<?php

namespace Odeo\Domains\Constant;

class BankAccountInquiryStatus {

  const COMPLETED = '50000';

  const FAILED = '90000';
  const FAILED_WRONG_ACCOUNT_NUMBER = '90001';
  const FAILED_CLOSED_BANK_ACCOUNT = '90003';
  const FAILED_BANK_REJECTION = '90004';
  const FAILED_VENDOR_DOWN = '90005';

  const INQUIRY_RESULT_EXPIRE_SEC = 7 * 24 * 60 * 60;

  public static function fromApiDisbursementStatus($status) {
    switch ($status) {
      case ApiDisbursement::COMPLETED:
        return BankAccountInquiryStatus::COMPLETED;

      case ApiDisbursement::FAILED_WRONG_ACCOUNT_NUMBER:
        return BankAccountInquiryStatus::FAILED_WRONG_ACCOUNT_NUMBER;

      case ApiDisbursement::FAILED_CLOSED_BANK_ACCOUNT:
        return BankAccountInquiryStatus::FAILED_CLOSED_BANK_ACCOUNT;

      case ApiDisbursement::FAILED_BANK_REJECTION:
        return BankAccountInquiryStatus::FAILED_BANK_REJECTION;

      case ApiDisbursement::FAILED_VENDOR_DOWN:
        return BankAccountInquiryStatus::FAILED_VENDOR_DOWN;

      default:
        return BankAccountInquiryStatus::FAILED;
    }
  }

  public static function toApiDisbursementStatus($status) {
    switch ($status) {
      case BankAccountInquiryStatus::COMPLETED:
        return ApiDisbursement::COMPLETED;

      case BankAccountInquiryStatus::FAILED_WRONG_ACCOUNT_NUMBER:
        return ApiDisbursement::FAILED_WRONG_ACCOUNT_NUMBER;

      case BankAccountInquiryStatus::FAILED_CLOSED_BANK_ACCOUNT:
        return ApiDisbursement::FAILED_CLOSED_BANK_ACCOUNT;

      case BankAccountInquiryStatus::FAILED_BANK_REJECTION:
        return ApiDisbursement::FAILED_BANK_REJECTION;

      case BankAccountInquiryStatus::FAILED_VENDOR_DOWN:
        return ApiDisbursement::FAILED_VENDOR_DOWN;

      case BankAccountInquiryStatus::FAILED:
        return ApiDisbursement::FAILED;
    }
  }

  public static function toDisbursementStatus($status) {
    switch ($status) {
      case BankAccountInquiryStatus::COMPLETED:
        return DisbursementStatus::COMPLETED;

      case BankAccountInquiryStatus::FAILED_WRONG_ACCOUNT_NUMBER:
        return DisbursementStatus::FAILED_WRONG_ACCOUNT_NUMBER;

      case BankAccountInquiryStatus::FAILED_CLOSED_BANK_ACCOUNT:
        return DisbursementStatus::FAILED_CLOSED_BANK_ACCOUNT;

      case BankAccountInquiryStatus::FAILED_BANK_REJECTION:
        return DisbursementStatus::FAILED_BANK_REJECTION;

      case BankAccountInquiryStatus::FAILED_VENDOR_DOWN:
        return DisbursementStatus::FAILED_VENDOR_DOWN;

      case BankAccountInquiryStatus::FAILED:
        return DisbursementStatus::FAILED;
    }
  }
}