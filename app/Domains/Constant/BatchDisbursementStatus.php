<?php

namespace Odeo\Domains\Constant;

class BatchDisbursementStatus {

  const PENDING = '10000';
  const ON_PROGRESS = '30000';
  const COMPLETED = '50000';
  const CANCELLED = '90000';

  const FILTERS = [self::PENDING, self::ON_PROGRESS, self::COMPLETED, self::CANCELLED];

  const STATUS_CODE_TO_MESSAGE = [
    self::PENDING => 'api_pending',
    self::ON_PROGRESS => 'api_on_progress',
    self::COMPLETED => 'api_completed',
    self::CANCELLED => 'api_cancelled',
  ];

  public static function toStatusMessage($status) {
    if (isset(self::STATUS_CODE_TO_MESSAGE[$status])) {
      return trans('status.' . self::STATUS_CODE_TO_MESSAGE[$status]);
    }

    return trans('status.api_unknown') . ': ' . $status;
  }
}
