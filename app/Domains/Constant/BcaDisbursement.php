<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/28/17
 * Time: 9:14 PM
 */

namespace Odeo\Domains\Constant;

use Carbon\Carbon;

class BcaDisbursement {

  const PENDING = '10000';
  const TIMEOUT = '30000';
  const SUCCESS = '50000';
  const FAIL = '90000';

  const ERROR_CODE_INQUIRY_FAILED = 'INQUIRY_FAILED';

  const TRANSFER_API_LIMIT = 1000;
  const TRANSFER_BASE_COST = 100;
  const TRANSFER_EXCEEDED_COST = 100;

  public static function isOperational($hourStart = 5, $hourEnd = 20, $minuteEnd = 59) {
    $now = Carbon::now();
    if ($now->hour <= $hourEnd && $now->hour >= $hourStart) {
      return $now->hour != $hourEnd || $now->minute < $minuteEnd;
    }
    return false;
  }
}
