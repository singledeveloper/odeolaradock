<?php

namespace Odeo\Domains\Constant;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Libraries\ISO8583\JAK8583;

class BillerArtajasa {

  const INDOSAT_SERVER = 'https://202.152.16.110:11002/';
  const INDOSAT_JSON_SERVER = 'https://202.152.16.107:30711/Odeo/';
  const INDOSAT_DATA_BILLER_ID = '45016';

  const TELKOMSEL_SERVER = '???';
  const ISO_PURCHASE_PROCESSING_CODE = '??????';
  const ISO_MERCHANT_TYPE = '6012';
  const ISO_CURRENCY_CODE = '360';
  const ISO_PURCHASE_MTI = '0200';
  const ISO_REVERSAL_MTI = '0400';

  const DEV_USER_ID = 'odeoteknologi';
  const DEV_PASSWORD = 'Passw0rd02';
  const PROD_USER_ID = 'odeo';
  const PROD_PASSWORD = 'Artajasa2017!';

  const REPLENISHMENT_UNIQUE_CODE = 2001;

  public static function getUserID() {
    return app()->environment() == 'production' ? self::PROD_USER_ID : self::DEV_USER_ID;
  }

  public static function getPassword() {
    return app()->environment() == 'production' ? self::PROD_PASSWORD : self::DEV_PASSWORD;
  }

  public static function encodeISO($mti, $data) {
    $jak = new JAK8583();

    $jak->addMTI($mti);
    foreach ($data as $key => $item) {
      $jak->addData($key, $item);
    }

    return $jak->getISO();
  }

  public static function decodeISO($string) {
    $jak = new JAK8583();

    $jak->addISO($string);
    return $jak->getData();
  }

  public static function formatISORequest($iso) {
    $header = pack("n*", (strlen($iso) + 1));
    $trailer = pack("H*", '03');

    return $header . $iso . $trailer;
  }

  public static function checkWrongNumberPattern($string) {
    return checkPatterns(strtolower($string), [
      'nomor tidak terdaftar'
    ]);
  }

  public static function generateSTAN() {
    $redis = Redis::connection();
    $date = Carbon::now()->toDateString();
    $lastDate = $redis->hget('odeo_core:artajasa_biller', 'date');
    $stanId = $redis->hincrby('odeo_core:artajasa_biller', 'stan_id', 1);
    if ($lastDate != $date || $stanId > 999999) {
      $stanId = 1;
      $redis->hmset('odeo_core:artajasa_biller', [
        'stan_id' => 1,
        'date' => $date
      ]);
    }
    return sprintf("%06d", $stanId);
  }
}
