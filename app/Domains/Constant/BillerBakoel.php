<?php

namespace Odeo\Domains\Constant;

use nusoap_client;

class BillerBakoel {

  const PULSA_SERVER = 'http://117.102.64.238:1212/pulsaPrabayar.php?wsdl';
  const PP_SERVER = 'http://117.102.64.238:1212/pp/index.php?wsdl';
  const BPJS_TK_SERVER = 'http://117.20.55.221:2012/index.php?wsdl';

  const PROD_KEY = 'b43f63c847744d3e9bc13603de4b2a4bdceb5a5e';
  const DEV_KEY = 'bd8446eaa574ab00ab72249f8b06a759';
  const PROD_SECRET = 'a7b3d809c755f4e5073c2e23d23479ece1cf413d';
  const DEV_SECRET = 'a3355920444fae8eb84b1f6304bbe1d9';
  const IP_WHITELIST = ['117.102.64.237'];

  const PLN_PREPAID = 'PLNPREPAIDB';
  const PLN_POSTPAID = 'PLNPOSTPAIDB';
  const BPJS_KES = 'BPJSKS';
  const PGN = 'PGN';
  const TELKOM_PSTN = 'TELKOMPSTN';
  const TELKOM_SPEEDY = 'TELKOMSPEEDY';
  const TELKOM_FLEXI = 'TELKOMFLEXI';
  const TELKOMSEL_HALO = 'TELKOMSELLHALO';
  const CMD_PRODUCT_LIST = 'productList';
  const CMD_PP_INQUIRY = 'ppInquiry';
  const CMD_PP_PAYMENT = 'ppPayment';
  const CMD_PAYMENT = 'payment';
  const CMD_PP_OPTION = 'ppOptions';
  const CMD_MITRA_INFO = 'mitraInfo';
  const CMD_CHECK_STATUS = 'checkStatus';
  const CMD_BPJS_TK_INQUIRY = 'inquiryKodeIuranByNIK';
  const CMD_BPJS_TK_IURAN_INQUIRY = 'inquiryKodeIuran';
  const CMD_BPJS_TK_PAYMENT = 'bayarIuran';

  const GENERAL_ERROR_MESSAGE = 'Sedang dalam gangguan. Mohon menghubungi Customer Service untuk keterangan lebih lanjut';

  public static function getKey() {
    return app()->environment() == "production" ? self::PROD_KEY : self::DEV_KEY;
  }

  public static function getSecret() {
    return app()->environment() == "production" ? self::PROD_SECRET : self::DEV_SECRET;
  }

  public static function setClient($server, $resTimeout = 360, $timeout = 60) {
    $client = new nusoap_client($server, true, false, false, false, false, $timeout, $resTimeout, '');
    $client->setCredentials(
      $server == self::BPJS_TK_SERVER ? self::PROD_KEY : self::getKey(),
      $server == self::BPJS_TK_SERVER ? self::PROD_SECRET : self::getSecret(),
      'basic');

    return $client;
  }

  public static function getProductList($code = '') {
    return self::setClient(self::PULSA_SERVER)->call(self::CMD_PRODUCT_LIST, [
      'product_id' => $code
    ]);
  }

  public static function getPlnPrepaidInquiry($number, $denom = '') {
    $inquiry = self::setClient(self::PP_SERVER, 10, 10)->call(self::CMD_PP_INQUIRY, [
      'productCode' => self::PLN_PREPAID,
      'idPel' => $number,
      'idPel2' => $denom != '' ? (str_replace('U', '', $denom) . '000') : '',
      'miscData' => strpos($denom, 'U') !== false ? '1' : ''
    ]);
    if (isset($inquiry['responseCode']) && $inquiry['responseCode'] == '00')
      return $inquiry;
    return [];
  }

  public static function checkWrongNumberPattern($string) {
    return checkPatterns($string, ['invalid msisdn']);
  }

  public static function translatePostpaidResponse($code, $billerMessage = '') {
    $messages = [
      0 => 'SUCCESS',
      4 => 'Gangguan', //Unregistered Biller
      5 => 'Gangguan', //Other
      6 => 'Gangguan', //Blocked Partner Central
      7 => 'Gangguan', //Blocked Terminal
      8 => 'Tidak dapat melakukan pembayaran saat ini. Mohon dicoba beberapa saat lagi.', //Invalid access-time
      9 => 'Nomor Pelanggan tidak valid', //MSN-ID/Subscriber ID not allowed
      11 => 'Gangguan', //Need to sign-on
      12 => 'Gangguan', //Cannot reverse because exceed time limit
      13 => 'Gangguan', //Invalid Transaction Amount
      14 => 'Nomor Pelanggan tidak valid.', //Unknown subscriber
      15 => 'Gangguan',
      16 => 'Nomor Pelanggan tidak valid.', //PRR Subscriber
      17 => 'Nomor Pelanggan tidak dapat melakukan pembayaran.', //Subscriber has outstanding bills
      26 => 'Gangguan', //Insufficient quota
      30 => 'Gangguan', //Invalid message
      31 => 'Gangguan', //Unregistered Bank Code
      32 => 'Gangguan', //Unregistered Partner Central
      33 => 'Gangguan', //Unregistered Product
      34 => 'Gangguan', //Unregistered terminal
      45 => 'Gangguan', //Invalid admin charges
      48 => 'Gangguan', //Registration is expired
      55 => 'Data tidak ditemukan',
      63 => 'Gangguan', //Unable to reverse because of no payment
      68 => 'Pembayaran suspect. Akan di-cek dengan rekon H+1.', //Timeout
      77 => 'Nomor Pelanggan di-suspend.',
      88 => 'Nomor Pelanggan telah melakukan pembayaran.', //Bills already paid
      89 => 'Tagihan bulan ini belum tersedia.', //Current bill is not available
      90 => 'Tidak dapat melakukan pembayaran saat. Mohon dicoba beberapa saat lagi.', //Cut-off is in progress
      91 => 'Gangguan', //Database
      92 => 'Gangguan', //Switcher receipt reference number is not available
      93 => 'Gangguan', //Invalid switcher reference number
      94 => 'Gangguan', //Reversal had been done
      95 => 'Gangguan', //Unregistered Merchant type
      96 => 'Gangguan', //Transaction was not found on Legacy System
      97 => 'Gangguan', //Switching ID and/or bank code is not identical with inquiry
      98 => 'Gangguan', //Reference number is not valid
      101 => 'Nomor Pelanggan telah melakukan pembayaran.'
    ];

    $responseCode = intval($code);

    if (isset($messages[$responseCode])) {
      if ($responseCode == 0) $status = SwitcherConfig::BILLER_SUCCESS;
      else if ($messages[$responseCode] == 'Error' || $responseCode == 68) $status = SwitcherConfig::BILLER_SUSPECT;
      else $status = SwitcherConfig::BILLER_FAIL;

      if ($messages[$responseCode] == 'Gangguan') {
        $messages[$responseCode] = 'Sedang dalam gangguan. Mohon menghubungi Customer Service untuk keterangan lebih lanjut. Error Code: ' . $responseCode;
      }

      return [$status, $messages[$responseCode]];
    }

    return [SwitcherConfig::BILLER_FAIL, 'Gagal. ' . ($billerMessage != '' ? trim($billerMessage) : 'Mohon menghubungi Customer Service untuk keterangan lebih lanjut') . '. Error Code: ' . $responseCode];
  }

}