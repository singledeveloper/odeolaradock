<?php

namespace Odeo\Domains\Constant;

class BillerBlackhawk {

  const SIGNATURE = 'BHNUMS';
  const PCC = '01';
  const PROD_PAN = '6039534201000000024';
  const DEV_PAN = '9877890000000000';
  const PROCESSING_CODE = '745400';
  const VOID_PROCESSING_CODE = '775400';
  const MCC = '5411';
  const POSEM = '041';
  const MID = '60300005061';
  const PROD_TERMINAL_ID = '00001     001   ';
  const DEV_TERMINAL_ID = '00000     001   ';
  const CURRENCY_CODE = '360';
  const VERSION = '43';
  const PROD_UPC = '505164402497';
  const DEV_UPC = '505164400264';
  const MERCHANT_LOC = 'ODEO KEMBANGAN JAKARTA INDONESIA';

  const PROD_URL = 'https://blast.blackhawk-net.com:8443/transactionManagement/v2/transaction';
  const PROD_URL2 = 'https://webpos.blackhawk-net.com:8443/transactionManagement/v2/transaction';
  const DEV_URL = 'https://blast.preprod.blackhawk-net.com:8443/transactionManagement/v2/transaction';

  const STATUS_SUCCESS = ['00', '01', '03'];
  const STATUS_REVERSAL = ['15', '74'];

  const TRX_TYPE_NORMAL = 'normal';
  const TRX_TYPE_VOID = 'void';

  public static function getTerminalId() {
    return app()->environment() == 'production' ? self::PROD_TERMINAL_ID : self::DEV_TERMINAL_ID;
  }

  public static function getPAN() {
    return app()->environment() == 'production' ? self::PROD_PAN : env('GOOGLE_PLAY_PAN', self::DEV_PAN);
  }

  public static function getUPC() {
    return app()->environment() == 'production' ? self::PROD_UPC : env('GOOGLE_PLAY_UPC', self::DEV_UPC);
  }

  public static function getUrl() {
    return app()->environment() == 'production' ? self::PROD_URL2 : self::DEV_URL;
  }

}