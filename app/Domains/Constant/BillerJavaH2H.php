<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/15/17
 * Time: 1:20 PM
 */

namespace Odeo\Domains\Constant;


class BillerJavaH2H {

  const USER_ID = 'H0101';
  const PROD_KEY = '2d83c7ccf8faa4241023790dd615247a';
  const PROD_SECRET = 'b267b60c782988d99179eb87051a9912725671e18472f249718db6dba3788a97';

  const PATH = 'https://javah2h.com/api/connect/';
  const WEB_PRICELIST_PULSA = 'https://javah2h.com/h2h-pulsa-murah-all-operator/';

  const INQUIRY_DEPOSIT = 'D';
  const INQUIRY_LAST_BALANCE = 'S';
  const INQUIRY_PRICELIST = 'HARGA';
  const INQUIRY_TRANSACTION_STATUS = 'STATUS';
  const INQUIRY_PULSA = 'I';
  const INQUIRY_PLN = 'PLN';

  const BCA = 'bca';
  const BRI = 'bri';
  const MANDIRI = 'mandiri';

  const CODE_PLN = 'PLN';
  const CODE_PULSA = 'PULSA';
  const CODE_GAME = 'GAME';

  public static function getProductList() {
    $client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ],
      'timeout' => 90
    ]);

    $query = [
      'inquiry' => self::INQUIRY_PRICELIST,
      'code' => self::CODE_PLN
    ];

    $response = $client->request('POST', self::PATH, [
      "form_params" => $query,
      "headers" => [
        "h2h-userid" => self::USER_ID,
        "h2h-key" => self::PROD_KEY,
        "h2h-secret" => self::PROD_SECRET
      ]
    ]);

    if($response->getStatusCode() != 200) return null;

    return json_decode($response->getBody()->getContents())->message;

  }

}