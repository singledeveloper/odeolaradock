<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/10/17
 * Time: 3:21 PM
 */

namespace Odeo\Domains\Constant;


class BillerMobilePulsa {
  const USERNAME = '0895351232125';
  const WEB_REPORT_PASSWORD = 'billerke27';
  const API_KEY_DEV = '712598982047424d';
  const API_KEY_PROD = '8125993fd229c877812';
  const DEV_URL = 'https://testprepaid.mobilepulsa.net/v1/legacy/index';
  const DEV_URL_CHECK = 'https://testpostpaid.mobilepulsa.net/api/v1/bill/check';
  const PROD_URL = 'https://api.mobilepulsa.net/v1/legacy/index';
  const PROD_URL_CHECK = 'https://mobilepulsa.net/api/v1/bill/check';
  const LOGIN_URL = 'https://mobilepulsa.com/login';
  const TOPUP_FORM_URL = 'https://mobilepulsa.com/webapp/isi_deposit';
  const TOPUP_POST_URL = 'https://mobilepulsa.com/action/isi/deposit';

  const GENERAL_ERROR_MESSAGE = 'Sedang dalam gangguan. Mohon menghubungi Customer Service untuk keterangan lebih lanjut';

  const COMMAND_TOPUP = 'topup';
  const COMMAND_POSTPAID_PRICELIST = 'pricelist-pasca';
  const COMMAND_POSTPAID_INQUIRY = 'inq-pasca';
  const COMMAND_POSTPAID_PAY = 'pay-pasca';
  const COMMAND_CHECK_STATUS = 'checkstatus';

  const PREPAID_TRANSACTION_STATUS_PROCESS = '0';
  const PREPAID_TRANSACTION_STATUS_SUCCESS = '1';
  const PREPAID_TRANSACTION_STATUS_FAIL = '2';

  const PREPAID_RC_STATUS_SUCCESS = '00';
  const PREPAID_RC_STATUS_PENDING = '39';
  const PREPAID_RC_STATUS_FAIL = [
    '06' => 'TRANSACTION NOT FOUND',
    '07' => 'FAILED',
    '14' => 'INCORRECT DESTINATION NUMBER',
    '16' => 'NUMBER NOT MATCH WITH OPERATOR',
    '17' => 'INSUFFICIENT DEPOSIT',
    '20' => 'CODE NOT FOUND',
    '106' => 'PRODUCT IS TEMPORARILY OUT OF SERVICE',
    '107' => 'ERROR IN XML FORMAT',
    '202' => 'MAXIMUM 1 NUMBER 1 TIME IN 1 DAY',
    '203' => 'NUMBER IS TOO LONG',
    '204' => 'WRONG AUTHENTICATION',
    '205' => 'WRONG COMMAND'
  ];

  const POSTPAID_STATUS_SUCCESS = [
    '00' => 'PAYMENT / INQUIRY SUCCESS',
    '01' => 'INVOICE HAS BEEN PAID',
    '34' => 'BILL HAS BEEN PAID'
  ];

  const POSTPAID_STATUS_PENDING = [
    '05' => 'PENDING',
    '39' => 'PENDING',
    '201' => 'PENDING'
  ];

  const POSTPAID_ERROR_MAPPINGS = [
    '00' => "Success",
    '01' => "Nomor Pelanggan telah melakukan pembayaran.",
    '02' => "Bill unpaid",
    '03' => "Invalid ref id",
    '04' => "Billing id expired",
    '05' => "Undefined error",
    '06' => "Inquiry id not found",
    '07' => "Transaction failed",
    '08' => "Billing id blocked",
    '09' => "Inquiry blocked",
    '10' => "Bill is not avalable",
    '11' => "Duplicate ref id",
    '13' => "Customer number blocked",
    '14' => "Nomor Pelanggan tidak valid",
    '15' => "Nomor Pelanggan belum didukung.",
    '16' => "Number doesn't match the operator",
    '17' => "Sedang dalam gangguan. Mohon menghubungi Customer Service untuk keterangan lebih lanjut. Error Code: 17",
    '20' => "Product unregistered",
    '30' => "Payment has to be done via counter",
    '31' => "Transaction rejected due to exceeding max total bill allowed",
    '32' => "Transaction failed, please pay bill of all period",
    '33' => "Transaction can't be processed, please try again later",
    '34' => "Bill has been paid",
    '35' => "Transaction rejected", //due to another unpaid arrear??
    '36' => "Exceeding due date, please pay in the counter",
    '37' => "Payment failed",
    '38' => "Payment failed, please try again",
    '39' => "Transaction in process",
    '40' => "Transaction rejected", // DUE TO ALL OR ONE OF THE ARREAR/INVOICE HAS BEEN PAID
    '41' => "Please pay to the corresponding company", //CAN'T BE PAID IN COUNTER, PLEASE PAY TO THE CORRESPONDING COMPANY
    '42' => "Payment request haven't been received", //PAYMENT REQUEST HAVEN'T BEEN RECEIEVED
    '91' => "Connection problem", // DATABASE CONNECTION ERROR
    '100' => "Invalid signature",
    '101' => "Invalid command",
    '102' => "Invalid IP Address",
    '103' => "Timeout",
    '105' => "Sedang dalam gangguan. Mohon menghubungi Customer Service untuk keterangan lebih lanjut. Error Code: 105", // MISC ERROR / BILLER SYSTEM ERROR
    '106' => "Produk sedang gangguan",
    '107' => "Internal error", // XML FORMAT ERROR
    '108' => "SORRY, YOUR ID CAN'T BE USED FOR THIS PRODUCT TRANSACTION",
    '109' => "System Cut off",
    '110' => "System under maintenance",
    '201' => "Sedang dalam gangguan. Mohon menghubungi Customer Service untuk keterangan lebih lanjut. Error Code: 201" // UNDEFINED RESPONSE CODE
  ];


  public static function getKey() {
    return app()->environment() == 'production' ? self::API_KEY_PROD : self::API_KEY_DEV;
  }

  public static function getPostpaidUrl() {
    return app()->environment() == 'production' ? self::PROD_URL_CHECK : self::DEV_URL_CHECK;
  }

  public static function getPrepaidUrl() {
    return app()->environment() == 'production' ? self::PROD_URL : self::DEV_URL;
  }

}