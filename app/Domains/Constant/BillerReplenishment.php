<?php

namespace Odeo\Domains\Constant;

class BillerReplenishment {

  const PENDING = 10000;
  const PENDING_PROGRESS_AUTO_TRANSFER = 10001;
  const ON_PROGRESS_AUTO_TRANSFER = 30001;
  const WAIT_FOR_NOTIFY = 30002;
  const WAIT_FOR_MANUAL_VERIFY = 30003;
  const WAIT_FOR_BILLER_UPDATE = 30004;
  const FAILED = 40000;
  const COMPLETED = 50000;
  const CANCELLED = 90000;

  const DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT = 'bca_api_disbursement';
  const DISBURSEMENT_VENDOR_BCA_MANUAL = 'bca_manual_disbursement';
  const DISBURSEMENT_VENDOR_MANDIRI_MANUAL = 'mandiri_manual_disbursement';
  const DISBURSEMENT_VENDOR_BRI_API_DISBURSEMENT = 'bri_api_disbursement';
  const DISBURSEMENT_VENDOR_BRI_MANUAL = 'bri_manual_disbursement';
  const DISBURSEMENT_VENDOR_BNI_MANUAL = 'bni_manual_disbursement';

  const REDIS_NS = 'odeo_core:auto_breplenishment_lock';
  const REDIS_KEY_BILLER = 'breplenishment_biller_id_';
  const REDIS_KEY_ID = 'breplenishment_id_';

  const DEFAULT_MINIMAL_DEPOSIT = 2000000;
  const DEFAULT_MAXIMAL_DEPOSIT = 20000000;
  const SAME_DAY_MULTIPLIER = 10000;

  const SETTING_CHANNEL_TELEGRAM = 'telegram';
  const SETTING_CHANNEL_TELEGRAM_CHAT_ID = 'chat_id';
  const SETTING_CHANNEL_TELEGRAM_MESSAGE = 'telegram_message';
  const SETTING_CHANNEL_EMAIL = 'email';
  const SETTING_CHANNEL_EMAIL_TO = 'email_to';
  const SETTING_CHANNEL_EMAIL_BODY = 'email_body';
  const SETTING_CHANNEL_CONSTANT_VALUE = 'constant_value';
  const SETTING_CHANNEL_JABBER = 'jabber';
  const SETTING_CHANNEL_JABBER_CENTER = 'center_account';
  const SETTING_CHANNEL_JABBER_REQUEST_SAMPLE = 'request_sample';
  const SETTING_CHANNEL_JABBER_REPLY_SAMPLE = 'reply_sample';
  const SETTING_ROUTE_BEFORE = 'beforesend';
  const SETTING_ROUTE_AFTER = 'oncomplete';

  static function getAutoDisbursement() {
    return [
      self::DISBURSEMENT_VENDOR_BRI_API_DISBURSEMENT,
      self::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT
    ];
  }

  static function isAutoDisbursement($value) {
    return in_array($value, self::getAutoDisbursement());
  }

  static function getRequiredChannelParameters($channelType) {
    switch($channelType) {
      case self::SETTING_CHANNEL_TELEGRAM:
        return [self::SETTING_CHANNEL_TELEGRAM_CHAT_ID, self::SETTING_CHANNEL_TELEGRAM_MESSAGE];
      case self::SETTING_CHANNEL_EMAIL:
        return [self::SETTING_CHANNEL_EMAIL_TO, self::SETTING_CHANNEL_EMAIL_BODY];
      case self::SETTING_CHANNEL_CONSTANT_VALUE:
        return [self::SETTING_CHANNEL_CONSTANT_VALUE];
      case self::SETTING_CHANNEL_JABBER:
        return [self::SETTING_CHANNEL_JABBER_CENTER, self::SETTING_CHANNEL_JABBER_REQUEST_SAMPLE, self::SETTING_CHANNEL_JABBER_REPLY_SAMPLE];
    }
    return false;
  }

}
