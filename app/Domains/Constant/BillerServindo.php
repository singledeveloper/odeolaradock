<?php

namespace Odeo\Domains\Constant;

class BillerServindo {

  const PROD_SERVER = 'http://202.149.67.46:2013/';
  const DEV_SERVER = 'http://182.23.64.65:1910/';
  const USER_ID = 'ca';
  const PASSWORD = '1305';
  const IP_WHITELIST = ['182.23.64.65', '202.149.67.46'];
  const REPLENISHMENT_UNIQUE_CODE = 315;

  public static function getServer() {
    return app()->environment() == "production" ? self::PROD_SERVER : self::DEV_SERVER;
  }

  public static function checkWrongNumberPattern($string) {
    return checkPatterns($string, ['RC:14.', 'RC: 14.']);
  }

  public static function hardFixRes($string) {
    $string = str_replace('"message":"{"', '"message":{"', $string);
    $string = str_replace('"}","', '"},"', $string);

    return $string;
  }
}