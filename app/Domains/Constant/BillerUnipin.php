<?php

namespace Odeo\Domains\Constant;

class BillerUnipin {
  
  const PROD_SERVER = 'https://api.unipin.com/';

  const DEV_SERVER = 'https://dev-api.unipin.com/';
  const DEV_SERVER2 = 'https://dev.unipin.com/';
  const DEV_GUID = 'a00b72b2-c9d7-4f33-a8a2-e08c7f93ccfa';
  const DEV_SECRET = 'ulxmohkzjtnktqtc';
  const DEV_GUID2 = '9b42a14d-a986-40a9-b4cc-354be6aea6db';
  const DEV_SECRET2 = 'w56kbwxuxh3heka3';

  const TYPE_VOUCHER = 'voucher';
  const TYPE_TOPUP = 'topup';

  public static function getGUID($type) {
    if (app()->environment() == 'production')
      return env('UNIPIN_GUID');
    return $type == self::TYPE_VOUCHER ? self::DEV_GUID : self::DEV_GUID2;
  }

  public static function getSecret($type) {
    if (app()->environment() == 'production')
      return env('UNIPIN_SECRET');
    return $type == self::TYPE_VOUCHER ? self::DEV_SECRET : self::DEV_SECRET2;
  }

  public static function getURL($url) {
    return (app()->environment() == 'production' ? self::PROD_SERVER : self::DEV_SERVER2) .
      ltrim($url, '/');
  }

  public static function getVoucherURL($url) {
    return (app()->environment() == 'production' ? self::PROD_SERVER : self::DEV_SERVER) .
      ltrim($url, '/');
  }

  public static function getTopupHeader($url) {
    $partnerId = BillerUnipin::getGUID(BillerUnipin::TYPE_TOPUP);
    $logId = time() . $url;

    return [
      'partnerid' => $partnerId,
      'logid' => $logId,
      'auth' => md5($partnerId . $logId . BillerUnipin::getSecret(BillerUnipin::TYPE_TOPUP))
    ];
  }
}