<?php

namespace Odeo\Domains\Constant;

class BniDisbursement {

  const PENDING = '10000';
  const SUCCESS = '50000';
  const FAIL = '90000';

  const RESPONSE_CODE_INQUIRY_FAILED = 'INQUIRY_FAILED';

  const COST = 0;

}