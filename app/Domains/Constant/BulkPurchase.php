<?php

namespace Odeo\Domains\Constant;

class BulkPurchase {

  const PENDING = 10000;
  const REJECTED = 40000;
  const APPROVED = 50000;
  const REMOVED = 90000;

  const REDIS_TEMP_BULK = 'odeo-core:temp-bulk';
  const REDIS_TEMP_BULK_KEY_LOCK = 'lock';
  const REDIS_TEMP_BULK_KEY_LAST_ROW = 'last_row';

}