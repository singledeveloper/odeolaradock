<?php

namespace Odeo\Domains\Constant;

class CashWebhookType
{

  const USER_TOPUP = 'user_topups';
  const USER_WITHDRAW = 'user_withdraws';

}
