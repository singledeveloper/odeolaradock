<?php

namespace Odeo\Domains\Constant;

class ChannelStatus
{
  const PENDING = 10000;
  const ACTIVE = 50000;
  const SUSPENDED = 90000;
}
