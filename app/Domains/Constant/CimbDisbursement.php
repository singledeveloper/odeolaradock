<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/25/17
 * Time: 7:45 PM
 */

namespace Odeo\Domains\Constant;


class CimbDisbursement {

  const STATUS_PENDING = '10000';
  const STATUS_FAIL = '90000';
  const STATUS_SUCCESS = '50000';
  const STATUS_TIMEOUT = '40001';

  const STATUS_CODE_INQUIRY_FAILED = 'INQUIRY_FAILED';

  const COST = 0;
}