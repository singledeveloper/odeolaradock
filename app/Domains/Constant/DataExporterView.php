<?php

namespace Odeo\Domains\Constant;

class DataExporterView {

  const PRICE_LIST_KEY = 'price-list';
  const PRICE_LIST_VIEW = 'price-list';

  const API_DISBURSEMENT_INVOICE_KEY = 'api-disbursement-invoice';
  const API_DISBURSEMENT_INVOICE_VIEW = 'api-disbursement-invoice';

  const PAYMENT_GATEWAY_INVOICE_KEY = 'payment-gateway-invoice';
  const PAYMENT_GATEWAY_INVOICE_VIEW = 'payment-gateway-invoice';

  const SUPPLY_TRANSACTION_LIST_KEY = 'supply-transaction-list';
  const SUPPLY_TRANSACTION_LIST_VIEW = 'supply-transaction-list';

  const SUPPLY_GROUP_PRICE_LIST_KEY = 'supply-group-price-list';
  const SUPPLY_GROUP_PRICE_LIST_VIEW = 'supply-group-price-list';

}
