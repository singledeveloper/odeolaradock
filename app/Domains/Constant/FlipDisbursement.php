<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/3/17
 * Time: 4:12 PM
 */

namespace Odeo\Domains\Constant;


use Carbon\Carbon;

class FlipDisbursement {

  const DISBURSABLE_BANK = [
    1 => 'bca',
    2 => 'bni',
    3 => 'bri',
    4 => 'mandiri',
    6 => 'cimb',
    13 => 'bsm',
    20 => 'muamalat'
  ];

  const PENDING = '10000';
  const WAIT_FOR_NOTIFY = '20001';
  const SUCCESS = '50000';
  const FAIL = '90000';

  const FEE_LOCAL = 800;
  const FEE_DOMESTIC = 2500;

  public static function getFlipDisbursementBankName($bankId) {
    return self::DISBURSABLE_BANK[$bankId] ?? null;
  }

  public static function isOperational() {

    $now = Carbon::now();

    if ($now->dayOfWeek == 7) return false;

    if ($now->dayOfWeek == 6) {

      if ($now->hour >= 9 && $now->hour < 14) {
        if (($now->hour == 9 || $now->hour == 13) && $now->minute > 40) return false;
        return true;
      }

    } else {

      if ($now->hour >= 9 && $now->hour < 19) {
        if (($now->hour == 9 || $now->hour == 18) && $now->minute > 40) return false;
        return true;
      }

    }

    return false;

  }

}