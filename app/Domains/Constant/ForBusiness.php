<?php

namespace Odeo\Domains\Constant;


use Odeo\Domains\Transaction\Helper\Currency;

class ForBusiness {

  const FEE_PERCENTAGE = 'percentage';
  const FEE_BASE_VALUE = 'base_value';
  const DISCOUNT_VALUE = 'discount_value';

  const COMPARISON_QTY = 'qty';
  const COMPARISON_FEE = 'total_fee';
  const COMPARISON_TIER = 'tier';

  const TRANSACTION_DISBURSEMENT = 'disbursement';
  const TRANSACTION_DISBURSEMENT_BATCH = 'disbursement_batch';
  const TRANSACTION_PAYMENT_GATEWAY = 'payment_gateway';
  const TRANSACTION_DISBURSEMENT_INQUIRY = 'disbursement_inquiry';
  const TRANSACTION_WITHDRAW = 'withdraw';

  const REDIS_BUSINESS_RESELLER = 'odeo-core:business-reseller';
  const REDIS_LOCK_TRANSFER_FROM_MARKETING = 'marketing_transfer_lock';
  const REDIS_LOCK_TRANSFER_BONUS = 'bonus_transfer';
  const REDIS_LOCK_TRANSFER_CAPITAL = 'capital_transfer';

  const STARWIN_RESELLER_ID = 186520;

  public static function getFeeTypeList() {
    return [
      self::FEE_PERCENTAGE,
      self::FEE_BASE_VALUE,
      self::DISCOUNT_VALUE
    ];
  }

  public static function getConditionalTypeList() {
    return [
      self::COMPARISON_QTY,
      self::COMPARISON_FEE,
      self::COMPARISON_TIER
    ];
  }

  public static function getTransactionTypeList() {
    return [
      self::TRANSACTION_DISBURSEMENT,
      self::TRANSACTION_DISBURSEMENT_INQUIRY,
      self::TRANSACTION_PAYMENT_GATEWAY,
      self::TRANSACTION_WITHDRAW
    ];
  }

  public static function createMessageShort($item) {
    $currency = app()->make(Currency::class);
    if ($item->fee_type == ForBusiness::FEE_PERCENTAGE) {
      $type = $item->fee_value . '%';
    }
    else if ($item->fee_type == ForBusiness::FEE_BASE_VALUE) {
      $type = 'Base ' . $currency->formatPrice($item->fee_value)['formatted_amount'];
    }
    else if ($item->fee_type == ForBusiness::DISCOUNT_VALUE) {
      $type = 'Discount ' . $currency->formatPrice($item->fee_value)['formatted_amount'];
    }
    else $type = '';

    return $type;
  }

  public static function createMessageFromDetailData($detail) {
    $currency = app()->make(Currency::class);
    $message = '';
    if ($detail->fee_type == self::FEE_BASE_VALUE) {
      $message .= 'Modal ' . $currency->formatPrice($detail->fee_value)['formatted_amount'] . ' ';
    }
    else if ($detail->fee_type == self::DISCOUNT_VALUE) {
      $message .= 'Diskon ' . $currency->formatPrice($detail->fee_value)['formatted_amount'] . ' ';
    }
    else if ($detail->fee_type == self::FEE_PERCENTAGE) {
      $message .= $detail->fee_value . '% ';
    }

    if ($detail->minimal_type == null) $message .= 'untuk semua transaksi';
    else {
      $message .= 'dengan minimal ';
      if ($detail->minimal_type == self::COMPARISON_QTY) {
        $message .= $detail->minimal_value . ' transaksi';
      }
      else if ($detail->minimal_type == self::COMPARISON_FEE) {
        $message .= 'total fee ' . $currency->formatPrice($detail->minimal_value)['formatted_amount'];
      }
      else if ($detail->minimal_type == self::COMPARISON_TIER) {
        $message .= 'modal(tier) ' . $currency->formatPrice($detail->minimal_value)['formatted_amount'];
      }
    }

    if ($detail->campaign_start_date || $detail->campaign_end_date) {
      $message .= ' tertanggal';
      if ($detail->campaign_start_date) {
        $message .= ' dari ' . $detail->campaign_start_date;
      }
      if ($detail->campaign_end_date) {
        $message .= ' sampai ' . $detail->campaign_end_date;
      }
    }

    return $message;
  }

  public static function createDetail($detail, $item) {
    if (!$detail) $status = 'NO UPLOAD YET';
    else {
      if ($detail->kyc_status == '50000') $status = 'APPROVED';
      else if ($detail->kyc_status == '40000') $status = 'REJECTED';
      else if ($detail->kyc_status == '10001') $status = 'NOT COMPLETE';
      else $status = 'PENDING';
    }

    return [
      'id' => $detail ? $detail->id : 0,
      'option_id' => $item->id,
      'business_type' => $item->business_type,
      'business_name' => trans('business_kyc.business_' . $item->business_type . '_name'),
      'name' => $item->name,
      'tab_name' => trans('business_kyc.tab_' . $item->name),
      'title_name' => trans('business_kyc.title_' . $item->name),
      'has_string' => $item->has_string,
      'placeholder_string' => $item->has_string ? trans('business_kyc.placeholder_' . $item->name . '_name') : '',
      'has_number' => $item->has_number,
      'placeholder_number' => $item->has_number ? trans('business_kyc.placeholder_' . $item->name . '_number') : '',
      'has_date' => $item->has_date,
      'placeholder_date' => $item->has_date ? trans('business_kyc.placeholder_' . $item->name . '_date') : '',
      'has_file' => $item->has_file,
      'string_value' => $detail && isset($detail->string_value) ? $detail->string_value : '',
      'number_value' => $detail && isset($detail->number_value) ? $detail->number_value : '',
      'date_value' => $detail && isset($detail->date_value) ? $detail->date_value : '',
      'file_url' => $detail && isset($detail->file_url) ? AwsConfig::url($detail->file_url) : '',
      'kyc_status' => $detail && isset($detail->kyc_status) ? $detail->kyc_status : '',
      'kyc_status_message' => $status,
      'admin_note' => $detail && isset($detail->admin_note) ? $detail->admin_note : ''
    ];
  }

}
