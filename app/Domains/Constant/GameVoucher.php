<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/18/17
 * Time: 3:48 PM
 */

namespace Odeo\Domains\Constant;


class GameVoucher {

  const ITUNES = 'iTunes';
  const PLAYSTATION_STORE = 'Playstation Store';
  const STEAM = 'Steam';

  const ITUNES_USD_10 = 'iTunes $10';
  const ITUNES_USD_15 = 'iTunes $15';
  const ITUNES_USD_25 = 'iTunes $25';
  const ITUNES_USD_50 = 'iTunes $50';

  const PLAYSTATION_STORE_IDR_100 = 'Playstation Store IDR 100k';
  const PLAYSTATION_STORE_IDR_200 = 'Playstation Store IDR 200k';
  const PLAYSTATION_STORE_IDR_400 = 'Playstation Store IDR 400k';

  const STEAM_IDR_12 = 'Steam IDR 12k';
  const STEAM_IDR_45 = 'Steam IDR 45k';
  const STEAM_IDR_60 = 'Steam IDR 60k';
  const STEAM_IDR_90 = 'Steam IDR 90k';
  const STEAM_IDR_120 = 'Steam IDR 120k';
  const STEAM_IDR_250 = 'Steam IDR 250k';
}