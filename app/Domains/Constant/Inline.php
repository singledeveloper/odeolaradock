<?php

namespace Odeo\Domains\Constant;

class Inline {

  const CMD_SETHP = 'login';
  const CMD_HELP = 'help';
  const CMD_HI = 'hi';
  const CMD_START = '/start';
  const CMD_SALDO = 'saldo';
  const CMD_SALDO_SHORT = 's';
  const CMD_CEK_HARGA = 'harga';
  const CMD_CEK_HARGA_SHORT = 'h';
  const CMD_BELI = 'beli';
  const CMD_CEK_ORDER = 'cek';
  const CMD_CEK_ORDER_BY_DEST = 'status';
  const CMD_CHANGE_PIN = 'ubahpin';
  const CMD_CHANGE_PIN_SHORT = 'gp';
  const CMD_TAMBAH_NOMOR_SHORT = 'tn';
  const CMD_LOGOUT = 'logout';
  const CMD_TIKET = 'tiket';
  const CMD_HAPUS_TIKET = 'hapus';
  const CMD_COMPLAINT = 'k';
  const CMD_AGENT_REGISTRATION = 'reg';

  const COMMAND_FORMATS = [
    self::CMD_SETHP => 3,
    self::CMD_HELP => 1,
    self::CMD_HI => 1,
    self::CMD_START => 1,
    self::CMD_SALDO => 1,
    self::CMD_SALDO_SHORT => 2,
    self::CMD_CEK_HARGA => 2,
    self::CMD_CEK_HARGA_SHORT => 2,
    self::CMD_BELI => 4,
    self::CMD_CEK_ORDER => 3,
    self::CMD_CEK_ORDER_BY_DEST => 3,
    self::CMD_CHANGE_PIN => 3,
    self::CMD_CHANGE_PIN_SHORT => 3,
    self::CMD_TAMBAH_NOMOR_SHORT => 3,
    self::CMD_LOGOUT => 1,
    self::CMD_TIKET => 4,
    self::CMD_HAPUS_TIKET => 2,
    self::CMD_COMPLAINT => 2,
    self::CMD_AGENT_REGISTRATION => 5
  ];

  const COMMAND_DESCRIPTIONS = [
    self::CMD_SALDO => [
      'command' => self::CMD_SALDO,
      'description' => 'Pengecekan saldo'
    ],
    self::CMD_SALDO_SHORT => [
      'command' => self::CMD_SALDO_SHORT . '.pin',
      'description' => 'Pengecekan saldo'
    ],
    self::CMD_CEK_HARGA => [
      'command' => self::CMD_CEK_HARGA . '.kodeoperator',
      'description' => 'Cek harga pulsa. KODEOPERATOR yang bisa digunakan: TSEL, XL, ISAT, BOLT, AXIS, SMARTFREN, TRI, PLN'
    ],
    self::CMD_CEK_HARGA_SHORT => [
      'command' => self::CMD_CEK_HARGA_SHORT . '.kodeoperator',
      'description' => 'Cek harga pulsa. KODEOPERATOR yang bisa digunakan: TSEL, TSELDATA, XL, XLDATA, ISAT, ISATDATA, ISATSMS, BOLT, BOLTDATA, AXIS, AXISDATA, SMARTFREN, TRI, TRIDATA, PLN'
    ],
    self::CMD_TIKET => [
      'command' => self::CMD_TIKET . '.kodebank.nominal.pin',
      'description' => 'Buat tiket deposit. KODEBANK yang bisa digunakan: BCA, MANDIRI, BRI'
    ],
    self::CMD_HAPUS_TIKET => [
      'command' => self::CMD_HAPUS_TIKET . '.pin',
      'description' => 'Hapus tiket deposit'
    ],
    self::CMD_BELI => [
      'command' => 'kodeproduk.nomortujuan.pin',
      'description' => 'Pembelian normal. Untuk pengulangan gunakan KODEPRODUK.NOMORTUJUAN#PENGULANGAN.PIN'
    ],
    self::CMD_CEK_ORDER => [
      'command' => self::CMD_CEK_ORDER . '.orderid.pin',
      'description' => 'Cek status order (realtime)'
    ],
    self::CMD_CEK_ORDER_BY_DEST => [
      'command' => self::CMD_CEK_ORDER_BY_DEST . '.nomortujuan.pin',
      'description' => 'Cek status nomor tujuan (realtime). Jika nomor tujuan mempunyai lebih dari satu transaksi, maka status yang dikembalikan adalah status transaksi terakhir.'
    ],
    self::CMD_CHANGE_PIN_SHORT => [
      'command' => self::CMD_CHANGE_PIN_SHORT . '.pinbaru.pinlama',
      'description' => 'Penggantian pin dengan pin baru'
    ],
    self::CMD_TAMBAH_NOMOR_SHORT => [
      'command' => self::CMD_TAMBAH_NOMOR_SHORT . '.nomor.pin',
      'description' => 'Registrasi nomor baru untuk transaksi SMS'
    ],
    self::CMD_COMPLAINT => [
      'command' => self::CMD_COMPLAINT . '.pesan',
      'description' => 'SMS komplain ke CS ODEO. Mohon untuk tidak menggunakan tanda titik (.) di dalam pesan.'
    ],
    self::CMD_AGENT_REGISTRATION => [
      'command' => self::CMD_AGENT_REGISTRATION . '.kodemaster.namaagen.kecamatan.pinbaru',
      'description' => 'Registrasi agen. Mohon untuk tidak menggunakan tanda titik (.) di dalam nama agen dan kecamatan'
    ]
  ];

  public static function commandNoAuth($command) {
    return in_array($command, [
      self::CMD_HI,
      self::CMD_HELP,
      self::CMD_CEK_HARGA,
      self::CMD_CEK_HARGA_SHORT,
      self::CMD_SALDO,
      self::CMD_START,
      self::CMD_COMPLAINT,
      self::CMD_AGENT_REGISTRATION
    ]);
  }

  public static function commandHistory($command) {
    if (!isset((self::COMMAND_FORMATS)[$command])) return true;
    else return in_array($command, [
      self::CMD_BELI,
      self::CMD_CHANGE_PIN
    ]);
  }

}
