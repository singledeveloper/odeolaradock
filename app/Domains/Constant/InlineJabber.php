<?php

namespace Odeo\Domains\Constant;

use Odeo\Domains\Vendor\Jabber\ReceiveTester;

class InlineJabber {

  const DEFAULT_ACCOUNT = 'odeo@jabbr.id';

  const RES_PARAM = 'jabber_response';
  const DEV_SERVER = 'http://staging.odeo.co.id:5281/api';
  const DEV_HOST = 'staging.odeo.co.id';
  const PROD_SERVER = 'http://jabber.id:5281/api_8ef7';
  const PROD_HOST = 'jabber.id';

  const EXAMPLE_EMPTY_STOCK = 'Trx ke 085216721901. Stok kosong, silakan melakukan transaksi dengan produk lain.';
  const EXAMPLE_FAILED = 'Trx ke 085216721901. Tidak dapat melakukan transaksi sekarang, silakan mencoba lagi.';
  const EXAMPLE_SAME_TRANSACTION = 'Trx sdh pernah @05/07/2017 08:23:10 status: ' . self::EXAMPLE_SUCCESS;
  const EXAMPLE_CUTOFF = 'GAGAL Trx ke 085216721901. Sedang ada cutoff antara jam 23.30 - 00.30, silakan coba lagi setelah proses selesai.';
  const EXAMPLE_PENDING = 'PENDING Trx #123 Telkomsel 5; 085216721901 sedang diproses.';
  const EXAMPLE_SUCCESS = 'SUKSES Trx #123 Telkomsel 5; 085216721901 SN: 7041922145401007921. Sisa saldo :sisa_saldo.';
  const EXAMPLE_PLN_SUCCESS = 'SUKSES Trx #123 PLN Token 20; 14224814441 dengan SN: 5571-6314-0150-8114-0679/KOS ANGGREK (F)/R1/1300VA/200. Sisa saldo :sisa_saldo.';
  const EXAMPLE_REFUND = 'GAGAL Trx #123 Telkomsel 5; 085216721901, sudah di-refund. Sisa saldo :sisa_saldo.';

  const EXAMPLE_NUMBER = '085216721901';
  const EXAMPLE_NUMBER_PLN = '14224814441';
  const EXAMPLE_ITEM_NAME = 'Telkomsel 5';
  const EXAMPLE_ITEM_NAME_PLN = 'PLN Token 20';
  const EXAMPLE_ORDER_ID = '123';
  const EXAMPLE_SN = '7041922145401007921';
  const EXAMPLE_SN_PLN = '5571-6314-0150-8114-0679/KOS ANGGREK (F)/R1/1300VA/200';
  const EXAMPLE_REMAIN_OCASH_SUCCESS = '94,290 - 19,800 = Rp74,490';
  const EXAMPLE_REMAIN_OCASH_REFUND = '94,290 + 19,800 = 114,090';

  const TYPE_UNMATCH_TRANSACTION = 'unmatch_transaction';
  const TYPE_UNMATCH_PATTERN = 'unmatch_pattern';
  const TYPE_UNMATCH_ROUTE = 'unmatch_route';
  const TYPE_ERROR_REPORT = 'error_report';

  const ACCOUNT_TYPE_USER = 'user';
  const ACCOUNT_TYPE_BILLER = 'biller';

  const JAXL_CHAT = 'send_chat_msg';
  const JAXL_SUBSCRIBE = 'subscribe';
  const JAXL_CHANGE_STATUS = 'set_status';

  public static function getCommandDescriptions() {
    $descriptions = Inline::COMMAND_DESCRIPTIONS;
    return [
      $descriptions[Inline::CMD_SALDO],
      $descriptions[Inline::CMD_CEK_HARGA],
      $descriptions[Inline::CMD_TIKET],
      $descriptions[Inline::CMD_HAPUS_TIKET],
      $descriptions[Inline::CMD_BELI]
    ];
  }

  public static function getReplyExamples() {
    return [[
      'title' => 'Stok kosong',
      'reply' => trans('pulsa.inline.empty_stock', [
        'number' => self::EXAMPLE_NUMBER
      ])
    ], [
      'title' => 'Gagal',
      'reply' => trans('pulsa.inline.failed', [
        'number' => self::EXAMPLE_NUMBER
      ])
    ], [
      'title' => 'Duplikat transaksi',
      'reply' => trans('pulsa.inline.same_transaction', [
        'date' => date('d/m/Y H:i:s'),
        'message' => trans('pulsa.inline.success', [
          'order_id' => self::EXAMPLE_ORDER_ID,
          'item_name' => self::EXAMPLE_ITEM_NAME,
          'number' => self::EXAMPLE_NUMBER,
          'sn' => self::EXAMPLE_SN,
          'sisa_saldo' => self::EXAMPLE_REMAIN_OCASH_SUCCESS
        ])
      ])
    ], [
      'title' => 'Waktu cutoff',
      'reply' => trans('pulsa.inline.cut_off', [
        'number' => self::EXAMPLE_NUMBER
      ])
    ], [
      'title' => 'Pending',
      'reply' => trans('pulsa.inline.pending', [
        'order_id' => self::EXAMPLE_ORDER_ID,
        'item_name' => self::EXAMPLE_ITEM_NAME,
        'number' => self::EXAMPLE_NUMBER
      ])
    ], [
      'title' => 'Sukses pulsa dan paket data',
      'reply' => trans('pulsa.inline.success', [
        'order_id' => self::EXAMPLE_ORDER_ID,
        'item_name' => self::EXAMPLE_ITEM_NAME,
        'number' => self::EXAMPLE_NUMBER,
        'sn' => self::EXAMPLE_SN,
        'sisa_saldo' => self::EXAMPLE_REMAIN_OCASH_SUCCESS
      ])
    ], [
      'title' => 'Sukses PLN',
      'reply' => trans('pulsa.inline.success', [
        'order_id' => self::EXAMPLE_ORDER_ID,
        'item_name' => self::EXAMPLE_ITEM_NAME_PLN,
        'number' => self::EXAMPLE_NUMBER_PLN,
        'sn' => self::EXAMPLE_SN_PLN,
        'sisa_saldo' => self::EXAMPLE_REMAIN_OCASH_SUCCESS
      ])
    ], [
      'title' => 'Pengembalian saldo',
      'reply' => trans('pulsa.inline.refund', [
        'order_id' => self::EXAMPLE_ORDER_ID,
        'item_name' => self::EXAMPLE_ITEM_NAME,
        'number' => self::EXAMPLE_NUMBER,
        'sisa_saldo' => self::EXAMPLE_REMAIN_OCASH_REFUND
      ])
    ]];
  }

  public static function getServer() {
    return app()->environment() == 'production' ? self::PROD_SERVER : self::DEV_SERVER;
  }

  public static function getHost() {
    return app()->environment() == 'production' ? self::PROD_HOST : self::DEV_HOST;
  }

  public static function getParserPath($cmd) {
    switch($cmd) {
      case Inline::CMD_HI:
        return \Odeo\Domains\Subscription\Vendor\Jabber\HelpLauncher::class;
      case Inline::CMD_SALDO:
        return \Odeo\Domains\Transaction\Vendor\Jabber\CashSelector::class;
      case Inline::CMD_BELI:
        return \Odeo\Domains\Order\Vendor\Jabber\OrderPurchaser::class;
      case Inline::CMD_CEK_HARGA:
        return \Odeo\Domains\Inventory\Helper\Vendor\Odeo\JabberPulsaSearcher::class;
      case Inline::CMD_CEK_ORDER:
        return \Odeo\Domains\Order\Vendor\Jabber\OrderSelector::class;
      case Inline::CMD_TIKET:
        return \Odeo\Domains\Order\Vendor\Jabber\OrderTopupRequester::class;
      case Inline::CMD_HAPUS_TIKET:
        return \Odeo\Domains\Order\Vendor\Jabber\OrderTopupCanceller::class;
      default:
        return \Odeo\Domains\Order\Vendor\Jabber\OrderPurchaser::class;
    }
  }

  public static function getNotifyPath($from) {
    if (in_array($from, ReceiveTester::EMAILS)) return \Odeo\Domains\Vendor\Jabber\ReceiveTester::class;
    else {
      switch($from) {
        case Obox::CENTER:
        case Obox::CENTER2:
        case Obox::SENDER:
          return \Odeo\Domains\Vendor\SMS\Obox\SMSAcceptor::class;
        default:
          return '';
      }
    }
  }

  public static function getDefaultSocket($id = 1) {
    $jabberStore = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberStoreRepository::class);
    return $jabberStore->findById($id)->socket_path;
  }
}
