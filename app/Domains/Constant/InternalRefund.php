<?php

namespace Odeo\Domains\Constant;

class InternalRefund {

  const INTERNAL_REFUND_USER_ID = 118303;
  const INTERNAL_REFUND_USER_NAME = 'PT Odeo Teknologi Indonesia';

  static function getBankId($bankScrapeAccountNumber) {
    $target = $bankScrapeAccountNumber->bankScrapeAccount->referred_target;
    switch ($target) {
      case 'bca':
        return Bank::BCA;
      case 'bni':
        return Bank::BNI;
      case 'mandiri':
      case 'mandiri_giro':
        return Bank::MANDIRI;
      case 'cimb':
        return Bank::CIMBNIAGA;
      case 'bri':
        return Bank::BRI;
      case 'permata':
        return Bank::PERMATA;
      default:
        return null;
    }
  }

}