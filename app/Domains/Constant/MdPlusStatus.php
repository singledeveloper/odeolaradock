<?php

namespace Odeo\Domains\Constant;

class MdPlusStatus {
 
 const PENDING = 10000;
 const OK = 50000;
 const OK_FREE = 50001;
 const EXPIRED = 90001;
 
}
