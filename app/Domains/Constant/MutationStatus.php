<?php

namespace Odeo\Domains\Constant;

class MutationStatus {

  const IN = 'in';
  const OUT = 'out';
  const PENDING = 'pending';
}
