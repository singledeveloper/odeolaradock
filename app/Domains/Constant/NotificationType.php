<?php

namespace Odeo\Domains\Constant;

class NotificationType {

 const ALPHABET = 'alphabet';
 const WARNING = 'warning';
 const CHECK = 'check';

 const NOTICE_REFUND = 'refund';
 const NOTICE_DISTURBANCE = 'disturbance';
 const NOTICE_EMPTY_STOCK = 'empty_stock';
 const NOTICE_REMINDER = 'reminder';
 const NOTICE_NO_KYC = 'no_kyc';
 const PARSE_MULTIPLIER = '%multiplier%';

}
