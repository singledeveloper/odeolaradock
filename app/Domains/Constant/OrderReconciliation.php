<?php

namespace Odeo\Domains\Constant;

class OrderReconciliation {

  const OTHER_INCOME = 'other_income';
  const UNIQUE_CODE = 'unique_code';
  
  const OTHER_EXPENSE = 'other_expense';
  const COMPENSATION_FOR_CUSTOMERS = 'compensation_for_customers';
  const PROMOTION_EXPENSE = 'promotion_expense';
  const RUSH_BONUS = 'rush_bonus';
  const SYSTEM_FAULT = 'system_fault';
   
}
