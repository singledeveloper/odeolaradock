<?php


namespace Odeo\Domains\Constant;


class PaxPayment {

  const DEBIT_ON_US_BNI_MDR_PCT = 0.13;
  const DEBIT_OFF_US_BNI_MDR_PCT = 0.8;
  const CREDIT_ON_US_BNI_MDR_PCT = 1.5;
  const CREDIT_OFF_US_BNI_MDR_PCT = 1.8;

  const TYPE_DEBIT_OFF_US = 'DEBIT OFF US';
  const TYPE_DEBIT_ON_US = 'DEBIT ON US';
  const TYPE_CREDIT_OFF_US = 'CREDIT OFF US';
  const TYPE_CREDIT_ON_US = 'CREDIT ON US';

  const PAYMENT_GATEWAY_COST_TIERING = [
    [
      'trx' => 3000,
      'cost' => 150,
    ],
    [
      'trx' => 1000,
      'cost' => 175,
    ],
  ];

  const PAYMENT_GATEWAY_COST_DEFAULT = 200;

  const BNI_PARSE_STATUS_FAILED = 'failed';
  const BNI_PARSE_STATUS_SUCCESS = 'success';

}