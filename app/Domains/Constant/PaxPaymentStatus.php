<?php


namespace Odeo\Domains\Constant;


class PaxPaymentStatus {

  const COMPLETED = '50000';
  const FAILED = '90000';
  const VOIDED = '90004';

}