<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 04/12/18
 * Time: 21.12
 */

namespace Odeo\Domains\Constant;


class PaymentGateway {

  const PENDING = 10000;
  const WAITING_FOR_ACCEPTANCE = 10001;
  const ACCEPTED = 30000;
  const SUCCESS = 50000;
  const SUSPECT = 80000;
  const SUSPECT_TIMEOUT_PAYMENT_ACCEPTANCE = 80003;
  const FAILED = 90000;
  const FAILED_SYSTEM_ERROR_WHEN_WAITING_FOR_PAYMENT_ACCEPTANCE = 90001;
  const FAILED_PAYMENT_REJECTED_BY_MERCHANT = 90002;
  const FAILED_PAYMENT_MALFORMED_MERCHANT_RESPONSE = 90003;
  const FAILED_PAYMENT_EXTERNAL_REFERENCE_ID_IS_DUPLICATE = 90004;
  const FAILED_PAYMENT_UPDATE = 90005;
  const VOIDED = 90006;
  
  const STATUS_CODE_TO_MERCHANT_MESSAGE = [
    self::PENDING => 'api_pending',
    self::WAITING_FOR_ACCEPTANCE => 'api_pending_merchant_acceptance',
    self::ACCEPTED => 'api_on_progress_payment_approval',
    self::SUSPECT => 'api_suspect',
    self::SUSPECT_TIMEOUT_PAYMENT_ACCEPTANCE => 'api_suspect',
    self::SUCCESS => 'api_completed',
    self::FAILED => 'api_failed',
    self::FAILED_SYSTEM_ERROR_WHEN_WAITING_FOR_PAYMENT_ACCEPTANCE => 'api_failed',
    self::FAILED_PAYMENT_REJECTED_BY_MERCHANT => 'api_failed',
    self::FAILED_PAYMENT_MALFORMED_MERCHANT_RESPONSE => 'api_failed',
    self::FAILED_PAYMENT_EXTERNAL_REFERENCE_ID_IS_DUPLICATE => 'api_failed',
  ];

  const STATUS_CODE_TO_MERCHANT_CODE = [
    self::PENDING => 10001,
    self::WAITING_FOR_ACCEPTANCE => 10001,
    self::ACCEPTED => 30000,
    self::SUCCESS => 50000,
    self::SUSPECT => 80000,
    self::SUSPECT_TIMEOUT_PAYMENT_ACCEPTANCE => 80000,
    self::FAILED => 90001,
    self::FAILED_SYSTEM_ERROR_WHEN_WAITING_FOR_PAYMENT_ACCEPTANCE => 90000,
    self::FAILED_PAYMENT_REJECTED_BY_MERCHANT => 90001,
    self::FAILED_PAYMENT_MALFORMED_MERCHANT_RESPONSE => 90000,
    self::FAILED_PAYMENT_EXTERNAL_REFERENCE_ID_IS_DUPLICATE => 90000,
    self::FAILED_PAYMENT_UPDATE => 90000,
  ];

  const MERCHANT_MESSAGE_TO_STATUS = [
    'PENDING' => [
      self::PENDING,
      self::WAITING_FOR_ACCEPTANCE
    ],
    'ON_PROGRESS_PAYMENT_APPROVAL' => [
      self::ACCEPTED
    ],
    'SUCCESS' => [
      self::SUCCESS,
    ],
    'SUSPECT' => [
      self::SUSPECT,
      self::SUSPECT_TIMEOUT_PAYMENT_ACCEPTANCE
    ],
    'FAILED' => [
      self::FAILED,
      self::FAILED_SYSTEM_ERROR_WHEN_WAITING_FOR_PAYMENT_ACCEPTANCE,
      self::FAILED_PAYMENT_REJECTED_BY_MERCHANT,
      self::FAILED_PAYMENT_MALFORMED_MERCHANT_RESPONSE,
      self::FAILED_PAYMENT_EXTERNAL_REFERENCE_ID_IS_DUPLICATE,
    ],
  ];

  static function getMerchantStatusCode($statusCode) {
    return self::STATUS_CODE_TO_MERCHANT_CODE[$statusCode] ?? '99999';
  }

  static function getMerchantStatusMessage($statusCode, $translate = true) {
    $status = self::STATUS_CODE_TO_MERCHANT_MESSAGE[$statusCode] ?? 'api_unknown';
    return $translate ? trans('status.' . $status) : $status;
  }

  static function getMerchantStatusMessageWithCode($statusCode) {
    $merchantStatusCode = self::getMerchantStatusCode($statusCode);
    $unknownStatus = trans('status.api_unknown') . ' - ' . $merchantStatusCode;
    return (trans('status.' . self::STATUS_CODE_TO_MERCHANT_MESSAGE[$statusCode]) . ' - ' . $merchantStatusCode) ?? $unknownStatus;
  }

  static function exceedVaCodeLength($vaCode, $maxLength) {
    return strlen($vaCode) > $maxLength;
  }

}
