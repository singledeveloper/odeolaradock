<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 06/12/18
 * Time: 14.19
 */

namespace Odeo\Domains\Constant;


class PermataDisbursement {

  const STATUS_PENDING = '10000';
  const STATUS_TIMEOUT = '40001';
  const STATUS_SUCCESS = '50000';
  const STATUS_FAIL = '90000';

  const INQUIRY_COST = 0;
  const DISBURSEMENT_COST = 150;

  const STATUS_CODE_INQUIRY_FAILED = 'INQUIRY_FAILED';
}
