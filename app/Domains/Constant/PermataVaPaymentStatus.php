<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 20/08/19
 * Time: 19.27
 */

namespace Odeo\Domains\Constant;


class PermataVaPaymentStatus {

  const PENDING = 10000;
  const ACCEPTED = 30000;
  const SUCCESS = 50000;
  const SUSPECT = 80000;
  const FAILED = 90000;
  const FAILED_NEED_UPDATE = 90005;
  const SUSPECT_TIMEOUT = 80003;
  const SUSPECT_NEED_STATUS_RECHECK = 80004;
  const SUSPECT_FOR_TESTING = 89000;
  const SUSPECT_IS_CANCELLED_BUT_NEED_STATUS_RECHECK = 80005;

  public static function getPaymentGatewayStatus($status) {
    $map = [
      self::SUCCESS => PaymentGateway::SUCCESS,
      self::FAILED => PaymentGateway::FAILED,
      self::FAILED_NEED_UPDATE => PaymentGateway::FAILED,
      self::SUSPECT => PaymentGateway::SUSPECT,
      self::SUSPECT_TIMEOUT => PaymentGateway::SUSPECT,
      self::SUSPECT_FOR_TESTING => PaymentGateway::SUSPECT
    ];
    return $map[$status];
  }

}