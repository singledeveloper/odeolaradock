<?php

namespace Odeo\Domains\Constant;


class Plan {

  const LITE = 1;
  const STARTER = 2;
  const BUSINESS = 3;
  const PRO = 4;
  const ENTERPRISE = 5;
  const FREE = 99;

  const RENEWAL_ACTIVE = true;

  public static function getAvailableServices($planId = '') {

    if (!isProduction())
      return [
        Service::PULSA,
        Service::PULSA_POSTPAID,
        Service::PAKET_DATA,
        Service::BOLT,
        Service::PLN,
        Service::PLN_POSTPAID,
        Service::BPJS_KES,
        Service::PDAM,
        Service::BROADBAND,
        Service::LANDLINE,
        Service::GAME_VOUCHER,
        Service::GOOGLE_PLAY,
        Service::TRANSPORTATION,
        Service::MULTI_FINANCE,
        Service::EMONEY
      ];

    $availableServices = [
      self::LITE => [
        Service::PULSA
      ],
      self::STARTER => [
        Service::PULSA,
        Service::PAKET_DATA,
        Service::BOLT,
        Service::PLN,

        //additional after hide other plans
//        Service::PLN_POSTPAID,
//        Service::BPJS_KES,
//        Service::PDAM,
//        Service::BROADBAND,
//        Service::LANDLINE,
//        Service::GAME_VOUCHER,
//        Service::GOOGLE_PLAY,
//        Service::TRANSPORTATION,
//        Service::MULTI_FINANCE
      ],
      self::BUSINESS => [
        Service::PULSA,
        Service::PULSA_POSTPAID,
        Service::PAKET_DATA,
        Service::BOLT,
        Service::PLN,
        Service::PLN_POSTPAID
      ],
      self::PRO => [
        Service::PULSA,
        Service::PULSA_POSTPAID,
        Service::PAKET_DATA,
        Service::BOLT,
        Service::PLN,
        Service::PLN_POSTPAID,
        Service::BROADBAND,
        Service::LANDLINE
      ],
      self::ENTERPRISE => [
        Service::PULSA,
        Service::PULSA_POSTPAID,
        Service::PAKET_DATA,
        Service::BOLT,
        Service::PLN,
        Service::PLN_POSTPAID,
        Service::BPJS_KES,
        Service::PDAM,
        Service::PGN,
        Service::EMONEY,
        Service::BROADBAND,
        Service::LANDLINE,
        Service::GAME_VOUCHER,
        Service::GOOGLE_PLAY,
        Service::TRANSPORTATION,
        Service::MULTI_FINANCE,
        Service::EMONEY
        //Service::CREDIT_BILL
      ],
      self::FREE => [
        Service::PULSA,
        Service::PAKET_DATA,
        Service::PLN,
        Service::TRANSPORTATION,
        Service::GAME_VOUCHER,
        Service::BOLT,
        Service::EMONEY
      ]
    ];

    if ($planId == '') return $availableServices;
    else if (isset($availableServices[$planId])) return $availableServices[$planId];
    return [];
  }


  public static function isReservedKeyword($name) {

    $reservedContainWord = [
      'odeo', 'odeoteknologi', 'ocommerce', 'opay',
      'owallet', 'ocash', 'topup', 'deposit', 'odeposit', 'odeosafe', 'warranty',
      'owarranty','bokep', 'porn', 'pornographic', 'bb17', 'bb17++', 'porn++', 'bokep++', 'software',
      'hacker', 'hackers', 'community', 'forum', 'forums',
      'communities', 'digital', 'system', 'internet', 'systems', 'comunnist', 'ganja',
      'sabusabu', 'obat', 'obatobat', 'obatterlarang', 'scam', 'scams', 'porns',
      'anal', 'anus', 'arse', 'ass', 'ballsack', 'balls', 'bastard', 'bitch', 'biatch',
      'bloody', 'blowjob', 'blow-job', 'bollock', 'bollok', 'boner', 'boob',
      'bugger', 'bum', 'butt', 'buttplug', 'clitoris', 'cock', 'coon', 'crap',
      'cunt', 'damn', 'dick', 'dildo', 'dyke', 'fag', 'feck', 'fellate', 'fellatio',
      'felching', 'fuck', 'f-u-c-k', 'fudgepacker', 'fudge packer', 'flange',
      'Goddamn', 'God-damn', 'hell', 'homo', 'jerk', 'jizz', 'knobend', 'knob-end',
      'labia', 'lmao', 'lmfao', 'muff', 'nigger', 'nigga', 'omg', 'penis',
      'piss', 'poop', 'prick', 'pube', 'pussy', 'queer', 'scrotum', 'sex',
      'shit', 'sh1t', 'slut', 'smegma', 'spunk', 'tit', 'tosser', 'turd',
      'twat', 'vagina', 'wank', 'whore', 'wtf', 'moderation', 'discussion', 'versioning', 'versions',
      'apiv2', 'apiv1', 'api-v1', 'api-v2', 'api-v3', 'media', 'social-media', 'socialmedia', 'staging',
      'stage', 'mamamintapulsa', 'papamintapulsa', 'mama-minta-pulsa', 'papa-minta-pulsa'
    ];

    $reservedEqualWord =  [
      'android', 'iphone', 'tiket', 'ppob', 'application',
      'sandbox', 'development', 'develop', 'apidev', 'adminpanel', 'paketdata',
      'datapacket', 'packetdata', 'odeo', 'odeoteknologi', 'ocommerce', 'opay',
      'owallet', 'customer', 'customerservice', 'platform', 'payment', 'vendor',
      'withdraw', 'email', 'notification', 'channel', 'bpjs', 'bpjskesehatan',
      'bpjstenagakerja', 'bpjstenagakerjaan', 'telkomsel', 'indosat', 'three',
      'smartfren', 'axis', 'kredivo', 'sandbox2', 'administration', 'service',
      'agent', 'master', 'affiliate', 'partner', 'application', 'job', 'career',
      'profile', 'portfolio', 'jobs', 'company', 'stores', 'store', 'market',
      'webdev', 'services', 'zendesk', 'biller', 'subscribe', 'subscription',
      'script', 'programming', 'users', 'account', 'transaction', 'network',
      'providers', 'provider', 'activity', 'marketing', 'core', 'database', 'test',
      'resource', 'recources', 'storage', 'banner', 'domains', 'domain', 'console',
      'library', 'libraries', 'myapi', 'answer', 'question', 'errors', 'error',
      'windows', 'ocash', 'promo', 'promotion', 'discount', 'video', 'partnership',
      'mobile', 'mobileweb', 'webmobile', 'website', 'programs', 'training', 'employ',
      'employee', 'employement', 'management', 'manage', 'investor', 'invests', 'invest',
      'dashboard', 'landing', 'landpage', 'page', 'sales', 'report', 'reports', 'orders',
      'merchant', 'merchants', 'product', 'products', 'feedback', 'feeds', 'finance',
      'financial', 'database', 'configuration', 'config', 'configs', 'webstore', 'web-store',
      'bpjs-tenagakerja', 'bpjs-tenagakerjaan', 'bpjs-kesehatan', 'data-packet',
      'packet-data', 'paket-data', 'sand-box', 'voucher', 'vouchers', 'recons',
      'reconciliation', 'disbursement', 'transfer', 'withdrawal', 'search',
      'searching', 'searchs', 'merchant-success', 'works', 'topup', 'assurance',
      'bootstrap', 'storages', 'tests', 'testing', 'feature', 'features', 'homes',
      'register', 'login', 'online', 'authentication', 'authorization', 'delete',
      'remove', 'automatic', 'renewal', 'news', 'server', 'signin', 'sign-in',
      'sign-up', 'signup', 'section', 'sections', 'design', 'designs', 'daily',
      'daily-report', 'daily-reports', 'dailyreport', 'virtual', 'host', 'hosting',
      'request', 'fanpage', 'fan-page', 'deposit', 'odeposit', 'odeosafe', 'warranty',
      'owarranty', 'teams', 'team', 'commisions', 'hustler', 'leader', 'mentor', 'bonus',
      'hustlers', 'leaders', 'commision', 'mentors', 'device', 'devices', 'banks',
      'cashback', 'cashbacks', 'cash-back', 'merchant-news', 'information', 'informations',
      'themes', 'warning', 'confirm', 'confirmation', 'confirmations', 'public',
      'publics', 'saldo', 'faq', 'tanya', 'tanya-jawab', 'jawab', 'halaman', 'pelanggan',
      'fitur', 'telkomseldata', 'datatelkomsel', 'data-telkomsel', 'telkomsel-data',
      'indosat-data', 'data-indosat', 'indosatdata', 'dataindosat', 'indosatsms',
      'smsindosat', 'sms-indosat', 'indosat-sms', 'xldata', 'xl-data', 'data-xl',
      'threedata', 'data-three', 'datathree', 'axis-data', 'data-axis', 'axisdata',
      'dataaxis', 'plntoken', 'tokenpln', 'token-pln', 'pln-token', 'bolt-data', 'data-bolt',
      'inventory', 'inventories', 'hack', 'hackathon', 'hackathons', 'ideas', 'offical',
      'bokep', 'porn', 'pornographic', 'bb17', 'bb17++', 'porn++', 'bokep++', 'software',
      'download', 'upload', 'hacker', 'hackers', 'community', 'forum', 'forums',
      'communities', 'digital', 'system', 'internet', 'systems', 'comunnist', 'ganja',
      'sabusabu', 'obat', 'obatobat', 'obatterlarang', 'scam', 'scams', 'porns',
      'anal', 'anus', 'arse', 'ass', 'ballsack', 'balls', 'bastard', 'bitch', 'biatch',
      'bloody', 'blowjob', 'blow-job', 'bollock', 'bollok', 'boner', 'boob',
      'bugger', 'bum', 'butt', 'buttplug', 'clitoris', 'cock', 'coon', 'crap',
      'cunt', 'damn', 'dick', 'dildo', 'dyke', 'fag', 'feck', 'fellate', 'fellatio',
      'felching', 'fuck', 'f-u-c-k', 'fudgepacker', 'fudge packer', 'flange',
      'Goddamn', 'God-damn', 'hell', 'homo', 'jerk', 'jizz', 'knobend', 'knob-end',
      'labia', 'lmao', 'lmfao', 'muff', 'nigger', 'nigga', 'omg', 'penis',
      'piss', 'poop', 'prick', 'pube', 'pussy', 'queer', 'scrotum', 'sex',
      'shit', 'sh1t', 'slut', 'smegma', 'spunk', 'tit', 'tosser', 'turd',
      'twat', 'vagina', 'wank', 'whore', 'wtf', 'moderation', 'discussion', 'versioning', 'versions',
      'apiv2', 'apiv1', 'api-v1', 'api-v2', 'api-v3', 'media', 'social-media', 'socialmedia', 'staging',
      'stage', 'mamamintapulsa', 'papamintapulsa', 'mama-minta-pulsa', 'papa-minta-pulsa'
    ];

    foreach ($reservedEqualWord as $word) {
      if ($name === $word) {
        return true;
      }
    }

    foreach ($reservedContainWord as $word) {
      if (strstr($name, $word)) {
        return true;
      }
    }


    return false;
  }

}
