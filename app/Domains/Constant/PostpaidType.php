<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/26/16
 * Time: 11:08 PM
 */

namespace Odeo\Domains\Constant;


class PostpaidType {

  const PLN_POSTPAID = 'PLN Pascabayar';
  const TELKOM_PSTN = 'Telkom Telepon Rumah';
  const INDIHOME = 'Indihome';
  const TELKOM_SPEEDY = 'Telkom Speedy';
  const TELKOM_FLEXI = 'Telkom Flexi';
  const TELKOMSEL_HALO = 'Telkomsel Halo';
  const MATRIX_ISAT = 'Matrix Indosat';
  const SMARTFREN_POSTPAID = 'Smartfren Postpaid';
  const TRI_POSTPAID = 'Three Postpaid';
  const XL_POSTPAID = 'XL Postpaid';
  const ISAT_POSTPAID = 'Indosat Postpaid';
  const BPJS_KES = 'BPJS Kesehatan';
  const BPJS_TK = 'BPJS Ketenagakerjaan';
  const PDAM = 'PDAM';
  const PGN = 'PGN';
  const MULTIFINANCE = 'Multi Finance';

  const MULTIFINANCE_TYPE_CONVENTIONAL = 'Konvensional';
  const MULTIFINANCE_TYPE_SYARIAH = 'Syariah';

  public static function getConstKeyByValue($searchVal) {
    $constants = (new \ReflectionClass(__CLASS__))->getConstants();
    foreach ($constants as $name => $value) {
      if ($value == $searchVal) return $name;
    }
    return '';
  }

  public static function getConstValueByKey($keyVal) {
    $constants = (new \ReflectionClass(__CLASS__))->getConstants();
    return isset($constants[$keyVal]) ? $constants[$keyVal] : '';
  }

  public static function getAll() {
    return array(
      self::PLN_POSTPAID,
      self::TELKOM_PSTN,
      self::TELKOM_SPEEDY,
      self::INDIHOME,
      self::TELKOM_FLEXI,
      self::TELKOMSEL_HALO,
      self::ISAT_POSTPAID,
      self::MATRIX_ISAT,
      self::SMARTFREN_POSTPAID,
      self::TRI_POSTPAID,
      self::XL_POSTPAID,
      self::BPJS_KES,
      // self::BPJS_TK,
      self::PDAM,
      self::PGN,
      self::MULTIFINANCE,
      // self::MULTIFINANCE_TYPE_CONVENTIONAL,
      // self::MULTIFINANCE_TYPE_SYARIAH
    );
  }

  public static function groupBroadband() {
    return [
      self::INDIHOME,
      self::TELKOM_SPEEDY
    ];
  }

  public static function groupPulsaPostpaid() {
    return [
      self::TELKOMSEL_HALO,
      self::TELKOM_FLEXI,
      self::MATRIX_ISAT,
      self::SMARTFREN_POSTPAID,
      self::TRI_POSTPAID,
      self::XL_POSTPAID
    ];
  }

}