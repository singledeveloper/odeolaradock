<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 4:15 PM
 */

namespace Odeo\Domains\Constant;


class Pulsa {

  const CURRENT_NUMBER_TEST = [];
  const CURRENT_VSID_TEST = [
    SwitcherConfig::JATELINDO,
    SwitcherConfig::KUDO
  ];

  const OPERATOR_TELKOMSEL = 1;
  const OPERATOR_INDOSAT = 2;
  const OPERATOR_XL = 3;
  const OPERATOR_THREE = 4;
  const OPERATOR_AXIS = 5;
  const OPERATOR_SMARTFREN = 6;
  const OPERATOR_ESIA = 7;
  const OPERATOR_PLN = 8;
  const OPERATOR_BOLT = 9;

  const IS_MAINTANANCE = false;
  const MAINTANANCE_MESSAGE = 'Sedang dalam maintanance (perbaikan).';
  const DIFFERENCE_PRICE_LIMIT = 200;
  const PLN_SWITCH_FLAG = 10;

  const STATUS_OK = '50000';
  const STATUS_OK_HIDDEN = '50001';
  const STATUS_OK_SUPPLIER = '50002';
  const STATUS_PERMANENT_DELETED = '90000';

  const REPORT_TYPE_SKU = 'sku_odeo';
  const REPORT_TYPE_SKU_DISABLED = 'sku_odeo_disabled';
  const REPORT_TYPE_MARKETPLACE_SKU = 'sku_marketplace';
  const REPORT_TYPE_MARKETPLACE_SKU_DISABLED = 'sku_marketplace_disabled';
  const REPORT_TYPE_OOS = 'oos';
  const REPORT_TYPE_REFUND = 'refund';
  const REPORT_TYPE_DEAD_STOCK = 'dead_stock';

  const REDIS_H2H_NS = 'odeo_core:h2h';
  const REDIS_H2H_KEY_USERIDS = 'user_ids';
  const REDIS_H2H_KEY_PLN_USER_AGENT = 'pln_user_agent';
  const REDIS_H2H_KEY_PLN_AUTO_SCRAPE_LAST_ID = 'pln_auto_scrape_last_id';
  const REDIS_H2H_KEY_PLN_AUTO_SCRAPE_LOCK = 'pln_auto_scrape_lock';

  const REDIS_PULSA_SUCCESS_LOCK = 'pulsa_success_lock_';
  const REDIS_PULSA_CHECK_LOCK = 'pulsa_check_lock_';
  const REDIS_PLN_SWITCH_LOCK = 'pln_switch_lock';

  public static function getProxyURL($sessionId) {
    return "http://lum-customer-brian_j-zone-static-session-{$sessionId}:xb3c27gvwpiw@zproxy.lum-superproxy.io:22225";
  }

  public static function getUserProxyURL($ip) {
    return 'http://lum-customer-brian_j-zone-sc-ip-' . $ip . ':61p6a9y8hzjb@zproxy.lum-superproxy.io:22225';
  }

}