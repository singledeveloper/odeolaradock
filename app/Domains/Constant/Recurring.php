<?php

namespace Odeo\Domains\Constant;


class Recurring {

  const PENDING = '10000';
  const RETRIED = '10001';
  const FAILED = '40000';
  const COMPLETED = '50000';

  const TYPE_ONCE = 'once';
  const TYPE_DAY = 'every_day';
  const TYPE_DAY_WORK = 'every_workday';
  const TYPE_DAY_MON_SAT = 'every_monsatday';
  const TYPE_WEEK = 'every_week';
  const TYPE_MONTH = 'every_month';
  const TYPE_MANUAL = 'manual';

  const TYPES_FOR_PULSA = [
    self::TYPE_ONCE,
    self::TYPE_DAY,
    self::TYPE_WEEK . '_1',
    self::TYPE_WEEK . '_2',
    self::TYPE_WEEK . '_3',
    self::TYPE_WEEK . '_4',
    self::TYPE_WEEK . '_5',
    self::TYPE_WEEK . '_6',
    self::TYPE_WEEK . '_7',
    self::TYPE_MONTH
  ];

  const ALL_TYPES = [
    self::TYPE_ONCE,
    self::TYPE_DAY,
    self::TYPE_DAY_WORK,
    self::TYPE_DAY_MON_SAT,
    self::TYPE_WEEK . '_1',
    self::TYPE_WEEK . '_2',
    self::TYPE_WEEK . '_3',
    self::TYPE_WEEK . '_4',
    self::TYPE_WEEK . '_5',
    self::TYPE_WEEK . '_6',
    self::TYPE_WEEK . '_7',
    self::TYPE_MONTH
  ];

  const STATUS_CODE_TO_MESSAGE = [
    self::PENDING => 'api_pending',
    self::RETRIED => 'api_retried',
    self::FAILED => 'api_failed',
    self::COMPLETED => 'api_completed'
  ];

  public static function toStatusMessage($status) {
    if (isset(self::STATUS_CODE_TO_MESSAGE[$status])) {
      return trans('status.' . self::STATUS_CODE_TO_MESSAGE[$status]);
    }

    return trans('status.api_unknown') . ': ' . $status;
  }

  public static function getTime($number) {
    return date(trans('recurring.time_format'), strtotime(date('Y-m-d') . ' ' . ($number < 10 ? '0' : '') . $number . ':00:00'));
  }

  public static function getTypeName($type) {
    $types = self::ALL_TYPES;

    if (in_array($type, $types)) return trans('recurring.' . $type);
    else if (strpos($type, self::TYPE_DAY) !== false) {
      $time = str_replace(self::TYPE_DAY . '_', '', $type);
      return trans('recurring.' . self::TYPE_DAY) . ' ' . self::getTime($time);
    }
    else if (strpos($type, self::TYPE_DAY_WORK) !== false) {
      $time = str_replace(self::TYPE_DAY_WORK . '_', '', $type);
      return trans('recurring.' . self::TYPE_DAY_WORK) . ' ' . self::getTime($time);
    }
    else if (strpos($type, self::TYPE_DAY_MON_SAT) !== false) {
      $time = str_replace(self::TYPE_DAY_MON_SAT . '_', '', $type);
      return trans('recurring.' . self::TYPE_DAY_MON_SAT) . ' ' . self::getTime($time);
    }
    else if (strpos($type, self::TYPE_WEEK) !== false) {
      $temp = explode('_', str_replace(self::TYPE_WEEK . '_', '', $type));
      return trans('recurring.' . self::TYPE_WEEK . '_' . $temp[0]) . ' ' . self::getTime($temp[1]);
    }
    else if (strpos($type, self::TYPE_MONTH) !== false) {
      $date = str_replace(self::TYPE_MONTH . '_', '', $type);
      $temp = explode('_', $date);
      return trans('recurring.' . self::TYPE_MONTH) . ' (' . trans('recurring.date')
        . ' ' . $temp[0] . ') ' . (isset($temp[1]) ? self::getTime($temp[1]) : '');
    }
    else if (strpos($type, self::TYPE_ONCE) !== false) {
      $date = str_replace(self::TYPE_ONCE . '_', '', $type);
      $temp = explode('_', $date);
      return trans('recurring.' . self::TYPE_ONCE) . ' ('
        . date('d/m/Y', strtotime($temp[0])) . ') ' . (isset($temp[1]) ? self::getTime($temp[1]) : '');
    }
    return '';
  }

}