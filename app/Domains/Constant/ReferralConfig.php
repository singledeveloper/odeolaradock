<?php

namespace Odeo\Domains\Constant;

class ReferralConfig
{
    const AMOUNT_LIMIT = 1000;
    const VALUE = 2500000;
    const MIN_LIMIT = 200;
}