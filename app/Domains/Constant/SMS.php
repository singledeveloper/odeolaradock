<?php

namespace Odeo\Domains\Constant;

class SMS {

  const ZENZIVA = 'zenziva';
  const NEXMO = 'nexmo';
  const OBOX_OUTBOUND = 'outbound';
  const DARTMEDIA = 'dartmedia';

  const RESELLER_DARTMEDIA = [
    14 => 'BRIAN'
  ];

  public static function findDartmediaReseller($userId) {
    $resellers = [
      14 => 'BRIAN'
    ];
    return isset($resellers[$userId]) ? $resellers[$userId] : false;
  }
  
}
