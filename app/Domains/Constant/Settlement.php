<?php

namespace Odeo\Domains\Constant;

class Settlement {
  const ON_HOLD = 10000;
  const ON_PROGRESS = 10001;
  const SUCCESS = 50000;
  const SUSPECT = 80000;
  const NO_SETTLEMENT = 90000;

  const STATUS_CODE_TO_SETTLEMENT_CODE = [
    self::ON_HOLD => 10001,
    self::ON_PROGRESS => 10001,
    self::SUCCESS => 50000,
    self::SUSPECT => 80000,
    self::NO_SETTLEMENT => 90000
  ];

  const STATUS_CODE_TO_SETTLEMENT_MESSAGE = [
    self::ON_HOLD => 'api_on_hold',
    self::ON_PROGRESS => 'api_on_hold',
    self::SUCCESS => 'api_settled',
    self::SUSPECT => 'api_suspect',
    self::NO_SETTLEMENT => 'api_no_settlement'
  ];

  static function getSettlementStatusCode($trxStatus, $statusCode) {
    if ($trxStatus >= '90000') {
      return self::STATUS_CODE_TO_SETTLEMENT_CODE[self::NO_SETTLEMENT];
    }
    return self::STATUS_CODE_TO_SETTLEMENT_CODE[$statusCode] ?? '99999';
  }

  static function getSettlementStatusMessage($trxStatus, $statusCode) {
    if ($trxStatus >= '90000') {
      $status = self::STATUS_CODE_TO_SETTLEMENT_MESSAGE[self::NO_SETTLEMENT];
    }
    else $status = self::STATUS_CODE_TO_SETTLEMENT_MESSAGE[$statusCode] ?? 'api_unknown';
    return trans('status.' .$status);
  }

}
