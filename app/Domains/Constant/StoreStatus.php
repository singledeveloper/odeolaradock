<?php

namespace Odeo\Domains\Constant;

class StoreStatus {

  const PENDING = 10000;
  const OK = 50000;
  const WAIT_FOR_UPGRADE_VERIFY = 50001;
  const WAIT_FOR_RENEWAL_VERIFY = 50002;
  const CANCELLED = 90000;
  const EXPIRED = 90001;
  const WAIT_FOR_EXP_RENEWAL_VERIFY = 90002;

  const ACTIVE = [
    self::OK,
    self::WAIT_FOR_UPGRADE_VERIFY,
    self::WAIT_FOR_RENEWAL_VERIFY
  ];

  const STORE_DEFAULT = 3550;

  public static function isActive($storeStatus) {
    return in_array($storeStatus, self::ACTIVE);
  }

  public static function getStoreDefault() {
    if (isProduction()) return self::STORE_DEFAULT;
    else if (isStaging()) return 19;
    return 1;
  }
}
