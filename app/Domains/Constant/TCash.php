<?php

namespace Odeo\Domains\Constant;


class TCash {

  const PENDING_OTP = '10001';
  const PENDING_PIN = '10002';
  const CONNECTED = '50000';

  const LOGIN_OS_TYPE = 'Android';
  const LOGIN_WALLET_ID = 'ID';
  const DEFAULT_VERSION = '2.2.0';
  const DEFAULT_IMSI = '450061012341234';
  const DEFAULT_OS_VERSION = '7.1.2';
  const DEFAULT_PUSH_TYPE = 'GCM';
  const DEFAULT_MNO = 'Telkom';
  const MODEL_NO = [
    'Redmi 4',
    'SM-J400F',
    'SM-G900V',
    'Redmi 4X',
    'Redmi 5',
    'Redmi Note 3'
  ];
  const MAX_PURCHASE_LIMIT = 5;
  const MAX_CASH_MONTHLY = 20000000;

  const ACCOUNT_TYPE_BASIC = 'basic';
  const ACCOUNT_TYPE_FULL = 'full';

  const MENU_ID_TRANSFER = '4100000';
  const MENU_ID_AIRTIME_DENOMS = [
    '10000' => '3100001'
  ];

  const REDIS_TCASH = 'odeo-core:tcash';
  const REDIS_KEY_TCASH_VERSION = 'latest_version';
  const REDIS_KEY_TCASH_DISTRIBUTE_LOCK = 'distribute_lock_';
  const REDIS_KEY_TCASH_DISTRIBUTE_LOCK_CORE = 'distribute_lock_0';
  const REDIS_KEY_TCASH_LAST_INQUIRY_CHECK_DATE = 'last_inquiry_check_date';

  public static function simpleEncrypt($string) {
    return base64_encode(sha1($string, true));
  }

  public static function getModelNo() {
    $models = self::MODEL_NO;
    return $models[rand(0, count($models) - 1)];
  }

  public static function generateIMEI() {
    $len = 15;
    $rbi = ['01', '10', '30', '33', '35', '44', '45', '49', '50', '51', '52', '53', '54', '86', '91', '98', '99'];
    $arr = $rbi[rand(0, count($rbi) - 1)];
    $str = str_pad($arr[0] . $arr[1], $len, '0');

    $pos = 2;
    while ($pos < $len - 1) {
      $str[$pos++] = rand(0, 9);
    }

    $lenOffset = ($len + 1) % 2;
    $sum = 0;
    for ($pos = 0; $pos < $len - 1; $pos++) {
      if (($pos + $lenOffset) % 2) {
        $t = $str[$pos] * 2;
        if ($t > 9) $t -= 9;
        $sum += $t;
      }
      else $sum += $str[$pos];
    }

    $str[$len - 1] = (10 - ($sum % 10)) % 10;

    return $str;
  }

}