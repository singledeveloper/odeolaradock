<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 09-Apr-19
 * Time: 1:05 PM
 */

namespace Odeo\Domains\Constant;


class Transportation {

  const ETOLL = 'etoll';
  const GRAB = 'grab';
  const GOJEK = 'gojek';

  public static function getCategoriesByKey($key) {
    if (isProduction()) {
      switch ($key) {
        case self::ETOLL:
          return [
            'Transportasi e-Toll',
            'Transportasi e-Toll BNI'
          ];
        case self::GOJEK:
          return [
            'Transportasi Gojek'
          ];
        case self::GRAB:
          return [
            'Transportasi Grab',
            'Transportasi Grab Driver'
          ];
        default:
          return [];
      }

    } else {
      switch ($key) {
        case self::ETOLL:
          return [
            'Transportasi e-Toll',
          ];
        case self::GOJEK:
          return [
            'Gojek'
          ];
        case self::GRAB:
          return [
            'Grab',
          ];
        default:
          return [];
      }
    }
  }
}