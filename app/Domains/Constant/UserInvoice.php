<?php
namespace Odeo\Domains\Constant;

class UserInvoice {

  const PENDING = 10000;
  const OPENED = 10001;
  const OVERDUE = 10002; // only used by filter in business web
  const PARTIALLY_PAID = 20001;
  const VERIFIED = 30000;
  const COMPLETED = 50000;
  const MERGED = 50001;
  const COMPLETED_WITH_EXTERNAL_PAYMENT = 50002;
  const CANCELLED = 90000;
  const VOID = 90001;

  const BILLER_ODEO = 1;
  const BILLER_QLUSTER = 2;
  const BILLER_ASG_GLC = 3;
  const BILLER_ASG_GLC_FM = 4;
  const BILLER_ODEO_TEST_HS = 5;
  const BILLER_ASG_SCK = 8;

  const MINIMUM_PARTIAL_PAYMENT_PERCENTAGE = 1;

  const PAID = [
    self::COMPLETED,
    self::COMPLETED_WITH_EXTERNAL_PAYMENT
  ];

  const UNPAID = [
    self::PENDING,
    self::OPENED,
    self::PARTIALLY_PAID
  ];

  const TERMINAL_ENABLED_BILLER = [
    self::BILLER_ASG_GLC,
    self::BILLER_ASG_GLC_FM,
    self::BILLER_ODEO_TEST_HS,
  ];

  const BILLER_GROUP_ASG = [
    self::BILLER_ASG_GLC,
    self::BILLER_ASG_GLC_FM,
    self::BILLER_ASG_SCK,
  ];

  public static function generateInvoiceNumber($billerId) {
    usleep(mt_rand(1, 1000));
    list($usec, $sec) = explode(" ", microtime());

    return sprintf("%02d%08d%04d", $billerId, $sec, ($usec * 10000));
  }

  public static function isCompleted($status) {
    return in_array($status, self::PAID);
  }
}
