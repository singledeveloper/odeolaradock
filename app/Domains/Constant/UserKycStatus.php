<?php

namespace Odeo\Domains\Constant;

class UserKycStatus {

  const PENDING = '10000';
  const NOT_COMPLETE = '10001';
  const WAIT_VERIFY = '30000';
  const REJECTED = '40000';
  const VERIFIED = '50000';
  const CANCELLED = '90000';

  const SKIPPED_OPTIONS = [
    9, // akta_ubah
    15 // skdp
  ];

  public static function getMonitoringIcon($status) {
    if ($status == self::VERIFIED) {
      return 'fa-check-circle';
    }
    else return 'fa-exclamation-triangle';
  }

}
