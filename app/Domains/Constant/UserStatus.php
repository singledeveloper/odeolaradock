<?php

namespace Odeo\Domains\Constant;

class UserStatus {

 const PENDING = 10000;
 const OK = 50000;
 const RESET_PIN = 50001;
 const FORCE_CHANGE_PASSWORD = 50002; // USED IN SELLER CENTER TO FORCE CHANGE PASSWORD
 const FORCE_CHANGE_PIN = 50003; // USED IN SELLER CENTER TO FORCE CHANGE PIN
 const DEACTIVATED = 90000;
 const BANNED = 90001;
 const BANNED_DEVICE_SSAID_REACH_LIMIT = 90002;
 const BANNED_OTP_ATTEMPT_REACH_LIMIT = 90003;
 const BANNED_RECEIVE_TRANSFER_FROM_BANK_SUSPECT = 90004;
 const BANNED_TOPUP_FROM_DIFFERENT_BANK_ACCOUNT = 90005;
 const BANNED_WITHDRAW_RELATED_TO_BANNED_BANK_ACCOUNT = 90006;
 const BANNED_WITHDRAW_FROM_DIFFERENT_BANK_ACCOUNT = 90007;
 const BANNED_WITHDRAW_FROM_SPECIFIC_TOPUP = 90008;
 const BANNED_FRAUD_SUSPECT = 90009;

 const LOGIN_BLOCKED_COUNT = 3;
 const ANDROID_SSAID_LIMIT = 5;

 const INTERNAL_OVER_SETTLEMENT_USER_ID = 812;
 const INTERNAL_FRAUD_USER_ID = 79725;

 const OTP_ATTEMPTS_MAX = 3;

 const REDIS_USER_DATA = 'odeo-core:udata_';

 public static function groupBanned() {
  return [
    self::BANNED, 
    self::BANNED_DEVICE_SSAID_REACH_LIMIT, 
    self::BANNED_OTP_ATTEMPT_REACH_LIMIT, 
    self::BANNED_RECEIVE_TRANSFER_FROM_BANK_SUSPECT, 
    self::BANNED_TOPUP_FROM_DIFFERENT_BANK_ACCOUNT, 
    self::BANNED_WITHDRAW_RELATED_TO_BANNED_BANK_ACCOUNT, 
    self::BANNED_WITHDRAW_FROM_DIFFERENT_BANK_ACCOUNT,
    self::BANNED_WITHDRAW_FROM_SPECIFIC_TOPUP,
    self::BANNED_FRAUD_SUSPECT
  ];
 }
}
