<?php

namespace Odeo\Domains\Constant;


class Vendor {

  const ODEO_INTERNAL = 1;
  const ODEO = 2;
  const SEPULSA = 3;
  const TRAVELOKA = 4;
  const TIKETCOM = 5;

}