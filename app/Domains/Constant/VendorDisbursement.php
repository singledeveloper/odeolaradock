<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/3/17
 * Time: 11:12 PM
 */

namespace Odeo\Domains\Constant;

class VendorDisbursement {
  const BCA = 1;
  const FLIP = 2;
  const ARTAJASA = 3;
  const CIMB = 4;
  const BNI = 5;
  const BRI = 6;
  const PERMATA = 7;
  const REDIG = 8;
  const ODEO = 9;
  const MANDIRI = 10;
}
