<?php

namespace Odeo\Domains\Constant;

class WebhookNotifyType {
  
  const VA_INQUIRY = 'va_inquiry';
	const VA_PAYMENT = 'va_payment';
	const PAYMENT_STATUS = 'payment_status';
  
}
