<?php

namespace Odeo\Domains\Core;

use Odeo\Domains\Constant\Header;
use Validator;

trait IO {

  private $STATUS_SUCCESS = "SUCCESS";
  private $STATUS_BAD_REQUEST = "BAD_REQUEST";
  private $STATUS_INVALID_CODE = "INVALID_CODE";
  private $STATUS_NOT_VERIFIED = "NOT_VERIFIED";
  private $STATUS_INVALID_CREDENTIAL = "INVALID_CREDENTIAL";
  private $STATUS_EXPIRED = "EXPIRED";
  private $STATUS_UNAUTHORIZED = "UNAUTHORIZED";
  private $STATUS_NOT_FOUND = "NOT_FOUND";
  private $STATUS_BAD_EXPIRED = "EXPIRED";

  public function getRequestData($additionalData = []) {
    $request = app()->make(\Illuminate\Http\Request::class);
    if ($data = $request->json('data')) ;
    else {
      $data = [];
      foreach ($request->input() as $key => $item) $data[$key] = $item;
      foreach ($request->file() as $key => $file) $data[$key] = $file;
    }
    if ($request->input('auth')) $data['auth'] = $request->input('auth');
    if (!empty($additionalData)) $data = array_merge($data, $additionalData);

    if (isset($data['phone_number'])) $data['phone_number'] = purifyTelephone($data['phone_number']);
    if (isset($data['telephone'])) $data['telephone'] = purifyTelephone($data['telephone']);
    if (isset($data['provider_id']) && !isset($data['service_id']))
      $data['service_id'] = $data['provider_id'];
    return $data;
  }

  public function validateData($rules, $additionalData = []) {
    $data = $this->getRequestData($additionalData);
    $validator = Validator::make($data, $rules);

    if ($validator->fails()) {
      $errors = $validator->errors()->all();
      return [false, $this->buildErrorsResponse($errors), $errors];
    } else {
      return [true, $data, []];
    }
  }

  public function parseQuerySearch(&$data) {
    if (isset($data['search']) && is_string($data['search']) && $data['search'] != "" && !ctype_space($data['search'])) {
      $query = [];
      foreach (explode(',', $data['search']) as $q) {
        $s = explode(":", $q);
        if (count($s) == 2 && $s[1]) $query[$s[0]] = $s[1];
      }
      $data['search'] = $query;
    }
  }

  public function buildResponse($status_code = 200, $data = null, $status = "", $message = "", $errorCode = null) {
    $request = app()->make(\Illuminate\Http\Request::class);
    $response = [
      'status' => ($status != "") ? $status : $this->STATUS_SUCCESS,
      'data' => $data,
      'message' => $message,
    ];

    $updated = versioning($request, Header::ANDROID_HEADER_VER, env('ANDROID_LATEST_VERSION'));

    return response($status_code != 204 ? json_encode($response) : '', $status_code)->header("X-Odeo-Client-Updated", $updated)->header('Content-Type', 'application/json');

  }

  public function buildErrorsResponse($error = null, $code = 400, $message = "", $errorStatus = null) {
    if ($errorStatus) {
      $status = $errorStatus;
    } else if($code == 401) {
      $status = $this->STATUS_UNAUTHORIZED;
    } else if ($code == 404) {
      $status = $this->STATUS_NOT_FOUND;
      $error = "URL not found";
    } else {
      $status = $this->STATUS_BAD_REQUEST;
    }

    if (!$error) {
      clog("400_without_error_message", json_encode(debug_backtrace()));
      $error = "Something happened with our server.";
    }

    if ($error === trans('errors.server_query_timeout')) {
      $error = '';
    }

    if (!is_array($error)) $error = [$error];

    return $this->buildResponse($code, ["errors" => $error], $status, $message);
  }

  public function buildSuccessResponse($data = [], $code = 200, $message = "") {
    return $this->buildResponse($code, $data, $this->STATUS_SUCCESS, $message);
  }

  public function dummyResponse($response) {
    return response(")]}',\n" . $response);
  }
}
