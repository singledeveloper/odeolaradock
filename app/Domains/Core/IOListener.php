<?php

namespace Odeo\Domains\Core;

interface IOListener {

  function buildSuccessResponse($data, $statusCode = 200, $headers = []);

  function buildErrorsResponse($error = null, $statusCode = 400, $message = "", $errorStatus = null);

  function validateData($rules, $additionalData = []);

  function getRequestData($additionalData = []);
}
