<?php

namespace Odeo\Domains\Core;


trait MassUpdater {

  private function populateTempTable($data, $columnType) {
    return array_map(function ($item) use ($columnType) {
      $d = $this->transformValue($item, $columnType);
      $s = join(',', $d);
      return "($s)";
    }, $data);
  }

  private function toDatabaseValue($col, $val) {
    switch ($col) {
      case 'text':
      case 'character':
      case 'character varying':
        return is_null($val) ? "NULL" : "'" . str_replace("'", "''", $val) . "'";
      case 'timestamp without time zone':
        return $val ? "timestamp '$val'" : "NULL::timestamp";
      case 'boolean':
        return $val ? "true" : "false";
      default:
        return is_null($val) ? "NULL" : $val;
    }
  }

  private function transformValue($data, $columnType) {
    return array_map(function ($key, $val) use ($columnType) {
      $col = $columnType[$key];
      return $this->toDatabaseValue($col->data_type, $val);
    }, array_keys($data), $data);
  }

  private function getColumn($data) {
    return array_keys($data[0]);
  }

  private function getUpdateSets($data, $matchKey) {
    $columns = $this->getColumn($data);
    $columns = $this->removeMatchedKey($columns, $matchKey);
    return array_map(function ($cul) {
      return "$cul = emp.$cul";
    }, $columns);
  }

  private function removeMatchedKey($col, $matchedKey) {
    return array_filter($col, function ($item) use ($matchedKey) {
      return $item != $matchedKey;
    });
  }

  private function implode($arr) {
    return implode(',', $arr);
  }

  private function getColumnType($tableName) {
    $data = \DB::select(\DB::raw("select column_name, data_type from information_schema.columns where table_name = '$tableName'"));
    return collect($data)->keyBy('column_name')->all();
  }

  public function massUpdate($model, $data = [], $matchKey = 'id') {
    $tableName = $model->getTable();
    $columnType = $this->getColumnType($tableName);

    $res = $this->populateTempTable($data, $columnType);
    $res = $this->implode($res);

    $resColumn = $this->getColumn($data);
    $resColumn = $this->implode($resColumn);

    $sets = $this->getUpdateSets($data, $matchKey);
    $sets = $this->implode($sets);

    \DB::statement("with emp as (
        select * from 
        (values $res) as d($resColumn)
      ) 
      update $tableName as t set $sets 
      from $tableName join emp on emp.$matchKey = $tableName.$matchKey
      where t.$matchKey = $tableName.$matchKey
    ");
  }


}
