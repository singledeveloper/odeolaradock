<?php

namespace Odeo\Domains\Core;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Redis;
use Odeo\Jobs\Job;

class Pipeline implements PipelineListener {

  private $tasks = [];
  private $transaction = false;
  private $isTaskCurrentlySkip = false;
  private $isTaskTransaction = false;
  public $isNeedResponse = true;
  private $jobs = [];

  public $statusCode = 200;
  public $errorMessage = null;
  public $data = null;
  public $errorStatus = null;
  public $responseMessage = [];
  private $result = [];
  private $metadata = [];

  private $currentIndex = 0;
  private $currentLength = 0;
  private $currentAddedSubTask = 0;
  private $currentSubTaskIndex = 0;

  private $antiSpamKey, $antiSpamExpire;

  private function rollback() {
    if ($this->isTaskTransaction) \DB::rollback();
    if ($this->transaction) \DB::rollback();
    $this->errorMessage = $this->result;
    $this->data = "";
    return false;
  }

  private function commit() {
    if ($this->transaction) \DB::commit();
    if (is_array($this->result)) {
      $this->data = array_merge($this->result, $this->metadata);
    } else {
      $this->data = $this->result;
    }
    foreach ($this->jobs as $item) {
      dispatch($item);
    }
    return true;
  }

  public function getNew() {
    return new Pipeline;
  }

  public function add(Task $task) {
    $this->tasks[] = $task;
  }

  public function addNext(Task $task) {
    if ($this->currentSubTaskIndex != $this->currentIndex || ($this->currentSubTaskIndex == 0 && $this->currentAddedSubTask == 0)) {
      $this->currentSubTaskIndex = $this->currentIndex;
      $this->currentAddedSubTask = $this->currentIndex + 1;
    }
    array_splice($this->tasks, $this->currentAddedSubTask, 0, [$task]);
    ++$this->currentAddedSubTask;
  }

  public function enableTransaction() {
    $this->transaction = true;
  }

  public function disableResponse() {
    $this->isNeedResponse = false;
  }

  public function enableAntiSpam($uniqueID, $expire = 60) {
    $key = debug_backtrace()[1]['class'] . '_' . debug_backtrace()[1]['function'] . "_$uniqueID";
    $this->antiSpamKey = $key;
    $this->antiSpamExpire = $expire;
  }

  public function execute() {
    if (!$this->antiSpamKey) {
      return $this->executeTask(func_get_args());
    }

    $redis = Redis::connection();

    if (!$redis->setnx($this->antiSpamKey, 1)) {
      $this->statusCode = 400;
      $this->result = "{$this->antiSpamKey}: spam detected";
      app('sentry')->captureException(new \Exception($this->result));
      return $this->rollback();
    }

    $redis->expire($this->antiSpamKey, $this->antiSpamExpire);
    $res = $this->executeTask(func_get_args());
    $redis->del($this->antiSpamKey);

    return $res;
  }

  private function executeTask($argList) {
    if ($this->transaction) \DB::beginTransaction();
    $this->currentLength = sizeof($this->tasks);

    while ($this->currentIndex != sizeof($this->tasks)) {
      $item = $this->tasks[$this->currentIndex];

      if (!$this->isTaskCurrentlySkip) {
        if ($this->data == null) $this->data = isset($argList[0]) ? $argList[0] : [];
        try {
          if ($item->isStartTransaction()) {
            \DB::beginTransaction();
            $this->isTaskTransaction = true;
          }
          $class = app()->make($item->getClass());
          $functions = explode('.', $item->getFunction());
          $reference = $class;
          foreach ($functions as $func) {
            /*if (app()->environment() != 'production') {
              $temp = []; foreach ($this->tasks as $o) $temp[] = $o->getFunction();
              \Log::notice(implode(",", $temp));
            }*/
            $reference = call_user_func_array([$reference, $func], array_merge([$this, array_merge($this->data, $this->result, $item->getParams())], $argList));
          }

          if ($this->fail()) {
            if ($item->isStartSkip()) $this->isTaskCurrentlySkip = true;
            else return $this->rollback();
          } else if ($item->getCallback()) {
            $callback = $item->getCallback();
            $this->result = $callback($this->result);
          }
          if ($item->isEndTransaction()) {
            \DB::commit();
            $this->isTaskTransaction = false;
          }
        } catch (\Exception $e) {
          if (app()->environment() != 'production') {
            $this->rollback();
            if ((isset($this->data['auth']) && isset($this->data['auth']['type']) && $this->data['auth']['type'] == 'affiliate')) ;
            else throw $e;
          } else {
            $this->statusCode = 400;
            $this->result = $e->getMessage();
            $data = $this->data;
            unset($data['auth']);

            $isQueryTimeout = $this->isQueryTimeout($e);

            if ($isQueryTimeout) {
              $this->result = trans('errors.server_query_timeout');
            }

            !$isQueryTimeout &&
            app('sentry')->captureException($e, [
              'extra' => [
                'request_payload' => serialize(file_get_contents('php://input')),
                'last_pipeline_merged_data' => json_encode($data),
                'last_pipeline_response_status_code' => $this->statusCode,
                'last_pipeline_response_result' => json_encode($this->result),
                'last_pipeline_handled_class' => $item->getClass(),
                'last_pipeline_called_function' => $item->getFunction()
              ]
            ]);
          }
          return $this->rollback();
        }
      }
      if ($item->isEndSkip()) $this->isTaskCurrentlySkip = false;
      ++$this->currentIndex;
    }
    return $this->commit();
  }

  public function pushQueue(Job $job) {
    $this->jobs[] = $job;
  }

  public function responseWithErrorStatus($errorStatus, $resultData = [], $statusCode = 400) {
    $this->response($statusCode, $resultData, false, false, $errorStatus);
  }

  public function response($statusCode, $resultData = [], $overwriteResult = false, $overwriteStatus = false, $errorStatus = null) {
    if ($this->currentIndex > 0 && $statusCode == 204) ;
    else $this->statusCode = $statusCode;

    if ($overwriteStatus) {
      $this->statusCode = $statusCode;
    }

    if ($errorStatus) {
      $this->errorStatus = $errorStatus;
    }

    if (is_array($resultData) && !$overwriteResult) {
      if (isset($resultData["metadata"])) {
        if (empty($this->metadata)) {
          $this->metadata = ["metadata" => $resultData["metadata"]];
          $this->data["is_paginate"] = "false";
        }
        unset($resultData["metadata"]);
      }
      if (app()->environment() != 'production') {
        $diff = array_intersect_key($resultData, $this->result);
        if (count($diff) > 0) \Log::warning("PIPELINE_OVERWRITTEN_WARNING: " . implode(", ", array_keys($diff)));
      }
      $this->result = array_merge($this->result, $resultData);
    } else $this->result = $resultData;
  }

  public function fail() {
    $fail = ($this->statusCode != 200 && $this->statusCode != 201 && $this->statusCode != 204);
    if ($fail && $this->isTaskTransaction) {
      $this->isTaskTransaction = false;
      \DB::rollback();
    }
    return $fail;
  }

  public function success() {
    return !$this->fail();
  }

  public function replaceData($data) {
    $this->data = $data;
  }

  public function appendData($data = []) {
    $this->data = array_merge($this->data, $data);
  }

  public function clear() {
    return new Pipeline;
  }

  public function appendResponseMessage($message, $param = '') {
    if ($param == '') $this->responseMessage[] = $message;
    else $this->responseMessage[$param] = $message;
  }

  public function getResponseMessage($param = '') {
    if ($param == '') return implode(',', $this->responseMessage);
    else return isset($this->responseMessage[$param]) ? $this->responseMessage[$param] : '';
  }

  public function countTask() {
    return count($this->tasks);
  }

  private function isQueryTimeout($e) {
    return $e instanceof QueryException
      && str_contains($e->getMessage(), 'Query canceled');
  }

}
