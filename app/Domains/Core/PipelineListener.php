<?php

namespace Odeo\Domains\Core;

use Odeo\Jobs\Job;

interface PipelineListener {

  public function response($statusCode, $resultData = [], $overwriteResult = false, $overwriteStatus = false);

  public function responseWithErrorStatus($errorStatus, $resultData = [], $statusCode = 400);

  public function pushQueue(Job $job);

  public function addNext(Task $task);

  public function replaceData($data);

  public function appendData($data = []);

  public function appendResponseMessage($message, $param = '');
}
