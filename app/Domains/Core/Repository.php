<?php

namespace Odeo\Domains\Core;

use Illuminate\Database\Eloquent\Model;

abstract class Repository {

  use MassUpdater;

  protected $model;

  protected $filters = [];

  protected $count = 0;

  protected $extends = [];

  protected $extendBase = "";

  protected $cache;

  protected $simplePaginate = false;

  public function deleteById($id, $columnName = 'id') {
    if (is_array($id)) {
      $this->model->whereIn($columnName, $id)->delete();
    } else {
      $this->model->where($columnName, $id)->delete();
    }
  }

  public function count() {
    return $this->model->count();
  }

  public function runCache($tag, $key, $minutes, $action) {

    if (app()->environment() == 'testing') {
      return $action();
    }

    if (!$this->cache) {
      $this->cache = app()->make(\Illuminate\Contracts\Cache\Repository::class);
    }

    $tag = (array)$tag;

    return $this->cache->tags($tag)->remember($key, $minutes, $action);

  }

  public function getModel() {
    return $this->model;
  }

  public function getCloneModel() {
    return clone $this->model;
  }

  public function getFilters() {
    return $this->filters;
  }

  public function getTableName() {
    return $this->model->getTable();
  }

  public function getPagination() {
    if (sizeof($this->filters) == 0 || (isset($this->filters["is_paginate"]) && !$this->filters["is_paginate"])) return [];

    return [
      "metadata" => [
        "resultset" => [
          "limit" => $this->filters["limit"],
          "offset" => $this->filters["offset"],
          "count" => $this->count
        ]
      ]
    ];
  }

  public function setModel($model) {
    $this->model = $model;
  }

  public function setSimplePaginate($simplePaginate) {
    $this->simplePaginate = $simplePaginate;
  }

  public function getAll($fields = ['*']) {
    return $this->model->all($fields);
  }

  public function findById($id, $fields = ['*']) {
    return $this->model->find($id, $fields);
  }

  public function findTrashedById($id, $fields = ['*']) {
    return $this->model->withTrashed()->find($id, $fields);
  }

  public function findAllByAttributes($column, $value = null, $fields = ['*']) {
    return $this->findByAttribute($column, $value)->get($fields);
  }

  public function findByAttributes($column, $value = null, $fields = ['*']) {
    return $this->findByAttribute($column, $value)->first($fields);
  }

  public function updateBulk($data, $matchedKey = 'id') {
    $this->massUpdate($this->getModel(), $data, $matchedKey);
  }

  private function findByAttribute($column, $value) {
    if (is_string($column)) {
      if (is_null($value)) return $this->model;
      else {
        return $this->model->where($column, $value);
      }
    } else if (is_array($column)) {
      $query = $this->model;
      foreach ($column as $key => $value) {
        $query = $query->where($key, $value);
      }
      return $query;
    }
  }

  public function getNew($attributes = []) {

    return $this->model->newInstance($attributes);
  }

  public function findByIdOrNew($id) {
    if ($instance = $this->findById($id)) {
      return $instance;
    }
    return $this->getNew();
  }

  public function save($data) {
    if ($data instanceOf Model) return $this->storeEloquentModel($data);
    elseif (is_array($data)) return $this->storeArray($data);
    throw new \Exception();
  }

  public function saveBulk($data = []) {
    return $this->model->insert($data);
  }

  public function delete($model) {
    return $model->delete();
  }

  public function truncate() {
    return $this->model->query()->delete();
  }

  protected function storeEloquentModel($model) {
    if ($model->getDirty()) return $model->save();
    else return $model->touch();
  }

  protected function storeArray($data) {
    $model = $this->getNew($data);
    return $this->storeEloquentModel($model);
  }

  public function hasAttribute($column) {
    return \Schema::hasColumn($this->model->getTable(), $column);
  }

  public function saveBulkReturningId($values, $returning = ['id']) {
    $tableName = $this->model->getTable();
    $columns = implode(',', array_keys($values[0]));
    $values = $this->normalizeValue($values);
    $returning = implode(',', $returning);

    return \DB::select(\DB::raw("insert into $tableName ($columns) values $values RETURNING $returning "));
  }

  private function normalizeValue($data) {

    $data = array_map(function ($item) {

      $items = array_map(function ($val) {
        return $this->normalizeValueType($val);
      }, $item);

      $items = implode(',', $items);

      return "($items)";

    }, $data);

    return implode(',', $data);

  }

  private function normalizeValueType($val) {

    if (is_string($val) || is_object($val)) {
      return "'$val'";
    } else if (is_null($val)) {
      return 'NULL';
    }

    return $val;
  }


  public function normalizeFilters($data) {
    if (!isset($data["limit"])) $data["limit"] = 20;
    if (!isset($data["offset"])) $data["offset"] = 0;

    if (!isset($data["is_paginate"])) $data["is_paginate"] = true;

    if (!isset($data["fields"])) $data["fields"] = ['*'];
    else if (!is_array($data["fields"])) $data["fields"] = explode(',', $data["fields"]);

    $transforms = isset($this->model->additionalAttributes) ? $this->model->additionalAttributes : [];
    $data["transform_fields"] = [];
    foreach ($data["fields"] as $key => $item) {
      if (in_array($item, $transforms)) {
        unset($data["fields"][$key]);
        $data["transform_fields"][] = $item;
      }
    }
    $data["expand"] = isset($data["expand"]) ? explode(",", $data["expand"]) : [];
    $this->filters = $data;
  }

  public function addFilter($key, $value) {
    $this->filters[$key] = $value;
  }

  public function hasField($fieldName) {
    $fields = isset($this->filters["fields"]) ? $this->filters["fields"] : [];
    $transformFields = isset($this->filters["transform_fields"]) ? $this->filters["transform_fields"] : [];
    return in_array('*', $fields) || in_array($fieldName, $fields) || in_array($fieldName, $transformFields);
  }

  public function hasExpand($expandName) {
    $expand = isset($this->filters["expand"]) ? $this->filters["expand"] : [];
    if (!is_array($expand)) $expand = explode(",", $expand);
    return in_array('ext', $expand) || in_array($expandName, $expand);
  }

  public function getResult($query) {
    if (sizeof($this->filters) == 0) return $query->get();

    if (isset($this->filters["is_paginate"]) && !$this->filters["is_paginate"]) return $query->get($this->filters["fields"]);

    if ($this->filters["limit"] == 0) {
      $result = $query->get($this->filters["fields"]);
      $this->count = count($result);
    } else {
      $currentPage = ($this->filters["offset"] + $this->filters["limit"]) / $this->filters["limit"];
      \Illuminate\Pagination\Paginator::currentPageResolver(function () use ($currentPage) {
        return $currentPage;
      });

      if ($this->simplePaginate) {
        $result = $query->simplePaginate($this->filters["limit"], $this->filters["fields"]);
        $this->count = $this->filters["offset"] + $this->filters["limit"];

        if (count($result) > 0) {
          if ($this->filters["limit"] > count($result)) $this->count = $this->filters["offset"] + count($result);
          else $this->count += 100;
        }
      } else {
        $result = $query->paginate($this->filters["limit"], $this->filters["fields"]);
        $this->count = $result->total();
      }
    }
    return $result;
  }

  public function beginExtend($data, $base) {
    $ids = [];
    if (isset($data[0]) && isset($data[0][$base])) {
      foreach ($data as $item) {
        $ids[] = $item[$base];
      }
      $this->extendBase = $base;
      return $ids;
    } else if (isset($data[$base])) {
      $this->extendBase = $base;
      return [$data[$base]];
    }

    return false;
  }

  public function addExtend($param, $result) {
    if ($this->extendBase != "")
      $this->extends[$param] = $result;
  }

  public function finalizeExtend($data) {
    if ($this->extendBase != "") {
      if (isset($data[$this->extendBase])) {
        foreach ($this->extends as $key => $output) {
          if (isset($this->extends[$key][$data[$this->extendBase]])) {
            $data[$key] = $this->extends[$key][$data[$this->extendBase]];
          }
        }
      } else {
        foreach ($data as &$item) {
          foreach ($this->extends as $key => $output) {
            if (isset($this->extends[$key][$item[$this->extendBase]]))
              $item[$key] = $this->extends[$key][$item[$this->extendBase]];
          }
        }
      }
    }

    return $data;
  }
}
