<?php

namespace Odeo\Domains\Core;

class Task {
    
    private $callback = false;
    private $taskClass;
    private $taskFunction;
    private $params;
    private $isBeginner = false;
    private $isCommitter = false;
    private $isSkipStart = false;
    private $isSkipEnd = false;
    
    public function __construct($class, $function, $params = []) {
        $this->taskClass = $class;
        $this->taskFunction = $function;
        $this->params = $params;
    }
    
    public function setCallback($callback) {
        $this->callback = $callback;
    }
    
    public function getCallback() {
        return $this->callback;
    }
    
    public function getClass() {
        return $this->taskClass;
    }
    
    public function getFunction() {
        return $this->taskFunction;
    }
    
    public function getParams() {
        return $this->params;
    }
    
    public function singleTransaction() {
        $this->isBeginner = true;
        $this->isCommitter = true;
    }
    
    public function startTransaction() {
        $this->isBeginner = true;
    }
    
    public function endTransaction() {
        $this->isCommitter = true;
    }
    
    public function isStartTransaction() {
        return $this->isBeginner;
    }
    
    public function isEndTransaction() {
        return $this->isCommitter;
    }
    
    public function singleSkip() {
        $this->isSkipStart = true;
        $this->isSkipEnd = true;
    }
    
    public function startSkip() {
        $this->isSkipStart = true;
    }
    
    public function endSkip() {
        $this->isSkipEnd = true;
    }
    
    public function isStartSkip() {
        return $this->isSkipStart;
    }
    
    public function isEndSkip() {
        return $this->isSkipEnd;
    }
}