<?php


namespace Odeo\Domains\DDLending\Jobs;


use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Vendor\OdeoV2\Helper\ApiManager;
use Odeo\Jobs\Job;

class NotifyLoan extends Job {

  use SerializesModels;

  private $id;
  private $redis;

  public function __construct($id) {
    parent::__construct();
    $this->id = $id;
  }

  public function handle() {
    $this->redis = Redis::connection();
    $apiManager = app()->make(ApiManager::class);
    $response = $apiManager->request('POST', "/dd-lending/loans/{$this->id}/notify", null);

    if ($response['status_code'] == 200) {
      return;
    }

    if (!$this->canRetry()) {
      return;
    }

    dispatch(new NotifyLoan($this->id));
  }

  private function canRetry() {
    $retries = $this->redis->hincrby('odeo_core:ddlending_notify_loan_retries', $this->id, 1);
    return $retries < 3;
  }

}