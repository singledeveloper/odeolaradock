<?php

namespace Odeo\Domains\DDLending;

use Odeo\Domains\Constant\LoanStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\DDLending\Repository\LoanRepository;
use Odeo\Domains\Inventory\Helper\MarginFormatter;

class LoanPaymentValidator {

  private $loanRepo;
  private $marginFormatter;

  public function __construct() {
    $this->loanRepo = app()->make(LoanRepository::class);
    $this->marginFormatter = app()->make(MarginFormatter::class);
  }

  public function validate(PipelineListener $listener, $data) {
    $loan = $this->loanRepo->findById($data['item_id']);

    if (!$loan || $loan->status != LoanStatus::ON_PROGRESS_DISBURSED) {
      return $listener->response(400, 'Tagihan tidak ditemukan atau sudah dibayarkan');
    }

    $formatted = $this->marginFormatter->formatMargin($loan->payment_amount, $data);
    $formatted['merchant_price'] = 0;
    $formatted['sale_price'] = $formatted['base_price'];

    return $listener->response(200, array_merge($formatted, [
      'name' => $loan->borrower_name,
      'item_detail' => [
        'loan_id' => $loan->loan_id,
      ],
    ]));
  }

}