<?php


namespace Odeo\Domains\DDLending\Model;


use Odeo\Domains\Core\Entity;

class DDLendingUser extends Entity {

  protected $table = 'ddlending_users';

}