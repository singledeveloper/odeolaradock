<?php


namespace Odeo\Domains\DDLending\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\DDLending\Model\DDLendingUser;

class DDLendingUserRepository extends Repository {

  public function __construct(DDLendingUser $u) {
    $this->model = $u;
  }


  public function findByUserId($userId) {
    return $this->model->where('user_id', $userId)->first();
  }

}