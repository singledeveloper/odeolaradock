<?php


namespace Odeo\Domains\DDLending\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\DDLending\Model\Loan;

class LoanRepository extends Repository {

  public function __construct(Loan $loan) {
    $this->model = $loan;
  }

  public function findByPaymentOrderId($paymentOrderId) {
    return $this->model->where('payment_order_id', $paymentOrderId)->first();
  }
}