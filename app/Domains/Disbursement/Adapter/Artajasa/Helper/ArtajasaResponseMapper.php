<?php

namespace Odeo\Domains\Disbursement\Adapter\Artajasa\Helper;

use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\DisbursementStatus;


class ArtajasaResponseMapper {

  public function map($responseCode) {
    switch ($responseCode) {
      case '14': // Invalid card number (no such number)
      case '76': // Invalid to account
      case '77': // Invalid from account
        return DisbursementStatus::FAILED_WRONG_ACCOUNT_NUMBER;

      case '78': // Account is closed
        return DisbursementStatus::FAILED_CLOSED_BANK_ACCOUNT;

      case '01': // Refer to card issuer
      case '03': // Invalid merchant
//      case '05': // Do not honor
      case '12': // Invalid transaction
      case '13': // Invalid amount.
      case '15': // No such issuer
      case '20': // Invalid response
      case '30': // Format error
      case '31': // Bank not supported by switch
      case '39': // No credit account
      case '40': // Requested function not supported
      case '52': // No chequing account
      case '53': // No savings account
      case '54': // Expired card
      case '55': // Invalid PIN
      case '57': // Transaction not permitted to cardholder
      case '58': // Transaction not permitted to terminal
      case '61': // Exceeds withdrawal amount limit
      case '62': // Restricted card
      case '63': // Security violation
      case '65': // Exceeds withdrawal frequency limit
      case '75': // Allowable number of PIN tries exceeded
      case '91': // Issuer, Destination or switch is inoperative
      case '92': // Unable to route transaction
      case 'NF': // Transaction has not recorded on Remittance gateway.
        return DisbursementStatus::FAILED_BANK_REJECTION;


      case '89': // Link to Host down
      case '96': // System malfunction / system error
//      case 'EE': // General error. Details of the error is in the description of the response.
      case 'LD': // Link problem between Gateway and ATM Bersama Network.
        return DisbursementStatus::FAILED_VENDOR_DOWN;

      case '94': // Duplicate transmission / request
        return DisbursementStatus::FAILED_DUPLICATE_REQUEST;

      case '51': // Insufficient funds / over credit limit
      case '68': // Response received too late
      case 'TO': // Response time-out from ATM Bersama Network.
      case 'SG': // Signature error code
      case 'IF': // Insufficient deposit
      case ArtajasaDisbursement::RESPONSE_CODE_RTO: // Custom: RTO to AJ
      case ArtajasaDisbursement::RESPONSE_CODE_EXCEPTION: // Custom: Exception when doing transfer
        return DisbursementStatus::SUSPECT;

      case '00':
        return DisbursementStatus::COMPLETED;

      default:
        return DisbursementStatus::SUSPECT;
    }
  }

}