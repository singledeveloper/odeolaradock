<?php

namespace Odeo\Domains\Disbursement\Adapter\Artajasa;


use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaDisbursementRepository;

class Reserve {

  private $artajasaDisbursementRepo;

  public function __construct() {
    $this->artajasaDisbursementRepo = app()->make(DisbursementArtajasaDisbursementRepository::class);
  }

  public function reserve(PipelineListener $listener, $data) {
    $artajasa = $this->artajasaDisbursementRepo->getNew();
    $artajasa->disbursement_type = Disbursement::DISBURSEMENT;
    $artajasa->disbursement_reference_id = $data['disbursement_id'];
    $artajasa->status = ArtajasaDisbursement::PENDING;
    $this->artajasaDisbursementRepo->save($artajasa);
    return $listener->response(200, [
      'disbursement_vendor_reference_id' => $artajasa->id,
      'disbursement_vendor_updated_at' => strtotime($artajasa->updated_at),
    ]);
  }

}