<?php

namespace Odeo\Domains\Disbursement\Adapter\Bni;


use Odeo\Domains\Constant\BniDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Bni\Repository\DisbursementBniDisbursementRepository;

class Reserve {

  private $bniDisbursementRepo;

  public function __construct() {
    $this->bniDisbursementRepo = app()->make(DisbursementBniDisbursementRepository::class);
  }

  public function reserve(PipelineListener $listener, $data) {
    $bni = $this->bniDisbursementRepo->getNew();
    $bni->disbursement_type = Disbursement::DISBURSEMENT;
    $bni->disbursement_reference_id = $data['disbursement_id'];
    $bni->status = BniDisbursement::PENDING;
    $this->bniDisbursementRepo->save($bni);

    return $listener->response(200, [
      'disbursement_vendor_reference_id' => $bni->id,
      'disbursement_vendor_updated_at' => strtotime($bni->updated_at),
    ]);

  }

}