<?php

namespace Odeo\Domains\Disbursement\Adapter\Cimb\Helper;

use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\CimbApi;
use Odeo\Domains\Constant\CimbDisbursement;
use Odeo\Domains\Constant\DisbursementStatus;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Adapter\Helper\DisbursementInquirer;
use Odeo\Domains\Vendor\Cimb\Transfer;
use Odeo\Domains\Vendor\Cimb\Repository\DisbursementCimbDisbursementRepository;

class ExecuteCimbDisbursement {

  private $cimbDisbursementRepo, $disbursementInquirer;

  public function __construct() {
    $this->cimbDisbursementRepo = app()->make(DisbursementCimbDisbursementRepository::class);
    $this->disbursementInquirer = app()->make(DisbursementInquirer::class);
  }

  public function exec($disbursement) {
    $inquiryStatus = $this->disbursementInquirer->inquire($disbursement);
    if ($inquiryStatus != BankAccountInquiryStatus::COMPLETED) {
      $cimb = $this->cimbDisbursementRepo->findById($disbursement->vendor_disbursement_reference_id);
      $cimb->status = CimbDisbursement::STATUS_FAIL;
      $cimb->status_code = CimbDisbursement::STATUS_CODE_INQUIRY_FAILED;
      $this->cimbDisbursementRepo->save($cimb);

      return [
        'disbursement_id' => $disbursement->id,
        'status' => BankAccountInquiryStatus::toDisbursementStatus($inquiryStatus),
        'disbursement_vendor' => VendorDisbursement::CIMB,
        'disbursement_vendor_reference_id' => $cimb->id,
        'disbursement_vendor_updated_at' => strtotime($cimb->updated_at),
      ];
    }

    $requestId = CimbApi::API_DISBUSEMENT_PREFIX . $disbursement->id;

    $pipeline = new Pipeline;
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'amount' => $disbursement->amount,
      'memo' => $disbursement->description,
      'transaction_key' => 'DISB2#' . $disbursement->id,
      'account_number' => $disbursement->account_number,
      'account_name' => $disbursement->account_name,
      'request_id' => $requestId,
      'cimb_disbursement_id' => $disbursement->vendor_disbursement_reference_id,
    ]));

    $pipeline->execute();

    $cimb = $this->cimbDisbursementRepo->findById($disbursement->vendor_disbursement_reference_id);
    return [
      'disbursement_id' => $disbursement->id,
      'status' => intval($this->getDisbursementStatus($pipeline)),
      'disbursement_vendor' => VendorDisbursement::CIMB,
      'disbursement_vendor_reference_id' => $cimb->id,
      'disbursement_vendor_updated_at' => strtotime($cimb->updated_at),
    ];
  }

  private function getDisbursementStatus(Pipeline $pipeline) {
    if ($pipeline->success()) {
      return DisbursementStatus::COMPLETED;
    }

    switch ($pipeline->errorMessage['response_code'] ?? null) {
      case '001':
        return DisbursementStatus::FAILED_WRONG_ACCOUNT_NUMBER;

      case '037':
      case '045':
      case '22':
      case '28':
        return DisbursementStatus::FAILED_BANK_REJECTION;

      default:
        return DisbursementStatus::SUSPECT;
    }
  }
}
