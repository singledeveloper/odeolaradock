<?php

namespace Odeo\Domains\Disbursement\Adapter\Cimb;


use Odeo\Domains\Constant\CimbDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Cimb\Repository\DisbursementCimbDisbursementRepository;

class Reserve {

  private $cimbDisbursementRepo;

  public function __construct() {
    $this->cimbDisbursementRepo = app()->make(DisbursementCimbDisbursementRepository::class);
  }

  public function reserve(PipelineListener $listener, $data) {
    $cimb = $this->cimbDisbursementRepo->getNew();
    $cimb->disbursement_type = Disbursement::DISBURSEMENT;
    $cimb->disbursement_reference_id = $data['disbursement_id'];
    $cimb->status = CimbDisbursement::STATUS_PENDING;
    $this->cimbDisbursementRepo->save($cimb);

    return $listener->response(200, [
      'disbursement_vendor_reference_id' => $cimb->id,
      'disbursement_vendor_updated_at' => strtotime($cimb->updated_at),
    ]);

  }

}