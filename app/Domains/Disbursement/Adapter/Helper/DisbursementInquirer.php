<?php


namespace Odeo\Domains\Disbursement\Adapter\Helper;


use Odeo\Domains\Account\Helper\BankAccountInquirer;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;

class DisbursementInquirer {

  private $disbursementApiUserRepo, $bankAccountInquirer;

  public function __construct() {
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->bankAccountInquirer = app()->make(BankAccountInquirer::class);
  }

  public function inquire($disbursement) {
    try {
      $apiUser = $this->disbursementApiUserRepo->findByUserId($disbursement->user_id);

      list(, $inquiryStatus) = $this->bankAccountInquirer
        ->inquire($disbursement->bank_id, $disbursement->account_number, $disbursement->user_id, [
          'timeout' => 50,
          'sender_prefix' => $apiUser->alias,
          'customer_name' => $disbursement->customer_name,
          'reference_id' => $disbursement->id,
          'reference_type' => ArtajasaDisbursement::INQUIRY_REFERENCE_TYPE_DISBURSEMENT,
        ]);

      return $inquiryStatus;
    } catch (\Exception $e) {
      \Log::error($e);
      return BankAccountInquiryStatus::FAILED;
    }
  }
}