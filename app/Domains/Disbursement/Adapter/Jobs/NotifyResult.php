<?php

namespace Odeo\Domains\Disbursement\Adapter\Jobs;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\DisbursementStatus;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Adapter\Artajasa\Helper\ExecuteArtajasaDisbursement;
use Odeo\Domains\Disbursement\Repository\DisbursementRepository;
use Odeo\Domains\Vendor\OdeoV2\Helper\ApiManager;
use Odeo\Jobs\Job;

class NotifyResult extends Job implements ShouldQueue {

  private $result, $odeoV2ApiManager, $redis;

  public function __construct($result) {
    parent::__construct();
    $this->result = $result;
  }

  public function handle() {
    $this->odeoV2ApiManager = app()->make(ApiManager::class);
    $this->redis = Redis::connection();

    $res = $this->odeoV2ApiManager->request('POST', '/disbursement/legacy-adapter-callback', $this->result);
    $success = isset($res['status_code']) && $res['status_code'] == 200;

    if ($success) {
      return;
    }

    if (!$this->canRetry()) {
      return;
    }

    dispatch((new NotifyResult($this->result))->delay(60));
  }

  private function canRetry()
  {
    $attempt = $this->redis->hincrby('odeo_core:disbursement_notify_attempt', md5(json_encode($this->result)), 1);
    return $attempt < 5;
  }

}
