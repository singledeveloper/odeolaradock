<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/04/19
 * Time: 11.05
 */

namespace Odeo\Domains\Disbursement\Adapter\Mandiri\Helper;


use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\DisbursementStatus;
use Odeo\Domains\Constant\MandiriDisbursement;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Adapter\Helper\DisbursementInquirer;
use Odeo\Domains\Vendor\Mandiri\Repository\DisbursementMandiriDisbursementRepository;
use Odeo\Domains\Vendor\Mandiri\Transfer;

class ExecuteMandiriDisbursement {

  /**
   * @var DisbursementMandiriDisbursementRepository
   */
  private $mandiriDisbursementRepo;

  /**
   * @var DisbursementInquirer
   */
  private $disbursementInquirer;

  private function init() {
    $this->mandiriDisbursementRepo = app()->make(DisbursementMandiriDisbursementRepository::class);
    $this->disbursementInquirer = app()->make(DisbursementInquirer::class);
  }

  public function exec($disbursement) {
    $this->init();
    $inquiryStatus = $this->disbursementInquirer->inquire($disbursement);
    if ($inquiryStatus != BankAccountInquiryStatus::COMPLETED) {
      $mandiri = $this->mandiriDisbursementRepo->findById($disbursement->vendor_disbursement_reference_id);
      $mandiri->status = MandiriDisbursement::FAIL;
      $mandiri->response_code = MandiriDisbursement::RESPONSE_CODE_INQUIRY_FAILED;
      $this->mandiriDisbursementRepo->save($mandiri);

      return [
        'disbursement_id' => $disbursement->id,
        'status' => BankAccountInquiryStatus::toDisbursementStatus($inquiryStatus),
        'disbursement_vendor' => VendorDisbursement::MANDIRI,
        'disbursement_vendor_reference_id' => $mandiri->id,
        'disbursement_vendor_updated_at' => strtotime($mandiri->updated_at),
      ];
    }

    $pipeline = new Pipeline;
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'amount' => $disbursement->amount,
      'mandiri_disbursement_id' => $disbursement->vendor_disbursement_reference_id,
      'account_number' => $disbursement->account_number,
      'remark' => 'WDOCASH 2' . $disbursement->id,
      'account_name' => $disbursement->account_name,
      'transaction_key' => 'DISB2#' . $disbursement->id
    ]));

    $pipeline->execute();

    $mandiri = $this->mandiriDisbursementRepo->findById($disbursement->vendor_disbursement_reference_id);
    return [
      'disbursement_id' => $disbursement->id,
      'status' => $this->getDisbursementStatus($pipeline),
      'disbursement_vendor' => VendorDisbursement::MANDIRI,
      'disbursement_vendor_reference_id' => $mandiri->id,
      'disbursement_vendor_updated_at' => strtotime($mandiri->updated_at),
    ];
  }

  private function getDisbursementStatus(Pipeline $pipeline) {
    if ($pipeline->success()) {
      return DisbursementStatus::COMPLETED;
    }

    switch ($pipeline->errorMessage['response_code']) {
      case '2':
        return DisbursementStatus::FAILED_BANK_REJECTION;
      case '8':
        return DisbursementStatus::ON_PROGRESS_WAIT_FOR_NOTIFY;
      case '1': // success from mandiri but invalid signature
      case '3':
      default:
        return DisbursementStatus::SUSPECT;
    }
  }

}