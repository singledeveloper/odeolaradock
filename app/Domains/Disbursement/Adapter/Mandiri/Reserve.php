<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/04/19
 * Time: 12.01
 */

namespace Odeo\Domains\Disbursement\Adapter\Mandiri;


use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\MandiriDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Mandiri\Repository\DisbursementMandiriDisbursementRepository;

class Reserve {

  /**
   * @var DisbursementMandiriDisbursementRepository
   */
  private $mandiriDisbursementRepo;

  private function init() {
    $this->mandiriDisbursementRepo = app()->make(DisbursementMandiriDisbursementRepository::class);
  }

  public function reserve(PipelineListener $listener, $data) {
    $this->init();

    $mandiri = $this->mandiriDisbursementRepo->getNew();
    $mandiri->disbursement_type = Disbursement::DISBURSEMENT;
    $mandiri->disbursement_reference_id = $data['disbursement_id'];
    $mandiri->status = MandiriDisbursement::PENDING;
    $this->mandiriDisbursementRepo->save($mandiri);

    return $listener->response(200, [
      'disbursement_vendor_reference_id' => $mandiri->id,
      'disbursement_vendor_updated_at' => strtotime($mandiri->updated_at),
    ]);
  }

}