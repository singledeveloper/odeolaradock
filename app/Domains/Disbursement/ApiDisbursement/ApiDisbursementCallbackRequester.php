<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Carbon\Carbon;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Helper\ApiDisbursementNotifier;
use Odeo\Domains\Disbursement\Model\ApiDisbursement;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;

class ApiDisbursementCallbackRequester {

  private $disbursementApiUserRepo, $apiDisbursementNotifier;

  public function __construct() {
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->apiDisbursementNotifier = app()->make(ApiDisbursementNotifier::class);
  }

  public function testCallback(PipelineListener $listener, $data) {
    $apiUser = $this->disbursementApiUserRepo->findByUserId($data['auth']['user_id']);
    if (!$apiUser) {
      return $listener->response(400, 'No access');
    }

    $disbursement = new ApiDisbursement();
    $disbursement->id = -1;
    $disbursement->status = \Odeo\Domains\Constant\ApiDisbursement::COMPLETED;
    $disbursement->bank_id = 1;
    $disbursement->account_number = '12345678';
    $disbursement->customer_name = 'testcallback';
    $disbursement->account_name = 'testcallback';
    $disbursement->amount = 100000;
    $disbursement->fee = 6500;
    $disbursement->description = 'testcallback';
    $disbursement->reference_id = 'testcallback';
    $disbursement->created_at = Carbon::parse('2006-01-02 15:04:05');

    list($success, , $reason) = $this->apiDisbursementNotifier->notify($disbursement, $apiUser);

    return $listener->response(200, [
      'success' => $success,
      'reason' => $reason,
    ]);
  }
}
