<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;



use Illuminate\Http\Request;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\Helper\DisbursementApiHelper;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;

class ApiDisbursementGuard {

  private $disbursementApiUserRepo, $disbursementApiHelper, $apiDisbsursementRepo;

  /**
   * @var Request
   */
  private $request;

  public function __construct(Request $request) {
    $this->request = $request;
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->disbursementApiHelper = app()->make(DisbursementApiHelper::class);
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
  }

  public function guard(PipelineListener $listener, $data) {
    $apiUser = $this->disbursementApiUserRepo->findByAccessKeyId($data['access_key_id']);

    if (!$apiUser) {
      clog('disbursement_api_log', 'Access key id not found: ' . $data['access_key_id']);
      return $listener->response(401,
        ApiDisbursement::getErrorObject(ApiDisbursement::ERROR_UNAUTHORIZED));
    }

    if (app()->environment() == 'production' && $apiUser->whitelist != null &&
      !in_array($data['ip_address'], json_decode($apiUser->whitelist, true))) {
      clog('disbursement_api_log', 'IP address not authorized: ' . json_encode([
        'access_key_id' => $data['access_key_id'],
        'ip_address' => $data['ip_address']
      ]));

      return $listener->response(401,
        ApiDisbursement::getErrorObject(ApiDisbursement::ERROR_UNAUTHORIZED));
    }

    return $listener->response(200, [
      'user_id' => $apiUser->user_id,
      'type' => UserType::DISBURSEMENT,
      'api_user' => $apiUser,
    ]);
  }

  public function guardByAuthUser(PipelineListener $listener, $data) {
    $disb = $this->apiDisbursementRepo->findById($data['disbursement_id']);
    if (!$disb || $disb->user_id != $data['auth']['user_id']) {
      return $listener->response(400, trans('errors.data_not_exists'));
    }

    $listener->appendData([
      'api_disbursement' => $disb,
    ]);
    return $listener->response(200);
  }
}