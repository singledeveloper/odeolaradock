<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementInquiryRepository;
use Odeo\Domains\Transaction\Helper\CashManager;
use Odeo\Domains\Transaction\Helper\Currency;

class ApiDisbursementInquiryAdminSelector {

  private $apiDisbursementInquiryRepo;
  private $cashManager;
  private $currency;

  public function __construct() {
    $this->apiDisbursementInquiryRepo = app()->make(ApiDisbursementInquiryRepository::class);
    $this->cashManager = app()->make(CashManager::class);
    $this->currency = app()->make(Currency::class);
  }

  public function transforms($item, Repository $repository, $cashMap = []) {
    $inquiry = [];

    $inquiry['id'] = $item->id;
    $inquiry['fee'] = $this->currency->formatPrice($item->fee);
    $inquiry['account_number'] = $item->account_number;
    $inquiry['account_name'] = $item->account_name;
    $inquiry['created_at'] = $item->created_at->format('Y-m-d H:i:s');
    $inquiry['updated_at'] = $item->updated_at->format('Y-m-d H:i:s');

    if ($repository->hasExpand('user') && $user = $item->user) {
      $inquiry['user'] = [
        'id' => $user->id,
        'name' => $user->name,
        'email' => $user->email,
        'telephone' => $user->telephone
      ];
    }

    if ($repository->hasExpand('cash')) {
      $inquiry['cash'] = $cashMap[$item->user_id];
    }

    if ($repository->hasExpand('bank')) {
      $inquiry['account_bank'] = $item->bank->name;
    }

    $status = ApiDisbursement::getStatusMessage($item->status);

    if ($item->status == ApiDisbursement::FAIL_PROCESSING) {
      $status = 'FAIL PROCESSING';
    }

    $inquiry['status'] = strtoupper($status);

    return $inquiry;
  }

  public function get(PipelineListener $listener, $data) {
    $this->apiDisbursementInquiryRepo->normalizeFilters($data);
    $this->apiDisbursementInquiryRepo->setSimplePaginate(true);

    $inquiries = $this->apiDisbursementInquiryRepo->gets();
    $result = [];
    $cashMap = [];

    if ($this->apiDisbursementInquiryRepo->hasExpand('cash')) {
      $cashMap = $this->cashManager->getCashBalance($inquiries->pluck('user_id')->all(), Currency::IDR);
    }

    foreach ($inquiries as $item) {
      $result[] = $this->transforms($item, $this->apiDisbursementInquiryRepo, $cashMap);
    }

    if (sizeof($result) > 0) {
      return $listener->response(200, array_merge(
        ['inquiries' => $result],
        $this->apiDisbursementInquiryRepo->getPagination()
      ));
    }

    return $listener->response(204, ['inquiries' => []]);
  }
}
