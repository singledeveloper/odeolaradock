<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementInquiryRepository;

class ApiDisbursementInquirySummarySelector {

  private $apiDisbursementInquiryRepo;

  public function __construct() {
    $this->apiDisbursementInquiryRepo = app()->make(ApiDisbursementInquiryRepository::class);
  }

  public function getInquirySummary(PipelineListener $listener, $data) {
    $this->apiDisbursementInquiryRepo->normalizeFilters($data);
    $summary = $this->apiDisbursementInquiryRepo->getSummaryByUserId($data['auth']['user_id']);
    $summaryByStatus = [
      ApiDisbursement::FAILED => [
        'status' => ApiDisbursement::FAILED,
        'count' => 0,
      ],
      ApiDisbursement::COMPLETED => [
        'status' => ApiDisbursement::COMPLETED,
        'count' => 0,
      ],
    ];

    foreach ($summary as $row) {
      $status = ApiDisbursement::generalizeStatus($row->status);
      if (!isset($summaryByStatus[$status])) {
        continue;
      }

      $summaryByStatus[$status]['count'] += $row->count;
    }

    return $listener->response(200, [
      'summary' => array_values($summaryByStatus),
    ]);
  }
}