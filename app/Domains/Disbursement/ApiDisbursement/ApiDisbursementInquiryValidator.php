<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Illuminate\Http\Request;


use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Helper\CashManager;

class ApiDisbursementInquiryValidator {

  private $bankRepo, $cashManager;

  public function __construct(Request $request) {
    $this->bankRepo = app()->make(BankRepository::class);
    $this->cashManager = app()->make(CashManager::class);
  }

  public function validate(PipelineListener $listener, $data) {
    $userId = $data["auth"]["user_id"];
    $apiUser = $data['auth']['api_user'];
    $bankId = $data['bank_id'];
    $fee = intval($apiUser->inquiry_fee ?? ApiDisbursement::INQUIRY_FEE);

    if (isset($data['with_validation']) && $data['with_validation']) {
      $fee += intval($apiUser->inquiry_validation_fee ?? ApiDisbursement::INQUIRY_VALIDATION_FEE);
    }
    
    $bank = $this->bankRepo->findById($bankId);
    if ($bank == null || $bank->aj_bank_code === null || $bank->aj_regency_code === null) {
      return $listener->response(400,
        ApiDisbursement::getErrorObject(ApiDisbursement::ERROR_INVALID_BANK));
    }

    if ($fee == 0) {
      return $listener->response(200);
    }

    $balance = $this->cashManager->getCashBalance($userId);
    if (!isset($balance[$userId]) || $fee > $balance[$userId]) {
      return $listener->response(400,
        ApiDisbursement::getErrorObject(ApiDisbursement::ERROR_INSUFFICIENT_BALANCE));
    }

    return $listener->response(200);
  }
}