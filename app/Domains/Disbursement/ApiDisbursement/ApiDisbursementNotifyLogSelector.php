<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementNotifyLogRepository;

class ApiDisbursementNotifyLogSelector {

  private $apiDisbursementNotifyLogRepo;

  public function __construct() {
    $this->apiDisbursementNotifyLogRepo = app()->make(ApiDisbursementNotifyLogRepository::class);
  }

  public function transforms($item) {
    return [
      'id' => $item->id,
      'created_at' => $item->created_at->toDateTimeString(),
      'is_success' => $item->is_success,
      'response_status_code' => $item->response_status_code,
      'reason' => $item->response_reason,
    ];
  }

  public function listByDisbursementId(PipelineListener $listener, $data) {
    $logs = $this->apiDisbursementNotifyLogRepo->listByDisbursementId($data['disbursement_id']);
    return $listener->response(200, [
      'logs' => $logs->map([$this, 'transforms']),
    ]);
  }
}