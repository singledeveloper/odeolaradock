<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;

use Carbon\Carbon;
use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Helper\FeeCalculator;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ExecuteDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\NotifyApiDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\SendDisbursementReceipt;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\UpdateBatchDisbursementStatus;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\BatchDisbursementRepository;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;
use Odeo\Domains\Transaction\Helper\CashInserter;
use Odeo\Domains\Transaction\Helper\CashManager;

class ApiDisbursementRequester {

  private $apiDisbursementRepo, $bankRepo, $cashInserter, $cashManager, $selector, $feeCalculator;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->bankRepo = app()->make(BankRepository::class);
    $this->cashInserter = app()->make(CashInserter::class);
    $this->cashManager = app()->make(CashManager::class);
    $this->selector = app()->make(ApiDisbursementSelector::class);
    $this->feeCalculator = app()->make(FeeCalculator::class);
  }

  public function request(PipelineListener $listener, $data) {
//    $now = time();
//    $maintenanceTime = Carbon::parse('2018-12-11 16:00:00')->getTimestamp();
//    $maintenanceTimeEnd = Carbon::parse('2018-12-12 00:00:00')->getTimestamp();
//
//    if (!in_array($data['bank_id'], [Bank::BCA, Bank::BNI, Bank::CIMBNIAGA]) && $now >= $maintenanceTime && $now < $maintenanceTimeEnd) {
//      return $listener->response(400, ApiDisbursement::getErrorObject(ApiDisbursement::FAILED_VENDOR_DOWN));
//    }

    try {
      $disbursement = $this->addItem($data);
      $this->cashInserter->add([
        'user_id' => $data['auth']['user_id'],
        'trx_type' => TransactionType::API_DISBURSEMENT,
        'cash_type' => CashType::OCASH,
        'amount' => -($disbursement->amount + $disbursement->fee),
        'data' => json_encode([
          'id' => $disbursement->id,
          'to' => $disbursement->bank->name . ' - ' . $disbursement->account_number,
          'notes' => $disbursement->customer_name
        ]),
        'reference_type' => TransactionType::API_DISBURSEMENT,
        'reference_id' => $disbursement->id
      ]);

      $this->cashInserter->run();
    } catch (InsufficientFundException $e) {
      return $listener->response(400,
        ApiDisbursement::getErrorObject(ApiDisbursement::ERROR_INSUFFICIENT_BALANCE));
    }

    $listener->pushQueue(new ExecuteDisbursement($disbursement->id));
    return $listener->response(201, $this->selector->transforms($disbursement, $this->apiDisbursementRepo));
  }

  private function addItem($data) {
    $userId = $data["auth"]["user_id"];

    $disbursement = $this->apiDisbursementRepo->getNew();
    $disbursement->user_id = $userId;
    $disbursement->bank_id = $data['bank_id'];
    $disbursement->description = $data['description'] ?? '';
    $disbursement->amount = $data['amount'];
    $disbursement->fee = $this->feeCalculator->calculateFee($userId, $data['bank_id'], $data);
    $disbursement->account_number = $data['account_number'];
    $disbursement->customer_name = $data['customer_name'];
    $disbursement->customer_email = $data['customer_email'];
    $disbursement->reference_id = $data['reference_id'];
    $disbursement->status = ApiDisbursement::PENDING;

    $this->apiDisbursementRepo->save($disbursement);

    return $disbursement;
  }

  public function complete(PipelineListener $listener, $data) {
    $disbursement = $this->apiDisbursementRepo->findById($data['disbursement_id']);

    if (!$disbursement) {
      return $listener->response(400, trans('errors.database'));
    }

    $disbursement->status = ApiDisbursement::COMPLETED;
    $disbursement->verified_at = Carbon::now()->toDateTimeString();

    $this->apiDisbursementRepo->save($disbursement);

    $this->cashManager->checkNotifyOCash($disbursement->user_id);

    if ($disbursement->batch_disbursement_id) {
      $listener->pushQueue(new UpdateBatchDisbursementStatus($disbursement->batch_disbursement_id));
    } else {
      $listener->pushQueue(new NotifyApiDisbursement($disbursement->id));
    }

    if (!empty($disbursement->customer_email)) {
      $listener->pushQueue(new SendDisbursementReceipt($disbursement->id));
    }

    return $listener->response(200);
  }

}
