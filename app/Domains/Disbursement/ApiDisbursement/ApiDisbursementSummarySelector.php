<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Carbon\Carbon;
use Illuminate\Support\Arr;
use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\BatchDisbursementRepository;

class ApiDisbursementSummarySelector {

  private $apiDisbursementRepo, $bankRepo, $batchDisbursementRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->batchDisbursementRepo = app()->make(BatchDisbursementRepository::class);
    $this->bankRepo = app()->make(BankRepository::class);
  }

  public function getSummary(PipelineListener $listener, $data) {
    $this->apiDisbursementRepo->normalizeFilters($data);
    $summary = $this->apiDisbursementRepo->getSummaryByUserId($data['auth']['user_id']);
    return $listener->response(200, [
      'summary' => $this->mapSummary($summary, ApiDisbursement::GENERALIZED_STATUS_LIST),
    ]);
  }

  public function getTopBanks(PipelineListener $listener, $data) {
    $topBanks = $this->apiDisbursementRepo->getTopBanksByUserId($data['auth']['user_id']);
    $bankIds = $topBanks->pluck('bank_id')->all();
    $banks = [];

    if (!empty($bankIds)) {
      $banks = $this->bankRepo->listByIds($bankIds)
        ->keyBy('id')
        ->all();
    }

    foreach ($topBanks as $topBank) {
      $topBank->bank_name = $banks[$topBank->bank_id]['name'];
    }

    return $listener->response(200, [
      'top_banks' => $topBanks,
    ]);
  }

  public function getSummaryByPeriod(PipelineListener $listener, $data) {
    $date = Carbon::now();
    $period = $data['period'];
    $result = [];

    switch ($period) {
      case 'daily':
        $period = 'day';
        $date = $date->startOfDay();
        $interval = new \DateInterval('P1D');
        break;
      case 'weekly':
        $period = 'week';
        $date = $date->startOfWeek();
        $interval = new \DateInterval('P1W');
        break;
      case 'monthly':
        $period = 'month';
        $interval = new \DateInterval('P1M');
        $date = $date->startOfMonth();
        break;
      default:
        return $listener->response(400, 'invalid period');
    }

    for ($i = 0; $i < 7; $i++) {
      $result[$date->format('Y-m-d')] = $this->mapEmptyPeriodSummary($date);
      $date = $date->sub($interval);
    }

    $summary = $this->apiDisbursementRepo->getSummaryByUserIdAndPeriod($data['auth']['user_id'], $period, $date);

    foreach ($summary as $row) {
      $period = Carbon::parse($row->period)->format('Y-m-d');
      $result[$period] = [
        'period' => $period,
        'count' => $row->count,
        'sum' => +$row->sum,
      ];
    }

    return $listener->response(200, [
      'period_summary' => array_reverse(array_values($result)),
    ]);
  }

  public function getSummaryByBatchDisbursementId(PipelineListener $listener, $data) {
    $batchDisbursement = Arr::get($data, 'batch_disbursement');
    if (!$batchDisbursement) {
      $batchDisbursement = $this->batchDisbursementRepo
        ->findById($data['batch_disbursement_id'])
        ->toArray();
    }

    $this->apiDisbursementRepo->normalizeFilters($data);
    $summary = $this->apiDisbursementRepo->getSummaryByBatchDisbursementId($data['batch_disbursement_id']);
    $total = $summary->pluck('count')->sum();
    $completed = $summary
      ->filter(function ($row) {
        return in_array($row->status, ApiDisbursement::COMPLETED_STATUS_LIST);
      })
      ->pluck('count')
      ->sum();
    $statusList = ApiDisbursement::GENERALIZED_STATUS_LIST_BY_BATCH_STATUS[$batchDisbursement['status']];

    return $listener->response(200, [
      'summary' => $this->mapSummary($summary, $statusList),
      'progress' => [
        'total' => $total,
        'completed' => $completed,
      ]
    ]);
  }

  private function mapSummary($summary, $statusList) {
    $summaryByStatus = collect($statusList)
      ->map(function ($status) {
        return [
          'status' => $status,
          'count' => 0,
          'sum' => 0,
        ];
      })
      ->keyBy('status')
      ->toArray();

    foreach ($summary as $row) {
      $status = ApiDisbursement::generalizeStatus($row->status);
      if (!isset($summaryByStatus[$status])) {
        continue;
      }

      $summaryByStatus[$status]['count'] += $row->count;
      $summaryByStatus[$status]['sum'] += $row->sum;
    }

    return array_values($summaryByStatus);
  }

  private function mapEmptyPeriodSummary(Carbon $date) {
    return [
      'period' => $date->format('Y-m-d'),
      'count' => 0,
      'sum' => 0,
    ];
  }
}