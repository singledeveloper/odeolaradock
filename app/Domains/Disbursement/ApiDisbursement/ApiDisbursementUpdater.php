<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Illuminate\Support\Arr;
use Odeo\Domains\Account\Helper\BankAccountParser;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ValidateAccountName;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\BatchDisbursementRepository;
use Odeo\Domains\Transaction\Helper\CashManager;
use Odeo\Domains\Transaction\Helper\Currency;

class ApiDisbursementUpdater {

  private $apiDisbursementRepo;
  private $cashManager;
  private $currency;
  private $bankAccountParser;
  private $batchDisbursementRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->batchDisbursementRepo = app()->make(BatchDisbursementRepository::class);
    $this->cashManager = app()->make(CashManager::class);
    $this->currency = app()->make(Currency::class);
    $this->bankAccountParser = app()->make(BankAccountParser::class);
  }

  public function update(PipelineListener $listener, $data) {
    $disb = $data['api_disbursement'];

    if (!in_array($disb->status, ApiDisbursement::VALIDATION_STATUS_LIST)) {
      return $listener->response(400, trans('errors.invalid_request'));
    }

    $needInquiry = $disb->bank_id != $data['bank_id'] || $disb->account_number != $data['account_number']
      || $disb->customer_name != $data['customer_name'];
    $amountChanged = +$data['amount'] != +$disb->amount;
    $disb->bank_id = $data['bank_id'];
    $disb->account_number = $data['account_number'];
    $disb->customer_name = $data['customer_name'];
    $disb->amount = $data['amount'];
    $disb->reference_id = Arr::get($data, 'reference_id', null);

    if ($disb->reference_id == '') {
      $disb->reference_id = null;
    }

    if ($needInquiry) {
      $disb->status = ApiDisbursement::VALIDATION_ON_PROGRESS;
      $disb->account_name = '';
      $listener->pushQueue(new ValidateAccountName($disb->id));
    }

    $disb->save();

    if ($amountChanged) {
      $batch = $this->batchDisbursementRepo->findById($data['batch_disbursement_id']);
      $res = $this->apiDisbursementRepo->sumAndCountByBatchDisbursementId($data['batch_disbursement_id']);
      $batch->count = $res->count;
      $batch->total_fee = $res->sum_fee;
      $batch->total_amount = $res->sum;
      $batch->save();
    }

    return $listener->response(200);
  }

  public function markAsValidationSuccess(PipelineListener $listener, $data) {
    $disb = $data['api_disbursement'];
    if (!in_array($disb->status, [ApiDisbursement::VALIDATION_FAILED, ApiDisbursement::VALIDATION_FAILED_NAME_MISMATCH])) {
      return $listener->response(400, trans('errors.invalid_request'));
    }

    $disb->status = ApiDisbursement::VALIDATION_MANUAL_SUCCESS;
    $disb->save();

    return $listener->response(200);
  }

}