<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;



use Illuminate\Http\Request;


use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Helper\CashManager;

class ApiDisbursementValidator {

  private $bankRepo, $cashManager;

  public function __construct(Request $request) {
    $this->bankRepo = app()->make(BankRepository::class);
    $this->cashManager = app()->make(CashManager::class);
  }

  public function validate(PipelineListener $listener, $data) {
    $userId = $data["auth"]["user_id"];
    $apiUser = $data['auth']['api_user'];

    $amount = +$data['amount'];
    $bankId = $data['bank_id'];

    $fee = $apiUser->fee ?? ApiDisbursement::FEE;
    $fee = intval($fee);
    $minAmount = ApiDisbursement::MIN_AMOUNT;

    if ($amount < $minAmount) {
      return $listener->response(400,
        ApiDisbursement::getErrorObject(ApiDisbursement::ERROR_AMOUNT_BELOW_MINIMUM_LIMIT));
    }

    if ($amount > ApiDisbursement::MAX_AMOUNT) {
      return $listener->response(400,
        ApiDisbursement::getErrorObject(ApiDisbursement::ERROR_AMOUNT_EXCEEDS_MAXIMUM_LIMIT));
    }

    if (strlen($data['description']) > ApiDisbursement::MAX_DESCRIPTION_LENGTH) {
      return $listener->response(400,
        ApiDisbursement::getErrorObject(ApiDisbursement::ERROR_INVALID_DESCRIPTION));
    }

    $bank = $this->bankRepo->findById($bankId);

    if ($bank == null || $bank->aj_bank_code === null || $bank->aj_regency_code === null) {
      return $listener->response(400,
        ApiDisbursement::getErrorObject(ApiDisbursement::ERROR_INVALID_BANK));
    }

    $balance = $this->cashManager->getCashBalance($userId);
    if (!isset($balance[$userId]) || $amount + $fee > $balance[$userId]) {
      return $listener->response(400,
        ApiDisbursement::getErrorObject(ApiDisbursement::ERROR_INSUFFICIENT_BALANCE));
    }

    return $listener->response(200);
  }

}