<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Artajasa;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\ArtajasaMutationDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ExecuteDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\ArtajasaBalanceUpdater;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaDisbursementRepository;

class RetryArtajasaDisbursement {

  private $apiDisbursementRepo, $artajasaBalanceUpdater, $artajasaDisbursementRepo, $redis;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->artajasaBalanceUpdater = app()->make(ArtajasaBalanceUpdater::class);
    $this->artajasaDisbursementRepo = app()->make(DisbursementArtajasaDisbursementRepository::class);
    $this->redis = Redis::connection();
  }

  public function retry(PipelineListener $listener, $data) {
    if (!($disbursement = $this->apiDisbursementRepo->findById($data['disbursement_id']))) {
      return $listener->response(400, 'Disbursement not found');
    }

    if ($disbursement->status != ApiDisbursement::SUSPECT) {
      return $listener->response(400, 'Disbursement status is not suspect');
    }

    if (!($ajDisbursement = $this->artajasaDisbursementRepo->findById($disbursement->auto_disbursement_reference_id))) {
      return $listener->response(400, 'AJ Disbursement not found');
    }

    if ($ajDisbursement->status == ArtajasaDisbursement::SUCCESS) {
      return $listener->response(400, 'AJ Disbursement status is success');
    }

    $this->redis->hdel('odeo_core:artajasa_disbursement_lock', 'id_' . $ajDisbursement->id);
    $this->redis->hdel('odeo_core:api_disbursement_lock', 'id_' . $data['disbursement_id']);

    $ajDisbursement->last_inquire_date = null;
    $ajDisbursement->disbursement_artajasa_transfer_id = null;
    $ajDisbursement->disbursement_artajasa_transfer_inquiry_id = null;
    $ajDisbursement->status = ArtajasaDisbursement::PENDING;
    $this->artajasaDisbursementRepo->save($ajDisbursement);

    if ($disbursement->status != ApiDisbursement::PENDING && $disbursement->status != ApiDisbursement::PENDING_NOT_ENOUGH_DEPOSIT) {
      $this->artajasaBalanceUpdater->updateBalance([
        'amount' => $disbursement->amount,
        'fee' => ArtajasaDisbursement::COST,
        'type' => ArtajasaMutationDisbursement::TYPE_REFUND,
        'disbursement_artajasa_disbursement_id' => $ajDisbursement->id
      ]);
    }

    if ($disbursement->status != ApiDisbursement::PENDING) {
      $disbursement->status = ApiDisbursement::PENDING;
      $this->apiDisbursementRepo->save($disbursement);
    }

    $listener->pushQueue(new ExecuteDisbursement($disbursement->id));

    return $listener->response(200);
  }

}
