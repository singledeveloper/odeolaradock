<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BatchDisbursementStatus;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ExecuteBatchDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\BatchDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;
use Odeo\Domains\Transaction\Helper\CashInserter;

class BatchDisbursementApprover {

  private $batchDisbursementRepo;
  private $apiDisbursementRepo;
  private $cashInserter;
  private $disbursementApiUserRepo;
  private $redis;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->batchDisbursementRepo = app()->make(BatchDisbursementRepository::class);
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->cashInserter = app()->make(CashInserter::class);
    $this->redis = Redis::connection();
  }

  public function approve(PipelineListener $listener, $data) {
    $this->batchDisbursementRepo->lock();
    $batch = $this->batchDisbursementRepo->findById($data['batch_disbursement_id']);

    if ($batch->status != BatchDisbursementStatus::PENDING)
      return $listener->response(400, 'You need our approval to continue');

    if (!$batch->is_admin_approved)
      return $listener->response(400, trans('errors.invalid_request'));

    if ($batch->need_approval_from == null && $batch->user_id != $data['auth']['user_id'])
      return $listener->response(400, trans('errors.invalid_request'));

    if ($batch->need_approval_from != null && $batch->need_approval_from != $data['auth']['user_id'])
      return $listener->response(400, trans('errors.invalid_request'));

    if (!$this->apiDisbursementRepo->isReadyByBatchDisbursementId($data['batch_disbursement_id']))
      return $listener->response(400, trans('disbursement.not_ready'));

    $apiUser = $this->disbursementApiUserRepo->findByUserId($batch->user_id);
    if (!$apiUser) {
      return $listener->response(400, trans('errors.invalid_request'));
    }

    $res = $this->apiDisbursementRepo->sumAndCountByBatchDisbursementId($data['batch_disbursement_id']);
    $batch->count = $res->count;
    $batch->total_fee = $res->sum_fee;
    $batch->total_amount = $res->sum;
    $batch->status = BatchDisbursementStatus::ON_PROGRESS;
    $batch->approved_at = Carbon::now();
    $batch->save();

    $disbs = $this->apiDisbursementRepo->listByBatchDisbursementId($data['batch_disbursement_id']);
    foreach ($disbs as $disb) {
      $this->cashInserter->add([
        'user_id' => $batch->user_id,
        'trx_type' => TransactionType::BATCH_DISBURSEMENT,
        'cash_type' => CashType::OCASH,
        'amount' => -$disb->amount,
        'data' => json_encode([
          'id' => $batch->id,
          'disbursement_id' => $disb->id,
          'batch_name' => $batch->name,
          'to_number' => $disb->account_number,
          'to_account_name' => $disb->account_name,
          'to_customer_name' => $disb->customer_name,
          'description' => $disb->description
        ]),
        'reference_type' => TransactionType::API_DISBURSEMENT,
        'reference_id' => $disb->id
      ]);

      if ($disb->fee == 0) {
        continue;
      }

      $this->cashInserter->add([
        'user_id' => $batch->user_id,
        'trx_type' => TransactionType::BATCH_DISBURSEMENT_FEE,
        'cash_type' => CashType::OCASH,
        'amount' => -$disb->fee,
        'data' => json_encode([
          'id' => $batch->id,
          'disbursement_id' => $disb->id,
          'batch_name' => $batch->name,
          'description' => $disb->description
        ]),
        'reference_type' => TransactionType::API_DISBURSEMENT,
        'reference_id' => $disb->id
      ]);
    }

    try {
      $this->cashInserter->run();
    }
    catch (InsufficientFundException $e) {
      return $listener->response(400, 'Saldo oCash Anda tidak mencukupi.');
    }

    $batch->save();
    $this->apiDisbursementRepo->updateValidatingStatusByBatchDisbursementId($batch->id, ApiDisbursement::PENDING);

    $listener->pushQueue(new ExecuteBatchDisbursement($batch->id));
    return $listener->response(200);
  }

}