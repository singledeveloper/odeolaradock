<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BatchDisbursementStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\BatchDisbursementRepository;

class BatchDisbursementCanceller {

  private $batchDisbursementRepo;
  private $apiDisbursementRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->batchDisbursementRepo = app()->make(BatchDisbursementRepository::class);
  }

  public function cancel(PipelineListener $listener, $data) {
    $batch = $data['batch_disbursement'];

    if ($batch->status != BatchDisbursementStatus::PENDING) {
      return $listener->response(400, trans('errors.invalid_request'));
    }

    $batch->status = BatchDisbursementStatus::CANCELLED;
    $this->apiDisbursementRepo->updateValidatingStatusByBatchDisbursementId($batch->id, ApiDisbursement::CANCELLED);
    $batch->save();
    return $listener->response(200);
  }
}