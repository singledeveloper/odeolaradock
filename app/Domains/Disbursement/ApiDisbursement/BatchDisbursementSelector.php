<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Illuminate\Support\Arr;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\BatchDisbursementStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ExportBatchDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ExportFailedBatchDisbursement;
use Odeo\Domains\Disbursement\Repository\BatchDisbursementRepository;

class BatchDisbursementSelector {


  private $batchDisbursementRepo, $userId;

  public function __construct() {
    $this->batchDisbursementRepo = app()->make(BatchDisbursementRepository::class);
  }

  public function transforms($item) {
    $result = [
      'id' => $item->id,
      'user_id' => $item->user_id,
      'name' => $item->name,
      'count' => $item->count,
      'total_amount' => +$item->total_amount,
      'status' => $item->status,
      'message' => BatchDisbursementStatus::toStatusMessage($item->status),
      'is_admin_approved' => $item->is_admin_approved &&
        ($item->need_approval_from == $this->userId || $item->need_approval_from == null),
      'created_at' => $item->created_at->format('Y-m-d H:i:s'),
    ];

    return $result;
  }

  public function listByUserId(PipelineListener $listener, $data) {
    $this->userId = $data['auth']['user_id'];
    $this->batchDisbursementRepo->normalizeFilters($data);
    $batches = $this->batchDisbursementRepo
      ->listByUserId($this->userId)
      ->map([$this, 'transforms']);

    return $listener->response(
      sizeof($batches) > 0 ? 200 : 204,
      array_merge(
        ['batch_disbursements' => $batches],
        $this->batchDisbursementRepo->getPagination()
      ));
  }

  public function findById(PipelineListener $listener, $data) {
    $this->userId = $data['auth']['user_id'];
    $batch = Arr::get($data, 'batch_disbursement');
    if (!$batch) {
      $batch = $this->batchDisbursementRepo->findById($data['batch_disbursement_id']);
    }
    return $listener->response(200, [
      'batch_disbursement' => $this->transforms($batch),
    ]);
  }

  public function export(PipelineListener $listener, $data) {
    $user = app()->make(UserRepository::class)->findById(getUserId());
    if (!$user->email) {
      return $listener->response(400, trans('user.email_not_set'));
    }
    if (!$user->is_email_verified) {
      return $listener->response(400, 'Your email is not verified');
    }

    $exportId = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class)->create($data);

    $listener->pushQueue(new ExportBatchDisbursement($data['batch_disbursement_id'], $exportId));

    return $listener->response(200, [
      'export_id' => $exportId
    ]);
  }

  public function exportFailed(PipelineListener $listener, $data) {
    $user = app()->make(UserRepository::class)->findById(getUserId());
    if (!$user->email) {
      return $listener->response(400, trans('user.email_not_set'));
    }
    if (!$user->is_email_verified) {
      return $listener->response(400, 'Your email is not verified');
    }

    $exportId = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class)->create($data);

    $listener->pushQueue(new ExportFailedBatchDisbursement($data['batch_disbursement_id'], $exportId));

    return $listener->response(200, [
      'export_id' => $exportId
    ]);
  }
}