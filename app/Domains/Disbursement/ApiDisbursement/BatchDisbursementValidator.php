<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ValidateAccountName;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\BatchDisbursementRepository;

class BatchDisbursementValidator {


  private $batchDisbursementRepo, $apiDisbursementRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->batchDisbursementRepo = app()->make(BatchDisbursementRepository::class);
  }

  public function validate(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];
    $rows = $data['rows'];

    $validator = $this->createValidator($userId, $rows);
    if ($validator->fails()) {
      return $listener->response(400, $validator->errors()->all(), true);
    }

    return $listener->response(200);
  }

  public function validateAccountName(PipelineListener $listener, $data) {
    $disb = $data['api_disbursement'];
    if ($disb->status != ApiDisbursement::VALIDATION_FAILED) {
      return $listener->response(400, trans('errors.invalid_request'));
    }

    $disb->status = ApiDisbursement::VALIDATION_ON_PROGRESS;
    $disb->save();

    $listener->pushQueue(new ValidateAccountName($data['disbursement_id']));
    return $listener->response(200);
  }

  private function createValidator($userId, $rows) {
    $existingRefIds = $this
      ->getExistingReferenceIds($userId, $rows)
      ->flip();

    $fields = ['bank_id', 'account_number', 'amount', 'customer_name', 'reference_id'];
    $attributeNames = [];
    foreach ($rows as $key => $_) {
      $rowNo = $key + 1;

      foreach ($fields as $field) {
        $attributeNames["rows.$key.$field"] = trans('misc.row_field', ['row' => $rowNo, 'field' => $field]);
      }
    }

    $validateData = [
      'rows' => $rows
    ];
    $validator = Validator::make(
      $validateData,
      [
        'rows' => 'min:1|max:500',
        'rows.*.bank_id' => 'bank_exists',
        'rows.*.account_number' => 'required|digits_between:8,18',
        'rows.*.amount' => 'required|integer|min_amount|max:' . ApiDisbursement::MAX_AMOUNT,
        'rows.*.customer_name' => 'required|max:64',
        'rows.*.description' => 'nullable|max:' . ApiDisbursement::MAX_DESCRIPTION_LENGTH,
        'rows.*.reference_id' => 'nullable|max:32|alpha_dash|reference_id_unique',
      ],
      [
        'rows.min' => trans('validation.min.array', ['attribute' => 'template', 'min' => ':min']),
        'rows.max' => trans('validation.max.array', ['attribute' => 'template', 'max' => ':max']),
        'min_amount' => trans('validation.min.numeric', ['attribute' => ':attribute',
          'min' => trans('misc.or', ['a' => ApiDisbursement::MIN_AMOUNT_ODEO, 'b' => ApiDisbursement::MIN_AMOUNT])]),
        'reference_id_unique' => trans('validation.unique', ['attribute' => ':attribute']),
        'bank_exists' => trans('validation.exists', ['attribute' => ':attribute']),
      ]
    );
    $validator->setAttributeNames($attributeNames);
    $validator->addExtension('reference_id_unique', function ($_, $value) use ($existingRefIds) {
      return !isset($existingRefIds[$value]);
    });
    $validator->addExtension('bank_exists', function ($_, $value) {
      return $value !== null;
    });
    $validator->addExtension('min_amount', function ($attr, $value) use ($validateData) {
      $bankIdAttr = substr($attr, 0, strlen($attr) - strlen('.amount')) . '.bank_id';
      $bankId = Arr::get($validateData, $bankIdAttr);
      $min = ApiDisbursement::MIN_AMOUNT;
      if ($bankId == Bank::ODEO) {
        $min = ApiDisbursement::MIN_AMOUNT_ODEO;
      }
      return +$value >= $min;
    });

    return $validator;
  }

  private function getExistingReferenceIds($userId, $rows) {
    $referenceIds = collect($rows)
      ->pluck('reference_id')
      ->filter(function ($refId) {
        return $refId != '';
      });

    if (empty($referenceIds)) {
      return collect();
    }

    return $this->apiDisbursementRepo
      ->listByUserIdAndReferenceIds($userId, $referenceIds)
      ->pluck('reference_id');
  }

}