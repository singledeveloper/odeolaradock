<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Bca\Helper;

use Carbon\Carbon;
use Odeo\Domains\Account\Helper\BankAccountInquirer;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Domains\Vendor\Bca\Helper\ApiManager;
use Odeo\Domains\Vendor\Bca\Helper\CostCalculator;
use Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository;
use Odeo\Domains\Vendor\Bca\Transfer;

class ExecuteBcaDisbursement {

  private $apiDisbursementRepo, $bcaDisbursementRepo, $bankAccountInquirer, $disbursementApiUserRepo, $costCalculator;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->bcaDisbursementRepo = app()->make(DisbursementBcaDisbursementRepository::class);
    $this->bankAccountInquirer = app()->make(BankAccountInquirer::class);
    $this->costCalculator = app()->make(CostCalculator::class);
  }

  public function exec($disbursement) {
    $this->init($disbursement);

    $inquiryStatus = $this->inquiry($disbursement);
    if ($inquiryStatus != ApiDisbursement::COMPLETED) {
      $bca = $this->bcaDisbursementRepo->findById($disbursement->auto_disbursement_reference_id);
      $bca->status = BcaDisbursement::FAIL;
      $bca->error_code = BcaDisbursement::ERROR_CODE_INQUIRY_FAILED;
      $this->bcaDisbursementRepo->save($bca);

      return $inquiryStatus;
    }

    if (!BcaDisbursement::isOperational()) {
      return $this->delayDisbursement($disbursement);
    }

    $remark = "WDOCASH 2{$disbursement->id}";

    $pipeline = new Pipeline();
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'bca_disbursement_id' => $disbursement->auto_disbursement_reference_id,
      'corporate_id' => ApiManager::$CORPORATEID,
      'remark_1' => substr($remark, 0, 18),
      'remark_2' => strlen($remark > 18) ? substr($remark, 18, 18) : '',
      'transfer_from' => ApiManager::$ACCOUNTNUMBER[0],
      'transfer_to' => $disbursement->account_number,
      'transaction_key' => 'DISB#' . $disbursement->id,
    ]));

    $pipeline->execute();

    return $this->getDisbursementStatus($pipeline);
  }

  private function init($disbursement) {
    if (!$disbursement->auto_disbursement_reference_id
      || $disbursement->auto_disbursement_vendor != ApiDisbursement::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT) {
      $bca = $this->bcaDisbursementRepo->getNew();
      $bca->amount = $disbursement->amount;
      $bca->disbursement_type = Disbursement::API_DISBURSEMENT;
      $bca->disbursement_reference_id = $disbursement->id;
      $bca->status = BcaDisbursement::PENDING;
      $this->bcaDisbursementRepo->save($bca);

      $disbursement->auto_disbursement_reference_id = $bca->id;
      $disbursement->auto_disbursement_vendor = ApiDisbursement::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT;
    }

    $disbursement->cost = BcaDisbursement::TRANSFER_BASE_COST;
    $disbursement->status = ApiDisbursement::ON_PROGRESS;
    $this->apiDisbursementRepo->save($disbursement);
  }

  private function getDisbursementStatus(Pipeline $pipeline) {
    if ($pipeline->success()) {
      return ApiDisbursement::COMPLETED;
    }

    switch ($pipeline->errorMessage['error_code'] ?? null) {
      case 'ESB-82-014':
      case 'ESB-99-156':
      case 'ESB-82-012':
        return ApiDisbursement::FAILED_WRONG_ACCOUNT_NUMBER;

      case 'ESB-82-020':
      case 'ESB-82-018':
      case 'ESB-82-022':
        return ApiDisbursement::FAILED_CLOSED_BANK_ACCOUNT;

      case 'ESB-99-157':
        return ApiDisbursement::FAILED_BANK_REJECTION;

      case 'TIMEOUT':
      default:
        return ApiDisbursement::SUSPECT;
    }
  }

  private function inquiry($disbursement) {
    try {
      $apiUser = $this->disbursementApiUserRepo->findByUserId($disbursement->user_id);

      list($bankInquiry, $inquiryStatus) = $this->bankAccountInquirer
        ->inquire($disbursement->bank_id, $disbursement->account_number, $disbursement->user_id, [
          'timeout' => 50,
          'sender_prefix' => $apiUser->alias,
          'customer_name' => $disbursement->customer_name,
          'reference_id' => $disbursement->id,
          'reference_type' => ArtajasaDisbursement::INQUIRY_REFERENCE_TYPE_API_DISBURSEMENT,
        ]);

      if ($inquiryStatus == BankAccountInquiryStatus::COMPLETED) {
        $disbursement->account_name = $bankInquiry->account_name;
        $this->apiDisbursementRepo->save($disbursement);
      }

      return BankAccountInquiryStatus::toApiDisbursementStatus($inquiryStatus);
    } catch (\Exception $e) {
      \Log::error($e);
      return ApiDisbursement::FAILED;
    }
  }

  private function delayDisbursement($disbursement) {
    $disbursement->status = ApiDisbursement::COMPLETED;
    $disbursement->delayed_at = new \DateTime();
    $this->apiDisbursementRepo->save($disbursement);
    return $disbursement->status;
  }

}
