<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Bni;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BniDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ExecuteDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Vendor\Bni\Repository\DisbursementBniDisbursementRepository;

class RetryBniDisbursement {

  private $apiDisbursementRepo, $bniDisbursementRepo, $redis;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->bniDisbursementRepo = app()->make(DisbursementBniDisbursementRepository::class);
    $this->redis = Redis::connection();
  }

  public function retry(PipelineListener $listener, $data) {
    if (!($disbursement = $this->apiDisbursementRepo->findById($data['disbursement_id']))) {
      return $listener->response(400, 'Disbursement not found');
    }

    if ($disbursement->status == ApiDisbursement::COMPLETED) {
      return $listener->response(400, 'Disbursement status is completed');
    }

    if (!($bniDisbursement = $this->bniDisbursementRepo->findById($disbursement->auto_disbursement_reference_id))) {
      return $listener->response(400, 'Bni Disbursement not found');
    }

    if ($bniDisbursement->status == BniDisbursement::SUCCESS) {
      return $listener->response(400, 'Bni Disbursement status is success');
    }

    $this->redis->hdel('odeo_core:bni_disbursement_lock', 'id_' . $bniDisbursement->id);
    $this->redis->hdel('odeo_core:api_disbursement_lock', 'id_' . $data['disbursement_id']);

    $bniDisbursement->error_message = null;
    $bniDisbursement->status = null;
    $bniDisbursement->response_datetime = null;
    $bniDisbursement->response_code = null;
    $bniDisbursement->response_message = null;
    $this->bniDisbursementRepo->save($bniDisbursement);

    if ($disbursement->status != ApiDisbursement::PENDING) {
      $disbursement->status = ApiDisbursement::PENDING;
      $this->apiDisbursementRepo->save($disbursement);
    }

    $listener->pushQueue(new ExecuteDisbursement($disbursement->id));

    return $listener->response(200);
  }

}
