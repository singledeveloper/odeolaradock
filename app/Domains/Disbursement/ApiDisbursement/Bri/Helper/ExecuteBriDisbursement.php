<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Bri\Helper;

use Odeo\Domains\Account\Helper\BankAccountInquirer;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\BriDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Domains\Vendor\Bri\Transfer;
use Odeo\Domains\Vendor\Bri\Repository\DisbursementBriDisbursementRepository;
use Odeo\Domains\Vendor\Bri\Helper\ApiManager;

class ExecuteBriDisbursement {

  private $apiDisbursementRepo, $briDisbursementRepo, $bankAccountInquirer, $disbursementApiUserRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->briDisbursementRepo = app()->make(DisbursementBriDisbursementRepository::class);
    $this->bankAccountInquirer = app()->make(BankAccountInquirer::class);
  }

  public function exec($disbursement) {
    $this->init($disbursement);

    $inquiryStatus = $this->inquiry($disbursement);
    if ($inquiryStatus != ApiDisbursement::COMPLETED) {
      $bri = $this->briDisbursementRepo->findById($disbursement->auto_disbursement_reference_id);
      $bri->status = BriDisbursement::FAIL;
      $bri->response_code = BriDisbursement::RESPONSE_CODE_INQUIRY_FAILED;
      $this->briDisbursementRepo->save($bri);

      return $inquiryStatus;
    }

    $pipeline = new Pipeline;
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'bri_disbursement_id' => $disbursement->auto_disbursement_reference_id,
      'remark' => 'WDOCASH 2' . $disbursement->id,
      'transfer_from' => ApiManager::$accountNumber,
      'transfer_to' => $disbursement->account_number
    ]));
    $pipeline->execute();

    return $this->getDisbursementStatus($pipeline);
  }

  private function init($disbursement) {
    if (!$disbursement->auto_disbursement_reference_id
      || $disbursement->auto_disbursement_vendor != ApiDisbursement::DISBURSEMENT_VENDOR_BRI_API_DISBURSEMENT) {
      $bri = $this->briDisbursementRepo->getNew();
      $bri->amount = $disbursement->amount;
      $bri->disbursement_type = Disbursement::API_DISBURSEMENT;
      $bri->disbursement_reference_id = $disbursement->id;
      $bri->status = BriDisbursement::PENDING;
      $this->briDisbursementRepo->save($bri);

      $disbursement->auto_disbursement_reference_id = $bri->id;
      $disbursement->auto_disbursement_vendor = ApiDisbursement::DISBURSEMENT_VENDOR_BRI_API_DISBURSEMENT;
    }

    $disbursement->cost = BriDisbursement::COST;
    $disbursement->status = ApiDisbursement::ON_PROGRESS;
    $this->apiDisbursementRepo->save($disbursement);
  }

  private function getDisbursementStatus(Pipeline $pipeline) {
    if ($pipeline->success()) {
      return ApiDisbursement::COMPLETED;
    }

    switch ($pipeline->errorMessage['response_code'] ?? null) {
      case '0201':
        if (
          str_replace(BriDisbursement::BANK_REJECTION_ERROR_DESC, '', $pipeline->errorMessage['error_desc'])
        ) {
          return ApiDisbursement::FAILED_BANK_REJECTION;
        }
      case '0106':
      case '0207':
      case '0301':
      case '0401':
      case '0403':
      case '0404':
        return ApiDisbursement::FAILED_BANK_REJECTION;

      case '0405':
      case '0406':
        return ApiDisbursement::FAILED_CLOSED_BANK_ACCOUNT;

      default:
        return ApiDisbursement::SUSPECT;
    }
  }

  private function inquiry($disbursement) {
    try {
      $apiUser = $this->disbursementApiUserRepo->findByUserId($disbursement->user_id);

      list($bankInquiry, $inquiryStatus) = $this->bankAccountInquirer
        ->inquire($disbursement->bank_id, $disbursement->account_number, $disbursement->user_id, [
          'timeout' => 50,
          'sender_prefix' => $apiUser->alias,
          'customer_name' => $disbursement->customer_name,
          'reference_id' => $disbursement->id,
          'reference_type' => ArtajasaDisbursement::INQUIRY_REFERENCE_TYPE_API_DISBURSEMENT,
        ]);

      if ($inquiryStatus == BankAccountInquiryStatus::COMPLETED) {
        $disbursement->account_name = $bankInquiry->account_name;
        $this->apiDisbursementRepo->save($disbursement);
      }

      return BankAccountInquiryStatus::toApiDisbursementStatus($inquiryStatus);
    } catch (\Exception $e) {
      \Log::error($e);
      return ApiDisbursement::FAILED;
    }
  }

}
