<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Cimb\Helper;

use Odeo\Domains\Account\Helper\BankAccountInquirer;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\CimbApi;
use Odeo\Domains\Constant\CimbDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Domains\Vendor\Cimb\Transfer;
use Odeo\Domains\Vendor\Cimb\Repository\DisbursementCimbDisbursementRepository;

class ExecuteCimbDisbursement {

  private $apiDisbursementRepo, $cimbDisbursementRepo, $bankAccountInquirer, $disbursementApiUserRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->cimbDisbursementRepo = app()->make(DisbursementCimbDisbursementRepository::class);
    $this->bankAccountInquirer = app()->make(BankAccountInquirer::class);
  }

  public function exec($disbursement) {
    $this->init($disbursement);

    $inquiryStatus = $this->inquiry($disbursement);
    if ($inquiryStatus != ApiDisbursement::COMPLETED) {
      $cimb = $this->cimbDisbursementRepo->findById($disbursement->auto_disbursement_reference_id);
      $cimb->status = CimbDisbursement::STATUS_FAIL;
      $cimb->status_code = CimbDisbursement::STATUS_CODE_INQUIRY_FAILED;
      $this->cimbDisbursementRepo->save($cimb);

      return $inquiryStatus;
    }

    $requestId = CimbApi::API_DISBUSEMENT_PREFIX . $disbursement->id;

    $pipeline = new Pipeline;
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'memo' => $disbursement->description,
      'transaction_key' => 'APIDISB#' . $disbursement->id,
      'account_number' => $disbursement->account_number,
      'account_name' => $disbursement->account_name,
      'request_id' => $requestId,
      'cimb_disbursement_id' => $disbursement->auto_disbursement_reference_id,
    ]));

    $pipeline->execute();

    return $this->getDisbursementStatus($pipeline);
  }

  private function init($disbursement) {
    if (!$disbursement->auto_disbursement_reference_id
      || $disbursement->auto_disbursement_vendor != ApiDisbursement::DISBURSEMENT_VENDOR_CIMB_API_DISBURSEMENT) {
      $cimb = $this->cimbDisbursementRepo->getNew();
      $cimb->amount = $disbursement->amount;
      $cimb->disbursement_type = Disbursement::API_DISBURSEMENT;
      $cimb->disbursement_reference_id = $disbursement->id;
      $cimb->status = CimbDisbursement::STATUS_PENDING;
      $this->cimbDisbursementRepo->save($cimb);

      $disbursement->auto_disbursement_reference_id = $cimb->id;
      $disbursement->auto_disbursement_vendor = ApiDisbursement::DISBURSEMENT_VENDOR_CIMB_API_DISBURSEMENT;
    }

    $disbursement->cost = CimbDisbursement::COST;
    $disbursement->status = ApiDisbursement::ON_PROGRESS;
    $this->apiDisbursementRepo->save($disbursement);
  }

  private function getDisbursementStatus(Pipeline $pipeline) {
    if ($pipeline->success()) {
      return ApiDisbursement::COMPLETED;
    }

    switch ($pipeline->errorMessage['response_code'] ?? null) {
      case '001':
        return ApiDisbursement::FAILED_WRONG_ACCOUNT_NUMBER;

      case '037':
      case '045':
      case '22':
      case '28':
        return ApiDisbursement::FAILED_BANK_REJECTION;

      default:
        return ApiDisbursement::SUSPECT;
    }
  }

  private function inquiry($disbursement) {
    try {
      $apiUser = $this->disbursementApiUserRepo->findByUserId($disbursement->user_id);

      list($bankInquiry, $inquiryStatus) = $this->bankAccountInquirer
        ->inquire($disbursement->bank_id, $disbursement->account_number, $disbursement->user_id, [
          'timeout' => 50,
          'sender_prefix' => $apiUser->alias,
          'customer_name' => $disbursement->customer_name,
          'reference_id' => $disbursement->id,
          'reference_type' => ArtajasaDisbursement::INQUIRY_REFERENCE_TYPE_API_DISBURSEMENT,
        ]);

      if ($inquiryStatus == BankAccountInquiryStatus::COMPLETED) {
        $disbursement->account_name = $bankInquiry->account_name;
        $this->apiDisbursementRepo->save($disbursement);
        if ($this->isEMoneyAccount($disbursement->account_number, $bankInquiry->customer_name)) {
          return ApiDisbursement::FAILED;
        }
      }

      return BankAccountInquiryStatus::toApiDisbursementStatus($inquiryStatus);
    } catch (\Exception $e) {
      \Log::error($e);
      return ApiDisbursement::FAILED;
    }
  }

  private function isEMoneyAccount($accountNo, $accountName) {
    $gopayPrefixes = ['28490', '89808', '89889'];
    $ovoPrefixes = ['809908'];
    if (strpos($accountName, 'GOPAY ') === 0) {
      return $this->isValidAccountPrefix($accountNo, $gopayPrefixes, 5);
    } else if (strpos($accountName, 'OVO ') === 0) {
      return $this->isValidAccountPrefix($accountNo, $ovoPrefixes, 6);
    }
  }

  private function isValidAccountPrefix($accountNo, $prefixes, $prefixLength) {
    $prefix = substr($accountNo, 0, $prefixLength);
    if (in_array($prefix, $prefixes)) return true;
    return false;
  }

}
