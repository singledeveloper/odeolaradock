<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;

use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;

class DisbursementApiUserSelector {

  private $disbursementApiUserRepo;

  public function __construct() {
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function get(PipelineListener $listener, $data) {
    $apiUser = $this->disbursementApiUserRepo->findByUserId($data['auth']['user_id']);
    if (!$apiUser) {
      return $listener->response(400, 'No access');
    }

    $accessKeyId = $apiUser->access_key_id;
    if ($accessKeyId == ApiDisbursement::NO_ACCESS) {
      $accessKeyId = '';
    }

    $user = $this->users->findById($data['auth']['user_id']);

    return $listener->response(200, [
      'access_key_id' => $accessKeyId,
      'signing_key' => $apiUser->signing_key,
      'whitelist' => json_decode($apiUser->whitelist),
      'notify_url' => $apiUser->notify_url,
      'email' => $user && $user->email ? $user->email : ''
    ]);
  }

  public function getV2DisbursementUser(PipelineListener $listener, $data) {
    if (!$apiUser = $this->disbursementApiUserRepo->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('api_users.unauthorized'));
    }

    return $listener->response(200, [
      'notify_url' => $apiUser->notify_url,
      'fee' => $apiUser->fee,
      'inquiry_fee' => $apiUser->inquiry_fee
    ]);
  }

}