<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Helper;


use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementSelector;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;

class BatchDisbursementExporter {

  private $apiDisbursementRepo, $apiDisbursementSelector;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->apiDisbursementSelector = app()->make(ApiDisbursementSelector::class);
  }

  public function export($batchDisbursementId) {
    $this->apiDisbursementRepo->normalizeFilters([
      'is_paginate' => false,
      'expand' => 'bank',
    ]);
    $rows = $this->apiDisbursementRepo->listByBatchDisbursementId($batchDisbursementId);
    return $this->toCsv($rows);
  }

  public function exportFailed($batchDisbursementId) {
    $this->apiDisbursementRepo->normalizeFilters([
      'status_list' => ApiDisbursement::FAILED_STATUS_LIST,
      'is_paginate' => false,
      'expand' => 'bank',
    ]);
    $rows = $this->apiDisbursementRepo->listByBatchDisbursementId($batchDisbursementId);
    return $this->toCsv($rows);
  }

  private function toCsv($rows) {
    $writer = WriterFactory::create(Type::CSV);
    ob_start();

    $writer->openToFile('php://output');
    $writer->addRow([
      'id',
      'bank_id',
      'account_number',
      'account_name',
      'amount',
      'customer_name',
      'customer_email',
      'fee',
      'description',
      'reference_id',
      'status',
      'message',
      'created_at',
    ]);

    foreach ($rows as $row) {
      $row = $this->apiDisbursementSelector->transforms($row, $this->apiDisbursementRepo);
      $writer->addRow([
        $row['id'],
        $row['bank_id'],
        $row['account_number'],
        $row['account_name'],
        $row['amount'],
        $row['customer_name'],
        $row['customer_email'],
        $row['fee'],
        $row['description'],
        $row['reference_id'],
        $row['status'],
        $row['message'],
        date('Y-m-d H:i:s', $row['created_at']),
      ]);
    }

    $writer->close();
    return ob_get_clean();
  }
}
