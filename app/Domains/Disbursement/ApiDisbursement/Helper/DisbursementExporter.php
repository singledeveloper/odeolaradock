<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Helper;


use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementInquirySelector;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementSelector;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementInquiryRepository;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Box\Spout\Writer\WriterFactory;

class DisbursementExporter {
  protected $disbursementRepo, $disbursementSelector, $disbursementInquiryRepo, $disbursementInquirySelector;

  public function __construct() {
    $this->disbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->disbursementSelector = app()->make(ApiDisbursementSelector::class);
    $this->disbursementInquiryRepo = app()->make(ApiDisbursementInquiryRepository::class);
    $this->disbursementInquirySelector = app()->make(ApiDisbursementInquirySelector::class);
  }

  public function exportDisbursement($filters) {
    $this->disbursementRepo->normalizeFilters(array_merge($filters, [
      'is_paginate' => false,
      'expand' => 'bank',
    ]));

    $list = $this->disbursementRepo->getReport();
    $writer = WriterFactory::create($filters['file_type']);
    ob_start();

    $writer->openToFile('php://output');
    $writer->addRow([
      'id',
      'bank_id',
      'account_number',
      'account_name',
      'amount',
      'customer_name',
      'customer_email',
      'fee',
      'description',
      'reference_id',
      'status',
      'message',
      'created_at',
    ]);

    foreach ($list as $item) {
      $item = $this->disbursementSelector->transforms($item, $this->disbursementRepo);
      $writer->addRow([
        $item['id'],
        $item['bank_id'],
        $item['account_number'],
        $item['account_name'],
        $item['amount'],
        $item['customer_name'],
        $item['customer_email'],
        $item['fee'],
        $item['description'],
        $item['reference_id'],
        $item['status'],
        $item['message'],
        date('Y-m-d H:i:s', $item['created_at']),
      ]);
    }

    $writer->close();

    return [ob_get_clean(), $list->isNotEmpty()];
  }

  public function exportDisbursementInquiry($filters) {
    $this->disbursementInquiryRepo->normalizeFilters(array_merge($filters, [
      'is_paginate' => false,
      'expand' => 'bank',
    ]));

    $list = $this->disbursementInquiryRepo->getReport();
    $writer = WriterFactory::create($filters['file_type']);
    ob_start();

    $writer->openToFile('php://output');
    $writer->addRow([
      'id',
      'bank_id',
      'account_number',
      'account_name',
      'customer_name',
      'fee',
      'status',
      'message',
      'created_at',
    ]);

    foreach ($list as $item) {
      $item = $this->disbursementInquirySelector->transforms($item, $this->disbursementInquiryRepo);
      $writer->addRow([
        $item['id'],
        $item['bank_id'],
        $item['account_number'],
        $item['account_name'],
        $item['customer_name'],
        $item['fee'],
        $item['status'],
        $item['message'],
        date('Y-m-d H:i:s', $item['created_at']),
      ]);
    }

    $writer->close();

    return [ob_get_clean(), $list->isNotEmpty()];
  }
}
