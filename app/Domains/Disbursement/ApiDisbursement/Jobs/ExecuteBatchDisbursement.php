<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;


use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Jobs\Job;

class ExecuteBatchDisbursement extends Job {

  private $batchDisbursementId;

  public function __construct($batchDisbursementId) {
    parent::__construct();
    $this->batchDisbursementId = $batchDisbursementId;
  }

  public function handle() {
    $apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $disbs = $apiDisbursementRepo->listByBatchDisbursementId($this->batchDisbursementId);

    foreach ($disbs as $disb) {
      dispatch(new ExecuteDisbursement($disb->id));
    }
  }
}