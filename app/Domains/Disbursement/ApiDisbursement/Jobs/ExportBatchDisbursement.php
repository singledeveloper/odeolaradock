<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;


use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Disbursement\ApiDisbursement\Helper\BatchDisbursementExporter;
use Odeo\Domains\Disbursement\Repository\BatchDisbursementRepository;
use Odeo\Jobs\Job;

class ExportBatchDisbursement extends Job {


  private $batchDisbursementId, $exportId;

  public function __construct($batchDisbursementId, $exportId = '0') {
    parent::__construct();
    $this->batchDisbursementId = $batchDisbursementId;
    $this->exportId = $exportId;
  }

  public function handle() {
    $exporter = app()->make(BatchDisbursementExporter::class);
    $exportQueueManager = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class);
    $batchDisbursementRepo = app()->make(BatchDisbursementRepository::class);
    $userRepo = app()->make(UserRepository::class);

    $batch = $batchDisbursementRepo->findById($this->batchDisbursementId);
    $user = $userRepo->findById($batch->user_id);

    if (!$user->email) {
      return;
    }

    $file = $exporter->export($this->batchDisbursementId);

    $exportQueueManager->update($this->exportId, $file, preg_replace("/[^A-Za-z0-9 ]/", '_', $batch->name) . '_' . time() . '.csv');

    Mail::send('emails.batch_disbursement_report', [
      'data' => [
        'name' => $batch->name,
      ],
    ], function ($m) use ($user, $batch, $file) {

      $m->to($user->email)
        ->from('noreply@odeo.co.id', 'odeo')
        ->attachData($file, $batch->name . '.csv')
        ->subject("Batch Disbursement {$batch->name} Report");
    });
  }
}