<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;


use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\BatchDisbursementRepository;
use Odeo\Jobs\Job;

class SendBatchDisbursementValidatedAlert extends Job {

  private $batchDisbursementId;

  public function __construct($batchDisbursementId) {
    parent::__construct();
    $this->batchDisbursementId = $batchDisbursementId;
  }

  public function handle() {
    $userRepo = app()->make(UserRepository::class);
    $apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $batchDisbursementRepo = app()->make(BatchDisbursementRepository::class);

    $notFinished = $apiDisbursementRepo->isValidationNotCompletedByBatchDisbursementId($this->batchDisbursementId);
    if ($notFinished) {
      return;
    }

    $batch = $batchDisbursementRepo->findById($this->batchDisbursementId);
    $user = $userRepo->findById($batch->user_id);

    if (!$user->email) {
      return;
    }

    if (!$this->guardAlert()) {
      return;
    }

    Mail::send('emails.batch_disbursement_validation_completed_alert', [
      'data' => [
        'name' => $batch->name,
      ],
    ], function ($m) use ($user) {
      $m->from('noreply@odeo.co.id', 'odeo')
        ->to($user->email)
        ->subject('Batch Disbursement Validation Completed');
    });
  }

  private function guardAlert() {
    $redis = Redis::connection();
    $key = 'odeo_core:batch_disbursement_validation_completed_alert_' . $this->batchDisbursementId;
    if (!$redis->setnx($key, 1)) {
      return false;
    }
    $redis->expire($key, 5 * 60);
    return true;
  }
}