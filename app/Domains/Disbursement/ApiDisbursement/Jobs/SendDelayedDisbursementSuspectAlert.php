<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Jobs\Job;

class SendDelayedDisbursementSuspectAlert extends Job implements ShouldQueue {
  protected $disbursementIdList;

  public function __construct($disbursementIdList) {
    parent::__construct();
    $this->disbursementIdList = is_array($disbursementIdList) ? $disbursementIdList : [$disbursementIdList];
  }

  public function handle() {
    $disbursementRepo = app()->make(ApiDisbursementRepository::class);
    $disbursements = $disbursementRepo->getAllByIdList($this->disbursementIdList)->sortByDesc('id');

    Mail::send('emails.delayed_disbursement_suspect_alert', [
      'data' => $disbursements,
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('pg@odeo.co.id')
        ->subject('Delayed API Disbursement Suspect Alert');
    });
  }
}
