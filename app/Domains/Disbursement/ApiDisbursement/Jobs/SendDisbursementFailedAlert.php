<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Jobs\Job;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;

class SendDisbursementFailedAlert extends Job implements ShouldQueue
{

  use SerializesModels;

  private $disbursementId;

  public function __construct($disbursementId)
  {
    parent::__construct();
    $this->disbursementId = $disbursementId;
  }

  public function handle()
  {
    if (!isProduction()) {
      return;
    }

    $apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $disbursement = $apiDisbursementRepo->findById($this->disbursementId);
    Mail::send('emails.disbursement_failed_alert', [
      'data' => [
        'disbursement_id' => $disbursement->id,
        'status' => ApiDisbursement::getStatusMessage($disbursement->status),
        'vendor' => $disbursement->auto_disbursement_vendor,
      ],
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo')
        ->to('disbursement@odeo.co.id')
        ->subject('Disbursement Failure - ' . date('Y-m-d'));
    });
  }

}
