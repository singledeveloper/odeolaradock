<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\DataExporterType;
use Odeo\Domains\Constant\DataExporterView;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementInquiryRepository;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Exporter\Helper\DataExporter;
use Odeo\Jobs\Job;

class SendDisbursementInvoice extends Job implements ShouldQueue {

  private $userId, $period, $exportId;

  public function __construct($userId, $period, $exportId = '0') {
    parent::__construct();
    $this->userId = $userId;
    $this->period = $period;
    $this->exportId = $exportId;
  }

  public function handle() {
    if (!isProduction() && $this->exportId == '0') {
      return;
    }

    $apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $apiDisbursementInquiryRepo = app()->make(ApiDisbursementInquiryRepository::class);
    $userRepo = app()->make(UserRepository::class);
    $dataExporter = app()->make(DataExporter::class);
    $exportQueueManager = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class);

    $user = $userRepo->findById($this->userId);
    $disbursementRows = $apiDisbursementRepo
      ->getInvoiceRows($this->userId, $this->period)
      ->map(function ($row) {
        return $this->toRow('Disbursement Fee', $row);
      })
      ->toBase();
    $inquiryRows = $apiDisbursementInquiryRepo
      ->getInvoiceRows($this->userId, $this->period)
      ->map(function ($row) {
        return $this->toRow('Inquiry Fee', $row);
      })
      ->toBase();

    $rows = $disbursementRows->merge($inquiryRows);
    $data = [
      'invoice_no' => 'INV/DISB/' . $this->userId . '/' . str_replace('-', '/', $this->period),
      'period' => $this->period,
      'user_id' => $user->id,
      'user_name' => $user->name,
      'rows' => $rows,
      'total' => number_format($rows->sum('amount')),
    ];

    $file = $dataExporter->generate($data, DataExporterType::PDF, DataExporterView::API_DISBURSEMENT_INVOICE_KEY);
    $fileName = 'Tagihan API Disbursement - ' . $this->period . '.pdf';
    $exportQueueManager->update($this->exportId, $file, $fileName);

    Mail::send('emails.api-disbursement-invoice', [
      'data' => $data
    ], function ($m) use ($file, $user, $fileName) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->bcc('pg@odeo.co.id');
      $m->attachData($file, $fileName);
      $m->to($user->email)->subject('Tagihan API Disbursement - ' . $this->period);
    });
  }

  private function toRow($name, $item) {
    return [
      'name' => $name,
      'period' => $this->period,
      'quantity' => number_format($item->quantity),
      'price' => number_format($item->price),
      'formatted_amount' => number_format($item->amount),
      'amount' => $item->amount,
    ];
  }

}
