<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Odeo\Jobs\Job;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;

class SendDisbursementNotifyFailedAlert extends Job implements ShouldQueue {

  use SerializesModels;

  private $disbursementId;
  private $reason;
  private $userEmail;

  public function __construct($disbursementId, $reason, $userEmail) {
    parent::__construct();
    $this->disbursementId = $disbursementId;
    $this->reason = $reason;
    $this->userEmail = $userEmail;
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

    Mail::send('emails.disbursement_notify_failed_alert', [
      'data' => [
        'disbursement_id' => $this->disbursementId,
        'reason' => $this->reason
      ]
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo')
        ->to($this->userEmail)
        ->bcc('pg@odeo.co.id')
        ->subject('Disbursement Notify Failure - ' . date('Y-m-d'));
    });
  }

}
