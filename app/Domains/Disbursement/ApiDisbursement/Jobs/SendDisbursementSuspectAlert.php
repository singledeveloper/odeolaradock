<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Jobs\Job;

class SendDisbursementSuspectAlert extends Job implements ShouldQueue {
  private $redis;
  const REDIS_KEY = 'odeo_core:send_disbursement_suspect_alert';

  public function __construct() {
    parent::__construct();
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

    $this->redis = Redis::connection();
    $disbursementRepo = app()->make(ApiDisbursementRepository::class);
    $disbursements = $disbursementRepo->getSuspectNotRunning();
    $idList = $disbursements->pluck('id')->toArray();

    if ($disbursements->count() == 0) {
      return;
    }

    if ($this->isNotified($idList)) {
      return;
    }

    $this->saveNotifiedState($idList);

    Mail::send('emails.disbursement_suspect_alert', [
      'data' => $disbursements,
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('disbursement@odeo.co.id')
        ->subject('API Disbursement Suspect Alert');
    });
  }

  private function isNotified($idList) {
    $lastNotified = $this->redis->hget(self::REDIS_KEY, 'notified_at');
    if (time() - $lastNotified > 30 * 60) {
      return false;
    }

    $prevIdList = [];
    $json = $this->redis->hget(self::REDIS_KEY, 'id_list');
    if ($json) {
      $prevIdList = json_decode($json);
    }

    $diff = array_diff($idList, $prevIdList);
    return count($diff) == 0;
  }

  private function saveNotifiedState($idList) {
    $this->redis->hset(self::REDIS_KEY, 'id_list', json_encode($idList));
    $this->redis->hset(self::REDIS_KEY, 'notified_at', time());
  }
}
