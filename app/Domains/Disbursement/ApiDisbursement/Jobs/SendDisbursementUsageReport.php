<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/7/17
 * Time: 5:35 PM
 */

namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Helper\DisbursementExporter;
use Odeo\Jobs\Job;

class SendDisbursementUsageReport extends Job implements ShouldQueue {
  protected $user_id, $date, $email;

  public function __construct($userId, $date, $email) {
    parent::__construct();
    $this->user_id = $userId;
    $this->date = $date;
    $this->email = $email;
  }

  public function handle() {
    $filters = [
      "file_type" => "csv",
      "user_id" => $this->user_id,
      "start_date" => $this->date,
      "end_date" => $this->date,
    ];

    $exporter = app()->make(DisbursementExporter::class);
    list($disbursementFile, $hasDisbursement) = $exporter->exportDisbursement($filters);
    list($disbursementInquiryFile, $hasInquiry) = $exporter->exportDisbursementInquiry($filters);

    Mail::send('emails.disbursement_usage_report_mail', [
      'data' => $filters
    ], function ($m) use ($filters, $disbursementFile, $disbursementInquiryFile) {

      $m->from('noreply@odeo.co.id', 'odeo');

      $period = $filters['start_date'] . '.' . $filters['file_type'];
      $m->attachData($disbursementFile, 'Laporan Disbursement ' . $period);
      $m->attachData($disbursementInquiryFile, 'Laporan Disbursement Inquiry ' . $period);

      $m->to($this->email)->subject('Laporan API Disbursement ' . $filters['start_date']);
    });
  }
}
