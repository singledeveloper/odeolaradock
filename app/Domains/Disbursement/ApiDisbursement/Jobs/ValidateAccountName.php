<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Helper\BankAccountInquirer;
use Odeo\Domains\Account\Helper\BankAccountParser;
use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Approval\Repository\ApprovalPendingRequestRepository;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\BatchDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Jobs\Job;

class ValidateAccountName extends Job {

  private $apiDisbursementId;
  private $redis, $apiDisbursementRepo;

  public function __construct($apiDisbursementId) {
    parent::__construct();
    $this->apiDisbursementId = $apiDisbursementId;
  }

  public function handle() {
    $this->redis = Redis::connection();
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);

    if (!$this->lock()) {
      return;
    }

    try {
      $disb = $this->apiDisbursementRepo->findById($this->apiDisbursementId);
      if ($disb->status != ApiDisbursement::VALIDATION_ON_PROGRESS) {
        return;
      }

      list ($accountName, $status) = $this->inquireAccountName($disb);

      $disb2 = $this->apiDisbursementRepo->findById($disb->id);
      if ($this->needRevalidate($disb, $disb2)) {
        dispatch(new ValidateAccountName($this->apiDisbursementId));
        return;
      }

      if (!in_array($disb2->status, ApiDisbursement::VALIDATION_STATUS_LIST)) {
        return;
      }

      if ($status == ApiDisbursement::VALIDATION_FAILED && $this->canRetry()) {
        $status = ApiDisbursement::VALIDATION_ON_PROGRESS;
      }

      $disb2->status = $status;
      $disb2->account_name = $accountName;
      $disb2->save();

      if ($status == ApiDisbursement::VALIDATION_SUCCESS && $this->isBatchValidated($disb->batch_disbursement_id)) {
        dispatch(new SendBatchDisbursementValidatedAlert($disb->batch_disbursement_id));
        $batchDisbursementRepo = app()->make(BatchDisbursementRepository::class);
        $batch = $batchDisbursementRepo->findById($disb->batch_disbursement_id);
        if ($batch->need_approval_from != null) {
          $approvalPendingRequestRepo = app()->make(ApprovalPendingRequestRepository::class);
          $request = $approvalPendingRequestRepo->getNew();
          $request->raw_data = json_encode(['description' => $batch->name]);
          $request->path = Approval::PATH_BATCH_DISBURSEMENT;
          $request->requested_by = $batch->user_id;
          $request->need_approver_from = $batch->need_approval_from;
          $request->reference_id = $batch->id;
          $request->status = Approval::CREATED;
          $approvalPendingRequestRepo->save($request);
        }
        return;
      }

      if ($status == ApiDisbursement::VALIDATION_ON_PROGRESS) {
        dispatch((new ValidateAccountName($this->apiDisbursementId))->delay(90));
      }
    } finally {
      $this->unlock();
    }
  }

  private function lock() {
    return $this->redis->hsetnx('odeo_core:batch_disbursement_validate_account_name', $this->apiDisbursementId, 1);
  }

  private function unlock() {
    $this->redis->hdel('odeo_core:batch_disbursement_validate_account_name', $this->apiDisbursementId);
  }

  private function canRetry() {
    $key = 'odeo_core:batch_disbursement_validate_account_name_attempts_' . $this->apiDisbursementId;
    $attempts = $this->redis->incr($key);
    $this->redis->expire($key, 300);
    return $attempts < 3;
  }

  private function inquireAccountName($disb) {
    $bankAccountInquirer = app()->make(BankAccountInquirer::class);
    $bankAccountParser = app()->make(BankAccountParser::class);
    $disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);

    $apiUser = $disbursementApiUserRepo->findByUserId($disb->user_id);

    list($bankInquiry, $inquiryStatus) = $bankAccountInquirer->inquire($disb->bank_id, $disb->account_number, $disb->user_id, [
      'timeout' => 50,
      'sender_prefix' => $apiUser->alias,
      'customer_name' => $disb->customer_name,
      'reference_id' => $disb->id,
      'reference_type' => ArtajasaDisbursement::INQUIRY_REFERENCE_TYPE_DISBURSEMENT,
    ]);

    switch ($inquiryStatus) {
      case BankAccountInquiryStatus::COMPLETED:
        if (!$bankAccountParser->isNameValid($disb->customer_name, $bankInquiry->account_name)) {
          return [$bankInquiry->account_name, ApiDisbursement::VALIDATION_FAILED_NAME_MISMATCH];
        }

        return [$bankInquiry->account_name, ApiDisbursement::VALIDATION_SUCCESS];

      case BankAccountInquiryStatus::FAILED_WRONG_ACCOUNT_NUMBER:
        return [null, ApiDisbursement::VALIDATION_FAILED_WRONG_ACCOUNT_NUMBER];

      case BankAccountInquiryStatus::FAILED_CLOSED_BANK_ACCOUNT:
        return [null, ApiDisbursement::VALIDATION_FAILED_CLOSED_BANK_ACCOUNT];

      case BankAccountInquiryStatus::FAILED:
      case BankAccountInquiryStatus::FAILED_VENDOR_DOWN:
      case BankAccountInquiryStatus::FAILED_BANK_REJECTION:
      default:
        return [null, ApiDisbursement::VALIDATION_FAILED];
    }
  }

  private function needRevalidate($disb1, $disb2) {
    return $disb1->customer_name != $disb2->customer_name
      || $disb1->bank_id != $disb2->bank_id
      || $disb1->account_number != $disb2->account_number;
  }

  private function isBatchValidated($batchDisbursementId) {
    return !$this->apiDisbursementRepo->isValidationNotCompletedByBatchDisbursementId($batchDisbursementId);
  }
}