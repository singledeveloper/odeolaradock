<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement\Odeo\Helper;


use Illuminate\Support\Facades\DB;
use Odeo\Domains\Account\Helper\BankAccountInquirer;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Transaction\Helper\CashInserter;

class ExecuteOdeoDisbursement {

  private $userRepo, $bankAccountInquirer, $apiDisbursementRepo, $cashInserter;

  public function __construct() {
    $this->userRepo = app()->make(UserRepository::class);
    $this->bankAccountInquirer = app()->make(BankAccountInquirer::class);
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->cashInserter = app()->make(CashInserter::class);
  }

  public function exec($disbursement) {
    $this->init($disbursement);

    $inquiryStatus = $this->inquiry($disbursement);
    if ($inquiryStatus != ApiDisbursement::COMPLETED) {
      return $inquiryStatus;
    }

    try {
      DB::transaction(function () use ($disbursement) {
        $sender = $this->userRepo->findById($disbursement->user_id);
        $receiver = $this->userRepo->findByTelephone(purifyTelephone($disbursement->account_number));
        
        $this->cashInserter->add([
          'user_id' => $receiver->id,
          'trx_type' => TransactionType::DISBURSEMENT_OCASH,
          'cash_type' => CashType::OCASH,
          'amount' => $disbursement->amount,
          'data' => json_encode([
            'disbursement_id' => $disbursement->id,
            'description' => $disbursement->description,
            'from_user_id' => $disbursement->user_id,
            'from_user' => $sender->name,
          ]),
        ]);
        list($trxId) = $this->cashInserter->run();

        $disbursement->status = ApiDisbursement::COMPLETED;
        $this->apiDisbursementRepo->save($disbursement);
        
        $notifications = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
        $notifications->setup($receiver->id, NotificationType::CHECK, NotificationGroup::TRANSACTION);
        $notifications->transfer_in($trxId, $disbursement->amount, $disbursement->description);
  
        dispatch($notifications->queue());
      });
    } catch (\Exception $e) {
      clog('api_disbursement', $e->getMessage());
      $disbursement->status = ApiDisbursement::FAIL_PROCESSING;
      $this->apiDisbursementRepo->save($disbursement);
    }

    return $disbursement->status;
  }

  private function init($disbursement) {
    $disbursement->auto_disbursement_vendor = ApiDisbursement::DISBURSEMENT_VENDOR_ODEO_DISBURSEMENT;
    $disbursement->cost = 0;
    $disbursement->status = ApiDisbursement::ON_PROGRESS;
    $this->apiDisbursementRepo->save($disbursement);
  }

  private function inquiry($disbursement) {
    try {
      list($bankInquiry, $inquiryStatus) = $this->bankAccountInquirer
        ->inquire($disbursement->bank_id, $disbursement->account_number, $disbursement->user_id, []);

      if ($inquiryStatus == BankAccountInquiryStatus::COMPLETED) {
        $disbursement->account_name = $bankInquiry->account_name;
        $this->apiDisbursementRepo->save($disbursement);
      }

      return BankAccountInquiryStatus::toApiDisbursementStatus($inquiryStatus);
    } catch (\Exception $e) {
      \Log::error($e);
      return ApiDisbursement::FAILED;
    }
  }
}