<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Permata\Helper;

use Odeo\Domains\Account\Helper\BankAccountInquirer;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\PermataDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Domains\Vendor\Permata\Repository\DisbursementPermataDisbursementRepository;
use Odeo\Domains\Vendor\Permata\Transfer;

class ExecutePermataDisbursement {

  private $apiDisbursementRepo, $permataDisbursementRepo, $bankAccountInquirer, $disbursementApiUserRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->permataDisbursementRepo = app()->make(DisbursementPermataDisbursementRepository::class);
    $this->bankAccountInquirer = app()->make(BankAccountInquirer::class);
  }

  public function exec($disbursement) {
    $this->init($disbursement);

    $inquiryStatus = $this->inquiry($disbursement);
    if ($inquiryStatus != ApiDisbursement::COMPLETED) {
      $permata = $this->permataDisbursementRepo->findById($disbursement->auto_disbursement_reference_id);
      $permata->status = PermataDisbursement::STATUS_FAIL;
      $permata->status_code = PermataDisbursement::STATUS_CODE_INQUIRY_FAILED;
      $this->permataDisbursementRepo->save($permata);

      return $inquiryStatus;
    }

    $pipeline = new Pipeline;
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'desc' => 'WDOCASH 2' . $disbursement->id,
      'transaction_key' => 'APIDISB#' . $disbursement->id,
      'account_number' => $disbursement->account_number,
      'account_name' => $disbursement->account_name,
      'permata_disbursement_id' => $disbursement->auto_disbursement_reference_id,
    ]));

    $pipeline->execute();

    return $this->getDisbursementStatus($pipeline);
  }

  private function init($disbursement) {
    if (!$disbursement->auto_disbursement_reference_id
      || $disbursement->auto_disbursement_vendor != ApiDisbursement::DISBURSEMENT_VENDOR_PERMATA_API_DISBURSEMENT) {
      $permata = $this->permataDisbursementRepo->getNew();
      $permata->amount = $disbursement->amount;
      $permata->disbursement_type = Disbursement::API_DISBURSEMENT;
      $permata->disbursement_reference_id = $disbursement->id;
      $permata->status = PermataDisbursement::STATUS_PENDING;
      $this->permataDisbursementRepo->save($permata);

      $disbursement->auto_disbursement_reference_id = $permata->id;
      $disbursement->auto_disbursement_vendor = ApiDisbursement::DISBURSEMENT_VENDOR_PERMATA_API_DISBURSEMENT;
    }

    $disbursement->cost = PermataDisbursement::DISBURSEMENT_COST;
    $disbursement->status = ApiDisbursement::ON_PROGRESS;
    $this->apiDisbursementRepo->save($disbursement);
  }

  private function getDisbursementStatus(Pipeline $pipeline) {
    if ($pipeline->success()) {
      return ApiDisbursement::COMPLETED;
    }

    switch ($pipeline->errorMessage['error_code'] ?? null) {
      case '14': // account not found
      case '90': // invalid beneficiary/currency
        return ApiDisbursement::FAILED_WRONG_ACCOUNT_NUMBER;
      default:
        return ApiDisbursement::SUSPECT;
    }
  }

  private function inquiry($disbursement) {
    try {
      $apiUser = $this->disbursementApiUserRepo->findByUserId($disbursement->user_id);

      list($bankInquiry, $inquiryStatus) = $this->bankAccountInquirer
        ->inquire($disbursement->bank_id, $disbursement->account_number, $disbursement->user_id, [
          'timeout' => 50,
          'sender_prefix' => $apiUser->alias,
          'customer_name' => $disbursement->customer_name,
          'reference_id' => $disbursement->id,
          'reference_type' => ArtajasaDisbursement::INQUIRY_REFERENCE_TYPE_API_DISBURSEMENT,
        ]);

      if ($inquiryStatus == BankAccountInquiryStatus::COMPLETED) {
        $disbursement->account_name = $bankInquiry->account_name;
        $this->apiDisbursementRepo->save($disbursement);
      }

      return BankAccountInquiryStatus::toApiDisbursementStatus($inquiryStatus);
    } catch (\Exception $e) {
      \Log::error($e);
      return ApiDisbursement::FAILED;
    }
  }

}
