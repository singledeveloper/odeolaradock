<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Redig\Helper;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Helper\BankAccountInquirer;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\RedigDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Domains\Vendor\Redig\BalanceUpdater;
use Odeo\Domains\Vendor\Redig\Repository\DisbursementRedigDisbursementRepository;
use Odeo\Domains\Vendor\Redig\Transfer;

class ExecuteRedigDisbursement {

  private $redigDisbursementRepo, $apiDisbursementRepo, $disbursementApiUserRepo, $bankAccountInquirer, $redis;

  public function __construct() {
    $this->redigDisbursementRepo = app()->make(DisbursementRedigDisbursementRepository::class);
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->bankAccountInquirer = app()->make(BankAccountInquirer::class);
    $this->redis = Redis::connection();
  }

  public function exec($disbursement) {
    $redigDisb = $this->init($disbursement);
    $inquiryStatus = $this->inquiry($disbursement);

    if ($inquiryStatus != ApiDisbursement::COMPLETED) {
      $redigDisb->status = RedigDisbursement::FAIL;
      $redigDisb->save();
      return $inquiryStatus;
    }

    $pipeline = new Pipeline();
    $pipeline->add(new Task(BalanceUpdater::class, 'deductDisbursement', [
      'redig_disbursement_id' => $redigDisb->id,
      'amount' => $redigDisb->amount,
      'cost' => RedigDisbursement::COST,
    ]));
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'redig_disbursement' => $redigDisb,
    ]));
    $pipeline->execute();

    $status = $this->getDisbursementStatus($pipeline);

    if ($status == ApiDisbursement::SUSPECT) {
      $this->disableRedig();
    }

    return $status;
  }

  private function init($disbursement) {
    $redigDisb = $this->redigDisbursementRepo->getNew();
    $redigDisb->status = RedigDisbursement::PENDING;
    $redigDisb->bank_id = $disbursement->bank_id;
    $redigDisb->account_no = $disbursement->account_number;
    $redigDisb->amount = $disbursement->amount;
    $redigDisb->description = $disbursement->description;
    $redigDisb->disbursement_type = Disbursement::API_DISBURSEMENT;
    $redigDisb->disbursement_reference_id = $disbursement->id;
    $redigDisb->save();

    $disbursement->auto_disbursement_reference_id = $redigDisb->id;
    $disbursement->auto_disbursement_vendor = ApiDisbursement::DISBURSEMENT_VENDOR_REDIG_API_DISBURSEMENT;
    $disbursement->cost = RedigDisbursement::COST;
    $disbursement->status = ApiDisbursement::ON_PROGRESS;
    $this->apiDisbursementRepo->save($disbursement);

    return $redigDisb;
  }

  private function inquiry($disbursement) {
    try {
      $apiUser = $this->disbursementApiUserRepo->findByUserId($disbursement->user_id);

      list($bankInquiry, $inquiryStatus) = $this->bankAccountInquirer
        ->inquire($disbursement->bank_id, $disbursement->account_number, $disbursement->user_id, [
          'timeout' => 50,
          'sender_prefix' => $apiUser->alias,
          'customer_name' => $disbursement->customer_name,
          'reference_id' => $disbursement->id,
          'reference_type' => ArtajasaDisbursement::INQUIRY_REFERENCE_TYPE_API_DISBURSEMENT,
        ]);

      if ($inquiryStatus == BankAccountInquiryStatus::COMPLETED) {
        $disbursement->account_name = $bankInquiry->account_name;
        $this->apiDisbursementRepo->save($disbursement);
      }

      return BankAccountInquiryStatus::toApiDisbursementStatus($inquiryStatus);
    } catch (\Exception $e) {
      \Log::error($e);
      return ApiDisbursement::FAILED;
    }
  }

  private function getDisbursementStatus(Pipeline $pipeline) {
    if ($pipeline->success()) {
      return ApiDisbursement::COMPLETED;
    }

    switch ($pipeline->errorMessage['status'] ?? null) {
      case RedigDisbursement::SUCCESS:
        return ApiDisbursement::COMPLETED;

      case RedigDisbursement::FAIL:
        return ApiDisbursement::FAILED;

      case RedigDisbursement::SUSPECT:
      default:
        return ApiDisbursement::SUSPECT;
    }
  }

  private function disableRedig() {
    $n = max(4, $this->redis->incr(RedigDisbursement::DISABLED_COUNT_DISBURSEMENT_REDIS_KEY));
    $this->redis->set(RedigDisbursement::DISABLE_DISBURSEMENT_REDIS_KEY, true);
    $this->redis->set(RedigDisbursement::DISABLED_COUNT_DISBURSEMENT_REDIS_KEY, $n);
    $this->redis->expire(RedigDisbursement::DISABLE_DISBURSEMENT_REDIS_KEY, pow(2, $n) * 30 * 60);
    $this->redis->expire(RedigDisbursement::DISABLED_COUNT_DISBURSEMENT_REDIS_KEY, 12 * 60 * 60);
  }
}