<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Scheduler;

use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\SendDisbursementSuspectAlert;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;

class ApiDisbursementSuspectNotRunningAlertScheduler {

  private $disbursementRepo;

  public function __construct() {
    $this->disbursementRepo = app()->make(ApiDisbursementRepository::class);
  }

  public function run() {
    if (!isProduction()) {
      return;
    }

    dispatch(new SendDisbursementSuspectAlert());
  }
}