<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Scheduler;

use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;

class ApiDisbursementUpdateFeeScheduler {

  private $disbursementApiUserRepo;

  public function __construct() {
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
  }

  public function run() {
    if (!isProduction()) {
      return;
    }

    $user = $this->disbursementApiUserRepo->findByUserId(112342);
    $user->inquiry_fee = 200;
    $this->disbursementApiUserRepo->save($user);
  }
}