<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Scheduler;

use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Bca\Jobs\ExecuteDelayedBcaDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;

class DelayedApiDisbursementScheduler {

  private $disbursementRepo;

  public function __construct() {
    $this->disbursementRepo = app()->make(ApiDisbursementRepository::class);
  }

  public function run() {
    $disbursements = $this->disbursementRepo->getDelayedList();

    foreach ($disbursements as $disbursement) {
      $this->execute($disbursement);
    }
  }

  private function execute($disbursement) {
    switch ($disbursement->auto_disbursement_vendor) {
      case ApiDisbursement::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT:
        if (BcaDisbursement::isOperational()) {
          dispatch(new ExecuteDelayedBcaDisbursement($disbursement->id));
          sleep(1);
        }
        break;
    }
  }
}