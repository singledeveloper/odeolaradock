<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Jobs;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\BillerReplenishment\ReplenishmentCreator;
use Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository;
use Odeo\Domains\Inventory\Repository\VendorSwitcherRepository;
use Odeo\Jobs\Job;

class BeginReplenishment extends Job {

  private $data, $redis;
  private $billerReconRepo, $vendorSwitcherRepo, $billerReplenishmentRepo;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->redis = Redis::connection();

    $this->billerReconRepo = app()->make(VendorSwitcherReconciliationRepository::class);
    $this->vendorSwitcherRepo = app()->make(VendorSwitcherRepository::class);
    $this->billerReplenishmentRepo = app()->make(VendorSwitcherReplenishmentRepository::class);
  }

  public function handle() {

    if (Pulsa::IS_MAINTANANCE) return;

    $data = $this->data;

    if (!$this->redis->hsetnx(BillerReplenishment::REDIS_NS, $this->getRedisKeyName(), 1)) {
      $this->touchLastReplenishment();
      return;
    }

    if (is_object($data['vendor_switcher_id'])) return;

    if (isset($data['preferred_amount'])) {
      $topupNeeded = $data['preferred_amount'];
    }
    else {
      $biller = $this->vendorSwitcherRepo->findById($data['vendor_switcher_id']);
      $topupNeeded = 0;

      if ($biller->owner_store_id != null) {
        $this->deleteRedisKey();
        return;
      }

      if ($biller->minimum_topup != null) {
        if ($this->checkIfBillerStillHaveEnoughBalance($biller)) {
          $this->deleteRedisKey();
          return;
        }

        $topupNeeded = $this->calculateTopUpNeeded($biller);

        if ($topupNeeded <= 0) {
          $this->deleteRedisKey();
          return;
        }

        $topupNeeded = $this->round($topupNeeded);
      }

      if ($topupNeeded < BillerReplenishment::DEFAULT_MINIMAL_DEPOSIT)
        $topupNeeded = BillerReplenishment::DEFAULT_MINIMAL_DEPOSIT;
      else if ($topupNeeded > BillerReplenishment::DEFAULT_MAXIMAL_DEPOSIT)
        $topupNeeded = BillerReplenishment::DEFAULT_MAXIMAL_DEPOSIT;
    }

    $pipeline = new Pipeline();

    $pipeline->add(new Task(ReplenishmentCreator::class, 'request'));
    $pipeline->execute([
      'vendor_switcher_id' => $data['vendor_switcher_id'],
      'amount' => $topupNeeded
    ]);

  }

  private function checkIfBillerStillHaveEnoughBalance($biller) {

    $data = $this->data;

    if (isset($data['end_time'])) {
      $billerSale = $this->billerReconRepo->getBillerSales($data['vendor_switcher_id'],
        $data['start_time'], $data['end_time']
      );

      return $billerSale->sales_total + $biller->minimum_topup < $biller->current_balance;
    }

    return false;
  }

  private function round($value) {
    $divider = strlen($value) > 2 ? pow(10, strlen($value) - 2) : 10;
    return ceil($value / $divider) * $divider;
  }

  private function calculateTopUpNeeded($biller) {

    $data = $this->data;

    $startTimeCarbon = Carbon::parse($data['start_time']);
    $startTime4DayAgo = $startTimeCarbon->subDays(4)->toDateTimeString();

    $billerSaleFourDays = $this->billerReconRepo->getBillerSales($data['vendor_switcher_id'],
      $startTime4DayAgo, $data['start_time']
    );

    $nettTopup = ceil($billerSaleFourDays->sales_total / 2);

    if ($nettTopup + $biller->current_balance > $biller->maximum_topup) {
      $nettTopup = $biller->maximum_topup - $biller->current_balance;
    }

    return $nettTopup;
  }

  private function touchLastReplenishment() {
    $data = $this->data;
    if ($lastReplenishment = $this->billerReplenishmentRepo->findLastReplenishment($data['vendor_switcher_id'])) {
      $this->billerReplenishmentRepo->save($lastReplenishment);
    }
  }

  private function getRedisKeyName() {
    $data = $this->data;
    return BillerReplenishment::REDIS_KEY_BILLER . $data['vendor_switcher_id'];
  }

  private function deleteRedisKey() {
    $this->redis->hdel(BillerReplenishment::REDIS_NS, $this->getRedisKeyName());
  }

}
