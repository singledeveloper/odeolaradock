<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Jobs;

use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\BillerReplenishment\ReplenishmentCreator;
use Odeo\Jobs\Job;

class ResumeReplenishment extends Job {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    $pipeline = new Pipeline();
    $pipeline->add(new Task(ReplenishmentCreator::class, 'request'));
    $pipeline->execute([
      'vendor_switcher_id' => $this->data['vendor_switcher_id']
    ]);
  }

}
