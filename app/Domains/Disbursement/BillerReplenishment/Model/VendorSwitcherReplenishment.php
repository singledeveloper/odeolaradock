<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Model\VendorSwitcher;

class VendorSwitcherReplenishment extends Entity {

  public function vendorSwitcher() {
    return $this->belongsTo(VendorSwitcher::class);
  }

}