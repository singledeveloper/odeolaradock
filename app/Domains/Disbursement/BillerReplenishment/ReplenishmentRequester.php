<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Biller\Jobs\InsertMutation;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class ReplenishmentRequester {

  private $redis, $billerReplenishmentRepo, $settingExecutor;

  public function __construct() {

    $this->redis = Redis::connection();
    $this->billerReplenishmentRepo = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository::class);
    $this->settingExecutor = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Helper\SettingExecutor::class);
  }

  public function updateAmount(PipelineListener $listener, $data) {

    if ($lastReplenishment = $this->billerReplenishmentRepo->findLastReplenishment($data['vendor_switcher_id'])) {
      $status = $lastReplenishment->auto_disbursement_vendor == null || $lastReplenishment->auto_disbursement_vendor == BillerReplenishment::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT ?
        (!BcaDisbursement::isOperational() ? BillerReplenishment::PENDING_PROGRESS_AUTO_TRANSFER : BillerReplenishment::PENDING) : BillerReplenishment::WAIT_FOR_MANUAL_VERIFY;
      $lastReplenishment->amount = $data['amount'];
      $lastReplenishment->status = isset($data['status']) ? $data['status'] : $status;
      $this->billerReplenishmentRepo->save($lastReplenishment);
    }
    else clog('replenishment_fail', 'Not found last for biller ' . $data['vendor_switcher_id']);

    return $listener->response(200);

  }

  public function verify(PipelineListener $listener, $data) {

    if ($replenishment = $this->billerReplenishmentRepo->findById($data['replenishment_id'])) {
      if (!isset($data['bank'])) $data['bank'] = BillerReplenishment::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT;

      $replenishment->auto_disbursement_vendor = $data['bank'];
      $replenishment->status = BillerReplenishment::WAIT_FOR_NOTIFY;

      $replenishment->verified_at = date('Y-m-d H:i:s');
      $this->billerReplenishmentRepo->save($replenishment);

      $this->settingExecutor->onComplete($listener, array_merge($data, [
        'amount' => $replenishment->amount,
        'vendor_switcher_id' => $replenishment->vendor_switcher_id
      ]));
    }

    return $listener->response(200);

  }

  public function fail(PipelineListener $listener, $data) {

    if ($replenishment = $this->billerReplenishmentRepo->findById($data['replenishment_id'])) {
      if ($replenishment->status == BillerReplenishment::PENDING_PROGRESS_AUTO_TRANSFER && BcaDisbursement::isOperational())
        return $listener->response(400, "Can't stop this replenishment. Will resume the process shortly.");
      $replenishment->status = BillerReplenishment::FAILED;
      $replenishment->error_message = 'Manual fail from user_id ' . $data['auth']['user_id'];
      $this->billerReplenishmentRepo->save($replenishment);
    }

    return $listener->response(200);

  }

  public function complete(PipelineListener $listener, $data) {

    if ($replenishment = $this->billerReplenishmentRepo->findById($data['replenishment_id'])) {
      $replenishment->status = BillerReplenishment::COMPLETED;
      $this->billerReplenishmentRepo->save($replenishment);

      $this->redis->hdel(BillerReplenishment::REDIS_NS, BillerReplenishment::REDIS_KEY_BILLER . $replenishment->vendor_switcher_id);

      $biller = $replenishment->vendorSwitcher;
      if ($biller->minimum_topup != null) {
        dispatch(new InsertMutation([
          'vendor_switcher_id' => $biller->id,
          'amount' => $replenishment->amount,
          'mutation_route' => SwitcherConfig::ROUTE_REPLENISHER
        ]));
      }
    }

    return $listener->response(200);

  }

  public function cancel(PipelineListener $listener, $data) {

    if ($replenishment = $this->billerReplenishmentRepo->findById($data['replenishment_id'])) {
      $replenishment->status = BillerReplenishment::CANCELLED;
      $this->billerReplenishmentRepo->save($replenishment);
    }

    return $listener->response(200);

  }

}
