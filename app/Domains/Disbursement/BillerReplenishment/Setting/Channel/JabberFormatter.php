<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting\Channel;

use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Disbursement\BillerReplenishment\Setting\Contract\ChannelContract;
use Odeo\Domains\Vendor\Jabber\Jobs\ReplySocket;

class JabberFormatter implements ChannelContract {

  private $settingJabberHistories, $jabberUsers;

  public function __construct() {
    $this->settingJabberHistories = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Repository\VendorSwitcherReplenishmentJabberHistoryRepository::class);
    $this->jabberUsers = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberUserRepository::class);
  }

  public function beforeSend($data, $setting) {
    $settingData = json_decode($setting->data, true);

    $jabberHistory = $this->settingJabberHistories->getNew();
    $jabberHistory->vendor_switcher_id = $setting->vendor_switcher_id;
    $jabberHistory->log_request = str_replace(Supplier::addPattern(Supplier::SF_PARAM_DEPOSIT),
      $data['amount'], $settingData[BillerReplenishment::SETTING_CHANNEL_JABBER_REQUEST_SAMPLE]);
    $jabberHistory->status = SwitcherConfig::SWITCHER_IN_QUEUE;
    $this->settingJabberHistories->save($jabberHistory);

    dispatch(new ReplySocket(InlineJabber::JAXL_CHAT, [
      'to' => $settingData[BillerReplenishment::SETTING_CHANNEL_JABBER_CENTER],
      'message' => $jabberHistory->log_request
    ]));

    return ['on_hold' => true];
  }

  public function onComplete($data, $setting) {
    return [];
  }

  public function onSetup($data) {
    if (!$this->jabberUsers->findByEmail($data[BillerReplenishment::SETTING_CHANNEL_JABBER_CENTER])) {
      $jabberUser = $this->jabberUsers->getNew();
      $jabberUser->email = $data[BillerReplenishment::SETTING_CHANNEL_JABBER_CENTER];
      $jabberUser->account_type = InlineJabber::ACCOUNT_TYPE_BILLER;
      $this->jabberUsers->save($jabberUser);

      dispatch(new ReplySocket(InlineJabber::JAXL_SUBSCRIBE, [
        'to' => $jabberUser->email
      ]));
    }
    return;
  }

}
