<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting;

use Illuminate\Support\Facades\Validator;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\PipelineListener;

class ChannelValidator {

  public function checkData(PipelineListener $listener, $data) {

    if ($variables = BillerReplenishment::getRequiredChannelParameters($data['channel_type'])) {
      $validatorRequired = [];
      foreach ($variables as $item) $validatorRequired[$item] = 'required';

      $validator = Validator::make($data, $validatorRequired);
      if ($validator->fails()) return $listener->response(400, $validator->errors()->all());

      foreach ($variables as $item) {
        if (is_array($data[$item])) {
          $subVariables = $data[$item];
          foreach($subVariables as $key => $sub) {
            try {
              preg_match_all(Supplier::SF_PATTERN, trim($sub), $matches);
              foreach ($matches[1] as $output) {
                if (!in_array(strtolower($output), Supplier::SF_REPLENISHMENTS))
                  return [false, 'Format ' . Supplier::addPattern($output) . ' tidak valid. Mohon gunakan format yang terdaftar di ODEO.'];
                $subVariables[$key] = str_replace(Supplier::addPattern($output), Supplier::addPattern(strtolower($output)), $subVariables[$key]);
              }
            }
            catch (\Exception $e) {}
          }
          $data[$item] = $subVariables;
        }
        try {
          preg_match_all(Supplier::SF_PATTERN, trim($data[$item]), $matches);
          foreach ($matches[1] as $output) {
            if (!in_array(strtolower($output), Supplier::SF_REPLENISHMENTS))
              return [false, 'Format ' . Supplier::addPattern($output) . ' tidak valid. Mohon gunakan format yang terdaftar di ODEO.'];
            $data[$item] = str_replace(Supplier::addPattern($output), Supplier::addPattern(strtolower($output)), $data[$item]);
          }
        }
        catch (\Exception $e) {}
      }
      $listener->replaceData($data);

      return $listener->response(200);
    }
    return $listener->response(400, 'Channel Type is invalid.');

  }

}
