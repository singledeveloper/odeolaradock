<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting;

use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class SettingDetailizer {

  private $settings, $vendorSwitchers, $currency;

  public function __construct() {
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->settings = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Repository\VendorSwitcherReplenishmentSettingRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function getDetail(PipelineListener $listener, $data) {

    if ($biller = $this->vendorSwitchers->findById($data['vendor_switcher_id'])) {
      $detail = [
        'vendor_switcher_id' => $biller->id,
        'name' => $biller->name,
        'minimum_topup' => $this->currency->formatPrice($biller->minimum_topup),
        'maximum_topup' => $this->currency->formatPrice($biller->maximum_topup),
        'bank_account' => [
          'bca' => $biller->bca_account_number,
          'bri' => $biller->bri_account_number,
          'mandiri' => $biller->mandiri_account_number
        ],
        'is_auto_replenish' => $biller->is_auto_replenish,
        'topup_started_at' => $biller->topup_started_at,
        'topup_ended_at' => $biller->topup_ended_at,
        'has_manual_config' => SwitcherConfig::getVendorSwitcherReplenisherById($data['vendor_switcher_id']) != '',
        'settings' => []
      ];

      $settings = $this->settings->getByVendorSwitcherId($data['vendor_switcher_id']);
      if (count($settings) > 0) {
        foreach ($settings as $item) {
          $detail['settings'][] = [
            'setting_id' => $item->id,
            'route_type' => $item->route_type,
            'channel_type' => $item->channel_type,
            'data' => json_decode($item->data, true)
          ];
        }
      }
      return $listener->response(200, ['replenishment' => $detail]);
    }

    return $listener->response(204);

  }

  public function getConditionDetail(PipelineListener $listener, $data) {

    if ($setting = $this->settings->findById($data['biller_replenishment_setting_id'])) {
      return $listener->response(200, ['replenishment_setting' => [
        'id' => $setting->id,
        'route_type' => $setting->route_type,
        'channel_type' => $setting->channel_type,
        'data' => json_decode($setting->data, true)
      ]]);
    }

    return $listener->response(204);

  }

}
