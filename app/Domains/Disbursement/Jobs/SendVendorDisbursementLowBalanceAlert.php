<?php

namespace Odeo\Domains\Disbursement\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendVendorDisbursementLowBalanceAlert extends Job {

  private $balances;

  public function __construct($balances) {
    parent::__construct();
    $this->balances = $balances;
  }

  public function handle() {
    $currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

    foreach ($this->balances as $key => $balance) {
      $this->balances[$key] = $currency->formatPrice($balance)['formatted_amount'];
    }

    Mail::send('emails.vendor_disbursement_low_balance_alert', [
      'balances' => $this->balances,
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo')
        ->to('disbursement@odeo.co.id')
        ->subject('Vendor Disbursement Low Balance Alert - ' . date('Y-m-d'));
    });
  }

}
