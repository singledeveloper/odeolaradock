<?php

namespace Odeo\Domains\Disbursement\Model;


use Odeo\Domains\Account\Model\Bank;
use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Model\DisbursementArtajasaTransferInquiry;

class ApiDisbursementInquiry extends Entity {

  public function bank() {
    return $this->belongsTo(Bank::class);
  }

  public function user() {
    return $this->belongsTo(User::class);
  }

  public function disbursementArtajasaTransferInquiry() {
    return $this->belongsTo(DisbursementArtajasaTransferInquiry::class);
  }

}