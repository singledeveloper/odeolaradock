<?php

namespace Odeo\Domains\Disbursement\Repository;


use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\Model\ApiDisbursementInquiry;

class ApiDisbursementInquiryRepository extends Repository {

  public function __construct(ApiDisbursementInquiry $ApiDisbursementInquiry) {
    $this->model = $ApiDisbursementInquiry;
  }

  public function gets() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel();

    if (isset($filters['search'])) {
      if (isset($filters['search']['id'])) {
        $query = $query->where('id', '=', $filters['search']['id']);
      }

      if (isset($filters['search']['user_id'])) {
        $query = $query->where('user_id', '=', $filters['search']['user_id']);
      }

      if (isset($filters['search']['status'])) {
        $query = $query->where('status', '=', $filters['search']['status']);
      }

      if (isset($filters['search']['auto_disbursement_vendor'])) {
        $query = $query->where('auto_disbursement_vendor', '=', $filters['search']['auto_disbursement_vendor']);
      }
    }

    if ($this->hasExpand('bank')) {
      $query = $query->with('bank');
    }

    if ($this->hasExpand('user')) {
      $query = $query->with('user');
    }

    if ($this->hasExpand('disbursementArtajasaTransferInquiry')) {
      $query = $query->with('disbursementArtajasaTransferInquiry');
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function getAllByUserId($userId) {
    $filters = $this->getFilters();
    $query = $this->getCloneModel()
      ->where('user_id', $userId)
      ->orderBy('id', 'desc');

    if (isset($filters['id']) && $filters['id'] != '') {
      $query->where('id', '=', $filters['id']);
    }

    if (isset($filters['start_date']) && $filters['start_date']) {
      $query->where('created_at', '>=', $filters['start_date'] . ' 00:00:00');
    }

    if (isset($filters['end_date']) && $filters['end_date']) {
      $query->where('created_at', '<=', $filters['end_date'] . ' 00:00:00');
    }

    if ($this->hasExpand('bank')) {
      $query = $query->with('bank');
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function getReport() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel()
      ->with('bank')
      ->where('user_id', $filters['user_id'])
      ->whereDate('created_at', '>=', $filters['start_date'])
      ->whereDate('created_at', '<=', $filters['end_date'])
      ->orderBy('id', 'asc');

    return $this->getResult($query);
  }

  public function getInvoiceRows($userId, $period) {
    return $this->getCloneModel()
      ->select(\DB::raw('sum(fee) as amount, count(*) as quantity, fee as price'))
      ->whereIn('status', ApiDisbursement::NON_FREE_INQUIRY_STATUS)
      ->where('user_id', $userId)
      ->where(\DB::raw("to_char(created_at, 'YYYY-MM')"), $period)
      ->groupBy('fee')
      ->get();
  }

  public function getSumByMonth($date) {
    $statuses = implode(', ', array_map(function ($s) {
      return "'$s'";
    }, ApiDisbursement::NON_FREE_INQUIRY_STATUS));

    $query = "
      SELECT
        to_char(created_at, 'YYYY-MM') as period,
        sum(fee)                       as gmv,
        sum(fee-cost)                  as revenue,
        count(id)                      as trx
      FROM api_disbursement_inquiries
      WHERE created_at IS NOT NULL AND status IN ($statuses) 
          AND created_at::date BETWEEN :start_date AND :end_date 
      GROUP BY period";

    return \DB::select(\DB::raw($query), [
      'start_date' => $date->copy()->subYear()->format('Y-m-d'),
      'end_date' => $date->format('Y-m-d'),
    ]);
  }

  public function getSuccessfulInquiry($date) {
    return $this->getCloneModel()
      ->select(\DB::raw('vendor, sum(fee) as gross_amount, sum(fee - cost) as surplus, count(*) as order_total'))
      ->whereDate('created_at', '=', $date)
      ->whereIn('status', ApiDisbursement::NON_FREE_INQUIRY_STATUS)
      ->orderBy(\DB::raw('gross_amount'), 'desc')
      ->groupBy('vendor')->get();
  }

  public function getTopUsers($date) {
    return $this->getCloneModel()
      ->select(\DB::raw('users.name, count(*) as transaction_total, count(*) * 100 / sum(count(*)) over() as percentage'))
      ->join('users', 'users.id', '=', 'api_disbursement_inquiries.user_id')
      ->whereDate('api_disbursement_inquiries.created_at', '=', $date)
      ->whereIn('api_disbursement_inquiries.status', ApiDisbursement::NON_FREE_INQUIRY_STATUS)
      ->orderBy(\DB::raw('transaction_total'), 'desc')
      ->limit(10)
      ->groupBy('users.name')->get();
  }

  public function getSummaryByUserId($userId) {
    $filters = $this->getFilters();
    $query = $this->getCloneModel()
      ->groupBy('status')
      ->where('user_id', $userId)
      ->selectRaw('status, count(*) as count');

    if (isset($filters['status']) && $filters['status'] != '') {
      $query->where('status', $filters['status']);
    }

    if (isset($filters['start_date']) && $filters['start_date']) {
      $query->where('created_at', '>=', $filters['start_date'] . ' 00:00:00');
    }

    if (isset($filters['end_date']) && $filters['end_date']) {
      $query->where('created_at', '<=', $filters['end_date'] . ' 00:00:00');
    }

    return $query->get();
  }
}