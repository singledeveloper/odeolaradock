<?php

namespace Odeo\Domains\Disbursement\Repository;


use Carbon\Carbon;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\Model\ApiDisbursement as ApiDisbursementModel;

class ApiDisbursementRepository extends Repository {

  private $lock;

  public function __construct(ApiDisbursementModel $model) {
    $this->setModel($model);
  }

  public function lock() {
    $this->lock = true;
  }

  public function findById($id, $fields = ['*']) {
    $model = $this->getCloneModel();
    if ($this->lock) $model = $this->model->lockForUpdate();
    return $model->find($id, $fields);
  }

  public function getUnprocessedDisbursement() {
    return $this->model
      ->whereIn('status', [
        ApiDisbursement::PENDING,
        ApiDisbursement::PENDING_NOT_ENOUGH_DEPOSIT
      ])
      ->get();
  }

  public function getLastDisbursementDateByUserIds($userIds) {
    return $this->getCloneModel()
      ->whereIn('user_id', $userIds)
      ->select('user_id', \DB::raw('max(created_at) as last_date'))
      ->groupBy('user_id')->get()->keyBy('user_id');
  }

  public function gets() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel();

    if (isset($filters['search'])) {
      if (isset($filters['search']['id'])) {
        $query = $query->where('id', '=', $filters['search']['id']);
      }
      
      if (isset($filters['search']['reference_id'])) {
        $query = $query->where('reference_id', '=', $filters['search']['reference_id']);
      }

      if (isset($filters['search']['user_id'])) {
        $query = $query->where('user_id', '=', $filters['search']['user_id']);
      }

      if (isset($filters['search']['bank_id'])) {
        $query = $query->where('bank_id', '=', $filters['search']['bank_id']);
      }

      if (isset($filters['search']['status'])) {
        if ($filters['search']['status'] != ApiDisbursement::SUSPECT) {
          $query = $query->where('status', '=', $filters['search']['status']);
        } else {
          $query = $query->whereIn('status', [ApiDisbursement::SUSPECT, ApiDisbursement::SUSPECT_EVENTUALLY_SUCCESS]);
        }
      }


      if (isset($filters['search']['auto_disbursement_vendor'])) {
        $query = $query->where('auto_disbursement_vendor', '=', $filters['search']['auto_disbursement_vendor']);
      }
    }

    if ($this->hasExpand('bank')) {
      $query = $query->with('bank');
    }

    if ($this->hasExpand('user')) {
      $query = $query->with('user');
    }

    return $this->getResult($query->orderBy($filters['sort_by'], $filters['sort_type']));
  }

  public function getAllByUserId($userId) {
    $filters = $this->getFilters();
    $query = $this->getCloneModel()
      ->where('user_id', $userId)
      ->orderBy('id', 'desc');

    if (isset($filters['id']) && $filters['id'] != '') {
      $query->where('id', '=', $filters['id']);
    }

    if (isset($filters['reference_id']) && $filters['reference_id'] != '') {
      $query->where('reference_id', $filters['reference_id']);
    }

    if (isset($filters['status']) && $filters['status'] != '') {
      $query->where('status', '=', $filters['status']);
    }

    if (isset($filters['start_date']) && $filters['start_date']) {
      $query->where('created_at', '>=', $filters['start_date'] . ' 00:00:00');
    }

    if (isset($filters['end_date']) && $filters['end_date']) {
      $query->where('created_at', '<=', $filters['end_date'] . ' 00:00:00');
    }

    if ($this->hasExpand('bank')) {
      $query = $query->with('bank');
    }

    return $this->getResult($query);
  }

  public function getAllByIdList($idList) {
    return $this->getCloneModel()
      ->whereIn('id', $idList)
      ->get();
  }

  public function listByBatchDisbursementId($batchDisbursementId) {
    $filters = $this->getFilters();
    $query = $this->getCloneModel()
      ->where('batch_disbursement_id', $batchDisbursementId)
      ->orderBy('id', 'desc');

    if (isset($filters['reference_id']) && $filters['reference_id'] != '') {
      $query->where('reference_id', '=', $filters['reference_id']);
    }

    if (isset($filters['status']) && $filters['status'] != '') {
      $query->where('status', '=', $filters['status']);
    }

    if (isset($filters['status_list']) && !empty($filters['status_list'])) {
      $query->whereIn('status', $filters['status_list']);
    }

    if (isset($filters['bank_id']) && $filters['bank_id'] != '') {
      $query->where('bank_id', '=', $filters['bank_id']);
    }

    if (isset($filters['bank_id']) && $filters['bank_id'] != '') {
      $query->where('bank_id', '=', $filters['bank_id']);
    }

    if (isset($filters['start_date']) && $filters['start_date']) {
      $query->where('created_at', '>=', $filters['start_date'] . ' 00:00:00');
    }

    if (isset($filters['end_date']) && $filters['end_date']) {
      $query->where('created_at', '<=', $filters['end_date'] . ' 00:00:00');
    }

    if ($this->hasExpand('bank')) {
      $query = $query->with('bank');
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function isReadyByBatchDisbursementId($batchDisbursementId) {
    return !$this->getModel()
      ->where('batch_disbursement_id', $batchDisbursementId)
      ->where('status', '!=', ApiDisbursement::VALIDATION_SUCCESS)
      ->exists();
  }

  public function getReport() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel()
      ->with('bank')
      ->where('user_id', $filters['user_id'])
      ->whereDate('created_at', '>=', $filters['start_date'])
      ->whereDate('created_at', '<=', $filters['end_date'])
      ->orderBy('id', 'asc');

    return $this->getResult($query);
  }

  public function findByUserIdAndReferenceId($userId, $referenceId) {
    return $this->getCloneModel()
      ->where('user_id', $userId)
      ->where('reference_id', $referenceId)
      ->first();
  }

  public function getInvoiceRows($userId, $period) {
    return $this->getCloneModel()
      ->select(\DB::raw('sum(fee) as amount, count(*) as quantity, fee as price'))
      ->where('status', ApiDisbursement::COMPLETED)
      ->where('user_id', $userId)
      ->where(\DB::raw("to_char(created_at, 'YYYY-MM')"), $period)
      ->groupBy('fee')
      ->get();
  }

  public function getSuspectNotRunning() {
    return $this->getCloneModel()
      ->whereIn('status', [ApiDisbursement::PENDING, ApiDisbursement::PENDING_NOT_ENOUGH_DEPOSIT,
        ApiDisbursement::ON_PROGRESS, ApiDisbursement::FAIL_PROCESSING, ApiDisbursement::SUSPECT, ApiDisbursement::SUSPECT_EVENTUALLY_SUCCESS])
      ->where('created_at', '<', Carbon::now()->subMinutes(10))
      ->get();
  }

  public function getSuspect() {
    return $this->getCloneModel()
      ->whereIn('status', [ApiDisbursement::SUSPECT, ApiDisbursement::SUSPECT_EVENTUALLY_SUCCESS])
      ->where('auto_disbursement_vendor', ApiDisbursement::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT)
      ->where('created_at', '<', Carbon::now()->subMinutes(10))
      ->get();
  }

  public function getSuccessfulDisbursement($date, $isIncludeBatch = true) {
    $query = $this->getCloneModel()
      ->whereDate('verified_at', '=', $date)
      ->where('status', ApiDisbursement::COMPLETED);

    if (!$isIncludeBatch) $query->whereNull('batch_disbursement_id');

    return $query
      ->select(\DB::raw('auto_disbursement_vendor, sum(amount + fee) as gross_amount, sum(fee - cost) as surplus, count(*) as order_total'))
      ->orderBy(\DB::raw('gross_amount'), 'desc')
      ->groupBy('auto_disbursement_vendor')->get();
  }

  public function getSuccessfulDisbursementRecipients($date) {
    return $this->getCloneModel()
      ->select(\DB::raw('banks.id, banks.name, sum(amount) as gross_amount, count(*) as order_total, count(*) * 100 / sum(count(*)) over() as percentage'))
      ->join('banks', 'banks.id', '=', 'api_disbursements.bank_id')
      ->whereDate('verified_at', '=', $date)
      ->where('status', ApiDisbursement::COMPLETED)
      ->orderBy(\DB::raw('order_total'), 'desc')
      ->groupBy('banks.id', 'banks.name')->get();
  }

  public function getTopUsers($date) {
    return $this->getCloneModel()
      ->select(\DB::raw('users.name, count(*) as transaction_total, sum(amount) as gross_amount, count(*) * 100 / sum(count(*)) over() as percentage'))
      ->join('users', 'users.id', '=', 'api_disbursements.user_id')
      ->whereDate('verified_at', '=', $date)
      ->where('api_disbursements.status', ApiDisbursement::COMPLETED)
      ->orderBy(\DB::raw('transaction_total'), 'desc')
      ->limit(10)
      ->groupBy('users.name')->get();
  }

  public function getSumByMonth($date) {
    $query = "
      SELECT
        to_char(verified_at, 'YYYY-MM') AS period,
        sum(amount + fee)              AS gmv,
        sum(fee - cost)                AS revenue,
        count(id)                      AS trx
      FROM api_disbursements
      WHERE verified_at IS NOT NULL AND status=:status
          AND batch_disbursement_id IS NULL
          AND verified_at::date BETWEEN :start_date and :end_date
      GROUP BY period";

    return \DB::select(\DB::raw($query), [
      'status' => ApiDisbursement::COMPLETED,
      'start_date' => $date->copy()->subYear()->format('Y-m-d'),
      'end_date' => $date->format('Y-m-d'),
    ]);
  }

  public function getSumBatchByMonth($date) {
    $query = "
      SELECT
        to_char(verified_at, 'YYYY-MM') AS period,
        sum(amount + fee)              AS gmv,
        sum(fee - cost)                AS revenue,
        count(id)                      AS trx
      FROM api_disbursements
      WHERE verified_at IS NOT NULL AND status=:status
          AND batch_disbursement_id IS NOT NULL
          AND verified_at::date BETWEEN :start_date and :end_date
      GROUP BY period";

    return \DB::select(\DB::raw($query), [
      'status' => ApiDisbursement::COMPLETED,
      'start_date' => $date->copy()->subYear()->format('Y-m-d'),
      'end_date' => $date->format('Y-m-d'),
    ]);
  }

  public function getDelayedList() {
    return $this->getCloneModel()
      ->whereNotNull('delayed_at')
      ->get();
  }

  public function getPendingAmount($vendor, $date) {
    return $this->getCloneModel()
      ->select(\DB::raw('sum(amount) as amount'))
      ->where('auto_disbursement_vendor', $vendor)
      ->where(function ($query) use ($date) {
        return $query
          ->where('updated_at', '>=', $date)
          ->orWhereIn('status', [ApiDisbursement::PENDING, ApiDisbursement::ON_PROGRESS]);
      })
      ->first();
  }

  public function updateCost($vendor, $date, $startId, $cost) {
    return $this->getCloneModel()
      ->where('auto_disbursement_vendor', $vendor)
      ->whereDate("created_at", $date)
      ->where('id', '>=', $startId)
      ->where('cost', '<>', $cost)
      ->update([
        'cost' => $cost,
        'updated_at' => Carbon::now()->toDateTimeString(),
      ]);
  }

  public function getCountByVendor($date) {
    return $this->getCloneModel()
      ->select('auto_disbursement_vendor', \DB::raw('count(*)'))
      ->where('created_at', '>=', $date)
      ->groupBy('auto_disbursement_vendor')
      ->get();
  }

  public function getDisbursementIDListForSuspectedAJ() {
    $query = "
      SELECT *
      FROM api_disbursements
      WHERE api_disbursements.status = '80000' AND
        exists(
          SELECT 1
          FROM disbursement_artajasa_disbursements
          WHERE api_disbursements.id = disbursement_reference_id
            AND disbursement_artajasa_transfer_id IS NULL
            AND disbursement_artajasa_disbursements.status != '50000'
        )";

    return \DB::select(\DB::raw($query));
  }

  public function getSummaryByUserId($userId) {
    $filters = $this->getFilters();
    $query = $this->getCloneModel()
      ->groupBy('status')
      ->where('user_id', $userId)
      ->selectRaw('status, count(*) as count, sum(amount) as sum');

    if (isset($filters['id']) && $filters['id'] != '') {
      $query->where('id', '=', $filters['id']);
    }

    if (isset($filters['status']) && $filters['status'] != '') {
      $query->where('status', '=', $filters['status']);
    }

    if (isset($filters['start_date']) && $filters['start_date']) {
      $query->where('created_at', '>=', $filters['start_date'] . ' 00:00:00');
    }

    if (isset($filters['end_date']) && $filters['end_date']) {
      $query->where('created_at', '<=', $filters['end_date'] . ' 00:00:00');
    }

    return $query->get();
  }

  public function getSummaryByBatchDisbursementId($batchDisbursementId) {
    $filters = $this->getFilters();
    $query = $this->getCloneModel()
      ->groupBy('status')
      ->where('batch_disbursement_id', $batchDisbursementId)
      ->selectRaw('status, count(*) as count, sum(amount) as sum');

    if (isset($filters['reference_id']) && $filters['reference_id'] != '') {
      $query->where('reference_id', '=', $filters['reference_id']);
    }

    if (isset($filters['status']) && $filters['status'] != '') {
      $query->where('status', '=', $filters['status']);
    }

    if (isset($filters['bank_id']) && $filters['bank_id'] != '') {
      $query->where('bank_id', '=', $filters['bank_id']);
    }

    if (isset($filters['bank_id']) && $filters['bank_id'] != '') {
      $query->where('bank_id', '=', $filters['bank_id']);
    }

    if (isset($filters['start_date']) && $filters['start_date']) {
      $query->where('created_at', '>=', $filters['start_date'] . ' 00:00:00');
    }

    if (isset($filters['end_date']) && $filters['end_date']) {
      $query->where('created_at', '<=', $filters['end_date'] . ' 00:00:00');
    }

    return $query->get();
  }

  public function getTopBanksByUserId($userId, $limit = 5) {
    return $this->getCloneModel()
      ->selectRaw('bank_id, count(*) as count')
      ->where('user_id', $userId)
      ->where('status', ApiDisbursement::COMPLETED)
      ->groupBy('bank_id')
      ->orderByDesc('count')
      ->limit($limit)
      ->get();
  }

  public function getSummaryByUserIdAndPeriod($userId, $period, $date) {
    return $this->model
      ->where('user_id', $userId)
      ->where('status', ApiDisbursement::COMPLETED)
      ->where('created_at', '>=', $date)
      ->selectRaw('date_trunc(?, created_at) as period, count(*) as count, sum(amount) as sum', [$period])
      ->groupBy('period')
      ->orderBy('period')
      ->get();
  }

  public function listByUserIdAndReferenceIds($userId, $referenceIds) {
    return $this->model
      ->where('user_id', $userId)
      ->whereIn('reference_id', $referenceIds)
      ->select('reference_id')
      ->where('status', '<>', ApiDisbursement::CANCELLED)
      ->get();
  }

  public function sumAndCountByBatchDisbursementId($batchDisbursementId) {
    return $this->model
      ->where('batch_disbursement_id', $batchDisbursementId)
      ->selectRaw('count(*) as count, sum(amount) as sum, sum(fee) as sum_fee')
      ->first();
  }

  public function updateValidatingStatusByBatchDisbursementId($batchDisbursementId, $status) {
    return $this->model
      ->where('batch_disbursement_id', $batchDisbursementId)
      ->whereIn('status', ApiDisbursement::VALIDATION_STATUS_LIST)
      ->update(['status' => $status]);
  }

  public function isNotCompletedByBatchDisbursementId($batchDisbursementId) {
    return $this->model
      ->where('batch_disbursement_id', $batchDisbursementId)
      ->whereNotIn('status', ApiDisbursement::COMPLETED_STATUS_LIST)
      ->exists();
  }

  public function isValidationNotCompletedByBatchDisbursementId($batchDisbursementId) {
    return $this->model
      ->where('batch_disbursement_id', $batchDisbursementId)
      ->whereNotIn('status', ApiDisbursement::COMPLETED_VALIDATION_STATUS_LIST)
      ->exists();
  }
}