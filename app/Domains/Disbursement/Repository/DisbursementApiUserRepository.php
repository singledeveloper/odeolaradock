<?php

namespace Odeo\Domains\Disbursement\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\Model\DisbursementApiUser;

class DisbursementApiUserRepository extends Repository {

  public function __construct(DisbursementApiUser $disbursementApiUser) {
    $this->model = $disbursementApiUser;
  }

  public function findByAccessKeyId($accessKeyId) {
    $model = $this->getCloneModel();
    return $model->where('access_key_id', $accessKeyId)->first();
  }

  public function findByUserId($userId) {
    $model = $this->getCloneModel();
    return $model->where('user_id', $userId)->first();
  }

}