<?php


namespace Odeo\Domains\Disbursement\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\Model\UserDisbursementFee;

class UserDisbursementFeeRepository extends Repository {

  public function __construct(UserDisbursementFee $model) {
    $this->setModel($model);
  }

  public function findByUserIdAndBankId($userId, $bankId) {
    return $this->model
      ->where('user_id', $userId)
      ->where('bank_id', $bankId)
      ->first();
  }
}