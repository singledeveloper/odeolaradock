<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/3/17
 * Time: 10:17 PM
 */

namespace Odeo\Domains\Disbursement\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\Model\VendorDisbursement;

class VendorDisbursementRepository extends Repository {

  private $lock = false;


  public function __construct(VendorDisbursement $vendorDisbursement) {
    $this->model = $vendorDisbursement;
  }

  public function lock() {
    $this->lock = true;
  }

  public function findById($id, $fields = ['*']) {
    $model = $this->getCloneModel();
    if ($this->lock) $model = $this->model->lockForUpdate();
    return $model->find($id, $fields);
  }

  public function getByIds($ids) {
    return $this->model->whereIn('id', $ids)->get();
  }

}