<?php

namespace Odeo\Domains\Disbursement\Withdrawal\Artajasa\Helper;

use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\ArtajasaMutationDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Disbursement\Withdrawal\Artajasa\Jobs\TransferWithdraw;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\ArtajasaBalanceUpdater;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaDisbursementRepository;

class ExecuteTransfer {

  private $artajasaDisbursementRepo;
  private $userWithdraws;
  private $balanceUpdater;

  public function __construct() {
    $this->userWithdraws = app()->make(UserWithdrawRepository::class);
    $this->artajasaDisbursementRepo = app()->make(DisbursementArtajasaDisbursementRepository::class);
    $this->balanceUpdater = app()->make(ArtajasaBalanceUpdater::class);
  }

  public function execute($withdraw) {
    $artajasaId = null;

    if ($withdraw->auto_disbursement_reference_id) {
      $artajasaId = $withdraw->auto_disbursement_reference_id;
    } else {
      $artajasa = $this->artajasaDisbursementRepo->getNew();

      $artajasa->disbursement_type = Disbursement::DISBURSEMENT_FOR_WITHDRAW_OCASH;
      $artajasa->disbursement_reference_id = $withdraw->id;
      $artajasa->status = ArtajasaDisbursement::PENDING;

      $this->artajasaDisbursementRepo->save($artajasa);

      $artajasaId = $artajasa->id;

      $withdraw->auto_disbursement_reference_id = $artajasa->id;
      $withdraw->auto_disbursement_vendor = Withdraw::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT;
    }

    $withdraw->status = Withdraw::ON_PROGRESS_AUTO_TRANSFER;

    $this->userWithdraws->save($withdraw);

    $this->balanceUpdater->updateBalance([
      'amount' => $withdraw->amount - $withdraw->fee,
      'fee' => ArtajasaDisbursement::COST,
      'type' => ArtajasaMutationDisbursement::TYPE_TRANSFER,
      'disbursement_artajasa_disbursement_id' => $artajasaId,
    ]);

    dispatch(new TransferWithdraw([
      'withdraw_id' => $withdraw->id,
      'artajasa_disbursement_id' => $artajasaId,
      'amount' => $withdraw->amount - $withdraw->fee
    ]));
  }

}