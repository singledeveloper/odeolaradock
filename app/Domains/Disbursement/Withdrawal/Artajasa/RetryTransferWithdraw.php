<?php

namespace Odeo\Domains\Disbursement\Withdrawal\Artajasa;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\ArtajasaMutationDisbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\ArtajasaBalanceUpdater;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaDisbursementRepository;

class RetryTransferWithdraw {

  private $userWithdrawRepo, $artajasaBalanceUpdater, $artajasaDisbursementRepo, $redis;

  public function __construct() {
    $this->userWithdrawRepo = app()->make(UserWithdrawRepository::class);
    $this->artajasaBalanceUpdater = app()->make(ArtajasaBalanceUpdater::class);
    $this->artajasaDisbursementRepo = app()->make(DisbursementArtajasaDisbursementRepository::class);
    $this->redis = Redis::connection();
  }

  public function retry(PipelineListener $listener, $data) {
    if (!($withdraw = $this->userWithdrawRepo->findById($data['withdraw_id']))) {
      return $listener->response(400);
    }

    if ($withdraw->status == Withdraw::COMPLETED) {
      return $listener->response(400);
    }

    if (!($ajDisbursement = $this->artajasaDisbursementRepo->findById($data['artajasa_disbursement_id']))) {
      return $listener->response(400);
    }

    if ($ajDisbursement->status == ArtajasaDisbursement::SUCCESS || $ajDisbursement->status == ArtajasaDisbursement::TIMEOUT) {
      return $listener->response(400);
    }

    $this->redis->hdel('odeo_core:artajasa_disbursement_lock', 'id_' . $data['artajasa_disbursement_id']);
    $this->redis->hdel('odeo_core:auto_wd_lock', 'wd_id_' . $data['withdraw_id']);

    $ajDisbursement->transaction_key = null;
    $ajDisbursement->last_inquire_date = null;
    $ajDisbursement->disbursement_artajasa_transfer_id = null;
    $ajDisbursement->disbursement_artajasa_transfer_inquiry_id = null;
    $ajDisbursement->status = ArtajasaDisbursement::PENDING;
    $this->artajasaDisbursementRepo->save($ajDisbursement);

    if ($withdraw->status != Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
      $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER;
      $this->userWithdrawRepo->save($withdraw);
    }

    $this->artajasaBalanceUpdater->updateBalance([
      'amount' => $withdraw->amount - $withdraw->fee,
      'fee' => ArtajasaDisbursement::COST,
      'type' => ArtajasaMutationDisbursement::TYPE_REFUND,
      'disbursement_artajasa_disbursement_id' => $withdraw->auto_disbursement_reference_id,
    ]);

    return $listener->response(200);
  }

}