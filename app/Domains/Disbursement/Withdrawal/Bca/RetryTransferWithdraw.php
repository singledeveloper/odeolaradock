<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/8/17
 * Time: 11:07 PM
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Bca;


use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\PipelineListener;
use Illuminate\Support\Facades\Redis;

class RetryTransferWithdraw {

  private $userWithdraws, $bcaDisbursements, $redis;

  public function __construct() {

    $this->userWithdraws = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);

    $this->bcaDisbursements = app()->make(\Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository::class);

    $this->redis = Redis::connection();

  }


  public function retry(PipelineListener $listener, $data) {

    if (!($withdraw = $this->userWithdraws->findById($data['withdraw_id']))) return $listener->response(400);
    if ($withdraw->status == Withdraw::COMPLETED) return $listener->response(400);

    if (!($bcaDisbursement = $this->bcaDisbursements->findById($data['bca_disbursement_id']))) return $listener->response(400);
    if ($bcaDisbursement->status == BcaDisbursement::SUCCESS) return $listener->response(400);

    $this->redis->hdel('odeo_core:bca_disbursement_lock', 'id_' . $data['bca_disbursement_id']);
    $this->redis->hdel('odeo_core:auto_wd_lock', 'wd_id_' . $data['withdraw_id']);

    if (strlen($bcaDisbursement->transaction_key) || strlen($bcaDisbursement->bca_status) || strlen($bcaDisbursement->response_datetime)) {
      $bcaDisbursement->transaction_key = null;
      $bcaDisbursement->bca_status = null;
      $bcaDisbursement->response_datetime = null;
      $this->bcaDisbursements->save($bcaDisbursement);
    }

    if ($withdraw->status != Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
      $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER;
      $this->userWithdraws->save($withdraw);
    }

    return $listener->response(200);
  }

}