<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/8/17
 * Time: 11:07 PM
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Bni;

use Odeo\Domains\Constant\BniDisbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\PipelineListener;
use Illuminate\Support\Facades\Redis;

class RetryTransferWithdraw {

  private $userWithdraws, $bniDisbursements, $redis;

  public function __construct() {

    $this->userWithdraws = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);

    $this->bniDisbursements = app()->make(\Odeo\Domains\Vendor\Bni\Repository\DisbursementBniDisbursementRepository::class);

    $this->redis = Redis::connection();

  }


  public function retry(PipelineListener $listener, $data) {

    if (!($withdraw = $this->userWithdraws->findById($data['withdraw_id']))) return $listener->response(400);
    if ($withdraw->status == Withdraw::COMPLETED) return $listener->response(400);

    if (!($bniDisbursement = $this->bniDisbursements->findById($data['bni_disbursement_id']))) return $listener->response(400);
    if ($bniDisbursement->status == BniDisbursement::SUCCESS) return $listener->response(400);


    $this->redis->hdel('odeo_core:bni_disbursement_lock', 'id_' . $data['bni_disbursement_id']);
    $this->redis->hdel('odeo_core:auto_wd_lock', 'wd_id_' . $data['withdraw_id']);

    $bniDisbursement->error_message = null;
    $bniDisbursement->status = null;
    $bniDisbursement->response_datetime = null;
    $bniDisbursement->response_code = null;
    $bniDisbursement->response_message = null;

    $this->bniDisbursements->save($bniDisbursement);

    if ($withdraw->status != Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
      $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER;
      $this->userWithdraws->save($withdraw);
    }

    return $listener->response(200);
  }

}