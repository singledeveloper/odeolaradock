<?php

namespace Odeo\Domains\Disbursement\Withdrawal\Bri\Helper;

use Odeo\Domains\Constant\BriDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Disbursement\Withdrawal\Bri\Jobs\TransferWithdraw;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Vendor\Bri\Repository\DisbursementBriDisbursementRepository;

class ExecuteTransfer {

  private $userWithdrawRepo, $briDisbursementRepo;

  public function __construct() {
    $this->userWithdrawRepo = app()->make(UserWithdrawRepository::class);
    $this->briDisbursementRepo = app()->make(DisbursementBriDisbursementRepository::class);
  }

  public function execute($withdraw) {

    $briId = null;

    if ($withdraw->auto_disbursement_reference_id) {

      $briId = $withdraw->auto_disbursement_reference_id;

    } else {

      $bri = $this->briDisbursementRepo->getNew();

      $bri->amount = $withdraw->amount - $withdraw->fee;
      $bri->disbursement_type = Disbursement::DISBURSEMENT_FOR_WITHDRAW_OCASH;
      $bri->disbursement_reference_id = $withdraw->id;
      $bri->status = BriDisbursement::PENDING;

      $this->briDisbursementRepo->save($bri);

      $briId = $bri->id;

      $withdraw->auto_disbursement_reference_id = $briId;
      $withdraw->auto_disbursement_vendor = Withdraw::DISBURSEMENT_VENDOR_BRI_API_DISBURSEMENT;
    }

    $withdraw->status = Withdraw::ON_PROGRESS_AUTO_TRANSFER;

    $this->userWithdrawRepo->save($withdraw);

    dispatch(new TransferWithdraw([
      'withdraw_id' => $withdraw->id,
      'bri_disbursement_id' => $briId
    ]));
  }

}
