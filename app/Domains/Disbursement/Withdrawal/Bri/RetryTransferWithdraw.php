<?php

namespace Odeo\Domains\Disbursement\Withdrawal\Bri;

use Odeo\Domains\Constant\BriDisbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\PipelineListener;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Vendor\Bri\Repository\DisbursementBriDisbursementRepository;

class RetryTransferWithdraw {

  private $userWithdrawRepo, $briDisbursementRepo, $redis;

  public function __construct() {

    $this->userWithdrawRepo = app()->make(UserWithdrawRepository::class);

    $this->briDisbursementRepo = app()->make(DisbursementBriDisbursementRepository::class);

    $this->redis = Redis::connection();

  }


  public function retry(PipelineListener $listener, $data) {

    if (!($withdraw = $this->userWithdrawRepo->findById($data['withdraw_id']))) return $listener->response(400);
    if ($withdraw->status == Withdraw::COMPLETED) return $listener->response(400);

    if (!($briDisbursement = $this->briDisbursementRepo->findById($data['bri_disbursement_id']))) return $listener->response(400);
    if ($briDisbursement->status == BriDisbursement::SUCCESS) return $listener->response(400);


    $this->redis->hdel('odeo_core:bri_disbursement_lock', 'id_' . $data['bri_disbursement_id']);
    $this->redis->hdel('odeo_core:auto_wd_lock', 'wd_id_' . $data['withdraw_id']);

    $briDisbursement->error_description = null;
    $briDisbursement->status = null;
    $briDisbursement->response_datetime = null;
    $briDisbursement->response_code = null;
    $briDisbursement->response_description = null;

    $this->briDisbursementRepo->save($briDisbursement);

    if ($withdraw->status != Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
      $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER;
      $this->userWithdrawRepo->save($withdraw);
    }

    return $listener->response(200);
  }

}
