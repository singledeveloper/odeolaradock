<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/25/17
 * Time: 3:43 PM
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Cimb\Helper;

use Odeo\Domains\Constant\CimbDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Disbursement\Withdrawal\Cimb\Jobs\TransferWithdraw;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Vendor\Cimb\Repository\DisbursementCimbDisbursementRepository;

class ExecuteTransfer {

  private $userWithdrawRepo, $cimbDisbursementRepo;

  public function __construct() {
    $this->userWithdrawRepo = app()->make(UserWithdrawRepository::class);
    $this->cimbDisbursementRepo = app()->make(DisbursementCimbDisbursementRepository::class);
  }

  public function execute($withdraw) {

    if ($withdraw->auto_disbursement_reference_id) {

      $cimbId = $withdraw->auto_disbursement_reference_id;

    } else {

      $cimb = $this->cimbDisbursementRepo->getNew();

      $cimb->amount = $withdraw->amount - $withdraw->fee;
      $cimb->disbursement_type = Disbursement::DISBURSEMENT_FOR_WITHDRAW_OCASH;
      $cimb->disbursement_reference_id = $withdraw->id;
      $cimb->status = CimbDisbursement::STATUS_PENDING;

      $this->cimbDisbursementRepo->save($cimb);

      $cimbId = $cimb->id;
      $withdraw->auto_disbursement_reference_id = $cimbId;
      $withdraw->auto_disbursement_vendor = Withdraw::DISBURSEMENT_VENDOR_CIMB_API_DISBURSEMENT;

    }

    $withdraw->status = Withdraw::ON_PROGRESS_AUTO_TRANSFER;
    $this->userWithdrawRepo->save($withdraw);

    dispatch(new TransferWithdraw([
      'withdraw_id' => $withdraw->id,
      'cimb_disbursement_id' => $cimbId
    ]));
  }
}
