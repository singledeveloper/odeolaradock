<?php

namespace Odeo\Domains\Disbursement\Withdrawal\Cimb;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\CimbDisbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Vendor\Cimb\Repository\DisbursementCimbDisbursementRepository;

class RetryTransferWithdraw {

  private $userWithdrawRepo, $cimbDisbursementRepo, $redis;

  public function __construct() {

    $this->userWithdrawRepo = app()->make(UserWithdrawRepository::class);

    $this->cimbDisbursementRepo = app()->make(DisbursementCimbDisbursementRepository::class);

    $this->redis = Redis::connection();

  }


  public function retry(PipelineListener $listener, $data) {

    if (!($withdraw = $this->userWithdrawRepo->findById($data['withdraw_id']))) return $listener->response(400);
    if ($withdraw->status == Withdraw::COMPLETED) return $listener->response(400);

    if (!($cimbDisbursement = $this->cimbDisbursementRepo->findById($data['cimb_disbursement_id']))) return $listener->response(400);
    if ($cimbDisbursement->status == CimbDisbursement::STATUS_SUCCESS) return $listener->response(400);

    $this->redis->hdel('odeo_core:cimb_disbursement_lock', 'id_' . $data['cimb_disbursement_id']);
    $this->redis->hdel('odeo_core:auto_wd_lock', 'wd_id_' . $data['withdraw_id']);

    $cimbDisbursement->error_log_response = null;
    $cimbDisbursement->status = null;
    $cimbDisbursement->status_code = null;
    $cimbDisbursement->status_message = null;
    $cimbDisbursement->response_datetime = null;
    $cimbDisbursement->error_code = null;
    $cimbDisbursement->error_message = null;

    $this->cimbDisbursementRepo->save($cimbDisbursement);

    if ($withdraw->status != Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
      $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER;
      $this->userWithdrawRepo->save($withdraw);
    }

    return $listener->response(200);
  }

}
