<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/27/17
 * Time: 5:28 PM
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Flip;


use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Withdrawal\MiscRequester;
use Odeo\Domains\Transaction\WithdrawCanceller;
use Odeo\Domains\Transaction\WithdrawRequester;
use Odeo\Domains\Vendor\Flip\Exceptions\FlipException;

class FlipNotifyReceiver {

  public function receiveNotify(PipelineListener $listener, $data) {

    $result = $data['data'];

    switch ($result['status']) {
      case 'DONE':
        $listener->addNext(new Task(WithdrawRequester::class, 'complete', [
          'withdraw_id' => $data['withdraw_id']
        ]));
        break;
      case 'WRONG_ACCOUNT_NUMBER':
        $listener->addNext(new Task(WithdrawCanceller::class, 'cancel', [
          'withdraw_id' => $data['withdraw_id'],
          'withdraw_status' => Withdraw::CANCELLED_FOR_WRONG_ACCOUNT_NUMBER
        ]));
        break;
      default:
        throw new FlipException('status not found');
        break;
    }

    $listener->addNext(new Task(MiscRequester::class, 'removeRedisLock', [
      'withdraw_id' => $data['withdraw_id']
    ]));

    return $listener->response(200);
  }

}