<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 26/03/19
 * Time: 14.26
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Mandiri\Helper;


use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\MandiriDisbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Disbursement\Withdrawal\Mandiri\Job\TransferWithdraw;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Vendor\Mandiri\Repository\DisbursementMandiriDisbursementRepository;

class ExecuteTransfer {
  /**
   * @var UserWithdrawRepository
   */
  private $userWithdrawRepo;

  /**
   * @var DisbursementMandiriDisbursementRepository
   */
  private $mandiriDisbursementRepo;

  private function init() {
    $this->userWithdrawRepo = app()->make(UserWithdrawRepository::class);
    $this->mandiriDisbursementRepo = app()->make(DisbursementMandiriDisbursementRepository::class);
  }

  public function execute($withdraw) {
    $this->init();

    if ($withdraw->auto_disbursement_reference_id) {
      $mandiriId = $withdraw->auto_disbursement_reference_id;
    } else {
      $mandiri = $this->mandiriDisbursementRepo->getNew();
      $mandiri->amount = $withdraw->amount - $withdraw->fee;
      $mandiri->disbursement_type = Disbursement::DISBURSEMENT_FOR_WITHDRAW_OCASH;
      $mandiri->disbursement_reference_id = $withdraw->id;
      $mandiri->status = MandiriDisbursement::PENDING;

      $this->mandiriDisbursementRepo->save($mandiri);
      $mandiriId = $mandiri->id;

      $withdraw->auto_disbursement_reference_id = $mandiriId;
      $withdraw->auto_disbursement_vendor = Withdraw::DISBURSEMENT_VENDOR_MANDIRI_API_DISBURSEMENT;
    }

    $withdraw->status = Withdraw::ON_PROGRESS_AUTO_TRANSFER;
    $this->userWithdrawRepo->save($withdraw);

    dispatch(new TransferWithdraw([
      'withdraw_id' => $withdraw->id,
      'mandiri_disbursement_id' => $mandiriId
    ]));
  }
}