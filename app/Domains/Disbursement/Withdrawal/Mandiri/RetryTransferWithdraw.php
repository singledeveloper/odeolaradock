<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 26/03/19
 * Time: 14.26
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Mandiri;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\MandiriDisbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Vendor\Mandiri\Repository\DisbursementMandiriDisbursementRepository;

class RetryTransferWithdraw {

  /**
   * @var UserWithdrawRepository,
   * @var DisbursementMandiriDisbursementRepository,
   * @var Redis
   */
  private $userWithdraws, $mandiriDisbursements, $redis;

  public function init() {
    $this->userWithdraws = app()->make(UserWithdrawRepository::class);
    $this->mandiriDisbursements = app()->make(DisbursementMandiriDisbursementRepository::class);
    $this->redis = Redis::connection();
  }

  public function retry(PipelineListener $listener, $data) {
    $this->init();

    if (!($withdraw = $this->userWithdraws->findById($data['withdraw_id']))) return $listener->response(400);
    if ($withdraw->status == Withdraw::COMPLETED) return $listener->response(400);

    if (!($mandiriDisbursement = $this->mandiriDisbursements->findById($data['mandiri_disbursement_id']))) return $listener->response(400);
    if ($mandiriDisbursement ->status == MandiriDisbursement::SUCCESS) return $listener->response(400);

    $this->redis->hdel('odeo_core:mandiri_disbursement_lock', 'id_' . $data['mandiri_disbursement_id']);
    $this->redis->hdel('odeo_core:auto_wd_lock', 'wd_id_' . $data['withdraw_id']);

    $mandiriDisbursement->status = MandiriDisbursement::PENDING;
    $mandiriDisbursement->cust_reff_no = null;
    $mandiriDisbursement->request_time = null;
    $mandiriDisbursement->response_code = null;
    $mandiriDisbursement->response_message = null;
    $mandiriDisbursement->response_timestamp = null;
    $mandiriDisbursement->response_remmittance_no = null;
    $mandiriDisbursement->is_sign_verified = false;
    $mandiriDisbursement->response_error_message = null;
    $mandiriDisbursement->response_error_number = null;
    $mandiriDisbursement->ex_error_message = null;

    $this->mandiriDisbursements->save($mandiriDisbursement);
    $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER;
    $this->userWithdraws->save($withdraw);

    return $listener->response(200);
  }

}