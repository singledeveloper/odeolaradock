<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/11/17
 * Time: 5:08 PM
 */

namespace Odeo\Domains\Disbursement\Withdrawal;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;

class MarkAsSuccessRequester {

  private $userWithdraws, $notifications;

  public function __construct() {
    $this->userWithdraws =  app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);
    $this->notifications = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
  }

  public function markAsSuccess(PipelineListener $listener, $data) {

    if ($data['password'] != env('DISBURSEMENT_MARK_AS_SUCCESS_PASSWORD')) return $listener->response(400);

    $listener->addNext(new Task(\Odeo\Domains\Transaction\WithdrawRequester::class, 'complete'));

    return $listener->response(200);

  }

}