<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/11/17
 * Time: 5:08 PM
 */

namespace Odeo\Domains\Disbursement\Withdrawal;

use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Withdrawal\Mandiri\RetryTransferWithdraw;

class RetryRequester {


  public function retry(PipelineListener $listener, $data) {

    if ($data['password'] != env('DISBURSEMENT_RETRY_PASSWORD')) return $listener->response(400);

    switch ($data['auto_disbursement_vendor']) {

      case Withdraw::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT:
        $listener->addNext(new Task(\Odeo\Domains\Disbursement\Withdrawal\Bca\RetryTransferWithdraw::class, 'retry', [
          'bca_disbursement_id' => $data['auto_disbursement_reference_id']
        ]));
        break;

      case Withdraw::DISBURSEMENT_VENDOR_BNI_API_DISBURSEMENT:
        $listener->addNext(new Task(\Odeo\Domains\Disbursement\Withdrawal\Bni\RetryTransferWithdraw::class, 'retry', [
          'bni_disbursement_id' => $data['auto_disbursement_reference_id']
        ]));
        break;

      case Withdraw::DISBURSEMENT_VENDOR_BRI_API_DISBURSEMENT:
        $listener->addNext(new Task(\Odeo\Domains\Disbursement\Withdrawal\Bri\RetryTransferWithdraw::class, 'retry', [
          'bri_disbursement_id' => $data['auto_disbursement_reference_id']
        ]));
        break;

      case Withdraw::DISBURSEMENT_VENDOR_FLIP_API_DISBURSEMENT:
        $listener->addNext(new Task(\Odeo\Domains\Disbursement\Withdrawal\Flip\RetryTransferWithdraw::class, 'retry', [
          'flip_disbursement_id' => $data['auto_disbursement_reference_id']
        ]));
        break;

      case Withdraw::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT:
        $listener->addNext(new Task(\Odeo\Domains\Disbursement\Withdrawal\Artajasa\RetryTransferWithdraw::class, 'retry', [
          'artajasa_disbursement_id' => $data['auto_disbursement_reference_id']
        ]));
        break;

      case Withdraw::DISBURSEMENT_VENDOR_CIMB_API_DISBURSEMENT:
        $listener->addNext(new Task(\Odeo\Domains\Disbursement\Withdrawal\Cimb\RetryTransferWithdraw::class, 'retry', [
          'cimb_disbursement_id' => $data['auto_disbursement_reference_id']
        ]));
        break;

      case Withdraw::DISBURSEMENT_VENDOR_PERMATA_API_DISBURSEMENT:
        $listener->addNext(new Task(\Odeo\Domains\Disbursement\Withdrawal\Permata\RetryTransferWithdraw::class, 'retry', [
          'permata_disbursement_id' => $data['auto_disbursement_reference_id']
        ]));
        break;

      case Withdraw::DISBURSEMENT_VENDOR_MANDIRI_API_DISBURSEMENT:
        $listener->addNext(new Task(RetryTransferWithdraw::class, 'retry', [
          'mandiri_disbursement_id' => $data['auto_disbursement_reference_id']
        ]));
        break;

    }

    return $listener->response(200);

  }

}
