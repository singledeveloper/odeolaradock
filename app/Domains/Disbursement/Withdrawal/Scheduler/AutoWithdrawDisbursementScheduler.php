<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/10/17
 * Time: 9:46 PM
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Scheduler;

use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\BriDisbursement;
use Odeo\Domains\Constant\FlipDisbursement;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository;
use Odeo\Domains\Disbursement\Withdrawal\Artajasa\Helper\ExecuteTransfer as ExecuteTransferArtajasa;
use Odeo\Domains\Disbursement\Withdrawal\Bca\Helper\ExecuteTransfer as ExecuteTransferBca;
use Odeo\Domains\Disbursement\Withdrawal\Bni\Helper\ExecuteTransfer as ExecuteTransferBni;
use Odeo\Domains\Disbursement\Withdrawal\Bri\Helper\ExecuteTransfer as ExecuteTransferBri;
use Odeo\Domains\Disbursement\Withdrawal\Cimb\Helper\ExecuteTransfer as ExecuteTransferCimb;
use Odeo\Domains\Disbursement\Withdrawal\Flip\Helper\ExecuteTransfer as ExecuteTransferFlip;
use Odeo\Domains\Disbursement\Withdrawal\Mandiri\Helper\ExecuteTransfer as ExecuteTransferMandiri;
use Odeo\Domains\Disbursement\Withdrawal\Permata\Helper\ExecuteTransfer as ExecuteTransferPermata;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Vendor\Bca\Helper\ApiManager as BcaApiManager;

class AutoWithdrawDisbursementScheduler {

  private $userWithdrawRepo, $bankRepo, $vendorDisbursementRepo;

  public function __construct() {
    $this->userWithdrawRepo = app()->make(UserWithdrawRepository::class);
    $this->bankRepo = app()->make(BankRepository::class);
    $this->vendorDisbursementRepo = app()->make(VendorDisbursementRepository::class);
  }

  public function run() {
    $unProcessWithdraws = [];

    $availableArtajasaBankIdMap = $this->bankRepo->getATMBersamaBankList()
      ->keyBy('id');

    foreach ($this->userWithdrawRepo->getUnProcessedAutoWithdraw() as $withdraw) {
      $bankId = $withdraw->bank->id;

      switch ($bankId) {
        case Bank::BCA:
          ($unProcessWithdraws[VendorDisbursement::BCA][] = $withdraw);
          break;

        case Bank::CIMBNIAGA:
          $unProcessWithdraws[VendorDisbursement::CIMB][] = $withdraw;
          break;

        case Bank::BNI:
          $unProcessWithdraws[VendorDisbursement::BNI][] = $withdraw;
          break;

        case Bank::BRI:
          $unProcessWithdraws[VendorDisbursement::BRI][] = $withdraw;
          break;
          
        case Bank::PERMATA:
          if (strlen($withdraw->account_number) <= 12) {
            $unProcessWithdraws[VendorDisbursement::PERMATA][] = $withdraw;
          }
          break;
        case Bank::MANDIRI:
          $unProcessWithdraws[VendorDisbursement::MANDIRI][] = $withdraw;
          break;
        default:
          if (isProduction()) {
            if (isset($availableArtajasaBankIdMap[$bankId])) {
              $unProcessWithdraws[VendorDisbursement::ARTAJASA][] = $withdraw;
            }
            break;
          }
          break;
      }
    }

    FlipDisbursement::isOperational() &&
    count((isset($unProcessWithdraws[VendorDisbursement::FLIP]) ?
      $unProcessWithdraws[VendorDisbursement::FLIP] : [])) && $this->processFlip($unProcessWithdraws[VendorDisbursement::FLIP]);

    BcaDisbursement::isOperational() &&
    count((isset($unProcessWithdraws[VendorDisbursement::BCA]) ?
      $unProcessWithdraws[VendorDisbursement::BCA] : [])) && $this->processApiBca($unProcessWithdraws[VendorDisbursement::BCA]);

    count((isset($unProcessWithdraws[VendorDisbursement::CIMB]) ?
      $unProcessWithdraws[VendorDisbursement::CIMB] : [])) && $this->processApiCimb($unProcessWithdraws[VendorDisbursement::CIMB]);

    count((isset($unProcessWithdraws[VendorDisbursement::ARTAJASA]) ?
      $unProcessWithdraws[VendorDisbursement::ARTAJASA] : [])) && $this->processArtajasa($unProcessWithdraws[VendorDisbursement::ARTAJASA]);

    count((isset($unProcessWithdraws[VendorDisbursement::BNI]) ?
      $unProcessWithdraws[VendorDisbursement::BNI] : [])) && $this->processApiBni($unProcessWithdraws[VendorDisbursement::BNI]);

    BriDisbursement::isOperational() && count((isset($unProcessWithdraws[VendorDisbursement::BRI]) ?
      $unProcessWithdraws[VendorDisbursement::BRI] : [])) && $this->processApiBri($unProcessWithdraws[VendorDisbursement::BRI]);

    count((isset($unProcessWithdraws[VendorDisbursement::PERMATA]) ?
      $unProcessWithdraws[VendorDisbursement::PERMATA] : [])) && $this->processApiPermata($unProcessWithdraws[VendorDisbursement::PERMATA]);

    count((isset($unProcessWithdraws[VendorDisbursement::MANDIRI]) ?
      $unProcessWithdraws[VendorDisbursement::MANDIRI] : [])) && $this->processApiMandiri($unProcessWithdraws[VendorDisbursement::MANDIRI]);
  }

  private function processApiBca($apiBcaWithdraws) {
    $remainingBalance = $this->vendorDisbursementRepo->findById(VendorDisbursement::BCA)
      ->balance;

    foreach ($apiBcaWithdraws as $withdraw) {
      $amount = $withdraw->amount + $withdraw->fee;
      $balanceAfter = $remainingBalance - $amount;

      if ($balanceAfter >= 0) {
        app()->make(ExecuteTransferBca::class)
          ->execute($withdraw);
        $remainingBalance = $balanceAfter;

      } else if ($withdraw->status == Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
        $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER_NOT_ENOUGH_DEPOSIT;
        $this->userWithdrawRepo->save($withdraw);
      }
    }
  }

  private function processApiBni($apiBniWithdraws) {
    $remainingBalance = $this->vendorDisbursementRepo->findById(VendorDisbursement::BNI)
      ->balance;

    foreach ($apiBniWithdraws as $withdraw) {
      $amount = $withdraw->amount + $withdraw->fee;
      $balanceAfter = $remainingBalance - $amount;

      if ($balanceAfter >= 0) {
        $remainingBalance = $balanceAfter;
        app()->make(ExecuteTransferBni::class)
          ->execute($withdraw);

      } else if ($withdraw->status == Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
        $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER_NOT_ENOUGH_DEPOSIT;
        $this->userWithdrawRepo->save($withdraw);
      }
    }
  }

  private function processApiBri($apiBriWithdraws) {
    $remainingBalance = $this->vendorDisbursementRepo->findById(VendorDisbursement::BRI)
      ->balance;

    foreach ($apiBriWithdraws as $withdraw) {
      $amount = $withdraw->amount + $withdraw->fee;
      $balanceAfter = $remainingBalance - $amount;

      if ($balanceAfter >= 0) {
        $remainingBalance = $balanceAfter;
        app()->make(ExecuteTransferBri::class)
          ->execute($withdraw);

      } else if ($withdraw->status == Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
        $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER_NOT_ENOUGH_DEPOSIT;
        $this->userWithdrawRepo->save($withdraw);
      }
    }
  }

  private function processFlip($flipWithdraws) {
    $flipRemainingBalance = $this->vendorDisbursementRepo->findById(VendorDisbursement::FLIP)
      ->balance;

    foreach ($flipWithdraws as $flipWithdraw) {
      $withdraw = $flipWithdraw['withdraw'];
      $bankId = $flipWithdraw['bank_id'];
      $amount = $withdraw->amount - $withdraw->fee;
      $balanceAfter = $flipRemainingBalance - $amount;

      if ($balanceAfter >= 0) {
        $flipRemainingBalance = $balanceAfter;
        app()->make(ExecuteTransferFlip::class)
          ->execute($withdraw, $bankId);
      } else {
        if ($withdraw->status == Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
          $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER_NOT_ENOUGH_DEPOSIT;
          $this->userWithdrawRepo->save($withdraw);
        }
      }

    }
  }

  private function processArtajasa($withdraws) {
    $remainingBalance = $this->vendorDisbursementRepo->findById(VendorDisbursement::ARTAJASA)
      ->balance;

    foreach ($withdraws as $withdraw) {
      $balanceAfter = $remainingBalance - $withdraw->amount - ArtajasaDisbursement::COST;

      if ($balanceAfter >= 0) {
        $remainingBalance = $balanceAfter;
        app()->make(ExecuteTransferArtajasa::class)
          ->execute($withdraw);
      } else if ($withdraw->status == Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
        $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER_NOT_ENOUGH_DEPOSIT;
        $this->userWithdrawRepo->save($withdraw);
      }
    }
  }


  private function processApiCimb($cimbWithdraws) {
    $remainingBalance = app()->make(VendorDisbursementRepository::class)
      ->findById(VendorDisbursement::CIMB)
      ->balance;

    foreach ($cimbWithdraws as $withdraw) {
      $balanceAfter = $remainingBalance - $withdraw->amount;

      if ($balanceAfter >= 0) {
        $remainingBalance = $balanceAfter;
        app()->make(ExecuteTransferCimb::class)
          ->execute($withdraw);
      } else if ($withdraw->status == Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
        $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER_NOT_ENOUGH_DEPOSIT;
        $this->userWithdrawRepo->save($withdraw);
      }

    }
  }

  private function processApiPermata($permataWithdraws) {
    $remainingBalance = $this->vendorDisbursementRepo
      ->findById(VendorDisbursement::PERMATA)->balance;

    foreach ($permataWithdraws as $withdraw) {
      $balanceAfter = $remainingBalance - $withdraw->amount;

      if ($balanceAfter >= 0) {
        $remainingBalance = $balanceAfter;
        app()->make(ExecuteTransferPermata::class)
          ->execute($withdraw);
      } else if ($withdraw->status == Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
        $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER_NOT_ENOUGH_DEPOSIT;
        $this->userWithdrawRepo->save($withdraw);
      }
    }
  }

  private function processApiMandiri($mandiriWithdraws) {
    $remainingBalance = $this->vendorDisbursementRepo
      ->findById(VendorDisbursement::MANDIRI)->balance;

    foreach ($mandiriWithdraws as $withdraw) {
      $balanceAfter = $remainingBalance - $withdraw->amount;

      if ($balanceAfter >= 0) {
        $remainingBalance = $balanceAfter;
        app()->make(ExecuteTransferMandiri::class)
          ->execute($withdraw);
      } else if ($withdraw->status == Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
        $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER_NOT_ENOUGH_DEPOSIT;
        $this->userWithdrawRepo->save($withdraw);
      }
    }
  }

}
