<?php

namespace Odeo\Domains\Exporter\Helper;

use Odeo\Domains\Constant\DataExporterType;
use Odeo\Domains\Exporter\PdfExporter;
use Odeo\Domains\Exporter\SpreadsheetExporter;

class DataExporter {

  public function __construct() {
    $this->spreadsheetExporter = app()->make(SpreadsheetExporter::class);
    $this->pdfExporter = app()->make(PdfExporter::class);
  }

  public function generate($data, $fileType, $viewKey) {

    switch ($fileType) {
      case DataExporterType::PDF:
        return $this->pdfExporter->generate($data, $viewKey);

      case DataExporterType::CSV:
      case DataExporterType::XLSX:
      case DataExporterType::ODS:
        return $this->spreadsheetExporter->generate($data, $fileType);
    }

    return null;
  }

}
