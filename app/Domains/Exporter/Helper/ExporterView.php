<?php

namespace Odeo\Domains\Exporter\Helper;

use Odeo\Domains\Constant\DataExporterView;

class ExporterView {

  const map = [
    DataExporterView::PRICE_LIST_KEY => DataExporterView::PRICE_LIST_VIEW,
    DataExporterView::API_DISBURSEMENT_INVOICE_KEY => DataExporterView::API_DISBURSEMENT_INVOICE_VIEW,
    DataExporterView::PAYMENT_GATEWAY_INVOICE_KEY => DataExporterView::PAYMENT_GATEWAY_INVOICE_VIEW,
  ];

  public function getViewName($key) {
    return self::map[$key];
  }

}
