<?php

namespace Odeo\Domains\Exporter;

use Odeo\Domains\Exporter\Helper\ExporterView;

class PdfExporter {

  public function generate($data, $viewKey) {

    $exporterView = app()->make(ExporterView::class);

    $pdf = new \TCPDI();
    $pdf->AddPage();

    $view = $exporterView->getViewName($viewKey);
    $pdf->writeHTML(view($view)->with('data', $data));

    $file = $pdf->getPDFData();

    return $file;
  }

}
