<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 6/22/17
 * Time: 9:58 PM
 */

namespace Odeo\Domains\Faq;

use Odeo\Domains\Core\PipelineListener;

class FaqRemover {
  private $faqs;

  public function __construct() {
    $this->faqs = app()->make(\Odeo\Domains\Faq\Repository\FaqRepository::class);
  }

  public function remove(PipelineListener $listener, $data) {
    $this->faqs->getModel()
      ->where('priority', '>', \DB::raw('(select priority from faqs where id = '.$data['faq_id'].')'))
      ->update(['priority' => \DB::raw('priority - 1')]);
    $this->faqs->deleteById($data['faq_id']);
    return $listener->response(200);
  }
}