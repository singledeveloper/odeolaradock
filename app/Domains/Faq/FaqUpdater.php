<?php

namespace Odeo\Domains\Faq;


use Odeo\Domains\Core\PipelineListener;

class FaqUpdater {

  private $faqs;

  public function __construct() {
    $this->faqs = app()->make(\Odeo\Domains\Faq\Repository\FaqRepository::class);
  }

  public function update(PipelineListener $listener, $data) {

    if($faq = $this->faqs->findById($data['faq_id'])) {
      $faq->question = $data['question'];
      $faq->answer = $data['answer'];
      $faq->group = isset($data['group']) ? $data['group'] : '';
      $faq->slug = strtolower(trim($data['slug']));
      $this->faqs->save($faq);

      return $listener->response(200);
    }
    return $listener->response(204);
  }

  public function updatePriority(PipelineListener $listener, $data) {
    if ($data['current_priority'] < $data['new_priority']) {
      $this->faqs->getModel()->whereBetween('priority', [$data['current_priority'], $data['new_priority']])->update(['priority' => \DB::raw('priority - 1')]);
    } else if ($data['current_priority'] > $data['new_priority']) {
      $this->faqs->getModel()->whereBetween('priority', [$data['new_priority'], $data['current_priority']])->update(['priority' => \DB::raw('priority + 1')]);
    }
    $this->faqs->getModel()->where('id', '=', $data['faq_id'])->update(['priority' => $data['new_priority']]);
    return $listener->response(200);
  }

}