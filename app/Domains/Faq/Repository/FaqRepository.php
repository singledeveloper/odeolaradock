<?php

namespace Odeo\Domains\Faq\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Faq\Model\Faq;

class FaqRepository extends Repository {

  public function __construct(Faq $faq) {
    $this->model = $faq;
  }

  public function get() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel();
    if (isset($filters['group']) && $filters['group'] != '') $query = $query->where('group', '=', $filters['group']);
    return $query->orderBy('priority')->get();
  }

  // this function exists for BI compliance purpose, currently used on "ocash" app
  public function getOcashFaq() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel();
    if (isset($filters['group']) && $filters['group'] != '') $query = $query->where('group', '=', $filters['group']);
    return $query
      ->whereIn('id', [8, 9, 33, 34, 35, 37, 38])
      ->orderBy('priority')->get();
  }

}
