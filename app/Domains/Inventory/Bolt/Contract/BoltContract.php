<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 5:25 PM
 */

namespace Odeo\Domains\Inventory\Bolt\Contract;


use Odeo\Domains\Core\PipelineListener;

interface BoltContract {

  public function get(PipelineListener $listener, $data);

  public function checkout(PipelineListener $listener, $data);

  public function purchase(PipelineListener $listener, $data);

  public function validateInventory(PipelineListener $listener, $data);

}