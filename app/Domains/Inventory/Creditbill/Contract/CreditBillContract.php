<?php

namespace Odeo\Domains\Inventory\Creditbill\Contract;


use Odeo\Domains\Core\PipelineListener;

interface CreditBillContract {

  public function setBiller($biller);

  public function getPayableAmount(PipelineListener $listener, $data);

  public function request(PipelineListener $listener, $data);

  public function checkout(PipelineListener $listener, $data);

  public function completeOrder(PipelineListener $listener, $data);
  
}