<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 6:03 PM
 */

namespace Odeo\Domains\Inventory\Creditbill;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;

class CreditBillManager extends InventoryManager {

  private static $creditBillInstance;

  public function setVendor(PipelineListener $listener, $data) {

    self::$creditBillInstance = app()->make(\Odeo\Domains\Inventory\Creditbill\Odeo\OdeoManager::class);
    $this->setServiceDetail($data['service_detail_id']);

    return $listener->response(200, []);

  }


  public function __call($name, $argument) {
    $this->setVendor($argument[0], $argument[1]);
    self::$creditBillInstance->setBiller($argument[1]['biller']);
    return call_user_func_array([self::$creditBillInstance, $name], $argument);
  }




}