<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 10/1/16
 * Time: 3:08 PM
 */

namespace Odeo\Domains\Inventory\Creditbill\Odeo\Biller\Pundi;


use Odeo\Domains\Core\PipelineListener;

class InventoryValidator {

  public function __construct() {
    $this->pundiCreditBills = app()->make(\Odeo\Domains\Inventory\Creditbill\Odeo\Biller\Pundi\Repository\PundiCreditBillRepository::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    $creditBill = $this->pundiCreditBills->findById($data['credit_bill_id']);

    if ($creditBill) {
      $formatted = $this->marginFormatter->formatMargin($creditBill->amount);

      return $listener->response(200, array_merge($formatted, [
        'name' => 'Pundi-Pundi Credit Bill'
      ]));
    }
  }
}
