<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 12/21/16
 * Time: 9:28 PM
 */

namespace Odeo\Domains\Inventory\Creditbill\Odeo\Biller\Pundi\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Creditbill\Odeo\Biller\Pundi\Model\PundiCreditBill;

class PundiCreditBillRepository extends Repository {

  public function __construct(PundiCreditBill $pundiCreditBill) {
    $this->model = $pundiCreditBill;
  }
}