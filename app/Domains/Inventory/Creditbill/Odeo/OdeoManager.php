<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 5:16 PM
 */

namespace Odeo\Domains\Inventory\Creditbill\Odeo;

use Odeo\Domains\Constant\CreditBillOdeo;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Creditbill\Contract\CreditBillContract;

class OdeoManager implements CreditBillContract {

  private $instance;

  public function __construct() {

  }

  public function setBiller($biller) {
    if ($biller == CreditBillOdeo::PUNDI_BILLER) {
      $this->instance = app()->make(\Odeo\Domains\Inventory\Creditbill\Odeo\Biller\Pundi\PundiManager::class);
    }
  }

  public function getPayableAmount(PipelineListener $listener, $data) {

  }

  public function validateInventory(PipelineListener $listener, $data) {
    return $this->instance->validateInventory($listener, $data);
  }

  public function request(PipelineListener $listener, $data) {
    return $this->instance->request($listener, $data);
  }

  public function checkout(PipelineListener $listener, $data) {
    return $this->instance->checkout($listener, $data);
  }

  public function completeOrder(PipelineListener $listener, $data) {

  }
}