<?php

namespace Odeo\Domains\Inventory\EMoney\Contract;


use Odeo\Domains\Core\PipelineListener;

interface EMoneyContract {

  public function get(PipelineListener $listener, $data);

  public function checkout(PipelineListener $listener, $data);

  public function purchase(PipelineListener $listener, $data);

  public function validateInventory(PipelineListener $listener, $data);

}