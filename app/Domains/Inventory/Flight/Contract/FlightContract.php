<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/20/16
 * Time: 11:10 PM
 */

namespace Odeo\Domains\Inventory\Flight\Contract;


use Odeo\Domains\Core\PipelineListener;

interface FlightContract {

  public function getAirport(PipelineListener $listener, $data);

  public function searchFlight(PipelineListener $listener, $data);

  public function getBookingForm(PipelineListener $listener, $data);

  public function addToCart(PipelineListener $listener, $data);

  public function removeCart(PipelineListener $listener, $data);

  public function checkout(PipelineListener $listener, $data);

  public function purchase(PipelineListener $listener, $data);
}