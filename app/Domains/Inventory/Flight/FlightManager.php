<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/20/16
 * Time: 11:10 PM
 */

namespace Odeo\Domains\Inventory\Flight;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;


class FlightManager extends InventoryManager   {

  private static $flightInstance;

  public function setVendor(PipelineListener $listener, $data) {

    self::$flightInstance = app()->make(\Odeo\Domains\Inventory\Flight\Tiket\TiketManager::class);

    $this->setServiceDetail($data['service_detail_id']);

    return $listener->response(200);

  }


  public function __call($name, $argument) {
    $this->setVendor($argument[0], $argument[1]);
    return call_user_func_array([self::$flightInstance, $name], $argument);
  }




}