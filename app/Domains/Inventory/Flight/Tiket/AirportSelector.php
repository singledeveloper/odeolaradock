<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/21/16
 * Time: 8:25 PM
 */

namespace Odeo\Domains\Inventory\Flight\Tiket;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class AirportSelector implements SelectorListener {

  private $tiketAirports;

  public function __construct() {

    $this->tiketAirports = app()->make(\Odeo\Domains\Inventory\Flight\Tiket\Repository\TiketAirportRepository::class);

  }

  public function _transforms($item, Repository $repository) {

    return [
      'code' => $item->id,
      'name' => trim($item->location . ' (' . $item->country_name) . ')'
    ];

  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getAirport(PipelineListener $listener) {

    $airports = [];

    foreach ($this->tiketAirports->getAll() as $item) {
      $airports[] = $this->_transforms($item, $this->tiketAirports);
    }

    if (count($airports) > 0) {
      return $listener->response(200, [
        'airports' => $airports
      ]);
    }
    return $listener->response(204);
  }
}