<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/21/16
 * Time: 1:53 PM
 */

namespace Odeo\Domains\Inventory\Flight\Tiket;


use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\Tiket;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\CartInserter;
use Validator;

class BookingFormRequester {

  private $connector, $tokenManager, $orderHelper, $countries, $airlines,
    $tiketOrders, $marginFormatter, $currencyHelper;

  public function __construct() {

    $this->tokenManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\TokenManager::class);
    $this->connector = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Connector::class);
    $this->airlines = app()->make(\Odeo\Domains\Inventory\Flight\Tiket\Repository\TiketAirlineRepository::class);
    $this->countries = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository\TiketCountryRepository::class);
    $this->orderHelper = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Helper\OrderHelper::class);
    $this->tiketOrders = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository\OrderDetailTiketRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);

  }

  public function submitForm(PipelineListener $listener, $data) {


    list($token) = $this->tokenManager->getToken(['user_id' => $data['auth']['user_id']]);

    if ($token['status'] != Tiket::STATUS_SUCCESS) {
      return $listener->response($token['status_code'], $token['message']);
    }

    $query = $data['item_detail'];

    $query['token'] = $token['data']['token'];
    $query['output'] = 'json';

    $this->connector->request('GET', 'tiket', '/order/add/flight', $query);
    $orders = $this->connector->response([
      200 => function ($body) {
        return $body;
      },
    ])[0];

    if ($orders['status'] != Tiket::STATUS_SUCCESS) {

      return $listener->response($orders['status_code'], $orders['message']);
    }

    if ($orderData = $this->orderHelper->getOrder($data)) {

      $cartData = json_decode($data['cart']['cart_data'], true);
      $validateCart = isset($cartData['items']) && count($cartData['items']) > 0;

      foreach ($orderData['data']['data'] as $order) {

        $skip = false;

        if ($validateCart) {
          foreach ($cartData['items'] as $item) {
            if ($item['inventory_id'] == $order['order_detail_id']) {
              $skip = true;
              break;
            }
          }
        }

        if ($skip) continue;

        $adultPrice = $childPrice = $infantPrice = $adultCredit = $childCredit = $infantCredit = 0;

        foreach ($order['detail']['breakdown_price'] as $breakdownPrice) {

          if ($breakdownPrice['category'] == 'price adult') {

            list($adultPrice, $adultCredit, $margin) = $this->marginFormatter->formatMargin($breakdownPrice['value']);

            $adultPrice = $adultPrice * count($order['detail']['passengers']['adult']);
            $adultCredit = $adultCredit * count($order['detail']['passengers']['adult']);

          } else if ($breakdownPrice['category'] == 'price child') {

            list($childPrice, $childCredit, $margin) = $this->marginFormatter->formatMargin($breakdownPrice['value']);

            $childPrice = $childPrice * count($order['detail']['passengers']['child']);
            $childCredit = $childCredit * count($order['detail']['passengers']['child']);

          } else if ($breakdownPrice['category'] == 'price infant') {

            list($infantPrice, $infantCredit, $margin) = $this->marginFormatter->formatMargin($breakdownPrice['value']);


            $infantPrice = $infantPrice * count($order['detail']['passengers']['infant']);
            $infantCredit = $infantCredit * count($order['detail']['passengers']['infant']);
          }
        }


        $totalPrice = $adultPrice + $childPrice + $infantPrice + $order['detail']['baggage_fee'];
        $totalCredit = $adultCredit + $childCredit + $infantCredit + $order['detail']['baggage_fee'];

        $listener->addNext(new Task(CartInserter::class, 'addItem', [
          'sale_price' => $totalPrice,
          'base_price' => $totalCredit,
          'margin' => $margin,
          'name' => $order['order_name'],
          'item_detail' => [
            'order' => array_merge($order, [
              'tiket_order_id' => $orderData['data']['order_id'],
              'tiket_order_detail_id' => $orderData['data']['data'][0]['order_detail_id']
            ]),
            'booking_data' => $data['item_detail'],
          ],
          'cart' => $data['cart'],
          'item_id' => $order['order_detail_id'],
          'service_id' => $data['service_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));

      }

      return $listener->response(200);

    }

    return $listener->response(400);

  }


  public function getBookingForm(PipelineListener $listener, $data) {

    $validator = Validator::make($data, [
      // 'user_id' => 'required',
      'flight_id' => 'required',
      'date' => 'required|date',
      'ret_flight_id' => '',
      'ret_date' => 'date',
    ]);

    if ($validator->fails()) {
      return $listener->response(400, $validator->errors()->all());
    }

    $data['user_id'] = 1;

    list($token) = $this->tokenManager->getToken(['user_id' => $data['user_id']]);

    if ($token['status'] != Tiket::STATUS_SUCCESS) {
      return $listener->response($token['status_code'], $token['message']);
    }

    $data['token'] = $token['data']['token'];


    $query = $data;
    $query['output'] = 'json';

    unset($query['user_id']);

    $this->connector->request('GET', 'tiket', '/flight_api/get_flight_data', $query);


    $flightData = $this->connector->response([
      200 => function ($body) {
        return [
          'data' => $body['required'],
        ];
      },
    ])[0];

    if ($flightData['status'] != Tiket::STATUS_SUCCESS) {
      return $listener->response($flightData['status_code'], $flightData['message']);
    }

    $response = [];
    foreach ($flightData['data'] as $key => $value) {
      if ($value['mandatory'] == 1) {
        switch ($value['category']) {
          case 'contact':
            $response[$value['category']][$key]['type'] = $value['type'];
            if ($value['type'] == 'combobox') {
              $response[$value['category']][$key]['resource'] = $value['resource'];
            }
            break;
          case 'adult1':
            $key = substr($key, 0, -1);
            $value['category'] = substr($value['category'], 0, -1);

            $response[$value['category']][$key]['type'] = $value['type'];
            if ($value['type'] == 'combobox') {
              if ($key == 'passportissuinga' || $key == 'passportnationalitya') {
                $response[$value['category']][$key]['resource'] = $this->countries->getAll();
              } else {
                $response[$value['category']][$key]['resource'] = $value['resource'];
              }
            }
            break;
          case 'child1':
            $key = substr($key, 0, -1);
            $value['category'] = substr($value['category'], 0, -1);

            $response[$value['category']][$key]['type'] = $value['type'];
            if ($value['type'] == 'combobox') {
              if ($key == 'passportissuingc' || $key == 'passportnationalityc') {
                $response[$value['category']][$key]['resource'] = $this->countries->getAll();
              } else {
                $response[$value['category']][$key]['resource'] = $value['resource'];
              }
            }
            break;
          case 'infant1':
            $key = substr($key, 0, -1);
            $value['category'] = substr($value['category'], 0, -1);

            $response[$value['category']][$key]['type'] = $value['type'];
            if ($value['type'] == 'combobox') {
              if ($key == 'passportissuingi' || $key == 'passportnationalityi') {
                $response[$value['category']][$key]['resource'] = $this->countries->getAll();
              } else {
                $response[$value['category']][$key]['resource'] = $value['resource'];
              }
            }
            break;
          default:
            break;
        }
      }
    }


    $query = [
      'token' => $data['token'],
      'output' => 'json',
    ];

    $this->connector->request('GET', 'tiket', '/flight_api/getLionCaptcha', $query);
    $captcha = $this->connector->response([
      200 => function ($body) {
        $response = [];
        if (!empty($body['lioncaptcha']) && !empty($body['lionsessionid'])) {
          $response['lioncaptcha'] = $body['lioncaptcha'];
          $response['lionsessionid'] = $body['lionsessionid'];
        }

        return [
          'data' => ['captcha' => $response],
        ];
      },
    ])[0];


    if ($captcha['status'] != Tiket::STATUS_SUCCESS) {
      return $listener->response($captcha['status_code'], $captcha['message']);
    }


    $flightData['data']['captcha'] = $captcha['data']['captcha'];

    return $listener->response($flightData['status_code'], $response);

  }
}