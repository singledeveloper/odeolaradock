<?php

namespace Odeo\Domains\Inventory\Flight\Tiket\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;

class TiketAirport extends Entity {
  use SoftDeletes;

  protected $dates = ['deleted_at'];

  protected $fillable = ['id', 'name', 'location', 'country_id', 'country_name'];

  protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

  protected $casts = ['id' => 'string'];
}
