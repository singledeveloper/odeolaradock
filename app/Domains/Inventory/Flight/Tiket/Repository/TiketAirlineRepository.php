<?php

namespace Odeo\Domains\Inventory\Flight\Tiket\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Flight\Tiket\Model\TiketAirline;

class TiketAirlineRepository extends Repository {

  public function __construct(TiketAirline $airline) {
    $this->model = $airline;
  }

}
