<?php

namespace Odeo\Domains\Inventory\Flight\Tiket\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Flight\Tiket\Model\TiketAirport;

class TiketAirportRepository extends Repository {

  public function __construct(TiketAirport $airport) {
    $this->model = $airport;
  }

}
