<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/20/16
 * Time: 11:15 PM
 */

namespace Odeo\Domains\Inventory\Flight\Tiket;


use Odeo\Domains\Constant\Tiket;
use Odeo\Domains\Core\PipelineListener;
use Validator;

class Searcher {


  private $connector, $tokenManager, $airlines, $marginFormatter;

  public function __construct() {

    $this->tokenManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\TokenManager::class);
    $this->connector = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Connector::class);
    $this->airlines = app()->make(\Odeo\Domains\Inventory\Flight\Tiket\Repository\TiketAirlineRepository::class);

    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function searchFlight(PipelineListener $listener, $data) {

    $validator = Validator::make($data, [
      'origin' => 'required',
      'destination' => 'required',
      'date' => 'required|date',
      'ret_date' => 'date',
      'adult' => 'required|integer|min:1|max:6',
      'child' => 'integer|min:0|max:6',
      'infant' => 'integer|min:0|max:' . $data['adult'],
    ]);


    if ($validator->fails()) {
      return $listener->response(400, $validator->errors()->all());
    }



    $renewToken = false;

    do {

      list($token) = $this->tokenManager->getToken(['user_id' => $data['auth']['user_id']], $renewToken);

      $renewToken = false;


      if ($token['status'] != Tiket::STATUS_SUCCESS) {
        return $listener->response($token['status_code'], $token['message']);
      }

      $data['token'] = $token['data']['token'];

      $query = $data;
      $query['d'] = $query['origin'];
      $query['a'] = $query['destination'];
      $query['v'] = 3;
      $query['output'] = 'json';

      unset($query['auth']['user_id']);
      unset($query['origin']);
      unset($query['destination']);

      $this->connector->request('GET', 'tiket', '/search/flight', $query);
      $response = $this->connector->response([
        200 => function ($body) {
          $response = [];
          if (isset($body['departures']['result'])) {
            $response['departure'] = $body['departures']['result'];
          }
          if (isset($body['returns']['result'])) {
            $response['return'] = $body['returns']['result'];
          }

          return [
            'data' => $response,
          ];
        },
      ])[0];


      if ($response['status'] != Tiket::STATUS_SUCCESS) {

        if (isset($response['message'][0]) && strtolower($response['message'][0]) == 'wrong token') {
          $renewToken = true;
        } else {
          return $listener->response($response['status_code'], $response['message']);
        }

      }

    } while ($renewToken);

    if (!$response['data'] || !count($response['data']['departure']) ||
      isset($data['ret_date']) && (!isset($response['data']['return']) || !count($response['data']['return']))
    ) {
      return $listener->response(204, null);
    }

    $response['data']['airlines'] = $this->airlines->getAll();


    foreach ($response['data']['departure'] as &$departure) {
      $this->_reformatPrice($departure, $data);
    }

    if (isset($response['data']['return'])) {

      foreach ($response['data']['return'] as &$return) {
        $this->_reformatPrice($return, $data);
      }
    }


    return $listener->response($response['status_code'], $response['data']);
  }


  private function _reformatPrice(&$item, $data) {

    $item['price_adult'] = $item['price_adult'] / $data['adult'];
    $item['price_child'] = $data['child'] > 0 ? $item['price_child'] / $data['child'] : $item['price_child'];
    $item['price_infant'] = $data['infant'] > 0 ? $item['price_infant'] / $data['infant'] : $item['price_infant'];

    list($item['price_adult'], $_) = $this->marginFormatter->formatMargin($item['price_adult']);
    list($item['price_child'], $_) = $this->marginFormatter->formatMargin($item['price_child']);
    list($item['price_infant'], $_) = $this->marginFormatter->formatMargin($item['price_infant']);

    $item['price_adult'] = $this->currency->formatPrice($item['price_adult'] * $data['adult']);
    $item['price_child'] = $this->currency->formatPrice($item['price_child'] * $data['child']);
    $item['price_infant'] = $this->currency->formatPrice($item['price_infant'] * $data['infant']);
    $item['price_value'] = $this->currency->formatPrice($item['price_adult']['amount'] + $item['price_child']['amount'] + $item['price_infant']['amount']);

    return $item;

  }

}