<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/18/17
 * Time: 12:07 PM
 */

namespace Odeo\Domains\Inventory\GameVoucher\Contract;


use Odeo\Domains\Core\PipelineListener;

interface GameVoucherContract {

  public function searchNominal(PipelineListener $listener, $data);

  public function purchase(PipelineListener $listener, $data);

  public function checkout(PipelineListener $listener, $data);

  public function validateInventory(PipelineListener $listener, $data);

  public function getAllCategoryByServiceDetailId(PipelineListener $listener, $data);
}