<?php

namespace Odeo\Domains\Inventory\GooglePlay\Contract;

use Odeo\Domains\Core\PipelineListener;

interface GooglePlayContract {

  public function checkout(PipelineListener $listener, $data);

  public function purchase(PipelineListener $listener, $data);

  public function validateInventory(PipelineListener $listener, $data);

}