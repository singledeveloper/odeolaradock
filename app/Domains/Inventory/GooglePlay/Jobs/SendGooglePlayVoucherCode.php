<?php

namespace Odeo\Domains\Inventory\GooglePlay\Jobs;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;


class SendGooglePlayVoucherCode extends Job  {

  private $order, $data;

  public function __construct($order, $data) {
    parent::__construct();
    $this->order = $order;
    $this->data = $data;
  }

  public function handle() {

    if (!$this->order->email) return;

    $orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);
    $items = $orderDetailizer->detailizeItem($this->order);

    $item = $items[0];

    if ($item['service_id'] != \Odeo\Domains\Constant\Service::GOOGLE_PLAY) return;

    Mail::send('emails.google_play_voucher_code', [
      'data' => [
        'order_id' => $this->order->id,
        'purchase_at' => Carbon::parse($this->order->created_at)->toDateTimeString(),
        'name' => $this->order->name,
        'item_name' => $item['name'],
        'serial_number' => $item['item_detail']['serial_number'],
        'voucher_code' => $item['item_detail']['voucher_code'] ?? '-'
      ]
    ], function ($m) {

      $m->from('noreply@odeo.co.id', 'odeo');

      $m->to($this->order->email)->subject('[ODEO] Google Play Voucher Code - ' . $this->order->id);

    });

  }

}
