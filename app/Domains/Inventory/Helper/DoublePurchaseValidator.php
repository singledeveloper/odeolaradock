<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 26/07/18
 * Time: 14.13
 */

namespace Odeo\Domains\Inventory\Helper;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository;
use Odeo\Domains\Order\Helper\StatusParser;
use Odeo\Domains\Order\Model\Order;
use Odeo\Domains\Transaction\Helper\Currency;

class DoublePurchaseValidator {

  const EXPIRE = 3600;
  private $redis;
  private $switcherOrderRepo, $orderStatusParser, $currencyFormatter;

  public function __construct() {
    $this->redis = Redis::connection();
    $this->switcherOrderRepo = app()->make(OrderDetailPulsaSwitcherRepository::class);
    $this->orderStatusParser = app()->make(StatusParser::class);
    $this->currencyFormatter = app()->make(Currency::class);
  }

  //isDoublePurchase
  public function check($itemId, $serviceDetailId, $details, $userId) {
    if (!$userId) { // in case of pulsa recurring, there's no userId in Job
      return false;
    }

    $key = $this->getRedisKey($itemId, $serviceDetailId, $details, $userId);

    if ($this->redis->setnx($key, 0)) {
      $this->redis->expire($key, self::EXPIRE);
      return false;

    } else {
      $lastPurchase = $this->getData($itemId, $serviceDetailId, $details, $userId);

      if (!$lastPurchase) {
        return false;
      }

      $this->redis->incr($key);
      return !!$lastPurchase;
    }
  }

  private function getKeyFromItemDetail($serviceDetailId, $details) {
    switch ($serviceDetailId) {
      default:
        return $details['number'];
    }
  }

  private function getRedisKey($itemId, $serviceDetailId, $details, $userId) {
    $key = $this->getKeyFromItemDetail($serviceDetailId, $details);
    return "{$key}_{$itemId}_{$userId}";
  }

  public function getLastDetectedDoublePurchase($itemId, $serviceDetailId, $details, $userId = null) {
    $lastPurchaseData = $this->getData($itemId, $serviceDetailId, $details, $userId);
    if (!$lastPurchaseData) {
      return null;
    }

    $status = $this->orderStatusParser->parseForNewDesign(['status' => $lastPurchaseData['status']]);
    return array_merge($status, $lastPurchaseData);
  }

  private function getData($itemId, $serviceDetailId, $details, $userId) {
    $doublePurchaseOrder = $this->switcherOrderRepo
      ->getLastSamePurchaseInTime($details['number'], $itemId, $userId);

    if (!$doublePurchaseOrder) {
      return null;
    }

    if ($doublePurchaseOrder->status == OrderStatus::CREATED) {
      return null;
    }

    return [
      'status' => $doublePurchaseOrder->status,
      'order_id' => $doublePurchaseOrder->order_id,
      'name' => $doublePurchaseOrder->name,
      'created_at' => $doublePurchaseOrder->created_at->toDateTimeString(),
      'total' => $this->currencyFormatter->formatPrice($doublePurchaseOrder->total),
      'item_detail' => $this->getDoublePurchaseOrderDetail($serviceDetailId, $doublePurchaseOrder)
    ];
  }

  private function getDoublePurchaseOrderDetail($serviceDetailId, $doublePurchaseOrder) {
    switch ($serviceDetailId) {
      // you can extend here in case of different data returns of serviceDetailId
      default:
        return [
          'number' => $doublePurchaseOrder->number
        ];
    }
  }

}
