<?php

namespace Odeo\Domains\Inventory\Helper\Jobs;

use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaEditor;
use Odeo\Jobs\Job;

class AutoForceSuccess extends Job {

  use SerializesModels;

  private $switcherId;

  public function __construct($switcherId) {
    parent::__construct();
    $this->switcherId = $switcherId;
  }

  public function handle() {
    $pipeline = new Pipeline();
    $pipeline->add(new Task(PulsaEditor::class, 'editSwitcher', [
      "order_detail_pulsa_switcher_id" => $this->switcherId,
      "serial_number" => $this->switcherId,
      "admin_note" => "UPDATE",
      "force_success" => true
    ]));
    $pipeline->execute([]);
  }

}
