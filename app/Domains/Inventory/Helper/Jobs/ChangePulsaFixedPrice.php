<?php

namespace Odeo\Domains\Inventory\Helper\Jobs;

use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaInventoryEditor;
use Odeo\Jobs\Job;

class ChangePulsaFixedPrice extends Job {

  use SerializesModels;

  private $inventoryId, $newInventoryPrice;

  public function __construct($inventoryId, $newInventoryPrice) {
    parent::__construct();
    $this->inventoryId = $inventoryId;
    $this->newInventoryPrice = $newInventoryPrice;
  }

  public function handle() {

    $pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $pulsaInventory = $pulsaInventories->findById($this->inventoryId);

    $pulsaOdeoHistories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoHistoryRepository::class);
    if ($pulsaOdeoHistories->findByPulsaOdeoId($pulsaInventory->pulsa_odeo_id)) return;

    foreach ($pulsaInventories->getByPulsaOdeoId($pulsaInventory->pulsa_odeo_id, 5) as $item) {
      if ($this->newInventoryPrice < $item->base_price) {
        $this->newInventoryPrice = $item->base_price;
      }
    }

    $pulsa = $pulsaInventory->pulsa;
    $newPrice = $this->newInventoryPrice + $pulsa->serviceDetail->referral_cashback + 100;
    if ($newPrice == $pulsa->price) return;

    $pipeline = new Pipeline();
    $pipeline->add(new Task(PulsaInventoryEditor::class, 'edit', [
      "item_id" => $pulsaInventory->pulsa_odeo_id,
      "is_base_price_change_fixed" => "true",
      "base_price" => $newPrice,
      "changed_at" => date('Y-m-d H:i', time() + 300) . ':00'
    ]));
    $pipeline->execute([]);

  }

}
