<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 3:02 PM
 */

namespace Odeo\Domains\Inventory\Helper;

class MarginFormatter {

  private $inventoryManager;

  public function __construct() {
    $this->inventoryManager = app()->make(InventoryManager::class);
  }

  public function formatMargin($amount, $data = []) {

    $serviceDetail = isset($data['service_detail']) ? $data['service_detail'] : $this->inventoryManager->getServiceDetail();
    if ($serviceDetail->margin < 0) $basePrice = $amount + $serviceDetail->margin;
    else $basePrice = $amount;

    $merchantPrice = $basePrice;

    $marginType = $serviceDetail->margin_type;

    $mentorMargin = $serviceDetail->mentor_margin;
    $leaderMargin = $serviceDetail->leader_margin;

    $referralCashback = isset($data['is_own_product']) && $data['is_own_product'] ? 0 : $serviceDetail->referral_cashback;

    if (!isset($data['is_h2h'])) {
      if ($marginType == "percentage") $merchantPrice = $basePrice + max(floor($mentorMargin / 100 * $basePrice), 1) + max(floor($leaderMargin / 100 * $basePrice), 1);
      else if ($marginType == "value") $merchantPrice = $basePrice + $mentorMargin + $leaderMargin;
      $hustlerMargin = $serviceDetail->default_hustler_margin;
    }
    else {
      $hustlerMargin = 0;
      $mentorMargin = 0;
      $leaderMargin = 0;
      $referralCashback = 0;
    }

    $salePrice = $merchantPrice;

    if (!isset($data['is_h2h'])) {
      if ($marginType == "percentage") $salePrice = $merchantPrice + max(floor($hustlerMargin / 100 * $basePrice), 1);
      else if ($marginType == "value") $salePrice = $merchantPrice + $hustlerMargin;
    }

    return [
      'base_price' => $basePrice,
      'merchant_price' => $merchantPrice,
      'sale_price' => $salePrice,
      'margin' => $hustlerMargin,
      'mentor_margin' => $mentorMargin,
      'leader_margin' => $leaderMargin,
      'referral_cashback' => $referralCashback
    ];
  }

  public function formatMarginPostpaid($vendorFee, $total, $multiplier = 1, $serviceDetail = null) {

    $marginData = $this->formatMargin($total, value(function () use ($serviceDetail) {
      return $serviceDetail ? ['service_detail' => $serviceDetail] : [];
    }));

    $fee = $marginData['sale_price'] - $marginData['base_price'];
    $merchantFee = $marginData['merchant_price'] - $marginData['base_price'];
    $margin = $marginData['margin'];
    return [$fee, $total + (($vendorFee + $fee) * $multiplier), $margin, $total + (($vendorFee + $merchantFee) * $multiplier), $marginData['mentor_margin'], $marginData['leader_margin'], $merchantFee];
  }

}
