<?php

namespace Odeo\Domains\Inventory\Helper\Service\Pulsa\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;

class PulsaOperator extends Entity
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];


    public function prefixes()
    {
        return $this->hasMany(PulsaPrefix::class);
    }
}
