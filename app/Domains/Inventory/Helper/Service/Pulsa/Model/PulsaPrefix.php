<?php

namespace Odeo\Domains\Inventory\Helper\Service\Pulsa\Model;

use Odeo\Domains\Core\Entity;

class PulsaPrefix extends Entity {

  protected $hidden = ['created_at', 'updated_at'];

  public function operator() {
    return $this->belongsTo(PulsaOperator::class);
  }
}
