<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 8:15 PM
 */

namespace Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Service\Pulsa\Model\PulsaOperator;

class PulsaOperatorRepository extends Repository {

  public function __construct(PulsaOperator $pulsaOperator) {
    $this->model = $pulsaOperator;
  }

  public function getAll($fields = ['*']) {
    return $this->model->all();
  }

  public function findByCode($code) {
    return $this->model->where('code', $code)->first();
  }

}