<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 8:15 PM
 */

namespace Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Service\Pulsa\Model\PulsaPrefixCluster;

class PulsaPrefixClusterRepository extends Repository {

  public function __construct(PulsaPrefixCluster $pulsaPrefixCluster) {
    $this->model = $pulsaPrefixCluster;
  }

  public function checkNumber($number, $provinceId) {

    return $this->model
      ->whereIn('prefix', [
        substr($number, 0, 7),
        substr($number, 0, 6)
      ])
      ->where('province_id', $provinceId)
      ->first();

  }
}
