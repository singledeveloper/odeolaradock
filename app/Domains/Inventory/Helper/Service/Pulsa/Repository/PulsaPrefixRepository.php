<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 8:15 PM
 */

namespace Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Service\Pulsa\Model\PulsaPrefix;

class PulsaPrefixRepository extends Repository {

  public function __construct(PulsaPrefix $pulsaPrefix) {
    $this->model = $pulsaPrefix;
  }

  public function getNominal($data) {
    $key = $this->model->getTable();

    return $this->runCache($key, $key . '.' . $data['prefix'], 60 * 60 * 24, function () use ($data) {

      $query = $this->getCloneModel();

      return $query->where('prefix', $data['prefix'])->first();
    });

  }
}
