<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper;

use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\MarginFormatter;
use Odeo\Domains\Subscription\ManageInventory\Helper\PriceGetter;

class PostpaidDataManager {

  private $currencyHelper, $priceGetter, $marginFormatter;

  public function __construct() {
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->priceGetter = app()->make(PriceGetter::class);
    $this->marginFormatter = app()->make(MarginFormatter::class);
  }

  public function formatInquiry($data, $inquiry, $inventory) {

    //if (isset($inquiry['error_message'])) return [false, $inquiry['error_message']];

    $storeInventory = null;
    $pulsa = $inventory->pulsa;
    $odeoFee = $pulsa->price;
    $vendorFee = null;
    $result = [];

    if (isset($data['store_id'])) {
      $priceResult = $this->priceGetter->getPriceForInventory($data['store_id'], [
        'service_detail_id' => $pulsa->service_detail_id,
        'user_id' => isset($data['user_id']) ? $data['user_id'] : $data['auth']['user_id'],
        'inventory_ids' => [$pulsa->id],
      ]);

      if (!$priceResult->isEmpty()) {
        $storeInventory = $priceResult->first();
        $vendorFee = $storeInventory->sell_price;
        if (!isset($data['for_bill'])) {
          $result['store_inventory_detail_id'] = $storeInventory->inventory_detail_id;
        }
      }
    }
    list ($fee, $total, $margin, $merchantPrice, $mentorMargin, $leaderMargin, $merchantFee) = $this->marginFormatter->formatMarginPostpaid(
      $odeoFee, $inquiry['biller_price'], $inquiry['multiplier'], $pulsa->serviceDetail);

    $total = $storeInventory ? $merchantPrice + (($vendorFee - $odeoFee - $merchantFee) * $inquiry['multiplier']) : $total;

    $result['item_id'] = $pulsa->id;
    $result['service_detail_id'] = (int)$pulsa->service_detail_id;
    $result['name'] = $inquiry['name'];
    $result['item_detail'] = [
      'subscriber_id' => $inquiry['subscriber_id'],
      'subscriber_name' => $inquiry['subscriber_name'],
      'ref_id' => $inquiry['ref_id']
    ];

    if (isset($result['store_inventory_detail_id'])) {
      $result['agent_base_price'] = value(function () use ($storeInventory, $inquiry, $merchantPrice) {
        if ($storeInventory->base_price) {
          return $this->marginFormatter->formatMarginPostpaid($storeInventory->base_price, $inquiry['biller_price'], $inquiry['multiplier'])[3];
        }
        return $merchantPrice;
      });
      $result['order_extended_detail']['postpaid_multiplier'] = $inquiry['multiplier'];
      $result['order_extended_detail']['postpaid_merchant_fee'] = $merchantFee;
      $result['order_extended_detail']['postpaid_base_fee'] = $odeoFee;
    }

    if (isset($inquiry['tariff'])) $result['item_detail']['tariff'] = $inquiry['tariff'];
    if (isset($inquiry['meter_changes'])) $result['item_detail']['meter_changes'] = $inquiry['meter_changes'];
    if (!isset($data['for_bill']) && !isset($data['for_affiliate'])) {
      $result['status'] = 'SUCCESS';
      $result['base_price'] = $inquiry['biller_price'] + ($odeoFee * $inquiry['multiplier']);
      $result['sale_price'] = $total;
      $result['margin'] = $margin;
      $result['merchant_price'] = $merchantPrice;
      $result['mentor_margin'] = $mentorMargin;
      $result['leader_margin'] = $leaderMargin;
      if (isset($inquiry['details'])) $result['item_detail']['inquiries'] = $inquiry['details'];
      $result['item_detail']['multiplier'] = $inquiry['multiplier'];
      $result['item_detail']['biller_price'] = $inquiry['biller_price'];
    } else if (!isset($data['for_affiliate'])) {
      if (isset($inquiry['details'])) {
        switch ($pulsa->service_detail_id) {
          case ServiceDetail::PLN_POSTPAID_ODEO:
          case ServiceDetail::PULSA_POSTPAID_ODEO:
          case ServiceDetail::BROADBAND_ODEO:
          case ServiceDetail::LANDLINE_ODEO:
          case ServiceDetail::PDAM_ODEO:
            foreach ($inquiry['details'] as $item) {
              $result['item_detail']['inquiries'][] = $this->transformDetails($item);
            }
            $result['item_detail']['total_admin'] = $this->currencyHelper->formatPrice($this->getTotalAdminFee($inquiry));
            $result['item_detail']['month_counts'] = $this->getTotalMonth($inquiry);
            break;

          case ServiceDetail::BPJS_KES_ODEO:
            $detail = $inquiry['details'];
            $result['item_detail']['inquiries']['base_price'] = $this->currencyHelper->formatPrice($detail['base_price']);
            $result['item_detail']['inquiries']['bill_rest'] = $this->currencyHelper->formatPrice($detail['bill_rest']);
            $result['item_detail']['inquiries']['admin'] = $this->currencyHelper->formatPrice($detail['admin']);
            $result['item_detail']['inquiries']['participant_counts'] = $detail['participant_counts'];
            $result['item_detail']['inquiries']['month_counts'] = $detail['month_counts'];
            $result['item_detail']['inquiries']['branch_code'] = $detail['branch_code'];
            $result['item_detail']['inquiries']['branch_name'] = $detail['branch_name'];
            break;

          case ServiceDetail::PGN_ODEO:
            $detail = $inquiry['details'];
            $result['item_detail']['inquiries'] = $this->transformDetails($detail);
            break;

          case ServiceDetail::MULTI_FINANCE_ODEO:
            $detail = $inquiry['details'];
            $result['item_detail']['inquiries']['base_price'] = $this->currencyHelper->formatPrice($detail['base_price']);
            $result['item_detail']['inquiries']['coll_fee'] = isset($detail['coll_fee']) ? array_merge(
              $this->currencyHelper->formatPrice($detail['coll_fee']), [
                'label' => $detail['platform'] == PostpaidType::MULTIFINANCE_TYPE_SYARIAH ? 'Tazir' : 'Coll Fee'
              ]) : null;
            $result['item_detail']['inquiries']['fine'] = array_merge(
              $this->currencyHelper->formatPrice($detail['fine']), [
                'label' => $detail['platform'] == PostpaidType::MULTIFINANCE_TYPE_SYARIAH ? 'Tawidh' : 'Denda'
              ]);
            $result['item_detail']['inquiries']['admin'] = $this->currencyHelper->formatPrice($detail['admin']);
            $result['item_detail']['inquiries']['installment'] = $detail['installment'];
            $result['item_detail']['inquiries']['due_date'] = strtotime($detail['due_date']) ? date('d/m/Y', strtotime($detail['due_date'])) : $detail['due_date'];
            $result['item_detail']['inquiries']['platform'] = $detail['platform'];
            break;

          default:
            $result['item_detail']['inquiries'] = $inquiry['details'];
            break;
        }
      }
      $result['subtotal'] = $this->currencyHelper->formatPrice($inquiry['biller_price']);
      $result['discount'] = $this->currencyHelper->formatPrice($inquiry['biller_price'] - $total);
      $result['total'] = $this->currencyHelper->formatPrice($total);
      $result['item_description'] = '';
      $result['item_link'] = '';
    } else {
      $result['item_detail']['inquiries'] = $inquiry['details'];
      $result['subtotal'] = intval($inquiry['biller_price']);
      $result['discount'] = $inquiry['biller_price'] - $total;
      $result['total'] = $total;
    }

    //return [true, $result];
    return $result;

  }

  private function transformDetails($item) {
    $extends = [
      'period' => $item['period'],
      'base_price' => $this->currencyHelper->formatPrice($item['base_price']),
      'admin_fee' => $this->currencyHelper->formatPrice($item['admin_fee'])
    ];

    if (isset($item['fine'])) $extends['fine'] = $this->currencyHelper->formatPrice($item['fine']);
    if (isset($item['meter_changes'])) $extends['meter_changes'] = $item['meter_changes'];
    if (isset($item['usages'])) $extends['usages'] = $item['usages'];

    return $extends;
  }

  private function getTotalAdminFee($inquiries) {
    if (isset($inquiries['details'])) {
      return array_reduce($inquiries['details'], function ($total, $item) {
        return $total + $item['admin_fee'] ?? 0;
      }, 0);
    }
    return 0;
  }

  private function getTotalMonth($inquiries) {
    if (isset($inquiries['details'])) {
      return count($inquiries['details']);
    }

    return 1;
  }
}
