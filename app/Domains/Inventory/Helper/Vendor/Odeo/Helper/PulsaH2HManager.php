<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;

class PulsaH2HManager {

  public function __construct() {
    $this->redis = Redis::connection();
  }

  public function isUserH2H($userId) {
    if ($userIds = $this->redis->hget(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_USERIDS)) {
      return in_array($userId, json_decode($userIds, true));
    }
    return false;
  }

  public function getUserH2H() {
    if ($userIds = $this->redis->hget(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_USERIDS)) {
      return json_decode($userIds, true);
    }
    return [];
  }
}
