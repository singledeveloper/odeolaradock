<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper;

class PulsaSwitcherDataManager {

  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->pulsaHistories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoHistoryRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->competitorPrices = app()->make(\Odeo\Domains\Marketing\Competitor\Repository\CompetitorPriceRepository::class);
  }

  public function getCompetitorPriceBySKU($inventoryIds) {
    $results = [];

    if ($data = $this->competitorPrices->getBySKU($inventoryIds)) {
      foreach ($data as $item) {
        $results[$item['pulsa_odeo_id']] = [
          'competitor' => $item['competitor'],
          'price' => $this->currencyHelper->formatPrice($item['price']),
          'last_updated' => date('d/m/Y', strtotime((string)$item['created_at']))
        ];
      }
    }
    return $results;
  }

  public function getCountBySKU($inventoryIds, $isInventory = true) {
    $results = [];

    if ($data = $this->switcherOrder->getCountBySKU($inventoryIds, $isInventory)) {
      foreach ($data as $item) {
        $results[$isInventory ? $item->current_inventory_id : $item->pulsa_odeo_id] = $item->order_number_count;
      }
    }
    foreach ($inventoryIds as $item) {
      if (!isset($results[$item])) $results[$item] = 0;
    }
    return $results;
  }

  public function getOrderValueBySKU($inventoryIds, $isInventory = true) {
    $results = [];

    if ($data = $this->switcherOrder->getOrderValueBySKU($inventoryIds, $isInventory)) {
      foreach ($data as $item) {
        $results[$isInventory ? $item->current_inventory_id : $item->pulsa_odeo_id] = $this->currencyHelper->formatPrice($item->order_value);
      }
    }
    foreach ($inventoryIds as $item) {
      if (!isset($results[$item])) $results[$item] = $this->currencyHelper->formatPrice(0);
    }
    return $results;
  }

  public function getSubsidyAmountBySKU($inventoryIds, $isInventory = true) {
    $results = [];

    if ($data = $this->switcherOrder->getSubsidyAmountBySKU($inventoryIds, $isInventory)) {
      foreach ($data as $item) {
        $results[$isInventory ? $item->current_inventory_id : $item->pulsa_odeo_id] = $this->currencyHelper->formatPrice($item->total_subsidy);
      }
    }
    foreach ($inventoryIds as $item) {
      if (!isset($results[$item])) $results[$item] = $this->currencyHelper->formatPrice(0);
    }
    return $results;
  }

  public function getCheapestInventories($inventoryIds, $count = 3) {
    $results = [];

    if ($data = $this->pulsaInventories->getCheapestPriceGroup($inventoryIds, $count)) {
      foreach ($data as $item) {
        $results[$item->pulsa_odeo_id][] = [
          'inventory_id' => $item->id,
          'biller_name' => $item->biller_name,
          'code' => $item->code,
          'base_price' => $this->currencyHelper->formatPrice($item->base_price),
          'priority' => $item->priority_sort,
          //'last_updated' => date('d/m/Y', strtotime($item->updated_at))
          'last_updated' => $item->last_purchased_at == null ? 'NO PURCHASE' :
            date('d/m/Y', strtotime($item->last_purchased_at))
        ];
      }
    }
    return $results;
  }

  public function getLastPurchasedInventories($inventoryIds) {
    $results = [];

    if ($data = $this->pulsaInventories->getLastPurchasedInventories($inventoryIds)) {
      foreach ($data as $item) {
        $results[$item->pulsa_odeo_id][] = [
          'inventory_id' => $item->id,
          'biller_name' => $item->biller_name,
          'code' => $item->code,
          'base_price' => $this->currencyHelper->formatPrice($item->base_price),
          'last_updated' => date('d/m/Y', strtotime($item->last_purchased_at != null ?
            $item->last_purchased_at : $item->updated_at))
        ];
      }
    }
    return $results;
  }

  public function getOtherInventories($inventoryIds) {
    $results = [];
    $itemIds = [];
    $temp = [];
    foreach ($this->pulsaInventories->findByIds($inventoryIds) as $item) {
      $results[$item->id] = $item->pulsa_odeo_id;
      if (!in_array($item->pulsa_odeo_id, $itemIds)) $itemIds[] = $item->pulsa_odeo_id;
    }

    if ($data = $this->pulsaInventories->getCheapestPriceGroup($itemIds, 2, $inventoryIds)) {
      foreach ($data as $item) {
        $temp[$item->pulsa_odeo_id][] = [
          'inventory_id' => $item->id,
          'biller_name' => $item->biller_name,
          'code' => $item->code,
          'base_price' => $this->currencyHelper->formatPrice($item->base_price)
        ];
      }
    }

    foreach ($results as $key => $item) {
      if (isset($temp[$item])) $results[$key] = $temp[$item];
    }

    return $results;
  }

  public function getPriceHistories($inventoryIds) {
    $results = [];

    if ($data = $this->pulsaHistories->getByPulsaOdeoIds($inventoryIds)) {
      foreach ($data as $item) {
        $results[$item->pulsa_odeo_id] = [
          'price' => $this->currencyHelper->formatPrice($item->price),
          'changed_at' => date('F jS, Y H:i', strtotime($item->changed_at))
        ];
      }
    }
    return $results;
  }
}
