<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Pulsa\PulsaManager;
use Odeo\Domains\Marketing\QueueSelector;
use Odeo\Domains\Vendor\Jabber\JabberManager;
use Odeo\Domains\Core\Pipeline;

class JabberPulsaSearcher extends JabberManager {

  private $pulsaOdeo, $serviceDetails;

  public function __construct() {
    parent::__construct();
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {

    list($command, $code) = explode('.', $data['jabber_message']);
    $message = '';

    if (strtolower($code) == 'all') {
      $pipeline = new Pipeline();
      $pipeline->add(new Task(PulsaSearcher::class, 'searchAll'));
      $pipeline->execute(['is_paginate' => false]);
    }
    else if ($pulsaOdeo = $this->pulsaOdeo->findByCategory(PulsaCode::getConstValueByKey(strtoupper($code)))) {
      if ($pulsaOdeo->operator_id == Pulsa::OPERATOR_PLN) $serviceDetail = ServiceDetail::PLN_ODEO;
      else if ($pulsaOdeo->operator_id == Pulsa::OPERATOR_BOLT) $serviceDetail = ServiceDetail::BOLT_ODEO;
      else if (in_array($pulsaOdeo->category, PulsaCode::groupTransportation())) $serviceDetail = ServiceDetail::TRANSPORTATION_ODEO;
      else $serviceDetail = ServiceDetail::PULSA_ODEO;

      $detail = $this->serviceDetails->findById($serviceDetail);
      $data['service_id'] = $detail->service_id;
      $data['service_detail_id'] = $serviceDetail;
      if ($pulsaOdeo->operator_id) $data['operator_id'] = $pulsaOdeo->operator_id;

      $pipeline = new Pipeline();
      $pipeline->add(new Task(QueueSelector::class, 'get', ['use_preferred_store' => true]));
      $pipeline->add(new Task(PulsaManager::class, 'searchNominal'));
      $pipeline->execute($data);
    }
    else $message = 'Kode ' . $code . ' tidak valid';

    if (isset($pipeline)) {
      if ($pipeline->fail()) $message = $pipeline->errorMessage;
      else if (isset($pipeline->data['inventory'])) {
        $replies = [];
        foreach($pipeline->data['inventory'] as $item) {
          foreach ($item['details'] as $output) {
            if (isset($output['inventory_code'])) {
              if ($output['inventory_code'] == '') continue;
              $replies[] = $output['inventory_code'] . ' - ' . $output['name'] . ' : ' . $output['price']['formatted_amount'];
            }
          }
        }
        if (count($replies) > 0) $message = implode("\n", $replies);
        else $message = 'Stok kode ' . $code . ' kosong';
      }
      else if (isset($pipeline->data['inventories'])) {
        $replies = [];
        foreach ($pipeline->data['inventories'] as $output) {
          if (isset($output['inventory_code'])) {
            if ($output['inventory_code'] == '' || $output['service_detail_id'] == ServiceDetail::TRANSPORTATION_ODEO) continue;
            $replies[] = $output['inventory_code'] . ' - ' . $output['name'] . ' : ' .
              (isset($output['price_with_h2h_margin_example']) ? $output['price_with_h2h_margin_example']['formatted_amount'] :
                $output['price_with_margin_example']['formatted_amount']);
          }
        }
        if (count($replies) > 0) $message = implode("\n", $replies);
        else $message = 'Stok kode ' . $code . ' kosong';
      }
      else $message = 'Stok kode ' . $code . ' kosong';
    }

    if ($message != '') $listener->appendResponseMessage($message, InlineJabber::RES_PARAM);

    return $listener->response(200);
  }

}