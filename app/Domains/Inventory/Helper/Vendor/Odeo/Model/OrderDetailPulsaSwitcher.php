<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Model\VendorSwitcher;
use Odeo\Domains\Order\Model\OrderDetail;

class OrderDetailPulsaSwitcher extends Entity
{

  public function firstInventory() {
    return $this->belongsTo(PulsaOdeoInventory::class, 'first_inventory_id');
  }
  
  public function currentInventory() {
    return $this->belongsTo(PulsaOdeoInventory::class, 'current_inventory_id');
  }

  public function orderDetail() {
    return $this->belongsTo(OrderDetail::class);
  }

  public function vendorSwitcher() {
    return $this->belongsTo(VendorSwitcher::class);
  }

  public function plnDetails() {
    return $this->hasMany(OrderDetailPostpaidPlnDetail::class);
  }

  public function pulsaDetails() {
    return $this->hasMany(OrderDetailPostpaidPulsaDetail::class);
  }

  public function pdamDetails() {
    return $this->hasMany(OrderDetailPostpaidPdamDetail::class);
  }

  public function bpjsKesDetail() {
    return $this->hasOne(OrderDetailPostpaidBpjsKesDetail::class);
  }

  public function pgnDetail() {
    return $this->hasOne(OrderDetailPostpaidPgnDetail::class);
  }

  public function multiFinanceDetail() {
    return $this->hasOne(OrderDetailPostpaidMultiFinanceDetail::class);
  }

}
