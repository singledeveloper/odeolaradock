<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class PulsaH2hUser extends Entity
{
  function user() {
    return $this->belongsTo(User::class);
  }

  function group() {
    return $this->belongsTo(PulsaH2hGroup::class, 'pulsa_h2h_group_id');
  }
}
