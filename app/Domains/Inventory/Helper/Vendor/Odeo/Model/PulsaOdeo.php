<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model;

use Carbon\Carbon;
use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Helper\Service\Pulsa\Model\PulsaOperator;
use Odeo\Domains\Inventory\Model\ServiceDetail;

class PulsaOdeo extends Entity
{

  public function operator()
  {
    return $this->belongsTo(PulsaOperator::class);
  }

  public function serviceDetail()
  {
    return $this->belongsTo(ServiceDetail::class);
  }

  public function getNameAttribute($value)
  {
    if (isset($this->service_detail_id) && $this->service_detail_id == \Odeo\Domains\Constant\ServiceDetail::BPJS_KES_ODEO
      && isset($this->nominal) && $this->nominal)
      return $value . ' - ' . Carbon::now()->addMonthsNoOverflow($this->nominal - 1)->format('F Y')
        . ' (' . $this->nominal . ' bulan)';
    return $value;
  }

}
