<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Model\VendorSwitcher;
use Odeo\Domains\Supply\Model\PulsaSupply;

class PulsaOdeoInventory extends Entity
{
  protected $dates = ['deleted_at'];

  protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
  
  public function vendorSwitcher()
  {
    return $this->belongsTo(VendorSwitcher::class);
  }

  public function pulsa() {
    return $this->belongsTo(PulsaOdeo::class, 'pulsa_odeo_id');
  }
}
