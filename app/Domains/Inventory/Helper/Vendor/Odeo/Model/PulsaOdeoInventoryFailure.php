<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model;

use Odeo\Domains\Core\Entity;

class PulsaOdeoInventoryFailure extends Entity
{
  
  public function inventory()
  {
    return $this->belongsTo(PulsaOdeoInventory::class, 'pulsa_odeo_inventory_id');
  }

}
