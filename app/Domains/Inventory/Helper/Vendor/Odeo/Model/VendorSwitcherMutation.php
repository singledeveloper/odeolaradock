<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Model\VendorSwitcher;

class VendorSwitcherMutation extends Entity
{

  public function vendorSwitcher() {
    return $this->belongsTo(VendorSwitcher::class);
  }

}
