<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Odeo\Domains\Constant\AssetAws;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;

class PostpaidViewer {

  private $switcherOrder;

  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
  }

  public function getPlnReport(PipelineListener $listener, $data) {
    if ($switcherOrder = $this->switcherOrder->findByRefAndId($data['odps'], $data['ref'])) {
      return $listener->response(200, [
        'view_page' => 'postpaid-pln-report',
        'view_data' => ["so" => $switcherOrder]
      ]);
    }
    $listener->response(204);
  }

  public function getBpjsReport(PipelineListener $listener, $data) {

    if($switcherOrder = $this->switcherOrder->findByRefAndId($data['odps'], $data['ref']))
      return $listener->response(200, [
        'view_page' => 'bpjs-report',
        'view_data' => ["so" => $switcherOrder]
      ]);
    $listener->response(204);
  }

  public function getPostpaidInventories(PipelineListener $listener, $data) {
    $types = $this->pulsaOdeo->findByServiceDetailId($data['service_detail_id']);
    if (count($types) > 0) {
      $result = [];
      foreach ($types as $item) {
        $type = [
          "item_id" => $item->id,
          "type" => strtolower(PostpaidType::getConstKeyByValue($item->category))
        ];

        if ($data['service_detail_id'] == ServiceDetail::BPJS_KES_ODEO) {
          try {
            $output = preg_split("/[-()]+/", $item->name);
            $type['name'] = trim($output[1]);
            $type['description'] = trim($output[2]);
          }
          catch (\Exception $e) {
            $type["name"] = $item->name;
          }
        }
        else $type["name"] = $item->name;

        if (isset($item->logo)) {
          $type['logo'] = AssetAws::getAssetPath(AssetAws::PPOB_LOGO, $item->logo);
        }

        $result[] = $type;
      }
      return $listener->response(200, [
        'postpaids' => $result
      ]);
    }
    return $listener->response(204);
  }

}
