<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 11:19 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;

class PulsaAffiliateManager  {

  private $pulsaOdeo, $pulsaPrefix, $switcherOrder, $inventoryManager, $status;

  public function __construct() {
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->pulsaPrefix = app()->make(\Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaPrefixRepository::class);
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->inventoryManager = app()->make(\Odeo\Domains\Inventory\Helper\InventoryManager::class);
    $this->status = app()->make(\Odeo\Domains\Order\Helper\StatusParser::class);
  }

  public function findItem(PipelineListener $listener, $data) {

    if (strpos($data['number'], '#') !== false) {
      list($data['number'], $loop) = explode('#', $data['number']);
      $loop = intval($loop);
      if ($loop == 0) $loop = 1;
    }
    else $loop = 1;

    $data['number'] = purifyTelephone($data['number']);
    $listener->replaceData($data);

    if (intval($data['denom'])) {
      $temp = substr($data['number'], 0, 3);
      if ($temp != '999') $temp = substr($data['number'], 0, 5);
      $pulsaPrefix = $this->pulsaPrefix->getNominal(['prefix' => $temp]);

      $pulsaOdeo = $this->pulsaOdeo->findByNominal($data['denom'] . '000', $pulsaPrefix->operator_id);
    }
    else $pulsaOdeo = $this->pulsaOdeo->findByInventoryCode(strtoupper($data['denom']));

    if ($pulsaOdeo) {
      $same = $this->switcherOrder->getSamePurchaseCount($data['number'], $pulsaOdeo->id);
      if ($loop <= count($same)) return $listener->response(400, $this->status->parseMessageForSameTransaction($same[$loop - 1]));

      $response = $this->inventoryManager->getServiceDataFromCategory($pulsaOdeo->category);
      $response['item_id'] = $pulsaOdeo->id;
      $response['pulsa_odeo_id'] = $pulsaOdeo->id;
      $response['item_detail'] = [
        'number' => $data['number'],
        'operator' => $pulsaOdeo->category
      ];

      return $listener->response(200, $response);
    }

    return $listener->response(400, 'Kode ' . $data['denom'] . ' tidak valid');

  }
}
