<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/26/16
 * Time: 9:30 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Odeo\Domains\Core\PipelineListener;

class PulsaCheckouter {

  private $switcherOrder, $pulsaInventories, $h2hManager, $pulsaH2hGroupDetails;

  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->h2hManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper\PulsaH2HManager::class);
    $this->pulsaH2hGroupDetails = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaH2hGroupDetailRepository::class);
  }

  public function checkout(PipelineListener $listener, $data) {

    $switcher = $this->switcherOrder->getNew();
    $inventory = $this->pulsaInventories->findById($data['item']['inventory_id']);
    $pulsa = $inventory->pulsa;
    $switcher->order_detail_id = $data['order_detail']['id'];
    $switcher->first_inventory_id = $data['item']['inventory_id'];
    $switcher->current_inventory_id = $data['item']['inventory_id'];
    $switcher->number = isset($data['item']['item_detail']['number']) ? $data['item']['item_detail']['number'] : '-';
    $switcher->inventory_query_dumps = $data['item']['inventory_id'];
    $switcher->operator_id = $pulsa->operator_id;
    $switcher->vendor_switcher_id = $inventory->vendor_switcher_id;
    $switcher->pulsa_status = $pulsa->status;
    if (isset($data['item']['item_detail']['sms_to']))
      $switcher->sms_to = $data['item']['item_detail']['sms_to'];

    if ($inventory->base_price == null) {
      if ($pulsa->owner_store_id) {
        $switcher->first_base_price = $pulsa->price;
        $switcher->subsidy_amount = 0;
        $switcher->current_base_price = $pulsa->price;
      }
      else return $listener->response(400, 'Data supply tidak valid.');
    }
    else {
      $order = $switcher->orderDetail->order;
      if ($this->h2hManager->isUserH2H($order->user_id) && $detail = $this->pulsaH2hGroupDetails->findByUserId($order->user_id, $pulsa->id))
        $switcher->subsidy_amount = $inventory->base_price - $detail->price;
      else $switcher->subsidy_amount = $inventory->base_price - $pulsa->price;
      $switcher->first_base_price = $inventory->base_price;
      $switcher->current_base_price = $inventory->base_price;
    }

    $this->switcherOrder->save($switcher);

    return $listener->response(201);
  }

}