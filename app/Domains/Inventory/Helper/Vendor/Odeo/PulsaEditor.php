<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\OrderRefunder;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;

class PulsaEditor {

  private $switcherOrder, $selector, $vendorRecon, $pulsaPrefix, $billerManager;

  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->selector = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaOrderSelector::class);
    $this->vendorRecon = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository::class);
    $this->pulsaPrefix = app()->make(\Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaPrefixRepository::class);
    $this->billerManager = app()->make(\Odeo\Domains\Biller\BillerManager::class);
  }

  private function refundToForceSuccessUser($switcher) {
    $orderDetail = $switcher->orderDetail;
    if ($switcher->status == SwitcherConfig::SWITCHER_REFUNDED && !$orderDetail->order->confirm_receipt) {
      $inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
      $inserter->add([
        'user_id' => $orderDetail->order->user_id,
        'trx_type' => TransactionType::MANUAL_VOID,
        'cash_type' => CashType::OCASH,
        'amount' => -$orderDetail->sale_price,
        'data' => json_encode([
          "order_id" => $orderDetail->order_id
        ])
      ]);
      $inserter->run();
      $inserter->clear();
    }
  }

  public function editSwitcher(PipelineListener $listener, $data) {
    if (isset($data['order_detail_id'])) {
      $switcher = $this->switcherOrder->findByOrderDetailId($data['order_detail_id']);
    } else if (isset($data['order_id'])) {
      $orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
      $detail = $orderDetails->getByOrderId($data['order_id'])->first();
      $switcher = $this->switcherOrder->findByOrderDetailId($detail->id);
    } else $switcher = $this->switcherOrder->findById($data['order_detail_pulsa_switcher_id']);

    $changeBillerStatus = '';
    if (isset($data['timestamp']) && $data['timestamp'] != $switcher->updated_at->format('Y-m-d H:i:s'))
      return $listener->response(400, "Can't change this order, there may be an update from other billers or person.");
    if (isset($data['fail'])) {
      $switcher->current_response_status_code = isset($data['response_status_code']) ? $data['response_status_code'] : Supplier::RC_FAIL;
      $switcher->status = SwitcherConfig::SWITCHER_FAIL;
      $changeBillerStatus = SwitcherConfig::BILLER_FAIL;
    }
    if (isset($data['force_success'])) {
      if (isset($data['serial_number']) && $data['serial_number'] != '') {
        if ($this->switcherOrder->findByRef($data['serial_number'])) {
          return $listener->response(400, 'Seems that this SN ' . $data['serial_number'] .
            ' had been used by another order, please make sure to check by filter the phone number.');
        }
      }
      if (isset($data['current_base_price']) && $data['current_base_price'] != '') {
        $switcher->current_base_price = $data['current_base_price'];
      }

      $changeBillerStatus = SwitcherConfig::BILLER_SUCCESS;

      try {$this->refundToForceSuccessUser($switcher);}
      catch (InsufficientFundException $e) {
        return $listener->response(400, 'Void gagal. Saldo user tidak mencukupi.');
      }

      $switcher->current_response_status_code = Supplier::RC_OK;
    }

    if (isset($data['serial_number']) && $data['serial_number'] != '')
      $switcher->serial_number = $data['serial_number'];

    if ($changeBillerStatus != '') {
      $data['status'] = $changeBillerStatus;
      if ($switcher && strpos($switcher->admin_note, 'Using') !== false) { // GOJEK LOG
        $data['mod_request'] = $switcher->admin_note;
        $data['destination'] = str_replace('Using ', '', $switcher->admin_note);
      }
      $this->billerManager->expandSwitcher($switcher, $data);
      $this->billerManager->finalize($listener, false);

      $switcher = $this->billerManager->getCurrentSwitcher();
    }
    else $this->switcherOrder->save($switcher);

    if ($switcher) {
      if ($changeBillerStatus == SwitcherConfig::BILLER_FAIL)
        $switcher->status = SwitcherConfig::SWITCHER_REFUNDED;
      $this->switcherOrder->normalizeFilters($data);
      return $listener->response(200, ["order" => $this->selector->_extends($this->selector->_transforms($switcher, $this->switcherOrder), $this->switcherOrder)]);
    }
    return $listener->response(400, "Can't edit. Please contact Customer Service.");
  }

  public function editSwitcherSupply(PipelineListener $listener, $data) {

    $recon = $this->vendorRecon->findByOrderId($data['order_id'], $data['vendor_switcher_id']);

    $supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    list ($isValid, $message) = $supplyValidator->checkStore($recon->vendor_switcher_owner_store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    $switcher = $recon->switcher;

    if ($recon->vendor_switcher_id != $switcher->vendor_switcher_id)
      return $listener->response(400, 'Anda tidak dapat melakukan refund di order ini.');

    if ($data['status'] == SwitcherConfig::BILLER_SUCCESS) {
      try {$this->refundToForceSuccessUser($switcher);}
      catch (InsufficientFundException $e) {
        return $listener->response(400, 'Void gagal. Saldo user tidak mencukupi.');
      }

      if (!isset($data['serial_number']) || (isset($data['serial_number']) && $data['serial_number'] == ''))
        return $listener->response(400, 'Gagal. Anda harus meng-input SN/Ref jika status sukses.');

      $switcher->serial_number = $data['serial_number'];
      $switcher->current_response_status_code = Supplier::RC_OK;

      if ($switcher->status != SwitcherConfig::SWITCHER_COMPLETED) {
        $this->billerManager->expandSwitcher($switcher, $data);
        $this->billerManager->finalize($listener, false);

        $recon = $this->billerManager->getCurrentRecon();
        /*$orderDetail = $switcher->orderDetail;
        if ($recon->sale_amount == null) $recon->sale_amount = intval($orderDetail->sale_price);
        $inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
        $supplySalesInserter = app()->make(\Odeo\Domains\Supply\Formatter\SupplySalesInserter::class);
        $supplySalesData = $supplySalesInserter->getCashInserterData([
          'user_id' => $data['auth']['user_id'],
          'order_id' => $orderDetail->order_id,
          'amount' => $recon->sale_amount,
          'settlement_at' => Carbon::now()->addHours(9)->toDateTimeString()
        ]);
        foreach ($supplySalesData as $item) $inserter->add($item);
        $inserter->run();*/
      }
      else {
        $recon->reference_number = $switcher->serial_number;
        $this->switcherOrder->save($switcher);
      }

      $this->vendorRecon->save($recon);

      if ($recon->destination != null) {
        $biller = $switcher->vendorSwitcher;
        if ($biller->status = SwitcherConfig::BILLER_STATUS_HIDDEN_GOJEK) {
          $gojekUsers = app()->make(\Odeo\Domains\Biller\Gojek\Repository\GojekUserRepository::class);
          if ($gojekUser = $gojekUsers->findByTelephone($recon->destination)) {
            if ($gojekUser->store_id != null) $gojekUser->monthly_cash_in += $recon->base_price;
            $gojekUsers->save($gojekUser);
          }
        }
      }
    }
    else if ($data['status'] == SwitcherConfig::BILLER_FAIL) {
      if ($switcher->status != SwitcherConfig::SWITCHER_FAIL) {
        $switcher->status = SwitcherConfig::SWITCHER_FAIL;
        $switcher->current_response_status_code = Supplier::RC_FAIL;
        $recon->status = SwitcherConfig::BILLER_FAIL;
        $switcher->serial_number = null;
        $recon->reference_number = null;

        $this->vendorRecon->save($recon);
        $this->switcherOrder->save($switcher);

        $data['order_detail_id'] = $switcher->order_detail_id;
        $data['order_detail_pulsa_switcher_id'] = $switcher->id;
        app()->make(OrderRefunder::class)->refundCli($listener, $data);
      }
    }
    else return $listener->response(400, 'Status tidak valid.');

  }

}
