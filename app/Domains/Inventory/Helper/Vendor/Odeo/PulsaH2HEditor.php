<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Helper\Currency;

class PulsaH2HEditor {

  private $pulsaH2hGroups, $pulsaH2hUsers, $pulsaH2hGroupDetails, $currency, $redis;

  public function __construct() {
    $this->pulsaH2hGroups = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaH2hGroupRepository::class);
    $this->pulsaH2hUsers = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaH2hUserRepository::class);
    $this->pulsaH2hGroupDetails = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaH2hGroupDetailRepository::class);
    $this->currency = app()->make(Currency::class);
    $this->redis = Redis::connection();
  }

  public function addGroup(PipelineListener $listener, $data) {
    $group = $this->pulsaH2hGroups->getNew();
    $group->name = $data['h2h_group_name'];
    $group->description = $data['h2h_group_description'];

    $this->pulsaH2hGroups->save($group);

    return $listener->response(200);
  }

  public function editGroup(PipelineListener $listener, $data) {

    if ($group = $this->pulsaH2hGroups->findById($data['h2h_group_id'])) {

      $group->name = $data['h2h_group_name'];
      $group->description = $data['h2h_group_description'];

      $this->pulsaH2hGroups->save($group);
    }

    return $listener->response(200);
  }

  public function addUser(PipelineListener $listener, $data) {
    if (!$this->pulsaH2hUsers->findByUser($data['user_id'])) {
      $h2hUser = $this->pulsaH2hUsers->getNew();
      $h2hUser->user_id = $data['user_id'];
      $h2hUser->pulsa_h2h_group_id = $data['h2h_group_id'];
      $this->pulsaH2hUsers->save($h2hUser);

      $user = $h2hUser->user;

      if ($userIds = $this->redis->hget(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_USERIDS))
        $userIds = json_decode($userIds, true);
      else $userIds = [];
      $userIds[] = $user->id;
      $this->redis->hset(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_USERIDS, json_encode($userIds));

      return $listener->response(200, [
        'id' => $h2hUser->id,
        'user_id' => $user->id,
        'name' => $user->name
      ]);
    }

    return $listener->response(400, 'User already registered in a group');
  }

  public function removeUser(PipelineListener $listener, $data) {
    if ($h2hUser = $this->pulsaH2hUsers->findById($data['h2h_user_id'])) {
      $userId = $h2hUser->user_id;
      $this->pulsaH2hUsers->delete($h2hUser);

      if ($userIds = $this->redis->hget(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_USERIDS)) {
        $userIds = json_decode($userIds, true);
        if (($key = array_search($userId, $userIds)) !== false) {
          unset($userIds[$key]);
        }
        $this->redis->hset(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_USERIDS, json_encode($userIds));
      }

      return $listener->response(200);
    }

    return $listener->response(400, 'User not exist/already deleted');
  }

  public function addGroupDetail(PipelineListener $listener, $data) {

    if (!$this->pulsaH2hGroupDetails->findByPulsaOdeoId($data['pulsa_odeo_id'], $data['h2h_group_id'])) {
      $h2hGroupDetail = $this->pulsaH2hGroupDetails->getNew();
      $h2hGroupDetail->pulsa_odeo_id = $data['pulsa_odeo_id'];
      $h2hGroupDetail->pulsa_h2h_group_id = $data['h2h_group_id'];
      $h2hGroupDetail->price = $data['h2h_price'];
      $this->pulsaH2hGroupDetails->save($h2hGroupDetail);

      $pulsa = $h2hGroupDetail->pulsa;

      return $listener->response(200, [
        'id' => $h2hGroupDetail->id,
        'name' => $pulsa->name,
        'odeo_price' => $this->currency->formatPrice($pulsa->price),
        'price' => $this->currency->formatPrice($h2hGroupDetail->price),
        'price_difference' => $h2hGroupDetail->price - $pulsa->price
      ]);
    }

    return $listener->response(400, 'Inventory already added in this group.');
  }

  public function removeGroupDetail(PipelineListener $listener, $data) {
    if ($h2hGroupDetail = $this->pulsaH2hGroupDetails->findById($data['h2h_group_detail_id'])) {
      $this->pulsaH2hGroupDetails->delete($h2hGroupDetail);
      return $listener->response(200);
    }

    return $listener->response(400, 'Detail not exist/already deleted');
  }

}