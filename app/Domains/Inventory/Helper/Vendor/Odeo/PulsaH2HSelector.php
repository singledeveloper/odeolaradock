<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Transaction\Helper\Currency;

class PulsaH2HSelector implements SelectorListener {

  private $pulsaH2hGroups, $pulsaH2hUsers, $pulsaH2hGroupDetails, $currency;

  public function __construct() {
    $this->pulsaH2hGroups = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaH2hGroupRepository::class);
    $this->pulsaH2hUsers = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaH2hUserRepository::class);
    $this->pulsaH2hGroupDetails = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaH2hGroupDetailRepository::class);
    $this->currency = app()->make(Currency::class);
  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getGroupList(PipelineListener $listener, $data) {
    $groups = [];
    foreach ($this->pulsaH2hGroups->getAllActive() as $item) {
      $groups[] = [
        'id' => $item->id,
        'name' => $item->name,
        'description' => $item->description
      ];
    }

    if (sizeof($groups) > 0) return $listener->response(200, ['h2h_groups' => $groups]);
    return $listener->response(204);
  }

  public function getGroupDetails(PipelineListener $listener, $data) {
    $pulsaOdeoIds = [];
    $cheapestInventories = [];
    $h2hGroupDetails = $this->pulsaH2hGroupDetails->getByGroupId($data['h2h_group_id']);

    foreach ($h2hGroupDetails as $item) {
      $pulsaOdeoIds[] = $item->pulsa_odeo_id;
    }

    if (count($pulsaOdeoIds) > 0) {
      $switcherDataManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper\PulsaSwitcherDataManager::class);
      $cheapestInventories = $switcherDataManager->getLastPurchasedInventories($pulsaOdeoIds);
    }

    $groupDetailsNotOk = [];
    $groupDetailsOk = [];

    foreach ($h2hGroupDetails as $item) {
      if (isset($cheapestInventories[$item->pulsa_odeo_id]))
        $comparationPrice = $cheapestInventories[$item->pulsa_odeo_id][0]['base_price']['amount'];
      else $comparationPrice = null;

      $priceDiff = $comparationPrice !== null ? ($item->price - $comparationPrice) : 'EMPTY STOCK';

      $groupDetail = [
        'id' => $item->id,
        'name' => $item->name,
        'price' => $this->currency->formatPrice($item->price),
        'price_difference' => $priceDiff
      ];

      if ($priceDiff == '0' || intval($priceDiff) > 0) $groupDetailsOk[] = $groupDetail;
      else $groupDetailsNotOk[] = $groupDetail;
    }

    $users = [];
    foreach ($this->pulsaH2hUsers->getByGroupId($data['h2h_group_id']) as $item) {
      $users[] = [
        'id' => $item->id,
        'user_id' => $item->user_id,
        'name' => $item->name
      ];
    }

    return $listener->response(200, [
      'group_details' => array_merge($groupDetailsNotOk, $groupDetailsOk),
      'users' => $users
    ]);
  }

}