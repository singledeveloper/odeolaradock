<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 11:05 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Odeo\Domains\Biller\Jobs\InsertMutation;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;

class PulsaRefunder {

  private $switcherOrder, $vendorRecon, $inserter, $cashTransactions;
  
  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->vendorRecon = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository::class);
    $this->inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->cashTransactions = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
  }

  public function refund(PipelineListener $listener, $data) {

    $switcher = $this->switcherOrder->findById($data["order_detail_pulsa_switcher_id"]);
    if ($switcher->status == SwitcherConfig::SWITCHER_FAIL ||
      $switcher->status == SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER) {

      /*if (isset($data['auth'])) {
        $user = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class)->findById($data['auth']['user_id']);
        $note = 'Refund manual oleh ' . $user->name . ' [' . $user->id . '] @' . date('d/m/Y H:i:s');
        $switcher->admin_note = $switcher->admin_note == null ? $note : ('<br>' . $note);
      }*/
      $switcher->status = SwitcherConfig::SWITCHER_REFUNDED;
      $this->switcherOrder->save($switcher);

      $currentInventory = $switcher->currentInventory;
      $biller = $currentInventory->vendorSwitcher;

      if (!$currentInventory->is_unit) {
        //clog('hahaha', 'test');
        $repository = SwitcherConfig::getVendorSwitcherRepositoryById($biller->id);
        if ($repository != '' && $transaction = $repository->findBySwitcherId($data['order_detail_pulsa_switcher_id']))
          $recon = $this->vendorRecon->findByTransactionId($transaction->id, $biller->id);
        else $recon = $this->vendorRecon->findBySwitcherId($switcher->id, $biller->id);

        $listener->pushQueue(new InsertMutation([
          'order_detail_pulsa_switcher_id' => $data['order_detail_pulsa_switcher_id'],
          'vendor_switcher_id' => $biller->id,
          'transaction_id' => isset($transaction) ? $transaction->id : $recon->id,
          'transaction_status' => $recon->status,
          'transaction_status_prior' => SwitcherConfig::BILLER_SUCCESS,
          'amount' => $recon->base_price,
          'mutation_route' => SwitcherConfig::ROUTE_REFUNDER,
          'user_id' => isset($data['auth']) ? $data['auth']['user_id'] : null
        ]));
      }

      $orderDetail = $switcher->orderDetail;
      $order = $orderDetail->order;

      $this->inserter->clear();

      if ($order->status == OrderStatus::REFUNDED_AFTER_SETTLEMENT) {

        if (!isset($recon) || $recon->sale_amount == null) {
          $amount = $orderDetail->base_price - $orderDetail->sale_price;

          if ($amount < 0) {
            $this->inserter->add([
              'user_id' => UserStatus::INTERNAL_OVER_SETTLEMENT_USER_ID,
              'trx_type' => TransactionType::OVER_SETTLEMENT_REFUND,
              'cash_type' => CashType::OCASH,
              'amount' => $amount,
              'data' => json_encode([
                "order_id" => $order->id
              ])
            ]);
          }
        }

        $this->inserter->add([
          'user_id' => $order->user_id,
          'trx_type' => TransactionType::REFUND,
          'cash_type' => CashType::OCASH,
          'amount' => $orderDetail->sale_price,
          'data' => json_encode([
            "order_id" => $order->id
          ])
        ]);

        try {
          $this->inserter->run();
        } catch (InsufficientFundException $e) {
          return $listener->response(400, 'Insufficient refund account balance.');
        }
      }

      if (isset($recon)) {
        $recon->sale_amount = null;
        $this->vendorRecon->save($recon);
      }

      if (isset($data['auth']) && $biller->owner_store_id != null
        && $transaction = $this->cashTransactions->findByOrderId($order->id, TransactionType::AFFILIATE_INQUIRY_FEE)) {
        $this->inserter->clear();
        $this->inserter->add([
          'user_id' => $transaction->user_id,
          'trx_type' => TransactionType::REFUND,
          'cash_type' => CashType::OCASH,
          'amount' => abs($transaction->amount),
          'data' => json_encode([
            "order_id" => $order->id
          ])
        ]);
        $this->inserter->run();
      }
  
      return $listener->response(200, ["transaction" => $switcher]);
    }
    else {
      return $listener->response(400, "Can't refund this transaction at this moment. Please recheck with current biller.");
    }
  }

}