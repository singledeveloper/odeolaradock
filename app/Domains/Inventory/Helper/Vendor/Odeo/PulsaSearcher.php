<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 11:19 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;
use Odeo\Domains\Constant\Affiliate;
use Odeo\Domains\Constant\AssetAws;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Subscription\ManageInventory\Helper\PriceGetter;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;

class PulsaSearcher implements SelectorListener {

  private $pulsaOperator, $pulsaPrefix, $pulsaOdeo, $pulsaInventories, $marginFormatter,
    $serviceDetails, $currencyHelper, $priceGetter, $pulsaH2hGroupDetails, $h2hManager;

  public function __construct() {
    $this->pulsaOperator = app()->make(\Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaOperatorRepository::class);
    $this->pulsaPrefix = app()->make(\Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaPrefixRepository::class);
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->priceGetter = app()->make(\Odeo\Domains\Subscription\ManageInventory\Helper\PriceGetter::class);
    $this->pulsaH2hGroupDetails = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaH2hGroupDetailRepository::class);
    $this->h2hManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper\PulsaH2HManager::class);
  }

  public function searchNominal(PipelineListener $listener, $data) {
    if (isset($data['prefix'])) {
      $temp = substr(purifyTelephone($data['prefix']), 0, 3);
      if ($temp != '999') {
        $temp = substr(purifyTelephone($data['prefix']), 0, 5);
      } else $data['service_detail_id'] = ServiceDetail::BOLT_ODEO;

      $data['prefix'] = $temp;
      $pulsaPrefix = $this->pulsaPrefix->getNominal(['prefix' => $data['prefix']]);

      if ($pulsaPrefix) {
        $data['operator_id'] = $pulsaPrefix->operator_id;
      } else {
        return $listener->response(204);
      }
    } else if (isset($data['operator_code'])) {
      $pulsaOperator = $this->pulsaOperator->findByCode(strtoupper($data['operator_code']));
      if ($pulsaOperator) {
        $data['operator_id'] = $pulsaOperator->id;
      } else {
        return $listener->response(204);
      }
    }

    if (!isset($data['operator_id']) && !isset($data['service_detail_id']))
      return $listener->response(204);

    $includeInventoryIds = [];
    $agentSalePrice = [];
    $isH2H = $this->h2hManager->isUserH2H($data['auth']['user_id']);

    if (!$isH2H && isset($data['store_id']) && $data['store_id'] != StoreStatus::STORE_DEFAULT) {
      $agentSalePrice = $this->priceGetter->getPriceForInventory($data['store_id'], [
        'service_detail_id' => $data['service_detail_id'],
        'user_id' => $data['auth']['user_id'],
        'category' => isset($data['category']) ? $data['category'] : null,
        'operator_id' => isset($data['operator_id']) ? $data['operator_id'] : null
      ], PriceGetter::FLAG_DISABLE_BASE_PRICE);

      if (count($agentSalePrice) > 0) {
        $includeInventoryIds = $agentSalePrice->pluck('inventory_id');
      }
    }

    if (!isset($data['operator_id'])) {
      if (isset($data['category'])) {
        $pulsaInventories = $this->pulsaOdeo->findByServiceDetailIdAndCategory(
          $data['service_detail_id'], $data['category'], $includeInventoryIds);
      } else {
        $pulsaInventories = $this->pulsaOdeo->findByServiceDetailId(
          $data['service_detail_id'], $includeInventoryIds);
      }
    } else {
      $pulsaInventories = $this->pulsaOdeo->getByOperator(
        $data['operator_id'],
        isset($data['category']) ? $data['category'] : [],
        $includeInventoryIds
      );
    }
    if (count($pulsaInventories) <= 0) return $listener->response(204);

    $inventories = [];
    $inventoriesWithAgentPrices = [];
    $inventoriesFinal = [];
    $replicateFlags = []; // USED FOR ONLY AGENT PRICE APPEAR IN SAME PRODUCT
    $dataFormatter = [
      'user_id' => $data['auth']['user_id']
    ];
    $h2hPrices = $isH2H ? $this->pulsaH2hGroupDetails->getByUserId($data['auth']['user_id'], $pulsaInventories->pluck('id')) : [];
    $serviceDetails = [];

    foreach ($pulsaInventories as $item) {
      $temp = $this->_transforms($item, $this->pulsaInventories);
      if (!isset($data['service_detail_id'])) {
        if (!isset($serviceDetails[$temp['service_detail_id']]))
          $serviceDetails[$temp['service_detail_id']] = $this->serviceDetails->findById($temp['service_detail_id']);
        $dataFormatter['service_detail_id'] = $serviceDetails[$temp['service_detail_id']];
      }

      if (isset($h2hPrices[$item->id])) {
        $temp['price'] = $h2hPrices[$item->id]->price;
      } else if (isset($agentSalePrice[$item->id])) {
        $temp['price'] = $agentSalePrice[$item->id]->sell_price;
        if ($item->owner_store_id != null) $temp['name'] = $temp['name'] . ' SP';
      } else if ($item->status == Pulsa::STATUS_OK_SUPPLIER || $item->owner_store_id != null)
        continue;
      else $temp['price'] = $this->marginFormatter->formatMargin($temp['price']['amount'], $dataFormatter)['sale_price'];

      $temp['price'] = $this->currencyHelper->formatPrice($temp['price']);

      if ($item->replicate_flag) {
        $replicateFlags[$item->replicate_flag][] = $item->id;
      }

      $inventoriesWithAgentPrices[] = $temp;
    }

    foreach ($inventoriesWithAgentPrices as $item) {
      if ($item['owner_store_id'] == null && (isset($replicateFlags[$item['replicate_flag']])
          && count($replicateFlags[$item['replicate_flag']]) > 1))
        continue; // NOT SHOWING ODEO PRODUCT IF THERE IS ANY AGENT PRODUCT
      $category = $item['category'];
      unset($item['category'], $item['owner_store_id'], $item['replicate_flag']);
      $inventoriesFinal[$category][] = $item;
    }

    foreach ($inventoriesFinal as $key => $item) {
      $inventory['category'] = $key;
      $inventory['details'] = $item;
      $inventories[] = $inventory;
    }

    $response['operator'] = isset($data['operator_id']) ? $this->pulsaOperator->findById($data['operator_id'])->name : '';
    $response['inventory'] = $inventories;
    if (isset($data['service_detail_id']))
      $response['service_detail_id'] = (int)$data['service_detail_id'];

    return $listener->response(200, $response);
  }

  public function searchAll(PipelineListener $listener, $data) {

    $this->pulsaInventories->normalizeFilters($data);

    $vendorSwitcherId = isset($data['vendor_switcher_id']) ? $data['vendor_switcher_id'] : 0;
    $serviceDetails = [];

    if ($vendorSwitcherId == '0') {
      $pulsaInventories = $this->pulsaOdeo->gets($data);
      $serviceDetailData = $this->serviceDetails->getAll();

      foreach ($serviceDetailData as $item) {
        $serviceDetails[$item->id] = $item;
      }

    } else {
      $pulsaInventories = $this->pulsaInventories->gets($vendorSwitcherId, isset($data['ignore_active']));
    }

    $h2hPrices = isset($data['h2h_group_id']) ? $this->pulsaH2hGroupDetails->getByGroupIdNoJoin($data['h2h_group_id']) : [];

    if ($pulsaInventories) {
      $pulsas = [];
      foreach ($pulsaInventories as $item) {
        $temp = $this->_transforms($item, $this->pulsaInventories);

        if (isset($data['only_in_group']) && !isset($h2hPrices[$temp['item_id']])) continue;

        if (isset($h2hPrices[$temp['item_id']]))
          $temp['price_with_margin_example'] = $h2hPrices[$temp['item_id']]->price;
        else if (isset($temp['service_detail_id'])) {
          $priceWithMarginExample = $this->marginFormatter->formatMargin($temp['price']['amount'], [
            'service_detail' => $serviceDetails[$temp['service_detail_id']],
            'default' => true
          ]);

          $temp['price_with_margin_example'] = $priceWithMarginExample['sale_price'];
          $temp['referral_cashback'] = $this->currencyHelper->getAmountOnly($priceWithMarginExample['referral_cashback']);

        } else $temp['price_with_margin_example'] = $temp['price']['amount'];

        $temp['price_with_margin_example'] = $this->currencyHelper->formatPrice($temp['price_with_margin_example']);
        $temp['current_unit'] = $item->is_unit && $item->current_unit != null ? number_format($item->current_unit, 0, ',', '.') : '';
        $pulsas[] = $temp;
      }
      return $listener->response(200, ['inventories' => $this->_extends($pulsas, $this->pulsaInventories)]);
    }
    return $listener->response(204);
  }

  public function getAllOperator(PipelineListener $listener, $data) {

    $pulsaOperators = [];
    foreach ($this->pulsaOperator->getAll() as $item) {
      $pulsaOperators[] = $this->_transforms($item, $this->pulsaOperator);
    }

    if ($pulsaOperators) {
      return $listener->response(200, ['operators' => $pulsaOperators]);
    }
    return $listener->response(204);
  }

  public function getAllCategory(PipelineListener $listener, $data) {
    $categories = array_merge(
      PulsaCode::groupPulsa(),
      PulsaCode::groupData(),
      PulsaCode::groupBolt(),
      PulsaCode::groupPln(),
      PulsaCode::groupTransportation(),
      PulsaCode::groupGameVoucher(),
      PulsaCode::groupEMoney()
    );
    if (!isset($data['is_supply'])) {
      $categories = array_merge(
        $categories,
        [PostpaidType::BPJS_KES, PostpaidType::PDAM, PostpaidType::MULTIFINANCE, PostpaidType::PGN]
      );
    }
    $categories = array_unique($categories);
    sort($categories);

    if (isset($data['for_api'])) {
      $temp = [];
      foreach ($categories as $item) {
        $code = PulsaCode::getConstKeyByValue($item);
        if ($code == '') continue;
        $temp[] = [
          'code' => $code,
          'name' => $item
        ];
      }
      $categories = $temp;

      $inquiryFees = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateInquiryFeeRepository::class);
      $fee = $inquiryFees->findByUserId($data['auth']['user_id']);
      $fee = $fee && $fee->product_category_fee != null ? $fee->product_category_fee : Affiliate::DEFAULT_PRODUCT_CATEGORY_FEE;

      if ($fee > 0) {
        try {
          $cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
          $cashInserter->add([
            'user_id' => $data['auth']['user_id'],
            'trx_type' => TransactionType::AFFILIATE_INQUIRY_FEE,
            'cash_type' => CashType::OCASH,
            'amount' => -$fee,
            'data' => json_encode([
              'type' => 'categories'
            ])
          ]);
          $cashInserter->run();
        } catch (InsufficientFundException $e) {
          return $listener->response(400, 'Saldo Anda tidak mencukupi');
        }
      }
    }
    return $listener->response(200, [
      'categories' => $categories
    ]);
  }

  public function get(PipelineListener $listener, $data) {
    $pulsas = $this->pulsaOdeo->gets($data);
    if ($pulsas) {
      return $listener->response(200, ['pulsas' => $pulsas]);
    }
    return $listener->response(204);
  }

  public function _extends($data, Repository $repository) {
    if ($inventoryIds = $repository->beginExtend($data, "item_id")) {
      $manager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper\PulsaSwitcherDataManager::class);
      $filters = $repository->getFilters();
      $isInventory = isset($filters['vendor_switcher_id']) && $filters['vendor_switcher_id'] != '0';
      if ($repository->hasExpand('order_count')) {
        $repository->addExtend('order_count', $manager->getCountBySKU($inventoryIds, $isInventory));
      }
      if ($repository->hasExpand('order_value')) {
        $repository->addExtend('order_value', $manager->getOrderValueBySKU($inventoryIds, $isInventory));
      }
      if ($repository->hasExpand('order_subsidy')) {
        $repository->addExtend('order_subsidy', $manager->getSubsidyAmountBySKU($inventoryIds, $isInventory));
      }
      if (!$isInventory && $repository->hasExpand('cheapest_inventory')) {
        $repository->addExtend('cheapest_inventory', $manager->getCheapestInventories($inventoryIds));
      }
      if (!$isInventory && $repository->hasExpand('price_change')) {
        $repository->addExtend('price_change', $manager->getPriceHistories($inventoryIds));
      }
      if ($isInventory && $repository->hasExpand('other_inventory')) {
        $repository->addExtend('other_inventory', $manager->getOtherInventories($inventoryIds));
      }
    }
    return $repository->finalizeExtend($data);
  }

  public function _transforms($item, Repository $repository) {
    $pulsa = [];

    if (isset($item->id)) $pulsa["item_id"] = $item->id;
    if (isset($item->code)) $pulsa["code"] = $item->code;
    if (isset($item->service_detail_id)) $pulsa['service_detail_id'] = $item->service_detail_id;
    if (isset($item->inventory_code)) $pulsa["inventory_code"] = $item->inventory_code;
    if (isset($item->category)) $pulsa["category"] = $item->category;
    if (isset($item->name)) $pulsa["name"] = $item->name;
    if (isset($item->biller_name)) $pulsa["biller_name"] = $item->biller_name;
    if (isset($item->srp_price) && $item->srp_price) $pulsa["srp_price"] = $this->currencyHelper->formatPrice($item->srp_price);

    if (isset($item->base_price)) {
      $pulsa["price"] = $this->currencyHelper->formatPrice($item->base_price);
    } else if (isset($item->price)) {
      $pulsa["price"] = $this->currencyHelper->formatPrice($item->price);
    }

    if (isset($item->current_subsidy)) $pulsa['current_subsidy'] = $this->currencyHelper->formatPrice($item->current_subsidy);

    $pulsa["item_description"] = isset($item->description) && $item->description != '' ? explode('<br>', $item->description) : [];

    if (isset($item->link)) $pulsa['item_link'] = $item->link;
    $pulsa["item_detail"]["credit_amount"] = $item->nominal;
    if (isset($item->is_active)) $pulsa['is_active'] = $item->is_active;

    if (isset($item->status)) $pulsa['status'] = $item->status;
    $pulsa['owner_store_id'] = isset($item->owner_store_id) ? $item->owner_store_id : null;
    $pulsa['replicate_flag'] = isset($item->replicate_flag) ? $item->replicate_flag : null;

//    $pulsa['condition'] = rand(1, 5);
    $pulsa['condition'] = 5; // Hard coded for MVP release
    $pulsa['condition_desc'] = ['BURUK SEKALI', 'BURUK', 'SEDANG', 'BAIK', 'BAGUS'][$pulsa['condition'] - 1];

    return $pulsa;
  }

  public function getAllCategoryByServiceDetailId(PipelineListener $listener, $data) {

    $skipCategories = $data['except'] ?? [];
    $onlyCategories = $data['only'] ?? [];
    $itemInventories = $this->pulsaOdeo->getAllCategoryByServiceDetailId(
      $data['service_detail_id'],
      $skipCategories,
      $onlyCategories
    );

    $inventories = [];
    foreach ($itemInventories as $item) {
      $inventory = [
        'category' => $item->category,
      ];

      if (isset($item->logo)) {
        $inventory['logo'] = AssetAws::getAssetPath(AssetAws::PPOB_LOGO, $item->logo);
      }

      $inventories[] = $inventory;
    }
    $response['inventory'] = $inventories;

    return $listener->response(200, $response);
  }

  public function getSupplierInventories(PipelineListener $listener, $data) {
    $this->pulsaInventories->normalizeFilters($data);

    $inventories = [];

    foreach ($this->pulsaInventories->getSupplierInventories() as $item) {
      $inventories[] = [
        'id' => $item->id,
        'name' => $item->pulsa_name,
        'vendor_switcher_name' => $item->vendor_switcher_name,
        'supply_code' => $item->supply_code,
        'store_id' => $item->store_id,
        'store_name' => $item->store_name,
        'marketplace_price' => $this->currencyHelper->formatPrice($item->marketplace_price),
        'base_price' => $this->currencyHelper->formatPrice($item->base_price),
        'price_diff' => $item->marketplace_price - $item->base_price + SwitcherConfig::getPluginFee($item->vendor_switcher_status),
        'is_active' => $item->is_active,
        'updated_at' => $item->updated_at->format('Y-m-d H:i:s')
      ];
    }

    if (count($inventories) > 0) return $listener->response(200, array_merge([
      'inventories' => $inventories
    ], $this->pulsaInventories->getPagination()));

    return $listener->response(204, ['inventories' => []]);
  }

  public function getPrepaidInquiry(PipelineListener $listener, $data) {
    $temp = substr($data['number'], 0, 3);
    if ($temp != '999') $temp = substr(purifyTelephone($data['number']), 0, 5);
    if ($pulsaPrefix = $this->pulsaPrefix->getNominal(['prefix' => $temp])) {
      $listener->appendData([
        'operator_id' => $pulsaPrefix->operator_id
      ]);
      return $listener->response(200, [
        'number' => $data['number'],
        'operator_name' => $pulsaPrefix->operator->name
      ]);
    }
    return $listener->response(400, 'Not a valid Indonesian mobile phone number');
  }
}
