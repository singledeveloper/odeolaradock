<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ErrorStatus;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\OrderRefunder;

class PulsaSwitcher {

  private $biller, $switcherOrder, $order, $pulsaInventories;

  private $needRefund = [
    SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER,
    SwitcherConfig::SWITCHER_VOIDED,
    SwitcherConfig::SWITCHER_FAIL
  ];

  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->order = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
  }

  public function setBiller(PipelineListener $listener, $data) {
    $this->biller = '';
    if (!isset($data["vendor_switcher_id"])) {
      $inventoryId = isset($data["item"]) ? $data["item"]["inventory_id"] : (isset($data['inventory_id']) ? $data["inventory_id"] : '');
      if ($inventoryId != '') $data["vendor_switcher_id"] = $this->pulsaInventories->findById($inventoryId)->vendor_switcher_id;
    }

    if (isset($data['vendor_switcher_id'])) $this->biller = SwitcherConfig::getVendorSwitcherManagerById($data['vendor_switcher_id']);
    if ($this->biller == '') $this->biller = app()->make(\Odeo\Domains\Supply\Biller\Manager::class);
  }

  public function changeBiller(PipelineListener $listener, $data) {
    $switcherOrder = isset($data["switcher_order"]) ? $data["switcher_order"] : $this->switcherOrder->findById($data["order_detail_pulsa_switcher_id"]);
    if ($switcherOrder) {
      if (in_array($switcherOrder->status, array_merge([
        SwitcherConfig::SWITCHER_IN_QUEUE,
        SwitcherConfig::SWITCHER_PENDING
      ], $this->needRefund))) {
        if (time() - strtotime($switcherOrder->requested_at) > 600) {
          $switcherOrder->status = SwitcherConfig::SWITCHER_FAIL;
          $this->switcherOrder->save($switcherOrder);
        }
        else if (!in_array($switcherOrder->status, $this->needRefund)) {
          $pulsa = $switcherOrder->currentInventory->pulsa;
          if ($pulsa->owner_store_id != null) {
            $switcherOrder->status = SwitcherConfig::SWITCHER_FAIL;
            $this->switcherOrder->save($switcherOrder);
          }
          else {
            if ($switcherOrder->inventory_query_dumps == NULL) $dumps = [];
            else $dumps = explode(',', $switcherOrder->inventory_query_dumps);

            $redis = Redis::connection();
            if ($queues = $redis->hget(Supplier::REDIS_BILLER, Supplier::REDIS_BILLER_POI_QUEUE . $pulsa->id)) {
              $queues = explode(',', $queues);
              foreach ($dumps as $item) if (!in_array($item, $queues)) $queues[] = $item;
            } else $queues = $dumps;

            if ($newInventory = $this->pulsaInventories->getSameAmountFromDifferentBiller($switcherOrder->current_inventory_id, $queues, $switcherOrder->number)) {
              $switcherOrder->current_inventory_id = $newInventory->id;
              $switcherOrder->current_base_price = $newInventory->base_price;

              if (!in_array($newInventory->id, $dumps)) $dumps[] = $newInventory->id;
              $switcherOrder->inventory_query_dumps = implode(',', $dumps);
              $switcherOrder->vendor_switcher_id = $newInventory->vendor_switcher_id;

              $this->switcherOrder->save($switcherOrder);
              if (isset($data["switcher_order"]))
                $listener->replaceData(["switcher_order" => $switcherOrder]);

              if (!isset($data["continue_to"])) {
                $otherCategories = array_merge(PulsaCode::groupTransportation(), PulsaCode::groupGameVoucher(), PulsaCode::groupEMoney());
                $data["continue_to"] = $pulsa->operator_id != null || in_array($pulsa->category, $otherCategories) ? "purchase" : "purchasePostpaid";
              }
              $data["vendor_switcher_id"] = $newInventory->vendor_switcher_id;
              $this->setBiller($listener, $data);
              return call_user_func_array([$this->biller, $data["continue_to"]], [$listener, $data]);
            }
            else {
              $switcherOrder->status = SwitcherConfig::SWITCHER_FAIL;
              $this->switcherOrder->save($switcherOrder);
            }
          }
        }

        if (in_array($switcherOrder->status, $this->needRefund)) {
          if (!isset($pulsa)) $pulsa = $switcherOrder->currentInventory->pulsa;
          if ($switcherOrder->status == SwitcherConfig::SWITCHER_FAIL) {
            $internalNoticer = app()->make(\Odeo\Domains\Notification\Helper\InternalNoticer::class);
            if ($pulsa->owner_store_id == null && ($pulsa->category == PulsaCode::GOJEK && strpos($pulsa->name, 'Driver') === false) &&
              (!isset($data['external_user_id']) || (isset($data['external_user_id']) && $data['external_user_id'] == ''))) {
              $internalNoticer->saveMessage(NotificationType::PARSE_MULTIPLIER . ' refund terjadi di ' . $pulsa->name, NotificationType::NOTICE_REFUND, $pulsa->id, true);
            }
            else if ($switcherOrder->current_response_status_code == Supplier::RC_FAIL_DISTURBANCE)
              $internalNoticer->saveMessage(NotificationType::PARSE_MULTIPLIER . ' gagal gangguan terjadi di ' . $pulsa->name, NotificationType::NOTICE_DISTURBANCE, $pulsa->id, true);
          }

          $listener->addNext(new Task(OrderRefunder::class, 'refundCli', [
            'order_detail_id' => $switcherOrder->order_detail_id,
            'order_detail_pulsa_switcher_id' => $switcherOrder->id
          ]));
        }

        if (isset($data["switcher_order"])) $listener->replaceData(["switcher_order" => $switcherOrder]);
      }
      else clog('switcher_wall', 'Order ID: ' . $switcherOrder->orderDetail->order_id . ' try to switch with a blocked status (' . $switcherOrder->status . ')');
      return $listener->response(200);
    }
    return $listener->response(400, "Fail to change biller. ID not exist.");
  }

  public function __call($name, $argument) {
    $this->setBiller($argument[0], $argument[1]);
    if ($this->biller == '') return $argument[0]->responseWithErrorStatus(ErrorStatus::EMPTY_STOCK, trans('pulsa.empty_stock'));
    return call_user_func_array([$this->biller, $name], $argument);
  }

}
