<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Constant\SwitcherConfig;

class BillerRepository extends Repository {

  public function findBySwitcherId($switcherId) {
    return $this->model->where('switcher_reference_id', $switcherId)
      ->orderBy('id', 'desc')->first();
  }

  public function getWithPage($skip, $take) {
    return $this->model->orderBy('id', 'asc')->skip($skip)->take($take)->get();
  }

  public function findLastReplenishment() {
    return $this->model->where('status', SwitcherConfig::BILLER_IN_QUEUE)
      ->where('route', SwitcherConfig::ROUTE_REPLENISHER)->orderBy('id', 'desc')->first();
  }

}