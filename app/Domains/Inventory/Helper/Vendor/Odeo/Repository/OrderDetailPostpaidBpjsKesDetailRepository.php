<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\OrderDetailPostpaidBpjsKesDetail;

class OrderDetailPostpaidBpjsKesDetailRepository extends Repository {

  public function __construct(OrderDetailPostpaidBpjsKesDetail $orderDetailPostpaidBpjsKesDetail) {
    $this->model = $orderDetailPostpaidBpjsKesDetail;
  }

}