<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\OrderDetailPostpaidPdamDetail;

class OrderDetailPostpaidPdamDetailRepository extends Repository {

  public function __construct(OrderDetailPostpaidPdamDetail $orderDetailPostpaidPdamDetail) {
    $this->model = $orderDetailPostpaidPdamDetail;
  }

}