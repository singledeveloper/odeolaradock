<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\OrderDetailPostpaidPgnDetail;

class OrderDetailPostpaidPgnDetailRepository extends Repository {

  public function __construct(OrderDetailPostpaidPgnDetail $orderDetailPostpaidPgnDetail) {
    $this->model = $orderDetailPostpaidPgnDetail;
  }

}