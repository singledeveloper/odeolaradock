<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\OrderDetailPostpaidPlnDetail;

class OrderDetailPostpaidPlnDetailRepository extends Repository {

  public function __construct(OrderDetailPostpaidPlnDetail $orderDetailPostpaidPlnDetail) {
    $this->model = $orderDetailPostpaidPlnDetail;
  }

}