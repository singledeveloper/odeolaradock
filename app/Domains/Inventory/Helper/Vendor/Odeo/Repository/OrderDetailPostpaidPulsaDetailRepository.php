<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\OrderDetailPostpaidPulsaDetail;

class OrderDetailPostpaidPulsaDetailRepository extends Repository {

  public function __construct(OrderDetailPostpaidPulsaDetail $orderDetailPostpaidPulsaDetail) {
    $this->model = $orderDetailPostpaidPulsaDetail;
  }

}