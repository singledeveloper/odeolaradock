<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Carbon\Carbon;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\OrderDetailPulsaSwitcher;

class OrderDetailPulsaSwitcherRepository extends Repository {

  public function __construct(OrderDetailPulsaSwitcher $orderSwitcher) {
    $this->model = $orderSwitcher;
  }

  public function findByOrderDetailId($order_detail_id) {
    return $this->model
      ->select('order_detail_pulsa_switchers.*', 'pulsa_odeo_inventories.pulsa_odeo_id')
      ->join('pulsa_odeo_inventories', 'pulsa_odeo_inventories.id', '=', 'order_detail_pulsa_switchers.current_inventory_id')
      ->where('order_detail_id', $order_detail_id)
      ->first();
  }

  public function findByOrderId($orderId) {
    return $this->model->whereHas('orderDetail', function ($query) use ($orderId) {
      $query->where('order_id', $orderId);
    })->first();
  }

  public function findByRefAndId($id, $ref) {
    return $this->model->where('id', $id)->where('serial_number', $ref)->first();
  }

  public function findByRef($ref) {
    return $this->model->where('serial_number', $ref)
      ->where('requested_at', '>', Carbon::now()->subDay(1)->toDateTimeString())->first();
  }

  public function findByNumber($number, $userId) {
    return $this->model->where('number', $number)->whereHas('orderDetail', function ($query) use ($userId) {
      $query->whereHas('order', function ($subquery) use ($userId) {
        $subquery->where('user_id', $userId);
      });
    })->first();
  }

  public function findPendingNumberAndInventoryCode($number, $inventoryCode) {
    return $this->model->where('number', $number)->whereHas('currentInventory', function ($query) use ($inventoryCode) {
      $query->where('code', $inventoryCode);
    })->where('status', SwitcherConfig::SWITCHER_PENDING)->first();
  }

  public function getByDateAndNumber($number, $date) {
    return $this->model
      ->where('number', $number)
      ->where('status', '<>', SwitcherConfig::SWITCHER_PENDING)
      ->whereDate('created_at', '=', $date)
      ->get();
  }

  public function getAllPendingCounts() {
    return $this->model->join('pulsa_odeo_inventories', 'pulsa_odeo_inventories.id', '=', 'order_detail_pulsa_switchers.current_inventory_id')
      ->join('vendor_switchers', 'pulsa_odeo_inventories.vendor_switcher_id', '=', 'vendor_switchers.id')
      ->where('order_detail_pulsa_switchers.status', SwitcherConfig::SWITCHER_IN_QUEUE)
      ->groupBy('pulsa_odeo_inventories.vendor_switcher_id', 'queue_limit')
      ->select('pulsa_odeo_inventories.vendor_switcher_id', 'queue_limit', \DB::raw('count(order_detail_pulsa_switchers.id) as pending_counts'))->get();
  }

  public function gets() {
    $query = $this->getCloneModel();
    $filters = $this->getFilters();

    if (!isset($filters['sort_by'])) {
      $filters['sort_by'] = 'order_detail_pulsa_switchers.id';
    }
    if (!isset($filters['sort_type'])) {
      $filters['sort_type'] = 'desc';
    }
    if (isset($filters['search']) && is_array($filters['search']))
      $filters = array_merge($filters['search'], [
        'sort_by' => $filters['sort_by'],
        'sort_type' => $filters['sort_type']
      ]);

    $query = $query->join('order_details', 'order_details.id', '=', 'order_detail_pulsa_switchers.order_detail_id')
      ->join('orders', 'orders.id', '=', 'order_details.order_id')
      ->leftJoin('vendor_switchers', 'vendor_switchers.id', '=', 'order_detail_pulsa_switchers.vendor_switcher_id')
      ->leftJoin('stores', 'vendor_switchers.owner_store_id', '=', 'stores.id');

    if (isset($filters['order_id'])) {
      $query = $query->where('order_id', $filters['order_id']);
    }
    if (isset($filters['phone_number'])) {
      $query = $query->whereIn('number', [
        purifyTelephone(trim($filters['phone_number'])),
        trim($filters['phone_number'])
      ]);
    }
    if (isset($filters['pulsa_id'])) {
      $query = $query->join('pulsa_odeo_inventories', 'pulsa_odeo_inventories.id', '=', 'order_detail_pulsa_switchers.current_inventory_id')
        ->where('pulsa_odeo_id', '=', $filters['pulsa_id']);
    }
    if (isset($filters['status'])) {
      if ($filters['status'] == 'UPDATE') {
        $query = $query->where('order_detail_pulsa_switchers.status', SwitcherConfig::SWITCHER_COMPLETED)
          ->where('admin_note', 'like', 'UPDATE%');
      } else if ($filters['status'] == SwitcherConfig::SWITCHER_IN_QUEUE) {
        $query = $query->whereIn('order_detail_pulsa_switchers.status', [SwitcherConfig::SWITCHER_IN_QUEUE, SwitcherConfig::SWITCHER_PENDING])
          ->whereIn('orders.status', [OrderStatus::VERIFIED, OrderStatus::PARTIAL_FULFILLED, OrderStatus::WAITING_SUSPECT]);
      } else {
        $query = $query->where('order_detail_pulsa_switchers.status', $filters['status']);
        if ($filters['status'] != SwitcherConfig::SWITCHER_COMPLETED) {
          $query = $query->where('requested_at', '>', Carbon::now()->subDays(5)->toDateTimeString());
        }
      }
    } else $query = $query->where(function($subquery) {
      $subquery->where('order_detail_pulsa_switchers.status', '<>', SwitcherConfig::SWITCHER_PENDING)
        ->orWhereIn('orders.status', [OrderStatus::VERIFIED, OrderStatus::PARTIAL_FULFILLED]);
    });

    return $this->getResult($query->select(
      'order_detail_pulsa_switchers.id',
      'order_detail_pulsa_switchers.created_at',
      'order_detail_pulsa_switchers.updated_at',
      'requested_at',
      'vendor_switchers.name as current_biller_name',
      'order_detail_pulsa_switchers.status',
      'order_detail_pulsa_switchers.current_response_status_code',
      'number',
      'order_details.name',
      'current_base_price',
      'first_base_price',
      'subsidy_amount',
      'order_detail_id',
      'order_id',
      'serial_number',
      'admin_note',
      'vendor_switchers.owner_store_id',
      'stores.name as owner_store_name'
    )->orderBy($filters['sort_by'], $filters['sort_type']));
  }

  public function getCountBySKU($inventoryIds = [], $isInventory = false, $date = '', $limit = '') {
    $query = $this->model->where('order_detail_pulsa_switchers.status', SwitcherConfig::SWITCHER_COMPLETED);
    if ($isInventory) {
      $select = $where = 'current_inventory_id';
    } else {
      $select = \DB::raw('pulsa_odeo_id, name');
      $where = 'pulsa_odeo_id';
      $query = $query->join('pulsa_odeo_inventories', 'pulsa_odeo_inventories.id', '=', 'order_detail_pulsa_switchers.current_inventory_id')
        ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'pulsa_odeo_inventories.pulsa_odeo_id');
    }
    if (count($inventoryIds) > 0) $query = $query->whereIn($where, $inventoryIds);
    if ($date != '') $query = $query->where(\DB::raw('date(requested_at)'), $date);
    if ($limit != '') $query = $query->take($limit);

    return $query->select($select, \DB::raw('count(order_detail_pulsa_switchers.id) as order_number_count'))
      ->groupBy($select)->orderBy(\DB::raw('count(order_detail_pulsa_switchers.id)'), 'desc')->get();
  }

  public function getFailedBySKU($inventoryIds = [], $isInventory = false, $date = '', $limit = '') {
    $query = $this->model->whereIn('order_detail_pulsa_switchers.status', SwitcherConfig::getSwitcherFailedStatus());
    
    if ($isInventory) {
      $select = $where = 'current_inventory_id';
    } else {
      $select = \DB::raw('pulsa_odeo_id, name');
      $where = 'pulsa_odeo_id';
      $query = $query->join('pulsa_odeo_inventories', 'pulsa_odeo_inventories.id', '=', 'order_detail_pulsa_switchers.current_inventory_id')
        ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'pulsa_odeo_inventories.pulsa_odeo_id');
    }
    if (count($inventoryIds) > 0) $query = $query->whereIn($where, $inventoryIds);
    if ($date != '') $query = $query->where(\DB::raw('date(requested_at)'), $date);
    if ($limit != '') $query = $query->take($limit);

    return $query->select($select, \DB::raw('count(order_detail_pulsa_switchers.id) as order_number_count'))
      ->groupBy($select)->orderBy(\DB::raw('count(order_detail_pulsa_switchers.id)'), 'desc')->get();
  }

  public function getTotalFailed($day) {
    $date = Carbon::now()->subDays($day)->format('Y-m-d');
    return $this->model->whereIn('status', SwitcherConfig::getSwitcherFailedStatus())
      ->where(\DB::raw('date(requested_at)'), $date)->count();
  }

  public function getSamePurchaseCount($number, $pulsaOdeoId, $minutes = 720, $checkUser = false) {
    $datetime = Carbon::now()->subMinutes($minutes)->toDateTimeString();
    $query = $this->model->where('number', $number)->whereHas('currentInventory', function ($query) use ($pulsaOdeoId) {
      $query->where('pulsa_odeo_id', $pulsaOdeoId);
    })->where(function ($subquery) use ($datetime) {
      $subquery->where('requested_at', '>', $datetime)->orWhere(function ($subquery2) use ($datetime) {
        $subquery2->whereNull('requested_at')->where('created_at', '>', $datetime);
      });
    })->where('status', '<>', SwitcherConfig::SWITCHER_REFUNDED);

    if ($checkUser)
      $query = $query->whereHas('orderDetail', function ($subquery) {
        $subquery->whereHas('order', function ($subquery2) {
          $subquery2->where('user_id', getUserId());
        });
      });

    return $query->orderBy('id', 'asc')->get();
  }

  public function getLastSamePurchaseInTime($number, $pulsaOdeoId, $userId = null, $second = 3600) {
    $datetime = Carbon::now()->subSeconds($second);
    $query = $this->model->where('number', $number)
      ->join('pulsa_odeo_inventories', 'current_inventory_id', '=', 'pulsa_odeo_inventories.id')
      ->join('order_details', 'order_details.id', '=', 'order_detail_pulsa_switchers.order_detail_id')
      ->join('orders', 'orders.id', '=', 'order_details.order_id')
      ->where('pulsa_odeo_id', $pulsaOdeoId)
      ->where(function ($query) use ($datetime) {
        $query->where('order_detail_pulsa_switchers.requested_at', '>', $datetime)
          ->orWhere(function ($query) use ($datetime) {
            $query->whereNull('requested_at')->where('order_detail_pulsa_switchers.created_at', '>', $datetime);
          });
      })
      ->where('order_detail_pulsa_switchers.status', '<>', SwitcherConfig::SWITCHER_REFUNDED)
      ->where('user_id', $userId)
      ->select([
        'orders.status', 'order_details.name', 'orders.created_at', 'order_details.order_id',
        'order_detail_pulsa_switchers.number', 'orders.total'
      ]);

    return $query->orderBy('order_detail_pulsa_switchers.id', 'desc')->limit(1)->first();
  }

  public function getTotalOrderCount($day) {
    $date = Carbon::now()->subDays($day)->format('Y-m-d');
    return $this->model->where('status', SwitcherConfig::SWITCHER_COMPLETED)
      ->where(\DB::raw('date(requested_at)'), $date)->count();
  }

  public function getOrderValueBySKU($inventoryIds = [], $isInventory = false, $date = '', $limit = '') {
    $query = $this->model->where('order_detail_pulsa_switchers.status', SwitcherConfig::SWITCHER_COMPLETED);
    if ($isInventory) {
      $select = $where = 'current_inventory_id';
    } else {
      $select = \DB::raw('pulsa_odeo_id, name');
      $where = 'pulsa_odeo_id';
      $query = $query->join('pulsa_odeo_inventories', 'pulsa_odeo_inventories.id', '=', 'order_detail_pulsa_switchers.current_inventory_id')
        ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'pulsa_odeo_inventories.pulsa_odeo_id');
    }
    if (count($inventoryIds) > 0)
      $query = $query->whereIn($where, $inventoryIds);
    if ($date != '')
      $query = $query->where(\DB::raw('date(requested_at)'), $date);
    if ($limit != '')
      $query = $query->take($limit);
    return $query->select($select, \DB::raw('sum(first_base_price) as order_value'))
      ->groupBy($select)->orderBy(\DB::raw('sum(first_base_price)'), 'desc')->get();
  }

  public function getTotalOrderValue($day) {
    $date = Carbon::now()->subDays($day)->format('Y-m-d');
    return $this->model->where('status', SwitcherConfig::SWITCHER_COMPLETED)
      ->where(\DB::raw('date(requested_at)'), $date)
      ->select(\DB::raw('sum(first_base_price) as order_value'))->first();
  }

  public function getSubsidyAmountBySKU($inventoryIds = [], $isInventory = false, $date = '', $limit = '') {
    $query = $this->model->where('order_detail_pulsa_switchers.status', SwitcherConfig::SWITCHER_COMPLETED);
    if ($isInventory)
      $select = $where = 'current_inventory_id';
    else {
      $select = \DB::raw('pulsa_odeo_id, name');
      $where = 'pulsa_odeo_id';
      $query = $query->join('pulsa_odeo_inventories', 'pulsa_odeo_inventories.id', '=', 'order_detail_pulsa_switchers.current_inventory_id')
        ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'pulsa_odeo_inventories.pulsa_odeo_id');
    }

    if (count($inventoryIds) > 0) $query = $query->whereIn($where, $inventoryIds);
    if ($date != '') $query = $query->where(\DB::raw('date(requested_at)'), $date);
    if ($limit != '') $query = $query->take($limit);

    return $query->select($select, \DB::raw('sum(current_base_price - first_base_price + subsidy_amount) as total_subsidy'))
      ->groupBy($select)->orderBy(\DB::raw('sum(current_base_price - first_base_price + subsidy_amount)'), 'desc')->get();
  }

  public function getTotalSubsidy($day) {
    $date = Carbon::now()->subDays($day)->format('Y-m-d');
    return $this->model->where('status', SwitcherConfig::SWITCHER_COMPLETED)
      ->where(\DB::raw('date(requested_at)'), $date)
      ->select(\DB::raw('sum(current_base_price - first_base_price + subsidy_amount) as total_subsidy'))->first();
  }

  public function getLeadTimeAbove60($day) {
    $date = Carbon::now()->subDays($day)->format('Y-m-d');

    return $this->model->join('order_details', 'order_detail_pulsa_switchers.order_detail_id', '=', 'order_details.id')
      ->join('orders', 'order_details.order_id', '=', 'orders.id')
      ->join('pulsa_odeo_inventories', 'pulsa_odeo_inventories.id', '=', 'order_detail_pulsa_switchers.current_inventory_id')
      ->join('vendor_switchers', 'vendor_switchers.id', '=', 'pulsa_odeo_inventories.vendor_switcher_id')
      ->where(\DB::raw('closed_at - requested_at'), '>', \DB::raw("interval '1 minute'"))
      ->where('orders.status', OrderStatus::COMPLETED)
      ->where(\DB::raw('date(requested_at)'), $date)
      ->select('vendor_switchers.name', \DB::raw('count(orders.id) as order_counts'))
      ->groupBy('vendor_switchers.name')->get();
  }

  public function getOrderByStorePerDay($storeId) {
    return $this->model->join('order_details', 'order_details.id', '=', 'order_detail_pulsa_switchers.order_detail_id')
      ->join('orders', 'orders.id', '=', 'order_details.order_id')
      ->where('actual_store_id', $storeId)
      ->where('orders.status', OrderStatus::COMPLETED)
      ->whereNotNull('closed_at')
      ->where(\DB::raw('date(closed_at)'), '<>', date('Y-m-d'))
      ->select(\DB::raw('date(closed_at) as closed_at'), \DB::raw('sum(base_price) as total_sales_per_day'), \DB::raw('count(order_details.id) as total_count_per_day'))
      ->groupBy(\DB::raw('date(closed_at)'))
      ->orderBy(\DB::raw('date(closed_at)', 'desc'))->get();
  }

  public function getMarketPlaceSumByMonth($startDate, $endDate, $status = SwitcherConfig::SWITCHER_COMPLETED) {
    $query = "
      SELECT
        to_char(requested_at, 'YYYY-MM')                            AS period,
        sum(current_base_price)                                     AS gmv,
        sum(first_base_price - current_base_price - subsidy_amount) AS revenue,
        count(id)                                                   AS trx
      FROM order_detail_pulsa_switchers
      WHERE requested_at IS NOT NULL AND status=:status
          AND requested_at::date BETWEEN :start_date AND :end_date 
          AND exists(
              SELECT 1
              FROM pulsa_odeos
                JOIN pulsa_odeo_inventories ON pulsa_odeos.id = pulsa_odeo_inventories.pulsa_odeo_id
              WHERE current_inventory_id=pulsa_odeo_inventories.id AND owner_store_id IS NOT NULL
          )
      GROUP BY period";

    return \DB::select(\DB::raw($query), [
      'status' => $status,
      'start_date' => $startDate->format('Y-m-d'),
      'end_date' => $endDate->format('Y-m-d'),
    ]);
  }

  public function getSumByMonth($startDate, $endDate, $status = SwitcherConfig::SWITCHER_COMPLETED) {
    $h2hPlatforms = implode(', ', Platform::getH2HPlatforms());
    $appPlatforms = implode(', ', Platform::getAppPlatforms());

    $query = "
      SELECT
        CASE
        WHEN owner_store_id IS NOT NULL
          THEN
            'ppobMarketPlace'
        WHEN platform_id IN ({$h2hPlatforms})
          THEN
            'ppobH2H'
        WHEN platform_id IN ({$appPlatforms})
          THEN
            'ppobApp'
        ELSE
          'ppobOthers'
        END                                                         AS type,
        to_char(requested_at, 'YYYY-MM')                            AS period,
        sum(current_base_price)                                     AS gmv,
        sum(first_base_price - current_base_price - subsidy_amount) AS revenue,
        count(order_detail_pulsa_switchers.id)                      AS trx
      FROM order_detail_pulsa_switchers
        JOIN order_details ON order_detail_pulsa_switchers.order_detail_id = order_details.id
        JOIN orders ON order_details.order_id = orders.id
        JOIN pulsa_odeo_inventories ON pulsa_odeo_inventories.id = order_detail_pulsa_switchers.current_inventory_id
        JOIN pulsa_odeos ON pulsa_odeos.id = pulsa_odeo_inventories.pulsa_odeo_id
      WHERE requested_at IS NOT NULL AND order_detail_pulsa_switchers.status = :status
            AND requested_at :: DATE BETWEEN :start_date AND :end_date
      GROUP BY type, period";

    return \DB::select(\DB::raw($query), [
      'status' => $status,
      'start_date' => $startDate->format('Y-m-d'),
      'end_date' => $endDate->format('Y-m-d'),
    ]);
  }

  public function getSumByMonthAndService($startDate, $endDate, $serviceIdList, $status = SwitcherConfig::SWITCHER_COMPLETED) {
    $h2hPlatforms = implode(', ', Platform::getH2HPlatforms());
    $appPlatforms = implode(', ', Platform::getAppPlatforms());
    $serviceIdBinding = implode(', ', $serviceIdList);

    $query = "
      SELECT
        CASE
        WHEN owner_store_id IS NOT NULL
          THEN
            'ppobMarketPlace'
        WHEN platform_id IN ({$h2hPlatforms})
          THEN
            'ppobH2H'
        WHEN platform_id IN ({$appPlatforms})
          THEN
            'ppobApp'
        ELSE
          'ppobOthers'
        END                                                         AS type,
        displayed_name                                              AS service,
        to_char(requested_at, 'YYYY-MM')                            AS period,
        sum(current_base_price)                                     AS gmv,
        sum(first_base_price - current_base_price - subsidy_amount) AS revenue,
        count(order_detail_pulsa_switchers.id)                      AS trx
      FROM order_detail_pulsa_switchers
        JOIN order_details ON order_detail_pulsa_switchers.order_detail_id = order_details.id
        JOIN orders ON order_details.order_id = orders.id
        JOIN service_details ON order_details.service_detail_id = service_details.id
        JOIN services ON service_details.service_id = services.id
        JOIN pulsa_odeo_inventories ON pulsa_odeo_inventories.id = order_detail_pulsa_switchers.current_inventory_id
        JOIN pulsa_odeos ON pulsa_odeos.id = pulsa_odeo_inventories.pulsa_odeo_id
      WHERE requested_at IS NOT NULL AND order_detail_pulsa_switchers.status = :status
            AND requested_at :: DATE BETWEEN :start_date AND :end_date
            AND services.id IN ($serviceIdBinding)
      GROUP BY type, service, period
      ORDER BY type, service, period";

    return \DB::select(\DB::raw($query), [
      'status' => $status,
      'start_date' => $startDate->format('Y-m-d'),
      'end_date' => $endDate->format('Y-m-d'),
    ]);
  }

  public function getRecent($userId) {
    return $this->getResult($this->getCloneModel()->where('status', SwitcherConfig::SWITCHER_COMPLETED)
      ->whereHas('orderDetail', function ($query) use ($userId) {
        $query->whereHas('order', function ($query2) use ($userId) {
          $query2->where('user_id', $userId);
        });
      })->groupBy('number')->orderBy(\DB::raw('count(id)'), 'desc')
      ->select('number', \DB::raw('count(id) as order_total')));
  }

  public function getOrderSalesThisMonthByPlatform($day, $isCommunity) {

    $dateStart = Carbon::now()->subDays($day)->startOfMonth()->toDateString();
    $dateFinish = Carbon::now()->subDays($day)->toDateString();

    $query = $this->model
      ->join('order_details', 'order_details.id', '=', 'order_detail_pulsa_switchers.order_detail_id')
      ->join('orders', 'orders.id', '=', 'order_details.order_id')
      ->where('order_detail_pulsa_switchers.status', SwitcherConfig::SWITCHER_COMPLETED)
      ->whereDate('requested_at', '>=', $dateStart)
      ->whereDate('requested_at', '<=', $dateFinish);

    if ($isCommunity) {
      $query->whereNull('order_details.agent_base_price');
    } else {
      $query->whereNotNull('order_details.agent_base_price');
    }

    return $query->selectRaw('
        platform_id, 
        sum(current_base_price) as gross_amount, 
        sum(first_base_price - current_base_price - subsidy_amount) as surplus,
        count(*) as order_total
      ')
      ->groupBy('platform_id')
      ->orderBy('platform_id')
      ->get();

  }

  public function getAllVoidFailStatus() {
    return $this->model->where('status', SwitcherConfig::BILLER_VOID_FAIL)
      ->where('updated_at', '>=', Carbon::now()->subMinutes(5)->toDateTimeString())
      ->get();
  }

  public function getCompletedSwitcherNotHasMutation($date) {
    return $this->model->join('order_details', 'order_details.id', '=', 'order_detail_pulsa_switchers.order_detail_id')
      ->join('pulsa_odeo_inventories', 'pulsa_odeo_inventories.id', '=', 'order_detail_pulsa_switchers.current_inventory_id')
      ->leftJoin('vendor_switcher_reconciliation_mutations', 'vendor_switcher_reconciliation_mutations.order_detail_pulsa_switcher_id', '=', 'order_detail_pulsa_switchers.id')
      ->where('order_detail_pulsa_switchers.status', SwitcherConfig::SWITCHER_COMPLETED)
      ->whereNull('vendor_switcher_reconciliation_mutations.order_detail_pulsa_switcher_id')
      ->where('pulsa_odeo_inventories.is_unit', false)
      ->where(function ($query) use ($date) {
        $query->where(\DB::raw('date(requested_at)'), $date)->orWhere(function ($query2) use ($date) {
          $query2->whereNull('requested_at')->where(\DB::raw('date(order_detail_pulsa_switchers.created_at)'), $date);
        });
      })
      ->orderBy('pulsa_odeo_inventories.vendor_switcher_id', 'asc')->orderBy('order_id', 'asc')
      ->select('order_id', 'order_detail_pulsa_switchers.id', 'pulsa_odeo_inventories.vendor_switcher_id', 'current_base_price', 'requested_at')
      ->get();
  }

  public function getQueueCounts() {
    return $this->model
      ->join('order_details', 'order_details.id', '=', 'order_detail_pulsa_switchers.order_detail_id')
      ->join('orders', 'orders.id', '=', 'order_details.order_id')
      ->whereIn('order_detail_pulsa_switchers.status', [SwitcherConfig::SWITCHER_IN_QUEUE, SwitcherConfig::SWITCHER_PENDING])
      ->whereIn('orders.status', [OrderStatus::VERIFIED, OrderStatus::PARTIAL_FULFILLED, OrderStatus::WAITING_SUSPECT])
      ->select('order_detail_pulsa_switchers.id')
      ->count();
  }
}
