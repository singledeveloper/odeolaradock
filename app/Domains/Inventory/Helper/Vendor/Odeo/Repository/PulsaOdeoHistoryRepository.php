<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 8:15 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\PulsaOdeoHistory;

class PulsaOdeoHistoryRepository extends Repository {

  public function __construct(PulsaOdeoHistory $pulsaOdeoHistory) {
    $this->model = $pulsaOdeoHistory;
  }

  public function findChanged() {
    return $this->model->with('pulsa.serviceDetail')->where('is_changed', false)
      ->where('changed_at', '<=', Carbon::now()->toDateTimeString())->get();
  }

  public function getByPulsaOdeoIds($pulsaOdeoIds) {
    return $this->model->whereIn('pulsa_odeo_id', $pulsaOdeoIds)->where('is_changed', false)->get();
  }

  public function findByPulsaOdeoId($pulsaOdeoId) {
    return $this->model->where('pulsa_odeo_id', $pulsaOdeoId)->where('is_changed', false)->first();
  }

}