<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 8:15 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\PulsaOdeoInventory;

class PulsaOdeoInventoryRepository extends Repository {

  private $redis;

  public function __construct(PulsaOdeoInventory $pulsaOdeoInventory) {
    $this->model = $pulsaOdeoInventory;
    $this->redis = Redis::connection();
  }

  private function checkRecon() {
    return function($query) {
      $query->whereNull('recon_started_at')->orWhere(function($query2){
        $query2->where('recon_started_at', '<>', null)->whereRaw("
        cast(now() as timestamp) not between cast(concat(date(now()), ' ', recon_started_at) as timestamp)
          and cast(concat(date(now() + interval '1 day' * recon_interval), ' ', recon_ended_at) as timestamp)");
      });
    };
  }

  private function checkVendorSwitcherIdExcluded($number = '') {
    $excludeVendorIds = [];
    if (app()->environment() == 'production') {
      $excludeVendorIds = $this->redis->hget(Supplier::REDIS_BILLER, Supplier::REDIS_BILLER_PENDING_DISABLE);
      if (!$excludeVendorIds) $excludeVendorIds = [];
      else $excludeVendorIds = json_decode($excludeVendorIds, true);
    }

    if (count(Pulsa::CURRENT_VSID_TEST) > 0) {
      foreach(Pulsa::CURRENT_VSID_TEST as $item) {
        if (!in_array($item, $excludeVendorIds)) $excludeVendorIds[] = $item;
      }
    }

    if ($number != '') {
      $number = purifyTelephone($number);
      foreach ($this->redis->keys('*' . Pulsa::REDIS_PULSA_SUCCESS_LOCK . $number . '_*') as $item) {
        $lockVendorId = str_replace(Pulsa::REDIS_PULSA_SUCCESS_LOCK . $number . '_', '', $item);
        if (!in_array($lockVendorId, $excludeVendorIds)) $excludeVendorIds[] = $lockVendorId;
      }
    }

    return $excludeVendorIds;
  }

  public function findByVendorSwitcherId($vendorSwitcherId) {
    return $this->model->where('vendor_switcher_id', $vendorSwitcherId)
      ->where('is_seeded', true)->get();
  }

  public function updateActiveAllSeeded($vendorSwitcherId) {
    return $this->model->where('vendor_switcher_id', $vendorSwitcherId)
      ->where('is_seeded', true)->update(['is_active' => true]);
  }

  public function getCountByVendorSwitcherId($vendorSwitcherId) {
    return $this->model->where('vendor_switcher_id', $vendorSwitcherId)
      ->where('is_active', true)->count();
  }

  public function findByIds($ids) {
    return $this->model->whereIn('id', $ids)->get();
  }

  public function findSame($pulsaOdeoId, $vendorSwitcherId, $inventoryCode = '') {
    $query = $this->model->where('pulsa_odeo_id', $pulsaOdeoId)
      ->where('vendor_switcher_id', $vendorSwitcherId)->where('is_removed', false);

    if ($inventoryCode != '') $query->where('code', $inventoryCode);
    return $query->first();
  }

  public function disableInventory($vendorSwitcherId, $price = '') {
    $query = $this->model->with(['pulsa'])->where('vendor_switcher_id', $vendorSwitcherId);
    if ($price != '') $query = $query->where('base_price', $price);

    $inventories = $query->get();

    $query->update(['is_active' => false]);

    $vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $biller = $vendorSwitchers->findById($vendorSwitcherId);

    $pulsaOdeos = app()->make(PulsaOdeoRepository::class);
    foreach($inventories as $item) {
      $pulsa = $item->pulsa;
      if ($pulsa->owner_store_id && $pulsa->owner_store_id == $biller->owner_store_id) {
        $pulsa->is_active = false;
        $pulsaOdeos->save($pulsa);
      }
    }
  }

  public function disableSort($id) {
    $this->model->where('pulsa_odeo_id', $id)
      ->where('priority_sort', '<>', 99)
      ->update(['priority_sort' => 0]);
  }

  public function finalizeProducts($vendorSwitcherId, $codes = [], $notOperators = []) {
    if (count($codes) > 0) {
      $this->model->whereNotIn('code', $codes)
        ->whereHas('pulsa', function($subquery) use ($notOperators) {
        if (count($notOperators) > 0) $subquery->where('operator_id', '<>', null)
          ->whereNotIn('operator_id', $notOperators);
        else $subquery->where('operator_id', '<>', null);
      })->where('vendor_switcher_id', $vendorSwitcherId)->update(['is_active' => false]);
    }

    $this->model->where(function($query){
      $query->whereHas('pulsa', function($subquery){
        $subquery->where(\DB::raw('base_price - price'), '>', Pulsa::DIFFERENCE_PRICE_LIMIT)->where('price', '<>', 0);
      })->orWhereHas('vendorSwitcher', function($subquery) {
        $subquery->where(\DB::raw('current_balance - base_price'), '<', 0);
      });
    })->where('is_active', true)
      ->where('vendor_switcher_id', $vendorSwitcherId)->update(['is_active' => false]);
  }

  public function findExisting($vendorSwitcherId, $code, $pulsaOdeoId = '') {
    $query = $this->model->where('vendor_switcher_id', $vendorSwitcherId)->where('code', $code);
    if ($pulsaOdeoId != '') $query = $query->where('pulsa_odeo_id', $pulsaOdeoId);
    return $query->first();
  }

  public function findSameCodeFromDifferentPulsaId($vendorSwitcherId, $code, $pulsaOdeoId) {
    return $this->getCloneModel()
      ->where('vendor_switcher_id', $vendorSwitcherId)
      ->where('code', $code)
      ->where('pulsa_odeo_id', '<>', $pulsaOdeoId)->first();
  }

  public function getSameAmountFromDifferentBiller($inventoryId, $dumps = [], $number = '') {
    $query = $this->getCloneModel();
    $current = $this->findById($inventoryId);

    $query = $query->where("pulsa_odeo_id", $current->pulsa_odeo_id)->where('is_removed', false);
    if (count($dumps) > 0) $query = $query->whereNotIn("id", $dumps);

    if (in_array($number, Pulsa::CURRENT_NUMBER_TEST))
      $query = $query->whereIn('vendor_switcher_id', Pulsa::CURRENT_VSID_TEST);
    else {
      $excludeVendorIds = $this->checkVendorSwitcherIdExcluded($number);
      if (count($excludeVendorIds) > 0)
        $query = $query->whereNotIn('vendor_switcher_id', $excludeVendorIds);
    }

    return $query->where("is_active", true)->whereNotNull('base_price')
      ->whereHas('vendorSwitcher', $this->checkRecon())
      ->orderBy('priority_sort', 'desc')
      ->orderBy("base_price", "asc")->first();
  }

  public function getByPulsaOdeoId($pulsaOdeoId, $day = '') {
    $query = $this->model->where('pulsa_odeo_id', $pulsaOdeoId)
      ->where('is_active', true);

    if ($day != '') $query = $query->where('last_purchased_at', '>', Carbon::now()->subDays($day)->toDateTimeString());

    return $query->orderBy('base_price', 'asc')->get();
  }

  public function getByPulsaOdeoIdCount($pulsaOdeoId) {
    return $this->model->where('pulsa_odeo_id', $pulsaOdeoId)
      ->where('is_active', true)->count();
  }

  public function findByPulsaOdeoId($pulsaOdeoId) {
    return $this->model->where('pulsa_odeo_id', $pulsaOdeoId)->first();
  }

  public function getCheapestPrice($itemId, $number = '', $excludeIds = []) {

    $query = $this->model->where('pulsa_odeo_id', $itemId)->where('is_active', true)
      ->whereHas('vendorSwitcher', function($subquery) {
        $subquery->where($this->checkRecon())->where('is_active', true);
      });

    if (in_array($number, Pulsa::CURRENT_NUMBER_TEST))
      $query = $query->whereIn('vendor_switcher_id', Pulsa::CURRENT_VSID_TEST);
    else {
      $excludeVendorIds = $this->checkVendorSwitcherIdExcluded($number);
      if (count($excludeVendorIds) > 0)
        $query = $query->whereNotIn('vendor_switcher_id', $excludeVendorIds);
    }

    if (count($excludeIds) > 0)
      $query = $query->whereNotIn('id', $excludeIds);

    return $query
      ->orderBy('priority_sort', 'desc')
      ->orderBy('base_price', 'asc')
      ->orderBy('vendor_switcher_id', 'asc')->first();
  }

  public function getCheapestPriceGroup($itemIds, $rowCount = 1, $inventoryIds = []) {
    $arr = []; $arr1 = [];
    foreach ($itemIds as $item) $arr[] = '?';
    foreach ($inventoryIds as $item) $arr1[] = '?';
    $itemIds = array_merge($itemIds, $inventoryIds);
    $itemIds[] = $rowCount;
    return \DB::select(\DB::raw("
      select * from (
        select row_number() over (partition by pulsa_odeo_id 
        order by priority_sort desc, base_price asc, vendor_switcher_id asc) as r, t.*, v.name as biller_name
      from pulsa_odeo_inventories t join vendor_switchers v on v.id = t.vendor_switcher_id
      where t.is_active = true and t.base_price is not null 
        and pulsa_odeo_id in (" . implode(',', $arr) . ")" . (count($arr1) > 0 ? (" and t.id not in (" . implode(',', $arr1) . ")") : '') . ") x
      where x.r <= ?"), $itemIds);
  }

  public function getLastPurchasedInventories($itemIds) {
    $arr = [];
    foreach ($itemIds as $item) $arr[] = '?';
    return \DB::select(\DB::raw("
      select * from (
        select row_number() over (partition by pulsa_odeo_id 
        order by last_purchased_at desc nulls last, base_price asc, vendor_switcher_id asc) as r, t.*, v.name as biller_name
      from pulsa_odeo_inventories t join vendor_switchers v on v.id = t.vendor_switcher_id
      where t.is_active = true 
        and t.base_price is not null 
        and pulsa_odeo_id in (" . implode(',', $arr) . ")) x
      where x.r = 1"), $itemIds);
  }

  public function getCheapestFee($serviceDetailId, $category = '') {  
    $query2 = $this->model->whereHas('pulsa', function($query) use ($serviceDetailId, $category) {
      $query->where('service_detail_id', $serviceDetailId);
      if ($category != '') $query->where('category', $category);
      if ($serviceDetailId == ServiceDetail::BPJS_KES_ODEO)
        $query->whereIn('nominal', [0,1]); // TO DO BPJS DELETE LATER
    })->where('is_active', true)->orderBy('base_price', 'asc');

    return $query2->first();
  }

  public function getCheapestFeeByItemId($itemId) {
    return $this->model->where('pulsa_odeo_id', $itemId)
      ->where('is_active', true)
      ->orderBy('base_price', 'asc')->first();
  }

  public function gets($vendorSwitcherId, $ignoreActive) {
    $query = \DB::table('pulsa_odeo_inventories as a')
      ->join('vendor_switchers as v', 'v.id', '=', 'a.vendor_switcher_id')
      ->join('pulsa_odeos as c', 'c.id', '=', 'a.pulsa_odeo_id')
      ->where('a.vendor_switcher_id', $vendorSwitcherId)
      ->whereNotNull('base_price')->where('a.is_removed', false);

    if (!$ignoreActive) $query = $query->where('a.is_active', true)
      ->where('c.is_active', true);

    return $query->orderBy('c.operator_id', 'asc')
      ->orderBy('c.category', 'asc')
      ->orderBy('c.price', 'asc')
      ->select(
        'a.id', 'a.code', 'c.category', 'c.name',
        'v.name as biller_name', 'c.nominal', 'c.link', 'a.base_price',
        'a.is_active', \DB::raw("'0' as current_subsidy"), 'a.is_unit', 'a.current_unit')
      ->get();
  }

  public function updateUnit($vendorSwitcherId, $code, $amount) {
    $this->model->where('vendor_switcher_id', $vendorSwitcherId)
      ->where('code', $code)->where('is_unit', true)
      ->update(['current_unit' => $amount]);
  }

  public function getSupplierMarketplaceInventories() {
    $filters = $this->getFilters();

    return $this->getCloneModel()->join('vendor_switchers', 'vendor_switchers.id', '=', 'pulsa_odeo_inventories.vendor_switcher_id')
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'pulsa_odeo_inventories.pulsa_odeo_id')
      ->join('pulsa_odeos as pulsa_suppliers', 'pulsa_suppliers.id', '=', 'pulsa_odeo_inventories.supply_pulsa_id')
      ->where('vendor_switchers.owner_store_id', $filters['store_id'])
      ->where('pulsa_odeo_inventories.is_removed', false)
      ->select(
        'pulsa_odeo_inventories.id',
        'pulsa_odeos.id as pulsa_odeo_id',
        'vendor_switchers.id as vendor_switcher_id',
        'vendor_switchers.name as biller_name',
        'vendor_switchers.status as biller_status',
        'pulsa_odeos.name',
        'pulsa_suppliers.price as store_base_price',
        'pulsa_odeo_inventories.base_price as sell_price',
        'pulsa_odeo_inventories.code as biller_code',
        'vendor_switchers.status',
        'pulsa_odeo_inventories.is_active'
      )->orderBy('pulsa_odeos.name', 'asc')->get();
  }

  public function getByVendorStoreOwner($needActive = false) {

    $filters = $this->getFilters();

    $query = $this->model->join('vendor_switchers', 'vendor_switchers.id', '=', 'pulsa_odeo_inventories.vendor_switcher_id')
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'pulsa_odeo_inventories.pulsa_odeo_id')
      ->where('vendor_switchers.owner_store_id', $filters['store_id'])
      ->where('pulsa_odeos.owner_store_id', $filters['store_id'])
      ->where('pulsa_odeo_inventories.is_removed', false);

    if ($needActive) $query->where('pulsa_odeo_inventories.is_active', true);

    if (isset($filters['ids_not_in']) && sizeof($filters['ids_not_in']) > 0) {
      $query->whereNotIn('pulsa_odeo_inventories.pulsa_odeo_id', $filters['ids_not_in']);
    }
    if (isset($filters['vendor_switcher_id'])) {
      $query->where('pulsa_odeo_inventories.vendor_switcher_id', $filters['vendor_switcher_id']);
    }
    if (isset($filters['pulsa_category'])) {
      $query->where('pulsa_odeos.category', $filters['pulsa_category']);
    }

    $query = $query->select(
      'pulsa_odeos.id',
      'pulsa_odeo_inventories.vendor_switcher_id',
      'vendor_switchers.name as biller_name',
      'pulsa_odeo_inventories.pulsa_odeo_id',
      'pulsa_odeos.name as pulsa_name',
      'pulsa_odeo_inventories.code as biller_code',
      'pulsa_odeos.inventory_code as supply_code',
      'pulsa_odeos.price as store_base_price',
      'pulsa_odeo_inventories.base_price',
      'pulsa_odeo_inventories.is_active'
    )->orderBy('pulsa_name', 'asc');

    if (isset($filters['is_populated']))
      return $query->get();

    return $this->getResult($query);
  }

  public function getAllPulsaSupply($storeId) {
    return $this->model->join('pulsa_odeos as pulsa_suppliers', 'pulsa_suppliers.id', '=', 'pulsa_odeo_inventories.supply_pulsa_id')
      ->where('pulsa_suppliers.owner_store_id', $storeId)
      ->where('is_removed', false)
      ->get();
  }

  public function getSupplierInventories() {
    $filters = $this->getFilters();

    if (isset($filters['search'])) $filters = $filters['search'];

    $query = $this->model->join('vendor_switchers', 'vendor_switchers.id', '=', 'pulsa_odeo_inventories.vendor_switcher_id')
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'pulsa_odeo_inventories.pulsa_odeo_id')
      ->join('stores', 'stores.id', '=', 'vendor_switchers.owner_store_id')
      ->whereNotNull('supply_pulsa_id')
      ->where('pulsa_odeo_inventories.is_removed', false);

    if (isset($filters['store_id'])) {
      $query->where('vendor_switchers.owner_store_id', $filters['store_id']);
    }
    if (isset($filters['pulsa_odeo_id'])) {
      $query->where('pulsa_odeo_inventories.pulsa_odeo_id', $filters['pulsa_odeo_id']);
    }
    return $this->getResult($query->select(
      'pulsa_odeo_inventories.id',
      'pulsa_odeos.name as pulsa_name',
      'vendor_switchers.name as vendor_switcher_name',
      'vendor_switchers.status as vendor_switcher_status',
      'pulsa_odeo_inventories.code as supply_code',
      'stores.id as store_id',
      'stores.name as store_name',
      'pulsa_odeos.price as marketplace_price',
      'pulsa_odeo_inventories.base_price',
      'pulsa_odeo_inventories.is_active',
      'pulsa_odeo_inventories.updated_at'
    )->orderBy('pulsa_odeos.category', 'asc')
      ->orderBy('pulsa_odeo_inventories.base_price', 'asc'));
  }

  public function getMarketplaceInventories($supplyPulsaId) {
    return $this->getCloneModel()->with(['pulsa'])
      ->where('supply_pulsa_id', $supplyPulsaId)
      ->where('is_removed', false)
      ->get();
  }

  public function getCountsByOwner() {
    $query = "
      SELECT
        CASE WHEN owner_store_id IS NULL
          THEN 'odeo'
        ELSE 'marketplace' END AS owner,
        CASE WHEN pulsa_odeo_id IS NULL
          THEN 'nostock'
        ELSE 'instock' END     AS stock,
        count(*)
      FROM pulsa_odeos
        LEFT JOIN (
                    SELECT DISTINCT pulsa_odeo_id
                    FROM pulsa_odeo_inventories
                    WHERE is_active
                  ) inv ON inv.pulsa_odeo_id = pulsa_odeos.id
      WHERE deleted_at IS NULL AND status <> :status
      GROUP BY 1, 2;";

    return \DB::select(\DB::raw($query), [
      'status' => Pulsa::STATUS_PERMANENT_DELETED,
    ]);
  }
}