<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 8:15 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\VendorSwitcherReconciliationMutation;

class VendorSwitcherReconciliationMutationRepository extends Repository {

  public function __construct(VendorSwitcherReconciliationMutation $vendorSwitcherReconciliationMutation) {
    $this->model = $vendorSwitcherReconciliationMutation;
  }

  public function findLastRecordedMutation($vendorSwitcherId) {
    return $this->model->where('vendor_switcher_id', $vendorSwitcherId)->orderBy('id', 'desc')->first();
  }

  public function findDifferenceInSameTransaction($vendorSwitcherId, $transactionId) {
    return $this->model->where('vendor_switcher_id', $vendorSwitcherId)
      ->where('transaction_id', $transactionId)->select(\DB::raw('sum(amount) as total'))->first();
  }

  public function findBySwitcherId($switcherId, $route = '') {
    $query = $this->model->where('order_detail_pulsa_switcher_id', $switcherId);
    if ($route != '') $query = $query->where('route', $route);
    return $query->first();
  }

  public function findLastTransaction($vendorSwitcherId, $transactionId) {
    return $this->model->where('vendor_switcher_id', $vendorSwitcherId)
      ->where('transaction_id', $transactionId)->orderBy('id', 'desc')->first();
  }

  public function gets($vendorSwitcherId) {
    $query = $this->getCloneModel();
    $filters = $this->getFilters();

    $query = $query->leftJoin('order_detail_pulsa_switchers', 'order_detail_pulsa_switchers.id', '=', 'vendor_switcher_reconciliation_mutations.order_detail_pulsa_switcher_id')
      ->leftJoin('order_details', 'order_details.id', '=', 'order_detail_pulsa_switchers.order_detail_id')
      ->where('vendor_switcher_reconciliation_mutations.vendor_switcher_id', $vendorSwitcherId);

    if (isset($filters['search'])) $filters = $filters['search'];

    if (isset($filters['order_id'])) $query = $query->where('order_id', $filters['order_id']);
    if (isset($filters['from'])) $query = $query->where('route', 'like', '%' . $filters['from'] . '%');
    if (isset($filters['date'])) $query = $query->where(\DB::raw('date(vendor_switcher_reconciliation_mutations.created_at)'), date('Y-m-d', strtotime($filters['date'])));

    if (!isset($filters['sort_by'])) $filters['sort_by'] = 'vendor_switcher_reconciliation_mutations.id';
    if (!isset($filters['sort_type'])) $filters['sort_type'] = 'desc';

    return $this->getResult($query->select(
      'order_id',
      'last_balance_recorded_from_biller',
      'balance_after',
      'amount',
      'number',
      'balance_before',
      'vendor_switcher_reconciliation_mutations.created_at',
      'route'
    )->orderBy($filters['sort_by'], $filters['sort_type']));
  }

}