<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaMutationEditor;

class MutationChecker {

  public function run() {
    $pipeline = new Pipeline();
    $pipeline->add(new Task(PulsaMutationEditor::class, 'insertBalanceMutationAll'));
    $pipeline->execute(['date' => Carbon::now()->subDay(1)->format('Y-m-d')]);
  }

}
