<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Scheduler;

use Odeo\Domains\Affiliate\Jobs\AffiliateBroadcaster;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Subscription\ManageInventory\Jobs\StabilizeAllStoreProfit;

class PulsaAutoChanger {

  public function __construct() {
    $this->cache = app()->make(\Illuminate\Contracts\Cache\Repository::class);
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->pulsaOdeoHistories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoHistoryRepository::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
  }

  public function run() {
    $pipeline = new Pipeline();
    $pipeline->add(new Task(__CLASS__, 'check'));
    $pipeline->execute([]);
  }

  public function check(PipelineListener $listener, $data) {
    $histories = $this->pulsaOdeoHistories->findChanged();
    $caches = [];
    $newPrices = [];
    $inventoryToUpdate = [];

    foreach ($histories as $item) {

      $pulsa = $item->pulsa;

      $inventoryToUpdate[$pulsa->service_detail_id][] = [
        'inventory_id' => $pulsa->id,
        'price_before' => $pulsa->price,
        'price_after' => $item->price
      ];

      $pulsa->price = $item->price;
      $this->pulsaOdeo->save($pulsa);

      $item->is_changed = true;
      $this->pulsaOdeoHistories->save($item);

      if (!in_array($pulsa->operator_id, $caches)) $caches[] = $pulsa->operator_id;

      $temp = $this->marginFormatter->formatMargin($pulsa->price, ['service_detail' => $pulsa->serviceDetail]);
      $newPrices['default'][$pulsa->inventory_code] = $temp['sale_price'];
    }

    if (sizeof($caches) > 0) {
      $table = $this->pulsaOdeo->getModel()->getTable();
      foreach ($caches as $item) {
        $this->cache->tags($table)->forget($table . '.filter.operator.' . $item . '.pulsa');
        $this->cache->tags($table)->forget($table . '.filter.operator.' . $item . '.paket.data');
        $this->cache->tags($table)->forget($table . '.filter.operator.' . $item . '.sms.telp.packages');
        $this->cache->tags($table)->forget($table . '.filter.operator.' . $item);
      }
    }

    if (sizeof($newPrices) > 0) {
      $listener->pushQueue(new AffiliateBroadcaster([
          'status' => 'BROADCAST_NEW_PRICE',
          'message' => ''
        ], [
          'new_prices' => $newPrices
        ])
      );
    }

    !empty($inventoryToUpdate) &&
    $listener->pushQueue(new StabilizeAllStoreProfit([
      'inventories' => $inventoryToUpdate
    ]));

    return $listener->response(200);
  }

}
