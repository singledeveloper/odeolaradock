<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Scheduler;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;

class PulsaAutoPendingDisabler {

  private $pulsaSwitchers, $redis;

  public function __construct() {
    $this->pulsaSwitchers = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->redis = Redis::connection();
  }

  public function run() {
    $disabledBillers = [];
    foreach ($this->pulsaSwitchers->getAllPendingCounts() as $item) {
      if ($item->pending_counts >= ($item->queue_limit ? $item->queue_limit : SwitcherConfig::MAX_PENDING_ALLOWED))
        $disabledBillers[] = $item->vendor_switcher_id;
    }
    $this->redis->hset(Supplier::REDIS_BILLER, Supplier::REDIS_BILLER_PENDING_DISABLE, json_encode($disabledBillers));
  }

}
