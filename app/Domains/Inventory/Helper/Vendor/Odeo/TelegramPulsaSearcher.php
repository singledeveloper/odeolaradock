<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Pulsa\PulsaManager;
use Odeo\Domains\Marketing\QueueSelector;
use Odeo\Domains\Vendor\Telegram\TelegramManager;
use Odeo\Domains\Core\Pipeline;

class TelegramPulsaSearcher extends TelegramManager {

  private $pulsaOdeo, $serviceDetails;

  public function __construct() {
    parent::__construct();
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $this->initialize($data['chat_id'], $data['token']);

    list($command, $code) = explode('.', $data['telegram_message']);

    if ($pulsaOdeo = $this->pulsaOdeo->findByCategory(PulsaCode::getConstValueByKey(strtoupper($code)))) {
      if ($pulsaOdeo->operator_id == Pulsa::OPERATOR_PLN) $serviceDetail = ServiceDetail::PLN_ODEO;
      else if ($pulsaOdeo->operator_id == Pulsa::OPERATOR_BOLT) $serviceDetail = ServiceDetail::BOLT_ODEO;
      else $serviceDetail = ServiceDetail::PULSA_ODEO;

      $detail = $this->serviceDetails->findById($serviceDetail);
      $data['service_id'] = $detail->service_id;

      $pipeline = new Pipeline();
      $pipeline->add(new Task(QueueSelector::class, 'get', ['use_preferred_store' => true]));
      $pipeline->add(new Task(PulsaManager::class, 'searchNominal', [
        'service_detail_id' => $serviceDetail,
        'operator_id' => $pulsaOdeo->operator_id
      ]));
      $pipeline->execute($data);

      if ($pipeline->fail()) $this->reply($pipeline->errorMessage);
      else if (isset($pipeline->data['inventory'])) {
        $replies = [];
        foreach($pipeline->data['inventory'] as $item) {
          foreach ($item['details'] as $output) {
            if (isset($output['inventory_code'])) {
              if ($output['inventory_code'] == '') continue;
              $replies[] = $output['inventory_code'] . ' - ' . $output['name'] .
                ' : ' . $output['price']['formatted_amount'];
            }
          }
        }
        if (count($replies) > 0) $this->reply(implode("\n", $replies));
        else $this->reply('Stok kode ' . $code . ' kosong');
      }
      else $this->reply('Stok kode ' . $code . ' kosong');
    }
    else $this->reply('Kode ' . $code . ' tidak valid');

    return $listener->response(200);
  }

}