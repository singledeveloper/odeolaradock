<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/21/16
 * Time: 3:44 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Tiket;


use Carbon\Carbon;
use Odeo\Domains\Constant\Tiket;
use Odeo\Domains\Core\PipelineListener;
use Validator;

class CheckoutRequester {


  private $connector, $tokenManager, $tiketOrders, $tiketToken, $orderHelper;
  private static $checkoutData = null;

  public function __construct() {

    $this->tokenManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\TokenManager::class);
    $this->connector = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Connector::class);
    $this->tiketOrders = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository\OrderDetailTiketRepository::class);
    $this->tiketToken = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository\TiketTokenRepository::class);
    $this->orderHelper = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Helper\OrderHelper::class);

  }


  public function checkout(PipelineListener $listener, $data) {
    if (!self::$checkoutData && self::$checkoutData['tiket_order_id'] != $data['item']['item_detail']['order']['tiket_order_id']) {
      if (self::$checkoutData = $this->tiketCheckout($data)) ;
      else {
        return $listener->response(400);
      }
    }

    $tiketOrder = $this->tiketOrders->getNew();

    $tiketOrder->order_detail_id = $data['order_detail']['id'];
    $tiketOrder->user_id = $data['auth']['user_id'];
    $tiketOrder->tiket_order_id = self::$checkoutData['tiket_order_id'];
    $tiketOrder->tiket_order_detail_id = $data['item']['inventory_id'];
    $tiketOrder->name = $data['item']['name'];
    $tiketOrder->data = json_encode($data['item']);
    $tiketOrder->status = Tiket::TIKET_ORDER_CREATED;

    $this->tiketOrders->save($tiketOrder);

    return $listener->response(200);
  }


  public function tiketCheckout($data) {

    list($token) = $this->tokenManager->getToken(['user_id' => $data['auth']['user_id']]);

    if ($token['status'] != Tiket::STATUS_SUCCESS) {
      return false;
    }

    $token = $token['data']['token'];
    $tiketOrderId = $data['item']['item_detail']['order']['tiket_order_id'];

    $queryCheckout = [
      'token' => $token,
      'output' => 'json',
    ];


    $this->connector->request('GET', 'tiket', '/order/checkout/' . $tiketOrderId . '/IDR', $queryCheckout);
    $checkout = $this->connector->response([
      200 => function () {
        return [];
      },
    ])[0];


    if ($checkout['status'] != Tiket::STATUS_SUCCESS) {
      return false;
    }

    $queryCheckoutCustomer = [
      'token' => $token,
      'saveContinue' => 2,
      'output' => 'json',
    ];


    $queryCheckoutCustomer['salutation'] = $data['item']['item_detail']['booking_data']['conSalutation'] ?? $data['salutation'];
    $queryCheckoutCustomer['firstName'] = $data['item']['item_detail']['booking_data']['conFirstName'] ?? $data['first_name'];
    $queryCheckoutCustomer['emailAddress'] = $data['item']['item_detail']['booking_data']['conEmailAddress'] ?? $data['email'];
    $queryCheckoutCustomer['phone'] = $data['item']['item_detail']['booking_data']['conPhone'] ?? $data['telephone'];


    $this->connector->request('GET', 'tiket', '/checkout/checkout_customer', $queryCheckoutCustomer);
    $login = $this->connector->response([
      200 => function () {
        return [];
      },
    ])[0];

    if ($login['status'] != Tiket::STATUS_SUCCESS) {
      return false;
    }


    $orderData = $data['item']['item_detail']['order'];


    if ($orderData['order_type'] == 'hotel') {


      $queryCheckoutHotel = [
        'token' => $token,
        'conSalutation' => $queryCheckoutCustomer['salutation'],
        'conFirstName' => $queryCheckoutCustomer['firstName'],
        'conEmailAddress' => $queryCheckoutCustomer['emailAddress'],
        'conPhone' => $queryCheckoutCustomer['phone'],
        'detailId' => $orderData['order_detail_id'],
        'country' => 'ID',
        'output' => 'json',
      ];

      $this->connector->request('GET', 'tiket', '/checkout/checkout_customer', $queryCheckoutHotel);
      $customer = $this->connector->response([
        200 => function () {
          return [];
        }
      ])[0];

      if ($customer['status'] != Tiket::STATUS_SUCCESS) {
        return false;
      }

    }


    $queryCheckoutPayment = [
      'token' => $token,
      'btn_booking' => 1,
      'output' => 'json',
    ];

    $this->connector->request('GET', 'tiket', '/checkout/checkout_payment/8', $queryCheckoutPayment);
    $payment = $this->connector->response([
      200 => function ($body) {

        $response['order_id'] = $body['orderId'];
        $msg = $body['message'];
        $response['grand_total'] = $body['grand_total'];

        $hour = null;

        $expired_datetime = Carbon::now();

        if ($duration = $this->_getTime('jam', $msg)) {
          $expired_datetime->addHours($duration);
        }
        if ($duration = $this->_getTime('menit', $msg)) {
          $expired_datetime->addMinutes($duration);
        }
        if ($duration = $this->_getTime('detik', $msg)) {
          $expired_datetime->addSeconds($duration);
        }

        $response['expired_datetime'] = $expired_datetime->toDateTimeString();

        return [
          'data' => $response,
        ];
      },
    ])[0];


    if ($payment['status'] != Tiket::STATUS_SUCCESS) {
      return false;
    }

    $tiketToken = $this->tiketToken->getExistingToken($data['auth']['user_id']);

    $this->tiketToken->delete($tiketToken);

    return [
      'tiket_order_id' => $tiketOrderId,
      'expired_datetime' => $payment['data']['expired_datetime'],
      'customer' => [
        'salutation' => $queryCheckoutCustomer['salutation'],
        'firstName' => $queryCheckoutCustomer['firstName'],
        'emailAddress' => $queryCheckoutCustomer['emailAddress'],
        'phone' => $queryCheckoutCustomer['phone']
      ]
    ];
  }


  private function _getTime($query, $msg) {
    $pos = strpos(strtolower($msg), $query);

    if ($pos === false) return null;

    $time = '';

    for ($i = $pos - 2; $i > 0; $i--) {
      if ($msg[$i] == ' ') break;
      $time .= $msg[$i];

    }

    if ($time == "0") {
      return null;
    }

    return strrev($time);

  }
}