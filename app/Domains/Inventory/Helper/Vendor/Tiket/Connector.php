<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/20/16
 * Time: 9:16 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Tiket;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Promise;

use Odeo\Domains\Constant\Tiket;

class Connector {

  static $clients = [];
  protected $bulk_requests = [];

  private function getHttpClient($key) {


    if (app()->environment() == 'production') {
      $uri = Tiket::PROD_API_URI;
      $businessID = Tiket::PROD_ODEO_BUSINESS_ID;

    } else {
      $uri = Tiket::DEV_API_URI;
      $businessID = Tiket::DEV_ODEO_BUSINESS_ID;
    }

    $businessName = Tiket::ODEO_BUSINESS_NAME;


    if (!isset($clients[$key])) {
      self::$clients[$key] = new Client([
        'base_uri' => $uri,
        'cookies' => true,
        'headers' => [
          'User-Agent' => 'twh:' . $businessID . ';' . $businessName . ';',
        ],
        'http_errors' => false,
        'timeout' => Tiket::REQUEST_TIMEOUT,
        'verify' => false,
      ]);
    }

    return self::$clients[$key];
  }

  public function request($method, $key, $url, $data = null) {
    $this->bulk_requests[] = [$method, $key, $url, $data];
  }

  public function response($callbacks) {
    $promises = [];
    $responses = [];

    foreach ($this->bulk_requests as $request) {
      list($method, $key, $url, $data) = $request;
      if (strtolower($method) == 'get') {
        $option = 'query';
      } else {
        $option = 'json';
      }


      $client = $this->getHttpClient($key);

      $promises[] = $client->requestAsync($method, $url, [
        $option => $data
      ]);
    }

    try {
      $results = Promise\unwrap($promises);
    } catch (ConnectException $e) {
      for ($i = 0; $i < count($this->bulk_requests); ++$i) {
        $response = [
          'status' => Tiket::STATUS_BAD_REQUEST,
          'data' => null,
          'metadata' => null,
          'message' => 'ConnectException while requesting, : ' . $e->getMessage(),
          'status_code' => 400
        ];

        $responses[] = $response;
      }

      return $responses;
    }

    self::$clients = [];
    $this->bulk_requests = [];

    foreach ($results as $result) {
      $response = [
        'status' => Tiket::STATUS_SUCCESS,
        'data' => null,
        'metadata' => null,
        'message' => '',
        'status_code' => $result->getStatusCode()
      ];

      if ($result->getStatusCode() != 200) {
        $response['status'] = Tiket::STATUS_BAD_REQUEST;
        $response['message'] = 'Unknown error from Tiket.com, status code: ' . $result->getStatusCode();
        $response['status_code'] = 400;
      } else {
        $body = $result->getBody()->getContents();
        $body = json_decode($body, true);

        if (!isset($body['diagnostic']['status'])) {
          $response['status'] = Tiket::STATUS_BAD_REQUEST;
          $response['message'] = 'Error from Tiket.com, response: ' . json_encode($body);
          $response['status_code'] = 400;
        } else {
          if (!array_key_exists($body['diagnostic']['status'], $callbacks)) {
            $response['status'] = Tiket::STATUS_BAD_REQUEST;
            $response['status_code'] = 400;

            if (isset($body['diagnostic']['error_msgs'])) {
              $response['message'] = explode(',', $body['diagnostic']['error_msgs']);
            } else {
              $response['message'] = 'Error from Tiket.com, response: ' . json_encode($body);
            }
          } else {
            $cb = $callbacks[$body['diagnostic']['status']]($body);
            foreach ($cb as $key => $item) {
              $response[$key] = $item;
            }
          }
        }
      }

      $responses[] = $response;
    }

    return $responses;

  }
}