<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/21/16
 * Time: 4:00 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Tiket\Helper;


use Odeo\Domains\Constant\Tiket;

class OrderHelper {


  private $connector, $tiketOrders, $tokenManager;

  public function __construct() {

    $this->connector = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Connector::class);
    $this->tiketOrders = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository\OrderDetailTiketRepository::class);
    $this->tokenManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\TokenManager::class);

  }


  public function getOrder($data) {

    if (isset($data['token'])) {

      $token = $data['token'];

    } else {
      list($token) = $this->tokenManager->getToken(['user_id' => $data['auth']['user_id']]);
      if ($token['status'] != Tiket::STATUS_SUCCESS) return false;

      $token = $token['data']['token'];
    }


    $query = [
      'token' => $token,
      'output' => 'json',
    ];


    $this->connector->request('GET', 'tiket', '/order', $query);
    $order = $this->connector->response([
      200 => function ($body) {
        return [
          'data' => $body['myorder'],
        ];
      }
    ])[0];


    if ($order['status'] == Tiket::STATUS_SUCCESS) {

      return $order;

    }
    return false;
  }
}