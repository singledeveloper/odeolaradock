<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Tiket\Model;

use Odeo\Domains\Core\Entity;

class TiketToken extends Entity {

  protected $fillable = ['user_id', 'token'];

  protected $hidden = ['created_at', 'updated_at'];
}
