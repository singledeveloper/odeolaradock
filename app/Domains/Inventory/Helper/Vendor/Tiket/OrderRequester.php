<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/21/16
 * Time: 2:31 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Tiket;

use Odeo\Domains\Constant\Tiket;
use Odeo\Domains\Core\PipelineListener;
use Validator;

class OrderRequester {

  private $connector, $tokenManager;

  public function __construct() {

    $this->tokenManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\TokenManager::class);
    $this->connector = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Connector::class);
    $this->countries = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository\TiketCountryRepository::class);
    $this->tiketOrders = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository\OrderDetailTiketRepository::class);
    $this->tiketToken = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository\TiketTokenRepository::class);
    $this->orderHelper = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Helper\OrderHelper::class);

  }



  public function cancelOrder(PipelineListener $listener, $data) {

    list($token) = $this->tokenManager->getToken(['user_id' => $data['auth']['user_id']]);

    if ($token['status'] != Tiket::STATUS_SUCCESS) {
      return $listener->response($token['status_code'], $token['message']);
    }

    $data['token'] = $token['data']['token'];

    $query = [
      'token' => $data['token'],
      'order_detail_id' => $data['tiket_order_detail_id'],
      'output' => 'json',
    ];

    $this->connector->request('GET', 'tiket', '/order/delete_order', $query);
    $delete = $this->connector->response([
      200 => function () {
        return [];
      },
    ])[0];


    if ($delete['status'] != Tiket::STATUS_SUCCESS) {
      if (isset($delete['message'][0]) && str_contains($delete['message'][0], 'failed to delete')) {
        return $listener->response(200);
      }
      return $listener->response($delete['status_code'], $delete['message']);
    }

    return $listener->response(200);

  }


  public function getOrderHistory($data) {

    $validator = Validator::make($data, [
      'user_id'       => 'required',
      'order_id'      => 'required',
      'salutation'    => 'required|in:Mr,Mrs,Ms',
      'firstName'     => 'required',
      'phone'         => 'required',
      'emailAddress'  => 'required|email',
    ]);

    if ($validator->fails()) {
      //return $listener->response(400, $validator->errors()->all());
    }

    list($token) = $this->tokenManager->getToken(['user_id' => $data['user_id']]);

    if ($token['status'] != Tiket::STATUS_SUCCESS) {
      // return $listener->response($token['status_code'], $token['message']);
    }


    $validator = Validator::make($data, [
      'order_id' => 'required|exists:tiket_orders,order_id',
    ]);

    if ($validator->fails()) {
      //return $listener->response(400, $validator->errors()->all());
    }


    $tiketOrder = $this->tiketOrders->getExistingOrder($data['order_id']);

    if (app()->environment() == 'production') {
      $secretKey = Tiket::PROD_SECRET_KEY;
    }
    else {
      $secretKey = Tiket::DEV_SECRET_KEY;
    }


    $query = [
      'email' =>  $tiketOrder->email,
      'order_id' => $data['order_id'],
      'secretkey' => $secretKey,
      'output' => 'json',
    ];


    $this->connector->request('GET', 'tiket', '/check_order', $query);
    $history = $this->connector->response([
      200 => function ($body) {
        return [
          'data' => $body['result'],
        ];
      },
    ]);


    if ($history['status'] != Tiket::STATUS_SUCCESS) {
    //  return $this->buildErrorsResponse($history['message'], $history['status_code']);
    }

    //return $this->buildDataResponse($history['data'], $history['status_code']);
  }





}