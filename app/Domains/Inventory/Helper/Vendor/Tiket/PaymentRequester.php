<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/21/16
 * Time: 3:43 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Tiket;

use Odeo\Domains\Constant\Tiket;
use Odeo\Domains\Core\PipelineListener;
use Validator;

class PaymentRequester {


  private $connector, $tokenManager, $tiketOrders;

  static $hasSuccessedTiketOrderId = [];

  public function __construct() {

    $this->tokenManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\TokenManager::class);
    $this->connector = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Connector::class);
    $this->tiketOrders = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository\OrderDetailTiketRepository::class);
  }

  public function pay(PipelineListener $listener, $data) {

    if (in_array($data['tiket_order_id'], self::$hasSuccessedTiketOrderId)) {
      return $listener->response(200);
    }
    else {
      self::$hasSuccessedTiketOrderId[] = $data['tiket_order_id'];
    }

    if (app()->environment() == 'production') {
      $secretKey = Tiket::PROD_SECRET_KEY;
      $confirmKey = Tiket::PROD_CONFIRM_KEY;

    } else {
      $secretKey = Tiket::DEV_SECRET_KEY;
      $confirmKey = Tiket::DEV_CONFIRM_KEY;

    }

    $username = Tiket::ODEO_USERNAME;

    $query = [
      'order_id' => $data['tiket_order_id'],
      'textarea_note' => 'test',
      'tanggal' => date('Y-m-d'),
      'secretkey' => $secretKey,
      'confirmkey' => $confirmKey,
      'username' => $username,
      'output' => 'json',
    ];

    $this->connector->request('GET', 'tiket', '/partner/transactionApi/confirmPayment', $query);
    $confirm = $this->connector->response([
      200 => function () {
        return [];
      },
    ])[0];


    if ($confirm['status'] != Tiket::STATUS_SUCCESS) {
      throw new \Exception("tiket payment failed");
    }

    return $listener->response(200);

  }


}