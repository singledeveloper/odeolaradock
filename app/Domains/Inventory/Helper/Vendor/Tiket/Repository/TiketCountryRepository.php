<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Tiket\Model\TiketCountry;

class TiketCountryRepository extends Repository {

  public function __construct(TiketCountry $country) {
    $this->model = $country;
  }

}
