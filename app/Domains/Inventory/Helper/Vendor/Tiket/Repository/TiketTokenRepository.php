<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Tiket\Model\TiketToken;

class TiketTokenRepository extends Repository {

  public function __construct(TiketToken $tiketToken) {
    $this->model = $tiketToken;
  }

  public function getExistingToken($userId) {

    return $this->model->where('user_id', $userId)->first();

  }

}
