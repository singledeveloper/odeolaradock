<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/20/16
 * Time: 10:11 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Tiket;

use Odeo\Domains\Constant\Tiket;

class TokenManager {


  private $tiketToken;
  private $reNewTried = 0;

  public function __construct() {

    $this->tiketToken = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository\TiketTokenRepository::class);
    $this->connector = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Connector::class);
  }
  
  public function getToken($data, $renewToken = false)
  {


    if ($renewToken) {

      $this->reNewTried = ++$this->reNewTried;

      if ($this->reNewTried > 1) return [['status' => Tiket::STATUS_BAD_REQUEST, 'message' => 'Too Many Request']];

      return $this->_getNewToken($data);
    }

    if ($tiketToken = $this->tiketToken->getExistingToken($data['user_id'])) {
      return [
        [
          'status' => Tiket::STATUS_SUCCESS,
          'data' => [
            'token' => $tiketToken->token,
          ],
          'metadata' => null,
          'message' => '',
          'status_code' => 200,
        ],
      ];
    }
    else {
      return $this->_getNewToken($data);
    }
  }

  private function _getNewToken($data) {

    if (app()->environment() == 'production') {
      $secretKey = Tiket::PROD_SECRET_KEY;
    }
    else {
      $secretKey = Tiket::DEV_SECRET_KEY;
    }

    $query = [
      'method' => 'getToken',
      'secretkey' => $secretKey,
      'output' => 'json',
    ];

    $this->connector->request('GET', 'tiket', '/apiv1/payexpress', $query);


    $responses =  $this->connector->response([
      200 => function ($body) {
        return [
          'data' => ['token' => $body['token']],
        ];
      },
    ]);

    list($token) = $responses;

    if ($token['status'] != Tiket::STATUS_SUCCESS) {
      return $responses;
    }

    $tiketToken = $this->tiketToken->getNew();
    $tiketToken->user_id = $data['user_id'];
    $tiketToken->token = $token['data']['token'];

    $this->tiketToken->save($tiketToken);

    return $responses;
  }

}