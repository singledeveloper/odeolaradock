<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/20/16
 * Time: 11:10 PM
 */

namespace Odeo\Domains\Inventory\Hotel\Contract;


use Odeo\Domains\Core\PipelineListener;

interface HotelContract {

  public function searchAutoComplete(PipelineListener $listener, $data);

  public function searchHotel(PipelineListener $listener, $data);

  public function getRoomList(PipelineListener $listener, $data);

  public function addToCart(PipelineListener $listener, $data);

  public function removeCart(PipelineListener $listener, $data);

  public function checkout(PipelineListener $listener, $data);

  public function purchase(PipelineListener $listener, $data);

}