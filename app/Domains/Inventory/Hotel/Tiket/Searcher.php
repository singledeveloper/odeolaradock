<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/20/16
 * Time: 11:15 PM
 */

namespace Odeo\Domains\Inventory\Hotel\Tiket;


use Odeo\Domains\Constant\Tiket;
use Odeo\Domains\Core\PipelineListener;
use Validator;

class Searcher {


  private $connector, $tokenManager, $marginFormatter, $currency;

  public function __construct() {

    $this->tokenManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\TokenManager::class);
    $this->connector = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Connector::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

  }

  public function searchAutoComplete(PipelineListener $listener, $data) {


    $renewToken = false;

    do {


      list($token) = $this->tokenManager->getToken(['user_id' => $data['auth']['user_id']], $renewToken);

      $renewToken = false;

      if ($token['status'] != Tiket::STATUS_SUCCESS) {
        return $listener->response($token['status_code'], $token['message']);
      }

      $query = $data;
      $query['output'] = 'json';
      $query['token'] = $token['data']['token'];

      unset($query['auth']);

      $this->connector->request('GET', 'tiket', '/search/autocomplete/hotel', $query);
      $autocomplete = $this->connector->response([
        200 => function ($body) {
          return [
            'data' => [
              'results' => isset($body['results']['result']) ? $body['results']['result'] : null
            ],
          ];
        },
      ])[0];

      if ($autocomplete['status'] != Tiket::STATUS_SUCCESS) {

        if (isset($response['message'][0]) && strtolower($response['message'][0]) == 'wrong token') {
          $renewToken = true;
        } else {
          return $listener->response($autocomplete['status_code'], $autocomplete['message']);
        }
      }


    } while ($renewToken);


    return $listener->response($autocomplete['status_code'], $autocomplete['data']);

  }

  public function searchHotel(PipelineListener $listener, $data) {

    if (!isset($data['limit'])) {
      $data['limit'] = 10;
    }
    if (!isset($data['offset'])) {
      $data['offset'] = 0;
    }

    list($token) = $this->tokenManager->getToken(['user_id' => $data['auth']['user_id']]);

    if ($token['status'] != Tiket::STATUS_SUCCESS) {
      return $listener->response($token['status_code'], $token['message']);
    }

    $query = $data;
    $query['output'] = 'json';
    $query['page'] = $data['offset'] / $data['limit'] + 1;
    $query['offset'] = $data['limit'];
    $query['token'] = $token['data']['token'];

    unset($query['auth']);
    unset($query['limit']);

    $this->connector->request('GET', 'tiket', '/search/hotel', $query);
    $hotels = $this->connector->response([
      200 => function ($body) {
        return [
          'data' => [
            'results' => isset($body['results']['result']) ? $body['results']['result'] : null
          ],
          'metadata' => [
            'limit' => (int)$body['pagination']['offset'],
            'offset' => ($body['pagination']['current_page'] - 1) * $body['pagination']['offset'],
            'count' => $body['pagination']['total_found'],
          ],
          $body['pagination'],
        ];
      },
    ])[0];

    if ($hotels['status'] != Tiket::STATUS_SUCCESS) {

      return $listener->response($hotels['status_code'], $hotels['message']);

    }

    foreach ($hotels['data']['results'] as $result) {

      $result['oldprice'] = $result['oldprice'] / $data['night'] / $data['room'];
      $result['total_price'] = $result['total_price'] / $data['night'] / $data['room'];

      list($result['oldprice'], $_) = $this->marginFormatter->formatMargin($result['oldprice']);
      list($result['total_price'], $_) = $this->marginFormatter->formatMargin($result['total_price']);

      $result['oldprice'] = $this->currency->formatPrice($result['oldprice'] * $data['night'] * $data['room']);
      $result['total_price'] = $this->currency->formatPrice($result['total_price'] * $data['night'] * $data['room']);

      unset($result->price);
    }

    return $listener->response($hotels['status_code'], [
      'results' => $hotels['data']['results'],
      'metadata' => $hotels['metadata']
    ]);

  }


}