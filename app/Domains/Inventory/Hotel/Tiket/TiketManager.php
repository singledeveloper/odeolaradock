<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 7:43 PM
 */

namespace Odeo\Domains\Inventory\Hotel\Tiket;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\Vendor\Tiket\CheckoutRequester;
use Odeo\Domains\Inventory\Helper\Vendor\Tiket\OrderRequester;
use Odeo\Domains\Inventory\Helper\Vendor\Tiket\PaymentRequester;
use Odeo\Domains\Inventory\Hotel\Contract\HotelContract;

class TiketManager implements HotelContract {


  private $hotelRequester, $searcher, $paymentRequester,
    $checkoutRequester, $orderRequester;

  public function __construct() {

    $this->searcher = app()->make(Searcher::class);
    $this->hotelRequester = app()->make(HotelRequester::class);
    $this->paymentRequester = app()->make(PaymentRequester::class);
    $this->checkoutRequester = app()->make(CheckoutRequester::class);
    $this->orderRequester = app()->make(OrderRequester::class);

  }

  public function searchAutoComplete(PipelineListener $listener, $data) {

    return $this->searcher->searchAutoComplete($listener, $data);

  }

  public function searchHotel(PipelineListener $listener, $data) {

    return $this->searcher->searchHotel($listener, $data);

  }

  public function getRoomList(PipelineListener $listener, $data) {
    return $this->hotelRequester->getRoomList($listener, $data);
  }

  public function addToCart(PipelineListener $listener, $data) {
    return $this->hotelRequester->book($listener, $data);
  }

  public function removeCart(PipelineListener $listener, $data) {
    return $this->orderRequester->cancelOrder($listener, $data);
  }

  public function checkout(PipelineListener $listener, $data) {
    return $this->checkoutRequester->checkout($listener, $data);
  }

  public function purchase(PipelineListener $listener, $data) {
    return $this->paymentRequester->pay($listener, $data);
  }

}