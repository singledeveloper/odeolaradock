<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/2/17
 * Time: 3:46 PM
 */

namespace Odeo\Domains\Inventory\Jobs;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Constant\DataExporterView;
use Odeo\Domains\Exporter\Helper\DataExporter;
use Odeo\Jobs\Job;

class SendPriceList extends Job  {

  protected $data, $email, $fileType, $storeName;

  function __construct($data, $email, $fileType, $storeName) {
    parent::__construct();
    $this->data = $data;
    $this->email = $email;
    $this->fileType = $fileType;
    $this->storeName = $storeName;
  }

  public function handle() {

    $data = $this->data;
    $dataExporter = app()->make(DataExporter::class);
    $file = $dataExporter->generate($data, $this->fileType, DataExporterView::PRICE_LIST_KEY);

    Mail::send('emails.price_list_mail', [
      'data' => [
        'date' => Carbon::now()->toDateTimeString(),
        'store_name' => $this->storeName
      ]
    ], function ($m) use ($file) {

      $m->from('noreply@odeo.co.id', 'odeo');

      $m->attachData($file, 'Price List - ' . $this->storeName . ' - ' . Carbon::now()->format('YmdHis') . '.' . $this->fileType);

      $m->to($this->email)->subject('Price List Odeo');

    });

  }
}
