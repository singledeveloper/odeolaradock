<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 3:29 PM
 */

namespace Odeo\Domains\Inventory\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\Marketing\Model\InventoryMarginMultiplier;

class ServiceDetail extends Entity {
    
    public function vendor() {
        return $this->belongsTo(Vendor::class);
    }

    public function service() {
        return $this->belongsTo(Service::class);
    }

    public function marginMultipliers() {
      return $this->hasMany(InventoryMarginMultiplier::class);
    }
}
