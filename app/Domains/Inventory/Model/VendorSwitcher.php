<?php


namespace Odeo\Domains\Inventory\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Store;

class VendorSwitcher extends Entity {

  public function ownerStore() {
    return $this->belongsTo(Store::class, 'owner_store_id');
  }

}