<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 5:25 PM
 */

namespace Odeo\Domains\Inventory\PDAM\Contract;

use Odeo\Domains\Core\PipelineListener;

interface PDAMContract {

  public function inquiry(PipelineListener $listener, $data);

  public function checkout(PipelineListener $listener, $data);

  public function purchasePostpaid(PipelineListener $listener, $data);

  public function validatePostpaidInventory(PipelineListener $listener, $data);

}