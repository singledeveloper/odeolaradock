<?php

namespace Odeo\Domains\Inventory\PGN;

use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;

class PGNManager extends InventoryManager {

  private static $pgnInstance;

  public function setVendor(PipelineListener $listener, $data) {

    self::$pgnInstance = app()->make(\Odeo\Domains\Inventory\PGN\Odeo\OdeoManager::class);

    $this->setServiceDetail($data['service_detail_id']);

    return $listener->response(200);

  }

  public function __call($name, $argument) {

    $this->setVendor($argument[0], $argument[1]);
    return call_user_func_array([self::$pgnInstance, $name], $argument);

  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {

    if (Pulsa::IS_MAINTANANCE) return $listener->response(400, Pulsa::MAINTANANCE_MESSAGE);

    $repository = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    if (!isset($data['service_detail_id']) || (isset($data['service_detail_id']) && $data['service_detail_id'] == '0'))
      $data['service_detail_id'] = ServiceDetail::PGN_ODEO;

    if ($inventory = $repository->getCheapestFee($data['service_detail_id'])) {
      $this->setVendor($listener, $data);
      $data['inventory_id'] = $inventory->id;
      $data['vendor_switcher_id'] = $inventory->vendor_switcher_id;
      $listener->replaceData($data);

      self::$pgnInstance->validatePostpaidInventory($listener, $data);
    }
    else return $listener->response(400, trans('pulsa.cant_purchase'));
  }

}
