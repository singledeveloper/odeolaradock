<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 5:25 PM
 */

namespace Odeo\Domains\Inventory\Pln\Contract;


use Odeo\Domains\Core\PipelineListener;

interface PlnContract {

  public function get(PipelineListener $listener, $data);

  public function checkout(PipelineListener $listener, $data);

  public function purchase(PipelineListener $listener, $data);

}