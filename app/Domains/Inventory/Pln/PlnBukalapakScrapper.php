<?php

namespace Odeo\Domains\Inventory\Pln;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Affiliate;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;

class PlnBukalapakScrapper {

  private $plnInquiries, $plnInquiryUsers, $cashInserter, $inquiryFees, $redis;

  public function __construct() {
    $this->plnInquiries = app()->make(\Odeo\Domains\Inventory\Pln\Repository\PlnInquiryRepository::class);
    $this->plnInquiryUsers = app()->make(\Odeo\Domains\Inventory\Pln\Repository\PlnInquiryUserRepository::class);
    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->inquiryFees = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateInquiryFeeRepository::class);
    $this->redis = Redis::connection();
  }

  private $userAgent = false;

  private function setUserAgent($reset = false) {
    if (!$reset) $this->userAgent = $this->redis->hget(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_PLN_USER_AGENT);
    if ($reset || !$this->userAgent) {
      $this->userAgent = \Campo\UserAgent::random();
      $this->redis->hset(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_PLN_USER_AGENT, $this->userAgent);
    }
  }

  public function getPrepaidInquiry(PipelineListener $listener, $data) {

    $inquiry = $this->plnInquiries->findByNumber($data['number']);

    if (!$inquiry || ($inquiry && time() > strtotime($inquiry->expired_at))) {
      $this->setUserAgent();
      $client = new \GuzzleHttp\Client([
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ],
        'headers' => [
          'Content-Type' => 'application/json',
          'User-Agent' => $this->userAgent
        ]
      ]);

      try {
        $json = [
          'customer_number' => $data['number'],
          'product_id' => 0
        ];
        $response = json_decode($client->post('https://www.bukalapak.com/auth_proxies/request_token')->getBody()->getContents());
        $response = json_decode($client->post('https://api.bukalapak.com/electricities/prepaid-inquiries?access_token=' . $response->access_token, [
          'json' => $json
        ])->getBody()->getContents())->data;

        if (!$inquiry) $inquiry = $this->plnInquiries->getNew();
        $inquiry->number = $data['number'];
        $inquiry->name = $response->customer_name;
        $inquiry->tariff = $response->segmentation;
        $inquiry->power = $response->power;
        $inquiry->expired_at = Carbon::now()->addDays(7)->toDateTimeString();
        $this->plnInquiries->save($inquiry);
      }
      catch (\Exception $e) {
        if (isset($data['is_error'])) {
          $message = $e->getMessage();
          if (strpos($message, 'tidak ditemukan') !== false)
            return $listener->response(400, 'Nomor PLN tidak terdaftar.');
          else if (strpos($message, 'tetap semangat') !== false)
            return $listener->response(400, 'Nomor PLN tidak valid.');
          clog('bukalapak_pln_error', $message . ', data: ' . json_encode($json));
          return $listener->response(400, 'Inquiry PLN sedang dalam gangguan. Mohon kontak @odeo_cs');
        }
      }
    }

    if ($inquiry && $inquiry->id) {
      if (isset($data['auth']) && !$inquiryUser = $this->plnInquiryUsers->findByUserAndInquiryId($data['auth']['user_id'], $inquiry->id)) {
        $inquiryUser = $this->plnInquiryUsers->getNew();
        $inquiryUser->pln_inquiry_id = $inquiry->id;
        $inquiryUser->user_id = $data['auth']['user_id'];
        $this->plnInquiryUsers->save($inquiryUser);
      }

      if (isset($data['auth']) && isset($data['is_error'])) {
        $fee = $this->inquiryFees->findByUserId($data['auth']['user_id']);
        $fee = $fee && $fee->pln_inquiry_fee != null ? $fee->pln_inquiry_fee : Affiliate::DEFAULT_PLN_INQUIRY_FEE;
        if ($fee > 0) {
          try {
            $this->cashInserter->add([
              'user_id' => $data['auth']['user_id'],
              'trx_type' => TransactionType::AFFILIATE_INQUIRY_FEE,
              'cash_type' => CashType::OCASH,
              'amount' => -$fee,
              'data' => json_encode([
                'number' => $data['number'],
                'type' => 'pln'
              ]),
              'reference_type' => TransactionType::AFFILIATE_PLN_INQUIRY,
              'reference_id' => $inquiry->id
            ]);
            $ids = $this->cashInserter->run();
          }
          catch (InsufficientFundException $e) {
            return $listener->response(400, 'Saldo Anda tidak mencukupi.');
          }
        }
      }

      return $listener->response(200, [
        'inquiry' => array_merge((isset($ids) ? ['id' => $ids[0]] : []), [
          'number' => $data['number'],
          'subscriber_id' => '-',
          'name' => $inquiry->name,
          'tariff' => $inquiry->tariff,
          'power' => $inquiry->power
        ])
      ]);
    }

    return $listener->response(200, ['inquiry' => [
      'number' => $data['number'],
      'subscriber_id' => '-',
      'name' => '-',
      'tariff' => '-',
      'power' => '-'
    ]]);

  }

}