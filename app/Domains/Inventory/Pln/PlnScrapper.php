<?php

namespace Odeo\Domains\Inventory\Pln;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Affiliate;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;

class PlnScrapper {

  private $plnInquiries, $plnInquiryUsers, $cashInserter, $inquiryFees, $redis;

  public function __construct() {
    $this->plnInquiries = app()->make(\Odeo\Domains\Inventory\Pln\Repository\PlnInquiryRepository::class);
    $this->plnInquiryUsers = app()->make(\Odeo\Domains\Inventory\Pln\Repository\PlnInquiryUserRepository::class);
    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->inquiryFees = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateInquiryFeeRepository::class);
    $this->redis = Redis::connection();
  }

  private $userAgent = false;

  private function fetch($array, $plnKeys) {
    $result = [];

    $x = 0;
    foreach ($array as $item) {
      if (in_array($item, $plnKeys)) {
        $result[$item] = '';
        if (isset($array[$x + 1])) {
          $result[$item] = !in_array(trim($array[$x + 1]), $plnKeys) ? trim($array[$x + 1]) : '';
        }
      }
      $x++;
    }
    return $result;
  }

  private function setUserAgent($reset = false) {
    if (!$reset) $this->userAgent = $this->redis->hget(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_PLN_USER_AGENT);
    if ($reset || !$this->userAgent) {
      $this->userAgent = \Campo\UserAgent::random();
      $this->redis->hset(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_PLN_USER_AGENT, $this->userAgent);
    }
  }

  public function getPrepaidInquiry(PipelineListener $listener, $data) {

    $inquiry = $this->plnInquiries->findByNumber($data['number']);

    if (!$inquiry || ($inquiry && !isset($data['is_proxy']) && time() > strtotime($inquiry->expired_at))) {

      $params = [
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ]
      ];
      if (isset($data['is_proxy'])) $params['proxy'] = Pulsa::getProxyURL(rand(9990, 9999));
      $client = new \GuzzleHttp\Client($params);

      try {
        $this->setUserAgent();
        $alreadyLoop = false;
        do {
          $response = $client->request('POST', 'https://layanan.pln.co.id/id.co.iconpln.web.PDMohonEntryPoint/TransService', [
            'headers' => [
              'Content-Type' => 'text/x-gwt-rpc; charset=UTF-8',
              'User-Agent' => $this->userAgent
            ],
            'body' => '5|0|7|https://layanan.pln.co.id/id.co.iconpln.web.PDMohonEntryPoint/|'
              .time().'|id.co.iconpln.web.client.service.TransService|getDataPelangganBykriteria|java.lang.String/'
              .time().'|nometer|' . $data['number'] . '|1|2|3|4|2|5|5|6|7|',
            //'timeout' => isset($data['is_proxy']) ? 30 : 10
            'timeout' => 10
          ]);

          $response = $response->getBody()->getContents();
          preg_match_all('`"([^"]*)"`', $response, $raws);

          if (count($raws[1]) > 0 || $alreadyLoop) break;
          else {
            $this->setUserAgent(true);
            $alreadyLoop = true;
          }
        } while (true);

        if (isset($data['is_error']) && count($raws[1]) <= 0)
          return $listener->response(400, 'Inquiry PLN sedang dalam gangguan. Mohon kontak @odeo_cs');

        $result = $this->fetch($raws[1], ['nama', 'tarif', 'daya', 'idpel', 'namaup', 'merek_kwh', 'nopabrik_kwh']);

        if (!$inquiry) $inquiry = $this->plnInquiries->getNew();
        $inquiry->number = $data['number'];
        $inquiry->subscriber_id = isset($result['idpel']) ? $result['idpel'] : '-';
        $inquiry->name = $result['nama'];
        $inquiry->tariff = rtrim($result['tarif'], 'T');
        $inquiry->power = $result['daya'];
        $inquiry->area = strtoupper($result['namaup']);
        $inquiry->log = json_encode($raws[1]);
        $inquiry->expired_at = Carbon::now()->addDays(7)->toDateTimeString();
        $this->plnInquiries->save($inquiry);
      }
      catch (\Exception $e) {
        $errMsg = $e->getMessage();
        if (isset($data['switcher_id']) && strpos($errMsg, 'Undefined index') === false) {
          clog('pln_error', json_encode($data) . ', MESSAGE: ' . $errMsg);
          $lastId = $this->redis->hget(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_PLN_AUTO_SCRAPE_LAST_ID);
          if ($lastId && $lastId > $data['switcher_id']) {
            $this->redis->hset(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_PLN_AUTO_SCRAPE_LAST_ID, $data['switcher_id'] - 1);
          }
        }
        if (strpos($errMsg, 'timed out') !== false) {
          $switchFlag = $this->redis->get(Pulsa::REDIS_PLN_SWITCH_LOCK);
          if (!$switchFlag) $switchFlag = 0;
          $switchFlag++;
          $this->redis->set(Pulsa::REDIS_PLN_SWITCH_LOCK, $switchFlag);
          if ($switchFlag >= Pulsa::PLN_SWITCH_FLAG)
            $this->redis->expire(Pulsa::REDIS_PLN_SWITCH_LOCK, 3600);
        }
      }
    }

    if ($inquiry && $inquiry->id) {
      if (isset($data['auth']) && !$inquiryUser = $this->plnInquiryUsers->findByUserAndInquiryId($data['auth']['user_id'], $inquiry->id)) {
        $inquiryUser = $this->plnInquiryUsers->getNew();
        $inquiryUser->pln_inquiry_id = $inquiry->id;
        $inquiryUser->user_id = $data['auth']['user_id'];
        $this->plnInquiryUsers->save($inquiryUser);
      }
      if (isset($data['is_error'])) {
        if (!isset($result)) $result = $this->fetch(json_decode($inquiry->log, true), ['merek_kwh', 'nopabrik_kwh']);
        try {
          if (trim($result['merek_kwh']) == 'MELCOINDA' && trim($result['nopabrik_kwh']) == '56'
            && ((date('H') == 11 && intval(date('i')) >= 58) ||
              (date('H') == 12 && intval(date('i')) <= 5))) {
            return $listener->response(400, 'Nomor meter Anda terdaftar 
              dengan merek stand Melcoinda dan kode meter 56, sehingga tidak dapat membeli token 
              pada pukul 11.58 - 12.05 WIB, mohon lakukan pergantian ke PLN setempat.');
          }
        }
        catch(\Exception $e) {}
      }

      if (isset($data['auth']) && isset($data['is_error'])) {
        $fee = $this->inquiryFees->findByUserId($data['auth']['user_id']);
        $fee = $fee && $fee->pln_inquiry_fee != null ? $fee->pln_inquiry_fee : Affiliate::DEFAULT_PLN_INQUIRY_FEE;
        if ($fee > 0) {
          try {
            $this->cashInserter->add([
              'user_id' => $data['auth']['user_id'],
              'trx_type' => TransactionType::AFFILIATE_INQUIRY_FEE,
              'cash_type' => CashType::OCASH,
              'amount' => -$fee,
              'data' => json_encode([
                'number' => $data['number'],
                'type' => 'pln'
              ]),
              'reference_type' => TransactionType::AFFILIATE_PLN_INQUIRY,
              'reference_id' => $inquiry->id
            ]);
            $ids = $this->cashInserter->run();
          }
          catch (InsufficientFundException $e) {
            return $listener->response(400, 'Saldo Anda tidak mencukupi.');
          }
        }
      }

      return $listener->response(200, [
        'inquiry' => array_merge((isset($ids) ? ['id' => $ids[0]] : []), [
          'number' => $data['number'],
          'subscriber_id' => $inquiry->subscriber_id,
          'name' => $inquiry->name,
          'tariff' => $inquiry->tariff,
          'power' => $inquiry->power
        ])
      ]);
    }
    else if (isset($data['is_error'])) {
      if (!isset($result))
        return $listener->response(400, 'Inquiry PLN sedang dalam gangguan. Mohon dicoba beberapa saat lagi.');
      else if (!isset($result['idpel']))
        return $listener->response(400, 'Nomor PLN tidak terdaftar.');
    }

    return $listener->response(200, ['inquiry' => [
      'number' => $data['number'],
      'subscriber_id' => '-',
      'name' => '-',
      'tariff' => '-',
      'power' => '-'
    ]]);

  }

}