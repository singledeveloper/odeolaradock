<?php

namespace Odeo\Domains\Inventory\Pln\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Pln\Model\PlnInquiry;

class PlnInquiryRepository extends Repository {

  public function __construct(PlnInquiry $plnInquiry) {
    $this->model = $plnInquiry;
  }

  public function findByNumber($number) {
    return $this->model->where("number", $number)->first();
  }

}
