<?php

namespace Odeo\Domains\Inventory\Pln\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Pln\Model\PlnInquiryUser;

class PlnInquiryUserRepository extends Repository {

  public function __construct(PlnInquiryUser $plnInquiryUser) {
    $this->model = $plnInquiryUser;
  }

  public function findByUserAndInquiryId($userId, $plnInquiryId) {
    return $this->model->where("user_id", $userId)->where("pln_inquiry_id", $plnInquiryId)->first();
  }

}
