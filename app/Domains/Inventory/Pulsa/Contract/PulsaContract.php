<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 5:25 PM
 */

namespace Odeo\Domains\Inventory\Pulsa\Contract;


use Odeo\Domains\Core\PipelineListener;

interface PulsaContract {

  public function searchNominal(PipelineListener $listener, $data);

  public function checkout(PipelineListener $listener, $data);

  public function purchase(PipelineListener $listener, $data);

  public function validateInventory(PipelineListener $listener, $data);

}