<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 3:31 PM
 */

namespace Odeo\Domains\Inventory\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Model\ServiceDetail;

class ServiceDetailRepository extends Repository {

  public function __construct(ServiceDetail $serviceDetail) {
    $this->model = $serviceDetail;
  }

  public function findDetail($serviceDetailId) {
    return $this->model->with(['vendor', 'service'])->where("id", $serviceDetailId)->first();
  }

  public function getDetails($serviceDetailIds = []) {
    $query = $this->model->with(['vendor', 'service']);
    if (count($serviceDetailIds) > 0) $query = $query->whereIn("id", $serviceDetailIds)->with(['marginMultipliers' => function ($query) {
      $query->select('service_detail_id', 'margin', 'multiplier');
    }]);
    return $query->where("is_active", true)
      ->orderBy('priority_sort', 'desc')
      ->orderBy('id', 'asc')->get();
  }

  public function findById($id, $fields = ['*']) {

    $key = $this->model->getTable();

    return $this->runCache($key, $key . '.filter.id.' . $id, 60 * 60 * 24, function () use ($id, $fields) {
      return parent::findById($id, $fields);
    });

  }
}
