<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 3:31 PM
 */

namespace Odeo\Domains\Inventory\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Model\Vendor;

class VendorRepository extends  Repository {

  public function __construct(Vendor $vendor) {
    $this->model = $vendor;
  }



}