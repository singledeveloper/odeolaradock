<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/21/16
 * Time: 8:25 PM
 */

namespace Odeo\Domains\Inventory;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class ServiceDetailSelector implements SelectorListener {


  private $serviceDetail;

  public function __construct() {

    $this->serviceDetail = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);

  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getAllServiceDetail(PipelineListener $listener) {

    $serviceDetail = [];

    foreach ($this->serviceDetail->getAll() as $item) {
      $serviceDetail[] = $this->_transforms($item, $this->serviceDetail);
    }

    if (count($serviceDetail) > 0) {
      return $listener->response(200, [
        'serviceDetail' => $serviceDetail
      ]);
    }
    return $listener->response(204, null);
  }

  public function findServiceDetail(PipelineListener $listener, $data) {


    $serviceDetail = $this->serviceDetail->find($data['service_detail_id']);

    if (!$serviceDetail) return $listener->response(204, null);

    return $listener->response(200, [
      'serviceDetail' => $serviceDetail
    ]);
    
  }
}