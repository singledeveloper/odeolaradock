<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/21/16
 * Time: 8:25 PM
 */

namespace Odeo\Domains\Inventory;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class ServiceSelector implements SelectorListener {


  private $services;

  public function __construct() {

    $this->services = app()->make(\Odeo\Domains\Inventory\Repository\ServiceRepository::class);

  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getAllService(PipelineListener $listener) {

    $services = [];

    foreach ($this->services->getAll() as $item) {
      $services[] = $this->_transforms($item, $this->services);
    }

    if (count($services) > 0) {
      return $listener->response(200, [
        'service' => $services
      ]);
    }
    return $listener->response(204);
  }

  public function findService(PipelineListener $listener, $data) {

    $service = $this->services->find($data['service_id']);

    if (!$service) return $listener->response(204, null);

    return $listener->response(200, [
      'services' => $service
    ]);
    
  }
}