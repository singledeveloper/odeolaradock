<?php

namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Helper;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BillerASG;

class ApiManager {

  private $redis, $namespace;

  public function __construct() {
    $this->redis = Redis::connection();
    $this->namespace = 'odeo_core:asg_token';
    $this->client = new \GuzzleHttp\Client([
      'timeout' => 120,
    ]);
  }

  public function getByPhoneNumber($phoneNumber, $status, $limit = 20, $offset = 0) {
    $url = $this->buildUrl('api/invoice/GetByPhone', [$phoneNumber, $status, $limit, $offset]);
    return $this->callApi($url, 'GET');
  }

  public function getUnitByProjectId($projectId) {
    $url = $this->buildUrl('api/general/getunit', [$projectId]);

    return $this->callApi($url, 'GET');  
  }

  public function getUnitInvoiceByStatus($unitId, $status, $limit = 20, $offset = 1) {
    $url = $this->buildUrl('api/invoice/get', [$limit, $offset, $unitId, $status, BillerASG::OTYPE_OWNER]);

    return $this->callApi($url, 'GET');
  }

  public function getAgingInvoiceByUnit($projectId, $unitId) {
    $url = $this->buildUrl('api/invoice/getaging', [$projectId, $unitId, BillerASG::OTYPE_OWNER]);

    return $this->callApi($url, 'GET');
  }

  public function getByInvoiceId($invoiceId) {
    $url = $this->buildUrl('api/invoice/getinvoice', [$invoiceId]);

    return $this->callApi($url, 'GET');
  }

  public function getUnitInvoiceByPeriod($unitId, $period) {
    $url = $this->buildUrl('api/invoice/getdetail', [$unitId, $period]);

    return $this->callApi($url, 'GET');
  }

  public function getProjectInvoicesByStatus($projectId, $status, $limit = 20, $offset = 1) {
    $url = $this->buildUrl('api/invoice/getbyproject', [$limit, $offset, $clusterId, $status]);

    return $this->callApi($url, 'GET');
  }

  public function getProjectInvoicesByPeriod($projectId, $period) {
    $url = $this->buildUrl('api/invoice/getbycluster', [$projectId, BillerASG::ALL, $period]);

    return $this->callApi($url, 'GET');
  }

  public function getCurrentInvoicesByProject($projectId) {
    $url = $this->buildUrl('api/invoice/getcurrent', [$projectId]);

    return $this->callApi($url, 'GET');
  }

  public function getSummariesChartByPeriod($chartType, $period, $type) {
    $url = $this->buildUrl('api/summaries/get', [$chartType, BillerASG::ALL, $type, $period]);

    return $this->callApi($url, 'GET');
  }

  public function postPayment($orderId, $paymentDate, $paymentType, $projectId, $unitId, $refNo, $paymentAmount, $invoiceNumbers) {
    $url = $this->buildUrl('api/invoice/postpayment');

    return $this->callApi($url, 'POST', [
      'form_params' => [
        'paymentNo' => $orderId,
        'paymentDate' => $paymentDate,
        'paymentType' => $paymentType,
        'project' => $projectId,
        'house' => $unitId,
        'refNo' => $refNo,
        'amount' => $paymentAmount,
        'invNo' => $invoiceNumbers
      ]
    ]);
  }

  public function voidPayment($orderId) {
    $url = $this->buildUrl('api/invoice/voidpayment');

    return $this->callApi($url, 'POST', [
      'form_params' => [
        'paymentNo' => $orderId,
      ]
    ]);
  }

  public function pushNotification($invoiceNumbers) {
    $token = isProduction() ? BillerASG::NOTIFICATION_TOKEN : BillerASG::NOTIFICATION_TOKEN_STAGING;
    $url = isProduction() ? BillerASG::NOTIFICATION_URL : BillerASG::NOTIFICATION_URL_STAGING;

    try {
      $response = $this->client->request('POST', $url, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Authorization' => 'Bearer ' . $token,
        ],
        'json' => [
          'biller_invoice_number' => $invoiceNumbers
        ]
      ]);
      
      return json_decode($response->getBody()->getContents(), true);
    } catch (\Exception $e) {
      clog('asg_api', $e->getMessage());
      return ['status'=>false];
    }
  }

  private function callApi($url, $type, $params = []) {
    $token = $this->getToken();
    
    if (!$token) return false;

    try {
      $config = array_merge([
        'headers' => [
          'Authorization' => 'Bearer ' . $token,
        ],
        'http_errors' => false
      ], $params);
      $response = $this->client->request($type, $url, $config);

      $body = $response->getBody()->getContents();

      return array_merge(json_decode(empty($body) ? '{"error": true}' : $body, true), ['status_code' => $response->getStatusCode()]);
    } catch (\Exception $e) {
      clog('asg_api', $e->getMessage());
      return ['error' => true, 'status_code' => 999];
    }
  }

  private function getToken() {
//    if ($token = $this->redis->get($this->namespace)) return $token;

    try {
      if (isProduction()) {
        $url = BillerASG::TOKEN_URL;
      } else {
        $url = BillerASG::TOKEN_URL_STAGING;
      }
      $response = $this->client->request('POST', $url, [
        'headers' => [
          'Content-Type' => 'application/x-www-form-urlencoded',
        ],
        'form_params' => [
          'grant_type' => BillerASG::GRANT_TYPE,
          'username' => BillerASG::USERNAME,
          'password' => BillerASG::PASSWORD
        ]
      ]);

      $data = json_decode($response->getBody()->getContents(), true);

      $this->redis->setex(
        $this->namespace, 
        \Carbon\Carbon::parse($data['.expires'])->timestamp - \Carbon\Carbon::now()->timestamp - 60 * 5,
        $data['access_token']
      );

      return $data['access_token'];
    } catch (\Exception $exception) {
      throw $exception;
    }
  }

  private function buildUrl($path, $params = []) {
    if (isProduction()) {
      $url = BillerASG::BASE_URL . $path;
    } else {
      $url = BillerASG::BASE_URL_STAGING . $path;
    }

    foreach ($params as $param) {
      $url .= '/' . $param;
    }

    return $url;
  }

}