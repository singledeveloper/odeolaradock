<?php

namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Jobs;

use DB;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Constant\BillerASG;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\InvoiceUpdater;
use Odeo\Jobs\Job;

class NotifyInvoicePayment extends Job {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    if (isProduction()) {
      if ($this->data['payment_name'] == 'Debit / Credit Card Present (PAX Arta Jasa Switching)') return;
      
      $api = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Helper\ApiManager::class);
      $notifyLogRepo = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceNotifyLogRepository::class);

      $response = $api->postPayment(
        $this->data['order_id'],
        $this->data['payment_date'],
        $this->data['payment_name'],
        $this->data['project_id'],
        $this->data['billed_user_id'],
        $this->data['reference_id'],
        $this->data['payment_amount'],
        $this->data['invoice_numbers']
      );

      $notifyLog = $notifyLogRepo->getNew();
      $notifyLog->user_id = BillerASG::getOdeoUserId();
      $notifyLog->invoice_number = $this->data['reference_id'];
      $notifyLog->request = json_encode($this->data);
      $notifyLog->response = json_encode($response);
      $notifyLog->response_code = $response['status_code'];

      $notifyLogRepo->save($notifyLog);

      if (isset($response['error']) && !$response['error']) {
        $notifyResponse = $api->pushNotification($this->data['invoice_numbers']);
        
        if (!$notifyResponse['status']) {
          clog('asg_api', json_encode($notifyResponse));
        }
      } else if ($response['status_code'] != 200 || (isset($response['error']) && $response['error'])) {
        $notifyCount = $notifyLogRepo->getCloneModel()->where('invoice_number', $this->data['reference_id'])->count();
  
        if ($notifyCount < 5) {
          dispatch(with(new NotifyInvoicePayment($this->data))->delay(30 * $notifyCount));
        } else {
          dispatch(new SendInvoicePaymentNotifyFailedAlert($this->data['order_id']));
        }
      }
    } else {
      clog('affiliate_invoice_notify', json_encode($this->data));
    }
  }
}
