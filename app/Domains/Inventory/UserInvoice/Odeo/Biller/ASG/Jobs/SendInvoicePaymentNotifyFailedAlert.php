<?php

namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Odeo\Jobs\Job;
use Illuminate\Support\Facades\Mail;

class SendInvoicePaymentNotifyFailedAlert extends Job implements ShouldQueue {

  use SerializesModels;

  private $orderId;

  public function __construct($orderId) {
    parent::__construct();
    $this->orderId = $orderId;
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

    Mail::send('emails.invoice_payment_notify_failed_alert', [
      'data' => [
        'order_id' => $this->orderId,
      ]
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo')
        ->to('pg@odeo.co.id')
        ->subject('Invoice Payment Notify Failed - ' . date('Y-m-d'));
    });
  }

}
