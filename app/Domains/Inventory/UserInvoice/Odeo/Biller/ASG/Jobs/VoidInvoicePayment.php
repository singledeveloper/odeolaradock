<?php

namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Jobs;

use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceVoidLogRepository;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Helper\ApiManager;
use Odeo\Jobs\Job;

class VoidInvoicePayment extends Job {

  private $orderId;

  public function __construct($orderId) {
    parent::__construct();
    $this->orderId = $orderId;
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

    $api = app()->make(ApiManager::class);
    $voidLogRepo = app()->make(AffiliateUserInvoiceVoidLogRepository::class);

    $response = $api->voidPayment($this->orderId);

    $voidLog = $voidLogRepo->getNew();
    $voidLog->order_id = $this->orderId;
    $voidLog->response = json_encode($response);
    $voidLog->response_code = $response['status_code'];

    $voidLogRepo->save($voidLog);

    if ($response['status_code'] != 200 || (isset($response['error']) && $response['error'])) {
      $count = $voidLogRepo->getCloneModel()->where('order_id', $this->orderId)->count();

      if ($count < 5) {
        dispatch(with(new VoidInvoicePayment($this->orderId))->delay(30 * $count));
      } else {
        dispatch(new SendInvoiceVoidFailedAlert($this->orderId));
      }
    }
  }
}
