<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG;

use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\BillerASG;

class UserInvoiceManager {

  public function validateInventory(PipelineListener $listener, $data) {
    return app()->make(InventoryValidator::class)->validateInventory($listener, $data);
  }

  public function completeOrder(PipelineListener $listener, $data) {
    return app()->make(Requester::class)->completeOrder($listener, $data);
  }

  public function initializeInvoiceUsers(PipelineListener $listener, $data) {
    $api = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Helper\ApiManager::class);
    $userRepo = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceUserRepository::class);
    $vaRepo = app()->make(\Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountRepository::class);
    $vaDetailRepo = app()->make(\Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountDetailRepository::class);
    $vendorRepo = app()->make(\Odeo\Domains\VirtualAccount\Repository\VirtualAccountVendorRepository::class);

    $billerId = $data['biller_id'];
    $projectId = BillerASG::getProjectIdByBiller($data['biller_id']);

    $units = $api->getUnitByProjectId($projectId);

    $users = [];
    $virtualAccounts = [];
    $virtualAccountDetails = [];

    foreach ($units as $unit) {
      $users[] = [
        'biller_id' => $billerId,
        'user_id' => $unit['house'],
        'name' => '',
        'email' => '',
        'telephone' => '',
        'address' => $unit['houseDesc'],
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
      ];
      $virtualAccounts[] = [
        'user_id' => $unit['house'],
        'biller' => $billerId,
        'service_detail_id' => ServiceDetail::USER_INVOICE_ODEO,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
      ];
    }

    $userRepo->saveBulk($users);
    $vaRepo->saveBulk($virtualAccounts);

    $insertedVirtualAccounts = $vaRepo->findAllByAttributes('biller', $billerId);
    $vendors = $vendorRepo->getAffiliatePaymentVendors();

    foreach ($vendors as $vendor) {
      $availableLength = $vendor->max_length - strlen($vendor->prefix);

      foreach($insertedVirtualAccounts as $va) {
        $virtualAccountDetails[] = [
          'user_virtual_account_id' => $va->id,
          'virtual_account_number' => $vendor->prefix . sprintf('%0' . $availableLength . "s", $va->id),
          'vendor' => $vendor->code,
          'created_at' => \Carbon\Carbon::now(),
          'updated_at' => \Carbon\Carbon::now()
        ];
      }
    }
    $vaDetailRepo->saveBulk($virtualAccountDetails);

    return $listener->response(200);
  }

  public function inquiry(PipelineListener $listener, $data) {
    if (isset($data['biller_invoice_number'])) {
      return app()->make(Inquirer::class)->getByNumber($listener, $data);
    } else if (isset($data['user_id'])) {
      return app()->make(Inquirer::class)->getUnpaid($listener, $data);
    }
  }

  public function pay(PipelineListener $listener, $data) {
    return app()->make(Inquirer::class)->getByNumber($listener, array_merge($data, ['for_payment'=>true]));
  }

  public function cancelPay(PipelineListener $listener, $data) {
    $tempInvoice = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Helper\InvoiceTempInquiry::class);
    
    $tempInvoice->deleteInvoiceNumber($data['biller_id'], $data['user_id']);

    return $listener->response(200);
  }

  public function terminalInquiry(PipelineListener $listener, $data) {
    return app()->make(Inquirer::class)->getByPhoneNumber($listener, $data);
  }

  public function resendNotification(PipelineListener $listener, $data) {
    return app()->make(Requester::class)->resendNotification($listener, $data);
  }

  public function refund(PipelineListener $listener, $data) {
    return app()->make(Refunder::class)->refund($listener, $data);
  }
}
