<?php

namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Helper;

use Carbon\Carbon;
use Odeo\Domains\Constant\UserInvoice;

class UserInvoiceDataGenerator {

  public function generate($invoice, $data) {
    $invoiceData = [
      'user_id' => $data['biller_user_id'] ?? $data['auth']['user_id'],
      'billed_user_id' => $invoice['billed_user_id'],
      'billed_user_name' => $invoice['billed_user_name'],
      'billed_user_email' => $invoice['billed_user_email'] ?? '',
      'billed_user_telephone' => $invoice['billed_user_telephone'] ?? '',
      'period' => $invoice['period'] ?? '0000-00',
      'due_date' => $invoice['due_date'],
      'name' => $invoice['name'] ?? '',
      'items' => is_array($invoice['items']) ? json_encode($invoice['items']) : $invoice['items'],
      'invoice_number' => UserInvoice::generateInvoiceNumber($data['biller_id']),
      'biller_id' => $data['biller_id'],
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
      'created_by' => $data['auth']['user_id']
    ];
    
    $total = 0;
    foreach($invoice['items'] as $item) {
      $amount = $item['amount'] ?? 1;
      $total += $item['price'] * $amount;
    }

    if(isset($data['late_charge'])) {
      $invoiceData['late_charge'] = $invoice['late_charge'];
      $invoiceData['late_charge_interval'] = $invoice['late_charge_interval'] ?? 1;
      $invoiceData['enable_late_charge'] = true;
    }

    $invoiceData['subtotal'] = $total;
    $invoiceData['status'] = UserInvoice::OPENED;
    $invoiceData['total'] = $total;

    if(isset($data['enable_partial_payment'])) {
      $invoiceData['enable_partial_payment'] = $invoice['enable_partial_payment'];
      $invoiceData['minimum_payment'] = $invoice['enable_partial_payment'] ? ($invoice['minimum_payment'] ?? UserInvoice::MINIMUM_PARTIAL_PAYMENT_PERCENTAGE) : 100;
    }

    if(isset($data['enable_notification'])) {
      $invoiceData['enable_notification'] = $invoice['enable_notification'];
    }

    return $invoiceData;
  }
}