<?php

namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Jobs;

use Odeo\Jobs\Job;

class SendInvoiceEmail extends Job  {

  protected $billedUserEmail;
  protected $invoiceNumber;
  protected $invoiceName;
  protected $invoiceTotal;

  public function __construct($data) {
    parent::__construct();
    $this->billedUserEmail = $data['billed_user_email'];
    $this->invoiceName = $data['invoice_name'];
    $this->invoiceNumber = $data['invoice_number'];
    $this->invoiceTotal = $data['invoice_total'];
  }

  public function handle() {

  }
}
