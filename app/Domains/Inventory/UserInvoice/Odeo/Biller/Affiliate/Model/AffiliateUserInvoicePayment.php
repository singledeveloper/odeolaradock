<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Payment\Model\Payment;

class AffiliateUserInvoicePayment extends Entity {

  public $timestamps = false;

  protected $dates = ['paid_at'];

  public function payment() {
    return $this->belongsTo(Payment::class, 'order_id', 'order_id');
  }
}
