<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository;

use Carbon\Carbon;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Model\AffiliateUserInvoiceCharge;
use Odeo\Domains\Core\Repository;

class AffiliateUserInvoiceChargeRepository extends Repository {

  public function __construct(AffiliateUserInvoiceCharge $userInvoiceCharge) {
    $this->model = $userInvoiceCharge;
  }

  public function findByChargedAmount($invoiceId, $chargedAmount) {
    return $this->model->where('invoice_id', $invoiceId)->where('charged_amount', $chargedAmount)->first();
  }
}
