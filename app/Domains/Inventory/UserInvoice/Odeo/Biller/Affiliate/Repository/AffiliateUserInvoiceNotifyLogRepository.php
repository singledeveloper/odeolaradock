<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository;

use Carbon\Carbon;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Model\AffiliateUserInvoiceNotifyLog;
use Odeo\Domains\Core\Repository;

class AffiliateUserInvoiceNotifyLogRepository extends Repository {

  public function __construct(AffiliateUserInvoiceNotifyLog $userInvoiceNotifyLog) {
    $this->model = $userInvoiceNotifyLog;
  }

}
