<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository;

use Carbon\Carbon;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Model\AffiliateUserInvoicePayment;
use Odeo\Domains\Core\Repository;

class AffiliateUserInvoicePaymentRepository extends Repository {

  public function __construct(AffiliateUserInvoicePayment $userInvoicePayment) {
    $this->model = $userInvoicePayment;
  }

}
