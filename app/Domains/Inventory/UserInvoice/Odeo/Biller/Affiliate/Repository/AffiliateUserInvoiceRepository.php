<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository;

use Carbon\Carbon;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Model\AffiliateUserInvoice;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Constant\UserInvoice;

class AffiliateUserInvoiceRepository extends Repository {

  public function __construct(AffiliateUserInvoice $userInvoice) {
    $this->model = $userInvoice;
  }

  public function get() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel();

    $query = $query->where('biller_id', $filters['biller_id']);

    if (isset($filters["invoice_number"])) {
      $query = $query->where('invoice_number', $filters['invoice_number']);
    }
    if (isset($filters["biller_invoice_number"])) {
      if (is_array($filters["biller_invoice_number"])) {
        $query = $query->whereIn('name', $filters['biller_invoice_number']);
      } else {
        $query = $query->where('name', $filters['biller_invoice_number']);
      }
    }
    if (isset($filters["biller_invoice_status"])) {
      if (is_array($filters["biller_invoice_status"])) {
        $query = $query->whereIn('status', $filters['biller_invoice_status']);
      } else {
        $query = $query->where('status', $filters['biller_invoice_status']);
      }
    }
    if (isset($filters["billed_user_id"])) {
      $query = $query->where('billed_user_id', $filters['billed_user_id']);
    }
    if (isset($filters["period"])) {
      $query = $query->where('period', $filters['period']);
    }
    if (isset($filters["status"])) {
      switch ($filters['status']) {
        case "unpaid":
          $query = $query->where('status', UserInvoice::OPENED)->where('due_date', '<', Carbon::today());
          break;
        case "partial":
          $query = $query->where('status', UserInvoice::PARTIALLY_PAID)->where('due_date', '<', Carbon::today());
          break;
        case "overdue":
          $query = $query->where(function ($q) {
            $q->where('status', UserInvoice::OPENED)->orWhere('status', UserInvoice::PARTIALLY_PAID);
          })->where('due_date', '>=', Carbon::today());
          break;
        case "paid":
          $query = $query->where('status', UserInvoice::COMPLETED);
          break;
        case "void":
          $query = $query->where(function ($q) {
            $q->where('status', UserInvoice::VOID)->orWhere('status', UserInvoice::CANCELLED);
          });
          break;
        default:
          break;
      }
    }

    if ($this->hasExpand('payment')) $query = $query->with('invoicePayments.payment.information');

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function getDetail($lockForUpdate) {
    $filters = $this->getFilters();

    $query = $this->getCloneModel();

    if ($lockForUpdate) $query = $query->lockForUpdate();

    return $query->where('invoice_number', $filters['invoice_number'])->first();
  }

  public function getLate() {
    return $this->model->where('enable_late_charge', true)->where('status', '<', UserInvoice::COMPLETED)->where('due_date', '<', Carbon::today())->get();
  }

  public function findLatestByUserId($userId) {
    return $this->model->where('billed_user_id', $userId)->orderBy('id', 'desc')->first();
  }

  public function getChildren($invoiceId) {
    return $this->model->where('parent_invoice_id', $invoiceId)->get();
  }

  public function findUnpaidBillerInvoice($billerId, $name) {
    return $this->model->where('biller_id', $billerId)->where('name', $name)->whereIn('status', UserInvoice::UNPAID)->orderBy('id', 'desc')->first();
  }

  public function getByInvoiceNumber($invoiceNumber) {
    if (is_array($invoiceNumber)) {
      return $this->model->whereIn('invoice_number', $invoiceNumber)->get();
    } else {
      return $this->model->where('invoice_number', $invoiceNumber)->first();
    }
  }

  public function getUnpaidByBilledUser($billedUserId) {
    return $this->model->where('billed_user_id', $billedUserId)->whereIn('status', UserInvoice::UNPAID)->get();
  }

  public function getUnpaidByPhoneNumber($billerIds, $phoneNumber) {
    return $this->model
      ->whereIn('biller_id', $billerIds)
      ->where('billed_user_telephone', $phoneNumber)
      ->whereIn('status', UserInvoice::UNPAID)
      ->get();
  }

  public function getPaidByInvoiceNames($billerIds, $invoiceNames) {
    return $this->model
      ->whereIn('biller_id', $billerIds)
      ->whereIn('name', $invoiceNames)
      ->whereIn('status', UserInvoice::PAID)
      ->get();
  }

  public function getByInvoiceNames($billerIds, $invoiceNames) {
    return $this->model
      ->whereIn('biller_id', $billerIds)
      ->whereIn('name', $invoiceNames)
      ->get();
  }

  public function getByInvoiceNamesAndNotStatus($billerIds, $invoiceNames, $status) {
    return $this->model
      ->whereIn('biller_id', $billerIds)
      ->whereIn('name', $invoiceNames)
      ->where('status', '!=', $status)
      ->get();
  }

  public function findByOrderId($orderId) {
    return $this->model
      ->whereHas('invoicePayments', function ($query) use ($orderId) {
        $query->where('order_id', $orderId);
      })
      ->first();
  }

  public function listByOrderId($orderId) {
    return $this->model
      ->whereHas('invoicePayments', function ($query) use ($orderId) {
        $query->where('order_id', $orderId);
      })
      ->get();
  }

  public function getUnpaidByInvoiceNames($billerIds, $names) {
    return $this->model
      ->whereIn('biller_id', $billerIds)
      ->whereIn('name', $names)
      ->whereIn('status', UserInvoice::UNPAID)
      ->get();
  }

  public function getSumByMonthWithoutMdr($date) {
    $query = "
      SELECT
        to_char(paid_at, 'YYYY-MM')                  AS period,
        sum(total_paid + biller_fee)                 AS gmv,
        sum(biller_fee - payment_gateway_cost)       AS revenue,
        count(id)                                    AS trx
      FROM affiliate_user_invoices
      WHERE paid_at IS NOT NULL AND status=:status
        AND paid_at::date between :start_date and :end_date
        AND mdr = 0
      GROUP BY 1";

    return \DB::select(\DB::raw($query), [
      'status' => UserInvoice::COMPLETED,
      'start_date' => $date->copy()->subYear()->format('Y-m-d'),
      'end_date' => $date->format('Y-m-d'),
    ]);
  }

  public function getSumByMonthWithMdr($date) {
    $query = "
      SELECT
        to_char(paid_at, 'YYYY-MM')                  AS period,
        sum(total_paid + biller_fee)                 AS gmv,
        sum(biller_fee + mdr - payment_gateway_cost) AS revenue,
        count(id)                                    AS trx
      FROM affiliate_user_invoices
      WHERE paid_at IS NOT NULL AND status=:status
        AND paid_at::date between :start_date and :end_date
        AND mdr > 0
      GROUP BY 1";

    return \DB::select(\DB::raw($query), [
      'status' => UserInvoice::COMPLETED,
      'start_date' => $date->copy()->subYear()->format('Y-m-d'),
      'end_date' => $date->format('Y-m-d'),
    ]);
  }

  public function getUserInvoiceStat() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel();

    $query = $query->select(\DB::raw("SUM(CASE WHEN status = '10001' AND due_date >= now() THEN total ELSE 0 END) as total_unpaid,
      SUM(CASE WHEN status = '10001' AND due_date < now() THEN total ELSE 0 END) as total_overdue,
      SUM(CASE WHEN status = '50000' THEN total ELSE 0 END) as total_paid"));

    if (!isAdmin()) {
      $query = $query->where('store_id', $filters['store_id']);
    }

    return $query->first();
  }

}
