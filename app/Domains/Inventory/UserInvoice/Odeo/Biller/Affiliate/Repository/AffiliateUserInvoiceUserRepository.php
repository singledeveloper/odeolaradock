<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository;

use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Model\AffiliateUserInvoiceUser;
use Odeo\Domains\Core\Repository;

class AffiliateUserInvoiceUserRepository extends Repository {

  public function __construct(AffiliateUserInvoiceUser $userInvoiceUser) {
    $this->model = $userInvoiceUser;
  }

  public function get() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel();

    $query = $query->where('biller_id', $filters['biller_id']);

    if(isset($filters["user_id"])) {
      $query = $query->where('user_id', $filters['user_id']);
    }
    return $this->getResult($query->orderBy('id', 'asc'));
  }

  public function getByUserIds($billerIds, $userIds) {
    return $this->getCloneModel()
      ->whereIn('biller_id', $billerIds)
      ->whereIn('user_id', $userIds)
      ->get();
  }

}
