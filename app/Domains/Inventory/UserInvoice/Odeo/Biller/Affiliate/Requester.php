<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Jobs\UserInvoiceNotifier;
use Carbon\Carbon;

class Requester {

  public function completeOrder(PipelineListener $listener, $data) {
    $userInvoices = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository::class);
    $userInvoicePayments = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoicePaymentRepository::class);
    $orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
    $cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);

    $invoiceNumbers = json_decode($data['invoice_number']);
    if (is_array($invoiceNumbers) && count($invoiceNumbers) > 1) {
      return $listener->response(500, 'there should be only one invoice number');
    }

    $data['invoice_number'] = is_array($invoiceNumbers) ? $invoiceNumbers[0] : $invoiceNumbers;
    $userInvoices->normalizeFilters($data);
    $invoice = $userInvoices->getDetail(true);

    if($invoice->status >= UserInvoice::COMPLETED) return $listener->response(400);

    $orderDetail = $orderDetails->findById($data['order_detail_id']);

    $userInvoicePayment = $userInvoicePayments->getNew();
    $userInvoicePayment->invoice_id = $invoice->id;
    $userInvoicePayment->order_id = $orderDetail->order_id;
    $userInvoicePayment->amount = $orderDetail->sale_price;
    $userInvoicePayment->paid_at = Carbon::now();

    $userInvoicePayments->save($userInvoicePayment);

    $invoice->total_paid += $orderDetail->sale_price;
    if($invoice->total_paid == $invoice->total) {
      $invoice->status = UserInvoice::COMPLETED;
      $invoice->paid_at = Carbon::now();

      $settledAmount = $invoice->total_paid - $invoice->biller_fee;
      $cashInserter->add([
        'user_id' => $invoice->user_id,
        'trx_type' => TransactionType::INVOICE_PAYMENT,
        'cash_type' => CashType::OCASH,
        'amount' => $settledAmount,
        'data' => json_encode([
          'order_id' => $orderDetail->order_id,
          'invoice_number' => $invoice->invoice_number,
        ]),
      ]);

      $cashInserter->run();

      $listener->pushQueue(new UserInvoiceNotifier([
        'user_id' => $invoice->user_id,
        'invoice_number' => $invoice->invoice_number,
        'total' => $settledAmount
      ]));
    } else {
      $invoice->status = UserInvoice::PARTIALLY_PAID;
    }

    $userInvoices->save($invoice);

    return $listener->response(200);
  }

}
