<?php


namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Scheduler;

use Carbon\Carbon;

class LateChargeUpdater {

  private $userInvoices;

  public function __construct() {
    $this->userInvoices = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository::class);
    $this->userInvoiceCharges = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceChargeRepository::class);
  }

  public function run() {
    foreach ($this->userInvoices->getLate() as $userInvoice) {
      $now = Carbon::today();
      $dueDate = $userInvoice->due_date;
      $diff = $dueDate->diffInDays($now, false);

      if ($diff > 0 && $diff % $userInvoice->late_charge_interval == 0) {
        $chargedAmount = $userInvoice->subtotal + $userInvoice->total_child - $userInvoice->total_paid;
        $lateCharge = round($chargedAmount * ($userInvoice->late_charge / 100));
        $invoiceCharges = $this->userInvoiceCharges->findByChargedAmount($userInvoice->id, $chargedAmount);
        if (!$invoiceCharges) {
          $invoiceCharges = $this->userInvoiceCharges->getNew();
          $invoiceCharges->invoice_id = $userInvoice->id;
          $invoiceCharges->charged_amount = $chargedAmount;
        }
        $invoiceCharges->total_charges += $lateCharge;
        $this->userInvoiceCharges->save($invoiceCharges);
        $userInvoice->charges += $lateCharge;
        $userInvoice->total += $lateCharge;
        $this->userInvoices->save($userInvoice);
      }
    }
  }

}
