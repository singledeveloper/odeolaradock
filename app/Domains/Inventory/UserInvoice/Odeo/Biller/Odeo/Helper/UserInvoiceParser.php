<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Helper;

use Odeo\Domains\Constant\UserInvoice;
use Carbon\Carbon;

class UserInvoiceParser {

  public function parseStatus($invoice) {
    $status = [];
    $status['text_color'] = '#092529';

    switch($invoice->status) {
      case UserInvoice::OPENED:
        if($invoice->due_date && $invoice->due_date->lte(Carbon::now())) {
          $status['color'] = '#FF3700';
          $status['text'] = trans('status.api_overdue');
        } else {
          $status['color'] = '#F5B80E';
          $status['text'] = trans('status.api_unpaid');
        }
        break;
      case UserInvoice::PARTIALLY_PAID:
        $status['color'] = '#F87D0B';
        $status['text'] = trans('status.api_partial_paid');
        break;
      case UserInvoice::COMPLETED:
        $status['color'] = '#00A65A';
        $status['text'] = trans('status.api_paid');
        break;
      case UserInvoice::CANCELLED:
      case UserInvoice::VOID:
        $status['color'] = '#DD4B39';
        $status['text'] = trans('status.api_void');
        break;
    }

    return $status;
  }

  public function getStatusMessage($invoice) {
    switch($invoice->status) {
      case UserInvoice::OPENED:
        if($invoice->due_date && $invoice->due_date->lte(Carbon::now())) {
          return trans('status.api_overdue');
        } else {
          return trans('status.api_unpaid');
        }
      case UserInvoice::VERIFIED:
        return trans('status.api_verified');
      case UserInvoice::PARTIALLY_PAID:
        return trans('status.api_partial_paid');
      case UserInvoice::COMPLETED:
      case UserInvoice::MERGED:
      case UserInvoice::COMPLETED_WITH_EXTERNAL_PAYMENT:
        return trans('status.api_paid');
      case UserInvoice::CANCELLED:
      case UserInvoice::VOID:
        return trans('status.api_void');
    }

    return 'UNKNOWN';
  }
}
