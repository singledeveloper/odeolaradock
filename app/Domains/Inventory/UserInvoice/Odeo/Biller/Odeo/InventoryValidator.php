<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository;

class InventoryValidator {

  public function __construct() {
    $this->userInvoices = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Repository\UserInvoiceRepository::class);
    $this->userInvoices = app()->make(AffiliateUserInvoiceRepository::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    $userInvoice = $this->userInvoices->findByAttributes('invoice_number', $data['invoice_number']);
    $currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

    if ($userInvoice && $userInvoice->status < UserInvoice::COMPLETED) {
      if(isset($data['payment_amount'])) {
        if($userInvoice->total - $userInvoice->total_paid <= $data['payment_amount']) {
          $totalPayment = $userInvoice->total - $userInvoice->total_paid;
        } else if($data['payment_amount'] * 100 / $userInvoice->total < $userInvoice->minimum_payment) {
          return $listener->response(400, 'Pembayaran minimum ' . $currency->getFormattedOnly(ceil($userInvoice->total * $userInvoice->minimum_payment / 100)));
        } else {
          $totalPayment = $data['payment_amount'];
        }
      }
      else {
        $totalPayment = $userInvoice->total - $userInvoice->total_paid;
      }
      $formatted = $this->marginFormatter->formatMargin($totalPayment);
      $formatted['merchant_price'] = 0;
      $formatted['sale_price'] = $formatted['base_price'];

      return $listener->response(200, array_merge($formatted, [
        'name' => $userInvoice->name
      ]));
    }

    return $listener->response(400, 'Tagihan tidak ditemukan atau sudah dibayarkan');
  }
}
