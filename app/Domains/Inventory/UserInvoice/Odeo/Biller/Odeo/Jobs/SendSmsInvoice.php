<?php

namespace Odeo\Domains\UserInvoice\Odeo\Biller\Odeo\Jobs;

use Odeo\Domains\Constant\SMS;
use Odeo\Domains\Vendor\SMS\Job\SendSms;
use Odeo\Jobs\Job;

class SendSmsInvoice extends Job  {

  protected $invoiceNumber;
  protected $invoiceName;
  protected $storeName;
  protected $invoiceTotal;

  public function __construct($billedTo, $invoiceName, $storeName, $invoiceNumber, $invoiceTotal) {
    parent::__construct();
    $this->billedTo = $billedTo;
    $this->invoiceName = $invoiceName;
    $this->storeName = $storeName;
    $this->invoiceNumber = $invoiceNumber;
    $this->invoiceTotal = $invoiceTotal;
  }

  public function handle() {
    dispatch(new SendSms([
      'sms_destination_number' => $this->billedTo,
      'sms_text' => 'Anda menerima tagihan ' . $this->invoiceNumber . ' ' . $this->invoiceName . ' dengan total ' . $this->invoiceTotal . ' dari ' . $this->storeName . '. Silahkan melakukan pembayaran di http://odeo.co.id',
      'sms_path' => SMS::NEXMO
    ]));

  }
}
