<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Repository;

use Carbon\Carbon;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Model\UserInvoicePayment;
use Odeo\Domains\Core\Repository;

class UserInvoicePaymentRepository extends Repository {

  public function __construct(UserInvoicePayment $userInvoicePayment) {
    $this->model = $userInvoicePayment;
  }

}
