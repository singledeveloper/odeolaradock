<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\UserInvoice;
use Carbon\Carbon;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository;

class Requester {

  public function completeOrder(PipelineListener $listener, $data) {
    $userInvoices = app()->make(AffiliateUserInvoiceRepository::class);
    $orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);

    $userInvoices->normalizeFilters($data);
    $invoice = $userInvoices->getDetail(true);

    if($invoice->status >= UserInvoice::COMPLETED) return $listener->response(400);

    $orderDetail = $orderDetails->findById($data['order_detail_id']);

    $invoice->total_paid += $orderDetail->sale_price;
    if($invoice->total_paid == $invoice->total) {
      $invoice->status = UserInvoice::COMPLETED;
      $invoice->paid_at = Carbon::now();
    } else {
      $invoice->status = UserInvoice::PARTIALLY_PAID;
    }

    $userInvoices->save($invoice);

//    $userInvoicePayment = $userInvoicePayments->getNew();
//    $userInvoicePayment->invoice_id = $invoice->id;
//    $userInvoicePayment->order_id = $orderDetail->order_id;
//    $userInvoicePayment->amount = $orderDetail->sale_price;
//    $userInvoicePayment->paid_at = Carbon::now();

//    $userInvoicePayments->save($userInvoicePayment);

//    $data = [
//      'order_id' => $orderDetail->order_id,
//      'invoice_number' => $invoice->invoice_number
//    ];
//
//    if ($invoice->store_id) {
//      $data[] = [
//        'recent_desc' => $orderDetail->order->sellerStore->name
//      ];
//    }

//    $cashInserter->add([
//      'user_id' => $invoice->user_id,
//      'trx_type' => TransactionType::INVOICE_PAYMENT,
//      'cash_type' => CashType::OCASH,
//      'amount' => $orderDetail->sale_price,
//      'data' => json_encode($data),
//    ]);
//

//    $cashInserter->run();

    if ($invoice->store_id) {
      $orderStores = app()->make(\Odeo\Domains\Order\Repository\OrderStoreRepository::class);
      $orderStore = $orderStores->getNew();
      $orderStore->order_id = $orderDetail->order_id;
      $orderStore->store_id = $invoice->store_id;
      $orderStore->role = 'hustler';

      $orderStores->save($orderStore);
    }

    return $listener->response(200);
  }

}
