<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo;

use Odeo\Domains\Account\UserKtpValidator;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository;

class UserInvoiceDashboardSelector {

  public function __construct() {
    $this->userInvoices = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Repository\UserInvoiceRepository::class);
    $this->userInvoices = app()->make(AffiliateUserInvoiceRepository::class);
    $this->userInvoiceSelector = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\UserInvoiceSelector::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
  }

  public function get(PipelineListener $listener, $data) {
    $lang = app('translator');
    $currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

    $storeOwner = $this->stores->findOwner($data['store_id']);

    if (!app()->make(UserKtpValidator::class)->isVerified($storeOwner->user_id)) {
      return $listener->response(200, [
        'description' => $lang->has('agent.description_unlock_agent_feature') ? trans('agent.description_unlock_agent_feature') : '',
        'allow' => false
      ]);
    }

    $this->userInvoices->normalizeFilters(array_merge($data, [
      'limit' => 10,
      'offset' => 0
    ]));
//    $invoiceStat = $this->userInvoices->getStat();
    $invoiceStat = $this->userInvoices->getUserInvoiceStat();

    $result['allow'] = true;
    $result['total_paid'] = $currency->formatPrice($invoiceStat->total_paid);
    $result['total_unpaid'] = $currency->formatPrice($invoiceStat->total_unpaid);
    $result['total_overdue'] = $currency->formatPrice($invoiceStat->total_overdue);

    $result['invoices'] = [];
    $invoices = $this->userInvoices->get();
    if($invoices) {
      foreach($invoices as $invoice) {
        $result['invoices'][] = $this->userInvoiceSelector->_transforms($invoice, $this->userInvoices);
      }
      $result['invoices'] = $this->userInvoiceSelector->_extends($result['invoices'], $this->userInvoices);
    }

    return $listener->response(200, $result);
  }
}
