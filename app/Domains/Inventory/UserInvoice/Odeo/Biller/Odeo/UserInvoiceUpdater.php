<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo;

use Carbon\Carbon;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Jobs\SendSmsInvoice;

class UserInvoiceUpdater {

  public function __construct() {
    $this->userInvoices = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Repository\UserInvoiceRepository::class);
    $this->userInvoices = app()->make(AffiliateUserInvoiceRepository::class);
    $this->invoiceParser = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Helper\UserInvoiceParser::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function create(PipelineListener $listener, $data) {

    $invoice = $this->userInvoices->getNew();

    $invoice->user_id = $data['auth']['user_id'];
    $invoice->store_id = $data['store_id'];
    $invoice->billed_user_telephone = $data['telephone'];
    $invoice->billed_user_name = $data['user_name'];
    $invoice->billed_user_id = $data['user_id'];
    $invoice->name = $data['name'];
    $invoice->due_date = $data['due_date'];
    $invoice->notes = $data['notes'] ?? '';
    $invoice->category_id = $data['category_id'] ?? 999;
    $invoice->items = $data['items'];
    $invoice->period = Carbon::now()->format("Y-m");
    $invoice->biller_id = UserInvoice::BILLER_ODEO;
    $invoice->invoice_number = UserInvoice::generateInvoiceNumber(UserInvoice::BILLER_ODEO);

    $total = 0;
    foreach($data['items'] as $item) {
      $total += $item['amount'] * $item['price'];
    }
    $invoice->subtotal = $total;
    $invoice->total = $total;

    if(isset($data['enable_partial_payment'])) {
      $invoice->enable_partial_payment = $data['enable_partial_payment'];
      $invoice->minimum_payment = $data['enable_partial_payment'] ? ($data['minimum_payment'] ?? UserInvoice::MINIMUM_PARTIAL_PAYMENT_PERCENTAGE) : 100;
    }

    $invoice->status = UserInvoice::OPENED;

    if ($this->userInvoices->save($invoice)) {
      $invoiceNumber = preg_replace("/(^\d{4}|\d{3})(\d{1})/", "$1-$2", $invoice->invoice_number);
      $total = $this->currency->formatPrice($invoice->total)['formatted_amount'];

      if(isset($data['user_id'])) {
        $notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
        $notification->setup($data['user_id'], NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
        $notification->new_invoice($invoice->name, $invoice->store->name, $invoiceNumber, $total);

        $listener->pushQueue($notification->queue());
      } else {
        if(app()->environment() == 'production') {
          // $listener->pushQueue(new SendSmsInvoice($invoice->billed_user_telephone, $invoice->name, $invoice->store->name, $invoiceNumber, $total));
        }
      }
      return $listener->response(200, [
        'user_invoice' => [
          'invoice_number' => $invoice->invoice_number,
          'total' => $invoice->total
        ],
        'amount' => $invoice->total,
        'reference_id' => $invoice->invoice_number
      ]);
    }
    return $listener->response(400);
  }

  public function void(PipelineListener $listener, $data) {
    $this->userInvoices->normalizeFilters($data);

    $invoice = $this->userInvoices->getDetail(true);

    if($invoice && $invoice->status < UserInvoice::COMPLETED) {
      $invoice->status = UserInvoice::VOID;
      $this->userInvoices->save($invoice);

      return $listener->response(200);
    }

    return $listener->response(400, 'Tagihan tidak ditemukan atau sudah dibayar');
  }
}
