<?php
namespace Odeo\Domains\Inventory\UserInvoice\Repository;

use Carbon\Carbon;
use Odeo\Domains\Inventory\UserInvoice\Model\OrderDetailUserInvoice;
use Odeo\Domains\Core\Repository;

class OrderDetailUserInvoiceRepository extends Repository {

  public function __construct(OrderDetailUserInvoice $OrderDetailUserInvoice) {
    $this->model = $OrderDetailUserInvoice;
  }
  
}
