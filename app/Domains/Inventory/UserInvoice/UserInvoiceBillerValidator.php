<?php
namespace Odeo\Domains\Inventory\UserInvoice;

use Odeo\Domains\Core\PipelineListener;

class UserInvoiceBillerValidator {

  public function __construct() {
    $this->userInvoiceBiller = app()->make(\Odeo\Domains\Inventory\UserInvoice\Repository\UserInvoiceBillerRepository::class);
  }

  public function guard(PipelineListener $listener, $data) {

    if (!($biller = $this->userInvoiceBiller->findByAttributes([
        'id' => $data['biller_id'],
        'user_id' => $data['auth']['user_id']
      ]
    ))) {
      return $listener->response(401, authDebug('Biller not registered', $data['auth']['user_id']));
    }

    return $listener->response(200, [
      'biller_user_id' => $biller->biller_user_id
    ]);
  }
  
}
