<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/21/16
 * Time: 8:25 PM
 */

namespace Odeo\Domains\Inventory;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class VendorSelector implements SelectorListener {


  private $vendors;

  public function __construct() {

    $this->vendors = app()->make(\Odeo\Domains\Inventory\Repository\VendorRepository::class);

  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getAllVendor(PipelineListener $listener) {

    $vendors = [];

    foreach ($this->vendors->getAll() as $item) {
      $vendors[] = $this->_transforms($item, $this->vendors);
    }

    if (count($vendors) > 0) {
      return $listener->response(200, [
        'vendors' => $vendors
      ]);
    }
    return $listener->response(204, null);
  }

  public function findVendor(PipelineListener $listener, $data) {


    $service = $this->vendors->find($data['vendor_id']);

    if (!$service) return $listener->response(204, null);

    return $listener->response(200, [
      'vendors' => $service
    ]);

  }
}