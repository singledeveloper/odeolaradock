<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 17/12/19
 * Time: 14.08
 */

namespace Odeo\Domains\Invoice;


use Box\Spout\Reader\ReaderFactory;
use Odeo\Domains\Constant\BulkInvoiceTemplate;
use Odeo\Domains\Core\PipelineListener;

class BulkInvoiceParser {

  public function parse(PipelineListener $listener, $data) {
    $file = $data['file'];

    $reader = ReaderFactory::create($file->getClientOriginalExtension());
    $reader->open($file->path());

    $templateSheetFound = false;
    foreach ($reader->getSheetIterator() as $sheet) {
      switch (strtolower($sheet->getName())) {
        case BulkInvoiceTemplate::SHEET_INFORMATION:
          if (!$this->correctVersion($sheet)) {
            return $listener->response(400, trans('invoice/error.template_outdated'), [
              'version' => BulkInvoiceTemplate::VERSION
            ]);
          }
          break;
        case BulkInvoiceTemplate::SHEET_TEMPLATE:
          try {
            $templateSheetFound = true;
            $data = $this->parseTemplateSheet($sheet);
          } catch (\Exception $e) {
            return $listener->response(400, trans('invoice/error.template_input_invalid'));
          }
          break;
      }
    }

    if (!$templateSheetFound) {
      return $listener->response(400, trans('invoice/error.template_outdated', [
        'version' => BulkInvoiceTemplate::VERSION]));
    }

    return $listener->response(200, [
      'rows' => $data
    ]);
  }

  private function correctVersion($sheet) {
    foreach ($sheet->getRowIterator() as $row) {
      return strtolower($row[0]) == strtolower(BulkInvoiceTemplate::VERSION);
    }
    return false;
  }

  private function parseTemplateSheet($sheet) {
    $data = [];
    $fields = [
      'billed_user_id',
      'invoice_name',
      'item',
      'amount',
      'price',
      'billed_user_name',
      'billed_user_email',
      'billed_user_telephone',
      'billed_user_address'
    ];

    $firstRow = true;
    foreach ($sheet->getRowIterator() as $key => $row) {
      if ($firstRow) {
        $firstRow = false;
        continue;
      }
      $row = array_map('trim', $row);

      if (implode('', $row) == '') {
        continue;
      }

      $item = array_combine($fields, $row);

      $billedUserId = $item['billed_user_id'] == '' ? time() . ($key - 1) : $item['billed_user_id'];

      if (!isset($data[$billedUserId])) {
        $data[$billedUserId] = [
          'billed_user_id' => $billedUserId,
          'invoice_name' => $item['invoice_name'],
          'items' => [
            [
              'name' => $item['item'],
              'amount' => $item['amount'],
              'price' => $item['price'],
            ]
          ],
          'billed_user_name' => $item['billed_user_name'],
          'billed_user_email' => $item['billed_user_email'],
          'billed_user_telephone' => $item['billed_user_telephone'],
          'billed_user_address' => $item['billed_user_address']
        ];
      } else {
        $data[$billedUserId]['items'][] = [
          'name' => $item['item'],
          'amount' => $item['amount'],
          'price' => $item['price'],
        ];
      }
    }

    return array_values($data);
  }

}