<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 17/12/19
 * Time: 16.37
 */

namespace Odeo\Domains\Invoice;


use Carbon\Carbon;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceRepository;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceUserRepository;
use Odeo\Domains\VirtualAccount\VirtualAccountManager;

class BulkInvoiceRequester extends VirtualAccountManager {

  private $userInvoiceUserRepo, $userInvoiceRepo;

  function __construct() {
    parent::__construct();
    $this->userInvoiceRepo = app()->make(AffiliateUserInvoiceRepository::class);
    $this->userInvoiceUserRepo = app()->make(AffiliateUserInvoiceUserRepository::class);
  }

  public function request(PipelineListener $listener, $data) {
    $userId = getUserId();
    $rows = $data['rows'];

    $newUserIds = $this->getNewUserIds($rows);
    $newInvoiceUsers = [];
    $updateInvoiceUsers = [];
    $newVaUsers = [];
    $updateVaUsers = [];
    $newInvoices = [];
    $updateinvoices = [];
    $now = Carbon::now();
    $invoicePeriod = $now->format('Y-m');

    foreach ($rows as $row) {
      $invoiceNo = UserInvoice::generateInvoiceNumber(UserInvoice::BILLER_ODEO);
      $billedUserId = $row['billed_user_id'];
      $invoiceUser = $this->userInvoiceUserRepo->findUserByCreatedBy(UserInvoice::BILLER_ODEO, $row['billed_user_id'], $userId);

      if (in_array($row['billed_user_id'], $newUserIds) && !$invoiceUser) {
        $newInvoiceUsers[] = [
          'biller_id' => VirtualAccount::BILLER_ODEO,
          'user_id' => $billedUserId,
          'name' => $row['billed_user_name'],
          'email' => $row['billed_user_email'],
          'telephone' => $row['billed_user_telephone'],
          'address' => $row['billed_user_address'],
          'created_by' => $userId,
          'created_at' => $now,
          'updated_at' => $now
        ];
        $newVaUsers[] = $this->getNewVaUser($billedUserId, $userId, $invoiceNo, $row['invoice_name'], $now);
      } else {
        $updateInvoiceUsers[] = [
          'id' => $invoiceUser->id,
          'user_id' => $billedUserId,
          'name' => $row['billed_user_name'],
          'email' => $row['billed_user_email'],
          'telephone' => $row['billed_user_telephone'],
          'address' => $row['billed_user_address'],
          'created_by' => $userId,
          'updated_at' => $now
        ];
        $updateVaUsers[] = $this->updateVaUser($billedUserId, $userId, $invoiceNo, $row['invoice_name'], $now);
      }

      $subtotal = $this->calculateSubtotal($row['items']);

      if (!$invoice = $this->userInvoiceRepo->findOpenInvoiceUserInvoice($billedUserId, $userId)) {
        $invoice = $this->userInvoiceRepo->getNew();
        $invoice->billed_user_id = $billedUserId;
        $invoice->user_id = $userId;
        $invoice->biller_id = UserInvoice::BILLER_ODEO;
        $invoice->created_by = $userId;
        $newInvoices[] = [
          'billed_user_id' => $billedUserId,
          'user_id' => $userId,
          'biller_id' => UserInvoice::BILLER_ODEO,
          'created_by' => $userId,
          'period' => $invoicePeriod,
          'invoice_number' => $invoiceNo,
          'name' => $row['invoice_name'],
          'items' => json_encode($row['items']),
          'subtotal' => $subtotal,
          'total' => $subtotal,
          'charges' => 0,
          'due_date' => $row['invoice_due_date'] ?? null,
          'notes' => $row['invoice_notes'] ?? '',
          'status' => UserInvoice::OPENED,
          'billed_user_name' => $row['billed_user_name'],
          'billed_user_email' => $row['billed_user_email'],
          'billed_user_telephone' => $row['billed_user_telephone'],
          'created_at' => $now,
          'updated_at' => $now
        ];
      } else {
        $updateinvoices[] = [
          'id' => $invoice->id,
          'period' => $invoicePeriod,
          'invoice_number' => $invoiceNo,
          'name' => $row['invoice_name'],
          'items' => json_encode($row['items']),
          'subtotal' => $subtotal,
          'total' => $subtotal,
          'charges' => 0,
          'due_date' => $row['invoice_due_date'] ?? null,
          'notes' => $row['invoice_notes'] ?? '',
          'status' => UserInvoice::OPENED,
          'billed_user_name' => $row['billed_user_name'],
          'billed_user_email' => $row['billed_user_email'],
          'billed_user_telephone' => $row['billed_user_telephone'],
          'updated_at' => $now
        ];
      }
    }

    if (count($newInvoiceUsers)) {
      $this->userInvoiceUserRepo->saveBulk($newInvoiceUsers);
    }

    if (count($updateInvoiceUsers)) {
      $this->userInvoiceUserRepo->updateBulk($updateInvoiceUsers);
    }

    if (count($newVaUsers)) {
      $this->userVirtualAccounts->saveBulk($newVaUsers);
    }

    if (count($updateVaUsers)) {
      $this->userVirtualAccounts->updateBulk($updateVaUsers);
    }

    if (count($newInvoices)) {
      $this->userInvoiceRepo->saveBulk($newInvoices);
    }

    if (count($updateinvoices)) {
      $this->userInvoiceRepo->updateBulk($updateinvoices);
    }

    return $listener->response(200);
  }

  private function getNewUserIds($rows) {
    $bulkUserIds = collect($rows)->pluck('billed_user_id');
    $existingUserIds = $this->userInvoiceUserRepo->getAllByCreatedBy(VirtualAccount::BILLER_ODEO, getUserId())->pluck('user_id');
    return array_values($bulkUserIds->diff($existingUserIds)->toArray());
  }

  private function calculateSubtotal($items) {
    $subtotal = 0;
    foreach ($items as $item) {
      $subtotal += $item['amount'] * $item['price'];
    }
    return $subtotal;
  }

}