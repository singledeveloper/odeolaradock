<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 27/12/19
 * Time: 14.19
 */

namespace Odeo\Domains\Invoice;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Invoice\Repository\BusinessInvoiceUserRepository;
use Odeo\Domains\Invoice\Repository\BusinessInvoiceUserSettingRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayChannelRepository;

class BusinessInvoiceUserRequester {

  function __construct() {
    $this->invoiceUsers = app()->make(BusinessInvoiceUserRepository::class);
    $this->invoiceUserSettings = app()->make(BusinessInvoiceUserSettingRepository::class);
    $this->pgChannels = app()->make(PaymentGatewayChannelRepository::class);
  }

  public function request(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];
    if ($invoiceUser = $this->invoiceUsers->findByUserId($userId)) {
      return $listener->response(400, trans('invoice/error.invoice_user_exists'));
    }

    $invoiceUser = $this->invoiceUsers->getNew();
    $invoiceUser->user_id = $userId;
    $invoiceUser->charge_to_customer = false;
    $this->invoiceUsers->save($invoiceUser);

    return $listener->response(200, [
      'business_invoice_user_id' => $invoiceUser->id,
      'charge_to_customer' => $invoiceUser->charge_to_customer,
    ]);
  }

  public function toggleChargeToCustomer(PipelineListener $listener, $data) {
    if (!$invoiceUser = $this->invoiceUsers->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('invoice/error.invoice_user_not_found'));
    }

    $invoiceUser->charge_to_customer = !$invoiceUser->charge_to_customer;
    $this->invoiceUsers->save($invoiceUser);

    return $listener->response(200, [
      'business_invoice_user_id' => $invoiceUser->id,
      'charge_to_customer' => $invoiceUser->charge_to_customer
    ]);
  }

}