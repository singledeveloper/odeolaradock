<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 16/12/19
 * Time: 14.57
 */

namespace Odeo\Domains\Invoice\Helper;


use Box\Spout\Writer\WriterFactory;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceUserRepository;

class InvoiceExporter {

  protected $invoiceUsers;

  function __construct() {
    $this->invoiceUsers = app()->make(AffiliateUserInvoiceUserRepository::class);
  }

  public function exportUsers($billerId, $createdBy, $fileType = 'csv') {
    $invoiceUsers = $this->invoiceUsers->getAllByCreatedBy($billerId, $createdBy);
    $writer = WriterFactory::create($fileType);

    ob_start();

    $writer->openToFile('php://output');
    $writer->addRow([
      'user_id',
      'name',
      'email',
      'telephone',
      'address'
    ]);

    foreach ($invoiceUsers as $user) {
      $writer->addRow([
        'user_id' => $user->user_id,
        'name' => $user->name,
        'email' => $user->email,
        'telephone' => $user->telephone,
        'address' => $user->address
      ]);
    }

    $writer->close();

    return ob_get_clean();

  }

}