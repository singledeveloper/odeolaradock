<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 22/11/19
 * Time: 14.26
 */

namespace Odeo\Domains\Invoice;


use Carbon\Carbon;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\Settlement;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Helper\UserInvoiceParser;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceRepository;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceUserRepository;
use Odeo\Domains\Transaction\Helper\Currency;

class InvoiceSelector implements SelectorListener {

  function __construct() {
    $this->userInvoices = app()->make(AffiliateUserInvoiceRepository::class);
    $this->currency = app()->make(Currency::class);
    $this->invoiceParser = app()->make(UserInvoiceParser::class);
    $this->invoiceUsers = app()->make(AffiliateUserInvoiceUserRepository::class);
  }

  public function getAllByUser(PipelineListener $listener, $data) {
    $this->userInvoices->normalizeFilters($data);

    if ($invoices = $this->userInvoices->getAllByUser(UserInvoice::BILLER_ODEO, $data['auth']['user_id'])) {
      $res = [];
      foreach ($invoices as $invoice) {
        $res[] = $this->_transforms($invoice, $this->userInvoices);
      }

      if (count($res)) {
        return $listener->response(200, array_merge(
          ['invoices' => $this->_extends($res, $this->userInvoices)],
          $this->userInvoices->getPagination()
        ));
      }
    }

    return $listener->response(204, ['invoices' => []]);
  }

  public function getInvoiceByBilledUserId(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];

    if (!isset($data['billed_user_id'])) {
      $data['billed_user_id'] = $this->invoiceUsers->findByIdAndCreatedBy($data['id'], $userId)->id;
    }

    if (!$invoices = $this->userInvoices->getByBilledUserId($userId, $data['billed_user_id'])) {

      $res = [];

      foreach ($invoices as $invoice) {
        $res[] = $this->_transforms($invoice, $this->userInvoices);
      }

      if (count($res)) {
        return $listener->response(200, array_merge(
          ['invoices' => $this->_extends($res, $this->userInvoices)],
          $this->userInvoices->getPagination()
        ));
      }
    }

    return $listener->response(204, [
      'user_invoices' => []
    ]);
  }

  public function findInvoiceById(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];
    if ($invoice = $this->userInvoices->findByUserIdAndInvoiceId($userId, $data['id'])) {
      $user = app()->make(UserRepository::class)->findById($userId);

      $userData = [
        'user_name' => $user->name,
        'user_telephone' => $user->telephone,
        'user_email' => $user->email,
      ];

      $res = $this->_transforms($invoice, $this->userInvoices);
      return $listener->response(200, array_merge($userData, $res));
    }

    return $listener->response(204, []);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $data = [];
    $data['id'] = $item->id;
    $data['invoice_number'] = $item->invoice_number;
    $data['billed_user_id'] = $item->billed_user_id;
    $data['period'] = $item->period;
    $data['invoice_name'] = $item->name;
    $data['items'] = $item->items;
    $data['subtotal'] = $this->currency->formatPrice($item->subtotal);
    $data['charges'] = $this->currency->formatPrice($item->charges);
    $data['total'] = $this->currency->formatPrice($item->total);
    $data['total_paid'] = $this->currency->formatPrice($item->total_paid);
    $data['late_charge'] = $this->currency->formatPrice($item->late_charge);
    $data['late_charge_interval'] = $item->late_charge_interval ?? 0;
    $data['enable_late_charge'] = $item->enable_late_charge;
    $data['enable_partial_payment'] = $item->enable_partial_payment;
    $data['minimum_payment'] = $this->currency->formatPrice($item->minimum_payment);
    $data['status'] = $item->status;
    $data['status_message'] = $this->invoiceParser->getStatusMessage($item);
    if ($item->due_date) {
      $data['due_date'] = $item->due_date->format("Y-m-d");
    } else {
      $data['due_date'] = null;
    }
    $data['created_at'] = $item->created_at->format("Y-m-d H:i:s");
    $data['updated_at'] = $item->updated_at->format("Y-m-d H:i:s");

    if ($item->paid_at) {
      $data['paid_at'] = $item->paid_at->format("Y-m-d H:i:s");
    } else {
      $data['paid_at'] = null;
    }

    $data['billed_user_name'] = $item->billed_user_name;
    $data['billed_user_email'] = $item->billed_user_email;
    $data['billed_user_telephone'] = $item->billed_user_telephone;
    $data['biller_fee'] = $this->currency->formatPrice($item->biller_fee);
    $data['payment_gateway_cost'] = $this->currency->formatPrice($item->payment_gateway_cost);
    $data['notes'] = $item->notes;

    if ($item->settlement_at) {
      $data['settlement_at'] = Carbon::parse($item->settlement_at)->format("Y-m-d H:i:s");
      $data['settlement_status'] = $item->settlement_status;
      $data['settlement_status_message'] = Settlement::getSettlementStatusMessage($item->status, $item->settlement_status);
    } else {
      $data['settlement_at'] = null;
      $data['settlement_status'] = null;
      $data['settlement_status_message'] = null;
    }

    return $data;
  }
}
