<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 22/11/19
 * Time: 14.25
 */

namespace Odeo\Domains\Invoice;


use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Invoice\Jobs\SendInvoiceUsersList;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceUserRepository;
use Odeo\Domains\Transaction\Helper\ExportDownloader;

class InvoiceUserSelector implements SelectorListener {

  function __construct() {
    $this->invoiceUsers = app()->make(AffiliateUserInvoiceUserRepository::class);
  }

  public function getAllByUser(PipelineListener $listener, $data) {
    $this->invoiceUsers->normalizeFilters($data);
    if ($invoiceUsers = $this->invoiceUsers->getAllByCreatedBy(UserInvoice::BILLER_ODEO, $data['auth']['user_id'])) {
      $res = [];
      foreach ($invoiceUsers as $user) {
        $res[] = $this->_transforms($user, $this->invoiceUsers);
      }

      if (count($res)) {
        return $listener->response(200, array_merge(
          ['invoice_users' => $this->_extends($res, $this->invoiceUsers)],
          $this->invoiceUsers->getPagination()
        ));
      }
    }

    return $listener->response(204, [
      'invoice_users' => []
    ]);
  }

  public function findInvoiceUser(PipelineListener $listener, $data) {
    if (is_numeric($data['id']) && $invoiceUser = $this->invoiceUsers->findByIdAndCreatedBy($data['id'], $data['auth']['user_id'])) {
      $res = $this->_transforms($invoiceUser, $this->invoiceUsers);
      return $listener->response(200, $res);
    }

    return $listener->response(400, trans('invoice/error.billed_user_not_found'));
  }

  public function exportInvoiceUsers(PipelineListener $listener, $data) {
    $user = app()->make(UserRepository::class)->findById(getUserId());
    if (!$user->email) {
      return $listener->response(400, trans('user.email_not_set'));
    }

    if (!$user->is_email_verified) {
      return $listener->response(400 ,trans('user.email_not_verified'));
    }

    $exportId = app()->make(ExportDownloader::class)->create($data);
    $listener->pushQueue(new SendInvoiceUsersList($user->id, $user->email, $exportId, $data['file_type'] ?? 'csv'));

    return $listener->response(200, [
      'export_id' => $exportId
    ]);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $res = [];
    $res['id'] = $item->id;
    $res['billed_user_id'] = $item->user_id;
    $res['billed_user_name'] = $item->name;
    $res['billed_user_email'] = $item->email;
    $res['billed_user_telephone'] = $item->telephone;
    $res['billed_user_address'] = $item->address;
    $res['created_at'] = $item->created_at->format("Y-m-d H:i:s");
    $res['updated_at'] = $item->updated_at->format("Y-m-d H:i:s");
    return $res;
  }
}