<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 23/12/19
 * Time: 15.24
 */

namespace Odeo\Domains\Invoice\Jobs;


use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\Settlement;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceRepository;
use Odeo\Domains\Transaction\Helper\CashInserter;
use Odeo\Jobs\Job;

class SettleInvoicePayment extends Job {

  private $invoicePayment;
  public function __construct($invoicePayment) {
    parent::__construct();
    $this->invoicePayment = $invoicePayment;
  }

  public function handle() {
    $userInvoices = app()->make(AffiliateUserInvoiceRepository::class);
    $cashInserter = app()->make(CashInserter::class);

    $this->invoicePayment->settlement_status = Settlement::ON_PROGRESS;
    $this->invoicePayment->save();

    $order = $this->invoicePayment->order;
    $invoiceNames = explode(',', $this->invoicePayment->description);
    $invoices = $userInvoices->getByInvoiceNameAndCustomerName($invoiceNames, trim($this->invoicePayment->customer_name));
    $invoiceNumbers = $invoices->pluck('invoice_number')->toArray();

    $data = [
      'order_id' => $order->id,
      'invoice_number' => $invoiceNumbers
    ];

    if ($invoices[0]->store_id) {
      $data['recent_desc'] = $order->sellerStore->name;
    }

    $cashInserter->add([
      'user_id' => $order->user_id,
      'trx_type' => TransactionType::INVOICE_PAYMENT,
      'cash_type' => CashType::OCASH,
      'amount' => $order->total,
      'data' => json_encode($data),
    ]);

    $cashInserter->add([
      'user_id' => $order->user_id,
      'trx_type' => TransactionType::TRANSACTION_FEE,
      'cash_type' => CashType::OCASH,
      'amount' => -$invoices[0]->biller_fee,
      'data' => json_encode($data),
    ]);

    if ($invoices[0]->mdr > 0) {
      $cashInserter->add([
        'user_id' => $order->user_id,
        'trx_type' => TransactionType::MDR,
        'cash_type' => CashType::OCASH,
        'amount' => -$invoices[0]->mdr,
        'data' => json_encode([
          'order_id' => $order->id,
          'invoice_number' => $invoiceNumbers
        ])
      ]);
    }

    $cashInserter->run();

    $this->invoicePayment->settlement_status = Settlement::SUCCESS;
    $this->invoicePayment->save();
  }

}