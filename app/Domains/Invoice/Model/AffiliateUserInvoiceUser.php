<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 22/11/19
 * Time: 15.40
 */

namespace Odeo\Domains\Invoice\Model;


use Odeo\Domains\Core\Entity;

class AffiliateUserInvoiceUser extends Entity {

  protected $dates = ['created_at', 'updated_at'];

}