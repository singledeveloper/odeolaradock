<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 22/11/19
 * Time: 15.39
 */

namespace Odeo\Domains\Invoice\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Invoice\Model\AffiliateUserInvoiceUser;

class AffiliateUserInvoiceUserRepository extends Repository {

  function __construct(AffiliateUserInvoiceUser $invoiceUser) {
    $this->model = $invoiceUser;
  }

  public function getAllByCreatedBy($billerId, $createdBy) {
    $filters = $this->getFilters();
    $query = $this->getCloneModel()
      ->where('biller_id', $billerId)
      ->where('created_by',$createdBy);

    if (isset($filters['billed_user_id']) && $filters['billed_user_id'] != '') {
      $query = $query->where('user_id', $filters['billed_user_id']);
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function findUserByCreatedBy($billerId, $userId, $createdBy) {
    return $this->getCloneModel()
      ->where('biller_id', $billerId)
      ->where('user_id', $userId)
      ->where('created_by', $createdBy)
      ->first();
  }

  public function findByIdAndCreatedBy($id, $createdBy) {
    return $this->getCloneModel()
      ->whereId($id)
      ->where('created_by', $createdBy)
      ->first();
  }
}