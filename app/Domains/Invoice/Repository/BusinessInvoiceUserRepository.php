<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 26/12/19
 * Time: 17.45
 */

namespace Odeo\Domains\Invoice\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Invoice\Model\BusinessInvoiceUser;

class BusinessInvoiceUserRepository extends Repository {

  function __construct(BusinessInvoiceUser $invoiceUser) {
    $this->model = $invoiceUser;
  }

  function findByUserId($userId) {
    return $this->model->where('user_id', $userId)->first();
  }

}