<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 27/12/19
 * Time: 14.22
 */

namespace Odeo\Domains\Invoice\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Invoice\Model\BusinessInvoiceUserSetting;

class BusinessInvoiceUserSettingRepository extends Repository {

  function __construct(BusinessInvoiceUserSetting $userSetting) {
    $this->model = $userSetting;
  }

  function findByBusinessInvoiceUserIdAndChannelId($invoiceUserId, $channelId) {
    return $this->model
      ->where('business_invoice_user_id', $invoiceUserId)
      ->where('payment_gateway_channel_id', $channelId)
      ->first();
  }

  function findByBusinessInvoiceUserIdAndVaCode($invoiceUserId, $vaCode) {
    return $this->model
      ->join('payment_gateway_channels', 'business_invoice_user_settings.payment_gateway_channel_id', '=', 'payment_gateway_channels.id')
      ->join('virtual_account_vendors', 'payment_gateway_channels.va_vendor_id', '=', 'virtual_account_vendors.id')
      ->where('business_invoice_user_id', $invoiceUserId)
      ->whereRaw("left('$vaCode', length(virtual_account_vendors.prefix)) = virtual_account_vendors.prefix")
      ->first();
  }

  function getActiveVendorChannel($userId) {
    return $this->model
      ->join('business_invoice_users', 'business_invoice_users.id', '=', 'business_invoice_user_settings.business_invoice_user_id')
      ->join('payment_gateway_channels', 'payment_gateway_channels.id', '=', 'business_invoice_user_settings.payment_gateway_channel_id')
      ->join('virtual_account_vendors', 'virtual_account_vendors.id', '=', 'payment_gateway_channels.va_vendor_id')
      ->where('business_invoice_user_settings.active', true)
      ->where('virtual_account_vendors.is_active', true)
      ->where('business_invoice_users.user_id', $userId)
      ->select('virtual_account_vendors.*')
      ->get();
  }

}