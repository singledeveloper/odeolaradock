<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 23/12/19
 * Time: 16.06
 */

namespace Odeo\Domains\Invoice\Scheduler;


use Odeo\Domains\Invoice\Jobs\SettleInvoicePayment;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayInvoicePaymentRepository;

class SettleInvoicePaymentScheduler {

  private $invoicePayments;

  public function init() {
    $this->invoicePayments = app()->make(PaymentGatewayInvoicePaymentRepository::class);
  }

  public function run() {
    $this->init();

    $invoicePayments = $this->invoicePayments->getUnsettledPayment();

    foreach ($invoicePayments as $payment) {
      dispatch(new SettleInvoicePayment($payment));
    }
  }

}