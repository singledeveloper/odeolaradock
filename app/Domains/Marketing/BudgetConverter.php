<?php

namespace Odeo\Domains\Marketing;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\MarketingConfig;
use Odeo\Domains\Constant\NetworkConfig;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Carbon\Carbon;

class BudgetConverter {

  public function __construct() {
    $this->ads = app()->make(\Odeo\Domains\Marketing\Repository\AdsRepository::class);
    $this->adsClick = app()->make(\Odeo\Domains\Marketing\Repository\AdsClickRepository::class);
    $this->storeBudgets = app()->make(\Odeo\Domains\Marketing\Repository\StoreMarketingBudgetRepository::class);
    $this->multiplier = app()->make(\Odeo\Domains\Marketing\Repository\MarginMultiplierRepository::class);
    $this->budgetInserter = app()->make(\Odeo\Domains\Marketing\Helper\BudgetInserter::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->treeParentManager = app()->make(\Odeo\Domains\Network\Helper\ParentManager::class);
    $this->order = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
  }

  public function rush(PipelineListener $listener, $data) {
    $rush_bonus = [
      'me' => 0,
      'parent' => 0,
      'grandparent' => 0,
      'additionals' => [],
    ];

    return $listener->response(200, ['rush_bonus_inserted' => $rush_bonus]);

    // deprecated
    if(Carbon::now()->lt(Carbon::createFromTime(MarketingConfig::MIN_RUSH_HOUR, 0, 0)) &&
      Carbon::now()->gt(Carbon::createFromTime(MarketingConfig::MAX_RUSH_HOUR, 0, 0)))
      return $listener->response(200, ['rush_bonus_inserted' => $rush_bonus]);
    if(isset($data['rush_bonus']) && isset($data['tree_data'])) {
      $order = isset($data["order_id"]) ? $this->order->findById($data["order_id"]) : false;
      if ($order && $order->actual_store_id != NULL) $storeId = $order->actual_store_id;

      if(isset($storeId)) {
        $exact = $data['tree_data'];

        $storeName = $exact['me'] ? $exact['me']['store_name'] : "";
        $trxData = [
          'order_id' => $order->id,
          'recent_desc' => $storeName
        ];
        if(isset($data['order']['settlement_at'])) $trxData['settlement_at'] = $data['order']['settlement_at'];

        if($exact['me'] && isset($data['rush_bonus']['me'])) {
          if(isset($exact['me']['is_community']) && $exact['me']['is_community']) $trxData['is_community'] = true;
          $amount = $this->budgetInserter->convertBudget(
            $exact['me']['user_id'], $exact['me']['store_id'],
            TransactionType::RUSH_BONUS, [CashType::ORUSH, CashType::OADS],
            $data['rush_bonus']['me'], $trxData, $order->id);
          if($amount) {
            $rush_bonus['me'] = $amount;
            $this->notification->setup($exact['me']['user_id'], NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
            $this->notification->order_bonus($amount, $storeName, TransactionType::RUSH_BONUS);
          }
        }

        if($exact['parent'] && isset($data['rush_bonus']['parent'])) {
          $trxData['recent_desc'] = $exact['parent']['store_name'];
          if(isset($exact['parent']['is_community']) && $exact['parent']['is_community']) $trxData['is_community'] = true;
          $trxData['from'] = $storeName;
          $trxData['role'] = 'mentor';
          $amount = $this->budgetInserter->convertBudget(
            $exact['parent']['user_id'], $exact['parent']['store_id'],
            TransactionType::MENTOR_RUSH_BONUS, [CashType::ORUSH, CashType::OADS],
            $data['rush_bonus']['parent'], $trxData, $order->id);
          if($amount) {
            $rush_bonus['parent'] = $amount;
            $this->notification->setup($exact['parent']['user_id'], NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
            $this->notification->order_bonus($amount, $storeName, TransactionType::RUSH_BONUS);
          }
        }

        if($exact['grandparent'] && isset($data['rush_bonus']['grandparent'])) {
          $trxData['recent_desc'] = $exact['grandparent']['store_name'];
          if(isset($exact['grandparent']['is_community']) && $exact['grandparent']['is_community']) $trxData['is_community'] = true;
          $trxData['from'] = $storeName;
          $trxData['role'] = 'leader';
          $amount = $this->budgetInserter->convertBudget(
            $exact['grandparent']['user_id'], $exact['grandparent']['store_id'],
            TransactionType::LEADER_RUSH_BONUS, [CashType::ORUSH, CashType::OADS],
            $data['rush_bonus']['grandparent'], $trxData, $order->id);
          if($amount) {
            $rush_bonus['grandparent'] = $amount;
            $this->notification->setup($exact['grandparent']['user_id'], NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
            $this->notification->order_bonus($amount, $storeName, TransactionType::RUSH_BONUS);
          }
        }

        if($exact['additionals'] && isset($data['rush_bonus']['default'])) {
          foreach($exact['additionals'] as $additionals) {
            $trxData['recent_desc'] = $additionals['store_name'];
            if(isset($additionals['is_community']) && $additionals['is_community']) $trxData['is_community'] = true;
            unset($trxData['from']);
            unset($trxData['role']);
            $amount = $this->budgetInserter->convertBudget(
              $additionals['user_id'], $additionals['store_id'],
              TransactionType::RUSH_BONUS, [CashType::ORUSH, CashType::OADS],
              $data['rush_bonus']['default'], $trxData, $order->id);
            if($amount) {
              $rush_bonus['additionals'][$additionals['store_id']] = $amount;
              $this->notification->setup($additionals['user_id'], NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
              $this->notification->order_bonus($amount, $storeName, TransactionType::RUSH_BONUS);
            }
          }
        }

        if(!empty($this->notification->get())) {
          $this->notification->pushOnly();
          // $listener->pushQueue($this->notification->queue());
        }
      }
    }
    return $listener->response(200, ['rush_bonus_inserted' => $rush_bonus]);
  }

  public function ads(PipelineListener $listener, $data) {
    $ads = $this->ads->findById($data['ads_id']);
    $storeBudget = $this->storeBudgets->findByAttributes('store_id', $data['store_id']);

    if(!$ads || !$storeBudget) return $listener->response(204);

    $adsClick = $this->adsClick->getNew();
    $adsClick->ads_id = $data['ads_id'];
    $adsClick->store_id = $data['store_id'];
    $adsClick->ip = $data['ip'];
    $adsClick->user_agent = $data['user_agent'];
    $this->adsClick->save($adsClick);

    $adsBonus = round(mt_rand($storeBudget->min_ads, $storeBudget->max_ads) * MarketingConfig::OADS_MULTIPLIER);
    $exact = $this->treeParentManager->get_exact_parents($data['store_id']);
    $storeName = $exact->me ? $exact->me->store_name : "";

    $parentGetBonus = false;
    if($exact->me) {
      $amount = $this->budgetInserter->convertBudget(
        $exact->me->user_id, $exact->me->store_id,
        TransactionType::ADS_CLICK, CashType::OADS, $adsBonus, [
          'ads_id' => $data['ads_id'],
          'recent_desc' => $storeName
        ]);
      if($amount) {
        $parentGetBonus = true;
        $this->notification->setup($exact->me->user_id, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
        $this->notification->order_bonus($amount, $storeName, TransactionType::ADS_CLICK);
      }
    }

    if($parentGetBonus && $exact->parent) {
      $amount = $this->budgetInserter->convertBudget(
        $exact->parent->user_id, $exact->parent->store_id,
        TransactionType::MENTOR_ADS_CLICK, CashType::OADS,
        NetworkConfig::PARENT_BONUS * $adsBonus, [
          'ads_id' => $data['ads_id'],
          'recent_desc' => $storeName
        ]);
      if($amount){
        $this->notification->setup($exact->parent->user_id, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
        $this->notification->order_bonus($amount, $storeName, TransactionType::MENTOR_ADS_CLICK);
      }
    }

    if($parentGetBonus && $exact->grandparent) {
      $amount = $this->budgetInserter->convertBudget(
        $exact->grandparent->user_id, $exact->grandparent->store_id,
        TransactionType::LEADER_ADS_CLICK, CashType::OADS,
        NetworkConfig::GRANDPARENT_BONUS * $adsBonus, [
          'ads_id' => $data['ads_id'],
          'recent_desc' => $storeName
        ]);
      if($amount) {
        $this->notification->setup($exact->grandparent->user_id, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
        $this->notification->order_bonus($amount, $storeName, TransactionType::LEADER_ADS_CLICK);
      }
    }

    if(!empty($this->notification->get())) $listener->pushQueue($this->notification->queue());

    return $listener->response(200);
  }
}
