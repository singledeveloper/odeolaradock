<?php

namespace Odeo\Domains\Marketing;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\MarketingConfig;
use Carbon\Carbon;

class BudgetUpdater {

  public function __construct() {
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->storeBudgets = app()->make(\Odeo\Domains\Marketing\Repository\StoreMarketingBudgetRepository::class);
    $this->marketingBudget = app()->make(\Odeo\Domains\Marketing\Repository\MarketingBudgetRepository::class);
    $this->incomeOutcome = app()->make(\Odeo\Domains\Marketing\Repository\IncomeOutcomeRepository::class);
    $this->budgetInserter = app()->make(\Odeo\Domains\Marketing\Helper\BudgetInserter::class);
  }

  public function recalculateBudget() {
    $stores = $this->store->getAllActiveStoreWithoutFreeStore();
    $cashTransactions = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);

    foreach ($stores as $store) {
      $storeBudget = $this->storeBudgets->findByAttributes('store_id', $store->id);
      $startDate = $store->renewal_at->subDays(180);

      $currentMarketingBudget = $cashTransactions->getCloneModel()
        ->where('trx_type', 'marketing_budget')
        ->where('store_id', $store->id)
        ->whereDate('created_at', '>=', $startDate)
        ->sum('amount');
      $storeBudget->total_budget = Carbon::today()->diffInDays($store->renewal_at);
      $storeBudget->budget_allocation = $storeBudget->budget_allocation - (int) $currentMarketingBudget;

      $this->storeBudgets->save($storeBudget);
    }
  }

  public function updateBudget(PipelineListener $listener, $data) {
    $store = $this->store->findById($data['store_id']);

    $owner = $store->owner->first();
    $planSubscriptionMonth = $store->plan->minimal_months;
    $planPrice = $store->plan->cost_per_month * $planSubscriptionMonth;

    $storeBudget = $this->storeBudgets->findByAttributes('store_id', $store->id);

    $isNew = false;
    if (!$storeBudget) {
      $isNew = true;
      $storeBudget = $this->storeBudgets->getNew();
      $storeBudget->store_id = $store->id;
      $storeBudget->variant = mt_rand(1, 12);
    }

    $isRenewal = !isset($data['previous_plan_id']) && !Carbon::parse($data['established_at'])->isToday();

    $storeBudget->total_budget = $planSubscriptionMonth * 30;
    $storeBudget->min_ads = round(pow($planPrice, ((2 + pow($planPrice, 1.2) / pow(780000, 1.2) * 780000 / $planPrice) / 10)));
    $storeBudget->max_ads = round(pow($planPrice, (0.4 + $planPrice / 3900000 / 200)));
    $storeBudget->budget_allocation = $planPrice - 1000000;

    if (!$this->storeBudgets->save($storeBudget)) {
      return $listener->response(400);
    }

    if ($isNew) {

      $todaysBudget = $this->marketingBudget->today();
      $amount = round(($storeBudget->budget_allocation / $storeBudget->total_budget) * MarketingConfig::MARKETING_BUDGET_MULTIPLIER);

      $inserts = $this->budgetInserter->budgetData($amount, $owner->id, $store->id);

      $cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
      $cashInserter->clear();
      
      foreach ($inserts as $insert) {
        $cashInserter->add($insert);
      }
  
      $cashInserter->run();

      $todaysInOut = $this->incomeOutcome->today();
      $todaysInOut->outcome += $amount;
      $this->incomeOutcome->save($todaysInOut);
    }

    return $listener->response(200);
  }
}
