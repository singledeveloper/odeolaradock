<?php

namespace Odeo\Domains\Marketing;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ForBusiness;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Marketing\Repository\BusinessResellerBonusRepository;
use Odeo\Domains\Marketing\Repository\BusinessResellerGroupDetailRepository;
use Odeo\Domains\Marketing\Repository\BusinessResellerGroupRepository;
use Odeo\Domains\Marketing\Repository\BusinessResellerRepository;

class BusinessResellerGroupUpdater {

  private $businessResellerRepo, $businessResellerGroupRepo,
    $businessResellerGroupDetailRepo, $businessResellerBonusRepo;

  public function __construct() {
    $this->businessResellerRepo = app()->make(BusinessResellerRepository::class);
    $this->businessResellerGroupRepo = app()->make(BusinessResellerGroupRepository::class);
    $this->businessResellerGroupDetailRepo = app()->make(BusinessResellerGroupDetailRepository::class);
    $this->businessResellerBonusRepo = app()->make(BusinessResellerBonusRepository::class);
  }

  public function changeTemplate(PipelineListener $listener, $data) {
    if ($reseller = $this->businessResellerRepo->findById($data['business_reseller_id'])) {
      $reseller->template_id = $data['template_id'];
      $this->businessResellerRepo->save($reseller);

      return $listener->response(200);
    }
    return $listener->response(400, 'No data exist');
  }

  public function addGroup(PipelineListener $listener, $data) {
    if (!in_array($data['type'], ForBusiness::getTransactionTypeList()))
      return $listener->response(400, 'Transaction type is invalid');

    $paymentGroupId = isset($data['payment_group_id']) && $data['payment_group_id'] != '' ? $data['payment_group_id'] : null;
    if (isset($data['business_reseller_id'])) {
      $reseller = $this->businessResellerRepo->findById($data['business_reseller_id']);
      $data['template_id'] = $reseller->template_id;
    }
    if ($this->businessResellerGroupRepo->findExisting($data['template_id'], $data['type'], $paymentGroupId)) {
      return $listener->response(400, 'You have created this type of group.');
    }

    $group = $this->businessResellerGroupRepo->getNew();
    if (isset($data['business_reseller_id'])) {
      $group->business_reseller_id = $data['business_reseller_id'];
    }
    $group->type = $data['type'];
    $group->payment_group_id = $paymentGroupId;
    $group->template_id = $data['template_id'];
    $this->businessResellerGroupRepo->save($group);

    return $listener->response(200);
  }

  public function removeGroup(PipelineListener $listener, $data) {
    if ($group = $this->businessResellerGroupRepo->findById($data['group_id'])) {
      $group->is_active = false;
      $this->businessResellerGroupRepo->save($group);

      return $listener->response(200);
    }
    return $listener->response(400, 'No data');
  }

  public function addGroupDetail(PipelineListener $listener, $data) {

    if (!in_array($data['fee_type'], ForBusiness::getFeeTypeList()))
      return $listener->response(400, 'Fee type invalid');

    if ($data['fee_type'] == ForBusiness::FEE_PERCENTAGE && $data['fee_value'] > 100)
      return $listener->response(400, 'Fee value invalid');

    if (isset($data['minimal_type']) && $data['minimal_type'] != '') {
      if (!in_array($data['minimal_type'], ForBusiness::getConditionalTypeList()))
        return $listener->response(400, 'Minimal type invalid');

      if (!isset($data['minimal_value']) || (isset($data['minimal_value']) && $data['minimal_value'] == ''))
        return $listener->response(400, 'Minimal value need to be inputted');
    }

    $detail = $this->businessResellerGroupDetailRepo->getNew();
    $detail->business_reseller_group_id = $data['group_id'];
    if (isset($data['minimal_type']) && $data['minimal_type'] != '') {
      $detail->minimal_type = $data['minimal_type'];
      $detail->minimal_value = $data['minimal_value'];
    }
    if (isset($data['business_reseller_id'])) {
      $detail->business_reseller_id = $data['business_reseller_id'];
    }
    $detail->fee_type = $data['fee_type'];
    $detail->fee_value = $data['fee_value'];
    if (isset($data['campaign_start_date']) && $data['campaign_start_date'] != '') {
      $detail->campaign_start_date = $data['campaign_start_date'];
    }
    if (isset($data['campaign_end_date']) && $data['campaign_end_date'] != '') {
      $detail->campaign_end_date = $data['campaign_end_date'];
    }

    $this->businessResellerGroupDetailRepo->save($detail);

    return $listener->response(200, [
      'id' => $detail->id,
      'description' => ForBusiness::createMessageFromDetailData($detail)
    ]);
  }

  public function removeGroupDetail(PipelineListener $listener, $data) {
    if ($detail = $this->businessResellerGroupDetailRepo->findById($data['detail_id'])) {
      $detail->is_active = false;
      $this->businessResellerGroupDetailRepo->save($detail);

      return $listener->response(200);
    }
    return $listener->response(400, 'No data');
  }

  public function approveBonus(PipelineListener $listener, $data) {
    $this->businessResellerBonusRepo->approveAll();
    $redis = Redis::connection();
    $redis->hset(ForBusiness::REDIS_BUSINESS_RESELLER, ForBusiness::REDIS_LOCK_TRANSFER_BONUS, 1);

    return $listener->response(200);
  }

}
