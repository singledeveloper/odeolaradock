<?php

namespace Odeo\Domains\Marketing;

use Carbon\Carbon;
use Odeo\Domains\Constant\ForBusiness;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Disbursement\Helper\DisbursementApiHelper;
use Odeo\Domains\Transaction\Helper\Currency;

class BusinessResellerSelector implements SelectorListener {

  private $businessResellerRepo, $businessResellerGroupRepo,
    $businessResellerTemplateRepo, $businessResellerTransactionRepo,
    $currency, $graphicParams = '', $currentUserId = '';

  public function __construct() {
    $this->businessResellerRepo = app()->make(\Odeo\Domains\Marketing\Repository\BusinessResellerRepository::class);
    $this->businessResellerGroupRepo = app()->make(\Odeo\Domains\Marketing\Repository\BusinessResellerGroupRepository::class);
    $this->businessResellerTemplateRepo = app()->make(\Odeo\Domains\Marketing\Repository\BusinessResellerTemplateRepository::class);
    $this->businessResellerTransactionRepo = app()->make(\Odeo\Domains\Marketing\Repository\BusinessResellerTransactionRepository::class);
    $this->currency = app()->make(Currency::class);
    $this->graphicParams = 'amount';
  }

  public function _extends($data, Repository $repository) {
    if ($userIds = $repository->beginExtend($data, "user_id")) {
      $result = [];
      $percentage = [];
      $divider = [];
      $lastWeekFee = [];
      $lastWeekAmount = [];
      $paymentResult = [];

      $temp = [];
      foreach ($this->businessResellerTransactionRepo->getWeekTransactionsGroupByDate($userIds, $this->currentUserId) as $item) {
        if (!isset($temp[$item->user_id])) $temp[$item->user_id] = [];
        $temp[$item->user_id][$item->transaction_date] = [
          'transaction_date' => $item->transaction_date,
          'qty' => $item->qty ? $item->qty : 0,
          'fee' => $item->fee ? $item->fee : 0,
          'amount' => $item->amount
        ];
      }
      foreach ($userIds as $item) {
        if (!isset($result[$item])) $result[$item] = [];
        $lastWeekFee[$item] = 0;
        $lastWeekAmount[$item] = 0;
        for ($x = 7; $x >= 1; $x--) {
          $date = Carbon::now()->subDays($x)->toDateString();
          if (!(isset($temp[$item]) && isset($temp[$item][$date]))) {
            $detail = [
              'transaction_date' => $date,
              'qty' => 0,
              'fee' => 0,
              'amount' => 0
            ];
          }
          else $detail = $temp[$item][$date];
          $result[$item][] = $detail;
          if (!isset($divider[$item]) && $detail[$this->graphicParams] > 0) $divider[$item] = $detail[$this->graphicParams];
          $lastWeekFee[$item] += $detail['fee'];
          $lastWeekAmount[$item] += $detail['amount'];
        }
        $percentage[$item] = isset($divider[$item]) ?
          round(($result[$item][count($result[$item])-1][$this->graphicParams]
              - $divider[$item])/$divider[$item]*100, 1) : 0;
        $lastWeekFee[$item] = $this->currency->formatPrice($lastWeekFee[$item]);
        $lastWeekAmount[$item] = $this->currency->formatPrice($lastWeekAmount[$item]);

        if (!isset($paymentResult[$item])) $paymentResult[$item] = [];
      }
      $repository->addExtend('transactions', $result);
      $repository->addExtend('type_transactions', $paymentResult);
      $repository->addExtend('change_percentage', $percentage);
      $repository->addExtend('week_total_fee', $lastWeekFee);
      $repository->addExtend('week_total_amount', $lastWeekAmount);
    }
    return $repository->finalizeExtend($data);
  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function getOptions(PipelineListener $listener, $data) {
    $list = [];

    foreach($this->businessResellerRepo->getOptions(isset($data['business_reseller_id']) ? $data['business_reseller_id'] : '') as $item) {
      $list[] = [
        'id' => $item->id,
        'name' => $item->reseller_user_name . ' > ' . $item->user_name
      ];
    }

    return $listener->response(200, $list);
  }

  public function getList(PipelineListener $listener, $data) {
    $this->businessResellerRepo->normalizeFilters($data);
    $this->businessResellerRepo->setSimplePaginate(true);
    $list = [];

    foreach($this->businessResellerRepo->get() as $item) {
      $list[] = [
        'id' => $item->id,
        'reseller_user_name' => $item->reseller_user_name,
        'user_name' => $item->user_name,
        'template_name' => $item->template_name ? $item->template_name : '',
        'created_at' => $item->created_at->format('Y-m-d')
      ];
    }

    if (sizeof($list) > 0)
      return $listener->response(200, array_merge(
        ["business_resellers" => $list],
        $this->businessResellerRepo->getPagination()
      ));

    return $listener->response(204, ["business_resellers" => []]);
  }

  public function getTemplates(PipelineListener $listener, $data) {
    $list = [];

    foreach($this->businessResellerTemplateRepo->get() as $item) {
      $list[] = [
        'id' => $item->id,
        'name' => $item->name
      ];
    }

    if (sizeof($list) > 0)
      return $listener->response(200, ["business_templates" => $list]);

    return $listener->response(204, ["business_templates" => []]);
  }

  public function getDetail(PipelineListener $listener, $data) {
    if ($business = $this->businessResellerRepo->findById($data['business_reseller_id'])) {
      $template = $business->template;
      $groups = [
        'reseller_user_name' => $business->reseller->name,
        'user_name' => $business->user->name,
        'template_id' => $business->template_id,
        'template_name' => $template ? $template->name : '',
        'details' => []
      ];
      $temps = [];
      foreach($this->businessResellerGroupRepo->getByTemplateId($business->template_id, $data['business_reseller_id']) as $item) {
        if (!isset($temps[$item->id]))
          $temps[$item->id] = [
            'id' => $item->id,
            'type' => $item->type,
            'name' => ucwords(str_replace('_', ' ', $item->type))
              . ($item->type == ForBusiness::TRANSACTION_PAYMENT_GATEWAY ? (' / ' . ($item->payment_group_name ? $item->payment_group_name : 'All')) : ''),
            'payment_group_id' => $item->payment_group_id,
            'is_shared' => $item->business_reseller_id == null,
            'details' => []
          ];

        if ($item->detail_id) {
          $temps[$item->id]['details'][] = [
            'id' => $item->detail_id,
            'minimal_type' => $item->minimal_type,
            'minimal_value' => $item->minimal_value,
            'fee_type' => $item->fee_type,
            'fee_value' => $item->fee_value,
            'description' => ForBusiness::createMessageFromDetailData($item),
            'is_shared' => $item->detail_business_reseller_id == null
          ];
        }
      }

      foreach ($temps as $item) {
        $groups['details'][] = $item;
      }

      return $listener->response(200, ["business_reseller_details" => $groups]);
    }

    return $listener->response(204, ["business_resellers_details" => []]);
  }

  public function getDetailTemplate(PipelineListener $listener, $data) {
    if ($template = $this->businessResellerTemplateRepo->findById($data['template_id'])) {
      $groups = [
        'template_name' => $template->name,
        'details' => []
      ];
      $temps = [];
      foreach($this->businessResellerGroupRepo->getByTemplateId($data['template_id']) as $item) {
        if (!isset($temps[$item->id]))
          $temps[$item->id] = [
            'id' => $item->id,
            'type' => $item->type,
            'name' => ucwords(str_replace('_', ' ', $item->type))
              . ($item->type == ForBusiness::TRANSACTION_PAYMENT_GATEWAY ? (' / ' . ($item->payment_group_name ? $item->payment_group_name : 'All')) : ''),
            'payment_group_id' => $item->payment_group_id,
            'details' => [],
            'is_shared' => false
          ];

        if ($item->detail_id) {
          $temps[$item->id]['details'][] = [
            'id' => $item->detail_id,
            'minimal_type' => $item->minimal_type,
            'minimal_value' => $item->minimal_value,
            'fee_type' => $item->fee_type,
            'fee_value' => $item->fee_value,
            'description' => ForBusiness::createMessageFromDetailData($item),
            'is_shared' => false
          ];
        }
      }

      foreach ($temps as $item) {
        $groups['details'][] = $item;
      }

      return $listener->response(200, ["business_template_details" => $groups]);
    }

    return $listener->response(204, ["business_template_details" => []]);
  }

  public function getAgentList(PipelineListener $listener, $data) {
    $this->currentUserId = $data['auth']['user_id'];
    $this->businessResellerRepo->normalizeFilters($data);
    $this->businessResellerRepo->setSimplePaginate(true);
    $list = [];
    if (isset($data['graphic_param']))
      $this->graphicParams = $data['graphic_param'];

    foreach($this->businessResellerRepo->getAgents($data['auth']['user_id']) as $item) {
      $pos = strpos($item->user_telephone, UserType::BUSINESS_PREFIX);
      $name = $pos !== false && $pos == 0 ? str_replace(UserType::BUSINESS_PREFIX . '0', 'P', $item->user_telephone) : '';
      $list[] = [
        'id' => $item->id,
        'user_id' => $item->user_id,
        'user_name' => ($name != '' ? ('[' . $name . '] ') : '') . $item->user_name,
        'created_at' => $item->created_at->format('Y-m-d')
      ];
    }

    if (sizeof($list) > 0)
      return $listener->response(200, array_merge(
        ["business_resellers" => $this->_extends($list, $this->businessResellerRepo)],
        $this->businessResellerRepo->getPagination()
      ));

    return $listener->response(204, ["business_resellers" => []]);
  }

  public function getAgentDetail(PipelineListener $listener, $data) {
    if ($reseller = $this->businessResellerRepo->findById($data['business_reseller_id'])) {
      if (!isAdmin($data) && $reseller->reseller_user_id != $data['auth']['user_id']) {
        return $listener->response(400, 'No access');
      }

      $templates = [];
      foreach ($this->businessResellerGroupRepo->getByTemplateId($reseller->template_id, $reseller->id) as $item) {
        $index = $item->type . ($item->payment_group_id ? ('_' . $item->payment_group_id) : '');
        if (!isset($templates[$index])) $templates[$index] = [];
        $templates[$index][] = ForBusiness::createMessageShort($item);
      }

      $paymentTypeNeedToBe = [];

      $disbursementApiUserRepo = app()->make(\Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository::class);
      $disbursementUser = $disbursementApiUserRepo->findByUserId($reseller->user_id);
      $paymentTypeNeedToBe[ForBusiness::TRANSACTION_DISBURSEMENT] = [
        'name' => trans('disbursement.name'),
        'type' => isset($templates[ForBusiness::TRANSACTION_DISBURSEMENT]) ? implode(' / ', $templates[ForBusiness::TRANSACTION_DISBURSEMENT]) : '-',
        'qty' => 0,
        'fee' => $this->currency->formatPrice(0),
        'amount' => $this->currency->formatPrice(0),
        'avg_amount' => $this->currency->formatPrice($disbursementUser ? $disbursementUser->fee : DisbursementApiHelper::DISBURSEMENT_FEE)
      ];
      $paymentTypeNeedToBe[ForBusiness::TRANSACTION_DISBURSEMENT_INQUIRY] = [
        'name' => trans('disbursement.inquiry_name'),
        'type' => isset($templates[ForBusiness::TRANSACTION_DISBURSEMENT_INQUIRY]) ? implode(' / ', $templates[ForBusiness::TRANSACTION_DISBURSEMENT_INQUIRY]) : '-',
        'qty' => 0,
        'fee' => $this->currency->formatPrice(0),
        'amount' => $this->currency->formatPrice(0),
        'avg_amount' => $this->currency->formatPrice($disbursementUser ? $disbursementUser->inquiry_fee : DisbursementApiHelper::DISB_INQUIRY_FEE)
      ];

      $virtualAccountVendorRepo = app()->make(\Odeo\Domains\VirtualAccount\Repository\VirtualAccountVendorRepository::class);
      foreach ($virtualAccountVendorRepo->getFeeDataOnUser($reseller->user_id) as $item) {
        $index = ForBusiness::TRANSACTION_PAYMENT_GATEWAY . '_' . $item->payment_group_id;
        $paymentTypeNeedToBe[$index] = [
          'name' => trans('payment_gateway.name') . ' / ' . $item->name,
          'type' => isset($templates[$index]) ? implode(' / ', $templates[$index]) :
            (isset($templates[ForBusiness::TRANSACTION_PAYMENT_GATEWAY]) ? implode(' / ', $templates[ForBusiness::TRANSACTION_PAYMENT_GATEWAY]) : '-'),
          'qty' => 0,
          'fee' => $this->currency->formatPrice(0),
          'amount' => $this->currency->formatPrice(0),
          'avg_amount' => $this->currency->formatPrice($item->fee ? $item->fee :
            ($reseller->reseller_user_id == ForBusiness::STARWIN_RESELLER_ID ?
              $item->starwin_default_fee : $item->default_fee))
        ];
      }

      foreach ($this->businessResellerTransactionRepo->getWeekTransactionsGroupByType($reseller->user_id, $reseller->reseller_user_id) as $item) {
        $index = $item->type . ($item->payment_group_id ? ('_' . $item->payment_group_id) : '');
        $paymentTypeNeedToBe[$index]['qty'] = $item->qty ? number_format($item->qty, 0) : 0;
        $paymentTypeNeedToBe[$index]['fee'] = $this->currency->formatPrice($item->fee ? $item->fee : 0);
        $paymentTypeNeedToBe[$index]['amount'] = $this->currency->formatPrice($item->amount ? $item->amount : 0);
      }

      $result = [];
      foreach ($paymentTypeNeedToBe as $item) {
        $result[] = $item;
      }

      return $listener->response(200, ['type_transactions' => $result]);
    }
    return $listener->response(204, ['type_transactions' => []]);
  }

  public function getPgTransactions(PipelineListener $listener, $data) {
    $pg = [];
    $businessResellerTransactionRepo = app()->make(\Odeo\Domains\Marketing\Repository\BusinessResellerTransactionRepository::class);
    foreach ($businessResellerTransactionRepo->getLastTenDaysPaymentGatewayUsage($data['payment_group_id']) as $item) {
      $pg[] = [
        'name' => $item->name,
        'amount' => $this->currency->formatPrice($item->amount),
        'qty' => $item->qty
      ];
    }
    if (sizeof($pg) > 0)
      return $listener->response(200, ["pg" => $pg]);

    return $listener->response(204, ["pg" => []]);
  }

}
