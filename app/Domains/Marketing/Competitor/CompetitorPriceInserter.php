<?php

namespace Odeo\Domains\Marketing\Competitor;

use Carbon\Carbon;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository;
use Odeo\Domains\Marketing\Competitor\Repository\CompetitorPriceRepository;

class CompetitorPriceInserter {

  private $prices, $pulsas;

  public function __construct() {
    $this->prices = app()->make(CompetitorPriceRepository::class);
    $this->pulsas = app()->make(PulsaOdeoRepository::class);
  }

  public function bulkInsert(PipelineListener $listener, $data) {

    $lines = preg_split('/\n|\r\n?/', trim($data['prices']));

    list ($response, $competitorData, $valid) = $this->normalize($lines);

    $valid && $this->prices->saveBulk($competitorData);

    return $listener->response(200, [
      'response' => $response,
      'all_valid' => $valid,
    ]);

  }

  private function normalize($lines) {

    $competitorData = [];
    $response = [];
    $allValid = true;

    foreach ($lines as $line) {

      $row = explode(';', $line);

      try {

        if (count($row) !== 3) {
          throw new \Exception('jumlah data tidak 3');
        }

        $priceRow = $this->mappingPriceRow($row);
        $competitorData[] = $priceRow;

        $competitor = $this->prices->findByCompetitor($priceRow['competitor']);

        if (!count($competitor)) {
          $res = $this->responseWarning('data berhasil masuk. Kompetitor baru');
        } else {
          $res = $this->responseSuccess('data berhasil masuk');
        }

      } catch (\Exception $e) {
        $res = $this->responseError($e->getMessage());
        $allValid = false;
      }

      $res['line'] = $line;
      $response[] = $res;
    }

    return [$response, $competitorData, $allValid];
  }

  private function mappingPriceRow($data) {

    $dateTime = Carbon::now();

    $competitor = strtolower(trim($data[0]));
    $productName = strtolower(trim($data[1]));
    $price = strtolower(trim($data[2]));

    if (!isset($competitor)) {
      throw new \Exception('nama competitor kosong');
    }
    if (!($product = $this->pulsas->findByName($productName))) {
      throw new \Exception('nama product tidak ditemukan');
    }
    if (!ctype_digit($price)) {
      throw new \Exception('format harga tidak valid');
    }

    return [
      'competitor' => $competitor,
      'pulsa_odeo_id' => $product->id,
      'price' => $price,
      'created_at' => $dateTime,
      'updated_at' => $dateTime
    ];
  }

  private function responseError($message) {
    return [
      'status' => 'error',
      'message' => $message
    ];
  }

  private function responseWarning($message) {
    return [
      'status' => 'warning',
      'message' => $message
    ];
  }

  private function responseSuccess($message) {
    return [
      'status' => 'success',
      'message' => $message
    ];
  }

}