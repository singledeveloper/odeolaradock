<?php

namespace Odeo\Domains\Marketing\Competitor\Payfazz\Scheduler;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Odeo\Domains\Constant\PayfazzProductCode;
use Odeo\Domains\Marketing\Competitor\Repository\CompetitorPriceRepository;

class PriceScraperScheduler {

  private $priceRepo;

  private $PAYFAZZ_PRODUCT_URL = 'https://api.payfazz.com/v2/products';

  public function __construct() {
    $this->priceRepo = app()->make(CompetitorPriceRepository::class);
  }

  public function run() {
    $products = $this->getPayfazzProduct();
    $allowedOperator = PayfazzProductCode::allowedOperator();
    $mapWithOdeo = PayfazzProductCode::mapWithOdeo();

    $data = [];

    $createdAt = Carbon::now();

    foreach ($products as $item) {

      if (!in_array($item->operatorCode, $allowedOperator)) continue;
      if (!array_key_exists($item->productCode, $mapWithOdeo)) continue;

      $pulsaOdeoId = $mapWithOdeo[$item->productCode];

      if ($pulsaOdeoId) {
        $data[] = [
          'competitor' =>  PayfazzProductCode::PAYFAZZ,
          'pulsa_odeo_id' => $pulsaOdeoId,
          'price' => $item->sellPrice,
          'created_at' => $createdAt,
          'updated_at' => $createdAt
        ];
      }
    }


    !empty($data) && $this->priceRepo->saveBulk($data);

  }

  private function getPayfazzProduct() {

    try {

      $client = new Client();
      $response = $client->request('GET', $this->PAYFAZZ_PRODUCT_URL);
      $data = json_decode($response->getBody()->getContents())->data;

      return $data;

    } catch (\Exception $e) {

      return [];

    }
  }

}
