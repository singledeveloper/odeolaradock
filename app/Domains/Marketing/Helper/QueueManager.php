<?php

namespace Odeo\Domains\Marketing\Helper;

use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\Service;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use DB;

class QueueManager {

  private $rows = [];

  public function __construct() {
    $this->redis = Redis::connection();
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->storeParser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);
    $this->tree = app()->make(\Odeo\Domains\Network\Repository\TreeRepository::class);
    $this->cash = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
  }

  public function getByDeposit($serviceId, $minDeposit, $level, $exclude = []) {
    $activeStores = $this->stores->getAllActiveMarketPlace('owner')->keyBy('id');
    $activeStoreIds = $activeStores->pluck('id')->toArray();
    $storeRoles = $this->tree->getStoreRoles($activeStoreIds)->keyBy('store_id');
    $storeDeposits = $this->cash->getStoreBalance($activeStoreIds, CashType::ODEPOSIT)->toArray();

    $profitQueue = 'odeo_core:store_profit_queue:' . $serviceId;

    $stores = $this->redis->zrevrange($profitQueue, 0, -1);
    $validStores = array();
    foreach($stores as $store) {
      $storeId = explode(':', $store)[0];
      if(in_array($storeId, $exclude)) continue;
      if(!isset($activeStores[$storeId])) continue;
      $role = $storeRoles[$storeId];
      if(isset($storeDeposits[$storeId]) && $storeDeposits[$storeId] >= $minDeposit && $role->{'is_'.$level}) {
        $validStores[] = [
          'user_id' => $activeStores[$storeId]->owner[0]->id,
          'store_id' => $storeId,
          'store_name' => $activeStores[$storeId]->name,
        ];
      }
    }

    return $validStores;
  }

  public function getByRush($serviceId, $minRush, $minDeposit, $exclude = [], $orderBy = 'profit') {
    $activeStores = $this->stores->getAllActiveStoreWithoutFreeStore('owner')->keyBy('id');
    $activeStoreIds = $activeStores->pluck('id')->toArray();
    $storeRushes = $this->cash->getStoreBalance($activeStoreIds, CashType::ORUSH)->toArray();
    $storeDeposits = $this->cash->getStoreBalance($activeStoreIds, CashType::ODEPOSIT)->toArray();

    $profitQueue = 'odeo_core:store_profit_queue:' . $serviceId;
    $profitIdx = 'odeo_core:store_profit_idx_queue';

    $storeProfits = $this->redis->zrevrange($profitQueue, 0, -1);
    asort($storeRushes);

    $stores = $orderBy == 'profit' ? $storeProfits : $storeRushes;
    $validStores = array();

    foreach($stores as $key => $value) {
      $storeId = $orderBy == 'profit' ? explode(':', $value)[0] : $key;
      if(in_array($storeId, $exclude)) continue;
      if(!in_array($storeId, $activeStoreIds)) continue;
      $storeDetail = $activeStores[$storeId];
      $currentData = json_decode($this->storeParser->currentData($storeDetail->plan_id, $storeDetail->current_data));
      $serviceName = strtolower(Service::getConstKeyByValue($serviceId));
      if(!isset($currentData) || !isset($currentData->$serviceName) || (isset($currentData->$serviceName) && $currentData->$serviceName <= 0)) continue;
      if($storeRushes[$storeId] >= $minRush && $storeDeposits[$storeId] >= $minDeposit) {
        $validStores[] = [
          'user_id' => $activeStores[$storeId]->owner[0]->id,
          'store_id' => $storeId,
          'store_name' => $activeStores[$storeId]->name,
        ];
      }
    }

    return $validStores;
  }
}
