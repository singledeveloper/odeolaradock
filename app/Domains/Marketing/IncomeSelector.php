<?php

namespace Odeo\Domains\Marketing;

use Carbon\Carbon;
use Odeo\Domains\Core\PipelineListener;

class IncomeSelector {

  public function __construct() {
    $this->incomes = app()->make(\Odeo\Domains\Marketing\Repository\IncomeOutcomeRepository::class);
  }

  public function get(PipelineListener $listener, $data) {

    $dayNum = $data['day_num'];
    $incomes = $this->incomes->findLastNDay(10);
    $response = array();
    $incomes_array = array();

    foreach($incomes as $income) {
      $incomes_array[$income->date] = [
        "income" => $income->income,
        "outcome" => $income->outcome
      ];
    }
    $now = Carbon::now()->timestamp;

    for($i = 1; $i <= $dayNum; $i++) {
      $date = date('Y-m-d', $now - ($dayNum - $i) * 86400);
      $response["incomes"][$date] = isset($incomes_array[$date]) ? $incomes_array[$date]["income"] : 0;
      $response["outcomes"][$date] = isset($incomes_array[$date]) ? $incomes_array[$date]["outcome"] : 0;
    }

    return $listener->response(200, $response);

  }



}
