<?php

namespace Odeo\Domains\Marketing\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class BusinessReseller extends Entity
{
  public function user() {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function reseller() {
    return $this->belongsTo(User::class, 'reseller_user_id');
  }

  public function template() {
    return $this->belongsTo(BusinessResellerTemplate::class, 'template_id');
  }
}
