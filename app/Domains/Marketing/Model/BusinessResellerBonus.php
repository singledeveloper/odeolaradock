<?php

namespace Odeo\Domains\Marketing\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class BusinessResellerBonus extends Entity
{
  public function transaction() {
    return $this->belongsTo(BusinessResellerTransaction::class, 'business_reseller_transaction_id');
  }

  public function reseller() {
    return $this->belongsTo(User::class, 'reseller_user_id');
  }

}
