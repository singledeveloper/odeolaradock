<?php

namespace Odeo\Domains\Marketing\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class BusinessResellerTransaction extends Entity
{
  public function user() {
    return $this->belongsTo(User::class);
  }
}
