<?php

namespace Odeo\Domains\Marketing\Model;

use Odeo\Domains\Core\Entity;

class MarketingBudget extends Entity
{
  public $timestamps = false;
}
