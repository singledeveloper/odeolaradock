<?php

namespace Odeo\Domains\Marketing\Model;

use Odeo\Domains\Core\Entity;

class StoreMarketingBudget extends Entity
{
  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = ['created_at', 'updated_at'];

  protected $fillable = ['store_id', 'variant', 'additional_budget', 'plan_price_per_day'];

  public $timestamps = true;

}
