<?php

namespace Odeo\Domains\Marketing\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Model\ServiceDetail;
use Odeo\Domains\Subscription\Model\Store;

class StoreRevenue extends Entity
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date'];

    public $timestamps = false;

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
