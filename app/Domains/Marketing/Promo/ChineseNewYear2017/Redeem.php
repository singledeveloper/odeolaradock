<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/26/17
 * Time: 9:19 PM
 */

namespace Odeo\Domains\Marketing\Promo\ChineseNewYear2017;

use Carbon\Carbon;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;

class Redeem {


  private $orderDetailPulsaSwitcher;

  public function __construct() {
    $this->orderDetailPulsaSwitcher = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
  }

  public function redeem(PipelineListener $listener, $data) {

    $order = $data['order'];

    if ($order->platform_id != Platform::ANDROID && $order->platform_id != Platform::WEB_APP) {
      return $listener->response(200);
    }
    if (Carbon::parse($order->created_at)->toDateString() != '2017-01-28') {
      return $listener->response(200);
    }
    if (count($this->orderDetailPulsaSwitcher->getByDateAndNumber($data['phone_number'], '2017-01-28')) > 1) {
      return $listener->response(200);
    }

    $inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);

    $cashbackAmount = floor(min($order->total / 100 * 5, 10000));

    $inserter->add([
      'user_id' => $order->user_id,
      'trx_type' => TransactionType::PROMO_CASHBACK,
      'cash_type' => CashType::OCASH,
      'amount' =>   $cashbackAmount,
      'data' => json_encode([
        'order_id' => $order->id,
        "event" => "chinese new year 2017",
        "start_date" => '2017-01-28',
        "end_date" => '2017-01-28'
      ])
    ]);

    $inserter->run();

    $notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);

    $notification->setup($order->user_id, NotificationType::CHECK, NotificationGroup::TRANSACTION);
    $notification->promoCashback($cashbackAmount, '"promo imlek" event');

    $listener->pushQueue($notification->queue());


    return $listener->response(200);

  }
  
}