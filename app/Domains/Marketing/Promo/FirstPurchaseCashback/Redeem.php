<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/26/17
 * Time: 9:19 PM
 */

namespace Odeo\Domains\Marketing\Promo\FirstPurchaseCashback;


use Carbon\Carbon;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Model\Order;

class Redeem {

  public function redeem(PipelineListener $listener, $data) {

    $order = $data['order'];

    $date = Carbon::parse($order->created_at);

    if ($date->year != 2017 || $date->month != 3 || $date->day < 6 || $date->day > 12) {
      return $listener->response(200);
    }

    $user = app()->make(UserRepository::class)->findById($order->user_id);

    if ($user->type != 'seller') {
      return $listener->response(200);
    }


    $count = app()->make(Order::class)
      ->join('order_details', 'order_details.order_id', '=', 'orders.id')
      ->where('orders.user_id', '=', $order->user_id)
      ->where('orders.status', '>=', 30001)
      ->whereIn('order_details.service_detail_id', [
        ServiceDetail::PULSA_ODEO,
        ServiceDetail::PAKET_DATA_ODEO,
        ServiceDetail::BOLT_ODEO,
        ServiceDetail::PULSA_POSTPAID_ODEO,
        ServiceDetail::PLN_ODEO,
        ServiceDetail::PLN_POSTPAID_ODEO,
        ServiceDetail::BPJS_KES_ODEO,
        ServiceDetail::BROADBAND_ODEO,
        ServiceDetail::LANDLINE_ODEO,
        ServiceDetail::PDAM_ODEO,
        ServiceDetail::PGN_ODEO,
        ServiceDetail::EMONEY_ODEO
      ])->count();

    if ($count != 1) return $listener->response(200);

    $inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);

    $cashbackAmount = floor(min($order->total / 100 * 5, 50000));

    $inserter->add([
      'user_id' => $order->user_id,
      'trx_type' => TransactionType::PROMO_CASHBACK,
      'cash_type' => CashType::OCASH,
      'amount' => $cashbackAmount,
      'data' => json_encode([
        'order_id' => $order->id,
        "event" => "cashback 5% max Rp50ribu pakai kredivo",
        "start_date" => '2017-03-6',
        "end_date" => '2017-03-12'
      ])
    ]);

    $inserter->run();

    $notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);

    $notification->setup($order->user_id, NotificationType::CHECK, NotificationGroup::TRANSACTION);
    $notification->promoCashback($cashbackAmount, '"promo cashback 5% pakai kredivo"');

    $listener->pushQueue($notification->queue());

    return $listener->response(200);

  }

}
