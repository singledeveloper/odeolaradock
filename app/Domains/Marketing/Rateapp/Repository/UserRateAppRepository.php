<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/2/17
 * Time: 3:17 PM
 */

namespace Odeo\Domains\Marketing\Rateapp\Repository;


use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Rateapp\Model\UserRateApp;

class UserRateAppRepository extends Repository {

  public function __construct(UserRateApp $userRateApp) {
    $this->model = $userRateApp;
  }

  public function getExistingRate($userId, $platformId) {
    return $this->model
      ->where('user_id', $userId)
      ->where('platform_id', $platformId)
      ->first();
  }

  public function getMustBeApplied() {
    return $this->model
      ->where('closed_at', '=', null)
      ->where('applied_at', '<=', Carbon::now())
      ->get();
  }

  public function getRate($day) {

    $date = Carbon::now()->subDays($day)->format('Y-m-d');

    return $this->model->where(\DB::raw('date(closed_at)'), $date)->get();
  }

  public function checkIsUsedOnAndroid($data) {

    return $this->model
      ->join('user_android_identifiers', 'app_identifier_id', '=', 'user_android_identifiers.id')
      ->where('ssaid', $data['ssaid'])
      ->where('instance_id', $data['instance_id'])
      ->get();

  }
}