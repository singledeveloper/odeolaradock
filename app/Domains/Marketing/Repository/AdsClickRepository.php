<?php

namespace Odeo\Domains\Marketing\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Model\AdsClick;

class AdsClickRepository extends Repository {

  public function __construct(AdsClick $adsClick) {
    $this->model = $adsClick;
  }

}
