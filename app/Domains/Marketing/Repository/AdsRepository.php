<?php

namespace Odeo\Domains\Marketing\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Model\Ads;

class AdsRepository extends Repository {

  public function __construct(Ads $ads) {
    $this->model = $ads;
  }

}
