<?php

namespace Odeo\Domains\Marketing\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Model\BusinessResellerBonus;

class BusinessResellerBonusRepository extends Repository {

  public function __construct(BusinessResellerBonus $businessResellerBonus) {
    $this->model = $businessResellerBonus;
  }

  public function findByTransactionId($transactionId) {
    return $this->getCloneModel()->where('business_reseller_transaction_id', $transactionId)->first();
  }

  public function getUnsent() {
    return $this->getCloneModel()->with(['transaction', 'reseller'])
      ->where('is_sent', false)->where('is_approved', true)
      ->skip(0)->take(50)->orderBy('id', 'asc')
      ->get();
  }

  public function setSent($ids) {
    $this->getCloneModel()->whereIn('id', $ids)->update(['is_sent' => true]);
  }

  public function approveAll() {
    $this->getCloneModel()->where('is_approved', false)->update(['is_approved' => true]);
  }

}
