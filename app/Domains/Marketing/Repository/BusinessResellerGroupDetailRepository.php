<?php

namespace Odeo\Domains\Marketing\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Model\BusinessResellerGroupDetail;

class BusinessResellerGroupDetailRepository extends Repository {

  public function __construct(BusinessResellerGroupDetail $businessResellerGroupDetail) {
    $this->model = $businessResellerGroupDetail;
  }

  public function getByGroupId($groupId, $resellerId) {
    return $this->getCloneModel()
      ->where('business_reseller_group_id', $groupId)
      ->where(function($query) use ($resellerId) {
      $query->whereNull('business_reseller_id')
        ->orWhere('business_reseller_id', $resellerId);
    })
      ->where('is_active', true)
      ->orderByRaw('campaign_start_date desc nulls last')
      ->orderByRaw('minimal_value desc nulls last')
      ->get();
  }

}
