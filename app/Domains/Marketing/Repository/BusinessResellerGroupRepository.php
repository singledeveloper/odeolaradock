<?php

namespace Odeo\Domains\Marketing\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Model\BusinessResellerGroup;

class BusinessResellerGroupRepository extends Repository {

  public function __construct(BusinessResellerGroup $businessResellerGroup) {
    $this->model = $businessResellerGroup;
  }

  public function findByType($resellerId, $templateId, $type, $pgGroupId = null) {
    return $this->getCloneModel()->where('template_id', $templateId)
      ->where(function($query) use ($resellerId) {
        $query->whereNull('business_reseller_id')
          ->orWhere('business_reseller_id', $resellerId);
      })
      ->where(function($query2) use ($pgGroupId) {
        $query2->whereNull('payment_group_id');
        if ($pgGroupId != null) $query2->orWhere('payment_group_id', $pgGroupId);
      })
      ->where('type', $type)->where('is_active', true)
      ->orderBy('payment_group_id', 'asc')->first();
  }

  public function getByTemplateId($templateId, $businessResellerId = '') {
    $query3 = $this->getCloneModel()
      ->leftJoin('business_reseller_group_details', 'business_reseller_group_details.business_reseller_group_id', '=', 'business_reseller_groups.id')
      ->leftJoin('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'business_reseller_groups.payment_group_id')
      ->where('business_reseller_groups.is_active', true)
      ->where(function($query4){
        $query4->whereNull('business_reseller_group_details.is_active')
          ->orWhere('business_reseller_group_details.is_active', true);
      })
      ->where('business_reseller_groups.template_id', $templateId);

    if ($businessResellerId != '')
      $query3->where(function($query) use ($businessResellerId) {
        $query->whereNull('business_reseller_groups.business_reseller_id')
          ->orWhere('business_reseller_groups.business_reseller_id', $businessResellerId);
      })->where(function($query2) use ($businessResellerId) {
        $query2->whereNull('business_reseller_group_details.business_reseller_id')
          ->orWhere('business_reseller_group_details.business_reseller_id', $businessResellerId);
      });
    else {
      $query3->whereNull('business_reseller_groups.business_reseller_id')
        ->whereNull('business_reseller_group_details.business_reseller_id');
    }

    return $query3->select(
      'business_reseller_groups.id',
      'business_reseller_groups.type',
      'business_reseller_groups.payment_group_id',
      'payment_odeo_payment_channel_informations.name as payment_group_name',
      'business_reseller_groups.business_reseller_id',
      'business_reseller_group_details.id as detail_id',
      'business_reseller_group_details.minimal_type',
      'business_reseller_group_details.minimal_value',
      'business_reseller_group_details.fee_type',
      'business_reseller_group_details.fee_value',
      'business_reseller_group_details.campaign_start_date',
      'business_reseller_group_details.campaign_end_date',
      'business_reseller_group_details.business_reseller_id as detail_business_reseller_id'
    )
      ->get();
  }

  public function findExisting($templateId, $type, $pgGroupId) {
    $query = $this->getCloneModel()->where('template_id', $templateId)
      ->where('type', $type)->where('is_active', true);

    if ($pgGroupId) $query->where('payment_group_id', $pgGroupId);
    else $query->whereNull('payment_group_id');

    return $query->first();
  }

  public function checkExisting($resellerId) {
    return $this->getCloneModel()->where('business_reseller_id', $resellerId)
      ->where('is_active', true)->first();
  }
}
