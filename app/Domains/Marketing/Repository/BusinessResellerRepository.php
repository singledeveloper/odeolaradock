<?php

namespace Odeo\Domains\Marketing\Repository;

use Odeo\Domains\Constant\ForBusiness;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Model\BusinessReseller;

class BusinessResellerRepository extends Repository {

  public function __construct(BusinessReseller $businessReseller) {
    $this->model = $businessReseller;
  }

  public function getByUserId($userId) {
    return $this->getCloneModel()->where('user_id', $userId)
      ->where('is_active', true)->get();
  }

  public function checkExistingReseller($userId) {
    return $this->getCloneModel()->where('reseller_user_id', $userId)
      ->where('is_active', true)->first();
  }

  public function get() {
    return $this->getResult($this->getCloneModel()
      ->join('users as reseller_users', \DB::raw('reseller_users.id'), '=', \DB::raw('business_resellers.reseller_user_id'))
      ->join('users', 'users.id', '=', 'business_resellers.user_id')
      ->leftJoin('business_reseller_templates', 'business_reseller_templates.id', '=', 'business_resellers.template_id')
      ->where('business_resellers.is_active', true)
      ->select(
        'business_resellers.id',
        \DB::raw('reseller_users.name as reseller_user_name'),
        \DB::raw('users.name as user_name'),
        \DB::raw('business_reseller_templates.name as template_name'),
        'business_resellers.created_at'
      )->orderBy(\DB::raw('users.telephone'), 'desc')
    );
  }

  public function getOptions($resellerId) {
    $query = $this->getCloneModel()
      ->join('users as reseller_users', \DB::raw('reseller_users.id'), '=', \DB::raw('business_resellers.reseller_user_id'))
      ->join('users', 'users.id', '=', 'business_resellers.user_id')
      ->where('business_resellers.is_active', true);

    if ($resellerId != '') $query->where('business_resellers.id', '<>', $resellerId);

    return $query->select(
      'business_resellers.id',
      \DB::raw('reseller_users.name as reseller_user_name'),
      \DB::raw('users.name as user_name')
    )->orderBy(\DB::raw('reseller_users.name'), 'asc')->get();
  }

  public function getAgents($resellerId) {
    return $this->getResult($this->getCloneModel()
      ->join('users', 'users.id', '=', 'business_resellers.user_id')
      ->where('reseller_user_id', $resellerId)
      ->where('business_resellers.is_active', true)
      ->select(
        'business_resellers.id',
        \DB::raw('users.id as user_id'),
        \DB::raw('users.name as user_name'),
        \DB::raw('users.telephone as user_telephone'),
        'business_resellers.created_at'
      )->orderBy(\DB::raw('users.name'), 'asc')
    );
  }

  public function isStarwinAgent($userId) {
    return $this->getCloneModel()->where('user_id', $userId)
      ->where('reseller_user_id', ForBusiness::STARWIN_RESELLER_ID)->first(); // JIM PRIVATE
  }

}
