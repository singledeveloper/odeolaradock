<?php

namespace Odeo\Domains\Marketing\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Model\BusinessResellerTemplate;

class BusinessResellerTemplateRepository extends Repository {

  public function __construct(BusinessResellerTemplate $businessResellerTemplate) {
    $this->model = $businessResellerTemplate;
  }

  public function get() {
    return $this->getCloneModel()->where('is_active', true)
      ->orderBy('name', 'asc')->get();
  }

}
