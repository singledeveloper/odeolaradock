<?php

namespace Odeo\Domains\Marketing\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Model\MarketingBudget;

class MarketingBudgetRepository extends Repository {

  public function __construct(MarketingBudget $marketingBudget) {
    $this->model = $marketingBudget;
  }

  public function today() {
    return $this->model->where('date', date('Y-m-d'))->first();
  }

  public function findTotalBudget($variant, $dateStart, $dateEnd, $isNew) {
    if($isNew) $multiplier = '(0.55 + 0.0025 * day_num)';
    else $multiplier = '1';
    return $this->model->select(\DB::raw('sum(variant_' . $variant . ' * ' . $multiplier . ') as total'))
      ->from(\DB::raw('(select variant_' . $variant . ', row_number() over () as day_num from marketing_budgets where
      date >= \'' . $dateStart . '\' and date < \'' . $dateEnd . '\') mb'))->first()->total;
  }
}
