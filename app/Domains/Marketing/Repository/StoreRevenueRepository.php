<?php

namespace Odeo\Domains\Marketing\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Model\StoreRevenue;
use Carbon\Carbon;

class StoreRevenueRepository extends Repository {

  private $lock = false;

  public function __construct(StoreRevenue $storeRevenue) {
    $this->model = $storeRevenue;
  }

  public function lock() {
    $this->lock = true;
  }

  public function findYesterday($storeIds, $orderBy = 'total', $sort = 'desc') {
    return $this->model->whereIn('store_id', $storeIds)->where('date', Carbon::yesterday())->orderBy($orderBy, $sort)->get();
  }

  public function findToday($orderBy = 'amount', $sort = 'desc') {
    return $this->model->where('date', Carbon::today())->orderBy($orderBy, $sort)->get();
  }

  public function findLatest($activeStores) {
    $query = $this->getCloneModel();

    $query = $query->select(\DB::raw('*, max(total_profit_free) over (partition by service_id) as max_profit'))
      ->from(\DB::raw('(select distinct on (store_id, service_id) *
        from "store_revenues"
        order by "store_id" asc, "service_id" asc, "date" desc) s'))
      ->whereIn('store_id', $activeStores);

    return $query->get();
  }

  public function findTodayByStore($storeId, $serviceId) {
    $model = $this->model;
    if ($this->lock) $model = $model->lockForUpdate();
    return $this->model->where('store_id', $storeId)->where('service_id', $serviceId)->where('date', Carbon::today())->first();
  }

  public function findLatestByStore($storeId, $serviceId) {
    $model = $this->model;
    if ($this->lock) $model = $model->lockForUpdate();
    return $this->model->where('store_id', $storeId)->where('service_id', $serviceId)->orderBy('id', 'desc')->first();
  }

  public function getDetails($storeId, $dates, &$lifetime = []) {
    $query = $this->getCloneModel();

    $query = $query->select(\DB::raw('store_id, date, sum(amount) as total_revenue, sum(amount_profit) + sum(amount_rush) as total_profit, sum(order_count) as total_order'))
      ->where('store_id', $storeId)
      ->whereIn('date', $dates)->groupBy('date', 'store_id');
    return $query->get();
  }

  public function exportDetails($storeId, $dates, $filters) {
    $query = $this->getCloneModel();

    $query = $query->select(\DB::raw('store_id, date, sum(amount) as total_revenue, sum(amount_profit) + sum(amount_rush) as total_profit, sum(order_count) as total_order'))
      ->where('store_id', $storeId)
      ->whereIn('date', $dates)->groupBy('date', 'store_id')
      ->where('date', '>=', $filters['start_date'])
      ->where('date', '<=', $filters['end_date'])
      ->orderBy('date', 'desc');
    return $query->get();
  }

  public function findStoreLifetimeTotal($storeIds) {

    $query = $this->model
      ->select(\DB::raw('store_id, sum(total) as lifetime_revenue, sum(total_profit) + sum(total_rush) as lifetime_profit, sum(total_order_count) as lifetime_order'))
      ->from(\DB::raw('(select distinct on (store_id, service_id) *
        from "store_revenues"
        order by "store_id" asc, "service_id" asc, "date" desc) s'))
      ->groupBy('store_id');

    if (is_array($storeIds)) $query->whereIn('store_id', $storeIds);
    else {
      $query->where('store_id', $storeIds);
    }

    $query->groupBy('store_id');

    if (is_array($storeIds)) return $query->get();
    else {
      return $query->first();
    }
  }

  public function getWeekStoreOrder($storeId, $isPrevWeek = false) {

    $query = $this->model
      ->select(\DB::raw('store_id, sum(total_order_count) as count'))
      ->from(\DB::raw('(select distinct on (store_id, service_id) *
        from "store_revenues"
        order by "store_id" asc, "service_id" asc, "date" desc) s'));

    $query->where('store_id', $storeId);

    $date = Carbon::now()->startOfWeek()->addDays(2);

    if ($isPrevWeek) {
      $date = $date->subWeek();
    }
    if ($date->dayOfWeek < 3) {
      $date = $date->subWeek();
    }

    $startDate = $date->format('Y-m-d');
    $endDate = $date->addWeek()->subDay()->format('Y-m-d');

    $dates = [
      "start_date" => $startDate,
      "end_date" => $endDate,
    ];

    $query->where('date', '>=', $startDate)
      ->where('date', '<=', $endDate)
      ->groupBy('store_id');

    $result = $query->first();

    $result = $result ? $result->toArray() : ['count' => 0];

    return array_merge($result, $dates);
  }

  public function findStoreTotalBetween($storeIds, $dateStart, $dateEnd, &$profits = [], &$orderCounts = []) {
    $query = $this->getCloneModel();

    $query = $query->select(\DB::raw('store_id, date, sum(amount) as total_revenue, sum(amount_profit) + sum(amount_rush) as total_profit, sum(order_count) as total_order'))
      ->where('date', '>=', $dateStart)
      ->where('date', '<=', $dateEnd);

    if (is_array($storeIds)) $query = $query->whereIn('store_id', $storeIds);
    else $query = $query->where('store_id', $storeIds);

    $query = $query->groupBy('date', 'store_id');
    $profits = $query->pluck('total_profit', 'store_id');
    $orderCounts = $query->pluck('total_order', 'store_id');

    return $query->pluck('total_revenue', 'store_id');
  }
}
