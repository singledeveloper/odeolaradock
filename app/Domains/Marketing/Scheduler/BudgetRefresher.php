<?php

namespace Odeo\Domains\Marketing\Scheduler;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\MarketingConfig;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;

class BudgetRefresher {

  public function __construct() {
    $this->marketingBudget = app()->make(\Odeo\Domains\Marketing\Repository\MarketingBudgetRepository::class);
    $this->storeBudgets = app()->make(\Odeo\Domains\Marketing\Repository\StoreMarketingBudgetRepository::class);
    $this->marginMultiplier = app()->make(\Odeo\Domains\Marketing\Repository\MarginMultiplierRepository::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->userCash = app()->make(\Odeo\Domains\Transaction\Repository\UserCashRepository::class);
    $this->incomeOutcome = app()->make(\Odeo\Domains\Marketing\Repository\IncomeOutcomeRepository::class);
    $this->budgetInserter = app()->make(\Odeo\Domains\Marketing\Helper\BudgetInserter::class);
  }

  public function run() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(__CLASS__, 'refreshMultiplier'));
    $pipeline->add(new Task(__CLASS__, 'refreshBudget'));
    $pipeline->enableTransaction();
    $pipeline->execute([]);
  }

  public function refreshMultiplier(PipelineListener $listener, $data) {
    $services = $this->serviceDetails->getAll();
    $this->marginMultiplier->truncate();

    foreach($services as $service) {
      if(!isset($service->margin)) continue;

      $min = abs($service->default_hustler_margin);
      $max = abs($service->default_hustler_margin);

      for($margin = $min; $margin <= $max; $margin += $min) {
        $newMultiplier = $this->marginMultiplier->getNew();
        $newMultiplier->service_detail_id = $service->id;
        $newMultiplier->margin = $service->margin_type == 'value' ? floor($margin) : $margin;
        if($max > 0) {
          $newMultiplier->multiplier = $this->generateMultiplier($margin / $max);
          $this->marginMultiplier->save($newMultiplier);
        } else {
          $newMultiplier->multiplier = 1;
          $this->marginMultiplier->save($newMultiplier);
          break;
        }
      }
    }
    $listener->response(200);
  }

  public function refreshBudget(PipelineListener $listener, $data) {
    $stores = $this->storeBudgets->getActive();

    $totalBudget = 0;
    $inserts = [];

    foreach($stores as $store) {
      $amount = round(($store->budget_allocation / $store->total_budget) * MarketingConfig::MARKETING_BUDGET_MULTIPLIER);

      $inserts = array_merge($inserts, $this->budgetInserter->budgetData($amount, $store->user_id, $store->store_id));

      $totalBudget += $amount;
    }
    
    $cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $cashInserter->clear();

    foreach ($inserts as $insert) {
      $cashInserter->add($insert);
    }

    $cashInserter->run();

    $todaysInOut = $this->incomeOutcome->today();
    $todaysInOut->outcome += $totalBudget;
    $this->incomeOutcome->save($todaysInOut);
    $listener->response(200);
  }

  private function generateMultiplier($margin) {
    return mt_rand(200, 400) / 100;
  }
}
