<?php

namespace Odeo\Domains\Marketing\Scheduler;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\CashConfig;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\ForBusiness;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Notification\Helper\InternalNoticer;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;

class BusinessBonusSender {

  private $businessTransactionRepo, $businessResellerRepo, $businessBonusRepo,
    $businessResellerGroupRepo, $businessResellerGroupDetailRepo,
    $cashInserter, $redis, $userCashRepo,
    $salesUserId, $marketingUserId, $escrowUserId, $capitalUserId;

  public function __construct() {
    $this->businessTransactionRepo = app()->make(\Odeo\Domains\Marketing\Repository\BusinessResellerTransactionRepository::class);
    $this->businessResellerRepo = app()->make(\Odeo\Domains\Marketing\Repository\BusinessResellerRepository::class);
    $this->businessBonusRepo = app()->make(\Odeo\Domains\Marketing\Repository\BusinessResellerBonusRepository::class);
    $this->businessResellerGroupRepo = app()->make(\Odeo\Domains\Marketing\Repository\BusinessResellerGroupRepository::class);
    $this->businessResellerGroupDetailRepo = app()->make(\Odeo\Domains\Marketing\Repository\BusinessResellerGroupDetailRepository::class);
    $this->userCashRepo = app()->make(\Odeo\Domains\Transaction\Repository\UserCashRepository::class);
    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->redis = Redis::connection();
    $this->salesUserId = isProduction() ? 183136 : 16574;
    $this->marketingUserId = isProduction() ? CashConfig::MARKETING_USER_ID : 16752;
    $this->escrowUserId = isProduction() ? CashConfig::COMPANY_USER_ID : 16753;
    $this->capitalUserId = isProduction() ? 200442 : 16753;
  }

  private function setBonus($bonus, $transaction, $minus = false) {
    $type = $transaction->user_id == $bonus->reseller_user_id ? TransactionType::BUSINESS_CASHBACK :
      ($bonus->business_reseller_group_detail_id == null ? TransactionType::SALES_BONUS : TransactionType::RESELLER_BONUS);
    return [
      'user_id' => $minus ? $this->marketingUserId : $bonus->reseller_user_id,
      'trx_type' => $type,
      'cash_type' => CashType::OCASH,
      'amount' => $minus ? -$bonus->reseller_fee : $bonus->reseller_fee,
      'reference_type' => TransactionType::BUSINESS_RESELLER,
      'reference_id' => $bonus->id,
      'data' => json_encode([
        'id' => $bonus->id,
        'to_account_name' => $bonus->reseller ? $bonus->reseller->name : '',
        'description' => ucwords(str_replace('_', ' ', $type)) .
          ' - ' . ucwords(str_replace('_', ' ', $transaction->type)) .
          (' [' . $transaction->user->name . ']') . ' / ' . $transaction->transaction_date
      ])
    ];
  }

  public function run() {
    if ($this->redis->hget(ForBusiness::REDIS_BUSINESS_RESELLER, ForBusiness::REDIS_LOCK_TRANSFER_CAPITAL))
      $this->transferToCapital();
    else if ($this->redis->hget(ForBusiness::REDIS_BUSINESS_RESELLER, ForBusiness::REDIS_LOCK_TRANSFER_FROM_MARKETING))
      $this->reloadMarketingBudget();
    else if ($this->redis->hget(ForBusiness::REDIS_BUSINESS_RESELLER, ForBusiness::REDIS_LOCK_TRANSFER_BONUS))
      $this->transferBonus();
    else $this->accumulateBonus();
  }

  private function accumulateBonus() {
    if ($transaction = $this->businessTransactionRepo->getUnprocessedTransaction()) {
      $trxTime = strtotime($transaction->transaction_date);
      if ($this->businessBonusRepo->findByTransactionId($transaction->id)) {
        $transaction->is_processed = true;
        $this->businessTransactionRepo->save($transaction);
        return;
      }
      foreach ($this->businessResellerRepo->getByUserId($transaction->user_id) as $reseller) {
        $resellerFeeType = '';
        $resellerFeeValue = 0;
        $resellerFee = 0;
        $groupDetailId = null;
        if ($group = $this->businessResellerGroupRepo->findByType($reseller->id, $reseller->template_id, $transaction->type, $transaction->payment_group_id)) {
          foreach ($this->businessResellerGroupDetailRepo->getByGroupId($group->id, $reseller->id) as $item) {
            if (($item->campaign_start_date != null && $trxTime < strtotime($item->campaign_start_date))
              || ($item->campaign_end_date != null && $trxTime > strtotime($item->campaign_end_date))) continue;
            if ($item->minimal_type == ForBusiness::COMPARISON_QTY && $transaction->qty < $item->minimal_value) continue;
            else if ($item->minimal_type == ForBusiness::COMPARISON_FEE && $transaction->total_fee < $item->minimal_value) continue;
            else if ($item->minimal_type == ForBusiness::COMPARISON_TIER && ($transaction->total_fee/$transaction->qty < $item->minimal_value)) continue;
            $resellerFeeType = $item->fee_type;
            $resellerFeeValue = $item->fee_value;
            $groupDetailId = $item->id;
            break;
          }
        }
        if ($resellerFeeType == ForBusiness::FEE_PERCENTAGE)
          $resellerFee = round($resellerFeeValue / 100 * $transaction->total_fee);
        else if ($resellerFeeType == ForBusiness::FEE_BASE_VALUE)
          $resellerFee = $transaction->total_fee - ($resellerFeeValue * $transaction->qty);
        else if ($resellerFeeType == ForBusiness::DISCOUNT_VALUE) {
          $resellerFee = $resellerFeeValue * $transaction->qty;
          if ($resellerFee > $transaction->total_fee) $resellerFee = 0;
        }
        if ($resellerFee > 0) {
          $resellerBonus = $this->businessBonusRepo->getNew();
          $resellerBonus->reseller_user_id = $reseller->reseller_user_id;
          $resellerBonus->business_reseller_transaction_id = $transaction->id;
          $resellerBonus->business_reseller_group_detail_id = $groupDetailId;
          $resellerBonus->reseller_fee = $resellerFee;
          $resellerBonus->fee_type = $resellerFeeType;
          $resellerBonus->fee_value = $resellerFeeValue;
          $this->businessBonusRepo->save($resellerBonus);
        }
      }

      $salesBonus = $this->businessBonusRepo->getNew();
      $salesBonus->reseller_user_id = $this->salesUserId;
      $salesBonus->business_reseller_transaction_id = $transaction->id;
      $salesBonus->reseller_fee = round(0.2 * $transaction->total_fee);
      $salesBonus->fee_type = ForBusiness::FEE_PERCENTAGE;
      $salesBonus->fee_value = 20;
      $this->businessBonusRepo->save($salesBonus);

      $transaction->is_processed = true;
      $this->businessTransactionRepo->save($transaction);
    }
  }

  private function reloadMarketingBudget() {
    $this->cashInserter->add([
      'user_id' => $this->escrowUserId,
      'trx_type' => TransactionType::TRANSFER,
      'cash_type' => CashType::OCASH,
      'amount' => -50000000,
      'data' => json_encode([
        'notes' => 'Automatic Transfer for Reload Marketing Budget',
        'to_user_id' => $this->marketingUserId
      ])
    ]);

    $this->cashInserter->add([
      'user_id' => $this->marketingUserId,
      'trx_type' => TransactionType::TRANSFER,
      'cash_type' => CashType::OCASH,
      'amount' => 50000000,
      'data' => json_encode([
        'notes' => 'Automatic Transfer for Reload Marketing Budget',
        'from_user_id' => $this->escrowUserId
      ])
    ]);

    try {
      $this->cashInserter->run();
      $this->redis->hdel(ForBusiness::REDIS_BUSINESS_RESELLER, ForBusiness::REDIS_LOCK_TRANSFER_FROM_MARKETING);
      $this->redis->hset(ForBusiness::REDIS_BUSINESS_RESELLER, ForBusiness::REDIS_LOCK_TRANSFER_BONUS, 1);
    }
    catch (InsufficientFundException $e) {
      $noticer = app()->make(InternalNoticer::class);
      $noticer->saveMessage('Escrow gagal melakukan reload untuk budget marketing (reseller Business).');
    }
  }

  private function transferBonus() {
    $ids = [];
    foreach ($this->businessBonusRepo->getUnsent() as $item) {
      $ids[] = $item->id;
      $this->cashInserter->add($this->setBonus($item, $item->transaction, true));
      $this->cashInserter->add($this->setBonus($item, $item->transaction));
    }

    try {
      if (count($ids) > 0) {
        $this->cashInserter->run();
        $this->businessBonusRepo->setSent($ids);
      }
      else {
        $this->redis->hdel(ForBusiness::REDIS_BUSINESS_RESELLER, ForBusiness::REDIS_LOCK_TRANSFER_BONUS);
        $this->redis->hset(ForBusiness::REDIS_BUSINESS_RESELLER, ForBusiness::REDIS_LOCK_TRANSFER_CAPITAL, 1);
      }
    }
    catch (InsufficientFundException $e) {
      $this->redis->hdel(ForBusiness::REDIS_BUSINESS_RESELLER, ForBusiness::REDIS_LOCK_TRANSFER_BONUS);
      $this->redis->hset(ForBusiness::REDIS_BUSINESS_RESELLER, ForBusiness::REDIS_LOCK_TRANSFER_FROM_MARKETING, 1);
    }
  }

  private function transferToCapital() {
    $this->cashInserter->clear();
    foreach ($this->userCashRepo->getBusinessPrivateCash() as $item) {
      $this->cashInserter->add([
        'user_id' => $item->id,
        'trx_type' => TransactionType::TRANSFER,
        'cash_type' => CashType::OCASH,
        'amount' => -$item->amount,
        'data' => json_encode([
          'notes' => 'Withdraw oCash',
          'to_user_id' => $this->capitalUserId
        ])
      ]);
      $this->cashInserter->add([
        'user_id' => $this->capitalUserId,
        'trx_type' => TransactionType::TRANSFER,
        'cash_type' => CashType::OCASH,
        'amount' => $item->amount,
        'data' => json_encode([
          'notes' => 'Withdraw oCash',
          'from' => $item->name,
          'from_user_id' => $item->id
        ])
      ]);
    }
    $this->cashInserter->run();
    $this->redis->hdel(ForBusiness::REDIS_BUSINESS_RESELLER, ForBusiness::REDIS_LOCK_TRANSFER_CAPITAL);
  }
}
