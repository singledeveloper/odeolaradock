<?php
/**
 * Created by PhpStorm.
 * User: mrp130
 * Date: 10/31/2017
 * Time: 7:11 PM
 */

namespace Odeo\Domains\Marketing\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Subscription\Repository\StoreRepository;
use Odeo\Domains\Transaction\Repository\DepositRecordRepository;

class LastDepositRecorder {

  public function __construct() {
    $this->stores = app()->make(StoreRepository::class);
    $this->records = app()->make(DepositRecordRepository::class);
  }

  public function run() {

    $now = Carbon::now();

    $storeCashes = $this->stores->getModel()
      ->join('user_cashes', 'user_cashes.store_id', '=', 'stores.id')
      ->whereIn('status', StoreStatus::ACTIVE)
      ->where('plan_id', '<>', Plan::FREE)
      ->where('user_cashes.cash_type', 'deposit')
      ->selectRaw("store_id, amount")
      ->get();


    $data = array_map(function ($data) use ($now) {

      $data['updated_at'] = $data['created_at'] = $now;

      return $data;

    }, $storeCashes->toArray());


    $this->records->saveBulk($data);
  }

}