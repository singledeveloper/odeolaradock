<?php

namespace Odeo\Domains\Marketing\Scheduler;

use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Marketing\Jobs\PopulateRevenueQueue;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;

class StoreRevenueRefresher {

  public function run() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(__CLASS__, 'resetQueueId'));
    $pipeline->execute([]);
  }

  public function refresh() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(__CLASS__, 'refreshRevenue'));
    $pipeline->execute([]);
  }

  public function refreshRevenue(PipelineListener $listener, $data) {
    $listener->pushQueue(new PopulateRevenueQueue());
    $listener->response(200);
  }

  public function resetQueueId(PipelineListener $listener, $data) {
    Redis::pipeline(function($pipe) {
      $pipe->del('odeo_core:store_profit_idx');
      $pipe->del('odeo_core:store_failed_order_count');
    });
    $listener->response(200);
  }
}
