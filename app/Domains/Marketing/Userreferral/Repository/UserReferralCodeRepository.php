<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/3/17
 * Time: 12:00 AM
 */

namespace Odeo\Domains\Marketing\Userreferral\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Userreferral\Model\UserReferralCode;

class UserReferralCodeRepository extends Repository {

  public function __construct(UserReferralCode $userReferralCode) {
    $this->model = $userReferralCode;
  }

  public function get() {
    $filters = $this->getFilters();

    $query = $this->getCloneModel();
    
    if ($this->hasExpand('user')) $query = $query->with('user');

    if (isset($filters['store_id'])) {
      $query->where('store_referred_id', $filters['store_id']);
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function findByUserId($userId) {
    return $this->model->where('user_id', $userId)->first();
  }

  public function getUserReferredIdByDate($userId, $date) {
    return $this->model
      ->where('user_referred_id', $userId)
      ->whereDate('created_at', '=', $date)->get();
  }

  public function getIdentical($userId, $userReferredId) {
    return $this->model
      ->where('user_id', $userId)
      ->where('user_referred_id', $userReferredId)
      ->get();
  }
}