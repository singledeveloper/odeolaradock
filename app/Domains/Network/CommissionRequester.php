<?php

namespace Odeo\Domains\Network;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;

class CommissionRequester {

  public function __construct() {
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->treeParentManager = app()->make(\Odeo\Domains\Network\Helper\ParentManager::class);
    $this->treeChildManager = app()->make(\Odeo\Domains\Network\Helper\ChildManager::class);
    $this->plan = app()->make(\Odeo\Domains\Subscription\Repository\PlanRepository::class);
    $this->planManager = app()->make(\Odeo\Domains\Subscription\Helper\PlanManager::class);
    $this->commission = app()->make(\Odeo\Domains\Network\Repository\TeamCommissionRepository::class);
    $this->inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
  }

  private $default_first_amount = 200000;
  private $default_second_amount = 50000;

  public function create(PipelineListener $listener, $data) {
    $parents = $this->treeParentManager->get_me_and_parents($data["store_id"]);

    $maximum_rules = $this->plan->findMostExpensive();
    $maximum_pairs_can_be_achieved = $maximum_rules->max_pairing;
    $maximum_potentials_can_be_achieved = $maximum_rules->max_pairing_potential;

    $upgrade_datetime = isset($data["upgrade_request_datetime"]) ? $data["upgrade_request_datetime"] : date("Y-m-d H:i:s", time());

    $now = date("Y-m-d H:i:s", time());
    $now_day = date("Y-m-d", time());

    $plans = $this->planManager->getPlans();

    foreach ($parents as $item) {
      if (!$item->is_network_enable) continue;

      $info = $this->treeChildManager->get_child_info($item->store_id);

      $rules = $plans[$item->store->plan->id];
      $sv = floor(($info->left_branch->point > $info->right_branch->point ? $info->right_branch->point : $info->left_branch->point) / 100);

      $recorded_sv = $this->commission->getRecordedSV($item->store_id);
      $new_sv = $sv - $recorded_sv;

      $max_pairs = $rules->max_pairing;
      $max_potential = $rules->max_pairing_potential;

      $total_pairs = 0;
      $total_potential = 0;
      $amount_get = 0;

      $today_sv = $this->commission->findTodaySV($item->store_id, $upgrade_datetime);
      $incompleted_pairs = 0;
      $total_unclaimed_potential = 0;

      foreach ($today_sv as $output) {
        if ($output->first_amount_claimed_at != NULL && $output->second_amount_claimed_at != NULL)
          $total_pairs++;

        if ($output->first_amount_claimed_at != NULL)
          $total_potential += $output->first_amount;

        if ($output->second_amount_claimed_at != NULL)
          $total_potential += $output->second_amount;
      }

      foreach ($today_sv as $output) {
        if ($output->first_amount_claimed_at != NULL && $output->second_amount_claimed_at != NULL) {
          continue;
        }
        if ($max_potential > $total_potential) {
          if ($max_pairs > $total_pairs) {
            if ($output->first_amount_claimed_at == NULL) {
              $output->first_amount_claimed_at = $now;
              $amount_get += $output->first_amount;
              $total_potential += $output->first_amount;
            }
            $total_pairs++;
          } else {
            if ($total_pairs + $incompleted_pairs < $maximum_pairs_can_be_achieved) {
              $incompleted_pairs++;
              if ($total_potential + $total_unclaimed_potential < $maximum_potentials_can_be_achieved) {
                $total_unclaimed_potential += $output->first_amount;
              }
            }
          }

          if ($output->second_amount_claimed_at == NULL) {
            $output->second_amount_claimed_at = $now;
            $amount_get += $output->second_amount;
            $total_potential += $output->second_amount;
          }

          $this->commission->save($output);
        } else if ($max_potential <= $total_potential || $max_potential == 0) {
          if ($total_pairs + $incompleted_pairs < $maximum_pairs_can_be_achieved) {
            $incompleted_pairs++;
            if ($total_potential + $total_unclaimed_potential < $maximum_potentials_can_be_achieved) {
              $total_unclaimed_potential += $output->first_amount;
            }
          }

          if ($output->second_amount_claimed_at == NULL && $total_potential + $total_unclaimed_potential < $maximum_potentials_can_be_achieved) {
            $total_unclaimed_potential += $output->second_amount;
          }
        }
      }

      for ($x = 0; $x < $new_sv; $x++) {
        $insert["store_id"] = $item->store_id;
        $insert["first_amount"] = $this->default_first_amount;
        $insert["second_amount"] = $this->default_second_amount;
        $insert["day_limit"] = $now_day;
        $insert["created_at"] = $now;
        $insert["updated_at"] = $now;
        $insert["first_amount_claimed_at"] = NULL;
        $insert["second_amount_claimed_at"] = NULL;

        if ($max_potential > $total_potential) {
          $insert["second_amount_claimed_at"] = $now;
          $amount_get += $insert["second_amount"];
          $total_potential += $insert["second_amount"];

          if ($max_pairs > $total_pairs) {
            $insert["first_amount_claimed_at"] = $now;
            $amount_get += $insert["first_amount"];
            $total_potential += $insert["first_amount"];
            $total_pairs++;
          } else {
            if ($total_pairs + $incompleted_pairs < $maximum_pairs_can_be_achieved) {
              $incompleted_pairs++;
              if ($total_potential + $total_unclaimed_potential < $maximum_potentials_can_be_achieved) {
                $total_unclaimed_potential += $insert["first_amount"];
              }
            }
          }
        } else if ($max_potential <= $total_potential || $max_potential == 0) {
          if ($total_pairs + $incompleted_pairs < $maximum_pairs_can_be_achieved) {
            $incompleted_pairs++;
            if ($total_potential + $total_unclaimed_potential < $maximum_potentials_can_be_achieved) {
              $total_unclaimed_potential += $insert["first_amount"];
            }
          }
          if ($total_potential + $total_unclaimed_potential < $maximum_potentials_can_be_achieved) {
            $total_unclaimed_potential += $insert["second_amount"];
          }
        }

        $this->commission->save($insert);
      }

      if ($incompleted_pairs > 0 || $amount_get > 0) {
        $owner = $this->store->findOwner($item->store_id);
      }

      if ($incompleted_pairs > 0) {
        $suggest = $this->planManager->getSuggestionPotential($total_potential + $total_unclaimed_potential, $max_potential, $total_pairs + $incompleted_pairs, $max_pairs, $plans);
        if ($suggest["is_needed"] || $item->plan_id != $maximum_rules->id) {
          $this->notification->setup($owner->user_id, NotificationType::WARNING, NotificationGroup::TRANSACTION);
          $this->notification->team_commission_incompleted($item->store_id, $item->store->name, $total_unclaimed_potential, isset($suggest["plan_name"]) ? $suggest["plan_name"] : $maximum_rules->name, $now_day . " 23:59:59");
        }
      }
      if ($amount_get > 0) {
        $this->notification->setup($owner->user_id, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
        $this->notification->team_commission($amount_get, $item->store->name);

        $this->inserter->add([
          'user_id' => $owner->user_id,
          'store_id' => $item->store_id,
          'trx_type' => TransactionType::TEAM_COMMISSION,
          'cash_type' => CashType::ODEPOSIT,
          'amount' => ($amount_get / 2),
          'data' => json_encode(['recent_desc' => $item->store->name])
        ]);

        $this->inserter->add([
          'user_id' => $owner->user_id,
          'trx_type' => TransactionType::TEAM_COMMISSION,
          'cash_type' => CashType::OCASH,
          'amount' => ($amount_get / 2)
        ]);
      }
    }

    $this->inserter->run();

    $listener->pushQueue($this->notification->queue());

    return $listener->response(200);
  }
}
