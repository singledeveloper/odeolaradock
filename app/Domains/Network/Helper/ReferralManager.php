<?php

namespace Odeo\Domains\Network\Helper;
use Odeo\Domains\Constant\ReferralConfig;

class ReferralManager {
  
  public function __construct() {
    $this->referral = app()->make(\Odeo\Domains\Network\Repository\ReferralRepository::class);
    $this->referralDetail = app()->make(\Odeo\Domains\Network\Repository\ReferralDetailRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }
  
  function getCurrentReferral($referredStoreIds) {
    if (!is_array($referredStoreIds)) $referredStoreIds = [$referredStoreIds];
    
    $claimedAmounts = $this->referral->findClaimed($referredStoreIds);
    $referredStores = $this->referralDetail->findByReferredStoreId($referredStoreIds);
    
    $totalReferral = [];
    foreach ($referredStores as $item) {
      if (!isset($totalReferral[$item->referred_store_id])) $totalReferral[$item->referred_store_id] = 0;
      $totalReferral[$item->referred_store_id] += $item->plan->sales_volume;
    }
    
    $referrals = [];
    foreach ($referredStoreIds as $item) {
      if (!isset($claimedAmounts[$item])) $claimedAmounts[$item] = 0;
      if (!isset($totalReferral[$item])) $totalReferral[$item] = 0;
      
      $ref["formatted_gift_value"] = $this->currency->getFormattedOnly(ReferralConfig::VALUE);
      $ref["current_value"] = intval($totalReferral[$item] - $claimedAmounts[$item]);
      $ref["current_limit"] = ReferralConfig::AMOUNT_LIMIT;
      $ref["formatted_current_value"] = str_replace(" ", "", formatPoint($ref["current_value"]) . "/" . formatPoint(ReferralConfig::AMOUNT_LIMIT));
      
      $ref["can_claim"] = $ref["current_value"] >= ReferralConfig::AMOUNT_LIMIT;
      $points_need_to_claim = ReferralConfig::AMOUNT_LIMIT - $ref["current_value"];
      $ref["points_need_to_claim"] = ($points_need_to_claim > 0) ? $points_need_to_claim : 0;
      
      $referrals[$item] = $ref;
    }
    
    return $referrals;
  }
}
