<?php

namespace Odeo\Domains\Network\Jobs;

use Odeo\Jobs\Job;

class UpdateNetwork extends Job  {

  protected $storeId, $tree;

  public function __construct($storeId) {
    parent::__construct();
    $this->tree = app()->make(\Odeo\Domains\Network\Repository\TreeRepository::class);
    $this->storeId = $storeId;
  }

  public function handle() {
    $treeNode = $this->tree->findByStoreId($this->storeId);
    $parentNode = $this->tree->findByStoreId($treeNode->referred_store_id);

    if($parentNode) {
      $parentNode->hustler_store_ids .= ($parentNode->hustler_store_ids == '' ? '' : ',') . $this->storeId;
      $parentNode->save();
      if($parentNode->referred_store_id) {
        $grandparentNode = $this->tree->findByStoreId($parentNode->referred_store_id);
        $grandparentNode->grand_hustler_store_ids .= ($grandparentNode->grand_hustler_store_ids == '' ? '' : ',') . $this->storeId;
        $grandparentNode->save();

        if($grandparentNode->referred_store_ids) {
          $updatedNodes = $this->tree->getCloneModel()->whereIn('store_id', array_slice(explode(',', $grandparentNode->referred_store_ids), 0, 2))->get();
          foreach($updatedNodes as $updated) {
            $updated->team_store_ids .= ($updated->team_store_ids == '' ? '' : ',') . $this->storeId;
            $updated->save();
          }
        }
      }
    }

    $parentNodes = array_merge([$parentNode->store_id], $parentNode->referred_store_ids != '' ? array_slice(explode(',', $parentNode->referred_store_ids), 0, 3) : []);

    $treeNode->referred_store_ids = implode($parentNodes, ',');
    $treeNode->save();
  }
}
