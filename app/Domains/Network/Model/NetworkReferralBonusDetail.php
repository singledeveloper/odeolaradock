<?php

namespace Odeo\Domains\Network\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Plan;
use Odeo\Domains\Subscription\Model\Store;

class NetworkReferralBonusDetail extends Entity
{
    
    public $timestamps = false;
    
    public function plan() {
      return $this->belongsTo(Plan::class);
    }

    public function referredStore() {
      return $this->belongsTo(Store::class, 'referred_store_id');
    }

    public static function boot() {
      parent::boot();

      static::creating(function($model) {
        $timestamp = $model->freshTimestamp();
        $model->setCreatedAt($timestamp);
      });
    }
}
