<?php

namespace Odeo\Domains\Network\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Network\Model\NetworkStatus;

class NetworkStatusRepository extends Repository {

  public function __construct(NetworkStatus $status) {
    $this->model = $status;
  }
  
  public function valid($userId) {
    if ($result = $this->model->where("user_id", $userId)->first()) {
      return $result->is_network_enable;
    }
    return false;
  }
}
