<?php

namespace Odeo\Domains\Network\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Network\Model\NetworkReferralBonus;

class ReferralRepository extends Repository {

  public function __construct(NetworkReferralBonus $ref) {
    $this->model = $ref;
  }

  public function findClaimed($referredStoreIds) {
    $query = $this->model->groupBy('referred_store_id')->selectRaw('sum(amount) as total_amount, referred_store_id');

    if (is_array($referredStoreIds)) $query = $query->whereIn("referred_store_id", $referredStoreIds);
    else $query = $query->where("referred_store_id", $referredStoreIds);

    return $query->pluck('total_amount', 'referred_store_id');
  }
}
