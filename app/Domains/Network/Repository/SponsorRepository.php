<?php

namespace Odeo\Domains\Network\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Network\Model\NetworkSponsorBonus;

class SponsorRepository extends Repository {

  public function __construct(NetworkSponsorBonus $sponsor) {
    $this->model = $sponsor;
  }
  
  public function getLeftover($storeId, $minimalPlanId, $dayLimit) {
    return $this->model->selectRaw("sum(amount - claimed_amount) as total")->where("sponsor_store_id", $storeId)
      ->where("minimal_required_plan_id", "<=", $minimalPlanId)
      ->whereNotNull("minimal_required_plan_id")
      ->where("day_limit", $dayLimit)->first();
  }
  
  public function getAllLeftover($storeId, $upgradeDatetime) {
    return $this->model->with('plan')->where("sponsor_store_id", $storeId)
      ->whereNotNull("minimal_required_plan_id")
      ->where("day_limit", date("Y-m-d", strtotime($upgradeDatetime)))
      ->orderBy("minimal_required_plan_id", "ASC")->get();
  }
}
