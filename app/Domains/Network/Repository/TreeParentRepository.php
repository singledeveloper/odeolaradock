<?php

namespace Odeo\Domains\Network\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Network\Model\NetworkTreeParent;

class TreeParentRepository extends Repository {

  public function __construct(NetworkTreeParent $parent) {
    $this->model = $parent;
  }
  
  public function findByStoreId($storeId) {
    return $this->model->where("store_id", $storeId)->first();
  }
}
