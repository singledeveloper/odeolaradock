<?php

namespace Odeo\Domains\Network\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Network\Model\NetworkTree;
use Odeo\Domains\Constant\StoreStatus;

class TreeRepository extends Repository {

  public function __construct(NetworkTree $tree) {
    $this->model = $tree;
  }

  public function findByStoreId($storeId) {
    return $this->model->where("store_id", $storeId)->first();
  }

  public function findByStoreIds($storeIds, $with = []) {
    $query = $this->model;
    if (count($with) > 0) $query = $query->with($with);
    if (!is_array($storeIds)) $storeIds = [$storeIds];
    return $query->whereIn("store_id", $storeIds)->get();
  }

  public function findByParent($parentStoreId, $with = []) {
    $query = $this->model;
    if (count($with) > 0) $query = $query->with($with);
    return $query->where("parent_store_id", $parentStoreId)->get();
  }

  public function checkUpline($storeId) {
    return $this->model->where("parent_store_id", $storeId)->orWhere("referred_store_id", $storeId)->first();
  }

  public function checkDirectDownline($referredStoreId, $storeId) {
    return $this->model->where("referred_store_id", $referredStoreId)->where("store_id", $storeId)->first();
  }

  public function getReferral() {
    $filters = $this->getFilters();
    $query = $this->model->with('plan')->join("stores", 'stores.id', '=', 'network_trees.store_id')
      ->where("referred_store_id", $filters["store_id"])->whereIn("stores.status", StoreStatus::ACTIVE);

    if (!isset($filters["skip_parent_check"])) $query = $query->where("parent_store_id", NULL);

    return $this->getResult($query);
  }

  public function getStoreRoles($storeIds) {
    $query = $this->model;
    if (!is_array($storeIds)) $storeIds = [$storeIds];
    return $query->select(\DB::raw("store_id, true as is_hustler,
      hustler_store_ids <> '' as is_mentor, grand_hustler_store_ids <> '' as is_leader"))
      ->whereIn("store_id", $storeIds)->get();
  }
}
