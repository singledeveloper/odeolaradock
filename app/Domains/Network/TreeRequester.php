<?php

namespace Odeo\Domains\Network;

use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Network\Jobs\UpdateNetwork;

class TreeRequester {

  public function __construct() {
    $this->tree = app()->make(\Odeo\Domains\Network\Repository\TreeRepository::class);
    $this->treeStatus = app()->make(\Odeo\Domains\Network\Repository\NetworkStatusRepository::class);
    $this->treeChildManager = app()->make(\Odeo\Domains\Network\Helper\ChildManager::class);
    $this->treeChildInserter = app()->make(\Odeo\Domains\Network\Helper\ChildInserter::class);
    $this->treeManager = app()->make(\Odeo\Domains\Network\Helper\TreeManager::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
  }

  public function update(PipelineListener $listener, $data) {
    $response = [];

    if (!$network = $this->tree->findByStoreId($data["store_id"]))  {
      $network = $this->tree->getNew();
      $network->store_id = $data["store_id"];
    }

    if (isset($data['referred_store_id']) && $network->referred_store_id == NULL) {
      $network->referred_store_id = $data['referred_store_id'];

      $referredOwner = $this->store->findOwner($network->referred_store_id);
      $this->notification->setup($referredOwner->user_id, NotificationType::ALPHABET, NotificationGroup::ACCOUNT);
      $store = $network->store;
      $this->notification->invitation_code($network->referred_store_id, $store->name, $network->referredStore->name, $store->plan->name);
      $listener->pushQueue($this->notification->queue());
      $listener->pushQueue(new UpdateNetwork(
        $data['store_id']
      ));
    }

    if (isset($data["network_switch"])) {
      $network->is_network_enable = ($network->is_network_enable) ? "0" : "1";
      $response["is_network_enable"] = $network->is_network_enable;
    }

    $this->tree->save($network);

    return $listener->response(200, $response);
  }

  public function checkAutoAssign(PipelineListener $listener, $data) {
    $network = $this->tree->findByStoreId($data["store_id"]);

    if ($network->referred_store_id != NULL && $network->parent_store_id == NULL) {
      $referred_network = $this->tree->findByStoreId($network->referred_store_id);
      $referredOwner = $this->store->findOwner($network->referred_store_id);
      if (!$referred_network->is_network_enable || !$this->treeStatus->valid($referredOwner->user_id)) {
        $network->parent_store_id = $this->treeChildManager->get_deepest_child_id($network->referred_store_id);
        $network->position = "0";
        $this->tree->save($network);
      }
    }

    return $listener->response(200);
  }

  public function assign(PipelineListener $listener, $data) {
    $network = $this->tree->findByStoreId($data["store_id"]);

    if (!StoreStatus::isActive($network->store->status)) {
      return $listener->response(400, "You can't assign a pending store.");
    }
    if ($network->position != NULL) {
      return $listener->response(400, "You have assigned this store.");
    }

    $referred_network = $this->tree->findByStoreId($network->referred_store_id);
    $referredOwner = $this->store->findOwner($network->referred_store_id);
    if ($data["assigned_store_id"] != $network->referred_store_id || !$referred_network->is_network_enable || !$this->treeStatus->valid($referredOwner->user_id)) {
      return $listener->response(400, "You don't have credential to assign this store.");
    }

    $network->parent_store_id = $data['parent_store_id'];

    $position = isset($data["position"]) ? $data["position"] : "";
    list($error, $available_position) = $this->treeManager->check_position($position, $data["parent_store_id"]);
    if ($error) return $listener->response(400, $error);

    if (isset($data["position"])) $network->position = $data["position"];
    else $network->position = $available_position;

    $this->tree->save($network);

    $this->treeChildInserter->check($network);

    return $listener->response(200, [
      "id" => $network->id,
      "store_id" => $network->store_id,
      "referred_store_id" => $network->referred_store_id,
      "parent_store_id" => $network->parent_store_id,
      "position" => $network->position
    ]);
  }

  public function visualize() {
    $activeStores = $this->store->getAllActiveStore()->pluck('name', 'id')->toArray();

    $nodes = $this->tree->getModel()->whereIn('store_id', array_keys($activeStores))->orderBy('id', 'desc')->get()->toArray();

    $nodeArrays = array();
    $tempNodes = array();

    while(count($nodes)) {
      $node = array_pop($nodes);

      if(!isset($nodeArrays[$node['store_id']])) {
        $temp = (object)[];
        $temp->store_id = $node['store_id'];
        $temp->store_name = $activeStores[$node['store_id']];
        $temp->children = [];
        $nodeArrays[$node['store_id']] = $temp;
        $tempNodes[$node['store_id']] = $temp;
      }
      if(!is_null($node['referred_store_id']) && isset($tempNodes[$node['referred_store_id']])) {
        $tempNodes[$node['referred_store_id']]->children[] = $tempNodes[$node['store_id']];
        unset($nodeArrays[$node['store_id']]);
      } else {
        if(!is_null($node['referred_store_id'])) array_unshift($nodes, $node);
      }
    }

    foreach($nodeArrays as $node) {
      $this->printText($node, 0, "", false);
    }
  }

  private function printText($node, $level, $prefix, $last) {
    if($level == 0) echo '<div style="font-family: monospace">' . $node->store_name . '</div>';
    else if($level == 1) echo '<div style="font-family: monospace">' . html_entity_decode(($last ? '&#x02514;' : '&#x0251C;') . '&#x02500;&nbsp;') . $node->store_name . '</div>';
    else echo '<div style="font-family: monospace">' . $prefix . html_entity_decode(($last ? '&#x02514;' : '&#x0251C;'). '&#x02500;&nbsp;') . $node->store_name . '</div>';
    if(!empty($node->children)) {
      foreach($node->children as $child) {
        $this->printText($child, $level + 1, $prefix . ($level > 0 ? ($last ? '&nbsp;&nbsp;&nbsp;' : html_entity_decode('&#x02502;&nbsp;&nbsp;')) : ''), $child === end($node->children));
      }
    }
  }
}
