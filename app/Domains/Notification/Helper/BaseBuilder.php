<?php

namespace Odeo\Domains\Notification\Helper;

use Odeo\Domains\Notification\Jobs\SaveNotification;
use Odeo\Domains\Constant\NotificationType;

class BaseBuilder {

  private $message = [];
  private $messages = [];
  private $userId = "";
  private $title = null;
  private $type = NotificationType::ALPHABET;
  private $group = null;
  private $pushOnly = false;

  public function setup($userId, $type = NotificationType::ALPHABET, $group = null) {
    $this->userId = $userId;
    $this->type = $type;
    $this->group = $group;
  }

  public function get() {
    return $this->messages;
  }

  public function pushOnly() {
    $this->pushOnly = true;
  }

  public function queue() {
    return new SaveNotification($this->messages, $this->pushOnly);
  }

  public function title($title) {
    $this->title = $title;
  }

  protected function plain($message) {
    $data["type"] = "plainText";
    $data["text"] = $message;
    $this->message[] = $data;
  }

  protected function attributed($message, $color) {
    $data["type"] = "attributedText";
    $data["color"] = $color;
    $data["text"] = $message;
    $this->message[] = $data;
  }

  protected function link($layer, $id = "") {
    $data["type"] = "link";
    $data["layer"] = $layer;
    if ($id != "") {
      $data["id"] = $id;
    }
    $this->message[] = $data;
  }

  protected function button($message, $layer, $id) {
    $data["type"] = "button";
    $data["layer"] = $layer;
    if ($id != "") {
      $data["id"] = intval($id);
    }
    $data["text"] = $message;
    $this->message[] = $data;
  }

  protected function countdown($limit = "") {
    $data["type"] = "countdown";
    $data["limit"] = ($limit == "") ? time() : strtotime($limit);
    $this->message[] = $data;
  }

  protected function convert($desc) {
    $data["type"] = "convert";
    $data["desc"] = $desc;
    $this->message[] = $data;
  }

  protected function eof() {
    if ($this->userId != "") {
      $this->messages[] = [
        "user_id" => $this->userId,
        "type" => $this->type,
        "group" => $this->group,
        "title" => $this->title,
        "description" => json_encode($this->message)
      ];
    }
    $this->message = [];
  }

}
