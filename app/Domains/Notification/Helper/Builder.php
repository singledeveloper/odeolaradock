<?php

namespace Odeo\Domains\Notification\Helper;

use Odeo\Domains\Account\Helper\UserKtpReasonGenerator;
use Odeo\Domains\Constant\ReferralConfig;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Constant\AssetAws;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Transaction\Helper\Currency;

class Builder extends BaseBuilder {

  public function userKtpAccepted() {
    $this->title("KTP");
    $this->plain("Your KTP has been accepted");
    $this->link('ktp');
    $this->eof();
  }

  public function removeAgent($name) {
    $this->title("Agent");
    $this->plain($name . ' has removed you from their agency.');
    $this->eof();
  }

  public function agentRemoveMaster($name) {
    $this->title("Agent");
    $this->plain($name . ' has removed you as a master.');
    $this->eof();
  }

  public function googlePlaySoftDecline($orderId) {
    $this->title("Google Play");
    $this->plain("Your Google Play transaction");
    $this->attributed('#' . $orderId, "#FFFF00");
    $this->plain("has been failed and we have refunded the oCash. Please try again.");
    $this->link("refund_transaction", $orderId);
    $this->eof();
  }

  public function googlePlayHardDecline($orderId) {
    $this->title("Google Play");
    $this->plain("Your Google Play transaction");
    $this->attributed('#' . $orderId, "#FFFF00");
    $this->plain("has been failed and we have refunded the oCash. Please contact our customer service (cs@odeo.co.id) for futher information.");
    $this->link("refund_transaction", $orderId);
    $this->eof();
  }

  public function agentCancelRequestMaster($name) {
    $this->title("Agent");
    $this->plain($name . ' has cancelled the agent request.');
    $this->eof();
  }

  public function requestAgency($name) {
    $this->title("Agent Request");
    $this->plain($name . ' has requested to be your agent. Please approve within 2x24 hour');
    $this->link('agent_dashboard');
    $this->eof();
  }

  public function autoAcceptAgency($name) {
    $this->title("Agent");
    $this->plain($name . ' has added to your agent list. Start to manage your agent');
    $this->link('agent_dashboard');
    $this->eof();
  }

  public function requestExpired($masterCode) {
    $this->title("Request Expired");
    $this->plain('Your agent request with master code "' . $masterCode . '" has expired.');
    $this->eof();
  }

  public function agentAccepted($masterCode, $bonus = 0) {
    $this->title("Agent Request Accepted");
    $text = 'Your agent request is approved by master code "' . $masterCode . '".';
    if ($bonus > 0) {
      $text .= ' Rp' . number_format($bonus) . ' has been added to your oCash';
    }
    $this->plain($text);
    $this->link("recent_transaction");
    $this->eof();
  }

  public function agentNotReceiveMasterBonus($phoneNumber) {
    $this->title("Agent");
    $this->plain('agent (' . $phoneNumber . ') is not receiving your master bonus you have set, due to insufficient deposit.');
    $this->eof();
  }

  public function agentRejected($masterCode) {
    $this->title("Agent Request Rejected");
    $text = 'Your agent request has been rejected by master code "' . $masterCode . '"';
    $this->plain($text);
    $this->eof();
  }

  public function promoCashback($amount, $promoName) {
    $this->title("Promo Cashback");
    $this->plain("You get Rp" . number_format($amount) . " from " . $promoName);
    $this->link("recent_transaction");
    $this->eof();
  }

  public function userKtpRejected($nikStatus, $nameStatus, $ktpStatus, $ktpSelfieStatus, $lastOcashBalanceStatus, $lastTransactionDescriptionStatus) {
    $this->title("User Ktp Rejected");
    $reasonGenerator = app()->make(UserKtpReasonGenerator::class);
    $reasons = $reasonGenerator->getReasons($nikStatus, $nameStatus, $ktpStatus, $ktpSelfieStatus, $lastOcashBalanceStatus, $lastTransactionDescriptionStatus);

    $this->plain("Your KTP is rejected, with the following reasons:\n\n" . $reasons . "\n\nPlease recheck your KTP information and resubmit. Contact our Customer Services for further information.");
    $this->link('ktp');
    $this->eof();
  }

  public function refund($orderId, $description = '') {
    $this->title("Refund");
    $this->plain('Your order with ID ' . $orderId . ' has been refunded. ' . $description);
    $this->link("refund_transaction", $orderId);
    $this->eof();
  }

  public function pln_success($meter, $token) {
    $this->title("PLN Success");
    $this->plain('Token for PLN ID ' . $meter . ' is');
    $this->attributed($token, '#FFFF00');
    $this->eof();
  }

  public function order_bonus($amount, $store_name, $order_type) {
    $this->title("Order Bonus");
    $this->plain("You get Rp" . number_format($amount) . " " . ucwords(str_replace("_", " ", $order_type)) . " from");
    $this->attributed($store_name, "#FFFF00");
    $this->link("recent_transaction");
    $this->eof();
  }

  public function order_fail($store_name, $storeId) {
    $this->title("Order Failed");
    $this->plain("An order on");
    $this->attributed($store_name, "#FFFF00");
    $this->plain("failed because store doesn't have enough oDeposit");
    $this->link("store_detail", $storeId);
    $this->eof();
  }

  public function invitation_code($store_id, $client_store_name, $store_name, $plan_name) {
    $this->title("Invitation");
    $this->attributed($client_store_name, "#FFFF00");
    $this->plain("subscribed " . $plan_name . " plan with " . $store_name . " invitation code");
    $this->link("store_referral_detail", $store_id);
    $this->eof();
  }

  public function invitation_new($store_id, $store_name) {
    $this->title("Invitation");
    $this->attributed($store_name, "#FFFF00");
    $this->plain("get Rp200.000 oDeposit from using invitation code");
    $this->link("store_detail", $store_id);
    $this->eof();
  }

  public function shareUserReferralCode($amount) {
    $this->title("Referral Code");
    $this->plain("You get");
    $this->attributed("Rp" . number_format($amount), "#FFFF00");
    $this->plain("from sharing referral code.");
    $this->link("recent_transaction");
    $this->eof();
  }

  public function redeemUserReferralCode($amount) {
    $this->title("Referral Code");
    $this->plain("You get");
    $this->attributed("Rp" . number_format($amount), "#FFFF00");
    $this->plain("from referral code redeem.");
    $this->link("recent_transaction");
    $this->eof();
  }

  public function sponsor_bonus($amount, $store_name, $referred_store_name, $type) {
    $this->title("Sponsor Bonus");
    $this->plain($store_name . " earn");
    $this->attributed("Rp" . number_format($amount), "#FFFF00");
    $this->plain("from " . $referred_store_name . " subscription" . ($type == "upgrade" ? (" " . $type) : ""));
    $this->link("recent_transaction");
    $this->eof();
  }

  public function sponsor_bonus_from_upgrade($amount, $store_name) {
    $this->title("Sponsor Bonus");
    $this->plain("You claim");
    $this->attributed("Rp" . number_format($amount), "#FFFF00");
    $this->plain("bonus from " . $store_name . " subscription upgrade");
    $this->link("recent_transaction");
    $this->eof();
  }

  public function sponsor_bonus_leftover($amount_get, $amount, $store_id, $store_name, $client_store_name, $plan_id, $plan_name, $limit) {
    $this->title("Sponsor Bonus");
    $this->plain($store_name . " earn Rp" . number_format($amount_get) . " from");
    $this->attributed($client_store_name, "#FFFF00");
    $this->plain("subscription. Upgrade " . $store_name . " plan to");
    $this->attributed($plan_name, "#FFFF00");
    $this->plain("and claim your Rp" . number_format($amount));
    $this->link("recent_transaction");
    $this->button("Upgrade Now", "store_upgrade", $store_id);
    $this->countdown($limit);
    $this->convert("upgrade_store_id:" . $store_id);
    $this->convert("sponsor_bonus_store_with_plan_id:" . $store_id . $plan_id);
    $this->eof();
  }

  public function sponsor_bonus_leftover_still($amount, $store_id, $store_name, $plan_name, $limit) {
    $this->title("Sponsor Bonus");
    $this->plain("You still have Rp" . number_format($amount) . " unclaimed bonus from");
    $this->attributed($store_name, "#FFFF00");
    $this->plain(". Upgrade your plan to");
    $this->attributed($plan_name, "#FFFF00");
    $this->plain("and claim your bonus.");
    $this->button("Upgrade Now", "store_upgrade", $store_id);
    $this->countdown($limit);
    $this->convert("upgrade_store_id:" . $store_id);
    $this->convert("sponsor_bonus_store_id:" . $store_id);
    $this->eof();
  }

  public function referral($store_id, $store_name) {
    $this->title("Referral");
    $this->attributed($store_name, "#FFFF00");
    $this->plain("have earned a total of or more than " . (ReferralConfig::AMOUNT_LIMIT) . " oPoints. Enjoy your free " . (new Currency)->getFormattedOnly(ReferralConfig::VALUE) . " oDeposit!");
    $this->button("Claim Now", "claim_referral", $store_id);
    $this->convert("claim_referral:" . $store_id);
    $this->eof();
  }

  public function referral_left($store_id, $store_name, $client_store_name, $amount, $left) {
    $this->title("Referral");
    $this->plain($store_name . " have earned " . $amount . " oPoints from");
    $this->attributed($client_store_name, "#FFFF00");
    $this->plain("subcription. " . $store_name . " have " . ($left) . " oPoints remaining for " . (new Currency)->getFormattedOnly(ReferralConfig::VALUE) . " free oDeposit");
    $this->link("store_network", $store_id);
    $this->eof();
  }

  public function team_commission($amount, $store_name) {
    $this->title("Team Commission");
    $this->attributed($store_name, "#FFFF00");
    $this->plain("get");
    $this->attributed("Rp" . number_format($amount), "#FFFF00");
    $this->plain("Team Commission");
    $this->link("recent_transaction");
    $this->eof();
  }

  public function team_commission_incompleted($store_id, $store_name, $amount, $plan_name, $limit) {
    $this->title("Team Commission Incompleted");
    $this->attributed($store_name, "#FFFF00");
    $this->plain("has");
    $this->attributed("Rp" . number_format($amount), "#FFFF00");
    $this->plain("unclaimed team commission because " . $store_name . " have earn maximum daily bonus. Upgrade " . $store_name . " plan to");
    $this->attributed($plan_name, "#FFFF00");
    $this->plain("and earn more.");
    $this->button("Upgrade Now", "store_upgrade", $store_id);
    $this->countdown($limit);
    $this->convert("upgrade_store_id:" . $store_id);
    $this->convert("team_commission_store_id:" . $store_id);
    $this->eof();
  }

  public function deposit_request($amount, $store_name, $order_id) {
    $this->title("Deposit");
    $this->attributed("Rp" . number_format($amount), "#FFFF00");
    $this->plain("oDeposit has been added to");
    $this->attributed($store_name, "#FFFF00");
    $this->link("recent_transaction", $order_id);
    $this->eof();
  }

  public function transfer_in($id, $amount, $notes) {
    $this->title("Transfer");
    $this->plain("You get");
    $this->attributed("Rp" . number_format($amount), "#FFFF00");
    $this->plain("oCash Transfer.");
    if (trim($notes, "-") != "") {
      $this->plain("Notes: " . $notes);
    }
    $this->link("transfer_in", $id);
    $this->eof();
  }

  public function transfer_out($id, $amount, $notes) {
    $this->title("Transfer");
    $this->plain("You have successfully transferred");
    $this->attributed("Rp" . number_format($amount), "#FFFF00");
    $this->plain("oCash.");
    if (trim($notes, "-") != "") {
      $this->plain("Notes: " . $notes);
    }
    $this->link("transfer_out", $id);
    $this->eof();
  }

  public function payment_in($amount, $name) {
    $this->title("Payment Received");
    $this->plain("You get");
    $this->attributed("Rp" . number_format($amount), "#FFFF00");
    $this->plain("oCash payment at");
    $this->attributed($name, "#FFFF00");
    $this->link("recent_transaction");
    $this->eof();
  }

  public function payment_out($amount, $name) {
    $this->title("Payment Successful");
    $this->plain("You have successfully paid");
    $this->attributed("Rp" . number_format($amount), "#FFFF00");
    $this->plain("oCash to");
    $this->attributed($name, "#FFFF00");
    $this->link("recent_transaction");
    $this->eof();
  }

  public function new_invoice($invoiceName, $storeName, $invoiceNumber, $total) {
    $this->title("New Invoice");
    $this->plain("You have a new invoice");
    $this->attributed($invoiceNumber . " \"" . $invoiceName . "\"", "#FFFF00");
    $this->plain("with a total of");
    $this->attributed($total, "#FFFF00");
    $this->plain("from");
    $this->attributed($storeName, "#FFFF00");
    $this->link("user_invoice");
    $this->eof();
  }

  public function cashback($amount, $name) {
    $this->title("Cashback");
    $this->plain("You get");
    $this->attributed("Rp" . number_format($amount), "#FFFF00");
    $this->plain("cashback from your transaction at");
    $this->attributed($name, "#FFFF00");
    $this->link("recent_transaction");
    $this->eof();
  }

  public function withdraw($id, $amount, $to, $isBankTransfer) {
    $this->title($isBankTransfer ? "Bank Transfer" : "Withdraw");
    $this->plain("You have successfully " . ($isBankTransfer ? "transferred" : "withdrawn"));
    $this->attributed("Rp" . number_format($amount), "#FFFF00");
    $this->plain("oCash to " . $to . ".");
    $this->link("withdraw", $id);
    $this->eof();
  }

  public function withdraw_cancel($reasonText, $isBankTransfer) {
    $this->title($isBankTransfer ? "Bank Transfer" : "Withdraw");
    $this->plain("Your " . ($isBankTransfer ? "bank transfer" : "withdraw") . " request cannot be processed. " . $reasonText . ". Contact our Customer Services for further information.");
    $this->link("withdraw");
    $this->eof();
  }

  public function topup($orderId, $amount) {
    $this->title("Topup");
    $this->plain("Your Top Up oCash");
    $this->attributed("Rp" . number_format($amount), "#FFFF00");
    $this->plain("has been approved.");
    $this->link("recent_transaction", $orderId);
    $this->eof();
  }

  public function warranty($id, $store_name) {
    $this->title("Warranty");
    $this->plain("Your odeosafe+ for");
    $this->attributed($store_name, "#FFFF00");
    $this->plain("has been approved.");
    $this->link("store_warranty", $id);
    $this->eof();
  }

  public function mdPlus($id, $storeName, $month, $isNew) {
    $this->title("MD Plus");
    $this->plain("MD Plus for");
    $this->attributed($storeName, "#FFFF00");
    if ($isNew) {
      $this->plain("is now active for");
    } else {
      $this->plain("has been renewed for");
    }
    $this->plain($month . " month" . ($month > 1 ? "s." : "."));
    $this->link("store_md_plus", $id);
    $this->eof();
  }

  public function mdPlusReferralBonus($bonus, $userName, $month) {
    $this->title("MD Plus Referral Bonus");
    $this->plain("You get");
    $this->attributed($bonus, "#FFFF00");
    $this->plain("because your agent");
    $this->attributed($userName, "#FFFF00");
    $this->plain('has subscribed MD Plus for ' . $month . " month" . ($month > 1 ? "s." : "."));
    $this->eof();
  }

  public function store_alter($id, $name, $plan_name, $status = null) {
    $this->title("Store");
    if ($status == StoreStatus::PENDING) {
      $this->plain("Your new store");
      $this->attributed($name, "#FFFF00");
      $this->plain("has been approved. Start managing your store and earn money");
    } else if ($status == StoreStatus::WAIT_FOR_UPGRADE_VERIFY) {
      $this->attributed($name, "#FFFF00");
      $this->plain("has been upgraded to " . $plan_name . " Plan");
    } else if ($status == StoreStatus::WAIT_FOR_RENEWAL_VERIFY || $status == StoreStatus::WAIT_FOR_EXP_RENEWAL_VERIFY) {
      $this->plain("Your store");
      $this->attributed($name, "#FFFF00");
      $this->plain("has been renewed.");
    }
    $this->link("store_detail", $id);
    $this->eof();
  }


  public function inventoryPricingUpdate($storeName, $serviceName, $storeId, $date) {
    $this->title("Inventory Pricing Update");
    $this->attributed($storeName, "#FFFF00");
    $this->plain("has just updated their " . $serviceName . ' price.');
    $this->plain('pricing will be reflected on ' . $date);
    $this->plain('Go to manage inventory menu for more pricing information.');
    $this->link("store_inventory", $storeId);
    $this->eof();
  }

  public function agentPricingUpdate($storeName, $serviceName, $date) {
    $this->title("Agent Pricing Update");
    $this->plain('Your master');
    $this->attributed($storeName, "#FFFF00");
    $this->plain("has just updated their " . $serviceName . ' price.');
    $this->plain('pricing will be reflected on ' . $date);
    $this->eof();
  }

  public function report($text) {
    $this->title("Report");
    $this->plain($text);
    $this->eof();
  }

  public function billReminder($billReminder) {
    $this->title("Bill Reminder");
    $this->plain('Remember to pay your ' . $billReminder->name . ' bill today!');
    $this->link('bill_reminder');
    $this->convert("service_detail_id:" . $billReminder->service_detail_id);
    $this->convert("number:" . $billReminder->number);
    $this->convert("pulsa_odeo_id:" . $billReminder->pulsa_odeo_id);
    $this->convert("pulsa_odeo_name:" . $billReminder->pulsa_odeo_name);
    if ($billReminder->pulsa_odeo_type) {
      $this->convert("pulsa_odeo_type:" . strtolower(PostpaidType::getConstKeyByValue($billReminder->pulsa_odeo_type)));
    }
    if ($billReminder->pulsa_odeo_logo) {
      $this->convert("pulsa_odeo_logo:" . AssetAws::getAssetPath(AssetAws::PPOB_LOGO, $billReminder->pulsa_odeo_logo));
    }
    $this->eof();
  }

  public function gojekSupplierWarning() {
    $this->title("Gojek Supplier");
    $this->plain("SIAP-SIAP! User ODEO menambahkan OCASH untuk transaksi.");
    $this->eof();
  }

  public function gojekSupplierNoAccount() {
    $this->title("Gojek Supplier");
    $this->plain("Anda mulai kehilangan sales karena tidak ada akun.");
    $this->eof();
  }

  public function gojekLogout($number) {
    $this->title("Gojek Supplier");
    $this->plain("Akun Gojek " . $number . " ter-logout dari sistem. Mohon cek kembali.");
    $this->eof();
  }

  public function oCashAlmostSpentOutWarning($amount) {
    $this->title("ODEO oCash Warning");
    $this->plain("oCash Anda tersisa " . $amount . ". Segera topup agar tidak kehabisan deposit");
    $this->eof();
  }
}
