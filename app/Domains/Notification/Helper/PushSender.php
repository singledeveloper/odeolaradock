<?php

namespace Odeo\Domains\Notification\Helper;

use GuzzleHttp\Client;
use Odeo\Domains\Account\Repository\UserFcmTokenRepository;
use Odeo\Domains\Constant\NotificationConfig;

class PushSender {

  private $headers = [
    'Content-Type' => NotificationConfig::CONTENT_TYPE,
    'Authorization' => NotificationConfig::AUTHORIZATION
  ];

  public function __construct() {
    $this->userFcm = app()->make(UserFcmTokenRepository::class);
  }

  public function toUser($userId, $message) {
    $tokens = $this->userFcm->findByUserId($userId);
    $payload = $deviceTokens = [];

    if (!empty($tokens)) {
      foreach ($tokens as $item) {
        $deviceTokens[] = $item->device_token;
      }
    }

    if (sizeof($deviceTokens) > 0) {
      if (sizeof($deviceTokens) == 1) $deviceTokens = $deviceTokens[0];

      $client = new Client([
        'headers' => $this->headers,
      ]);

      $payload['priority'] = 'high';
      $payload['notification']['title'] = $message['title'] ?? 'ODEO';
      $payload['notification']['body'] = $message['message'] ?? $message;
      $payload['notification']['sound'] = 'default';

      if (isset($message['type'])) {
        $payload['notification']['tag'] = $message['type'];
        $payload['data']['type'] = $message['type'];
      }
      if (isset($message['additional'])) {
        $payload['data']['additional'] = $message['additional'];
      }

      if (isset($message['data'])) {
        $payload['data']['data'] = $message['data'];
      }

      if (isset($message['click_action'])) {
        $payload['notification']['click_action'] = $message['click_action'];
      }

      if (is_array($deviceTokens)) {
        $payload['registration_ids'] = $deviceTokens;
      } else {
        $payload['to'] = $deviceTokens;
      }

      $response = $client->request('POST', NotificationConfig::URL, [
        'headers' => $this->headers,
        'json' => $payload,
        'timeout' => 60,
        'http_errors' => false
      ]);

      $data = json_decode($response->getBody()->getContents());

      if (app()->environment() != 'production') {
        return ['data' => $data];
      }

      return isset($data->failure) && $data->failure == 0;
    }

    return false;
  }


  public function toGroup($groupName, $message) {
    if (empty($groupName) || empty($message)) {
      return 'Require message and device_token';
    }

    $client = new Client();

    $result = $client->request('POST', NotificationConfig::URL, [
      'headers' => $this->headers,
      'json' => [
        'data' => [
          'body' => $message
        ],
        'condition' => "'" . $groupName . "' in topics"
      ],
      'timeout' => 60,
      'http_errors' => false
    ])->getBody();

    $result = json_decode($result);
    return $result;
  }


  public function toAll($message) {
    if (empty($message))
      return 'Message required';

    $client = new Client();
    $result = $client->request('POST', NotificationConfig::URL, [
      'headers' => $this->headers,
      'json' => [
        'data' => [
          'body' => $message
        ],
        'condition' => "'all' in topics",
      ],
      'timeout' => 60,
      'http_errors' => false
    ])->getBody();

    $result = json_decode($result);
    return $result;
  }

}
