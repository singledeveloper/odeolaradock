<?php

namespace Odeo\Domains\Notification;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;

use Carbon\Carbon;

class NotificationSelector implements SelectorListener {

  private $user;

  public function __construct() {
    $this->notification = app()->make(\Odeo\Domains\Notification\Repository\NotificationRepository::class);
    $this->userRepo = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    $filters = $repository->getFilters();

    $notification = [];

    if ($item->type) $notification["type"] = $item->type;
    if ($item->group) $notification["group"] = $item->group;
    if ($item->title) $notification["title"] = $item->title;
    if ($item->description) {
      $desc = str_replace("{count}", "1", $item->description);
      $notification["message"] = json_decode($desc, true);
    }
    if ($item->created_at) $notification["date"] = $item->created_at;

    if ($item->data) {

      $data = json_decode($item->data, true);
      $notification["data"] = $data;

      if (isset($data['custom']) && $data['custom'] == 'date_use_user_created_at') {

        if (!$this->user) {
          $this->user = $this->userRepo->findById($filters['auth']['user_id']);
        }

        $notification["date"] = $this->user->created_at->toDateTimeString();


      }

    }

    return $notification;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {
    $this->notification->setSimplePaginate(true);
    $this->notification->normalizeFilters($data);

    $notifications = [];

    if (isset($data['group'])) {
      $notificationList = $this->notification->getByGroup($data['group']);
    } else {
      $notificationList = $this->notification->get();
    }

    foreach ($notificationList as $item) {
      $notifications[] = $this->_transforms($item, $this->notification);
    }

    if (sizeof($notifications) > 0)
      return $listener->response(200, array_merge(
        ["notifications" => $this->_extends($notifications, $this->notification)],
        $this->notification->getPagination()
      ));
    return $listener->response(204, ["notifications" => []]);
  }

  public function getStatus(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];
    $lastReadDate = Carbon::createFromTimestampMs($data['last_read_timestamp'])->toDateTimeString();

    return $listener->response(200,
      ["unread" => $this->notification->getNumberOfUnread($userId, $lastReadDate)]
    );
  }

  public function getBalanceNotification(PipelineListener $listener, $data) {
    $notificationBalanceRepo = app()->make(\Odeo\Domains\Notification\Repository\NotificationBalanceRepository::class);

    if ($notification = $notificationBalanceRepo->findByUserId($data['auth']['user_id']))
      return $listener->response(200, [
        'amount' => $notification->amount,
        'email' => $notification->email
      ]);

    return $listener->response(204);
  }
}
