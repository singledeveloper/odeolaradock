<?php

namespace Odeo\Domains\Notification\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Notification\Model\Announcement;

class AnnouncementRepository extends Repository {

  public function __construct(Announcement $announcement) {
    $this->model = $announcement;
  }

  public function get() {
    $filters = $this->getFilters();
    
    $query = $this->model->orderBy($filters['sort_by'], $filters['sort_type']);

    return $this->getResult($query);
  }

  public function getActive() {
    return $this->runCache('announcements', 'announcements', 1440, function () {
      $date = \Carbon\Carbon::today()->toDateString();

      return $this->model
        ->select('id', 'title', 'content')
        ->where('start_date', '<=', $date)
        ->where('end_date', '>=', $date)
        ->where('is_active', true)
        ->orderBy('priority', 'desc')
        ->orderBy('id', 'asc')
        ->get()
        ->toArray();
    });
  }

}
