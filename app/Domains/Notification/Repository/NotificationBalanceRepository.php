<?php

namespace Odeo\Domains\Notification\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Notification\Model\NotificationBalance;

class NotificationBalanceRepository extends Repository {

  public function __construct(NotificationBalance $notificationBalance) {
    $this->model = $notificationBalance;
  }

  public function findByUserId($userId) {
    return $this->getCloneModel()->where('user_id', $userId)->first();
  }
}
