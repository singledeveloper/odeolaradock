<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/07/19
 * Time: 11.34
 */

namespace Odeo\Domains\OAuth2\Helper;

use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Constant\UserType;

class OAuth2ClientHelper {

  const SCOPE_PG = 'pg_get_payment:read';
  const SCOPE_DG = 'dg_disbursement:write dg_inquiry:write';

  public static function isAllowedUserId($userId) {
    $allowedUserIds = [
      14, // 'Brian
      19, // 'Gerard Edwin
      143671 // 'Odeo V2 Internal User
    ];

    $userRepo = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    foreach ($userRepo->getModel()->where('status', UserStatus::OK)
               ->where(function($query){
                 $query->where('telephone', 'like', UserType::BUSINESS_PREFIX . '%')
                   ->orWhere('telephone', 'like', '62802%');
               })
               ->get() as $item) {
      $allowedUserIds[] = $item->id;
    }
    return in_array($userId, $allowedUserIds);
  }

}