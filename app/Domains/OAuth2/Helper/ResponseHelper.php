<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 17/06/19
 * Time: 18.27
 */

namespace Odeo\Domains\OAuth2\Helper;


class ResponseHelper {

  const STATUS_ERROR = 400;
  const CODE_ERROR = 10001;

  public static function parseResponseStatus($response) {
    if ($response['responseCode'] == '00') {
      return 200;
    }

    return 400;
  }

  public static function parseResponseToXml($response, $title) {
    $xml = '<?xml version="1.0"?>' . PHP_EOL;
    $xml .= '<' . $title . '>' . PHP_EOL;

    foreach ($response as $key => $value) {
      $xml .= '<' . $key . '>' . $value . '</' . $key . '>' . PHP_EOL;
    }
    $xml .= '</' . $title . '>';
    return $xml;
  }

}