<?php

namespace Odeo\Domains\OAuth2\Helper;

class SignatureGenerator {
  
  private $oAuth2Clients; 

  public function __construct() {
    $this->oAuth2Clients = app()->make(\Odeo\Domains\OAuth2\Repository\OAuth2ClientRepository::class);
  }
  
  public function generateSignatureByClientId($clientId, $stringToSign) {
    $oAuth2Client = $this->oAuth2Clients->findByClientId($clientId);
    return $this->generateSignature($oAuth2Client, $stringToSign);
  }

  public function generateSignatureByUserId($userId, $stringToSign) {
    $oAuth2Client = $this->oAuth2Clients->findByUserId($userId);
    return $this->generateSignature($oAuth2Client, $stringToSign);
  }

  public function generateSignature($oauth2Client, $stringToSign) {
    if (!$oauth2Client) {
      return null;
    }

    return base64_encode(hash_hmac('sha256', $stringToSign, $oauth2Client->signing_key, true));
  }
}
