<?php

namespace Odeo\Domains\OAuth2\Model;

use Odeo\Domains\Core\Entity;

class OAuth2Client extends Entity
{

  protected $table = 'oauth2_clients';

}
