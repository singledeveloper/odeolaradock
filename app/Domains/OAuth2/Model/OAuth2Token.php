<?php

namespace Odeo\Domains\OAuth2\Model;

use Odeo\Domains\Core\Entity;

class OAuth2Token extends Entity
{

  protected $table = 'oauth2_tokens';

}
