<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 03/07/19
 * Time: 17.27
 */

namespace Odeo\Domains\OAuth2;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\DisbursementApiUserUpdater;
use Odeo\Domains\OAuth2\Helper\OAuth2ClientHelper;
use Odeo\Domains\OAuth2\Repository\OAuth2ClientRepository;

class OAuth2ClientUpdater {

  private $oauthClientRepo;

  public function __construct() {
    $this->oauthClientRepo = app()->make(OAuth2ClientRepository::class);
  }

  public function createUserOAuth2Client(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];

    if (isProduction() && !OAuth2ClientHelper::isAllowedUserId($userId)) {
      return $listener->response(400, trans('api_users.unauthorized'));
    }

    if ($this->oauthClientRepo->findByUserId($userId)) {
      return $listener->response(409, trans('api_users.client_exists'));
    }

    $client = $this->oauthClientRepo->getNew();
    $client->user_id = $userId;
    $client->client_id = randomStr(32);
    $client->secret = randomStr(64);
    $client->signing_key = randomStr(64);
    $client->domain = '';
    $client->allowed_scope = '';
    $this->oauthClientRepo->save($client);

    return $listener->response(200, [
      'client_id' => $client->client_id,
      'secret' => $client->secret,
      'signing_key' => $client->signing_key
    ]);
  }

  public function updateScope(PipelineListener $listener, $data) {
    if (!$client = $this->oauthClientRepo->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('api_users.unauthorized'));
    }
    if (strpos($client->allowed_scope, $data['scope']) !== false) {
      return $listener->response(409, trans('api_users.scope_exists'));
    }

    $client->allowed_scope .= ($client->allowed_scope == '' ? '' : ' ') . $data['scope'];
    $this->oauthClientRepo->save($client);

    return $listener->response(200, [
      'signing_key' => $client->signing_key
    ]);
  }

  public function generateSecret(PipelineListener $listener, $data) {
    if (!$client = $this->oauthClientRepo->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('api_users.unauthorized'));
    }

    $client->secret = randomStr(64);
    $this->oauthClientRepo->save($client);

    return $listener->response(200, [
      'secret' => $client->secret
    ]);
  }

  public function generateSigningKey(PipelineListener $listener, $data) {
    if (!$client = $this->oauthClientRepo->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('api_users.unauthorized'));
    }

    $client->signing_key = randomStr(64);
    $this->oauthClientRepo->save($client);

    $listener->addNext(new Task(DisbursementApiUserUpdater::class, 'updateSigningKey', [
      'user_id' => $data['auth']['user_id'],
      'signing_key' => $client->signing_key
    ]));

    return $listener->response(200, [
      'signing_key' => $client->signing_key
    ]);
  }

}