<?php

namespace Odeo\Domains\OAuth2;

use Odeo\Domains\Core\PipelineListener;

class OAuth2TokenRequestParser {

  public function __construct() {
    $this->oauth2ClientRepository = app()->make(\Odeo\Domains\OAuth2\Repository\OAuth2ClientRepository::class);
  }
  
  public function parseClientIdAndClientSecret(PipelineListener $listener, $data) {
    if (isset($data['client_id']) && isset($data['client_secret'])) {
      return $listener->response(200);
    }

    $request = app()->make(\Illuminate\Http\Request::class);
    if (!$request->header('Authorization')) {
      return $listener->response(400, trans('oauth2.invalid_auth_header'));
    }

    $authorizationHeader = explode(", ", $request->header('Authorization'));
    $authorizations = [];
    foreach ($authorizationHeader as $authorization) {
      $auth = explode(" ", $authorization);
      if (count($auth) == 2) {
        $authorizations[strtolower($auth[0])] = $auth[1];
      }
    }
    if (!isset($authorizations['basic'])) {
      return $listener->response(400, trans('oauth2.invalid_auth_header'));
    }

    $decoded = base64_decode($authorizations['basic']);
    $clientData = explode(":", $decoded);
    if (count($clientData) !== 2) {
      return $listener->response(400, trans('oauth2.invalid_auth_header'));
    }

    return $listener->response(200, [
      'client_id' => $clientData[0],
      'client_secret' => $clientData[1]
    ]);
  }

  public function parseScope(PipelineListener $listener, $data) {
    $client = $this->oauth2ClientRepository->findByClientIdAndSecret($data['client_id'], $data['client_secret']);
    if (!$client) {
      return
        $listener->response(400, trans('oauth2.invalid_client'));
    }
    
    if (!isset($data['scope'])) {
      return $listener->response(200, [
        'scope' => $client->allowed_scope,
        'user_id' => $client->user_id
      ]);
    }

    $allowedScopes = explode(" ", $client->allowed_scope);
    $selectedScopes = explode(" ", $data['scope']);
    foreach ($selectedScopes as $selectedScope) {
      if (!in_array($selectedScope, $allowedScopes)) {
        return $listener->response(400, trans('oauth2.invalid_scope'));
      }
    }

    return $listener->response(200, ['user_id' => $client->user_id]);
  }
}
