<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:31 PM
 */

namespace Odeo\Domains\Order;


use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Order\Repository\OrderRepository;
use Odeo\Domains\Transaction\Helper\Currency;

class AdminOrderSelector implements SelectorListener {

  private $orderRepo, $currencyHelper;

  public function __construct() {
    $this->orderRepo = app()->make(OrderRepository::class);
    $this->currencyHelper = app()->make(Currency::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {

    $output = [];
    $output['order_id'] = $item->id;
    $output['status'] = $item->status;
    $output['created_at'] = $item->created_at;
    $output['opened_at'] = $item->opened_at;
    $output['paid_at'] = $item->paid_at;
    $output['expired_at'] = $item->expired_at;
    $output['closed_at'] = $item->closed_at;

    $output['gateway'] = Payment::getGatewayName($item->gateway_id);
    $output['user']['id'] = $item->user_id;
    $output['user']['telephone'] = $item->phone_number;
    $output['user']['email'] = $item->email;
    $output['user']['name'] = $item->name;
    $output['cart_data']['seller_store_name'] = $item->store_name ?? 'Odeo';

    $output['cart_data']['total'] = $this->currencyHelper->formatPrice($item->total);
    $output['cart_data']['subtotal'] = $this->currencyHelper->formatPrice($item->subtotal);
    $output['cart_data']['seller_store_id'] = $item->seller_store_id ?? null;
    $output['cart_data']['actual_store_id'] = $item->actual_store_id ?? null;
    $output['cart_data']['service_id'] = $item->service_id;
    $output['cart_data']['service_name'] = $item->service_name;
    $output['cart_data']['item_name'] = $item->item_name;
    $output['cart_data']['item_number'] = $item->number;

    $output['payment']['opc'] = $item->opc;
    $output['payment']['opc_group'] = $item->info_id;
    $output['payment']['name'] = $item->payment_name;
    $output['cart_data']['charges'] = $item->charges->map(function($c) {
      return [
        'type' => $c->type,
        'group_type' => $c->group_type,
        'displayed_name' => trans('order.order_charge_' . $c->type),
        'price' => $this->currencyHelper->formatPrice($c->amount)
      ];
    });

    return $output;
  }

  public function get(PipelineListener $listener, $data) {
    $this->orderRepo->normalizeFilters($data);
    $this->orderRepo->setSimplePaginate(true);
    $orders = $this->orderRepo->getWithAdminPrivilege();
    $orders = $orders->map(function($item) {
      return $this->_transforms($item, $this->orderRepo);
    });

    if (!empty($orders)) {
      return $listener->response(200, array_merge(
        ["orders" => $orders],
        $this->orderRepo->getPagination()
      ));
    }

    return $listener->response(204, ["orders" => []]);

  }


}
