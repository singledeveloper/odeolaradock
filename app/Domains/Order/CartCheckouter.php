<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 7:53 PM
 */

namespace Odeo\Domains\Order;

use Carbon\Carbon;
use Odeo\Domains\Constant\OrderConfig;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;

class CartCheckouter {

  private $carts, $cartParser;

  public function __construct() {
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->carts = app()->make(\Odeo\Domains\Order\Repository\CartRepository::class);
    $this->cartParser = app()->make(\Odeo\Domains\Order\Helper\CartParser::class);
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
    $this->orderCharges = app()->make(\Odeo\Domains\Order\Repository\OrderChargeRepository::class);
    $this->inventoryManager = app()->make(\Odeo\Domains\Inventory\Helper\InventoryManager::class);
  }

  public function generateSignature(PipelineListener $listener, $data) {

    if (!$cart = $this->carts->getUserCart($data['auth']['user_id']))
      return $listener->response(400, trans('errors.cart.not_found'));

    $paymentChannels = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);

    $signature = Platform::generateCartSignature($data);
    $response = [
      'signature' => $signature,
      'cart_id' => $cart->id,
    ];

    if (isset($data['with_opc']) && $data['with_opc']) {
      $response['opc'] = $paymentChannels->findByAttributes(['gateway_id' => $data['gateway_id'], 'info_id' => $data['info_id']])->code;
    }

    return $listener->response(200, $response);
  }

  public function checkout(PipelineListener $listener, $data) {
    if (!$cart = $this->carts->getUserCart($data['auth']['user_id']))
      return $listener->response(400, trans('errors.cart.not_found'));

    $data['cart_id'] = $cart->id;

//    if (env('APP_ENV') != 'local' && $data['signature'] != Platform::generateCartSignature($data))
//      return $listener->response(400, "Data not valid.");

    $cartData = $this->cartParser->parseDefault($cart, ['base_price', 'extended_detail']);

    $cartData = $this->cartParser->parsePrice($cartData);
    $order = $this->orders->getNew();

    $user = $this->users->findById($data['auth']['user_id']);

    $order->email = $data["email"] ?? $user->email;
    $order->phone_number = $data["telephone"] ?? $user->telephone;
    $order->salutation = $data["salutation"] ?? null;
    $order->name = $data['first_name'] ?? $user->name;
    $order->additional_phone_number = $data['additional_phone_number'] ?? null;
    $order->additional_email = $data['additional_email'] ?? null;

    if (isset($data["store_id"])) {
      $order->seller_store_id = $data["store_id"];
      $order->actual_store_id = $data["store_id"];
    }

    $order->user_id = $cart->user_id;
    $order->gateway_id = $data['gateway_id'];
    $order->platform_id = isset($data['auth']['platform_id']) ? $data['auth']['platform_id'] : $data['platform_id'];

    $now = Carbon::now();

    $order->created_at = $now->toDateTimeString();
    $order->expired_at = $now->copy()->addHour(24)->toDateTimeString();

    $order->status = OrderStatus::CREATED;
    $order->subtotal = $cartData['subtotal']['amount'];
    $order->total = $cartData['total']['amount'];

    $item = array_first($cartData['items']);

    if (!in_array($item['service_id'], [Service::DOMAIN, Service::WARRANTY, Service::PLAN, Service::OCASH, Service::ODEPOSIT])) {
      $order->expired_at = $now->copy()->addHour(OrderConfig::BASE_EXPIRE_HOUR)->toDateTimeString();
    }

    if (!$order->skip_check_temp_deposit && in_array($item['service_id'], [Service::CREDIT_BILL, Service::USER_INVOICE])) {
      $order->skip_check_temp_deposit = true;
    }

    $orderDetail = $this->orderDetails->getNew();

    if (app()->make(\Odeo\Domains\Order\Helper\CartChecker::class)->isFreePurchase($item)) {
      $orderDetail->verified_at = $now;
      $order->status = OrderStatus::FREE_ORDER_COMPLETED;
      $order->closed_at = $now;
    }

    $this->orders->save($order);

    $orderDetail->name = $item['name'];
    $orderDetail->order_id = $order->id;
    $orderDetail->quantity = $item['quantity'];
    $orderDetail->sale_price = $item['price']['amount'];
    $orderDetail->agent_base_price = $item['agent_base_price'] ?? null;
    $orderDetail->extended_detail = $item['order_extended_detail'] ?? json_encode([]);
    $orderDetail->base_price = $item['base_price'];
    $orderDetail->merchant_price = $item['merchant_price'];
    $orderDetail->margin = $item['margin'];
    $orderDetail->mentor_margin = $item['mentor_margin'];
    $orderDetail->leader_margin = $item['leader_margin'];
    $orderDetail->store_inventory_detail_id = $item['store_inventory_detail_id'] ?? null;
    $orderDetail->service_detail_id = $item['service_detail_id'];
    $orderDetail->referral_cashback = $item['referral_cashback'];

    $this->orderDetails->save($orderDetail);

    $className = $this->inventoryManager->getManagerClassName($item['service_id']);

    $listener->addNext(new Task($className, 'checkout', [
      'service_id' => $item['service_id'],
      'service_detail_id' => $item['service_detail_id'],
      'item' => $item,
      'order' => $order->toArray(),
      'order_detail' => $orderDetail->toArray()
    ]));


    foreach ($cartData['charges'] as $charge) {

      $orderCharge = $this->orderCharges->getNew();

      $orderCharge->order_id = $order->id;
      $orderCharge->type = $charge['type'];
      $orderCharge->amount = $charge['price']['amount'];

      $this->orderCharges->save($orderCharge);
    }

    $this->carts->delete($cart);

    return $listener->response(201, [
      "order_id" => $order->id
    ]);

  }

}
