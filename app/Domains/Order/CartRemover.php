<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 7:09 PM
 */

namespace Odeo\Domains\Order;


use Odeo\Domains\Constant\Service;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Flight\FlightManager;
use Odeo\Domains\Inventory\Hotel\HotelManager;
use Odeo\Domains\Subscription\StoreRequester;
use Odeo\Domains\Transaction\DepositRequester;
use Odeo\Domains\Transaction\TopupRequester;
use Odeo\Domains\Subscription\WarrantyRequester;
use Validator;


class CartRemover {


  private $carts, $cartParser;

  public function __construct() {

    $this->carts = app()->make(\Odeo\Domains\Order\Repository\CartRepository::class);

    $this->cartParser = app()->make(\Odeo\Domains\Order\Helper\CartParser::class);

    $this->cartChecker = app()->make(\Odeo\Domains\Order\Helper\CartChecker::class);


  }

  public function removeItem(PipelineListener $listener, $data) {


    if (!isset($data['cart_item'])) {

      $cart = $this->carts->getUserCart($data['auth']['user_id']);

      if (!$cart) return $listener->response(400, trans('errors.cart.not_found'));

      $cart_data = $this->cartParser->parseDefault($cart, [
        'base_price'
      ]);

      $uid = $this->cartChecker->hasUid($cart_data, $data['uid']);

      if ($uid === false) return $listener->response(400, trans('errors.cart.item_not_found'));

      $item = $cart_data['items'][$uid];


      $cart_data['subtotal'] -= $item["price"];
      $cart_data['total'] -= $item["price"];

      array_splice($cart_data['items'], $uid, 1);

      if (count($cart_data['items'])) {
        $cart->cart_data = json_encode($cart_data);
      } else {
        $cart->cart_data = "";
      }


      $this->carts->save($cart);

    } else {
      $item = $data["cart_item"];
    }

    switch ($item['service_id']) {
      case Service::PLAN:

        $listener->addNext(new Task(StoreRequester::class, 'rollback', [
          'store_id' => $item['item_detail']['store_id']
        ]));

        break;
      case Service::ODEPOSIT:

        $listener->addNext(new Task(DepositRequester::class, 'cancel', [
          'store_deposit_id' => $item['item_detail']['store_deposit_id']
        ]));

        break;
      case Service::OCASH:

        $listener->addNext(new Task(TopupRequester::class, 'cancel'));

        break;


      case Service::CREDIT_BILL:

        $listener->addNext(new Task(\Odeo\Domains\Inventory\Creditbill\Odeo\Biller\Pundi\Requester::class, 'cancel', [
          'credit_bill_id' => $item['item_detail']['credit_bill_id']
        ]));

        break;

      case Service::WARRANTY:

        $listener->addNext(new Task(WarrantyRequester::class, 'cancel', [
          'store_id' => $item['item_detail']['store_id']
        ]));

        break;

      case Service::FLIGHT:

        $tiketOrderDetailId = null;

        if (isset($item['item_detail']) && isset($item['item_detail']['order']) && isset($item['item_detail']['order']['tiket_order_detail_id'])) {
          $tiketOrderDetailId = $item['item_detail']['order']['tiket_order_detail_id'];
        }

        $listener->addNext(new Task(FlightManager::class, 'removeCart', [
          'service_id' => $item['service_id'],
          'service_detail_id' => $item['service_detail_id'],
          'tiket_order_detail_id' => $item['tiket_order_detail_id'] ?? $tiketOrderDetailId
        ]));

        break;

      case Service::HOTEL:

        $tiketOrderDetailId = null;

        if (isset($item['item_detail']) && isset($item['item_detail']['order']) && isset($item['item_detail']['order']['tiket_order_detail_id'])) {
          $tiketOrderDetailId = $item['item_detail']['order']['tiket_order_detail_id'];
        }

        $listener->addNext(new Task(HotelManager::class, 'removeCart', [
          'service_id' => $item['service_id'],
          'service_detail_id' => $item['service_detail_id'],
          'tiket_order_detail_id' => $item['tiket_order_detail_id']  ?? $tiketOrderDetailId,
        ]));

        break;
    }

    return $listener->response(200);

  }


  public function clear(PipelineListener $listener, $data) {

    if ($cart = $this->carts->getUserCart($data['auth']['user_id'])) {
      $cart_data = $this->cartParser->parseDefault($cart);

      if ((isset($data['skip_rollback']) && $data['skip_rollback']) ? false : true) {

        foreach ($cart_data['items'] as $item) {

          $this->removeItem($listener, ["cart_item" => $item]);

        }

      }

      $this->carts->delete($cart);

    }

    return $listener->response(200);

  }

  public function removeCharge(PipelineListener $listener, $data) {

    $validator = Validator::make($data, [
      'type' => 'required',
    ]);

    if ($validator->fails()) return $listener->response(400, $validator->errors()->all());


    $cart = $this->carts->getUserCart($data['auth']['user_id']);

    if (!$cart) return $listener->response(400, trans('errors.cart.not_found'));

    $cart_data = $this->cartParser->parseDefault($cart, [
      'base_price'
    ]);

    $type = $this->cartChecker->hasCharge($cart_data, $data['type']);

    $charge = $cart_data['charges'][$type];

    $cart_data['total'] -= $charge["price"];

    unset($cart_data['charges'][$type]);

    $cart->cart_data = json_encode($cart_data);

    $this->carts->save($cart);

    return $listener->response(200);

  }

}