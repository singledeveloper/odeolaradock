<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 6:17 PM
 */

namespace Odeo\Domains\Order;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class CartSelector implements SelectorListener {

  private $carts, $cartParser;

  public function __construct() {

    $this->carts = app()->make(\Odeo\Domains\Order\Repository\CartRepository::class);

    $this->cartParser = app()->make(\Odeo\Domains\Order\Helper\CartParser::class);

  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function get(PipelineListener $listener, $data) {

    $cart = $this->carts->getUserCart($data['auth']['user_id']);

    if (!$cart) return $listener->response(204);

    $cartData = $this->cartParser->parseDefault($cart);

    $cartData = $this->cartParser->parsePrice($cartData);

    return $listener->response(200, array_merge(["cart_id" => $cart->id], $cartData));
  }
}