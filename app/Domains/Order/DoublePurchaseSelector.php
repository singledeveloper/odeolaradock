<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 26/07/18
 * Time: 20.38
 */

namespace Odeo\Domains\Order;


use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Helper\InventoryManager;

class DoublePurchaseSelector {

  private $inventoryManager;

  public function __construct() {
    $this->inventoryManager = app()->make(InventoryManager::class);
  }

  public function get(PipelineListener $listener, $data) {
    $className = $this->inventoryManager->getManagerClassName(ServiceDetail::getServiceId($data['service_detail_id']));
    return $listener->addNext(new Task($className, 'getLastDetectedDoublePurchase', $data));
  }
}
