<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:31 PM
 */

namespace Odeo\Domains\Order;

use Carbon\Carbon;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class FinancialOrderSelector {

  private $orders, $currencyHelper, $orderDetailizer, $payments, $storeParser,
    $paymentInformations;

  public function __construct() {

    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);

    $this->orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);

    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

    $this->orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);

    $this->payments = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);

    $this->storeParser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);

    $this->paymentInformations = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelInformationRepository::class);

  }


  public function _transforms($orderDetail, Repository $repository) {

    $output = [];

    $item = $this->orderDetailizer->extractItem($orderDetail, array_flip([
      'base_price'
    ]));

    $output['order_id'] = $orderDetail->order_id;
    $output['product_name'] = $item['name'];
    $output['product_price'] = $this->currencyHelper->formatPrice($orderDetail->base_price);

    switch ($item['service_id']) {
      case Service::PLAN:
        $output['product_description'] = $item['item_detail']['store_domain'];
        break;
      case Service::PULSA:
      case Service::PAKET_DATA:
      case Service::PLN:
      case Service::BOLT:
      case Service::EMONEY:
      case Service::TRANSPORTATION:
        $output['product_description'] = $item['item_detail']['operator'] . ' - ' . $item['item_detail']['number'];
        $output['product_price'] = $this->currencyHelper->formatPrice($item['item_detail']['current_base_price']);
        break;
      case Service::PLN_POSTPAID:
      case Service::PULSA_POSTPAID:
      case Service::BPJS_KES:
      case Service::BROADBAND:
      case Service::LANDLINE:
      case Service::PDAM:
      case Service::PGN:
      case Service::MULTI_FINANCE:
        $output['product_description'] = $item['item_detail']['number'];
      default:
        $output['product_description'] = '';
        break;
    }

    $output['product_type'] = strtoupper($item['type']);
    $output['customer_id'] = isset($orderDetail->user->telephone) ? $orderDetail->user->telephone : $orderDetail->phone_number;

    $output['seller'] = $orderDetail->actual_store_id ? $this->storeParser->domainName($orderDetail->actualStore) : 'ODEO';

    $payment = $this->payments->findByOrderId($orderDetail->order_id);

    $output['payment_method'] = 'unknown';

    if ($payment) {
      $output['payment_method'] = $this->paymentInformations->findById($payment->info_id)->actual_name;
    }

    $output['merchant_cost'] = $this->currencyHelper->formatPrice($orderDetail->base_price);
    $output['price'] = $this->currencyHelper->formatPrice($orderDetail->sale_price);
    $output['net_sale'] = $this->currencyHelper->formatPrice($orderDetail->sale_price);

    $charges = $this->orderDetailizer->detailizeCharge($orderDetail->order_id);

    foreach ($charges as $charge) {
      if ($charge['type'] == OrderCharge::PAYMENT_SERVICE_COST || $charge['type'] == OrderCharge::UNIQUE_CODE) {
        $output['payment_fee'] = $charge['price'];
      }
    }

    $output['total'] = $this->currencyHelper->formatPrice($orderDetail->total);
    $output['date'] = Carbon::parse($orderDetail->paid_at)->toDateString();
    $output['time'] = Carbon::parse($orderDetail->paid_at)->toTimeString();

    return $output;

  }

  public function get(PipelineListener $listener, $data) {

    $this->orders->normalizeFilters($data);
    $this->orders->setSimplePaginate(true);

    $financialOrders = [];

    foreach ($this->orders->getPaginatedFinancialOrders() as $orderDetail) {
      $financialOrders[] = $this->_transforms($orderDetail, $this->orders);
    }

    if (sizeof($financialOrders) > 0) {
      return $listener->response(200, array_merge(
        ["financial_orders" => $financialOrders],
        $this->orders->getPagination()
      ));
    }
    return $listener->response(204, ["financial_orders" => []]);

  }

  public function getExportedFinancialOrder($data) {

    $this->orders->normalizeFilters($data);

    $orders = $this->orders->getAllFinancialOrders();

    $writer = WriterFactory::create(Type::CSV);

    $writer->openToBrowser('Financial Order ' . ($data['search']['start_date'] ?? '0000-00-00') . ' to ' . ($data['search']['end_date'] ?? Carbon::now()->toDateString()) . '.csv');

    $writer->addRow([
      'Date',
      'Time',
      'Order ID',
      'Product Type',
      'Product Name',
      'Product Description',
      'Customer ID',
      'Seller',
      'From',
      'Payment Method',
      'Payment Reference',
      'Vendor Price',
      'Merchant Cost',
      'Merchant Price',
      'Net. Sale',
   //   'Payment Code',
      'Total',
      'Status'
    ]);

    while ($order = $orders->fetch(\PDO::FETCH_ASSOC)) {

      $productDescription = (function () use ($order) {

        if ($order['service_id'] == 1) {
          if ($order['domain_name']) {
            return $order['domain_name'];
          } else {
            return $order['subdomain_name'] . '.odeo.co.id';
          }
        } else if (in_array($order['service_id'], [
          2, 3, 9, 10
        ])) {
          return $order['product_name'];
        }
        return $order['product_name'] . ' - ' . $order['number_from_order_detail_pulsa_switcher'];

      })();

      $seller = (function () use ($order) {

        if ($order['domain_name']) {
          return $order['domain_name'];
        } else {
          return $order['subdomain_name'] . '.odeo.co.id';
        }
      })();


      $status = (function () use ($order) {

        if ($order['order_status'] == '90001') {
          return 'REFUNDED';
        } else if ($order['order_status'] == '30003') {
          return 'INVALID';
        }
        return 'COMPLETED';

      })();

      $productName = (function () use ($order) {
        if ($order['service_name'] == 'pln') {
          return substr($order['product_name'], 0, strpos($order['product_name'], '/'));
        }
        return $order['product_name'];

      })();

      $writer->addRow([
        $order['date'],
        $order['time'],
        $order['order_id'],
        $order['service_name'],
        $productName,
        $productDescription,
        $order['user_id'],
        $seller,
        Payment::getGatewayName($order['gateway_id']),
        isset($order['payment_method']) ? $order['payment_method'] : '',
        isset($order['payment_reference']) ? '="' . $order['payment_reference'] . '"' : '',
        $order['vendor_price'],
        $order['merchant_cost'],
        $order['price'],
        $order['net_sale'],
    //    isset($order['charges']['payment_fee']) ? $order['payment_fee'] : '',
        $order['total'],
        $status
      ]);
    }

    $writer->close();

  }


}
