<?php

namespace Odeo\Domains\Order\Helper;

use Odeo\Domains\Constant\MarketingConfig;
use Odeo\Domains\Constant\NetworkConfig;

class OrderBonusCalculator {

  public function calculateBonus($orderDetail) {
    $marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $serviceDetail = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);

    $basePrice = $orderDetail->base_price;
    $merchantPrice = $orderDetail->merchant_price;
    $salePrice = $orderDetail->sale_price;
    $mentorMargin = $orderDetail->mentor_margin;
    $serviceDetailId = $orderDetail->service_detail_id;

    $defaultMargin = $marginFormatter->formatMargin($basePrice, [
      'service_detail' => $serviceDetail->findById($serviceDetailId),
      'default' => true
    ]);

    $marginType = $mentorMargin <= 1 ? 'percentage' : 'value';

    if ($marginType == "percentage") {
      $mentorBonus = floor(($mentorMargin / 100) * $basePrice);
    } else if ($marginType == "value") {
      $mentorBonus = $mentorMargin;
    }

    $leaderBonus = $merchantPrice - $basePrice - $mentorBonus;
    if($leaderBonus < 1 && $mentorBonus > 0) {
      $mentorBonus = $mentorBonus - 1;
      $leaderBonus = 1;
    }

    $hustlerBonus = $salePrice - $merchantPrice;
    $defaultBonus = $defaultMargin['sale_price'] - $defaultMargin['merchant_price'];

    return [$hustlerBonus, $mentorBonus, $leaderBonus, $defaultBonus];
  }

  public function calculateRush($orderDetail, $orderBonus) {
    $hustlerRush = 0;
    $mentorRush = 0;
    $leaderRush = 0;
    $defaultHustlerRush = 0;
    $serviceDetailId = $orderDetail->service_detail_id;
    $inventoryMargin = $orderDetail->margin;

    $marginMultiplier = app()->make(\Odeo\Domains\Marketing\Repository\MarginMultiplierRepository::class);
    $multiplier = $marginMultiplier->getDefaultMultiplier($serviceDetailId);

    if($multiplier) {
      $selfBonus = $orderBonus['me'];
      if($inventoryMargin == $multiplier->margin) {
        $hustlerRush = $selfBonus * ($multiplier->multiplier - 1) * MarketingConfig::ORUSH_MULTIPLIER;
      }
      else {
        $hustlerRush = 0;
      }
      $defaultHustlerRush = $orderBonus['default'] * ($multiplier->multiplier - 1) * MarketingConfig::ORUSH_MULTIPLIER;

      $parentBonus = $orderBonus['parent'];
      $mentorRush = $parentBonus * ($multiplier->multiplier - 1) * MarketingConfig::ORUSH_MULTIPLIER;

      $leaderRush = $orderBonus['grandparent'] * ($multiplier->multiplier - 1) * MarketingConfig::ORUSH_MULTIPLIER;
    }

    return [$hustlerRush, $mentorRush, $leaderRush, $defaultHustlerRush];
  }
}
