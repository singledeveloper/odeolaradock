<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 4:04 PM
 */

namespace Odeo\Domains\Order\Helper;


class OrderCharger {


  private $orderCharge;

  public function __construct() {
    $this->orderCharge = app()->make(\Odeo\Domains\Order\Repository\OrderChargeRepository::class);
  }

  public function charge($orderId, $type, $amount, $groupType) {

    if ($charge = $this->orderCharge->getExisting([
      'order_id' => $orderId,
      'type' => $type,
      'group_type' => $groupType
    ])->first());
    else {
      $charge = $this->orderCharge->getNew();
    }

    $charge->type = $type;
    $charge->group_type = $groupType;
    $charge->amount = $amount;
    $charge->order_id = $orderId;

    $this->orderCharge->save($charge);

  }

  public function removeCharge($order, $type) {
    $charge = $this->orderCharge->getExisting([
      'order_id' => $order->id,
      'type' => $type
    ])->first();


    if ($charge) {
      $order->total -= $charge->amount;
      $order->save();

      $this->orderCharge->delete($charge);

    }

  }


}