<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/26/16
 * Time: 11:03 PM
 */

namespace Odeo\Domains\Order\Helper;

use Odeo\Domains\Constant\CreditBillOdeo;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\DDLending\Repository\LoanRepository;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository;
use Odeo\Domains\Payment\Pax\Repository\EdcTransactionRepository;

class OrderDetailizer {

  private $orders, $orderDetails, $currencyHelper, $charges, $loanRepo;

  public function __construct() {
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->tiketOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository\OrderDetailTiketRepository::class);
    $this->planOrder = app()->make(\Odeo\Domains\Subscription\Repository\OrderDetailPlanRepository::class);
    $this->topupOrder = app()->make(\Odeo\Domains\Transaction\Repository\OrderDetailTopupRepository::class);
    $this->warrantyOrder = app()->make(\Odeo\Domains\Subscription\Repository\OrderDetailWarrantyRepository::class);
    $this->mdPlusOrder = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\OrderDetailMdPlusRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->charges = app()->make(\Odeo\Domains\Order\Repository\OrderChargeRepository::class);
    $this->plans = app()->make(\Odeo\Domains\Subscription\Repository\PlanRepository::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->warranties = app()->make(\Odeo\Domains\Subscription\Repository\WarrantyRepository::class);
    $this->inquiryFormatter = app()->make(\Odeo\Domains\Supply\Formatter\InquiryManager::class);
    $this->loanRepo = app()->make(LoanRepository::class);
    $this->edcTransactionRepo = app()->make(EdcTransactionRepository::class);
  }

  public function extractItem($detail, $expand) {

    $item = null;

    switch ($detail->service_detail_id) {
      case ServiceDetail::PULSA_ODEO:
      case ServiceDetail::PLN_ODEO:
      case ServiceDetail::BOLT_ODEO:
      case ServiceDetail::PAKET_DATA_ODEO:
      case ServiceDetail::TRANSPORTATION_ODEO:
      case ServiceDetail::EMONEY_ODEO:
        $item = $this->switcherOrder->findByOrderDetailId($detail->id);

        if ($detail->service_detail_id == ServiceDetail::PULSA_ODEO)
          $temp['service_id'] = Service::PULSA;
        else if ($detail->service_detail_id == ServiceDetail::PAKET_DATA_ODEO)
          $temp['service_id'] = Service::PAKET_DATA;
        else if ($detail->service_detail_id == ServiceDetail::PLN_ODEO)
          $temp['service_id'] = Service::PLN;
        else if ($detail->service_detail_id == ServiceDetail::BOLT_ODEO)
          $temp['service_id'] = Service::BOLT;
        else if ($detail->service_detail_id == ServiceDetail::TRANSPORTATION_ODEO)
          $temp['service_id'] = Service::TRANSPORTATION;
        else if ($detail->service_detail_id == ServiceDetail::EMONEY_ODEO)
          $temp['service_id'] = Service::EMONEY;

        $temp['order_detail_pulsa_switcher_id'] = ($item) ? $item->id : "";
        $temp['inventory_id'] = ($item) ? $item->current_inventory_id : "";
        //$temp['item_detail']['operator'] = ($item) ? $item->operator : "";
        $temp['item_detail']['operator'] = '';
        $temp['item_detail']['number'] = ($item) ? $item->number : "";
        $temp['item_detail']['serial_number'] = ($item) ? $item->serial_number : "";
        $temp['item_detail']['current_base_price'] = ($item) ? $item->current_base_price : 0;

        if (isset($expand['mask_phone_number']) && $length = strlen($temp['item_detail']['number'])) {
          $temp['item_detail']['number'] = str_repeat('*', max($length - 4, 0)) . substr($temp['item_detail']['number'], max($length - 4, 0));
        }

      //if ($detail->order_id == 4829099)
      //$temp['item_detail']['voucher_code'] = 'TEST';

        break;

      case ServiceDetail::GOOGLE_PLAY_ODEO:
        $item = $this->switcherOrder->findByOrderDetailId($detail->id);
        $temp['service_id'] = Service::GOOGLE_PLAY;

        $temp['order_detail_pulsa_switcher_id'] = ($item) ? $item->id : "";
        $temp['inventory_id'] = ($item) ? $item->current_inventory_id : "";

        if ($item && $item->serial_number && strpos($item->serial_number, '#') !== false) {
          list($temp['item_detail']['serial_number'], $temp['item_detail']['voucher_code']) = explode('#', $item->serial_number);
          $temp['item_detail']['voucher_code'] = wordwrap($temp['item_detail']['voucher_code'], 4, '-', true);
          $temp['item_detail']['voucher_code_label'] = 'Voucher Code';
        } else $temp['item_detail']['serial_number'] = $item ? $item->serial_number : '';

        break;

      case ServiceDetail::GAME_VOUCHER_ODEO:
        $item = $this->switcherOrder->findByOrderDetailId($detail->id);
        $temp['service_id'] = Service::GAME_VOUCHER;

        $temp['order_detail_pulsa_switcher_id'] = ($item) ? $item->id : "";
        $temp['inventory_id'] = ($item) ? $item->current_inventory_id : "";
        if ($item && $item->serial_number && strpos($item->serial_number, '/VC:') !== false) {
          list($temp['item_detail']['serial_number'], $temp['item_detail']['voucher_code']) = explode('/VC:', $item->serial_number);
          $temp['item_detail']['voucher_code_label'] = 'PIN';
        } else $temp['item_detail']['serial_number'] = $item ? $item->serial_number : '';
        $temp['item_detail']['number'] = ($item) ? $item->number : "";
        break;

      case ServiceDetail::PULSA_POSTPAID_ODEO:
      case ServiceDetail::BROADBAND_ODEO:
      case ServiceDetail::LANDLINE_ODEO:
      case ServiceDetail::PDAM_ODEO:
      case ServiceDetail::PGN_ODEO:
      case ServiceDetail::PLN_POSTPAID_ODEO:
      case ServiceDetail::BPJS_KES_ODEO:
      case ServiceDetail::MULTI_FINANCE_ODEO:
        $item = $this->switcherOrder->findByOrderDetailId($detail->id);

        if ($detail->service_detail_id == ServiceDetail::PLN_POSTPAID_ODEO)
          $temp['service_id'] = Service::PLN_POSTPAID;
        else if ($detail->service_detail_id == ServiceDetail::PULSA_POSTPAID_ODEO)
          $temp['service_id'] = Service::PULSA_POSTPAID;
        else if ($detail->service_detail_id == ServiceDetail::BPJS_KES_ODEO)
          $temp['service_id'] = Service::BPJS_KES;
        else if ($detail->service_detail_id == ServiceDetail::PDAM_ODEO)
          $temp['service_id'] = Service::PDAM;
        else if ($detail->service_detail_id == ServiceDetail::PGN_ODEO)
          $temp['service_id'] = Service::PGN;
        else if ($detail->service_detail_id == ServiceDetail::BROADBAND_ODEO)
          $temp['service_id'] = Service::BROADBAND;
        else if ($detail->service_detail_id == ServiceDetail::LANDLINE_ODEO)
          $temp['service_id'] = Service::LANDLINE;
        else if ($detail->service_detail_id == ServiceDetail::MULTI_FINANCE_ODEO)
          $temp['service_id'] = Service::MULTI_FINANCE;

        $temp['order_detail_pulsa_switcher_id'] = ($item) ? $item->id : "";
        $temp['inventory_id'] = ($item) ? $item->current_inventory_id : "";
        $temp['item_detail']['number'] = ($item) ? $item->number : "";
        $temp['item_detail']['reff'] = ($item) ? $item->serial_number : "";

        break;

      case ServiceDetail::PLAN_ODEO:

        if (!isset($storeParser)) $storeParser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);

        $item = $this->planOrder->findByOrderDetailId($detail->id);
        $plan = $this->plans->findById($item->plan_id);
        $store = $this->stores->findById($item->store_id);
        $temp['service_id'] = Service::PLAN;
        $temp['item_detail']['store_id'] = ($item) ? $item->store_id : "";
        $temp['item_detail']['subscription_months'] = $plan->minimal_months;
        $temp['item_detail']['store_domain'] = ($store) ? $storeParser->domainName($store) : "";
        $temp['item_detail']['store_name'] = ($store) ? $store->name : "";
        break;

      case ServiceDetail::DEPOSIT_ODEO:
        if (!isset($depositOrder)) $depositOrder = app()->make(\Odeo\Domains\Transaction\Repository\OrderDetailStoreDepositRepository::class);

        $item = $depositOrder->findByOrderDetailId($detail->id);
        $temp['service_id'] = Service::ODEPOSIT;
        $temp['item_detail']['store_deposit_id'] = ($item) ? $item->store_deposit_id : "";
        break;

      case ServiceDetail::CREDIT_BILL_ODEO:
        if (!isset($orderDetailCreditBills)) $orderDetailCreditBills = app()->make(\Odeo\Domains\Inventory\Creditbill\Repository\OrderDetailCreditBillRepository::class);
        $item = $orderDetailCreditBills->findByOrderDetailId($detail->id);
        $temp['service_id'] = Service::CREDIT_BILL;
        $temp['item_detail']['credit_bill_id'] = ($item) ? $item->credit_bill_id : "";
        $temp['item_detail']['biller'] = ($item) ? $item->biller : "";
        if ($item->biller == CreditBillOdeo::PUNDI_BILLER) {
          if (!isset($pundiCreditBills)) $pundiCreditBills = app()->make(\Odeo\Domains\Inventory\Creditbill\Odeo\Biller\Pundi\Repository\PundiCreditBillRepository::class);
          $creditBill = $pundiCreditBills->findById($item->credit_bill_id);
          $temp['item_detail']['number'] = ($creditBill) ? $creditBill->phone_number : "";
        }
        break;

      case ServiceDetail::USER_INVOICE_ODEO:
        if (!isset($orderDetailUserInvoice)) $orderDetailUserInvoice = app()->make(\Odeo\Domains\Inventory\UserInvoice\Repository\OrderDetailUserInvoiceRepository::class);
        $item = $orderDetailUserInvoice->findByAttributes('order_detail_id', $detail->id);
        $temp['service_id'] = Service::USER_INVOICE;
        $temp['item_detail']['invoice_number'] = ($item) ? $item->invoice_number : "";
        $temp['item_detail']['biller_id'] = ($item) ? $item->biller_id : "";

        if (!in_array($temp['item_detail']['biller_id'], UserInvoice::TERMINAL_ENABLED_BILLER)) {
          break;
        }

        if (!isset($affiliateUserInvoiceRepo)) {
          $affiliateUserInvoiceRepo = app()->make(AffiliateUserInvoiceRepository::class);
        }

        $invoiceNumbers = json_decode($item->invoice_number);
        if (!$invoiceNumbers) {
          break;
        }

        if (!is_array($invoiceNumbers)) {
          $invoiceNumbers = [$invoiceNumbers];
        }

        $invoices = $affiliateUserInvoiceRepo->getByInvoiceNumber($invoiceNumbers);
        if (!$invoices) {
          break;
        }

        $temp['item_detail']['details'] = $invoices
          ->load('biller')
          ->map(function ($inv) {
            return [
              'invoice_number' => $inv->invoice_number,
              'house_number' => $inv->billed_user_id,
              'period' => $inv->period,
              'biller_invoice_number' => $inv->name,
              'total' => $inv->total,
              'items' => $inv->items,
              'project' => $inv->biller->biller_name,
            ];
          })
          ->toArray();

        break;

      case ServiceDetail::TOPUP_ODEO:
        $item = $this->topupOrder->findByOrderDetailId($detail->id);
        $temp['service_id'] = Service::OCASH;
        $temp['item_detail']['topup_id'] = ($item) ? $item->topup_id : "";
        break;

      case ServiceDetail::WARRANTY_ODEO:
        $item = $this->warrantyOrder->findByOrderDetailId($detail->id);
        $warranty = $this->warranties->findById($item->warranty_id);
        $temp['service_id'] = Service::WARRANTY;
        $temp['item_detail']['store_id'] = ($warranty) ? $warranty->store_id : "";
        $temp['item_detail']['warranty_id'] = ($item) ? $item->warranty_id : "";
        break;

      case ServiceDetail::HOTEL_TIKET:
        $item = $this->tiketOrder->findByOrderDetailId($detail->id);
        $temp['service_id'] = Service::HOTEL;
        $temp['tiket_order_id'] = ($item) ? $item->tiket_order_id : "";
        $temp['tiket_order_detail_id'] = ($item) ? $item->tiket_order_detail_id : "";
        break;

      case ServiceDetail::FLIGHT_TIKET:
        $item = $this->tiketOrder->findByOrderDetailId($detail->id);
        $temp['service_id'] = Service::FLIGHT;
        $temp['tiket_order_id'] = ($item) ? $item->tiket_order_id : "";
        $temp['tiket_order_detail_id'] = ($item) ? $item->tiket_order_detail_id : "";
        break;

      case ServiceDetail::MD_PLUS_ODEO:
        $item = $this->mdPlusOrder->findByAttributes("order_detail_id", $detail->id);
        $temp['service_id'] = Service::MD_PLUS;
        $temp['item_detail']['store_id'] = $item->store_id;
        $temp['item_detail']['md_plus_plan_id'] = $item->md_plus_plan_id;
        $temp['item_detail']['is_auto_renew'] = $item->is_auto_renew;
        $temp['item_detail']['subscription_months'] = $item->subscription_months;
        $temp['item_detail']['expired_at'] = $item->expired_at;
        $temp['item_detail']['type'] = $item->type;
        break;

      case ServiceDetail::DDLENDING_PAYMENT:
        $item = $this->loanRepo->findByPaymentOrderID($detail->order_id);
        $temp['service_id'] = Service::DDLENDING_PAYMENT;
        $temp['item_detail']['loan_id'] = ($item) ? $item->id : "";
        break;

      case ServiceDetail::EDC_TRANSACTION:
        $item = $this->edcTransactionRepo->findByOrderId($detail->order_id);
        $temp['service_id'] = Service::EDC_TRANSACTION;
        $temp['item_detail']['edc_transaction_id'] = $item->id;
        break;
    }

    $temp['quantity'] = $detail->quantity;
    if (!isset($temp['inventory_id'])) $temp['inventory_id'] = ($item) ? $item->inventory_id : "";
    if (!isset($temp['pulsa_odeo_id'])) $temp['pulsa_odeo_id'] = ($item) ? $item->pulsa_odeo_id : "";
    $temp['service_detail_id'] = $detail->service_detail_id;
    $temp['name'] = $detail->name;
    $temp['price'] = $this->currencyHelper->formatPrice($detail->sale_price);
    $temp['type'] = str_replace('_', ' ', strtolower(Service::getConstKeyByValue($temp['service_id'])));

    if (isset($expand['order_detail_id'])) {
      $temp['order_detail_id'] = $detail->id;
    }
    if (isset($expand['base_price'])) {
      $temp['base_price'] = $this->currencyHelper->formatPrice($detail->base_price);
      $temp['agent_base_price'] = $detail->agent_base_price ? $this->currencyHelper->formatPrice($detail->base_price) : null;
    }
    if (isset($expand['verified_at'])) {
      $temp['verified_at'] = $detail->verified_at;
    }

    $snManager = $this->inquiryFormatter->setPath($detail->service_detail_id);
    if ($snManager != '') {
      $inquiry = $snManager->toOrderInquiry($item, $detail);
      if (isset($inquiry['discount'])) {
        $temp['discount'] = $inquiry['discount'];
        unset($inquiry['discount']);
      }
      $temp['item_detail']['inquiry'] = $inquiry;
    }

    return $temp;
  }


  public function detailizeItem($order, $expand = []) {

    $orderDetail = [];

    $expand = array_flip($expand);

    foreach ($order->details as $detail) {
      $orderDetail[] = $this->extractItem($detail, $expand);
    }

    return $orderDetail;

  }

  public function detailizeCharge($orderId, $expandGroup = []) {

    $chargeList = $this->charges->getCharge($orderId);

    $output = [];

    foreach ($chargeList as $charge) {

      if (in_array($charge->group_type, [
          \Odeo\Domains\Constant\OrderCharge::GROUP_TYPE_CHARGE_TO_COMPANY,
          \Odeo\Domains\Constant\OrderCharge::GROUP_TYPE_COMPANY_PROFIT,
        ]) && !in_array($charge->group_type, $expandGroup)
      ) {
        continue;
      }

      $output[] = [
        'type' => $charge->type,
        'displayed_name' => trans('order.order_charge_' . $charge->type),
        'price' => $this->currencyHelper->formatPrice($charge->amount)
      ];
    }


    return $output;
  }

  public function detailizeReceipt($order) {

    $receiptOrderDetail = [];
    $receiptPaymentDetail = [];
    $item = $order['cart_data']['items'][0];

    array_push($receiptOrderDetail, [
      'label' => trans('order.product_name'),
      'value' => $item['name']
    ]);

    switch ($item['service_id']) {
      case Service::GOOGLE_PLAY:
      case Service::OCASH:
      case Service::ODEPOSIT:
        array_push($receiptPaymentDetail, [
          'label' => trans('order.amount'),
          'value' => $item['price']['formatted_amount']
        ]);
        break;
      case Service::PLAN:
        array_push($receiptOrderDetail, [
          'label' => trans('order.store_name'),
          'value' => $item['item_detail']['store_name'] ?? '-'
        ], [
          'label' => trans('order.store_domain'),
          'value' => $item['item_detail']['store_domain'] ?? '-'
        ]);
        array_push($receiptPaymentDetail, [
          'label' => trans('order.amount_due'),
          'value' => $item['price']['formatted_amount']
        ]);
        break;
      case Service::PLN:
        if (!isset($item['item_detail']['inquiry'])) {
          break;
        }
        array_push($receiptOrderDetail, [
          'label' => trans('order.customer_number'),
          'value' => $item['item_detail']['number']
        ], [
          'label' => trans('order.name'),
          'value' => $item['item_detail']['inquiry']['name'] ?? '-'
        ], [
          'label' => trans('order.customer_id'),
          'value' => $item['item_detail']['inquiry']['subscriber_id'] ?? $item['item_detail']['number']
        ], [
          'label' => trans('order.kwh'),
          'value' => $item['item_detail']['inquiry']['kwh'] ?? '-'
        ], [
          'label' => trans('order.tariff'),
          'value' => $item['item_detail']['inquiry']['tariff'] ?? '-'
        ]);
        array_push($receiptPaymentDetail, [
          'label' => trans('order.product_price'),
          'value' => $item['price']['formatted_amount']
        ]);
        break;
      case Service::PULSA_POSTPAID:
        if (!isset($item['item_detail']['inquiry'])) {
          break;
        }
        array_push($receiptOrderDetail, [
          'label' => trans('order.customer_number'),
          'value' => $item['item_detail']['number']
        ], [
          'label' => trans('order.customer_id'),
          'value' => $item['item_detail']['inquiry']['subscriber_id'] ?? '-'
        ], [
          'label' => trans('order.name'),
          'value' => $item['item_detail']['inquiry']['subscriber_name'] ?? '-'
        ], [
          'label' => trans('order.period'),
          'value' => $item['item_detail']['inquiry']['total_month']
        ]);
        array_push($receiptPaymentDetail, [
          'label' => trans('order.amount_due'),
          'value' => $item['item_detail']['inquiry']['total_bill']['formatted_amount']
        ], [
          'label' => trans('order.admin_fee'),
          'value' => $item['item_detail']['inquiry']['total_admin_fee']['formatted_amount']
        ], [
          'label' => trans('order.discount'),
          'value' => $item['discount']['formatted_amount']
        ]);
        break;
      case Service::LANDLINE:
      case Service::BROADBAND:
        if (!isset($item['item_detail']['inquiry'])) {
          break;
        }
        array_push($receiptOrderDetail, [
          'label' => trans('order.customer_number'),
          'value' => $item['item_detail']['number']
        ], [
          'label' => trans('order.customer_id'),
          'value' => $item['item_detail']['inquiry']['subscriber_id'] ?? '-'
        ], [
          'label' => trans('order.customer_name'),
          'value' => $item['item_detail']['inquiry']['subscriber_name']
        ], [
          'label' => trans('order.total_month'),
          'value' => $item['item_detail']['inquiry']['total_month']
        ]);
        array_push($receiptPaymentDetail, [
          'label' => trans('order.amount_due'),
          'value' => $item['item_detail']['inquiry']['total_bill']['formatted_amount']
        ], [
          'label' => trans('order.admin_fee'),
          'value' => $item['item_detail']['inquiry']['total_admin_fee']['formatted_amount']
        ], [
          'label' => trans('order.discount'),
          'value' => $item['discount']['formatted_amount']
        ]);
        break;
      case Service::PLN_POSTPAID:
        if (!isset($item['item_detail']['inquiry'])) {
          break;
        }
        $ref = $item['item_detail']['inquiry']['ref_number'];
        if (str_contains($ref, "REF:")) {
          $ref = str_after($ref, "REF:");
        }
        array_push($receiptOrderDetail, [
          'label' => trans('order.customer_number'),
          'value' => $item['item_detail']['number']
        ], [
          'label' => trans('order.reference_number'),
          'value' => $ref
        ], [
          'label' => trans('order.customer_id'),
          'value' => $item['item_detail']['inquiry']['subscriber_id']
        ], [
          'label' => trans('order.customer_name'),
          'value' => $item['item_detail']['inquiry']['subscriber_name']
        ], [
          'label' => trans('order.total_month'),
          'value' => $item['item_detail']['inquiry']['total_month']
        ]);
        array_push($receiptPaymentDetail, [
          'label' => trans('order.amount_due'),
          'value' => $item['item_detail']['inquiry']['total_bill']['formatted_amount']
        ], [
          'label' => trans('order.admin_fee'),
          'value' => $item['item_detail']['inquiry']['total_admin_fee']['formatted_amount']
        ], [
          'label' => trans('order.fine'),
          'value' => $item['item_detail']['inquiry']['total_fine']['formatted_amount']
        ], [
          'label' => trans('order.discount'),
          'value' => $item['discount']['formatted_amount']
        ]);
        break;
      case Service::BPJS_KES:
        if (!isset($item['item_detail']['inquiry'])) {
          break;
        }
        array_push($receiptOrderDetail, [
          'label' => trans('order.customer_number'),
          'value' => $item['item_detail']['number']
        ], [
          'label' => trans('order.customer_id'),
          'value' => $item['item_detail']['inquiry']['subscriber_id']
        ], [
          'label' => trans('order.name'),
          'value' => $item['item_detail']['inquiry']['subscriber_name']
        ], [
          'label' => trans('order.period'),
          'value' => $item['item_detail']['inquiry']['month_counts']
        ], [
          'label' => trans('order.reference_number'),
          'value' => $item['item_detail']['inquiry']['ref_number'] ?? '-'
        ]);
        array_push($receiptPaymentDetail, [
          'label' => trans('order.amount_due'),
          'value' => $item['item_detail']['inquiry']['price']['formatted_amount']
        ], [
          'label' => trans('order.admin_fee'),
          'value' => $item['item_detail']['inquiry']['admin_fee']['formatted_amount']
        ], [
          'label' => trans('order.discount'),
          'value' => $item['discount']['formatted_amount']
        ]);
        break;
      case Service::PDAM:
        if (!isset($item['item_detail']['inquiry'])) {
          break;
        }
        array_push($receiptOrderDetail, [
          'label' => trans('order.customer_number'),
          'value' => $item['item_detail']['number']
        ], [
          'label' => trans('order.customer_id'),
          'value' => $item['item_detail']['inquiry']['subscriber_id']
        ], [
          'label' => trans('order.name'),
          'value' => $item['item_detail']['inquiry']['subscriber_name']
        ], [
          'label' => trans('order.period'),
          'value' => $item['item_detail']['inquiry']['total_month']
        ], [
          'label' => trans('order.reference_number'),
          'value' => $item['item_detail']['reff'] ?? '-'
        ]);
        array_push($receiptPaymentDetail, [
          'label' => trans('order.amount_due'),
          'value' => $item['item_detail']['inquiry']['total_bill']['formatted_amount']
        ], [
          'label' => trans('order.admin_fee'),
          'value' => $item['item_detail']['inquiry']['total_admin_fee']['formatted_amount']
        ], [
          'label' => trans('order.fine'),
          'value' => $item['item_detail']['inquiry']['total_fine']['formatted_amount']
        ], [
          'label' => trans('order.discount'),
          'value' => $item['discount']['formatted_amount']
        ]);
        break;
      case Service::GAME_VOUCHER:
        array_push($receiptOrderDetail, [
          'label' => trans('order.player_id'),
          'value' => $item['item_detail']['number']
        ]);
        array_push($receiptPaymentDetail, [
          'label' => trans('order.product_price'),
          'value' => $item['price']['formatted_amount']
        ]);
        break;
      case Service::USER_INVOICE:
        array_push($receiptOrderDetail, [
          'label' => trans('order.customer_name'),
          'value' => $item['name']
        ]);
        array_push($receiptPaymentDetail, [
          'label' => trans('order.product_name'),
          'value' => $item['item_detail']['invoice_number']
        ]);
        array_push($receiptPaymentDetail, [
          'label' => trans('order.product_price'),
          'value' => $item['price']['formatted_amount']
        ]);
        break;
      case Service::MULTI_FINANCE:
        array_push($receiptOrderDetail, [
          'label' => trans('order.customer_id'),
          'value' => $item['item_detail']['inquiry']['subscriber_id']
        ], [
          'label' => trans('order.name'),
          'value' => $item['item_detail']['inquiry']['subscriber_name']
        ], [
          'label' => trans('order.reference_number'),
          'value' => $item['item_detail']['inquiry']['ref_number'] ?? '-'
        ], [
          'label' => trans('order.period'),
          'value' => $item['item_detail']['inquiry']['installment'] ?? '-'
        ], [
          'label' => trans('order.due_date'),
          'value' => $item['item_detail']['inquiry']['due_date'] ?? '-'
        ]);
        if (isset($item['item_detail']['inquiry']['coll_fee'])) {
          array_push($receiptOrderDetail, [
            'label' => $item['item_detail']['inquiry']['coll_fee']['label'],
            'value' => $item['item_detail']['inquiry']['coll_fee']['formatted_amount']
          ]);
        }

        array_push($receiptPaymentDetail, [
          'label' => trans('order.amount_due'),
          'value' => $item['item_detail']['inquiry']['price']['formatted_amount'] ?? '-'
        ], [
          'label' => $item['item_detail']['inquiry']['fine']['label'],
          'value' => $item['item_detail']['inquiry']['fine']['formatted_amount']
        ], [
          'label' => trans('order.admin_fee'),
          'value' => $item['item_detail']['inquiry']['admin_fee']['formatted_amount'] ?? '-'
        ], [
          'label' => trans('order.discount'),
          'value' => $item['discount']['formatted_amount']
        ]);
        break;
      case Service::PGN:
        array_push($receiptOrderDetail, [
          'label' => trans('order.customer_id'),
          'value' => $item['item_detail']['inquiry']['subscriber_id']
        ], [
          'label' => trans('order.name'),
          'value' => $item['item_detail']['inquiry']['subscriber_name']
        ], [
          'label' => trans('order.period'),
          'value' => $item['item_detail']['inquiry']['period'] ?? '-'
        ], [
          'label' => trans('order.meter_change'),
          'value' => $item['item_detail']['inquiry']['meter_changes'] ?? '-'
        ], [
          'label' => trans('order.usage'),
          'value' => $item['item_detail']['inquiry']['usages'] ?? '-'
        ], [
          'label' => trans('order.reference_number'),
          'value' => $item['item_detail']['inquiry']['ref_number'] ?? '-'
        ]);
        array_push($receiptPaymentDetail, [
          'label' => trans('order.amount_due'),
          'value' => $item['item_detail']['inquiry']['price']['formatted_amount'] ?? '-'
        ], [
          'label' => trans('order.admin_fee'),
          'value' => $item['item_detail']['inquiry']['admin_fee']['formatted_amount'] ?? '-'
        ], [
          'label' => trans('order.discount'),
          'value' => $item['discount']['formatted_amount']
        ]);
        break;
      default:
        array_push($receiptOrderDetail, [
          'label' => trans('order.product_name'),
          'value' => $item['name']
        ]);
        array_push($receiptPaymentDetail, [
          'label' => trans('order.product_price'),
          'value' => $item['price']['formatted_amount']
        ]);
    }

    foreach ($order['cart_data']['charges'] as $charge) {
      array_push($receiptPaymentDetail, [
        'label' => $charge['displayed_name'],
        'value' => $charge['price']['formatted_amount']
      ]);
    }

    return [
      'receipt_order_detail' => $receiptOrderDetail,
      'receipt_payment_detail' => $receiptPaymentDetail
    ];
  }

}
