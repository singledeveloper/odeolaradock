<?php

namespace Odeo\Domains\Order\Helper;

use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Order\Model\Order;

class OrderParser {

  public function __construct() {
    $this->validator = app()->make(\Odeo\Domains\Payment\Helper\PaymentValidator::class);
    $this->channels = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);
    $this->payments = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);
  }

  public function status($order) {

    $payment = $this->payments->findByOrderId($order->id);

    if ($order->status == OrderStatus::CREATED) {
       return "PENDING_AT_PAY";
    } else if ($order->status == OrderStatus::OPENED) {
      if ($payment && ($this->validator->isBankTransfer($payment->info_id) ||
        $this->validator->isAlfa($payment->info_id) ||
        $this->validator->isAtmTransferVa($payment->info_id))) {
        return "PENDING_AT_CONFIRM";
      }  else {
        return "PENDING_AT_PAY";
      }
    } else if ($order->status == OrderStatus::CONFIRMED || $order->status == OrderStatus::FAKE_CONFIRMED) {
      return "PENDING_AT_CONFIRM_CAN_CANCEL";
    } else if ($order->status == OrderStatus::VERIFIED || $order->status == OrderStatus::PARTIAL_FULFILLED) {
      return "PENDING_WAIT_FOR_ACCEPTANCE";
    } else if ($order->status == OrderStatus::CANCELLED) {
      return "CANCELED";
    } else if (in_array($order->status, [
      OrderStatus::FREE_ORDER_COMPLETED, OrderStatus::COMPLETED
    ])) {
      return "OK";
    } else {
      return 'PENDING';
    }
  }
}
