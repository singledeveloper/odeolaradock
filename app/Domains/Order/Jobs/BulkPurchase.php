<?php

namespace Odeo\Domains\Order\Jobs;

use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Jobs\Job;

class BulkPurchase extends Job  {

  use SerializesModels;

  private $bulkId, $storeId, $userId, $jabberId, $jabberFrom, $message;

  public function  __construct($bulkId, $storeId, $userId, $jabberId, $jabberFrom, $message) {
    parent::__construct();
    $this->bulkId = $bulkId;
    $this->storeId = $storeId;
    $this->userId = $userId;
    $this->jabberId = $jabberId;
    $this->jabberFrom = $jabberFrom;
    $this->message = $message;
  }

  public function handle() {

    $bulkPurchaseRepo = app()->make(\Odeo\Domains\Order\Repository\PulsaBulkPurchaseRepository::class);

    $pipeline = new Pipeline();
    $pipeline->add(new Task(\Odeo\Domains\Order\Vendor\Jabber\OrderPurchaser::class, 'parse'));
    $pipeline->execute([
      'store_id' => $this->storeId,
      'auth' => [
        'user_id' => $this->userId,
        'type' => UserType::SELLER,
        'platform_id' => Platform::JABBER
      ],
      'jabber_user_id' => $this->jabberId,
      'jabber_store_id' => $this->storeId,
      'jabber_message' => $this->message,
      'jabber_from' => $this->jabberFrom,
      'no_check_history' => true
    ]);

    $reply = '';
    if ($pipeline->getResponseMessage(InlineJabber::RES_PARAM) != '') {
      $reply = htmlspecialchars(strip_tags($pipeline->getResponseMessage(InlineJabber::RES_PARAM)));
      if (strpos($reply, 'GAGAL') !== false) $reply .= '; ' . $this->message;
    }

    $orderId = isset($pipeline) && isset($pipeline->data['order_id']) ? $pipeline->data['order_id'] : null;

    $bulkPurchase = $bulkPurchaseRepo->findById($this->bulkId);
    $bulkPurchase->order_id = $orderId;
    $bulkPurchase->response_message = $reply;
    $bulkPurchase->is_bulk_processed = true;
    $bulkPurchaseRepo->save($bulkPurchase);

  }

}
