<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/7/17
 * Time: 5:35 PM
 */

namespace Odeo\Domains\Order\Jobs;


use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\WriterFactory;
use Odeo\Domains\Order\BulkPurchaseSelector;
use Odeo\Domains\Order\Repository\PulsaBulkPurchaseRepository;
use Odeo\Domains\Order\Repository\PulsaBulkRepository;
use Odeo\Jobs\Job;

class BulkPurchaseExport extends Job  {
  protected $filters, $exportId;

  public function __construct($filters, $exportId) {
    parent::__construct();
    $this->filters = $filters;
    $this->exportId = $exportId;
  }

  public function handle() {
    $exportQueueManager = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class);

    $bulkRepo = app()->make(PulsaBulkRepository::class);
    $bulkPurchaseRepo = app()->make(PulsaBulkPurchaseRepository::class);
    $bulkPurchaseSelector = app()->make(BulkPurchaseSelector::class);
    $bulkPurchaseRepo->normalizeFilters($this->filters);

    $writer = WriterFactory::create($this->filters['file_type']);
    ob_start();

    $writer->openToFile('php://output');
    $writer->addRow([
      'Order ID',
      'Number',
      'Requested At',
      'Nominal',
      'Total',
      'SN / Response',
      'Status'
    ]);

    $list = $bulkPurchaseRepo->gets(true);

    foreach ($list as $item) {
      $item = $bulkPurchaseSelector->_transforms($item, $bulkPurchaseRepo);
      $writer->addRowWithStyle([
        $item['order_id'],
        $item['number'],
        $item['requested_at'],
        $item['nominal'],
        $item['total'] ? $item['total']['formatted_amount'] : '',
        !$item['order_id'] && $item['status'] == '40000' ? $item['response'] : $item['serial_number'],
        $item['status_message']
      ], (new StyleBuilder())->setShouldWrapText(true)->build());
    }

    $writer->close();

    list($file, $hasData) = [ob_get_clean(), $list->isNotEmpty()];

    if (!$hasData) {
      $exportQueueManager->error($this->exportId, 'You don\'t have any data to be exported.');
      return;
    }

    $bulk = $bulkRepo->findById($this->filters['pulsa_bulk_id']);
    $fileName = 'Laporan Bulk Purchase - ' . str_replace(['+', '.', "\n", "\t"], '_', $bulk->name) . '.' . $this->filters['file_type'];
    $exportQueueManager->update($this->exportId, $file, $fileName);
  }
}
