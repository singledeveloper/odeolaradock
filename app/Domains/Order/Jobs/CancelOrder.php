<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/30/17
 * Time: 16:47
 */

namespace Odeo\Domains\Order\Jobs;


use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\OrderCanceller;
use Odeo\Jobs\Job;

class CancelOrder extends Job {

  private $orderId;

  public function __construct($orderId) {
    parent::__construct();
    $this->orderId = $orderId;
  }

  public function handle() {

    $pipeline = new Pipeline;

    $pipeline->add(new Task(OrderCanceller::class, 'cancel'));
    $pipeline->enableTransaction();
    $pipeline->execute([
      'order_id' => $this->orderId,
      'auth' => [
        'type' => 'admin'
      ]
    ]);

  }
}
