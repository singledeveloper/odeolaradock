<?php

namespace Odeo\Domains\Order\Jobs;

use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\PulsaRecurring\OrderPurchaser;
use Odeo\Jobs\Job;

class PurchasePulsaRecurring extends Job  {

  use SerializesModels;

  private $recurringId;

  public function  __construct($recurringId) {
    parent::__construct();
    $this->recurringId = $recurringId;
  }

  public function handle() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(OrderPurchaser::class, 'purchase'));
    $pipeline->execute(['pulsa_recurring_id' => $this->recurringId]);

  }

}
