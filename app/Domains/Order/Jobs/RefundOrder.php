<?php

namespace Odeo\Domains\Order\Jobs;


use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\OrderRefunder;
use Odeo\Jobs\Job;

class RefundOrder extends Job  {

  use SerializesModels;

  private $orderId, $orderDetailId, $orderDetailPulsaSwitcherId;

  public function  __construct($orderId, $orderDetailId, $orderDetailPulsaSwitcherId) {
    parent::__construct();
    $this->orderId = $orderId;
    $this->orderDetailId = $orderDetailId;
    $this->orderDetailPulsaSwitcherId = $orderDetailPulsaSwitcherId;
  }

  public function handle() {

    $pipeline = new Pipeline;
    $pipeline->add(new Task(OrderRefunder::class, 'refundCli'));
    $pipeline->execute([
      'order_id' => $this->orderId,
      'order_detail_id' => $this->orderDetailId,
      'order_detail_pulsa_switcher_id' => $this->orderDetailPulsaSwitcherId
    ]);

  }

}
