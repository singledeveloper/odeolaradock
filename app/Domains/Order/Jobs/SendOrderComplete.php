<?php
namespace Odeo\Domains\Order\Jobs;

use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Constant\BillerPundi;
use Odeo\Domains\Payment\Jobs\EmailHelper;
use Odeo\Jobs\Job;

class SendOrderComplete extends Job {

  use SerializesModels, EmailHelper;

  private $order, $data;
  private $paymentValidator, $orderDetailizer, $currencyHelper, $paymentChannels;

  public function __construct($order, $data) {
    parent::__construct();
    $this->order = $order;
    $this->data = $data;
  }

  public function handle() {
    
    $this->orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->paymentValidator = app()->make(\Odeo\Domains\Payment\Helper\PaymentValidator::class);

    if ($this->order->actual_store_id == BillerPundi::ODEO_STORE_ID) return;

    $this->paymentChannels = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);

    $response = [];

    if ($response['email'] = $this->getEmailAndAdditionalEmail($this->order, $this->data)) ;
    else return;

    $paymentData = $this->order->payment;

    $response['date_opened'] = $this->order->created_at;
    $response['date_closed'] = $this->order->closed_at;
    $response['date_expired'] = $this->order->expired_at;
    $response['date_settlement'] = $this->order->settlement_at;
    $response['settlement_limit'] = Carbon::parse($this->order->settlement_at)->diffInHours(Carbon::parse($this->order->closed_at));

    $response['order_id'] = $this->order->id;
    $response['platform_id'] = $this->order->platform_id;
    $response['cart_data']['items'] = $this->orderDetailizer->detailizeItem($this->order, [
      'mask_phone_number'
    ]);
    $response['cart_data']['charges'] = $this->orderDetailizer->detailizeCharge($this->order->id);
    $response['cart_data']['total'] = $this->currencyHelper->formatPrice($this->order->total);
    $response['cart_data']['subtotal'] = $this->currencyHelper->formatPrice($this->order->subtotal);
    $response['payment_data'] = $paymentData->information->toArray();

    $response = array_merge($response, $this->orderDetailizer->detailizeReceipt($response));
    $response = array_merge($response, $this->getStoreData($this->order->actual_store_id));

    $response['email_info'] = $this->getEmailInfo($this->order->actual_store_id);

    $response['confirmation_link'] = $this->_generateOrderConfLink();

    $response['customer_name'] = $this->order->name;
    $response['purchase_at'] = Carbon::parse($this->order->created_at)->format('d F Y h:i A');

    Mail::send('emails.order_complete', ['data' => $response], function ($m) use ($response) {

      $m->from($response['email_info']['sender_email'], $response['email_info']['sender_name']);

      $m->to($response['email'])->subject('[' . strtoupper($response['email_info']['sender_name']) . '] Order Complete - ' . $response['order_id']);
    });
  }

  private function _generateOrderConfLink() {
    if(app()->environment() == 'production') {
      $domain = 'https://m.odeo.co.id';
    } else {
      $domain = 'http://m.staging.odeo.co.id';
    }

    return $domain . '/order/confirm/' . $this->order->id;
  }

}
