<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/7/17
 * Time: 5:35 PM
 */

namespace Odeo\Domains\Order\Jobs;


use Box\Spout\Writer\WriterFactory;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendOrderReport extends Job  {
  protected $filters, $email;

  public function __construct($filters, $email) {
    parent::__construct();
    $this->filters = $filters;
    $this->email = $email;
  }

  public function handle() {
    $orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $statusParser = app()->make(\Odeo\Domains\Order\Helper\StatusParser::class);
    $writer = WriterFactory::create($this->filters['file_type']);

    ob_start();

    $writer->openToFile('php://output');
    $orders->normalizeFilters($this->filters);
    $report = $orders->getUserPurchaseReport();

    $writer->addRow([
      'Tanggal Transaksi',
      'Order Id',
      'Toko',
      'Barang',
      'Nomor Tujuan',
      'Serial Number',
      'Harga',
      'Pembayaran',
      'Status'
    ]);

    while($row = $report->fetch(\PDO::FETCH_ASSOC)) {
      $writer->addRow([
        $row['created_at'],
        $row['id'],
        $row['store_name'] ?? 'odeo',
        $row['item_name'],
        $row['item_number'] ?? '',
        $row['item_serial_number'],
        $row['item_price'],
        $row['payment_name'],
        $statusParser->parse($row)['status_message']
      ]);
    }

    $writer->close();

    $file = ob_get_clean();

    Mail::send('emails.email_transaction_report', [
      'data' => $this->filters
    ], function ($m) use ($file) {

      $m->from('noreply@odeo.co.id', 'odeo');

      $m->attachData($file, 'Laporan Pembelian ' . $this->filters['start_date'] . ' - ' . $this->filters['end_date'] . '.' . $this->filters['file_type']);

      $m->to($this->email)->subject('[ODEO] Laporan Pembelian');

    });
  }
}
