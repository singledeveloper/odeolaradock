<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 4/12/17
 * Time: 2:35 PM
 */

namespace Odeo\Domains\Order\Jobs;

use Odeo\Domains\Payment\Jobs\EmailHelper;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendRefundMail extends Job  {
  
  use EmailHelper;

  protected $order, $orderDetail, $paymentInfoId, $switcher, $paymentInformations;

  function __construct($order, $orderDetail, $paymentInfoId, $switcher) {
    parent::__construct();
    $this->order = $order;
    $this->orderDetail = $orderDetail;
    $this->paymentInfoId = $paymentInfoId;
    $this->switcher = $switcher;
  }

  public function handle() {
    
    $this->paymentInformations = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelInformationRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

    if ($this->order['email']) {

      Mail::send('emails.refund_alert', [
        'order' => $this->order,
        'orderDetail' => $this->orderDetail,
        'itemNumber' => isset($this->switcher) ? revertTelephone($this->switcher->number) : '',
        'paymentInfo' => $this->paymentInformations->findById($this->paymentInfoId),
        'total' => $this->currencyHelper->formatPrice($this->order->total),
        'store' => $this->getStoreData($this->order->actual_store_id)
      ], function ($m) {
        $m->from('noreply@odeo.co.id', 'odeo');
        $m->to($this->order['email'])->subject('[ODEO] Refund Order - ' . $this->order['id']);
      });
    }
  }
}
