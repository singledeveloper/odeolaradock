<?php
namespace Odeo\Domains\Order\Jobs;

use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Constant\BillerPundi;
use Odeo\Domains\Constant\SMS;
use Odeo\Domains\Vendor\SMS\Job\SendSms;
use Odeo\Jobs\Job;

class SendSmsOrderComplete extends Job {

  use SerializesModels;

  private $order;
  private $smsOrderDetailizer, $currencyHelper, $paymentChannels;

  public function __construct($order) {
    parent::__construct();
    $this->order = $order;
    $this->smsOrderDetailizer = app()->make(\Odeo\Domains\Order\Helper\SmsOrderDetailizer::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

  }

  public function handle() {
    if ($this->order->actual_store_id == BillerPundi::ODEO_STORE_ID) return;

    $destinationPhoneNumber = purifyTelephone($this->order->additional_phone_number);
    $detail = $this->smsOrderDetailizer->detailizeItem($this->order, [
      'mask_phone_number'
    ]);
    $productName = $detail['name'];
    $customerNumber = $detail['number'];
    $total = $this->currencyHelper->formatPrice($this->order->total)['formatted_amount'];

    dispatch(new SendSms([
      'sms_destination_number' => $destinationPhoneNumber,
      'sms_text' => 'Pembelian ' . $productName . ' dengan nomor ' . $customerNumber . ' dan total harga ' . $total . '  berhasil',
      'sms_path' => SMS::NEXMO
    ]));

  }

}
