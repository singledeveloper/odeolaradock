<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/19/17
 * Time: 12:56 PM
 */

namespace Odeo\Domains\Order\Jobs;


use Box\Spout\Writer\WriterFactory;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendStoreOrderReport extends Job  {
  private $filters, $email;

  public function __construct($filters, $email) {
    parent::__construct();
    $this->filters = $filters;
    $this->email = $email;
  }

  public function handle() {
    $orderStores = app()->make(\Odeo\Domains\Order\Repository\OrderStoreRepository::class);
    $statusParser = app()->make(\Odeo\Domains\Order\Helper\StatusParser::class);

    $writer = WriterFactory::create($this->filters['file_type']);

    ob_start();

    $writer->openToFile('php://output');
    $orderStores->normalizeFilters($this->filters);
    $report = $orderStores->getExportData();

    $writer->addRow([
      'Tanggal Transaksi',
      'Order Id',
      'Barang',
      'Nomor Tujuan',
      'Serial Number',
      'Harga',
      'Pembayaran',
      'Status'
    ]);

    while($row = $report->fetch(\PDO::FETCH_ASSOC)) {
      $length = strlen($row['item_number']);
      $writer->addRow([
        $row['created_at'],
        $row['id'],
        $row['item_name'],
        $row['item_number'] ? ($row['is_community'] ? str_repeat('*', max(0, $length - 4)) . substr($row['item_number'], $length - 4) : $row['item_number']) : '',
        $row['item_serial_number'],
        $row['item_price'],
        $row['payment_name'],
        $statusParser->parse($row)['status_message']
      ]);
    }

    $writer->close();

    $file = ob_get_clean();

    Mail::send('emails.email_transaction_report', [
      'data' => $this->filters,
      'title' => 'Laporan Penjualan Toko'
    ], function ($m) use ($file) {

      $m->from('noreply@odeo.co.id', 'odeo');

      $m->attachData($file, 'Laporan Penjualan Toko ' . $this->filters['start_date'] . ' - ' . $this->filters['end_date'] . '.' . $this->filters['file_type']);

      $m->to($this->email)->subject('[ODEO] Laporan Penjualan Toko');

    });
  }

}
