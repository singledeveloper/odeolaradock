<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 10/4/16
 * Time: 7:58 PM
 */

namespace Odeo\Domains\Order\Jobs;


use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\OrderVerificator;
use Odeo\Jobs\Job;

class VerifyOrder extends Job  {

  use SerializesModels;

  private $orderId;

  public function  __construct($orderId) {
    parent::__construct();
    $this->orderId = $orderId;
  }

  public function handle() {

    $pipeline = new Pipeline;

    $pipeline->add(new Task(OrderVerificator::class, 'verify'));
    $pipeline->add(new Task(OrderVerificator::class, 'completeOrder'));
    $pipeline->execute([
      'order_id' => $this->orderId,
      'auth' => [
        'type' => 'admin'
      ]
    ]);

    if ($pipeline->fail()) {
      \Log::notice('With ' . $this->orderId . ', ' . $pipeline->errorMessage);
    }

  }

}
