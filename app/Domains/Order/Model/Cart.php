<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 9:22 PM
 */

namespace Odeo\Domains\Order\Model;


use Odeo\Domains\Core\Entity;

class Cart extends Entity {

  protected $table = 'user_carts';

}