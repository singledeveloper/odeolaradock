<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 9:21 PM
 */

namespace Odeo\Domains\Order\Model;


use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;
use Odeo\Domains\Payment\Model\Payment;
use Odeo\Domains\Subscription\Model\Store;

class Order extends Entity {

  public $timestamps = false;

  public function details() {
    return $this->hasMany(OrderDetail::class);
  }

  public function payment() {
    return $this->hasOne(Payment::class);
  }

  public function user() {
    return $this->belongsTo(User::class);
  }

  public function sellerStore() {
    return $this->belongsTo(Store::class, 'seller_store_id');
  }

  public function actualStore() {
    return $this->belongsTo(Store::class, 'actual_store_id');
  }

  public function charges() {
    return $this->hasMany(OrderCharge::class);
  }
}
