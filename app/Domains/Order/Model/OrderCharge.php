<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 4:02 PM
 */

namespace Odeo\Domains\Order\Model;


use Odeo\Domains\Core\Entity;

class OrderCharge extends Entity {

  public $timestamps = false;

  public static function boot() {
    parent::boot();

    static::creating(function($model) {
      $timestamp = $model->freshTimestamp();
      $model->setCreatedAt($timestamp);
    });
  }
}