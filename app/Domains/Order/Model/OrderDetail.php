<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 9:21 PM
 */

namespace Odeo\Domains\Order\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\OrderDetailPulsaSwitcher;
use Odeo\Domains\Inventory\Model\ServiceDetail;
use Odeo\Domains\Inventory\UserInvoice\Model\OrderDetailUserInvoice;
use Odeo\Domains\Subscription\Model\OrderDetailPlan;

class OrderDetail extends Entity {

  public $timestamps = false;
  
  public function order() {
    return $this->belongsTo(Order::class);
  }

  public function serviceDetail() {
    return $this->belongsTo(ServiceDetail::class);
  }

  public function orderDetailPlans() {
    return $this->hasOne(OrderDetailPlan::class);
  }

  public function switcher() {
    return $this->hasOne(OrderDetailPulsaSwitcher::class);
  }

  public function userInvoice() {
    return $this->hasOne(OrderDetailUserInvoice::class);
  }
}