<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/31/17
 * Time: 22:16
 */

namespace Odeo\Domains\Order;

use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\StoreTempDeposit;
use Odeo\Domains\Core\PipelineListener;

class OrderAgentSalesProcessor {

  public function __construct() {
    $this->depositConverter = app()->make(\Odeo\Domains\Transaction\Helper\DepositConverter::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->agentMasterSalesManager = app()->make(\Odeo\Domains\Order\Helper\AgentMasterSalesManager::class);
    $this->storeTempDeposits = app()->make(\Odeo\Domains\Transaction\Repository\StoreTempDepositRepository::class);
    $this->tempDepositHelper = app()->make(\Odeo\Domains\Transaction\Helper\TempDepositHelper::class);
    $this->revenueInserter = app()->make(\Odeo\Domains\Marketing\Helper\RevenueInserter::class);
    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->vendorSwitcherRecons = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository::class);
  }

  public function convertDeposit(PipelineListener $listener, $data) {

    $agentStoreIds = [];

    if (!$data['order']['is_agent'] || $data['order']['status'] != OrderStatus::COMPLETED) {
      return $listener->response(200);
    }

    $pushNotification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $pushNotification->pushOnly();

    $agentSales = $this->storeTempDeposits->findByOrderId($data['order_id'], [
      StoreTempDeposit::AGENT_SERVER_ORDER, StoreTempDeposit::AGENT_ORDER, StoreTempDeposit::SUPPLIER_CASH
    ]);

    if ($agentSales->isEmpty()) {
      return $listener->response(200);
    }

    $disableMentorLeader = false;

    $isSupplierCashAlreadyProcessed = false;

    foreach ($agentSales as $sale) {

      $desc = json_decode($sale->description);

      $disableMentorLeader = $disableMentorLeader || isset($desc->disable_mentor_leader);

      if ($sale->type == StoreTempDeposit::SUPPLIER_CASH) {
        if (!$isSupplierCashAlreadyProcessed) {
          $supplySalesInserter = app()->make(\Odeo\Domains\Supply\Formatter\SupplySalesInserter::class);
          $supplySalesData = $supplySalesInserter->getCashInserterData([
            'user_id' => $desc->store_user_id,
            'order_id' => $sale->reference_id,
            'amount' => $sale->sale_amount,
            'settlement_at' => isset($data['order']['settlement_at']) ? $data['order']['settlement_at'] : ''
          ]);

          if ($recon = $this->vendorSwitcherRecons->findByOrderId($sale->reference_id)) {
            $recon->sale_amount = $recon->sale_amount == null ? intval($sale->sale_amount) : (intval($sale->sale_amount) + $recon->sale_amount);
            $this->vendorSwitcherRecons->save($recon);
          }

          foreach ($supplySalesData as $item) $this->cashInserter->add($item);
          $isSupplierCashAlreadyProcessed = true;
        }
        else clog('supply_sales_data', json_encode($agentSales));
      }
      else {
        $trxData = [
          'order_id' => $data['order_id'],
          'recent_desc' => $desc->store_name,
        ];

        if ($data['order']['seller_store_id'] != $sale->store_id) {
          $trxData['is_community'] = true;
          $trxData['role'] = 'agent';
        }

        if (isset($data['order']['settlement_at'])) {
          $trxData['settlement_at'] = $data['order']['settlement_at'];
        }

        if (isset($desc->item_from_store_id)) {
          $trxData['item_from_store_id'] = $desc->item_from_store_id;
        }

        $this->depositConverter->convert([
          'order_id' => $data['order_id'],
          'user_id' => $desc->store_user_id,
          'store_id' => $sale->store_id,
          'paid_amount' => $sale->sale_amount,
          'deposit_amount' => $sale->amount,
          'revenue_type' => 'agent',
          'convert_type' => $sale->type == StoreTempDeposit::AGENT_ORDER && isset($desc->pay_in_ocash)
          && $desc->pay_in_ocash ? 'agent' : 'agent_server'
        ], $trxData);

        $pushNotification->setup($desc->store_user_id, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
        $pushNotification->order_bonus($sale->sale_amount, $desc->store_name, 'revenue');
      }

      $this->revenueInserter->add([
        'store_id' => $sale->store_id,
        'service_id' => $data['service_id'],
        'amount' => $sale->sale_amount,
        'margin' => $sale->sale_amount - $sale->amount,
        'rush' => 0,
        'is_free' => false
      ]);

      $this->agentMasterSalesManager->update([
        'date' => $data['order']['purchase_at'],
        'user_id' => $desc->purchaser_user_id,
        'store_id' => $sale->store_id,
        'service_id' => $data['service_id'],
        'quantity' => 1,
        'sale_amount' => $sale->sale_amount,
        'base_sale_amount' => $sale->amount
      ]);

      $agentStoreIds[] = $sale->store_id;
    }

    $this->tempDepositHelper->deleteOnlyAgentAndServerDeposit($data['order_id']);

    $this->revenueInserter->run();

    $this->cashInserter->run();

    // $listener->pushQueue($pushNotification->queue());

    return $listener->response(200, [
      'agent_store_ids' => $agentStoreIds,
      'disable_mentor_leader' => $disableMentorLeader
    ]);
  }
}
