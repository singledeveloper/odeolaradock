<?php

namespace Odeo\Domains\Order;

use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Account\Repository\UserGroupRepository;
use Odeo\Domains\Account\UserHistoryCreator;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Bolt\BoltManager;
use Odeo\Domains\Inventory\BPJS\BPJSManager;
use Odeo\Domains\Inventory\Broadband\BroadbandManager;
use Odeo\Domains\Inventory\EMoney\EMoneyManager;
use Odeo\Domains\Inventory\GameVoucher\GameVoucherManager;
use Odeo\Domains\Inventory\GooglePlay\GooglePlayManager;
use Odeo\Domains\Inventory\Landline\LandlineManager;
use Odeo\Domains\Inventory\MultiFinance\MultiFinanceManager;
use Odeo\Domains\Inventory\PDAM\PDAMManager;
use Odeo\Domains\Inventory\PGN\PGNManager;
use Odeo\Domains\Inventory\Pln\PlnManager;
use Odeo\Domains\Inventory\Pulsa\PulsaManager;
use Odeo\Domains\Inventory\Transportation\TransportationManager;
use Odeo\Domains\Order\Jobs\SendRefundMail;
use Odeo\Domains\Order\Jobs\SendSmsTransfer;
use Odeo\Domains\Transaction\CashRequester;
use Odeo\Domains\Transaction\TransactionRequester;

class OrderRefunder {


  private $orderDetail, $orderDetailizer, $serviceDetails, $paymentValidator,
    $inserter, $currency, $user, $paymentVoidRequester, $gatewayCallback, $userHistoryCreator;

  public function __construct() {
    $this->user = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->order = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->orderDetail = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
    $this->orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->paymentValidator = app()->make(\Odeo\Domains\Payment\Helper\PaymentValidator::class);
    $this->inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->paymentVoidRequester = app()->make(\Odeo\Domains\Payment\PaymentVoidRequester::class);
    $this->gatewayCallback = app()->make(\Odeo\Domains\Order\Helper\GatewayCallbacker::class);
    $this->tempDeposit = app()->make(\Odeo\Domains\Transaction\Repository\StoreTempDepositRepository::class);
    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->tempDepositHelpers = app()->make(\Odeo\Domains\Transaction\Helper\TempDepositHelper::class);
    $this->userHistoryCreator = app()->make(UserHistoryCreator::class);
  }

  public function refundCli(PipelineListener $listener, $data) {
    $orderDetail = $this->orderDetail->findById($data['order_detail_id']);
    $serviceDetail = $this->serviceDetails->findById($orderDetail->service_detail_id);
    $order = $orderDetail->order;
    $orderStatus = $order->status;

    if (OrderStatus::isRefunded($order->status)) return $listener->response(200);

    $orderDetail->refunded_at = date("Y-m-d H:i:s");
    $this->orderDetail->save($orderDetail);

    if (!$order->confirm_receipt) {
      $order->status = OrderStatus::REFUNDED;
    } else {
      $order->status = OrderStatus::REFUNDED_AFTER_SETTLEMENT;
    }

    $this->order->save($order);

    if ($switcher = $orderDetail->switcher) {
      $cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
      $switchers = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
      $callbackMessage = trans('pulsa.inline.refund', [
        'order_id' => $order->id,
        'item_name' => $orderDetail->name,
        'number' => revertTelephone($switcher->number),
        'sisa_saldo' => $cashManager->findOrderHistory($order, TransactionType::REFUND)['message']
      ]);
      $switcher->h2h_message = $callbackMessage;
      $switchers->save($switcher);
      $this->gatewayCallback->callback($listener, $order, $callbackMessage, Supplier::RC_FAIL);
    }

    if ($orderDetail->agent_base_price != null) {
      $this->tempDepositHelpers->refundAgentDeposit($order->id);
    }

    $this->tempDepositHelpers->deleteDeposit($order->id);

    $payments = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);

    $paymentInfoId = $payments->findByOrderId($order->id)->info_id;

    switch ($serviceDetail->service_id) {
      case Service::PULSA:
      case Service::PAKET_DATA:
      case Service::PULSA_POSTPAID:
        $listener->addNext(new Task(PulsaManager::class, 'refund', [
          'order_detail_pulsa_switcher_id' => $data['order_detail_pulsa_switcher_id'],
          'service_detail_id' => $orderDetail->service_detail_id
        ]));
        break;
      case Service::PLN:
      case Service::PLN_POSTPAID:
        $listener->addNext(new Task(PlnManager::class, 'refund', [
          'order_detail_pulsa_switcher_id' => $data['order_detail_pulsa_switcher_id'],
          'service_detail_id' => $orderDetail->service_detail_id
        ]));
        break;
      case Service::BOLT:
        $listener->addNext(new Task(BoltManager::class, 'refund', [
          'order_detail_pulsa_switcher_id' => $data['order_detail_pulsa_switcher_id'],
          'service_detail_id' => $orderDetail->service_detail_id
        ]));
        break;
      case Service::BPJS_KES:
        $listener->addNext(new Task(BPJSManager::class, 'refund', [
          'order_detail_pulsa_switcher_id' => $data['order_detail_pulsa_switcher_id'],
          'service_detail_id' => $orderDetail->service_detail_id
        ]));
        break;
      case Service::BROADBAND:
        $listener->addNext(new Task(BroadbandManager::class, 'refund', [
          'order_detail_pulsa_switcher_id' => $data['order_detail_pulsa_switcher_id'],
          'service_detail_id' => $orderDetail->service_detail_id
        ]));
        break;
      case Service::LANDLINE:
        $listener->addNext(new Task(LandlineManager::class, 'refund', [
          'order_detail_pulsa_switcher_id' => $data['order_detail_pulsa_switcher_id'],
          'service_detail_id' => $orderDetail->service_detail_id
        ]));
        break;
      case Service::PDAM:
        $listener->addNext(new Task(PDAMManager::class, 'refund', [
          'order_detail_pulsa_switcher_id' => $data['order_detail_pulsa_switcher_id'],
          'service_detail_id' => $orderDetail->service_detail_id
        ]));
        break;
      case Service::PGN:
        $listener->addNext(new Task(PGNManager::class, 'refund', [
          'order_detail_pulsa_switcher_id' => $data['order_detail_pulsa_switcher_id'],
          'service_detail_id' => $orderDetail->service_detail_id
        ]));
        break;
      case Service::GAME_VOUCHER:
        $listener->addNext(new Task(GameVoucherManager::class, 'refund', [
          'order_detail_pulsa_switcher_id' => $data['order_detail_pulsa_switcher_id'],
          'service_detail_id' => $orderDetail->service_detail_id
        ]));
        break;
      case Service::GOOGLE_PLAY:
        $listener->addNext(new Task(GooglePlayManager::class, 'refund', [
          'order_detail_pulsa_switcher_id' => $data['order_detail_pulsa_switcher_id'],
          'service_detail_id' => $orderDetail->service_detail_id
        ]));
        break;
      case Service::EMONEY:
        $listener->addNext(new Task(EMoneyManager::class, 'refund', [
          'order_detail_pulsa_switcher_id' => $data['order_detail_pulsa_switcher_id'],
          'service_detail_id' => $orderDetail->service_detail_id
        ]));
        break;
      case Service::TRANSPORTATION:
        $listener->addNext(new Task(TransportationManager::class, 'refund', [
          'order_detail_pulsa_switcher_id' => $data['order_detail_pulsa_switcher_id'],
          'service_detail_id' => $orderDetail->service_detail_id
        ]));
        break;
      case Service::MULTI_FINANCE:
        $listener->addNext(new Task(MultiFinanceManager::class, 'refund', [
          'order_detail_pulsa_switcher_id' => $data['order_detail_pulsa_switcher_id'],
          'service_detail_id' => $orderDetail->service_detail_id
        ]));
        break;
    }

    if (!$order->confirm_receipt) {
      if (!in_array($order->gateway_id, [Payment::JABBER, Payment::TELEGRAM, Payment::SMS_CENTER])) {
        $userGroupRepo = app()->make(UserGroupRepository::class);
        if (!$userGroupRepo->findUserWithinGroup($order->user_id, UserType::GROUP_NO_REFUND_EMAIL)) {
          $listener->pushQueue(new SendRefundMail($order, $orderDetail, $paymentInfoId, $switcher));
        }

        $notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);

        if (isset($data['notification_type'])) {
          $description = (function () use ($data) {
            switch ($data['notification_type']) {
              case 'notification_pln_max_kwh':
                return 'KWH Total Exceeded.';
                break;
              case 'notification_operator_is_currently_down':
                return 'Operator is currently down.';
              case 'notification_operator_fail_to_process_the_number':
                return 'Operator fail to process the number.';
            }
            return '';
          })();
        }

        $notification->setup($order->user_id, NotificationType::CHECK, NotificationGroup::TRANSACTION);
        $notification->refund($order->id, $description ?? '');

        $listener->pushQueue($notification->queue());
      }

      $isCod = substr($order->gateway_id, -2) == '05';

      if ($order->user->type == UserType::GUEST) {
        $user = $order->user;
        $random_pass = rand(100000, 999999);
        $user->type = UserType::SELLER;
        $user->password = Hash::make($random_pass);
        $user->telephone = purifyTelephone($order->phone_number);
        $user->status = UserStatus::RESET_PIN;

        $this->user->save($user);

        $this->userHistoryCreator->create($user);

        $listener->pushQueue(new SendSmsTransfer($user->telephone, $random_pass, $this->currency->formatPrice($orderDetail->sale_price), "refund"));
      }

      if (!$this->paymentVoidRequester->void([
        'order_id' => $order->id,
        'amount' => $isCod ? $orderDetail->base_price : $order->subtotal,
        'user_id' => $order->user_id,
        'paid_at' => $order->paid_at
      ])
      ) {
        return $listener->response(400);
      }

      $listener->addNext(new Task(UserPurchaseUpdater::class, 'update', [
        'service_id' => $serviceDetail->service_id,
        'service_detail_id' => $serviceDetail->id,
        'amount' => -$orderDetail->sale_price,
        'quantity' => -1,
        'platform_id' => $order->platform_id,
        'gateway_id' => $order->gateway_id,
        'info_id' => $paymentInfoId,
        'user_id' => $order->user_id,
        'date' => $order->paid_at,
        'store_id' => $order->actual_store_id ?? null
      ]));

      if ($orderStatus == OrderStatus::COMPLETED) {
        $listener->addNext(new Task(TransactionRequester::class, 'refundOrder', [
          'service_id' => $serviceDetail->service_id,
          'service_detail_id' => $serviceDetail->id
        ]));
      }
    }

    $listener->addNext(new Task(CashRequester::class, 'settleOrder'));

    return $listener->response(200, ['order_id' => $order->id]);
  }

  public function refund(PipelineListener $listener, $data) {
    //if (!isAdmin()) return $listener->response(400, "You don't have credential to access this data.");

    return $this->refundCli($listener, $data);
  }

  public function faultRefund(PipelineListener $listener, $data) {
    if (!isAdmin()) return $listener->response(400, "You don't have credential to access this data.");

    if ($order = $this->order->findById($data['order_id'])) {
      if ($order->status != OrderStatus::COMPLETED) return $listener->response(400, "Order not completed yet.");
      else if ($data['amount'] > $order->total) return $listener->response(400, "Invalid amount.");
      else {
        $this->inserter->add([
          'user_id' => $order->user_id,
          'trx_type' => \Odeo\Domains\Constant\TransactionType::FAULT_ORDER_REFUND,
          'cash_type' => \Odeo\Domains\Constant\CashType::OCASH,
          'amount' => $data['amount'],
          'data' => json_encode([
            'order_id' => $order->id
          ])
        ]);
        $this->inserter->run();
      }
    } else return $listener->response(400, "No order data.");

    return $listener->response(200);
  }
}
