<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/12/17
 * Time: 2:41 PM
 */

namespace Odeo\Domains\Order;

use Carbon\Carbon;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;

class OrderUpdater {

  private $orders;

  public function __construct() {
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
  }

  public function updateStatus(PipelineListener $listener, $data) {

    $order = $this->orders->findById($data['order_id']);

    $order->status = $data['status'];

    $this->orders->save($order);

    return $listener->response(200);
  }

  public function reopen(PipelineListener $listener, $data) {
    $order = $this->orders->findById($data['order_id']);
    $order->status = OrderStatus::CREATED;
    $order->closed_at = null;
    $this->orders->save($order);
  }

  public function confirmReceipt(PipelineListener $listener, $data) {

    $order = $this->orders->findById($data['order_id']);

    if($order->confirm_receipt)
      return $listener->response(400, 'Order sudah pernah dikonfirmasi');

    if ($order->status != OrderStatus::COMPLETED)
      return $listener->response(400, 'Tidak dapat melakukan konfirmasi, order belum sukses.');

    $order->confirm_receipt = true;
    $order->confirmed_at = Carbon::now();

    if(isset($data['rating'])) $order->rating = $data['rating'];
    if(isset($data['feedback'])) $order->feedback = $data['feedback'];

    $this->orders->save($order);

    return $listener->response(200);
  }

  public function updateRating(PipelineListener $listener, $data) {

    $order = $this->orders->findById($data['order_id']);

    if($order->rating) return $listener->response(400, 'Order sudah memiliki rating');

    $order->rating = $data['rating'];
    if(isset($data['feedback'])) $order->feedback = $data['feedback'];

    $this->orders->save($order);

    return $listener->response(200);
  }
}
