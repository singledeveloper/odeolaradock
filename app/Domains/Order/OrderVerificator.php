<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 6:05 PM
 */

namespace Odeo\Domains\Order;

use Carbon\Carbon;
use Odeo\Domains\Biller\Jobs\SendPurchaseWarning;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\DDLending\LoanPaymentRequester;
use Odeo\Domains\Inventory\Bolt\BoltManager;
use Odeo\Domains\Inventory\BPJS\BPJSManager;
use Odeo\Domains\Inventory\Broadband\BroadbandManager;
use Odeo\Domains\Inventory\Flight\FlightManager;
use Odeo\Domains\Inventory\GameVoucher\GameVoucherManager;
use Odeo\Domains\Inventory\GooglePlay\GooglePlayManager;
use Odeo\Domains\Inventory\Hotel\HotelManager;
use Odeo\Domains\Inventory\Landline\LandlineManager;
use Odeo\Domains\Inventory\MultiFinance\MultiFinanceManager;
use Odeo\Domains\Inventory\PDAM\PDAMManager;
use Odeo\Domains\Inventory\PGN\PGNManager;
use Odeo\Domains\Inventory\Pln\PlnManager;
use Odeo\Domains\Inventory\Pulsa\PulsaManager;
use Odeo\Domains\Inventory\Transportation\TransportationManager;
use Odeo\Domains\Marketing\BudgetUpdater;
use Odeo\Domains\Marketing\IncomeUpdater;
use Odeo\Domains\Marketing\RevenueUpdater;
use Odeo\Domains\Network\CommissionRequester;
use Odeo\Domains\Network\ReferralRequester;
use Odeo\Domains\Network\SponsorRequester;
use Odeo\Domains\Order\Jobs\CompleteOrder;
use Odeo\Domains\Order\Jobs\SendOrderComplete;
use Odeo\Domains\Order\Jobs\SendSmsOrderComplete;
use Odeo\Domains\Payment\Jobs\SendPaymentSuccess;
use Odeo\Domains\Payment\Pax\EdcTransactionRequester;
use Odeo\Domains\Subscription\DistributionChannel\ChannelUpdater;
use Odeo\Domains\Subscription\MdPlus\MdPlusRequester;
use Odeo\Domains\Subscription\StoreVerificator;

class OrderVerificator {

  private $orders, $orderDetailizer, $paymentValidator, $h2hManager;

  public function __construct() {
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);
    $this->orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
    $this->bonusCalculator = app()->make(\Odeo\Domains\Order\Helper\OrderBonusCalculator::class);
    $this->paymentValidator = app()->make(\Odeo\Domains\Payment\Helper\PaymentValidator::class);
    $this->h2hManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper\PulsaH2HManager::class);
  }

  public function verify(PipelineListener $listener, $data) {
    $order = $this->orders->findById($data['order_id']);

    $payment = $order->payment;
    $data['opc'] = $payment->opc;

    $this->updateOrderToVerifyStatus($listener, array_merge($data, [
      'order' => $order,
      'skip_pipeline_response' => true
    ]));

    if (!isset($data['store_id'])) {
      $data['store_id'] = $order->seller_store_id;
    }

    $item = $this->orderDetailizer->detailizeItem($order, ['order_detail_id', 'verified_at'])[0];

    if ($item['verified_at']) {
      $listener->addNext(new Task(__CLASS__, 'verifyDetail', [
        "order_detail_id" => $item['order_detail_id'],
        'store_id' => $data['store_id'],
        'no_verify' => true
      ]));
    }

    $noVerify = false;

    switch ($item['service_id']) {
      case Service::PLAN:
        $params = [
          'store_id' => $item['item_detail']['store_id'],
          'order_detail_id' => $item['order_detail_id']
        ];

        $taskStart = new Task(StoreVerificator::class, 'verify', $params);

        $taskStart->startTransaction();
        $taskStart->startSkip();

        $listener->addNext($taskStart);

        $listener->addNext(new Task(ReferralRequester::class, 'create', $params));
        $listener->addNext(new Task(SponsorRequester::class, 'create', $params));
        $listener->addNext(new Task(CommissionRequester::class, 'create', $params));
        $listener->addNext(new Task(BudgetUpdater::class, 'updateBudget'));
        $listener->addNext(new Task(MdPlusRequester::class, 'createFree'));
        $listener->addNext(new Task(ChannelUpdater::class, 'createChannel'));
        $listener->addNext(new Task(RevenueUpdater::class, 'initRevenue'));
        $listener->addNext(new Task(IncomeUpdater::class, 'updateIncome', ['amount' => $item['price']['amount']]));
        break;

      case Service::OCASH:
        $noVerify = true;
        $task = new Task(\Odeo\Domains\Transaction\TopupRequester::class, 'complete', [
          'topup_id' => $item['item_detail']['topup_id'],
          'order_detail_id' => $item['order_detail_id']
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::CREDIT_BILL:
        $task = new Task(\Odeo\Domains\Inventory\Creditbill\Odeo\Biller\Pundi\Requester::class, 'completeOrder', [
          'credit_bill_id' => $item['item_detail']['credit_bill_id'],
          'order_detail_id' => $item['order_detail_id']
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::USER_INVOICE:
        $task = new Task(\Odeo\Domains\Inventory\UserInvoice\UserInvoiceManager::class, 'completeOrder', [
          'service_detail_id' => $item['service_detail_id'],
          'invoice_number' => $item['item_detail']['invoice_number'],
          'order_detail_id' => $item['order_detail_id'],
          'biller_id' => $item['item_detail']['biller_id'],
          'payment_name' => $payment->information->actual_name
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::ODEPOSIT:

        $task = new Task(\Odeo\Domains\Transaction\DepositRequester::class, 'complete', [
          'store_deposit_id' => $item['item_detail']['store_deposit_id'],
          'order_detail_id' => $item['order_detail_id'],
          'user_id' => $order->user_id
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::WARRANTY:
        $task = new Task(\Odeo\Domains\Subscription\WarrantyRequester::class, 'verify', [
          'warranty_id' => $item['item_detail']['warranty_id'],
          'order_detail_id' => $item['order_detail_id']
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::MD_PLUS:
        $task = new Task(\Odeo\Domains\Subscription\MdPlus\MdPlusRequester::class, 'verify', [
          'order_detail_id' => $item['order_detail_id'],
          'store_id' => $item['item_detail']['store_id'],
          'md_plus_plan_id' => $item['item_detail']['md_plus_plan_id'],
          'is_auto_renew' => $item['item_detail']['is_auto_renew'],
          'subscription_months' => $item['item_detail']['subscription_months'],
          'expired_at' => $item['item_detail']['expired_at'],
          'type' => $item['item_detail']['type']
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::PULSA:
      case Service::PAKET_DATA:
        $noVerify = true;
        $task = new Task(PulsaManager::class, 'purchase', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::PLN:
        $noVerify = true;
        $task = new Task(PlnManager::class, 'purchase', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'order_detail_id' => $item['order_detail_id'],
          'number' => $item['item_detail']['number'],
          'store_id' => $data['store_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::TRANSPORTATION:
        $noVerify = true;
        $task = new Task(TransportationManager::class, 'purchase', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'order_detail_id' => $item['order_detail_id'],
          'number' => $item['item_detail']['number'],
          'store_id' => $data['store_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::PLN_POSTPAID:
        $noVerify = true;
        $task = new Task(PlnManager::class, 'purchasePostpaid', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'order_detail_id' => $item['order_detail_id'],
          'number' => $item['item_detail']['number'],
          'store_id' => $data['store_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::PULSA_POSTPAID:
        $noVerify = true;
        $task = new Task(PulsaManager::class, 'purchasePostpaid', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'order_detail_id' => $item['order_detail_id'],
          'number' => $item['item_detail']['number'],
          'store_id' => $data['store_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::BROADBAND:
        $noVerify = true;
        $task = new Task(BroadbandManager::class, 'purchasePostpaid', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'order_detail_id' => $item['order_detail_id'],
          'number' => $item['item_detail']['number'],
          'store_id' => $data['store_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::LANDLINE:
        $noVerify = true;
        $task = new Task(LandlineManager::class, 'purchasePostpaid', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'order_detail_id' => $item['order_detail_id'],
          'number' => $item['item_detail']['number'],
          'store_id' => $data['store_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::PDAM:
        $noVerify = true;
        $task = new Task(PDAMManager::class, 'purchasePostpaid', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'order_detail_id' => $item['order_detail_id'],
          'number' => $item['item_detail']['number'],
          'store_id' => $data['store_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::PGN:
        $noVerify = true;
        $task = new Task(PGNManager::class, 'purchasePostpaid', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'order_detail_id' => $item['order_detail_id'],
          'number' => $item['item_detail']['number'],
          'store_id' => $data['store_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::BPJS_KES:
        $noVerify = true;
        $task = new Task(BPJSManager::class, 'purchasePostpaid', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'order_detail_id' => $item['order_detail_id'],
          'number' => $item['item_detail']['number'],
          'month_counts' => $item['item_detail']['inquiry']['month_counts'], // TO DO BPJS DELETE LATER
          'store_id' => $data['store_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::BOLT:
        $noVerify = true;
        $task = new Task(BoltManager::class, 'purchase', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'order_detail_id' => $item['order_detail_id'],
          'number' => $item['item_detail']['number'],
          'store_id' => $data['store_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::EMONEY:
        $noVerify = true;
        $task = new Task(BoltManager::class, 'purchase', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'order_detail_id' => $item['order_detail_id'],
          'number' => $item['item_detail']['number'],
          'store_id' => $data['store_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::HOTEL:
        $task = new Task(HotelManager::class, 'purchase', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_id' => $item['order_detail_id'],
          'tiket_order_id' => $item['tiket_order_id'],
          'store_id' => $data['store_id']
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::FLIGHT:
        $task = new Task(FlightManager::class, 'purchase', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_id' => $item['order_detail_id'],
          'tiket_order_id' => $item['tiket_order_id'],
          'store_id' => $data['store_id']
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::GAME_VOUCHER:
        $task = new Task(GameVoucherManager::class, 'purchase', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::GOOGLE_PLAY:
        $task = new Task(GooglePlayManager::class, 'purchase', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::MULTI_FINANCE:
        $noVerify = true;
        $task = new Task(MultiFinanceManager::class, 'purchasePostpaid', [
          'service_detail_id' => $item['service_detail_id'],
          'order_detail_pulsa_switcher_id' => $item['order_detail_pulsa_switcher_id'],
          'inventory_id' => $item['inventory_id'],
          'order_detail_id' => $item['order_detail_id'],
          'number' => $item['item_detail']['number'],
          'store_id' => $data['store_id'],
          'route' => SwitcherConfig::ROUTE_ORDER_VERIFICATOR
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;


      case Service::DDLENDING_PAYMENT:
        $task = new Task(LoanPaymentRequester::class, 'completeOrder', [
          'service_id' => $item['service_id'],
          'service_detail_id' => $item['service_detail_id'],
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;

      case Service::EDC_TRANSACTION:
        $task = new Task(EdcTransactionRequester::class, 'completeOrder', [
          'service_id' => $item['service_id'],
          'service_detail_id' => $item['service_detail_id'],
        ]);
        $task->startTransaction();
        $task->startSkip();
        $listener->addNext($task);
        break;
    }

    if ($this->paymentValidator->isBankTransfer($payment->info_id)) {
      $listener->addNext(new Task(\Odeo\Domains\Payment\Odeo\BankTransfer\Helper\UniqueCodeRefunder::class, 'refund', [
        'order' => $order,
        'payment' => $payment
      ]));
    }

    $listener->addNext(new Task(UserPurchaseUpdater::class, 'update', [
      'service_id' => $item['service_id'],
      'service_detail_id' => $item['service_detail_id'],
      'amount' => $item['price']['amount'],
      'quantity' => 1,
      'platform_id' => $order->platform_id,
      'gateway_id' => $order->gateway_id,
      'info_id' => $payment->info_id,
      'user_id' => $order->user_id,
      'date' => $order->paid_at,
      'store_id' => $order->seller_store_id ?? null
    ]));

    $taskEnd = new Task(__CLASS__, 'verifyDetail', [
      "order_detail_id" => $item['order_detail_id'],
      'store_id' => $data['store_id'],
      'no_verify' => $noVerify
    ]);
    $taskEnd->endTransaction();
    $taskEnd->endSkip();

    $listener->addNext($taskEnd);


    if (!in_array($order->gateway_id, [Payment::JABBER, Payment::TELEGRAM, Payment::AFFILIATE, Payment::SMS_CENTER])) {
      $listener->pushQueue(new SendPaymentSuccess($order, $data));
    }

    if (isProduction() && $this->isPurchaseWarningRisk($order->total, $item['service_id'], $order->user_id)) {
      dispatch($this->sendPurchaseWarning($order, $item['name']));
    }

    return $listener->response(200);
  }

  private function isPurchaseWarningRisk($orderTotal, $serviceId, $userId) {
    return $orderTotal >= CashType::AMOUNT_WARNING && !in_array($serviceId, [
        Service::OCASH, Service::ODEPOSIT, Service::USER_INVOICE
      ]) && !$this->h2hManager->isUserH2H($userId);
  }


  private function sendPurchaseWarning($order, $itemName) {
    return new SendPurchaseWarning([
      'order_id' => $order->id,
      'payment_name' => $order->payment->information->actual_name,
      'item_name' => $itemName,
      'user_id' => $order->user_id,
      'user_name' => $order->name,
      'user_email' => $order->email,
      'total' => $order->total
    ]);
  }

  public function verifyDetail(PipelineListener $listener, $data) {
    if ($orderDetail = $this->orderDetails->findById($data["order_detail_id"])) {
      if (!isset($data['no_verify']) || !$data['no_verify']) {
        $orderDetail->verified_at = date("Y-m-d H:i:s");
        $this->orderDetails->save($orderDetail);
      }

      return $listener->response(200);
    }

    throw new \Exception("Can't verify. Order detail does not exists.");
  }

  public function verifyAndCalculateData(PipelineListener $listener, $data) {

    $order = $this->orders->findById($data['order_id']);

    if ($order->status != OrderStatus::COMPLETED) {
      throw new \Exception(json_encode([
        'order_id' => $data['order_id'],
        'order_status' => $order->status,
        'desc' => 'completedOrder is called but order is not 50000'
      ]));
    }

    $data["order_bonus"] = [
      'me' => 0,
      'parent' => 0,
      'grandparent' => 0,
      'default' => 0,
    ];
    $data["total_deposit"] = 0;
    $data["total_revenue"] = 0;
    $data['rush_bonus'] = [
      'me' => 0,
      'parent' => 0,
      'grandparent' => 0,
      'default' => 0
    ];

    $orderDetail = $order->details->first();
    if (in_array($order->gateway_id, [Payment::ANDROID, Payment::WEB_STORE, Payment::WEB_APP, Payment::IOS, Payment::WEB_SWITCHER])
      && $orderDetail->service_detail_id != ServiceDetail::CREDIT_BILL_ODEO
    ) {
      $listener->pushQueue(new SendOrderComplete($order, $data));
      if($order->additional_phone_number) {
        $listener->pushQueue(new SendSmsOrderComplete($order, $data));
      }
    }

    list($hustlerBonus, $mentorBonus, $leaderBonus, $defaultBonus) = $this->bonusCalculator->calculateBonus($orderDetail);
    $data['order_bonus']['me'] = $hustlerBonus;
    $data['order_bonus']['parent'] = $mentorBonus;
    $data['order_bonus']['grandparent'] = $leaderBonus;
    $data['order_bonus']['default'] += $defaultBonus;

    $data['referral_cashback'] = $orderDetail->referral_cashback;

    $data['total_deposit'] = $orderDetail->base_price;
    $data['total_revenue'] = $orderDetail->sale_price;
    $data['total_agent_deposit'] = $orderDetail->base_agent_price;

    list($hustlerRush, $mentorRush, $leaderRush, $defaultHustlerRush) = $this->bonusCalculator->calculateRush($orderDetail, [
      'me' => $hustlerBonus,
      'parent' => $mentorBonus,
      'grandparent' => $leaderBonus,
      'default' => $defaultBonus
    ]);
    $data['rush_bonus']['me'] = $hustlerRush;
    $data['rush_bonus']['parent'] = $mentorRush;
    $data['rush_bonus']['grandparent'] = $leaderRush;
    $data['rush_bonus']['default'] = $defaultHustlerRush;

    $data['service_id'] = $orderDetail->serviceDetail->service_id;
    $data['order'] = [
      'user_id' => $order->user_id,
      'status' => $order->status,
      'seller_store_id' => $order->actual_store_id,
      'is_agent' => !!$orderDetail->agent_base_price,
      'store_inventory_detail_id' => $orderDetail->store_inventory_detail_id,
      'purchase_at' => $order->paid_at,
      'settlement_at' => $order->settlement_at
    ];

    return $listener->response(200, $data);
  }

  public function verifyDetailAndCompleteOrder(PipelineListener $listener, $data) {

    $order = null;

    if (isset($data['order_id'])) {
      $order = $this->orders->findById($data['order_id']);
    } else if (isset($data['order_detail_id'])) {
      $order = $this->orderDetails->findById($data['order_detail_id'])->order;
    }

    $details = $order->details()->lockForUpdate()->get();
    $completed = false;

    foreach ($details as $item) {

      if ($item->id == $data['order_detail_id'] && $item->verified_at == NULL) {
        $item->verified_at = date("Y-m-d H:i:s");
        $this->orderDetails->save($item);
        $completed = true;
        break;
      }

    }

    if ($completed) {
      $order->status = OrderStatus::COMPLETED;
      if ($order->closed_at == null) $order->closed_at = date("Y-m-d H:i:s");

      $paymentInfo = $order->payment->information;
      $settlementAt = NULL;

      if (isset($paymentInfo->settlement_hours) && $paymentInfo->settlement_hours > 0) {
        $settlementAt = Carbon::now()->addHours($paymentInfo->settlement_hours)->toDateTimeString();
      }

      $order->settlement_at = $settlementAt;

      $this->orders->save($order);

      $listener->pushQueue(new CompleteOrder($order->id));
    }

    return $listener->response(200);
  }

  public function completeOrder(PipelineListener $listener, $data) {

    $order = $this->orders->findById($data['order_id']);

    if (!in_array($order->status, [
      OrderStatus::OPENED,
      OrderStatus::WAITING_FOR_UPDATE,
      OrderStatus::WAITING_SUSPECT,
      OrderStatus::REFUNDED,
      OrderStatus::REFUNDED_AFTER_SETTLEMENT,
      OrderStatus::COMPLETED
    ])
    ) {
      $orderDetail = $order->details()->first();
      if ($orderDetail->verified_at == NULL) {
        if ($order->status == OrderStatus::PARTIAL_FULFILLED) {
          return $listener->response(200);
        }
        $order->status = OrderStatus::PARTIAL_FULFILLED;
        $this->orders->save($order);
        return $listener->response(200);
      }

      $order->status = OrderStatus::COMPLETED;
      if ($order->closed_at == null) $order->closed_at = date("Y-m-d H:i:s");

      $paymentInfo = $order->payment->information;
      $settlementAt = NULL;

      if ($orderDetail->service_detail_id !== ServiceDetail::DEPOSIT_ODEO && isset($paymentInfo->settlement_hours) && $paymentInfo->settlement_hours > 0) {
        $settlementAt = Carbon::now()->addHours($paymentInfo->settlement_hours)->toDateTimeString();
      }

      $order->settlement_at = $settlementAt;

      $this->orders->save($order);

      $listener->pushQueue(new CompleteOrder($order->id));
    }

    return $listener->response(200);
  }


  public function updateOrderToVerifyStatus(PipelineListener $listener, $data) {
    if (!isset($data['order'])) {
      $data['order'] = $this->orders->findById($data['order_id']);
    }

    $order = $data['order'];

    $this->validateBankInquiry($order);
    $order->settlement_at = $data['settlement_at'] ?? null;

    if ($order->status != OrderStatus::VERIFIED) {

      if ($order->status < OrderStatus::VERIFIED || $order->status == OrderStatus::CANCELLED) {

        if ($order->status == OrderStatus::OPENED) {
          $order->paid_at = date('Y-m-d H:i:s');
        }
        $order->verified_by = $data['verified_by'] ?? null;
        $order->status = OrderStatus::VERIFIED;
      } else {
        throw new \Exception('trying to verify proccesed order');
      }

      $this->orders->save($order);
    }

    if (isset($data['skip_pipeline_response']) && $data['skip_pipeline_response']) {
      return true;
    }
    return $listener->response(200);
  }

  private function validateBankInquiry($order) {
    $bankInquiries = Payment::getBankInquiryRepository($order->payment->info_id);

    if (isProduction() && $bankInquiries) {
      $inquiry = $bankInquiries->findByOrderId($order->id);
      
      if (!$inquiry && $order->payment->info_id == Payment::OPC_GROUP_BANK_TRANSFER_MANDIRI) {
        $bankInquiries = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository::class);
        $inquiry = $bankInquiries->findByOrderId($order->id);
      }

      if (!$inquiry) {
        throw new \Exception('bank inquiry not exist');
      }
    }
  }
}
