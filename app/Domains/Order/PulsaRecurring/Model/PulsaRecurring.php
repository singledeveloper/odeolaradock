<?php

namespace Odeo\Domains\Order\PulsaRecurring\Model;

use Odeo\Domains\Subscription\Model\Store;
use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\PulsaOdeo;
use Odeo\Domains\Core\Entity;

class PulsaRecurring extends Entity {

  public function pulsaOdeo() {
    return $this->belongsTo(PulsaOdeo::class);
  }

  public function store() {
    return $this->belongsTo(Store::class);
  }

  public function user() {
    return $this->belongsTo(User::class);
  }
}