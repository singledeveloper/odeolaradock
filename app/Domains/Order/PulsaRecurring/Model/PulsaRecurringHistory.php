<?php

namespace Odeo\Domains\Order\PulsaRecurring\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Order\Model\Order;

class PulsaRecurringHistory extends Entity {

  public function pulsaRecurring() {
    return $this->belongsTo(PulsaRecurring::class);
  }

  public function order() {
    return $this->belongsTo(Order::class);
  }
}