<?php

namespace Odeo\Domains\Order\PulsaRecurring;

use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Helper\InventoryManager;
use Odeo\Domains\Marketing\QueueSelector;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Order\PulsaRecurring\Repository\PulsaRecurringHistoryRepository;
use Odeo\Domains\Order\PulsaRecurring\Repository\PulsaRecurringRepository;
use Odeo\Domains\Payment\PaymentOpenRequester;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Transaction\TempDepositRequester;

class OrderPurchaser {

  private $pulsaRecurring, $pulsaRecurringHistory, $inventoryManager;

  public function __construct() {
    $this->pulsaRecurring = app()->make(PulsaRecurringRepository::class);
    $this->pulsaRecurringHistory = app()->make(PulsaRecurringHistoryRepository::class);
    $this->inventoryManager = app()->make(InventoryManager::class);
  }

  public function purchase(PipelineListener $listener, $data) {
    $pulsaRecurring = $this->pulsaRecurring->findById($data['pulsa_recurring_id']);
    $pulsaOdeo = $pulsaRecurring->pulsaOdeo;

    $pulsaRecurringHistory = $this->pulsaRecurringHistory->getNew();
    $pulsaRecurringHistory->pulsa_recurring_id = $data['pulsa_recurring_id'];
    $this->pulsaRecurringHistory->save($pulsaRecurringHistory);

    $pipeline = new Pipeline();

    $data = array_merge($data, $this->inventoryManager->getServiceDataFromCategory($pulsaOdeo->category));

    if ($pulsaOdeo->service_detail_id == ServiceDetail::PULSA_POSTPAID_ODEO)
      $data['item_detail']['postpaid_type'] = PostpaidType::getConstKeyByValue($pulsaOdeo->category);

    $data['info_id'] = Payment::OPC_GROUP_OCASH;
    $data['pulsa_odeo_id'] = $data['item_id'] = $pulsaOdeo->id;
    $data['gateway_id'] = Payment::AUTO_RECURRING;
    $data['platform_id'] = Platform::AUTO_RECURRING;
    $data['opc'] = 375;
    $data['item_detail'] = [
      'number' => $pulsaRecurring->destination_number,
      'operator' => $pulsaOdeo->category
    ];
    $data['auth'] = [
      'user_id' => $pulsaRecurring->user_id,
      'type' => UserType::SELLER,
      'platform_id' => Platform::AUTO_RECURRING
    ];

    $usePreferredStore = $pulsaRecurring->store_id != null;

    $pipeline->add(new Task(QueueSelector::class, 'get', ['use_preferred_store' => $usePreferredStore]));
    $pipeline->add(new Task(CartRemover::class, 'clear'));
    $pipeline->add(new Task(CartInserter::class, 'addToCart'));
    $pipeline->add(new Task(CartCheckouter::class, 'checkout'));
    $pipeline->add(new Task(TempDepositRequester::class, 'tempDeposit'));
    $pipeline->add(new Task(PaymentRequester::class, 'request'));
    $pipeline->add(new Task(PaymentOpenRequester::class, 'open', [
      'with_password' => true
    ]));

    $pipeline->enableTransaction();
    $pipeline->execute($data);

    if ($pipeline->fail()) {
      $errorMessage = $pipeline->errorMessage;
      if (is_array($pipeline->errorMessage)) {
        clog('pulsa_recurring_failed', 'ID: '. $data['pulsa_recurring_id'] . '. Error: '.json_encode($pipeline->errorMessage));
        $errorMessage = 'Error. Please ensure the number for purchase is correct.';
      }
      $pulsaRecurringHistory->status = Recurring::FAILED;
      $pulsaRecurringHistory->error_message = $errorMessage;
    } else {
      $pulsaRecurringHistory->order_id = $pipeline->data['order_id'];
      $pulsaRecurringHistory->status = Recurring::COMPLETED;
    }

    $this->pulsaRecurringHistory->save($pulsaRecurringHistory);

    return $listener->response(200);
  }

}
