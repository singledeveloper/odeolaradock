<?php

namespace Odeo\Domains\Order\PulsaRecurring;

use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Order\Helper\StatusParser;
use Odeo\Domains\Order\OrderSelector;
use Odeo\Domains\Order\PulsaRecurring\Repository\PulsaRecurringHistoryRepository;
use Odeo\Domains\Transaction\Helper\Currency;

class PulsaRecurringHistorySelector implements SelectorListener {

  private $pulsaRecurringHistories, $orderSelector, $orderStatusParser, $currencyHelper;

  public function __construct() {
    $this->pulsaRecurringHistories = app()->make(PulsaRecurringHistoryRepository::class);
    $this->orderSelector = app()->make(OrderSelector::class);
    $this->orderStatusParser = app()->make(StatusParser::class);
    $this->currencyHelper = app()->make(Currency::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $output['order_id'] = $item->order_id;

    $output['order_status'] = null;
    $output['total'] = null;

    if (isset($item->order)) {
      $output['order_status'] = $this->orderStatusParser->parseForNewDesign(['status' => $item->order_status])['status'];
      $output['total'] = $this->currencyHelper->formatPrice($item->total);
    }

    if ($item->status == Recurring::FAILED) {
      $output['status'] = 'FAILED';
      $output['status_color'] = '#a94442';
      $output['status_text_color'] = '#fff';
    }
    else if ($item->status == Recurring::COMPLETED) {
      $output['status_color'] = '#00a65a';
      $output['status'] = 'COMPLETED';
      $output['status_text_color'] = '#fff';
    }
    else {
      $output['status_color'] = '#d2d6de';
      $output['status'] = $item->status == Recurring::PENDING ? 'PENDING' : 'UNKNOWN';
      $output['status_text_color'] = '#000';
    }

    $output['error_message'] = $item->error_message;
    $output['created_at'] = $item->created_at->format('Y-m-d H:i:s');

    return $output;
  }

  public function get(PipelineListener $listener, $data) {

    $this->pulsaRecurringHistories->normalizeFilters($data);

    $recurrings = [];

    foreach ($this->pulsaRecurringHistories->getHistories($data['pulsa_recurring_id'], $data['auth']['user_id']) as $recurring) {
      $recurrings[] = $this->_transforms($recurring, $this->pulsaRecurringHistories);
    }
    if (sizeof($recurrings) > 0)
      return $listener->response(200, array_merge([
        "pulsa_recurring_histories" => $this->_extends($recurrings, $this->pulsaRecurringHistories)
      ], $this->pulsaRecurringHistories->getPagination()));
    return $listener->response(204);

  }

}
