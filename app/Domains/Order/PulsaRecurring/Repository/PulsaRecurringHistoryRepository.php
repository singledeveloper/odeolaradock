<?php

namespace Odeo\Domains\Order\PulsaRecurring\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\PulsaRecurring\Model\PulsaRecurringHistory;

class PulsaRecurringHistoryRepository extends Repository {

  public function __construct(PulsaRecurringHistory $pulsaRecurringHistory) {
    $this->model = $pulsaRecurringHistory;
  }

  public function getHistories($pulsaRecurringId, $userId) {
    return $this->getResult($this->model
      ->select(\DB::raw('
        pulsa_recurring_histories.*,
        orders.total as total,
        orders.status as order_status
      '))
      ->leftJoin('orders', 'orders.id', '=', 'pulsa_recurring_histories.order_id')
      ->where('pulsa_recurring_id', $pulsaRecurringId)
      ->whereHas('pulsaRecurring', function($query) use ($userId) {
        $query->where('user_id', $userId);
      })->orderBy('pulsa_recurring_histories.id', 'desc'));
  }

  public function findByOrderId($orderId) {
    return $this->model->where('order_id', $orderId)->first();
  }

}
