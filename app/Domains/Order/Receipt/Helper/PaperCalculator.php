<?php
/**
 * Created by PhpStorm.
 * User: odeo
 * Date: 11/28/17
 * Time: 4:20 PM
 */

namespace Odeo\Domains\Order\Receipt\Helper;


class PaperCalculator {

  const defaultWidth = 58;
  const widthToCharLength = [
    58 => 32,
    57 => 31
  ];

  public function getMaxChar($paperWidth) {
    if (isset(self::widthToCharLength[$paperWidth])) {
      return self::widthToCharLength[$paperWidth];
    }

    return self::widthToCharLength[self::defaultWidth];
  }


  public function getPossibleWidth() {
    return array_keys(self::widthToCharLength);
  }

}