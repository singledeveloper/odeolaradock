<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/9/17
 * Time: 12:45 PM
 */

namespace Odeo\Domains\Order\Receipt\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Receipt\Model\UserReceiptConfig;

class UserReceiptConfigRepository extends Repository {

  public function __construct(UserReceiptConfig $userReceiptConfig) {
    $this->model = $userReceiptConfig;
  }

  public function findByUserId($id) {
    return $this->model->where('user_id', '=', $id)->first();
  }
}