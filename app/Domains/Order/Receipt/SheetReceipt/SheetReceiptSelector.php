<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/9/17
 * Time: 3:55 PM
 */

namespace Odeo\Domains\Order\Receipt\SheetReceipt;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Order\Receipt\Helper\PaperCalculator;

class SheetReceiptSelector implements SelectorListener {

  private $receiptConfigs, $orders, $orderDetailizer,
    $currencyHelper, $paperCalculator;

  public function __construct() {
    $this->receiptConfigs = app()->make(\Odeo\Domains\Order\Receipt\Repository\UserReceiptConfigRepository::class);
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->paperCalculator = app()->make(PaperCalculator::class);
  }

  public function find(PipelineListener $listener, $data) {

    $receipt = $this->receiptConfigs->findByUserId($data['auth']['user_id']);

    if (!$receipt) return $listener->response(204);

    $receipt->paper_max_char = $this->paperCalculator->getMaxChar($receipt->paper_width);


    $this->orders->normalizeFilters($data);
    $order = $this->orders->findById($data['order_id']);
    $output = $this->_transforms($order, $this->orders);

    unset(
      $receipt['id'],
      $receipt['user_id'],
      $receipt['created_at'],
      $receipt['updated_at'],
      $receipt['paper_width']
    );

    $output['receipt'] = $receipt;

    return $listener->response(200, $output);
  }

  public function _extends($data, Repository $repository) {}

  public function _transforms($order, Repository $repository) {

    $output = [];

    $output['order_id'] = $order->id;
    $output['paid_at'] = $order->paid_at;
    $output['cart_data']['total'] = $this->currencyHelper->formatPrice($order->total);
    $output['cart_data']['subtotal'] = $this->currencyHelper->formatPrice($order->subtotal);

    $output['cart_data']['items'] = $this->orderDetailizer->detailizeItem($order);

    unset(
      $output['cart_data']['items'][0]['order_detail_pulsa_switcher_id'],
      $output['cart_data']['items'][0]['service_detail_id'],
      $output['cart_data']['items'][0]['item_detail']['current_base_price']
    );

    return $output;
  }
}