<?php

namespace Odeo\Domains\Order\Reconciliation\Jobs;

use DB;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\Reconciliation\OrderReconciler;
use Odeo\Jobs\Job;

class ReconcileOrder extends Job {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    $pipeline = new Pipeline;

    $redis = Redis::connection();
    $key = 'odeo_core:order_reconciliation_offset';

    $this->data['offset'] = $redis->incr($key);

    $pipeline->add(new Task(OrderReconciler::class, 'reconcileOrder'));
    $pipeline->enableTransaction();
    $pipeline->execute($this->data);

    $redis->decr($key);
  }
}
