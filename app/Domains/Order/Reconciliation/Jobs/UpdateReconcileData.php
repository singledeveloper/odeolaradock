<?php

namespace Odeo\Domains\Order\Reconciliation\Jobs;

use DB;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\Reconciliation\OrderReconciler;
use Odeo\Jobs\Job;

class UpdateReconcileData extends Job {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(OrderReconciler::class, 'updateRecon'));
    $pipeline->enableTransaction();
    $pipeline->execute([
      'recons' => $this->data
    ]);

  }
}
