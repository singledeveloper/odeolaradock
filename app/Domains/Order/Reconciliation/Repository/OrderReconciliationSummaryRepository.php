<?php

namespace Odeo\Domains\Order\Reconciliation\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Reconciliation\Model\OrderReconciliationSummary;

class OrderReconciliationSummaryRepository extends Repository {

  public function __construct(OrderReconciliationSummary $orderReconciliationSummary) {
    $this->model = $orderReconciliationSummary;
  }

  public function getByDate($dateStart, $dateEnd) {
    return $this->model->where('date', '>=', $dateStart)->where('date', '<=', $dateEnd)->get();
  }

}