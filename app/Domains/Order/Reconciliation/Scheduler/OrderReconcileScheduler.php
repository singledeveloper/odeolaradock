<?php

namespace Odeo\Domains\Order\Reconciliation\Scheduler;

use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;

class OrderReconcileScheduler {

  public function init() {
    $pipeline = new Pipeline;
    $pipeline->add(new Task(\Odeo\Domains\Order\Reconciliation\OrderReconciler::class, 'dispatchRecon'));

    $pipeline->enableTransaction();
    $pipeline->execute([
      'new_data_only' => true,
    ]);
  }

  public function run() {
    $pipeline = new Pipeline;
    $pipeline->add(new Task(\Odeo\Domains\Order\Reconciliation\OrderReconciler::class, 'dispatchRecon'));

    $pipeline->enableTransaction();
    $pipeline->execute([
      'trx_month' => \Carbon\Carbon::now()->month,
      'new_data_only' => false,
    ]);
  }
}