<?php

namespace Odeo\Domains\Order\Reconciliation\Scheduler;

use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;

class OrderReconciliationSummarizer {

  public function run() {
    \DB::beginTransaction();

    try {
      $orderReconRepo = app()->make(\Odeo\Domains\Order\Reconciliation\Repository\OrderReconciliationRepository::class);
      $orderReconSummaryRepo = app()->make(\Odeo\Domains\Order\Reconciliation\Repository\OrderReconciliationSummaryRepository::class);

      $date = \Carbon\Carbon::now()->subHour()->toDateString();
      $reconSummaries = $orderReconRepo->getSummaryByDate($date);

      foreach ($reconSummaries as $reconSummary) {
        $orderReconSummary = $orderReconSummaryRepo->findByAttributes([
          'date' => $date, 
          'diff_reason' => $reconSummary['diff_reason']
        ]);

        if (!$orderReconSummary) {
          $orderReconSummary = $orderReconSummaryRepo->getNew();
          $orderReconSummary->date = $date;
          $orderReconSummary->diff_reason = $reconSummary['diff_reason'];
        }

        $orderReconSummary->total_diff = $reconSummary['total_diff'];

        $orderReconSummaryRepo->save($orderReconSummary);
      }

      \DB::commit();
    } catch (Exception $e) {
      \DB::rollback();
    }
  }
}