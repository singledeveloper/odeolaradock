<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 4:02 PM
 */

namespace Odeo\Domains\Order\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Model\OrderCharge;

class OrderChargeRepository extends Repository {

  public function __construct(OrderCharge $orderCharge) {
    $this->model = $orderCharge;
  }

  public function getCharge($order_id) {
    return $this->model
      ->where('order_id', $order_id)
      ->get();
  }

  public function getExisting($data) {
    $query = $this->getCloneModel();

    if (isset($data['type'])) $query = $query->where('type', $data['type']);
    if (isset($data['group_type'])) $query = $query->where('group_type', $data['group_type']);
    if (isset($data['order_id'])) $query = $query->where('order_id', $data['order_id']);

    return $query;
  }
}