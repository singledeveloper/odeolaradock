<?php

namespace Odeo\Domains\Order\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Model\OrderStore;

class OrderStoreRepository extends Repository {

  public function __construct(OrderStore $orderStore) {
    $this->model = $orderStore;
  }

  public function gets() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel();

    $query = $query->select('orders.id as id', 'orders.rating', 'orders.confirm_receipt',
      'order_details.id as order_detail_id', 'stores.name as store_name', 'stores.id as store_id',
      'actual_store_id', 'actual_store.name as actual_store_name',
      'order_stores.role as role', 'order_stores.is_community as is_community',
      'orders.created_at as created_at', 'paid_at', 'expired_at', 'closed_at', 'total', 'subtotal', 'service_id',
      'service_details.id as service_detail_id', 'orders.status', 'payments.opc as payment_opc',
      'payments.info_id as payment_info_id', 'user_id', 'orders.name as user_name', 'orders.email as user_email',
      'orders.phone_number', 'payment_odeo_payment_channel_informations.name as payment_name',
      'order_details.name as item_name', 'order_detail_pulsa_switchers.number as item_number',
      'order_detail_pulsa_switchers.serial_number as item_serial_number',
      'order_details.sale_price as item_price',
      \DB::raw('(order_details.sale_price - order_details.merchant_price) as item_margin'))
      ->from('order_stores')
      ->join('orders', 'order_stores.order_id', '=', 'orders.id')
      ->join('payments', 'payments.order_id', '=', 'orders.id')
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payments.info_id')
      ->join('order_details', 'orders.id', '=', 'order_details.order_id')
      ->join('service_details', 'service_details.id', '=', 'order_details.service_detail_id')
      ->leftJoin('users', 'users.id', '=', 'orders.user_id')
      ->leftJoin('stores', 'stores.id', '=', 'order_stores.store_id')
      ->leftJoin('stores as actual_store', 'actual_store.id', '=', 'orders.actual_store_id')
      ->leftJoin('order_detail_pulsa_switchers', 'order_detail_pulsa_switchers.order_detail_id', '=', 'order_details.id');
    if (isset($filters['start_date']) && $filters['start_date'] != '' && isset($filters['end_date']) && $filters['end_date'])
      $query->whereBetween('orders.created_at', [date($filters['start_date'] . ' 00:00:00', time()), date($filters['end_date'] . ' 23:59:59', time())]);
    if (isset($filters['order_id']) && $filters['order_id'] != '') $query->where('orders.id', '=', $filters['order_id']);
    if (isset($filters['status_code']) && $filters['status_code'] != '') $query->where('orders.status', '=', $filters['status_code']);
    if (isset($filters['service_id']) && $filters['service_id'] != '') $query->where('service_details.service_id', '=', $filters['service_id']);
    if (isset($filters['payment_id']) && $filters['payment_id'] != '') $query->where('payment_odeo_payment_channel_informations.id', '=', $filters['payment_id']);

    if (isset($filters['search'])) {
      if (isset($filters['search']['store_id'])) {
        $query = $query->where('store_id', $filters['search']['store_id']);
      }
      if (isset($filters['search']['order_id'])) {
        $query = $query->where('order_id', $filters['search']['order_id']);
      }
      if (isset($filters['search']['role'])) {
        $query = $query->where('role', $filters['search']['role']);
      }
    }
    return $this->getResult($query->orderBy('orders.id', 'desc'));
  }

  public function getExportData() {
    $filters = $this->getFilters();

    $query = $this->model->select(
      'order_detail_pulsa_switchers.number as item_number',
      'orders.id as id',
      'order_details.name as item_name',
      'order_stores.is_community as is_community',
      'order_detail_pulsa_switchers.serial_number as item_serial_number',
      'order_details.sale_price as item_price',
      \DB::raw('(order_details.sale_price - order_details.merchant_price) as item_margin'),
      'payment_odeo_payment_channel_informations.name as payment_name',
      'orders.status',
      'orders.closed_at',
      'orders.opened_at',
      'orders.created_at',
      'orders.paid_at'
    )
      ->from('order_stores')
      ->join('orders', 'order_stores.order_id', '=', 'orders.id')
      ->join('payments', 'payments.order_id', '=', 'orders.id')
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payments.info_id')
      ->join('order_details', 'orders.id', '=', 'order_details.order_id')
      ->leftJoin('order_detail_pulsa_switchers', 'order_detail_pulsa_switchers.order_detail_id', '=', 'order_details.id');

    if (isset($filters['start_date']) && $filters['start_date'] != '' && isset($filters['end_date']) && $filters['end_date']) {
      $query->whereDate('orders.created_at', '>=', $filters['start_date']);
      $query->whereDate('orders.created_at', '<=', $filters['end_date']);
    }

    $query->orderBy('orders.id', 'asc');

    $db = \DB::connection()->getPdo();

    $result = $db->prepare($query->toSql());
    $result->execute($query->getBindings());

    return $result;
  }

}
