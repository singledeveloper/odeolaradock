<?php

namespace Odeo\Domains\Order\Repository;

use Odeo\Domains\Constant\BulkPurchase;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Model\PulsaBulk;

class PulsaBulkRepository extends Repository {

  public function __construct(PulsaBulk $pulsaBulk) {
    $this->model = $pulsaBulk;
  }

  public function gets() {
    return $this->getResult($this->model->where('status', '<>', BulkPurchase::REMOVED)
      ->orderBy('id', 'desc'));
  }

}