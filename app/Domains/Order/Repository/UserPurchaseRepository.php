<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/9/17
 * Time: 11:47 PM
 */

namespace Odeo\Domains\Order\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Model\UserPurchase;
use Odeo\Domains\Order\Salesreport\Helper\Initializer;

class UserPurchaseRepository extends Repository {


  public function __construct(UserPurchase $userPurchase) {
    $this->model = $userPurchase;
  }

  public function getExisting($data) {

    return $this->model
      ->where('date', $data['date'])
      ->where('user_id', $data['user_id'])
      ->where('service_id', $data['service_id'])
      ->where('service_detail_id', $data['service_detail_id'])
      ->where('info_id', $data['info_id'])
      ->where('gateway_id', $data['gateway_id'])
      ->where('platform_id', $data['platform_id'])
      ->where('store_id', $data['store_id'])
      ->first();

  }

  public function getoCommerceServiceSales($day) {
    $date = Carbon::now()->subDays($day)->format('Y-m-d');

    return $this->model->where('date', $date)
      ->whereIn('service_id', Initializer::getOcommerceServiceIds())
      ->select('service_id', \DB::raw('sum(sales_amount) as sales_amount'),
        \DB::raw('sum(sales_quantity) as sales_quantity'))
      ->groupBy('service_id')->orderBy(\DB::raw('sum(sales_amount)'), 'desc')
      ->get();
  }

  public function getoCommercePaymentSales($day) {
    $date = Carbon::now()->subDays($day)->format('Y-m-d');

    return $this->model->where('date', $date)
      ->whereIn('service_id', Initializer::getOcommerceServiceIds())
      ->select('info_id', \DB::raw('sum(sales_amount) as sales_amount'),
        \DB::raw('sum(sales_quantity) as sales_quantity'))
      ->orderBy(\DB::raw('sum(sales_amount)'), 'desc')
      ->groupBy('info_id')->get();
  }

  public function getUserSales($day) {
    $date = Carbon::now()->subDays($day)->format('Y-m-d');

    return $this->model->where('date', $date)
      ->select('user_id', \DB::raw('sum(sales_amount) as sales_amount'), \DB::raw('sum(sales_quantity) as sales_quantity'))
      ->groupBy('user_id')->orderBy(\DB::raw('sum(sales_amount)'), 'desc')->get();
  }

}