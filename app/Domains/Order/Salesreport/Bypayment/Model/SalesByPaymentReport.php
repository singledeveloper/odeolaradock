<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/28/16
 * Time: 9:15 PM
 */

namespace Odeo\Domains\Order\Salesreport\Bypayment\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\Payment\Model\PaymentOdeoPaymentChannelInformation;

class SalesByPaymentReport extends Entity {

  public function information() {
    return $this->belongsTo(PaymentOdeoPaymentChannelInformation::class, 'info_id');
  }

}