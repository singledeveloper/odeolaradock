<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/2/17
 * Time: 1:46 PM
 */

namespace Odeo\Domains\Order\Salesreport\Bypayment;

use Carbon\Carbon;
use Odeo\Domains\Order\Salesreport\Helper\Initializer;

class SalesByPaymentReportUpdater {

  private $byPaymentReport, $payments;

  public function __construct() {

    $this->byPaymentReport = app()->make(\Odeo\Domains\Order\Salesreport\Bypayment\Repository\SalesByPaymentRepository::class);
    $this->payments = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);

  }

  public function updateByPaymentReport($opc, $day = Initializer::YESTERDAY) {

    $salesLastNDayByPayment = $this->byPaymentReport->findSalesLastNDayByOpcId($day, $opc['id']);

    if (!$salesLastNDayByPayment) {
      $sales = $this->payments->salesLastNDayByOpcId($day, $opc['id']);
      $salesPaymentReport = $this->byPaymentReport->getNew();
      $salesPaymentReport->info_id = $opc['id'];
      $salesPaymentReport->date = Carbon::now()->subDays($day)->format('Y-m-d');
      $salesPaymentReport->sales = $sales->total_count;
      $salesPaymentReport->sales_value = $sales->total_order ? $sales->total_order : 0;
      $salesPaymentReport->sales_target = Initializer::DEFAULT_TARGET_BYPAYMENT;
      $salesPaymentReport->is_locked = FALSE;

      $this->byPaymentReport->save($salesPaymentReport);

    } else if(!$salesLastNDayByPayment['is_locked']) {

      $sales = $this->payments->salesLastNDayByOpcId($day, $opc['id']);
      $salesLastNDayByPayment->sales = $sales->total_count;
      $salesLastNDayByPayment->sales_value = $sales->total_order ? $sales->total_order : 0;
      $salesLastNDayByPayment->is_locked = TRUE;

      $this->byPaymentReport->save($salesLastNDayByPayment);
    }

    $salesPaymentReport = $this->byPaymentReport->findSalesTodayByOpcId($opc['id']);
    if(!$salesPaymentReport) {

      $salesPaymentReport = $this->byPaymentReport->getNew();
      $salesPaymentReport->info_id= $opc['id'];
      $salesPaymentReport->date = Carbon::now()->format('Y-m-d');
      $salesPaymentReport->sales_target = Initializer::DEFAULT_TARGET_BYPAYMENT;

    }

    $sales = $this->payments->salesTodayByOpcId($opc['id']);
    $salesPaymentReport->sales = $sales->total_count;
    $salesPaymentReport->sales_value = $sales->total_order ? $sales->total_order : 0;

    $this->byPaymentReport->save($salesPaymentReport);

  }

}