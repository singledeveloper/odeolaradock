<?php

namespace Odeo\Domains\Order\Salesreport\Helper;

use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Service as Constant;

class Initializer {

  const DAY_NUM = 7;
  const YESTERDAY = 1;

  const PLAN_FREE = 99;

  const DEFAULT_TARGET_OCOMMERCE = 0;
  const DEFAULT_TARGET_PLATFORM = 0;
  const DEFAULT_TARGET_BYPAYMENT = 0;

  const OPC_GROUP_ID = [
    Payment::OPC_GROUP_CC,
    Payment::OPC_GROUP_DOKU_WALLET,
    Payment::OPC_GROUP_ATM_TRANSFER_VA,
    Payment::OPC_GROUP_ALFA,
    Payment::OPC_GROUP_MANDIRI_MPT,
    Payment::OPC_GROUP_KREDIVO,
    Payment::OPC_GROUP_OCASH,
    Payment::OPC_GROUP_MANDIRI_E_CASH,
    Payment::OPC_GROUP_MANDIRI_CLICKPAY,
    Payment::OPC_GROUP_CC_INSTALLMENT,
    Payment::OPC_GROUP_CASH_ON_DELIVERY,
    Payment::OPC_GROUP_BANK_TRANSFER_MANDIRI,
    Payment::OPC_GROUP_BANK_TRANSFER_BCA,
    Payment::OPC_GROUP_BANK_TRANSFER_BNI,
    Payment::OPC_GROUP_BANK_TRANSFER_BRI
  ];

  public static function getOcommerceServiceIds() {
    return array_merge(
      Constant::PPOB,
      [
        Constant::HOTEL,
        Constant::FLIGHT,
        Constant::CREDIT_BILL,
        Constant::USER_INVOICE
      ]
    );
  }

}
