<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/28/16
 * Time: 9:15 PM
 */

namespace Odeo\Domains\Order\Salesreport\Ocommerce\Model;


use Odeo\Domains\Inventory\Model\Service;
use Odeo\Domains\Core\Entity;

class SalesOcommerceReport extends Entity{

  public function service() {
    return $this->belongsTo(Service::class);
  }

}