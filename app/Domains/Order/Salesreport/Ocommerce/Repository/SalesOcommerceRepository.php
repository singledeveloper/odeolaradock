<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/28/16
 * Time: 9:05 PM
 */

namespace Odeo\Domains\Order\Salesreport\Ocommerce\Repository;


use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Salesreport\Ocommerce\Model\SalesOcommerceReport;

class SalesOcommerceRepository extends Repository {

  public function __construct(SalesOcommerceReport $salesOcommerceReport) {
    $this->model = $salesOcommerceReport;
  }

  public function getLockedReport($day) {
    $date = Carbon::today()->subDays($day)->format('Y-m-d');

    return $this->model->with('service')->where('date', '=', $date)->where('is_locked', true)
      ->orderBy('total_sale_amount', 'desc')->get();
  }

  public function findSalesTodayByServiceId($id) {
    $date = Carbon::now()->format('Y-m-d');

    return $this->model->whereHas('service', function ($query) use ($id) {
      $query->where('services.id', $id);
    })->where('date', '=', $date)->first();
  }

  public function findSalesLastNDayByServiceId($dayNum, $id) {
    $date = Carbon::today()->subDays($dayNum)->format('Y-m-d');

    return $this->model->whereHas('service', function ($query) use ($id) {
      $query->where('services.id', $id);
    })->where('date', '=', $date)->first();
  }

  public function getLastNDay($dayNum) {

    $query = $this->getCloneModel();
    $filters = $this->getFilters();

    if (isset($filters['search']['start_date'])) {
      $date = Carbon::parse($filters['search']['start_date']);
    } else {
      $date = Carbon::today();
    }

    if (isset($filters['search']['cursor'])) {
      if($filters['search']['cursor'] < 0) $date->addDays($filters['search']['cursor'] * 7);
      else $date->addDays($filters['search']['cursor'] * 7);
    }

    $dateEnd = clone $date;
    $dateStart = clone $date->subDays($dayNum);

    return $query->with('service')->where('date', '>', $dateStart)->where('date', '<=', $dateEnd)->orderBy('date', 'asc')->get();

  }

}
