<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/28/16
 * Time: 9:15 PM
 */

namespace Odeo\Domains\Order\Salesreport\Platform\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Plan;

class SalesPlatformReport extends Entity{

  public function plan() {
    return $this->belongsTo(Plan::class);
  }

}