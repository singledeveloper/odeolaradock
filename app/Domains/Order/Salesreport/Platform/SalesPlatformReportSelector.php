<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/2/17
 * Time: 1:18 PM
 */

namespace Odeo\Domains\Order\Salesreport\Platform;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;

class SalesPlatformReportSelector {

  private $salesPlatforms;

  public function __construct() {

    $this->salesPlatforms = app()->make(\Odeo\Domains\Order\Salesreport\Platform\Repository\SalesPlatformRepository::class);
  }

  public function _transforms($data, Repository $repository) {

    $output = [];

    $output['sales'] = $data->sales;
    $output['sales_target'] = $data->sales_target;
    $output['plan_id'] = $data->plan_id;

    return $output;

  }

  public function getSalesPlatformReport(PipelineListener $listener, $data) {

    $this->salesPlatforms->normalizeFilters($data);
    $dayNum = $data['day_num'];

    $response = array();

    foreach ($this->salesPlatforms->getLastNDay($dayNum) as $salesByPayment) {

      $date = $salesByPayment->date;
      $type = $salesByPayment->plan_id;

      $response[$date][$type] = $this->_transforms($salesByPayment, $this->salesPlatforms);
    }


    return $listener->response(200, ['platform' => $response]);

  }

}