<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/28/16
 * Time: 10:22 PM
 */

namespace Odeo\Domains\Order\Salesreport\Scheduler;

use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;

class SalesReportRefresher {

  private $services, $plans, $opc;
  private $ocommerceUpdater, $platformUpdater, $byPaymentUpdater;

  public function __construct() {

    $this->services = app()->make(\Odeo\Domains\Inventory\Repository\ServiceRepository::class);
    $this->plans = app()->make(\Odeo\Domains\Subscription\Repository\PlanRepository::class);
    $this->opc = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelInformationRepository::class);

    $this->ocommerceUpdater = app()->make(\Odeo\Domains\Order\Salesreport\Ocommerce\SalesOcommerceReportUpdater::class);
    $this->platformUpdater = app()->make(\Odeo\Domains\Order\Salesreport\Platform\SalesPlatformReportUpdater::class);
    $this->byPaymentUpdater = app()->make(\Odeo\Domains\Order\Salesreport\Bypayment\SalesByPaymentReportUpdater::class);
  }

  public function runOcommerceSalesReport() {
    $pipeline = new Pipeline;
    $pipeline->add(new Task(__CLASS__, 'refreshOcommerceSalesReport'));
    $pipeline->enableTransaction();
    $pipeline->execute([]);
  }

  public function runPlatformSalesReport() {
    $pipeline = new Pipeline;
    $pipeline->add(new Task(__CLASS__, 'refreshPlatformSalesReport'));
    $pipeline->enableTransaction();
    $pipeline->execute([]);
  }

  public function runPaymentSalesReport() {
    $pipeline = new Pipeline;
    $pipeline->add(new Task(__CLASS__, 'refreshByPaymentSalesReport'));
    $pipeline->enableTransaction();
    $pipeline->execute([]);
  }

  public function refreshOcommerceSalesReport(PipelineListener $listener, $data) {
    foreach ($this->services->getOcommerceProduct() as $services) {
      $this->ocommerceUpdater->updateOcommerceReport($services);
    }

    $listener->response(200);
  }

  public function recalculateOcommerceSalesReport(PipelineListener $listener, $data) {
    foreach ($this->services->getOcommerceProduct() as $services) {
      $this->ocommerceUpdater->recalculateOcommerceReport($services);
    }

    $listener->response(200);
  }

  public function refreshPlatformSalesReport(PipelineListener $listener, $data) {

    foreach ($this->plans->getByPlatformProduct() as $plan) {
      $this->platformUpdater->updatePlatformReport($plan);
    }

    $listener->response(200);
  }

  public function refreshByPaymentSalesReport(PipelineListener $listener, $data) {

    foreach ($this->opc->getByPaymentProduct() as $opc) {
      $this->byPaymentUpdater->updateByPaymentReport($opc);
    }

    $listener->response(200);
  }





}
