<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/1/16
 * Time: 2:17 PM
 */

namespace Odeo\Domains\Order\Scheduler;


use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\CartRemover;

class AutoCancelScheduler {

  private $orders;

  public function __construct() {
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);
    $this->tempDepositHelpers = app()->make(\Odeo\Domains\Transaction\Helper\TempDepositHelper::class);
  }

  public function run() {
    foreach ($this->orders->getExpiredActiveOrder() as $order) {
      $order->status = OrderStatus::CANCELLED;
      $order->closed_at = date('Y-m-d H:i:s');
      $this->orders->save($order);

      $orderDetail = array_first($this->orderDetailizer->detailizeItem($order, ['order_detail_id', 'base_price']));

      $pipeline = new Pipeline;
      $pipeline->add(new Task(__CLASS__, 'switchUser', ["next_user_to_switch" => $order->user_id]));
      $pipeline->add(new Task(\Odeo\Domains\Order\CartRemover::class, 'removeItem', [
        "cart_item" => $orderDetail
      ]));

      $pipeline->enableTransaction();
      $pipeline->execute();

      if (isset($orderDetail['agent_base_price']) && $orderDetail['agent_base_price'] != null) {
        $this->tempDepositHelpers->refundAgentDeposit($order->id);
      }
      $this->tempDepositHelpers->deleteDeposit($order->id);
    }
  }


  public function switchUser(PipelineListener $listener, $data) {

    $listener->appendData([
      'auth' => [
        'user_id' => $data['next_user_to_switch']
      ]
    ]);

  }

}