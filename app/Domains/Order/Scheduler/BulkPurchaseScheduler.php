<?php

namespace Odeo\Domains\Order\Scheduler;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BulkPurchase;

class BulkPurchaseScheduler {

  public function run() {

    $redis = Redis::connection();

    if ($redis->hget(BulkPurchase::REDIS_TEMP_BULK, BulkPurchase::REDIS_TEMP_BULK_KEY_LOCK)) return;

    $redis->hset(BulkPurchase::REDIS_TEMP_BULK, BulkPurchase::REDIS_TEMP_BULK_KEY_LOCK, 1);

    if (!$lastRow = $redis->hget(BulkPurchase::REDIS_TEMP_BULK, BulkPurchase::REDIS_TEMP_BULK_KEY_LAST_ROW))
      $lastRow = 0;

    $bulkPurchaseRepo = app()->make(\Odeo\Domains\Order\Repository\PulsaBulkPurchaseRepository::class);
    $jabberUserRepo = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberUserRepository::class);

    $bulkPurchases = $bulkPurchaseRepo->getLastPurchases($lastRow);

    if (count($bulkPurchases) > 0) {
      $jabberUsers = [];

      $x = 0;
      foreach ($bulkPurchases as $item) {
        if (!isset($jabberUsers[$item->user_id]))
          $jabberUsers[$item->user_id] = $jabberUserRepo->findByUserId($item->user_id);

        dispatch((new \Odeo\Domains\Order\Jobs\BulkPurchase(
          $item->id,
          $jabberUsers[$item->user_id]->store_id,
          $item->user_id,
          $jabberUsers[$item->user_id]->id,
          $jabberUsers[$item->user_id]->email,
          $item->request_message
        ))->delay($x * 3));
        $x++;
        $lastRow = $item->id;
      }
      $redis->hset(BulkPurchase::REDIS_TEMP_BULK, BulkPurchase::REDIS_TEMP_BULK_KEY_LAST_ROW, $lastRow);
    }
    else if ($lastRow != 0) $redis->hdel(BulkPurchase::REDIS_TEMP_BULK, BulkPurchase::REDIS_TEMP_BULK_KEY_LAST_ROW);

    $redis->hdel(BulkPurchase::REDIS_TEMP_BULK, BulkPurchase::REDIS_TEMP_BULK_KEY_LOCK);
  }

}