<?php
namespace Odeo\Domains\Order\Scheduler;

use Odeo\Domains\Order\Jobs\PurchasePulsaRecurring;
use Odeo\Domains\Constant\Recurring;

class PulsaRecurringScheduler {

  private $pulsaRecurring, $pulsaRecurringHelper;

  public function __construct() {
    $this->pulsaRecurring = app()->make(\Odeo\Domains\Order\PulsaRecurring\Repository\PulsaRecurringRepository::class);
    $this->pulsaRecurringHelper = app()->make(\Odeo\Domains\Order\PulsaRecurring\Helper\PulsaRecurringHelper::class);
  }

  public function run() {
    if (date('H') >= 7 && date('H') < 23) {
      foreach($this->pulsaRecurring->getRecurringQueues() as $item) {
        $item->last_executed_at = date('Y-m-d H:i:s');
        $item->next_payment = $this->pulsaRecurringHelper->getNextPayment($item->type, date('Y-m-d H:i:s'));
        if (strpos($item->type, Recurring::TYPE_ONCE) !== false) {
          $item->is_active = false;
        }
        $this->pulsaRecurring->save($item);
        dispatch(new PurchasePulsaRecurring($item->id));
      }
    }
  }

}
