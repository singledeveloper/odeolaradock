<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:31 PM
 */

namespace Odeo\Domains\Order;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelInformationRepository;
use Odeo\Domains\Marketing\RevenueSelector;
use Odeo\Domains\Order\Jobs\SendStoreOrderReport;

class StoreOrderSelector implements SelectorListener {


  private $orderStores, $currencyHelper, $orderDetailizer, $payments, $paymentOdeoBankTransferDetails, $statusParser;


  public function __construct() {

    $this->orderStores = app()->make(\Odeo\Domains\Order\Repository\OrderStoreRepository::class);

    $this->orderParser = app()->make(\Odeo\Domains\Order\Helper\OrderParser::class);

    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

    $this->orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);

    $this->payments = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);

    $this->paymentInformations = app()->make(PaymentOdeoPaymentChannelInformationRepository::class);

    $this->statusParser = app()->make(\Odeo\Domains\Order\Helper\StatusParser::class);

    $this->orderTypeParser = app()->make(\Odeo\Domains\Order\Helper\OrderTypeParser::class);

    $this->network = app()->make(\Odeo\Domains\Network\Repository\TreeRepository::class);

    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }


  public function _transforms($orderStore, Repository $repository) {
    $filters = $repository->getFilters();

    $output = [];

    $output['order_id'] = $orderStore->id;
    $output['store_id'] = $orderStore->store_id;
    $output['store_name'] = $orderStore->role == 'rush' ? $orderStore->store_name : $orderStore->actual_store_name;

    $orderStore->role = $orderStore->role ?? 'hustler';
    $orderStore->is_agent = isset($orderStore->is_community) ? !$orderStore->is_community : false;
    $output = array_merge($output, $this->orderTypeParser->parse($orderStore));

    $output['created_at'] = $orderStore->created_at;
    $output['paid_at'] = $orderStore->paid_at;
    $output['expired_at'] = $orderStore->expired_at;
    $output['closed_at'] = $orderStore->closed_at;
    $output['cart_data']['total'] = $this->currencyHelper->formatPrice($orderStore->total);
    $output['cart_data']['subtotal'] = $this->currencyHelper->formatPrice($orderStore->subtotal);

    $output['user'] = [
      'id' => $orderStore->user_id ?? "",
      'telephone' => $orderStore->phone_number ?? "",
      'email' => $orderStore->user_email ?? "",
      'name' => $orderStore->user_name ?? ""
    ];

    // $output['cart_data']['charges'] = $this->orderDetailizer->detailizeCharge($orderStore->id);
    $output['cart_data']['charges'] = [];

    $item['name'] = explode('/', $orderStore->item_name)[0];
    $item['service_id'] = $orderStore->service_id;
    $item['service_detail_id'] = $orderStore->service_detail_id;
    $item['order_detail_id'] = $orderStore->order_detail_id;
    $item['type'] = str_replace('_', ' ', strtolower(Service::getConstKeyByValue($orderStore->service_id)));
    $item['price'] = $this->currencyHelper->formatPrice($orderStore->item_price);
    $item['item_detail']['number'] = $orderStore->item_number ?? '';
    $item['item_detail']['serial_number'] = $orderStore->item_serial_number ?? '';

    $output['cart_data']['items'][] = $item;

    $mask = [];
    if ($orderStore->is_community || $orderStore->actual_store_id != $filters['search']['store_id']) {
      if ($length = strlen($output['user']['telephone'])) {
        $output['user']['telephone'] = str_repeat('*', max(0, $length - 4)) . substr($output['user']['telephone'], $length - 4);
      }
      if ($length = strlen($output['user']['email'])) {
        $output['user']['email'] = substr($output['user']['email'], 0, 5) . str_repeat('*', max(0, $length - 5));
      }
      if ($length = strlen($output['cart_data']['items'][0]['item_detail']['number'])) {
        $output['cart_data']['items'][0]['item_detail']['number'] = str_repeat('*', max(0, $length - 4)) . substr($output['cart_data']['items'][0]['item_detail']['number'], $length - 4);
      }
      $mask[] = 'mask_phone_number';

      $output['user']['name'] = "";
    }

    $output = array_merge($output, $this->statusParser->parse($orderStore, $orderStore->service_id));

    $output['payment']['opc'] = $orderStore->payment_opc;
    $output['payment']['opc_group'] = $orderStore->payment_info_id;
    $output['payment']['name'] = $orderStore->payment_name;

    $output['confirm_receipt'] = $orderStore->confirm_receipt;
    $output['rating'] = $orderStore->rating;

    return $output;
  }

  public function get(PipelineListener $listener, $data) {
    $this->orderStores->setSimplePaginate(true);
    $this->orderStores->normalizeFilters($data);

    $orders = [];
    $dates = [];
    foreach ($this->orderStores->gets() as $orderStore) {
      $temp = $this->_transforms($orderStore, $this->orderStores);
      $dates[] = \Carbon\Carbon::parse($temp['created_at'])->format('Y-m-d');
      $orders[] = $temp;
    }

    $dates = array_unique($dates);
    if (sizeof($dates) > 0) $listener->addNext(new Task(RevenueSelector::class, 'getDetail', ['dates' => $dates, 'sum_with_cash' => true]));

    if (sizeof($orders) > 0)
      return $listener->response(200, array_merge(
        ["orders" => $this->_extends($orders, $this->orderStores)],
        $this->orderStores->getPagination()
      ));
    return $listener->response(204, ["orders" => []]);
  }

  public function export(PipelineListener $listener, $data) {
    $this->orderStores->normalizeFilters($data);
//    if (app()->environment() === 'production' && Redis::get('store_sales_order_export_' . $data['search']['store_id'])) {
//      return $listener->response(400, trans('user.order_report_already_exported'));
//    }

    $userName = $this->users->findById($data['auth']['user_id'])->name;

    if (app()->environment() === 'production' && Redis::get('store_sales_order_export_' . $data['search']['store_id'])) {
      return $listener->response(400, trans('user.order_report_already_exported'));
    }

    $filters = [
      'start_date' => $data['start_date'],
      'end_date' => $data['end_date'],
      'file_type' => $data['file_type'] ?? 'csv',
      'search' => ['store_id' => $data['search']['store_id']],
      'user_name' => $userName
    ];

//    app()->environment() === 'production' &&
//    Redis::setex('store_sales_order_export_' . $data['search']['store_id'], 3600, 1);
    $listener->pushQueue(new SendStoreOrderReport($filters, $data['email']));
    return $listener->response(200);

  }

}
