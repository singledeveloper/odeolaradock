<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:31 PM
 */

namespace Odeo\Domains\Order;


use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Order\Jobs\SendOrderReport;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelInformationRepository;

class UserOrderSelector implements SelectorListener {


  private $orders, $currencyHelper, $orderDetailizer, $payments, $statusParser, $services, $users;


  public function __construct() {

    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);

    $this->orderParser = app()->make(\Odeo\Domains\Order\Helper\OrderParser::class);

    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

    $this->orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);

    $this->payments = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);

    $this->paymentInformations = app()->make(PaymentOdeoPaymentChannelInformationRepository::class);

    $this->statusParser = app()->make(\Odeo\Domains\Order\Helper\StatusParser::class);

    $this->services = app()->make(\Odeo\Domains\Inventory\Repository\ServiceRepository::class);

    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }


  public function _transforms($order, Repository $repository) {

    $output = [];

    $output['order_id'] = $order->id;
    $output['store_id'] = $order->store_id ?? "";
    $output['store_name'] = $order->store_name ?? "odeo";

    if (isset($order->store_logo)) {
      $output = array_merge($output, parseFilePath($order->store_logo, "store_logo"));
    }

    $output['created_at'] = $order->created_at;
    $output['paid_at'] = $order->paid_at;
    $output['expired_at'] = $order->expired_at;
    $output['closed_at'] = $order->closed_at;
    $output['refunded_at'] = $order->refunded_at;
    $output['cart_data']['total'] = $this->currencyHelper->formatPrice(abs($order->total));
    $output['cart_data']['subtotal'] = $this->currencyHelper->formatPrice($order->subtotal);

    $output['user'] = [
      'id' => $order->user_id ?? "",
      'telephone' => $order->phone_number ?? "",
      'email' => $order->user_email ?? "",
      'name' => $order->user_name ?? ""
    ];

    //$output['cart_data']['charges'] = $this->orderDetailizer->detailizeCharge($order->id);
    $output['cart_data']['charges'] = [];
    /*$output['cart_data']['items'] = $this->orderDetailizer->detailizeItem($order, [
      'order_detail_id'
    ]);*/

    $item['name'] = explode('/', $order->item_name)[0];
    $item['service_id'] = $order->service_id;
    $item['service_detail_id'] = $order->service_detail_id;
    $item['order_detail_id'] = $order->order_detail_id;
    $item['type'] = str_replace('_', ' ', strtolower(Service::getConstKeyByValue($order->service_id)));
    $item['price'] = $this->currencyHelper->formatPrice($order->item_price);
    $item['item_detail'] = [
      'number' => $order->item_number ?? '',
      'user_note' => Supplier::getFailedReasonToUser($order->item_response_code, $order->status)
    ];

    if ($order->service_detail_id == ServiceDetail::GOOGLE_PLAY_ODEO && strpos($order->item_serial_number, '#') !== false) {
      list($item['item_detail']['serial_number'], $item['item_detail']['voucher_code']) = explode('#', $order->item_serial_number);
      $item['item_detail']['voucher_code'] = wordwrap($item['item_detail']['voucher_code'], 4, '-', true);
    }
    else $item['item_detail']['serial_number'] = $order->item_serial_number ?? '';

    $output['cart_data']['items'][] = $item;

    $output = array_merge($output, $this->statusParser->parse($order, $order->service_id));

    $output['payment']['opc'] = $order->payment_opc;
    $output['payment']['opc_group'] = $order->payment_info_id;
    $output['payment']['name'] = $order->payment_name;

    $output['confirm_receipt'] = $order->confirm_receipt;
    $output['rating'] = $order->rating;

    return $output;
  }

  public function get(PipelineListener $listener, $data) {
    isset($data['search']) && isset($data['search']['status']) && ($data['search']['status'] = $this->statusParser->normalize($data['search']['status']));

    $this->orders->setSimplePaginate(true);
    $this->orders->normalizeFilters($data);

    $orders = [];

    foreach ($this->orders->getUserOrders() as $order) {
      $orders[] = $this->_transforms($order, $this->orders);
    }
    if (sizeof($orders) > 0)
      return $listener->response(200, array_merge(
        ["orders" => $this->_extends($orders, $this->orders)],
        $this->orders->getPagination()
      ));
    return $listener->response(200, ["orders" => []]);

  }

  public function exportOrder(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];
//    $excludeValidation = in_array($userId, [
//      809
//    ]);
//    if (!$excludeValidation && (Redis::get("order_export_" . $userId))) return $listener->response(400, trans('user.order_report_already_exported'));
    $userName = $this->users->findById($userId)->name;

    if (isset($data['payment_id'])) {
      $paymentName = $this->paymentInformations->findById($data['payment_id'])->name;
    }

    if (isset($data['service_id'])) {
      $serviceName = $this->services->findById($data['service_id'])->displayed_name;
    }

    if (isset($data['status_code'])) {
      $statusMessage = $this->statusParser->parse(['status' => $data['status_code']])['status_message'];
    }

    $filters = [
      'start_date' => $data['start_date'] ?? '',
      'end_date' => $data['end_date'] ?? '',
      'file_type' => $data['file_type'] ?? 'csv',
      'user_id' => $userId,
      'user_name' => $userName,
      'order_id' => $data['order_id'] ?? '',
      'payment_id' => $data['payment_id'] ?? '',
      'payment_name' => $paymentName ?? '',
      'service_id' => $data['service_id'] ?? '',
      'service_name' => $serviceName ?? '',
      'status_code' => $data['status_code'] ?? '',
      'status_message' => $statusMessage ?? ''
    ];

//    !$excludeValidation && Redis::setex("order_export_" . $userId, 3600, 1);
    $listener->pushQueue(new SendOrderReport($filters, $data['email']));
    return $listener->response(200);
  }

}
