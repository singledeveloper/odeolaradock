<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/9/17
 * Time: 9:16 PM
 */

namespace Odeo\Domains\Order;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Core\PipelineListener;

class UserPurchaseUpdater {

  private $userPurchases, $redis;

  const USER_PURCHASES_MAX_CACHE_DAY = 2;

  public function __construct() {
    $this->redis = Redis::connection();
    $this->userPurchases = app()->make(\Odeo\Domains\Order\Repository\UserPurchaseRepository::class);
  }

  public function update(PipelineListener $listener, $data) {

    $date = Carbon::parse($data['date']);

    if ($userPurchase = $this->userPurchases->getExisting(
      [
        'service_id' => $data['service_id'],
        'service_detail_id' => $data['service_detail_id'],
        'platform_id' => $data['platform_id'],
        'gateway_id' => $data['gateway_id'],
        'info_id' => $data['info_id'],
        'user_id' => $data['user_id'],
        'date' => $date->toDateString(),
        'store_id' => $data['store_id']
      ])
    ) ;
    else {

      $userPurchase = $this->userPurchases->getNew();
      $userPurchase->service_id = $data['service_id'];
      $userPurchase->service_detail_id = $data['service_detail_id'];
      $userPurchase->platform_id = $data['platform_id'];
      $userPurchase->gateway_id = $data['gateway_id'];
      $userPurchase->info_id = $data['info_id'];
      $userPurchase->user_id = $data['user_id'];
      $userPurchase->store_id = $data['store_id'];
      $userPurchase->date = $date->toDateString();
      $userPurchase->sales_quantity = 0;
      $userPurchase->sales_amount = 0;

    }


    $userPurchase->sales_quantity = $userPurchase->sales_quantity + $data['quantity'];
    $userPurchase->sales_amount = $userPurchase->sales_amount + $data['amount'];

    $this->userPurchases->save($userPurchase);

    $this->_validateMaximumCacheDay($date) &&

    $this->redis->pipeline(function ($pipe) use ($userPurchase, $data, $date) {

      $namespace = 'odeo_core:user:' . $userPurchase->user_id . ':purchase_information_on_' . $date->toDateString();

      if (!in_array($data['service_id'], [
        Service::DOMAIN, Service::WARRANTY, Service::PLAN, Service::OCASH, Service::ODEPOSIT
      ])) {
        $pipe->hincrby($namespace, 'total_sales_quantity', $data['quantity']);
        $pipe->hincrbyfloat($namespace, 'total_sales_amount', $data['amount']);
      }

      $pipe->hincrby($namespace, 'total_sales_quantity_on_service_id_' . $data['service_id'], $data['quantity']);
      $pipe->hincrbyfloat($namespace, 'total_sales_amount_on_service_id_' . $data['service_id'], $data['amount']);

      $pipe->expireat($namespace, Carbon::create($date->year, $date->month, $date->day + self::USER_PURCHASES_MAX_CACHE_DAY, 0, 0, 0)->timestamp);

    });

    return $listener->response(200);
  }

  private function _validateMaximumCacheDay(Carbon $date) {

    $now = Carbon::now();

    $date = Carbon::createFromDate($date->year, $date->month, $date->day);
    $now = Carbon::createFromDate($now->year, $now->month, $now->day);

    return abs($date->diffInDays($now)) <= (self::USER_PURCHASES_MAX_CACHE_DAY - 1);

  }


}