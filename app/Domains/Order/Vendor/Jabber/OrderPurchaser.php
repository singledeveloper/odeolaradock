<?php

namespace Odeo\Domains\Order\Vendor\Jabber;

use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Marketing\QueueSelector;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Payment\PaymentOpenRequester;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Domains\Transaction\TempDepositRequester;
use Odeo\Domains\Vendor\Jabber\JabberManager;
use Odeo\Domains\Core\Pipeline;

class OrderPurchaser extends JabberManager {

  private $pulsaOdeo, $pulsaPrefix, $jabberOrder, $switcherOrder,
    $orderDetails, $inventoryManager, $status, $priceGetter, $users;

  public function __construct() {
    parent::__construct();
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->pulsaPrefix = app()->make(\Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaPrefixRepository::class);
    $this->jabberOrder = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberOrderRepository::class);
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
    $this->inventoryManager = app()->make(\Odeo\Domains\Inventory\Helper\InventoryManager::class);
    $this->status = app()->make(\Odeo\Domains\Order\Helper\StatusParser::class);
    $this->priceGetter = app()->make(\Odeo\Domains\Subscription\ManageInventory\Helper\PriceGetter::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $message = '';
    $continue = true;
    $temp = explode('.', $data['jabber_message']);
    if (count($temp) == 4) {
      list($command, $code, $number, $pin) = $temp;
      if ($command != Inline::CMD_BELI) list($code, $number, $smsTo, $pin) = $temp;
    }
    else if (count($temp) == 3) list($code, $number, $pin) = $temp;
    else {
      $message = 'GAGAL Format tidak valid.';
      $continue = false;
    }

    if ($continue) {
      if (revertTelephone($number) != $number)
        $message = 'GAGAL Nomor ' . $number . ' tidak valid.';
      else {
        $loop = 1;
        $pulsaOdeo = false;
        if (strpos($number, '/') !== false) {
          list($number, $trxId) = explode('/', $number);
          if ($jabberOrder = $this->jabberOrder->findByTrxId($trxId, $data['jabber_user_id'])) {
            $orderDetail = $this->orderDetails->findByOrderId($jabberOrder->order_id);
            $message = $this->status->parseMessageForSameTransaction($orderDetail->switcher);
            $continue = false;
          }
        }
        else if (strpos($number, '#') !== false) {
          list($number, $loop) = explode('#', $number);
          $loop = intval($loop);
          if ($loop == 0) $loop = 1;
        }

        if ($continue) {
          if (intval($code)) {
            $temp = substr($number, 0, 3);
            if ($temp != '999') $temp = substr(purifyTelephone($number), 0, 5);
            if (!$pulsaPrefix = $this->pulsaPrefix->getNominal(['prefix' => $temp])) {
              $message = 'GAGAL Nomor ' . $number . ' tidak valid atau stok kosong.';
              $continue = false;
            }
            else $pulsaOdeo = $this->pulsaOdeo->findByNominal($code . '000', $pulsaPrefix->operator_id);
          }
          else {
            $user = $this->users->findById($data['auth']['user_id']);
            if ($user->purchase_preferred_store_id) {
              $pulsaOdeo = $this->priceGetter->checkSupplyChainExistFromInventoryCode([
                'inventory_code' => strtoupper($code),
                'user_id' => $data['auth']['user_id'],
                'store_id' => $user->purchase_preferred_store_id
              ]);
            }
            if (!$pulsaOdeo) $pulsaOdeo = $this->pulsaOdeo->findByInventoryCode(strtoupper($code));
          }

          if ($continue) {
            if ($pulsaOdeo) {
              if (isset($smsTo) && $pulsaOdeo->operator_id != Pulsa::OPERATOR_PLN)
                $message = trans('pulsa.inline.block_sms_to', ['number' => revertTelephone($number)]);
              else if (!isset($data['no_check_history']) && $history = $this->jabberHistories->findSameMessage($data['jabber_from'], strtoupper($data['jabber_message']))) {
                if (!isset($trxId)) {
                  $same = $this->switcherOrder->getSamePurchaseCount(purifyTelephone($number), $pulsaOdeo->id);
                  if ($loop <= count($same)) {
                    $message = $this->status->parseMessageForSameTransaction($same[$loop - 1]);
                    $continue = false;
                  }
                }
                if ($continue) {
                  if (time() - strtotime($history->created_at) > 5 * 60) $this->jabberHistories->delete($history);

                  $message = trans('pulsa.inline.same_transaction', [
                    'date' => date('d/m/Y H:i', strtotime($history->created_at)),
                    'message' => 'Trx ke ' . $number . ' dalam antrian, mohon menunggu 5 menit.'
                  ]);
                }
              }
              else {
                $history = $this->jabberHistories->getNew();
                $history->update_id = $data['jabber_from'];
                $history->message = strtoupper($data['jabber_message']);
                $this->jabberHistories->save($history);

                $pipeline = new Pipeline();

                $data = array_merge($data, $this->inventoryManager->getServiceDataFromCategory($pulsaOdeo->category));

                $data['info_id'] = Payment::OPC_GROUP_OCASH;
                $data['pulsa_odeo_id'] = $data['item_id'] = $pulsaOdeo->id;
                $data['gateway_id'] = Payment::JABBER;
                $data['platform_id'] = Platform::JABBER;
                $data['opc'] = 365;
                $data['item_detail'] = [
                  'number' => purifyTelephone($number),
                  'operator' => $pulsaOdeo->category
                ];
                if (isset($smsTo)) $data['item_detail']['sms_to'] = $smsTo;

                $pipeline->add(new Task(QueueSelector::class, 'get', [
                  'use_preferred_store' => $pulsaOdeo->status != Pulsa::STATUS_OK_HIDDEN
                ]));
                $pipeline->add(new Task(CartRemover::class, 'clear'));
                $pipeline->add(new Task(CartInserter::class, 'addToCart'));
                $pipeline->add(new Task(CartCheckouter::class, 'checkout'));
                $pipeline->add(new Task(TempDepositRequester::class, 'tempDeposit'));
                $pipeline->add(new Task(PaymentRequester::class, 'request'));
                $pipeline->add(new Task(PaymentOpenRequester::class, 'open', [
                  'with_password' => true
                ]));

                $pipeline->enableTransaction();
                $pipeline->execute($data);

                if ($pipeline->fail()) {
                  $this->jabberHistories->delete($history);
                  try {
                    clog('_jabber_jaxl', 'error: ' . $pipeline->errorMessage);
                    $message = $pipeline->errorMessage == Pulsa::MAINTANANCE_MESSAGE ?
                      ($pipeline->errorMessage . ' Mohon untuk tidak menggagalkan transaksi yang sudah dijalankan.') :
                      ('GAGAL Trx ke ' . revertTelephone($number) . '. ' . $pipeline->errorMessage);
                  }
                  catch (\Exception $e) {
                    clog('jabber_jaxl', 'pipeline error message error. ' . json_encode($pipeline->errorMessage));
                    $message = 'GAGAL. Silakan dicoba beberapa saat lagi.';
                  }
                }
                else {
                  $jabberOrder = $this->jabberOrder->getNew();
                  $jabberOrder->order_id = $pipeline->data['order_id'];
                  $jabberOrder->jabber_user_id = $data['jabber_user_id'];
                  $jabberOrder->jabber_store_id = $data['jabber_store_id'];
                  $jabberOrder->trx_id = isset($trxId) ? $trxId : null;
                  $this->jabberOrder->save($jabberOrder);

                  $message = trans('pulsa.inline.pending', [
                    'order_id' => $pipeline->data['order_id'],
                    'number' => revertTelephone($number),
                    'item_name' => $pulsaOdeo->name
                  ]);
                }
              }
            }
            else $message = 'GAGAL Kode ' . $code . ' tidak valid';
          }
        }
      }
    }

    if ($message != '') $listener->appendResponseMessage($message, InlineJabber::RES_PARAM);

    return $listener->response(200, isset($pipeline) ? $pipeline->data : []);
  }

}