<?php

namespace Odeo\Domains\Order\Vendor\Jabber;

use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Jabber\JabberManager;

class OrderSelector extends JabberManager {

  private $order, $status;

  public function __construct() {
    parent::__construct();
    $this->order = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->status = app()->make(\Odeo\Domains\Order\Helper\StatusParser::class);
  }

  public function parse(PipelineListener $listener, $data) {

    list($command, $orderId, $pin) = explode('.', $data['jabber_message']);

    if (strpos($orderId, '#') !== false) {
      $temp = explode('#', strtolower($orderId));

      if ($temp[0] == 'p') {
        $orders = $this->order->findByUserId($data['auth']['user_id'], $temp[1], isset($temp[2]) ? $temp[2] : 0);
        $messages = [];
        foreach ($orders as $item) {
          $message = "Order #" . $item->id . ' status: ' . $this->status->parse($item)['status_message'];
          foreach ($item->details as $output) {
            $message .= ', ' . $output->name;
            if ($output->switcher) {
              $message .= ' ' . $output->switcher->number . ' - SN: ' .
                ($output->switcher->serial_number != null ? $output->switcher->serial_number : 'N/A');
            }
          }
          $messages[] = $message;
        }
        if (count($messages) > 0) $message = implode("\n", $messages);
        else $message = 'GAGAL Data tidak tersedia.';
      }
      else $message = 'GAGAL Format tidak valid.';
    }
    else {
      $order = $this->order->findById($orderId);

      if (!$order || ($order && $order->user_id != $data['auth']['user_id']))
        $message = "GAGAL Order ID tidak valid.";
      else {
        $message = "Order #" . $order->id . ' status: ' . $this->status->parse($order)['status_message'];
        foreach ($order->details as $item) {
          $message .= ', ' . $item->name;
          if ($item->switcher) {
            $message .= ' ' . $item->switcher->number . ' - SN: ' .
              ($item->switcher->serial_number != null ? $item->switcher->serial_number : 'N/A');
          }
        }
      }
    }

    $listener->appendResponseMessage($message, InlineJabber::RES_PARAM);

    return $listener->response(200);
  }

}