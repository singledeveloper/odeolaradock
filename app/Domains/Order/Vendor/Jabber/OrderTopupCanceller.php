<?php

namespace Odeo\Domains\Order\Vendor\Jabber;

use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\OrderCanceller;
use Odeo\Domains\Vendor\Jabber\JabberManager;
use Odeo\Domains\Core\Pipeline;

class OrderTopupCanceller extends JabberManager {

  private $topup, $orderDetailTopup;

  public function __construct() {
    parent::__construct();
    $this->topup = app()->make(\Odeo\Domains\Transaction\Repository\UserTopupRepository::class);
    $this->orderDetailTopup = app()->make(\Odeo\Domains\Transaction\Repository\OrderDetailTopupRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {

    if ($topup = $this->topup->findLastTopup($data["auth"]["user_id"])) {
      $orderDetailTopup = $this->orderDetailTopup->findByTopupId($topup->id);

      $pipeline = new Pipeline();
      $pipeline->add(new Task(OrderCanceller::class, 'cancel'));
      $pipeline->enableTransaction();
      $pipeline->execute([
        'order_id' => $orderDetailTopup->orderDetail->order_id,
        'auth' => $data['auth']
      ]);

      if ($pipeline->fail()) $message = 'GAGAL ' . $pipeline->errorMessage;
      else $message = 'Tiket berhasil dihapus.';
    }
    else $message = 'Anda belum pernah melakukan topup/tiket.';

    $listener->appendResponseMessage($message, InlineJabber::RES_PARAM);

    return $listener->response(200);
  }

}