<?php

namespace Odeo\Domains\Order\Vendor\Jabber;

use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Payment\PaymentOpenRequester;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Domains\Transaction\TopupRequester;
use Odeo\Domains\Vendor\Jabber\JabberManager;
use Odeo\Domains\Core\Pipeline;

class OrderTopupRequester extends JabberManager {

  private $currencyHelper, $jabberOrder, $topup, $orderDetailTopup;

  public function __construct() {
    parent::__construct();
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->jabberOrder = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberOrderRepository::class);
    $this->topup = app()->make(\Odeo\Domains\Transaction\Repository\UserTopupRepository::class);
    $this->orderDetailTopup = app()->make(\Odeo\Domains\Transaction\Repository\OrderDetailTopupRepository::class);
  }

  /**
   * @param PipelineListener $listener
   * @param $data
   * @return mixed
   */
  public function parse(PipelineListener $listener, $data) {

    list($command, $bank, $amount, $pin) = explode('.', $data['jabber_message']);

    $opc = (function () use ($bank) {
      switch (strtoupper($bank)) {
        case 'BCA':
          return 372;
        case 'MANDIRI':
          return 373;
        case 'BRI':
          return 374;
      }
      return null;
    })();
    if ($opc == null) $message = 'GAGAL Kode bank ' . strtoupper($bank) . ' tidak valid';
    else {
      $pipeline = new Pipeline();

      $data['gateway_id'] = Payment::JABBER;
      $data['platform_id'] = Platform::JABBER;

      $pipeline->add(new Task(CartRemover::class, 'clear'));
      $pipeline->add(new Task(TopupRequester::class, 'request'));
      $pipeline->add(new Task(CartInserter::class, 'addToCart', ['service_detail_id' => ServiceDetail::TOPUP_ODEO]));
      $pipeline->add(new Task(CartCheckouter::class, 'checkout'));
      $pipeline->add(new Task(PaymentRequester::class, 'request', ['opc' => $opc]));
      $pipeline->add(new Task(PaymentOpenRequester::class, 'open'));

      $pipeline->enableTransaction();
      $pipeline->execute(array_merge($data, [
        'cash' => [
          'amount' => $amount,
          'currency' => 'IDR'
        ]
      ]));

      if ($pipeline->fail()) {
        $error = $pipeline->errorMessage;
        if ($error == trans('errors.cash.pending_topup_exists')) {
          $topup = $this->topup->findLastTopup($data["auth"]["user_id"]);
          $orderDetailTopup = $this->orderDetailTopup->findByTopupId($topup->id);
          $order = $orderDetailTopup->orderDetail->order;
          $detail = json_decode($order->payment->information->detail);

          $error .= trans('pulsa.inline.topup_queue', [
            'amount' => $this->currencyHelper->formatPrice($order->total)['formatted_amount'],
            'bank_name' => $detail->acc_bank,
            'account_number' => $detail->acc_number,
            'account_name' => $detail->acc_name,
            'expired_at' => date('d-m-Y H:i', strtotime($order->expired_at)),
            'cancel_command' => strtoupper(Inline::CMD_HAPUS_TIKET) . '.PIN'
          ]);
        }
        $message = 'GAGAL ' . $error;
      }
      else {
        $jabberOrder = $this->jabberOrder->getNew();
        $jabberOrder->order_id = $pipeline->data['order_id'];
        $jabberOrder->jabber_user_id = $data['jabber_user_id'];
        $jabberOrder->jabber_store_id = $data['jabber_store_id'];
        $this->jabberOrder->save($jabberOrder);

        $result = $pipeline->data;
        $message = 'Order #' . $jabberOrder->order_id . " " .
          trans('pulsa.inline.topup_queue', [
            'amount' => $this->currencyHelper->formatPrice($result['total'])['formatted_amount'],
            'bank_name' => $result['content']->acc_bank,
            'account_number' => $result['content']->acc_number,
            'account_name' => $result['content']->acc_name,
            'expired_at' => date('d-m-Y H:i', strtotime($result['expired_at'])),
            'cancel_command' => strtoupper(Inline::CMD_HAPUS_TIKET) . '.PIN'
          ]);
      }
    }

    $listener->appendResponseMessage($message, InlineJabber::RES_PARAM);

    return $listener->response(200);
  }

}