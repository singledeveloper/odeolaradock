<?php

namespace Odeo\Domains\Order\Vendor\SMS;

use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Vendor\SMS\CenterManager;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Marketing\QueueSelector;
use Odeo\Domains\Payment\PaymentOpenRequester;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Domains\Transaction\TempDepositRequester;

class OrderPurchaser extends CenterManager {

  private $pulsaOdeo, $pulsaPrefix, $switcherOrder, $inventoryManager, $status, $priceGetter, $users;

  public function __construct() {
    parent::__construct();
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->pulsaPrefix = app()->make(\Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaPrefixRepository::class);
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->inventoryManager = app()->make(\Odeo\Domains\Inventory\Helper\InventoryManager::class);
    $this->status = app()->make(\Odeo\Domains\Order\Helper\StatusParser::class);
    $this->priceGetter = app()->make(\Odeo\Domains\Subscription\ManageInventory\Helper\PriceGetter::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $pulsaOdeo = false;
    $temp = explode('.', $data['sms_message']);
    if (count($temp) == 4) {
      list($command, $code, $number, $pin) = $temp;
      if ($command != Inline::CMD_BELI) list($code, $number, $smsTo, $pin) = $temp;
    }
    else if (count($temp) == 3) list($code, $number, $pin) = $temp;
    else return $listener->response(200);

    if (revertTelephone($number) != $number) $this->reply('GAGAL Nomor ' . $number . ' tidak valid.', $data['sms_to']);
    else {
      $loop = 1;
      if (strpos($number, '#') !== false) {
        list($number, $loop) = explode('#', $number);
        $loop = intval($loop);
        if ($loop == 0) $loop = 1;
      }

      if (intval($code)) {
        $temp = substr($number, 0, 3);
        if ($temp != '999') $temp = substr(purifyTelephone($number), 0, 5);
        if (!$pulsaPrefix = $this->pulsaPrefix->getNominal(['prefix' => $temp])) {
          $this->reply('Gagal nomor ' . $number . ' tidak valid atau stok kosong.', $data['sms_to']);
          return $listener->response(200);
        }

        $pulsaOdeo = $this->pulsaOdeo->findByNominal($code . '000', $pulsaPrefix->operator_id);
      }
      else {
        $user = $this->users->findById($data['auth']['user_id']);
        if ($user->purchase_preferred_store_id) {
          $pulsaOdeo = $this->priceGetter->checkSupplyChainExistFromInventoryCode([
            'inventory_code' => strtoupper($code),
            'user_id' => $data['auth']['user_id'],
            'store_id' => $user->purchase_preferred_store_id
          ]);
        }
        if (!$pulsaOdeo) $pulsaOdeo = $this->pulsaOdeo->findByInventoryCode(strtoupper($code));
      }

      if ($pulsaOdeo) {
        if (isset($smsTo) && $pulsaOdeo->operator_id != Pulsa::OPERATOR_PLN)
          $this->reply(trans('pulsa.inline.block_sms_to', [
            'number' => revertTelephone($number)
          ]), $data['sms_to']);
        else if ($history = $this->centerHistories->findSameMessage(strtoupper($data['sms_message']))) {
          $same = $this->switcherOrder->getSamePurchaseCount(purifyTelephone($number), $pulsaOdeo->id);
          if ($loop <= count($same)) $this->reply($this->status->parseMessageForSameTransaction($same[$loop - 1]), $data['sms_to']);
          else {
            if (time() - strtotime($history->created_at) > 10 * 60) $this->centerHistories->delete($history);

            $this->reply(trans('pulsa.inline.same_transaction', [
              'date' => date('d/m/Y H:i', strtotime($history->created_at)),
              'message' => 'Trx ke ' . $number . ' dalam antrian, mohon menunggu maksimal 10 menit.'
            ]), $data['sms_to']);
          }
        }
        else {
          $history = $this->saveHistory($data);

          $pipeline = new Pipeline();

          $data = array_merge($data, $this->inventoryManager->getServiceDataFromCategory($pulsaOdeo->category));

          $data['info_id'] = Payment::OPC_GROUP_OCASH;
          $data['pulsa_odeo_id'] = $data['item_id'] = $pulsaOdeo->id;
          $data['gateway_id'] = Payment::SMS_CENTER;
          $data['platform_id'] = Platform::SMS_CENTER;
          $data['opc'] = 367;
          $data['item_detail'] = [
            'number' => purifyTelephone($number),
            'operator' => $pulsaOdeo->category
          ];
          if (isset($smsTo)) $data['item_detail']['sms_to'] = $smsTo;

          $pipeline->add(new Task(QueueSelector::class, 'get', ['use_preferred_store' => true]));
          $pipeline->add(new Task(CartRemover::class, 'clear'));
          $pipeline->add(new Task(CartInserter::class, 'addToCart'));
          $pipeline->add(new Task(CartCheckouter::class, 'checkout'));
          $pipeline->add(new Task(TempDepositRequester::class, 'tempDeposit'));
          $pipeline->add(new Task(PaymentRequester::class, 'request'));
          $pipeline->add(new Task(PaymentOpenRequester::class, 'open', [
            'with_password' => true
          ]));

          $pipeline->enableTransaction();
          $pipeline->execute($data);

          if ($pipeline->fail()) {
            $this->centerHistories->delete($history);
            try {
              $this->reply($pipeline->errorMessage == Pulsa::MAINTANANCE_MESSAGE ?
                ($pipeline->errorMessage . ' Mohon untuk tidak menggagalkan.') :
                ('GAGAL Trx ke ' . revertTelephone($number) . '. ' . $pipeline->errorMessage), $data['sms_to']);
            }
            catch (\Exception $e) {
              $this->reply('GAGAL Trx ke ' . revertTelephone($number) . '. Silakan dicoba beberapa saat lagi.', $data['sms_to']);
            }
          }
          else {
            $history->order_id = $pipeline->data['order_id'];
            $this->centerHistories->save($history);
          }
        }
      }
      else $this->reply('GAGAL Kode ' . $code . ' tidak valid', $data['sms_to']);
    }

    return $listener->response(200);
  }

}
