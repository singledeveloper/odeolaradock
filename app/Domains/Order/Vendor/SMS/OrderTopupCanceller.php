<?php

namespace Odeo\Domains\Order\Vendor\SMS;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\OrderCanceller;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Vendor\SMS\CenterManager;

class OrderTopupCanceller extends CenterManager {

  private $topup, $orderDetailTopup;

  public function __construct() {
    parent::__construct();
    $this->topup = app()->make(\Odeo\Domains\Transaction\Repository\UserTopupRepository::class);
    $this->orderDetailTopup = app()->make(\Odeo\Domains\Transaction\Repository\OrderDetailTopupRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $this->saveHistory($data);

    if ($topup = $this->topup->findLastTopup($data["auth"]["user_id"])) {
      $orderDetailTopup = $this->orderDetailTopup->findByTopupId($topup->id);

      $pipeline = new Pipeline();
      $pipeline->add(new Task(OrderCanceller::class, 'cancel'));
      $pipeline->enableTransaction();
      $pipeline->execute([
        'order_id' => $orderDetailTopup->orderDetail->order_id,
        'auth' => $data['auth']
      ]);

      if ($pipeline->fail()) $this->reply('Penghapusan tiket gagal. Silakan dicoba beberapa saat lagi.', $data['sms_to']);
      else $this->reply('Tiket berhasil dihapus.', $data['sms_to']);
    }
    else $this->reply('Anda belum pernah melakukan topup/tiket.', $data['sms_to']);

    return $listener->response(200);
  }

}