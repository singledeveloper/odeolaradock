<?php

namespace Odeo\Domains\Order\Vendor\SMS;

use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Payment\PaymentOpenRequester;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Domains\Transaction\TopupRequester;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Vendor\SMS\CenterManager;

class OrderTopupRequester extends CenterManager {

  private $currencyHelper, $topup, $orderDetailTopup;

  public function __construct() {
    parent::__construct();
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->topup = app()->make(\Odeo\Domains\Transaction\Repository\UserTopupRepository::class);
    $this->orderDetailTopup = app()->make(\Odeo\Domains\Transaction\Repository\OrderDetailTopupRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $this->saveHistory($data);

    list($command, $bank, $amount, $pin) = explode('.', $data['sms_message']);

    $opc = (function () use ($bank) {
      switch (strtoupper($bank)) {
        case 'BCA':
          return 395;
        case 'MANDIRI':
          return 394;
        case 'BRI':
          return 396;
      }
      return null;
    })();

    if ($opc != null) {
      $history = $this->centerHistories->getNew();
      $history->message = $data['sms_message'];
      $history->sms_center_user_id = $data['sms_user_id'];
      $this->centerHistories->save($history);

      $pipeline = new Pipeline();

      $data['gateway_id'] = Payment::SMS_CENTER;
      $data['platform_id'] = Platform::SMS_CENTER;

      $pipeline->add(new Task(CartRemover::class, 'clear'));
      $pipeline->add(new Task(TopupRequester::class, 'request'));
      $pipeline->add(new Task(CartInserter::class, 'addToCart', ['service_detail_id' => ServiceDetail::TOPUP_ODEO]));
      $pipeline->add(new Task(CartCheckouter::class, 'checkout'));
      $pipeline->add(new Task(PaymentRequester::class, 'request', ['opc' => $opc]));
      $pipeline->add(new Task(PaymentOpenRequester::class, 'open'));

      $pipeline->enableTransaction();
      $pipeline->execute(array_merge($data, [
        'cash' => [
          'amount' => $amount,
          'currency' => 'IDR'
        ]
      ]));

      if ($pipeline->fail()) {
        $this->centerHistories->delete($history);

        $error = $pipeline->errorMessage;
        if ($error == trans('errors.cash.pending_topup_exists')) {
          $topup = $this->topup->findLastTopup($data["auth"]["user_id"]);
          $orderDetailTopup = $this->orderDetailTopup->findByTopupId($topup->id);
          $order = $orderDetailTopup->orderDetail->order;
          $detail = json_decode($order->payment->information->detail);

          $error .= trans('pulsa.inline.topup_queue', [
            'amount' => $this->currencyHelper->formatPrice($order->total)['formatted_amount'],
            'bank_name' => $detail->acc_bank,
            'account_number' => $detail->acc_number,
            'account_name' => $detail->acc_name,
            'expired_at' => date('d-m-Y H:i', strtotime($order->expired_at)),
            'cancel_command' => strtoupper(Inline::CMD_HAPUS_TIKET) . '.PIN'
          ]);
        }
        else $error = 'Topup gagal. Silakan dicoba beberapa saat lagi.';

        $this->reply($error, $data['sms_to']);
      }
      else {
        $history->order_id = $pipeline->data['order_id'];
        $this->centerHistories->save($history);

        $result = $pipeline->data;

        $this->reply(trans('pulsa.inline.topup_queue', [
          'amount' => $this->currencyHelper->formatPrice($result['total'])['formatted_amount'],
          'bank_name' => $result['content']->acc_bank,
          'account_number' => $result['content']->acc_number,
          'account_name' => $result['content']->acc_name,
          'expired_at' => date('d-m-Y H:i', strtotime($result['expired_at'])),
          'cancel_command' => strtoupper(Inline::CMD_HAPUS_TIKET) . '.PIN'
        ]), $data['sms_to']);
      }
    }

    return $listener->response(200);
  }

}