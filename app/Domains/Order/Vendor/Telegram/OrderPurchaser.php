<?php

namespace Odeo\Domains\Order\Vendor\Telegram;

use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Marketing\QueueSelector;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Payment\PaymentOpenRequester;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Domains\Transaction\TempDepositRequester;
use Odeo\Domains\Vendor\Telegram\TelegramManager;
use Odeo\Domains\Core\Pipeline;

class OrderPurchaser extends TelegramManager {

  private $pulsaOdeo, $pulsaPrefix, $telegramOrder,
    $switcherOrder, $inventoryManager, $status, $priceGetter, $users;

  public function __construct() {
    parent::__construct();
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->pulsaPrefix = app()->make(\Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaPrefixRepository::class);
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->telegramOrder = app()->make(\Odeo\Domains\Vendor\Telegram\Repository\TelegramOrderRepository::class);
    $this->inventoryManager = app()->make(\Odeo\Domains\Inventory\Helper\InventoryManager::class);
    $this->status = app()->make(\Odeo\Domains\Order\Helper\StatusParser::class);
    $this->priceGetter = app()->make(\Odeo\Domains\Subscription\ManageInventory\Helper\PriceGetter::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $this->initialize($data['chat_id'], $data['token']);

    try {
      $temp = explode('.', $data['telegram_message']);
      list($command, $code, $number, $pin) = $temp;
      if ($command != Inline::CMD_BELI) list($code, $number, $smsTo, $pin) = $temp;
    }
    catch(\Exception $e) {
      try {
        list($code, $number, $pin) = explode('.', $data['telegram_message']);
      }
      catch (\Exception $e) {
        $this->reply('Format tidak valid');
        return $listener->response(200);
      }
    }

    if (strpos($number, '#') !== false) {
      list($number, $loop) = explode('#', $number);
      $loop = intval($loop);
      if ($loop == 0) $loop = 1;
    }
    else $loop = 1;

    $pulsaOdeo = false;
    if (intval($code)) {
      $temp = substr(purifyTelephone($number), 0, 3);
      if ($temp != '999') $temp = substr(purifyTelephone($number), 0, 5);
      if (!$pulsaPrefix = $this->pulsaPrefix->getNominal(['prefix' => $temp])) {
        $this->reply('Gagal nomor ' . $number . ' tidak valid atau stok kosong.');
        return $listener->response(200);
      }
      $pulsaOdeo = $this->pulsaOdeo->findByNominal($code . '000', $pulsaPrefix->operator_id);
    }
    else {
      $user = $this->users->findById($data['auth']['user_id']);
      if ($user->purchase_preferred_store_id) {
        $pulsaOdeo = $this->priceGetter->checkSupplyChainExistFromInventoryCode([
          'inventory_code' => strtoupper($code),
          'user_id' => $data['auth']['user_id'],
          'store_id' => $user->purchase_preferred_store_id
        ]);
      }
      if (!$pulsaOdeo) $pulsaOdeo = $this->pulsaOdeo->findByInventoryCode(strtoupper($code));
    }

    if ($pulsaOdeo) {
      if (isset($smsTo) && $pulsaOdeo->operator_id != Pulsa::OPERATOR_PLN)
        $this->reply(trans('pulsa.inline.block_sms_to', [
          'number' => revertTelephone($number)
        ]));
      else {
        $same = $this->switcherOrder->getSamePurchaseCount(purifyTelephone($number), $pulsaOdeo->id);
        if ($loop <= count($same)) $this->reply($this->status->parseMessageForSameTransaction($same[$loop - 1]));
        else {
          $pipeline = new Pipeline();

          $data = array_merge($data, $this->inventoryManager->getServiceDataFromCategory($pulsaOdeo->category));

          $data['info_id'] = Payment::OPC_GROUP_OCASH;
          $data['pulsa_odeo_id'] = $data['item_id'] = $pulsaOdeo->id;
          $data['gateway_id'] = Payment::TELEGRAM;
          $data['platform_id'] = Platform::TELEGRAM;
          $data['opc'] = 364;
          $data['item_detail'] = [
            'number' => purifyTelephone($number),
            'operator' => $pulsaOdeo->category
          ];
          if (isset($smsTo)) $data['item_detail']['sms_to'] = $smsTo;

          $pipeline->add(new Task(QueueSelector::class, 'get', ['use_preferred_store' => true]));
          $pipeline->add(new Task(CartRemover::class, 'clear'));
          $pipeline->add(new Task(CartInserter::class, 'addToCart'));
          $pipeline->add(new Task(CartCheckouter::class, 'checkout'));
          $pipeline->add(new Task(TempDepositRequester::class, 'tempDeposit'));
          $pipeline->add(new Task(PaymentRequester::class, 'request'));
          $pipeline->add(new Task(PaymentOpenRequester::class, 'open', [
            'with_password' => true
          ]));

          $pipeline->enableTransaction();
          $pipeline->execute($data);

          if ($pipeline->fail()) $this->reply($pipeline->errorMessage);
          else {
            $telegramStore = $this->telegramStores->findByToken($data['token']);

            $telegramOrder = $this->telegramOrder->getNew();
            $telegramOrder->order_id = $pipeline->data['order_id'];
            $telegramOrder->chat_id = $this->getChatId();
            $telegramOrder->telegram_store_id = $telegramStore->id;
            $this->telegramOrder->save($telegramOrder);

            $this->reply('Order #' . $pipeline->data['order_id'] . ' : '
              . $number . ' - ' . $pulsaOdeo->name . ' sedang diproses');
          }
        }
      }
    }
    else $this->reply('Kode ' . $code . ' tidak valid');

    return $listener->response(200);
  }

}