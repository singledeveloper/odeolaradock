<?php

namespace Odeo\Domains\Order\Vendor\Telegram;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Telegram\TelegramManager;

class OrderSelector extends TelegramManager {

  private $order, $status;

  public function __construct() {
    parent::__construct();
    $this->order = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->status = app()->make(\Odeo\Domains\Order\Helper\StatusParser::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $this->initialize($data['chat_id'], $data['token']);

    list($command, $orderId, $pin) = explode('.', $data['telegram_message']);
    $order = $this->order->findById(intval($orderId));

    if (!$order || ($order && $order->user_id != $data['auth']['user_id']))
      $this->reply("Order ID tidak valid.");
    else {
      $message = "Order #" . $order->id . ' status: ' . $this->status->parse($order)['status_message'];
      foreach ($order->details as $item) {
        $message .= ', ' . $item->name;
        if ($item->switcher) {
          $message .= ' ' . $item->switcher->number . ' - SN: ' .
            ($item->switcher->serial_number != null ? $item->switcher->serial_number : 'N/A');
        }
      }
      $this->reply($message);
    }

    return $listener->response(200);
  }

}