<?php

namespace Odeo\Domains\Order\Vendor\Telegram;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\OrderCanceller;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Vendor\Telegram\TelegramManager;

class OrderTopupCanceller extends TelegramManager {

  private $topup, $orderDetailTopup;

  public function __construct() {
    parent::__construct();
    $this->topup = app()->make(\Odeo\Domains\Transaction\Repository\UserTopupRepository::class);
    $this->orderDetailTopup = app()->make(\Odeo\Domains\Transaction\Repository\OrderDetailTopupRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $this->initialize($data['chat_id'], $data['token']);

    if ($topup = $this->topup->findLastTopup($data["auth"]["user_id"])) {
      $orderDetailTopup = $this->orderDetailTopup->findByTopupId($topup->id);

      $pipeline = new Pipeline();
      $pipeline->add(new Task(OrderCanceller::class, 'cancel'));
      $pipeline->enableTransaction();
      $pipeline->execute([
        'order_id' => $orderDetailTopup->orderDetail->order_id,
        'auth' => $data['auth']
      ]);

      if ($pipeline->fail()) $this->reply('GAGAL ' . $pipeline->errorMessage);
      else $this->reply('Tiket berhasil dihapus.');
    }
    else $this->reply('Anda belum pernah melakukan topup/tiket.');

    return $listener->response(200);
  }

}