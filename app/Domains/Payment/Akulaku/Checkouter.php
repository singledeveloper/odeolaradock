<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/21/17
 * Time: 10:06 PM
 */

namespace Odeo\Domains\Payment\Akulaku;

use GuzzleHttp\Client;
use Odeo\Domains\Account\Repository\UserLoginHistoryRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository;
use Odeo\Domains\Order\Helper\OrderDetailizer;
use Odeo\Domains\Payment\Akulaku\Helper\AkulakuApi;
use Odeo\Domains\Payment\Akulaku\Helper\AkulakuManager;
use Odeo\Domains\Payment\Helper\FeeGenerator;

class Checkouter extends AkulakuManager {

  public function __construct() {
    parent::__construct();
    $this->users = app()->make(UserRepository::class);
    $this->loginHistories = app()->make(UserLoginHistoryRepository::class);
    $this->feeGenerator = app()->make(FeeGenerator::class);
  }

  public function checkout(PipelineListener $listener, $order, $paymentInformation, $data) {

    if ($data['auth']['type'] == 'guest') {
      return $listener->response(400, 'Login terlebih dahulu untuk menggunakan metode pembayaran akulaku');
    }

    if ($order->status != OrderStatus::CREATED) {
      return $listener->response(400, 'Please wait, we will verify your order once akulaku has notified us your payment.');
    }

    if ($this->isAbusableProduct($order)) {
      return $listener->response(400, 'Maaf, produk ini tidak bisa menggunakan pembayaran Akulaku, mohon menggunakan metode pembayaran lain.');
    }

    $user = $this->users->findById($data['auth']['user_id']);

    $orderDetails = self::getCurrentOrderDetail($order);

    $extraInfo = self::summarizeRecentOrders($user);

    $extraInfo['receiverEmail'] = $user->email;
    $extraInfo['registerTime'] = $user->created_at;
    $extraInfo['loginFrequency'] = (int)$this->loginHistories->getUserLoginCount($data['auth']['user_id']);

    $params = [
      'appId' => AkulakuApi::$APP_ID,
      'refNo' => $order->id,
      'receiverName' => trim($user->name),
      'receiverPhone' => $user->telephone,
      'details' => json_encode($orderDetails),
      'extraInfo' => json_encode($extraInfo)
    ];

    $params['sign'] = AkulakuApi::generateSign(implode(array_slice($params, 1)));

    $client = new Client();

    try {
      $response = $client->request('POST', AkulakuApi::$GENERATE_ORDER_URL,
        [
          'form_params' => $params,
          'timeout' => 60,
          'verify' => false
        ]);
    } catch (\Exception $e) {
      clog('akulaku_error', 'checkout:' . $e->getMessage());
      return $this->errorResponse($listener);
    }

    $res = json_decode($response->getBody()->getContents());

    $paymentUrl = AkulakuApi::$PAYMENT_ENTRY_URL . '?appId=' . AkulakuApi::$APP_ID . '&refNo=' . $order->id . '&sign=' . str_replace(['+', '/', '='], ['-', '_', ''], AkulakuApi::generateSign($order->id)) . '&lang=id';

    if (isset($res->success) && $res->success != true) {
      if ($res->errCode == 'openpay.0002') {
        if ($akulakuPayment = $this->akulakuPayments->findByRefNo($order->id)) {
          return $listener->response(200, [
            'type' => Payment::REQUEST_PAYMENT_TYPE_URL,
            'content' => [
              'link' => $paymentUrl,
            ]
          ]);
        }
      } else {
        clog('akulaku_error', 'checkout:' . \json_encode($params));
        clog('akulaku_error', 'checkout:' . \json_encode($res));
        return $this->errorResponse($listener);
      }
    }

    $akulakuPayment = $this->akulakuPayments->getNew();

    $akulakuPayment->ref_no = $order->id;
    $akulakuPayment->payment_entry_url = $paymentUrl;
    $akulakuPayment->log_request = json_encode($params);
    $akulakuPayment->log_response = json_encode($res);

    $this->akulakuPayments->save($akulakuPayment);

    $order->status = OrderStatus::CREATED;
    $this->orders->save($order);

    $url = $this->paymentUrls->getNew();

    $url->order_id = $order->id;
    $url->opc = $paymentInformation->code;
    $url->body = json_encode([
      'order_id' => $order->id,
      'opc' => $paymentInformation->code,
      'finish_redirect_url' => $data['finish_redirect_url']
    ]);

    $this->paymentUrls->save($url);

    return $listener->response(200, [
      'type' => Payment::REQUEST_PAYMENT_TYPE_URL,
      'content' => [
        'link' => $paymentUrl
      ]
    ]);
  }

  private function isAbusableProduct($order) {
    $switchers = app()->make(OrderDetailPulsaSwitcherRepository::class);
    if ($switcher = $switchers->findByOrderId($order->id)) {
      if ($switcher->currentInventory->pulsa->category == PulsaCode::GOJEK) {
        return true;
      }
    }
    return false;
  }

  private function errorResponse($listener) {
    return $listener->response(
      400,
      'Something went wrong with AKULAKU server'
    );
  }

  private function getCurrentOrderDetail($order) {

    $orderDetailizer = app()->make(OrderDetailizer::class);

    $item = array_first($orderDetailizer->detailizeItem($order));

    $amount = $item['price']['amount'];
    $fee = $this->calculateFee($amount);

    return [
      'skuId' => $item['service_detail_id'],
      'skuName' => $item['name'],
      'unitPrice' => $amount + $fee,
      'qty' => (integer)$item['quantity'],
      'img' => '',
      'reservedField' => ''
    ];
  }


  private function calculateFee($amount) {
    return $this->feeGenerator->getFee(1.5, $amount);
  }

  private function summarizeRecentOrders($user) {

    $orders = [];

    $lastThreeOrders = $this->orders->getUserLastThreeOrders();

    if ($lastThreeOrders->isEmpty()) return [];


    foreach ($lastThreeOrders as $order) {

      $temp['receiverName'] = $user->name;
      $temp['receiverPhone'] = $user->telephone;
      $temp['receiverAddress'] = $user->street;
      $temp['skuName'] = $order->item_name;
      $temp['payAmount'] = $order->subtotal;
      $temp['payTime'] = strtotime($order->paid_at);
      $orders[] = $temp;
    }
    return [
      'firstPurchasedTime' => strtotime($this->orders->findUserFirstOrder()->paid_at),
      'costAmount' => $this->orders->getSixMonthOrderTotal(),
      'details' => $orders
    ];
  }
}
