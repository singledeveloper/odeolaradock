<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/21/17
 * Time: 8:37 PM
 */

namespace Odeo\Domains\Payment\Akulaku\Helper;


use Odeo\Domains\Payment\Akulaku\Repository\PaymentAkulakuPaymentRepository;
use Odeo\Domains\Payment\Helper\PaymentManager;
use Odeo\Domains\Payment\Repository\PaymentUrlRepository;

class AkulakuManager extends PaymentManager {

  protected $api, $akulakuPayments, $akulakuPaymentDetails, $paymentUrls;

  public function __construct() {
    parent::__construct();
    $this->api = app()->make(AkulakuApi::class);
    $this->akulakuPayments = app()->make(PaymentAkulakuPaymentRepository::class);
    $this->paymentUrls = app()->make(PaymentUrlRepository::class);
    $this->api->init();
  }

}