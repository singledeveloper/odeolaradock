<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/24/17
 * Time: 1:17 PM
 */

namespace Odeo\Domains\Payment\Akulaku;

use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\CancelOrder;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Akulaku\Helper\AkulakuApi;
use Odeo\Domains\Payment\Akulaku\Helper\AkulakuManager;

class Notifier extends AkulakuManager {

  public function notify(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {

    if (!isset($data['status']) || !isset($data['refNo'])) {
      clog('akulaku_error', "notify:payload_missing: " . json_encode($data));
      return $listener->response(400);
    }
    if ($data['sign'] != AkulakuApi::generateSign($data['refNo'] . $data['status'])) {
      clog('akulaku_error', "notify:sign not match: " . json_encode($data));
      return $listener->response(400);
    }

    if ($akulakuPayment = $this->akulakuPayments->findByRefNo($data['refNo'])) {

      $akulakuPayment->status = $data['status'];
      $akulakuPayment->log_notify = json_encode($data);

      if ($data['status'] == AkulakuApi::$PAYMENT_STATUS_PENDING) {
        $order->status = OrderStatus::WAITING_FOR_PAYMENT_VENDOR_APPROVAL;
        $this->orders->save($order);
      } else {

        if (in_array($data['status'], [AkulakuApi::$PAYMENT_STATUS_SUCCESS, AkulakuApi::$PAYMENT_STATUS_RECEIPTED])) {

          $akulakuPayment->is_verified = true;

          $fee = $this->calculateFee($order->subtotal);
          $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);
          $order->total = $order->total + $fee;

          $order->status = OrderStatus::VERIFIED;
          $order->paid_at = date('Y-m-d H:i:s');
          $this->orders->save($order);


          $listener->pushQueue(new VerifyOrder($order->id));

        } else if (in_array($data['status'], [AkulakuApi::$PAYMENT_STATUS_FAILED, AkulakuApi::$PAYMENT_STATUS_CANCELLED])) {

          $listener->pushQueue(new CancelOrder($order->id));

        } else if ($data['status'] == AkulakuApi::$PAYMENT_STATUS_REFUND) {
          return $listener->response(400);
        }
      }

      $this->akulakuPayments->save($akulakuPayment);

    } else {
      clog('akulaku_error', "notify:UNKNOWN: " . json_encode($data));
      return $listener->response(400);
    }

    return $listener->response(200);
  }

  private function calculateFee($amount) {
    return $this->feeGenerator->getFee(1.5, $amount);
  }

}
