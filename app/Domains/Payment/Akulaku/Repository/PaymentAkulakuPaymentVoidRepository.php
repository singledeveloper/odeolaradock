<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/30/17
 * Time: 5:29 PM
 */

namespace Odeo\Domains\Payment\Akulaku\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Akulaku\Model\PaymentAkulakuPaymentVoid;

class PaymentAkulakuPaymentVoidRepository extends Repository {

  public function __construct(PaymentAkulakuPaymentVoid $paymentAkulakuPaymentVoid) {
    $this->model = $paymentAkulakuPaymentVoid;
  }

  public function findByOrderId($orderId) {
    return $this->model->where('order_id', $orderId)->first();
  }

  public function findUnprocessedOrder() {
    return $this->model->where('void_status', false)->get();
  }

}
