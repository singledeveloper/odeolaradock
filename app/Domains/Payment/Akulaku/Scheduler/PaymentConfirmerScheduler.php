<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/22/17
 * Time: 11:58 AM
 */

namespace Odeo\Domains\Payment\Akulaku\Scheduler;


use GuzzleHttp\Client;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Payment\Akulaku\Helper\AkulakuApi;
use Odeo\Domains\Payment\Akulaku\Helper\AkulakuManager;

class PaymentConfirmerScheduler extends AkulakuManager {

  public function run() {

    if ($unconfirmedPayments = $this->akulakuPayments->getUnconfirmedPayments()) {

      $client = new Client(['verify' => false]);

      foreach ($unconfirmedPayments as $payment) {

        $data = [
          'appId' => AkulakuApi::$APP_ID,
          'refNo' => $payment->ref_no,
          'sign' => AkulakuApi::generateSign($payment->ref_no)
        ];

        try {

          $response = $client->post(AkulakuApi::$CONFIRM_RECEIPT_URL, ['form_params' => $data]);

        } catch (\Exception $e) {
          clog('akulaku_error', 'confirm: ' . $e->getMessage());
          clog('akulaku_error', 'confirm: ' . $e->getTraceAsString());
          continue;
        }

        $data = \json_decode($response->getBody()->getContents(), true);

        if (!isset($data['data']) || $data['data'] != 'ok' || $data['success'] != true) {
          if ((isset($data['errCode']) && $data['errCode'] != 'openpay.0014')) {
            \Log::info('orderID: ' . $payment->ref_no . \json_encode($data));
            continue;
          }
        }

        $payment->is_confirmed = true;
        $payment->log_confirm = json_encode($data);

        $this->akulakuPayments->save($payment);


      }
    }
  }

}