<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/27/17
 * Time: 3:13 PM
 */

namespace Odeo\Domains\Payment\Akulaku\Scheduler;


use Carbon\Carbon;
use GuzzleHttp\Client;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Order\Jobs\CancelOrder;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Akulaku\Helper\AkulakuApi;
use Odeo\Domains\Payment\Akulaku\Helper\AkulakuManager;


class PaymentVerificationScheduler extends AkulakuManager {

  public function run() {

    if ($unverifiedPayments = $this->akulakuPayments->getUnverifiedPayments()) {

      $client = new Client(['verify' => false]);

      foreach ($unverifiedPayments as $payment) {

        $order = $this->orders->findById($payment->ref_no);

        if ($order->status >= OrderStatus::VERIFIED) continue;

        try {

          $response = $client->get(AkulakuApi::$INQUIRY_STATUS_URL, [
            'query' => [
              'appId' => AkulakuApi::$APP_ID,
              'refNo' => $payment->ref_no,
              'sign' => AkulakuApi::generateSign($payment->ref_no)
            ],
            'timeout' => 60,
          ]);

        } catch (\Exception $e) {
          continue;
        }

        $data = json_decode($response->getBody()->getContents(), true);

        if (!isset($data['data']['status']) || !isset($data['data']['refNo'])) continue;

        $payment->status = $data['data']['status'];
        $payment->log_notify = json_encode($data);

        if ($data['data']['status'] == AkulakuApi::$PAYMENT_STATUS_PENDING) {

          if ($this->orderHasExceeded($order)) {

            try {
              $this->cancelOrder($order->id);

            } catch (\Exception $e) {
              continue;
            }

            dispatch(new CancelOrder($order->id));

          } else {
            $order->status = OrderStatus::WAITING_FOR_PAYMENT_VENDOR_APPROVAL;
            $this->orders->save($order);
          }

        } else if (in_array($data['data']['status'], [AkulakuApi::$PAYMENT_STATUS_FAILED, AkulakuApi::$PAYMENT_STATUS_CANCELLED])) {

          dispatch(new CancelOrder($order->id));

        } else {

          $payment->is_verified = true;

          if (in_array($data['data']['status'], [AkulakuApi::$PAYMENT_STATUS_SUCCESS, AkulakuApi::$PAYMENT_STATUS_RECEIPTED])) {

            $fee = $this->calculateFee($order->subtotal);
            $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);
            $order->total = $order->total + $fee;


            $order->status = OrderStatus::VERIFIED;
            $order->paid_at = date('Y-m-d H:i:s');
            $this->orders->save($order);

            dispatch(new VerifyOrder($order->id));

          }
        }


        $this->akulakuPayments->save($payment);

      }
    }
  }

  private function calculateFee($amount) {
    return $this->feeGenerator->getFee(1.5, $amount);
  }

  private function orderHasExceeded($order) {
    $createdAt = Carbon::parse($order->created_at);
    return Carbon::now()->diffInDays($createdAt) > 3;
  }

  private function cancelOrder($orderId) {

    $client = new Client();

    $client->post(AkulakuApi::$CANCEL_ORDER_URL, [
      'form_params' => [
        'appId' => AkulakuApi::$APP_ID,
        'refNo' => $orderId,
        'sign' => AkulakuApi::generateSign($orderId)
      ],
      'timeout' => 60,
      'verify' => false
    ]);

  }

}
