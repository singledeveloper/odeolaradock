<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/30/17
 * Time: 4:41 PM
 */

namespace Odeo\Domains\Payment\Akulaku;

use Odeo\Domains\Payment\Akulaku\Repository\PaymentAkulakuPaymentRepository;
use Odeo\Domains\Payment\Akulaku\Repository\PaymentAkulakuPaymentVoidRepository;

class VoidRequester  {

  private $akulakuPaymentVoidRepo, $akulakuPaymentRepo;

  public function __construct() {
    $this->akulakuPaymentRepo = app()->make(PaymentAkulakuPaymentRepository::class);
    $this->akulakuPaymentVoidRepo = app()->make(PaymentAkulakuPaymentVoidRepository::class);
  }

  public function void($data, $payment) {

    if ($this->akulakuPaymentVoidRepo->findByOrderId($data['order_id'])) {
      return true;
    }

    $akulakuPayment = $this->akulakuPaymentRepo->findByRefNo($data['order_id']);

    $akulakuVoid = $this->akulakuPaymentVoidRepo->getNew();

    $akulakuVoid->akulaku_payment_id = $akulakuPayment->id;
    $akulakuVoid->order_id = $data['order_id'];
    $akulakuVoid->void_status = false;

    $this->akulakuPaymentVoidRepo->save($akulakuVoid);

    return true;
  }


}
