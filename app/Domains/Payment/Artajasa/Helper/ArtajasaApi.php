<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 08/08/19
 * Time: 15.01
 */

namespace Odeo\Domains\Payment\Artajasa\Helper;


class ArtajasaApi {

  const REQ_INQUIRY = 'reqinqpayment';
  const REQ_PAYMENT = 'reqnotification';
  const REQ_TRX_STATUS = 'reqtrxstatus';

  const RES_INQUIRY = 'resinqpayment';
  const RES_PAYMENT = 'resnotification';

  public static function getSignature() {
    $secret = env('AJ_VIRTUAL_ACCOUNT_SECRET');
    $username = env('AJ_VIRTUAL_ACCOUNT_USERNAME');
    return md5($secret . $username);
  }

  public static function validateRequest($data, $expectedRequest) {
    if ($data['type'] != $expectedRequest) {
      throw new \Exception("Invalid request");
    }

    if ($data['signature'] != self::getSignature()) {
      throw new \Exception("Invalid signature");
    }
  }

  public static function getUserName() {
    return env('AJ_VIRTUAL_ACCOUNT_USERNAME');
  }

}