<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 02/09/19
 * Time: 11.44
 */

namespace Odeo\Domains\Payment\Artajasa\Va;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Artajasa\Va\Repository\PaymentArtajasaVaInquiryRepository;

class InquirySelector {

  function __construct() {
    $this->ajVaInquiry = app()->make(PaymentArtajasaVaInquiryRepository::class);
  }

  public function getAll(PipelineListener $listener, $data) {
    $this->ajVaInquiry->normalizeFilters($data);
    $this->ajVaInquiry->setSimplePaginate(true);

    $result = [];
    $inquiries = $this->ajVaInquiry->get();

    foreach ($inquiries as $inquiry) {
      $result[] = $inquiry;
    }

    if (count($result) > 0) {
      return $listener->response(200, array_merge(
        ['inquiries' => $result],
        $this->ajVaInquiry->getPagination()
      ));
    }

    return $listener->response(200, [
      'inquiries' => []
    ]);
  }

}