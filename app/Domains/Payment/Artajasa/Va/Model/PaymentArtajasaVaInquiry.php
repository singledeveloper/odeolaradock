<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 08/08/19
 * Time: 18.37
 */

namespace Odeo\Domains\Payment\Artajasa\Va\Model;


use Odeo\Domains\Core\Entity;

class PaymentArtajasaVaInquiry extends Entity {

  protected $dates = ['booking_datetime'];

}