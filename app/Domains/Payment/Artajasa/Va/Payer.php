<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 06/08/19
 * Time: 18.22
 */

namespace Odeo\Domains\Payment\Artajasa\Va;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Constant\ArtajasaVaPaymentStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Artajasa\Helper\ArtajasaApi;
use Odeo\Domains\Payment\Artajasa\Va\Repository\PaymentArtajasaVaInquiryRepository;
use Odeo\Domains\Payment\Artajasa\Va\Repository\PaymentArtajasaVaPaymentRepository;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository;
use Odeo\Domains\Payment\Repository\PaymentRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;

class Payer {

  private $payment, $pgUserPaymentChannel, $odeoPaymentChannel, $ajVaInquiry, $ajVaPayment, $redis;

  function __construct() {
    $this->payment = app()->make(PaymentRepository::class);
    $this->ajVaInquiry = app()->make(PaymentArtajasaVaInquiryRepository::class);
    $this->ajVaPayment = app()->make(PaymentArtajasaVaPaymentRepository::class);
    $this->pgUserPaymentChannel = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
    $this->odeoPaymentChannel = app()->make(PaymentOdeoPaymentChannelRepository::class);
    $this->redis = Redis::connection();
  }

  public function vaPayment(PipelineListener $listener, $data) {
    try {
      ArtajasaApi::validateRequest($data, ArtajasaApi::REQ_PAYMENT);
    } catch (\Exception $e) {
      clog('artajasa_va', $e->getMessage());
      return $listener->response(400, $e->getMessage());
    }

    if (!$inquiry = $this->ajVaInquiry->findByBookingId($data['bookingid'])) {
      return $listener->response(400, [
        'message' => 'inquiry not found',
        'code' => '05']);
    }

    $currentDate = Carbon::now()->format('Y-m-d');
    $namespace = $this->getRedisNamespace($currentDate);
    if (!$this->redis->hsetnx($namespace, $inquiry->id, 1)) {
      return $listener->response(400, [
        'message' => 'duplicate transaction',
        'code' => '78'
      ]);
    }
    $this->redis->expire($namespace, 2 * 24 * 60 * 60);

    $tomorrowDate = Carbon::tomorrow()->format('Y-m-d');
    $namespace = $this->getRedisNamespace($tomorrowDate);
    if (!$this->redis->hset($namespace, $inquiry->id, 1)) {
      return $listener->response(400, [
        'message' => 'duplicate transaction',
        'code' => '78'
      ]);
    }
    $this->redis->expire($namespace, 2 * 24 * 60 * 60);

    if (!$pgUserChannel = $this->pgUserPaymentChannel->findByActivePrefix($inquiry->va_id)) {
      return $listener->response(400, [
        'message' => trans('payment.unrecognized_va_number'),
        'code' => '05'
      ]);
    }

    if (!$channel = $this->odeoPaymentChannel->findByGatewayIdAndGroupId(Payment::PAYMENT_GATEWAY, $pgUserChannel->payment_group_id)) {
      return $listener->response(400, [
        'message' => 'channel not found',
        'code' => '05'
      ]);
    }

    $vaPayment = $this->ajVaPayment->getNew();
    $vaPayment->booking_id = $data['bookingid'];
    $vaPayment->customer_name = $data['customer_name'];
    $vaPayment->issuer_bank = is_array($data['issuer_bank']) ? $data['issuer_bank'][0] : $data['issuer_bank'];
    $vaPayment->issuer_name = is_array($data['issuer_name']) ? $data['issuer_name'][0] : $data['issuer_name'];
    $vaPayment->amount = $data['amount'];
    $vaPayment->product_id = is_array($data['productid']) ? $data['productid'][0] : $data['productid'];
    $vaPayment->trx_id = $data['trxid'];
    $vaPayment->trx_date = is_array($data['trx_date']) ? $data['trx_date'][0] : $data['trx_date'];
    $vaPayment->username = $data['username'];
    $vaPayment->inquiry_id = $inquiry->id;
    $vaPayment->notification_datetime = is_array($data['notification_datetime']) ? $data['notification_datetime'][0] : $data['notification_datetime'];
    $vaPayment->cost = $pgUserChannel->cost;
    $vaPayment->status = ArtajasaVaPaymentStatus::PENDING;
    $this->ajVaPayment->save($vaPayment);

    $payment = $this->payment->getNew();
    $payment->source_type = PaymentChannel::PAYMENT_GATEWAY;
    $payment->info_id = $pgUserChannel->payment_group_id;
    $payment->opc = $channel->code;
    $payment->vendor_id = $vaPayment->id;
    $payment->vendor_type = 'va_artajasa';
    $this->payment->save($payment);
    
    $vaPayment->payment_id = $payment->id;
    $this->ajVaPayment->save($vaPayment);

    return $listener->response(200, [
      'type' => ArtajasaApi::RES_PAYMENT,
      'ack' => '00',
      'bookingid' => $data['bookingid'],
      'aj_payment_id' => $vaPayment->id,
      'payment_id' => $payment->id,
      'va_code' => $inquiry->va_id,
      'va_prefix' => $pgUserChannel->prefix,
      'user_id' => $pgUserChannel->user_id,
      'billed_amount' => $data['amount'],
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'customer_name' => $inquiry->customer_name,
      'vendor_reference_id' => $data['bookingid'],
      'signature' => ArtajasaApi::getSignature()
    ]);
  }

  public function getRedisNamespace($date) {
    return "odeo_core:aj_va_used_inquiry_id_$date";
  }

  public function updatePayment(PipelineListener $listener, $data) {
    $vaPayment = $this->ajVaPayment->findById($data['aj_payment_id']);
    $vaPayment->status = $data['pg_payment_status'];
    $this->ajVaPayment->save($vaPayment);

    if ($data['pg_payment_status'] != PaymentGateway::ACCEPTED) {
      return $listener->response(400, 'status is not accepted');
    }

    return $listener->response(200, []);
  }

}