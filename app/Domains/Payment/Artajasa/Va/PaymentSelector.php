<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 02/09/19
 * Time: 11.44
 */

namespace Odeo\Domains\Payment\Artajasa\Va;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Artajasa\Va\Repository\PaymentArtajasaVaPaymentRepository;

class PaymentSelector {

  function __construct() {
    $this->ajVaPayment = app()->make(PaymentArtajasaVaPaymentRepository::class);
  }

  public function getAll(PipelineListener $listener, $data) {
    $this->ajVaPayment->normalizeFilters($data);
    $this->ajVaPayment->setSimplePaginate(true);

    $result = [];
    $payments = $this->ajVaPayment->get();

    foreach ($payments as $payment) {
      $result[] = $payment;
    }

    if (count($result) > 0) {
      return $listener->response(200, array_merge(
        ['payments' => $result],
        $this->ajVaPayment->getPagination()
      ));
    }

    return $listener->response(200, ['payments' => []]);
  }

}