<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 08/08/19
 * Time: 18.34
 */

namespace Odeo\Domains\Payment\Artajasa\Va\Repository;


use Odeo\Domains\Constant\ArtajasaVaPaymentStatus;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Artajasa\Va\Model\PaymentArtajasaVaPayment;

class PaymentArtajasaVaPaymentRepository extends Repository {

  function __construct(PaymentArtajasaVaPayment $payment) {
    $this->model = $payment;
  }

  function get() {
    $filters = $this->getFilters();
    $q = $this->model
        ->join('payments', 'payment_artajasa_va_payments.payment_id', '=', 'payments.id')
        ->join('payment_artajasa_va_inquiries', 'payment_artajasa_va_payments.inquiry_id', '=', 'payment_artajasa_va_inquiries.id');

    if (isset($filters['search'])) {
      $search = $filters['search'];

      if (isset($search['id'])) {
        $q = $q->where('payment_artajasa_va_payments.id', $search['id']);
      }
      if (isset($search['va_id'])) {
        $q = $q->where('payment_artajasa_va_inquiries.va_id', $search['va_id']);
      }
      if (isset($search['booking_id'])) {
        $q = $q->where('payment_artajasa_va_payments.booking_id', $search['booking_id']);
      }
      if (isset($search['inquiry_id'])) {
        $q = $q->where('inquiry_id', $search['inquiry_id']);
      }
      if (isset($search['status'])) {
        $q = $q->where('status', $search['status']);
      }
    }

    $q = $q->select(
      'payment_artajasa_va_payments.id',
      'payment_artajasa_va_payments.booking_id',
      'payment_artajasa_va_payments.customer_name',
      'payment_artajasa_va_payments.issuer_bank',
      'payment_artajasa_va_payments.issuer_name',
      'payment_artajasa_va_payments.amount',
      'payment_artajasa_va_payments.product_id',
      'payment_artajasa_va_payments.trx_id',
      'payment_artajasa_va_payments.trx_date',
      'payment_artajasa_va_payments.username',
      'payment_artajasa_va_payments.notification_datetime',
      'payment_artajasa_va_payments.status',
      'payment_artajasa_va_payments.inquiry_id',
      'payment_artajasa_va_inquiries.va_id',
      'payments.source_id',
      'payments.source_type'
    );

    return $this->getResult($q->orderBy('payment_artajasa_va_payments.id', 'DESC'));
  }

  function getPendingPayment() {
    return $this->model
      ->whereIn('status', [
        ArtajasaVaPaymentStatus::ACCEPTED,
        ArtajasaVaPaymentStatus::PENDING,
        ArtajasaVaPaymentStatus::SUSPECT,
        ArtajasaVaPaymentStatus::SUSPECT_FOR_TESTING,
        ArtajasaVaPaymentStatus::SUSPECT_NEED_STATUS_RECHECK,
        ArtajasaVaPaymentStatus::SUSPECT_IS_CANCELLED_BUT_NEED_STATUS_RECHECK,
        ArtajasaVaPaymentStatus::SUSPECT_NOT_FOUND_ON_AJ
      ])
      ->get();
  }

  public function getUnsettled($date = NULL) {
    if (!isset($date)) $date = \Carbon\Carbon::today();

    return $this->model
      ->where('is_reconciled', false)
      ->where('status', ArtajasaVaPaymentStatus::SUCCESS)
      ->where('notification_datetime', '<', $date)
      ->orderBy('notification_datetime')
      ->selectRaw('date(notification_datetime) as date, payment_artajasa_va_payments.*')
      ->get();
  }
}