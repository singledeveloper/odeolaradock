<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 08/08/19
 * Time: 18.36
 */

namespace Odeo\Domains\Payment\Artajasa\Va\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Artajasa\Va\Model\PaymentArtajasaVaStatus;

class PaymentArtajasaVaStatusRepository extends Repository {

  function __construct(PaymentArtajasaVaStatus $status) {
    $this->model = $status;
  }

}