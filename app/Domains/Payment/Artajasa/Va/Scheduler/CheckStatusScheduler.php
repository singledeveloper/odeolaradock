<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 15/08/19
 * Time: 12.57
 */

namespace Odeo\Domains\Payment\Artajasa\Va\Scheduler;


use Odeo\Domains\Payment\Artajasa\Va\Job\InquiryStatus;
use Odeo\Domains\Payment\Artajasa\Va\Repository\PaymentArtajasaVaPaymentRepository;

class CheckStatusScheduler {

  private $ajVaPayment;

  function __construct() {
    $this->ajVaPayment = app()->make(PaymentArtajasaVaPaymentRepository::class);
  }

  public function run() {
    $pendingPayments = $this->ajVaPayment->getPendingPayment();
    foreach ($pendingPayments as $vaPayment) {
      dispatch(new InquiryStatus($vaPayment));
    }
  }

}