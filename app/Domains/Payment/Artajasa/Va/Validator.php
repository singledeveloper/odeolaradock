<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 21/10/19
 * Time: 17.00
 */

namespace Odeo\Domains\Payment\Artajasa\Va;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Artajasa\Helper\ArtajasaApi;
use Odeo\Domains\Payment\Helper\PaymentInvoiceHelper;

class Validator {

  public function validateInquiry(PipelineListener $listener, $data) {
    try {
      ArtajasaApi::validateRequest($data, ArtajasaApi::REQ_INQUIRY);
    } catch (\Exception $e) {
      clog('artajasa_va', $e->getMessage());
      return $listener->response(400, [
        'message' => $e->getMessage(),
        'code' => '01',
        'error_code' => PaymentInvoiceHelper::INVALID_SIGNATURE
      ]);
    }

    return $listener->response(200);
  }

  public function validatePayment(PipelineListener $listener, $data) {
    try {
      ArtajasaApi::validateRequest($data, ArtajasaApi::REQ_PAYMENT);
    } catch (\Exception $e) {
      clog('artajasa_va', $e->getMessage());
      return $listener->response(400, [
        'message' => $e->getMessage(),
        'code' => '01',
        'error_code' => PaymentInvoiceHelper::INVALID_SIGNATURE
      ]);
    }

    return $listener->response(200);
  }

}