<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/24/17
 * Time: 1:59 PM
 */

namespace Odeo\Domains\Payment\Cimb\Helper;


use Odeo\Domains\Payment\Cimb\Repository\PaymentCimbVaInquiryRepository;
use Odeo\Domains\Payment\Helper\PaymentManager;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;

class CimbManager extends PaymentManager {

  protected $cimbVaPayments, $virtualAccounts, $payments, $cimbVaInquiry, $pgUserPaymentChannel;
  public function __construct() {
    parent::__construct();
    $this->cimbVaPayments = app()->make(\Odeo\Domains\Payment\Cimb\Repository\PaymentCimbVaPaymentRepository::class);
    $this->virtualAccounts = app()->make(\Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountRepository::class);
    $this->payments = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);
    $this->cimbVaInquiry = app()->make(PaymentCimbVaInquiryRepository::class);
    $this->pgUserPaymentChannel = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
  }

}