<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/16/17
 * Time: 4:24 PM
 */

namespace Odeo\Domains\Payment\Cimb;


use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Cimb\Helper\CimbHelper;
use Odeo\Domains\Payment\Cimb\Helper\CimbManager;

class Inquirer extends CimbManager {

  public function __construct() {
    parent::__construct();
  }

  public function vaInquiry(PipelineListener $listener, $data) {

    $responseCode = CimbHelper::validateInquiryData($data);

    if($responseCode != '00') {
      return $listener->response(400, ['cimb_error_code' => $responseCode]);
    }

    $pgUserChannel = $this->pgUserPaymentChannel->findByActivePrefix($data['virtual_account_number']);
    if (!$pgUserChannel) {
      return $listener->response(400,  ['cimb_error_code' => '32']);
    }

    return $listener->response(200, [
      'va_code' => $data['virtual_account_number'],
      'va_prefix' => substr($data['virtual_account_number'], 0, 6),
      'user_id' => $pgUserChannel->user_id,
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'vendor_reference_id' => 1
    ]);
  }

  public function createInquiry(PipelineListener $listener, $data) {

    $billReff = $this->generateBillReff();
    $cimbInquiry = $this->cimbVaInquiry->getNew();
    $cimbInquiry->transaction_id = $data['TransactionID'];
    $cimbInquiry->channel_id = $data['ChannelID'] ?? '';
    $cimbInquiry->terminal_id = $data['TerminalID'] ?? '';
    $cimbInquiry->transaction_date = $data['TransactionDate'] ?? '';
    $cimbInquiry->company_code = $data['CompanyCode'] ?? '';
    $cimbInquiry->customer_key_1 = $data['CustomerKey1'];
    $cimbInquiry->customer_key_2 = $data['CustomerKey2'] ?? '';
    $cimbInquiry->customer_key_3 = $data['CustomerKey3'] ?? '';
    $cimbInquiry->additional_data_1 = $data['display_text'];
    $cimbInquiry->additional_data_2 = '';
    $cimbInquiry->additional_data_3 = '';
    $cimbInquiry->additional_data_4 = $billReff;

    $cimbInquiry->bill_currency = 'IDR';
    $cimbInquiry->bill_reference = $billReff;
    $cimbInquiry->currency = 'IDR';
    $cimbInquiry->amount = $data['amount'];
    $cimbInquiry->fee = CimbHelper::COST;
    $cimbInquiry->paid_amount = $data['amount'] + CimbHelper::COST;
    $cimbInquiry->customer_name = $data['customer_name'];
    $cimbInquiry->flag_payment = CimbHelper::FLAG_CLOSED_PAYMENT;
    $cimbInquiry->response_code = '00';
    $cimbInquiry->response_description = 'Transaction Success';

    $xmlResponse = CimbHelper::generateResponseXml([
      'TransactionID' => $cimbInquiry->transaction_id,
      'ChannelID' => $cimbInquiry->channel_id,
      'TerminalID' => $cimbInquiry->terminal_id,
      'TransactionDate' => $cimbInquiry->transaction_date,
      'CompanyCode' => $cimbInquiry->company_code,
      'CustomerKey1' => $cimbInquiry->customer_key_1,
      'CustomerKey2' => $cimbInquiry->customer_key_2,
      'CustomerKey3' => $cimbInquiry->customer_key_3,
      'BillDetailList' =>
        '<BillDetail>' . PHP_EOL .
        '<BillCode/>' . PHP_EOL .
        '<BillReference>' . $billReff . '</BillReference>' . PHP_EOL .
        '<BillAmount>' . $cimbInquiry->amount . '</BillAmount>' . PHP_EOL .
        '<BillCurrency>' . $cimbInquiry->bill_currency . '</BillCurrency>' . PHP_EOL .
        '</BillDetail>' . PHP_EOL,
      'Currency' => $cimbInquiry->currency,
      'Amount' => $cimbInquiry->amount,
      'Fee' => $cimbInquiry->fee,
      'PaidAmount' => $cimbInquiry->paid_amount,
      'CustomerName' => $cimbInquiry->customer_name,
      'AdditionalData1' => $cimbInquiry->additional_data_1,
      'AdditionalData2' => $cimbInquiry->additional_data_2,
      'AdditionalData3' => $cimbInquiry->additional_data_3,
      'AdditionalData4' => $cimbInquiry->additional_data_4,
      'FlagPayment' => $cimbInquiry->flag_payment,
      'ResponseCode' => $cimbInquiry->response_code,
      'ResponseDescription' => CimbHelper::RESPONSE_CODE_MAPPINGS[$cimbInquiry->response_code]
    ], CimbHelper::SERVICE_INQUIRY);

    $cimbInquiry->virtual_account_number = $data['va_code'];
    $this->cimbVaInquiry->save($cimbInquiry);

    return $listener->response(200, ['xml_response' => $xmlResponse]);
  }

  private function generateBillReff() {
    return time() . rand(100,999);
  }
}