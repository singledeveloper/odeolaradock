<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/24/17
 * Time: 1:57 PM
 */

namespace Odeo\Domains\Payment\Cimb\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\Payment\Model\Payment;

class PaymentCimbVaPayment extends Entity {

  public function payment() {
    return $this->belongsTo(Payment::class);
  }

}