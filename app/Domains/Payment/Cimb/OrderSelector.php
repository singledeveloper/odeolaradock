<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/25/17
 * Time: 4:28 PM
 */

namespace Odeo\Domains\Payment\Cimb;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Cimb\Helper\CimbManager;

class OrderSelector extends CimbManager {

  public function __construct() {
    parent::__construct();
  }

  public function select(PipelineListener $listener, $data) {
    if($order = $this->cimbVaPayments->findOrderByCustomerKey($data['CustomerKey1'])) {
      return $listener->response(200, ['order_id' => $order->order_id]);
    }
    return $listener->response(400, ['cimb_error_code' => '32']);
  }

}