<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/24/17
 * Time: 1:57 PM
 */

namespace Odeo\Domains\Payment\Cimb\Repository;

use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Cimb\Model\PaymentCimbVaPayment;

class PaymentCimbVaPaymentRepository extends Repository {

  function __construct(PaymentCimbVaPayment $paymentCimbVaPayment) {
    $this->setModel($paymentCimbVaPayment);
  }

  public function findOrderByCustomerKey($customerKey) {
    return $this->model->join('orders', 'orders.id', '=', 'payment_cimb_va_payments.order_id')
      ->where('customer_key_1', $customerKey)
      ->orderBy('order_id', 'desc')
      ->first();
  }

  public function findByVaCodeAndInquiryId($companyCode, $customerKey1, $inquiryId) {
    return $this->model
      ->where('company_code', $companyCode)
      ->where('customer_key_1', $customerKey1)
      ->where('inquiry_id', $inquiryId)
      ->first();
  }

  public function getPendingPayment() {
    return $this->model->whereIn('status', [
      PaymentGateway::ACCEPTED,
      PaymentGateway::PENDING,
      PaymentGateway::FAILED_PAYMENT_UPDATE,
      PaymentGateway::SUSPECT,
      PaymentGateway::SUSPECT_TIMEOUT_PAYMENT_ACCEPTANCE
    ])->get();
  }

}