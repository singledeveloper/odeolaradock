<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 22/10/19
 * Time: 13.00
 */

namespace Odeo\Domains\Payment\Cimb;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Cimb\Helper\CimbHelper;

class Validator {

  public function validateInquiry(PipelineListener $listener, $data) {

    $responseCode = CimbHelper::validateInquiryData($data);
    if ($responseCode != CimbHelper::TRX_SUCCESS) {
      return $listener->response(400, ['cimb_error_code' => $responseCode]);
    }

    return $listener->response(200);
  }

  public function validatePayment(PipelineListener $listener, $data) {

    $responseCode = CimbHelper::validatePaymentData($data);
    if ($responseCode != CimbHelper::TRX_SUCCESS) {
      return $listener->response(400, ['cimb_error_code' => $responseCode]);
    }

    return $listener->response(200);
  }

}