<?php

namespace Odeo\Domains\Payment\Dam\Mandiriecash\Helper;

use SoapClient;

class DamApi {

  public $merchant, $dam;

  public function initialize() {
    $this->setMerchantCredential();
    $this->setDam();
  }

  public function setMerchantCredential() {
    $this->merchant = [
      'login' => DamInitializer::$mid,
      'password' => DamInitializer::$tokenIpg
    ];
  }

  public function setDam() {
    $this->dam = new SoapClient(DamInitializer::$wsdlUrlToken, $this->merchant);
  }

  public function requestTransactionId($paymentData) {
    $this->dam->__setLocation(DamInitializer::$wsdlUrlTokenLocation);
    return $response = $this->dam->generate($paymentData)->return;
  }

}