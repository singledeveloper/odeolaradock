<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/2/16
 * Time: 8:58 PM
 */

namespace Odeo\Domains\Payment\Dam\Mandiriecash\Helper;


class DamInitializer {

  static $mid, $tokenIpg, $wsdlUrlToken, $wsdlUrlTokenLocation, $validation, $redirectUrl;
  const REDIRECT_URL = 'v1/payment/dam/mandiri/ecash/return-url';

  public static function init () {

    if (!isProduction()) {

      self::$mid = 'odeo';
      self::$tokenIpg = '4DE3883A34A8980C5F92449217EB7428';
      self::$wsdlUrlToken = 'https://sandbox.mandiri-ecash.com/ecommgateway/services/ecommgwws?wsdl';
      self::$wsdlUrlTokenLocation = 'https://sandbox.mandiri-ecash.com/ecommgateway/services/ecommgwws';
      self::$validation = 'https://sandbox.mandiri-ecash.com/ecommgateway/validation.html';
      self::$redirectUrl = 'https://sandbox.mandiri-ecash.com/ecommgateway/payment.html';

    } else {

      self::$mid = '49813000264';
      self::$tokenIpg = '89631C327C06988BAB45B2A07B893A3B';
      self::$wsdlUrlToken = 'https://mandiriecash.com/ecommgateway/services/ecommgwws?wsdl';
      self::$wsdlUrlTokenLocation = 'https://mandiriecash.com/ecommgateway/services/ecommgwws';
      self::$validation = 'https://mandiriecash.com/ecommgateway/validation.html';
      self::$redirectUrl = 'https://mandiriecash.com/ecommgateway/payment.html';

    }

  }

}
