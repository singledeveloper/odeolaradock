<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 11/14/16
 * Time: 8:52 PM
 */

namespace Odeo\Domains\Payment\Dam\Mandiriecash\Helper;


class DamLibrary {

  public function doHashingForm($amount, $clientAddress) {
    return sha1(strtoupper(DamInitializer::$mid) . $amount . $clientAddress);
  }

  public function generateRedirectUrlId($id) {
    return DamInitializer::$redirectUrl . '?id=' . $id;
  }

}