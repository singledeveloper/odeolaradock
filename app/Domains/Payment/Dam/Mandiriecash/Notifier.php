<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 11/19/16
 * Time: 5:50 PM
 */

namespace Odeo\Domains\Payment\Dam\Mandiriecash;


use GuzzleHttp\Client;
use Illuminate\Database\QueryException;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Dam\Mandiriecash\Helper\DamInitializer;
use Odeo\Domains\Payment\Dam\Mandiriecash\Helper\DamManager;

class Notifier extends DamManager {

  public function notified(PipelineListener $listener, $order, $data) {
    if (($result = explode(",", $data['response']))[4] != 'SUCCESS') return $listener->response(400);

    $client = new Client();
    $res = $client->get(DamInitializer::$validation . '?id=' . $result[0]);
    $confirmationData = trim($res->getBody());

    if ($data['response'] != $confirmationData) return $listener->response(400);

    $damMandiriEcash = $this->damMandiriEcash->findByEcashId($result[0]);

    $damMandiriEcash->trace_number = $result[1];
    $damMandiriEcash->phone_number = $result[2];
    $damMandiriEcash->merchant_trx_id = $result[3];
    $damMandiriEcash->status = $result[4];
    $damMandiriEcash->status_id = $result[0] . $result[4];
    $damMandiriEcash->notified_date_time = date('Y-m-d H:i:s', strtotime("now"));

    try {
      $this->damMandiriEcash->save($damMandiriEcash);
    } catch (QueryException $exception) {
      if (!str_contains($exception->getMessage(), 'duplicate')) {
        return $listener->response(200);
      }
    }

    if ($order->total > 250000) $fee = $this->feeGenerator->getFeeWithPrecision(1, $order->total);
    else{
      $fee = 2500;
    }

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');

    $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

    $order->total = $order->total + $fee;

    $this->orders->save($order);

    $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200);

  }
}
