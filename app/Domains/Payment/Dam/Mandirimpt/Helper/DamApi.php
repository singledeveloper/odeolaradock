<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/1/16
 * Time: 11:03 PM
 */

namespace Odeo\Domains\Payment\Dam\Mandirimpt\Helper;


use GuzzleHttp\Client;

class DamApi {

  public function initPayment($data) {

    $client = new Client();

    $request = $client->post(DamInitializer::$initPaymentApi, [
      'json' => $data,
      'headers' => [
        'content-type' => 'application/json',
        'MerchantClientKey' => DamInitializer::$mck,
        'Accept' => 'application/json'
      ]
    ]);

    $response = json_decode($request->getBody());

    return $response;

  }

}