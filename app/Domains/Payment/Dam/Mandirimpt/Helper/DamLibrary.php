<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/3/16
 * Time: 5:19 PM
 */

namespace Odeo\Domains\Payment\Dam\Mandirimpt\Helper;


class DamLibrary {

  public function generateValidateUrlId($ticket, $token, $id) {
    return DamInitializer::$validate . "?payment_ticket=" . $ticket . "&merchant_token=" . $token . "&merchant_id=" . $id;
  }

  public function generatePaymentCode($phoneNumber) {
    return "807" . $phoneNumber;
  }

}