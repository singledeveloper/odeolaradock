<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/2/16
 * Time: 12:30 AM
 */

namespace Odeo\Domains\Payment\Dam\Mandirimpt\Helper;


use Odeo\Domains\Payment\Dam\Mandirimpt\Repository\PaymentDamMandiriMptPaymentRepository;
use Odeo\Domains\Payment\Helper\PaymentManager;

class DamManager extends PaymentManager {

  protected $damMandiriMpt, $api, $initializer, $library, $user;

  public function __construct() {
    parent::__construct();

    $this->damMandiriMpt = app()->make(PaymentDamMandiriMptPaymentRepository::class);
    $this->api = app()->make(DamApi::class);
    $this->initializer = app()->make(DamInitializer::class);
    $this->library = app()->make(DamLibrary::class);
    $this->initializer->init();

  }

}