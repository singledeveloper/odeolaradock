<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/3/16
 * Time: 1:17 AM
 */

namespace Odeo\Domains\Payment\Dam\Mandirimpt;


use GuzzleHttp\Client;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Dam\Mandirimpt\Helper\DamInitializer;
use Odeo\Domains\Payment\Dam\Mandirimpt\Helper\DamManager;

class Notifier extends DamManager {

  public function __construct() {
    parent::__construct();
  }

  public function notified(PipelineListener $listener, $order, $payment, $data) {

    $client = new Client();


    $damMandiriMpt = $this->damMandiriMpt->findById($payment->reference_id);

    $res = $client->get($this->library->generateValidateUrlId($damMandiriMpt->payment_ticket, DamInitializer::$mpt, DamInitializer::$mid));

    $confirmationData = json_decode(trim($res->getBody()->getContents()));

    if ($confirmationData->status == '1') return $listener->response(400);

    $damMandiriMpt->trx_ref_no = $confirmationData->trx_ref_no;
    $damMandiriMpt->status_validate = $confirmationData->status;
    $damMandiriMpt->success_validate = $confirmationData->success;

    $this->damMandiriMpt->save($damMandiriMpt);

    $order->status = OrderStatus::VERIFIED;
    $order->save();

    $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200);

  }

}