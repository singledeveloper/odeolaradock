<?php

/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 11/14/16
 * Time: 8:17 PM
 */

namespace Odeo\Domains\Payment\Dam\Mandirimpt;

use Carbon\Carbon;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Dam\Mandirimpt\Helper\DamInitializer;
use Odeo\Domains\Payment\Dam\Mandirimpt\Helper\DamManager;

class Requester extends DamManager {

  // use EmailHelper;

  public function __construct() {
    parent::__construct();
    $this->user = app()->make(UserRepository::class);
  }

  public function request(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {


    if ($payment->reference_id) {
      $damMandiriMpt = $this->damMandiriMpt->findById($payment->reference_id);
    } else {
      $damMandiriMpt = $this->damMandiriMpt->getNew();
    }

    if(isset($data['phone_number'])) $phoneNumber = $data['phone_number'];
    else if (isset($order->phone_number)) $phoneNumber = $order->phone_number;
    else {
      $user = $this->user->findById($data['auth']['user_id']);
      $phoneNumber = $user->telephone;
    }

    $fee = 2000;


    $paymentData = array(
      'merchant_order_id' => $order->id,
      'payment_transfer_key' => $phoneNumber,
      'merchant_id' => DamInitializer::$mid,
      'trx_amount' => (int)$order->total,
      'fee_amount' => $fee,
      'atm_display_description' => DamInitializer::$atmDisplay,
      'time_limit_minute' => Carbon::now()->diffInMinutes(Carbon::parse($order->expired_at)),
      'expired_date' => date('Y/m/d', strtotime($order->expired_at)),
      'merchant_token' => DamInitializer::$mpt,
      'success_payment_callback' => DamInitializer::$callbackUrl,
    );

    $result = $this->api->initPayment($paymentData);

    if ($result->status == '1') {
      return $listener->response(400, 'Exception from Mandiri MPT.');
    }

    $damMandiriMpt->payment_account_number = $result->payment_account_number;
    $damMandiriMpt->payment_account_name = $result->payment_account_name;
    $damMandiriMpt->payment_amount = $result->payment_amount;
    $damMandiriMpt->payment_time_limit_info = $result->payment_time_limit_info;
    $damMandiriMpt->payment_ticket = $result->payment_ticket;
    $damMandiriMpt->status_init = $result->status;
    $damMandiriMpt->success_init = $result->success;

    $this->damMandiriMpt->save($damMandiriMpt);

    $order->status = OrderStatus::OPENED;
    $order->opened_at = date('Y-m-d H:i:s');

    $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

    $order->total = $order->total + $fee;
    $this->orders->save($order);

    $payment->reference_id = $damMandiriMpt->id;
    $payment->save();

    $data['pay_code'] = $this->library->generatePaymentCode($phoneNumber);

//    $email = $this->getEmail($order, $data);
//    $listener->addNext(new Task(CustomerEmailManager::class, 'pushJobIfEmailVerified', [
//      'email' => $email,
//      'job' => new SendPaymentInvoice($order, $paymentInformation, $data)
//    ]));

    return $listener->response(200, [
      'type' => Payment::REQUEST_PAYMENT_TYPE_REDIRECT_TO_PAYMENT_DETAIL,
      'content' => [
        'pay_code' => $data['pay_code']
      ]
    ]);

  }

}
