<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/13/17
 * Time: 2:40 PM
 */

namespace Odeo\Domains\Payment\Doku\Cimb;


use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;

class Notifier extends DokuManager {

  private $cimb;

  public function __construct() {
    parent::__construct();
    $this->cimb = app()->make(\Odeo\Domains\Payment\Doku\Cimb\Repository\PaymentDokuCimbClicksPaymentRepository::class);
  }

  public function notify(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {

    $this->initializer->switchAccount($paymentInformation->info_id);

    $dokuPayment = $this->dokuPayments->findById($payment->reference_id);

    $words = sha1($dokuPayment->amount . $dokuPayment->mall_id . $this->library->getSharedKey($dokuPayment->mall_id) . $order->id . $data['RESULTMSG'] . $data['VERIFYSTATUS']);

    if (!$dokuPayment || $dokuPayment->amount != $data['AMOUNT'] || $words != $data['WORDS']) {
      return $listener->response(400);
    }

    $cimb = $this->cimb->getNew();

    $cimb->purchase_currency = $data['PURCHASECURRENCY'];
    $cimb->payment_date_time = $data['PAYMENTDATETIME'];
    $cimb->liability = $data['LIABILITY'];
    $cimb->payment_channel = $data['PAYMENTCHANNEL'];
    $cimb->payment_code = $data['PAYMENTCODE'];
    $cimb->mcn = $data['MCN'];
    $cimb->result_msg = $data['RESULTMSG'];
    $cimb->verify_id = $data['VERIFYID'];
    $cimb->bank = $data['BANK'];
    $cimb->status_type = $data['STATUSTYPE'];
    $cimb->approval_code = $data['APPROVALCODE'];
    $cimb->edu_status = $data['EDUSTATUS'];
    $cimb->secured_3ds_status = $data['THREEDSECURESTATUS'];
    $cimb->verify_score = $data['VERIFYSCORE'];
    $cimb->response_code = $data['RESPONSECODE'];
    $cimb->verify_status = $data['VERIFYSTATUS'];
    $cimb->session_id = $data['SESSIONID'];

    $this->cimb->save($cimb);

    $dokuPayment->reference_id = $cimb->id;

    $this->dokuPayments->save($dokuPayment);


    if ($data['RESPONSECODE'] != '0000') {

      return $listener->response(400);

    }

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');

    $fee = $data['AMOUNT'] - $order->total;

    $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

    $order->total = $order->total + $fee;

    $this->orders->save($order);

    $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200);

  }

}