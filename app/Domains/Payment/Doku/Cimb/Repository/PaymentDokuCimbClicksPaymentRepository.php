<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/13/17
 * Time: 2:42 PM
 */

namespace Odeo\Domains\Payment\Doku\Cimb\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Doku\Cimb\Model\PaymentDokuCimbClicksPayment;

class PaymentDokuCimbClicksPaymentRepository extends Repository {

  public function __construct(PaymentDokuCimbClicksPayment $paymentDokuCimbClicksPayment) {
    $this->model = $paymentDokuCimbClicksPayment;
  }

}