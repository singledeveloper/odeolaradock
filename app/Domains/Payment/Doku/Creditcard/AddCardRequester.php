<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 3:24 PM
 */

namespace Odeo\Domains\Payment\Doku\Creditcard;


use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Odeo\Domains\Payment\Helper\directUrlGenerator;

class AddCardRequester {


  use directUrlGenerator;

  private $tokenCreator, $paymentDokuCcTokenizationPaymentRepository, $initializer, $library;

  public function __construct() {

    $this->tokenCreator = app()->make(\Odeo\Domains\Account\Helper\TokenCreator::class);
    $this->paymentDokuCcTokenizationPaymentRepository = app()->make(\Odeo\Domains\Payment\Doku\Creditcard\Repository\PaymentDokuCcTokenizationPaymentRepository::class);
    $this->initializer = app()->make(\Odeo\Domains\Payment\Doku\Helper\DokuInitializer::class);
    $this->library = app()->make(\Odeo\Domains\Payment\Doku\Helper\DokuLibrary::class);

  }

  public function request(PipelineListener $listener, $data) {
    if (!isset($data['idt'])) {

      $tokenCcPayment = $this->paymentDokuCcTokenizationPaymentRepository->getNew();
      $tokenCcPayment->user_id = $data['auth']['user_id'];
      $this->paymentDokuCcTokenizationPaymentRepository->save($tokenCcPayment);

      $referenceId = 'applc_' . app()->environment() . '_' . $tokenCcPayment->id;

      if ($data['auth']['platform_id'] == Platform::ANDROID) {
        $data['finish_redirect_url'] = baseUrl('v1/payment/redirect');
        $data['unfinish_redirect_url'] = baseUrl('v1/payment/redirect');
      }

      $url = $this->generateUrl([
        'opc_group' => $data['opc_group'],
        'reference_id' => $referenceId,
        'user_id' => $data['auth']['user_id'],
        'bearer' => (string)$this->tokenCreator->createToken($data['auth']['user_id'], $data['auth']['type'], $data['auth']['platform_id']),
        'finish_redirect_url' => $data['finish_redirect_url']
      ]);

      return $listener->response(400, 'Add Credit card feature is on maintenance');

      return $listener->response(200, [
        'type' => \Odeo\Domains\Constant\Payment::REQUEST_PAYMENT_TYPE_VIEW,
        'content' => [
          'link' => $url
        ]
      ]);
    }

    $this->initializer->switchAccount($data['opc_group']);

    $sessionId = $this->library->createSessionId($data['reference_id']);

    $formattedTotal = number_format(500, 2, '.', '');

    $words = $this->library->doCreateWords([
      'amount' => $formattedTotal,
      'invoice' => $data['reference_id'],
      'currency' => '360'
    ]);

    return $listener->response(200, [
        'view_page' => 'payment.doku-v1',
        'view_except' => ['order'],
        'view_data' => [
          'customer_id' => $data['user_id'],
          'orderID' => $data['reference_id'],
          'amount' => $formattedTotal,
          'MALLID' => DokuInitializer::$mallId,
          'currency' => 360,
          'words' => $words,
          'opc' => $data['opc_group'],
          'paymentChannel' => DokuInitializer::CC_CODE,
          'sessionID' => $sessionId,
          'submitURL' => baseUrl('v1/payment/direct/doku/submit'),
          'headers' => 'Bearer ' . $data['bearer'],
          'finish_redirect_url' => $data['finish_redirect_url'],
          'error_redirect_url' => $data['finish_redirect_url'],
          'unfinished_redirect_url' => $data['finish_redirect_url'],
        ]
      ]
    );

  }


}
