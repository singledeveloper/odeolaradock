<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 3:24 PM
 */

namespace Odeo\Domains\Payment\Doku\Creditcard;


use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;

class Requester extends DokuManager {

  private $dokuCcTokenizations, $tokenCreator;

  public function __construct() {
    parent::__construct();

    $this->dokuCcTokenizations = app()->make(\Odeo\Domains\Payment\Doku\Creditcard\Repository\PaymentDokuCcTokenizationRepository::class);
    $this->directPayments = app()->make(\Odeo\Domains\Payment\Repository\PaymentUserDirectPaymentRepository::class);
    $this->tokenCreator = app()->make(\Odeo\Domains\Account\Helper\TokenCreator::class);
  }

  public function request(PipelineListener $listener, $order, $paymentInformation, $data) {

    $this->initializer->switchAccount($paymentInformation->info_id);

    if (!isset($data['tokenize']) && !isset($data['idt']) && !isset($data['direct_payment_id'])) {

      $url = $this->generateUrl([
        'opc' => $paymentInformation->code,
        'order_id' => $order->id,
        'bearer' => (string)$this->tokenCreator->createToken($data['auth']['user_id'], $data['auth']['type'], $data['auth']['platform_id']),
        'finish_redirect_url' => $data['finish_redirect_url'],
        'platform_id' => $data['auth']['platform_id'],
        'unfinish_redirect_url' => isset($data['unfinish_redirect_url']) ? $data['unfinish_redirect_url'] : $data['finish_redirect_url']
      ]);

      return $listener->response(200, [
        'type' => \Odeo\Domains\Constant\Payment::REQUEST_PAYMENT_TYPE_VIEW,
        'content' => [
          'link' => $url
        ]
      ]);
    }

    $sessionId = $this->library->createSessionId($order->id);

    $fee = $this->feeGenerator->getFee(2.6, $order->total);

    if (!(isset($data['tokenize']) && $data['tokenize'])) {
      $fee += 1500;
    }

    $total = $fee + $order->total;

    $formattedTotal = number_format($total, 2, '.', '');

    $words = $this->library->doCreateWords([
      'amount' => $formattedTotal,
      'invoice' => $order->id,
      'currency' => '360'
    ]);


    $res = [];

    if (isset($data['tokenize']) && $data['tokenize']) {

      $res['customer_id'] = $order->user_id;

      $tokenization = false;
      if (isset($data['direct_payment_id'])) {
        $directPayment = $this->directPayments->findById($data['direct_payment_id']);
      } else {
        $directPayment = $this->directPayments->getSelectedDirectPayment($order->user_id, Payment::OPC_GROUP_CC_TOKENIZATION);
      }


      if (!$directPayment) {
        return $listener->response(400, 'Credit card purchase is on maintenance');
      }

      if ($directPayment->info_id == Payment::OPC_GROUP_CC_TOKENIZATION && $directPayment->user_id == $order->user_id && $directPayment->reference_id !== NULL) {
        $tokenization = $this->dokuCcTokenizations->findById($directPayment->reference_id);
      }

      if ($tokenization) {

        if ($tokenization->bank == 'BRI') {
          return $listener->response(400, 'Credit card purchase is on maintenance');
        }

       // \Log::info(\json_encode($data));
       // \Log::info(\json_encode($tokenization));
        return $this->doDirectPayment($listener, $order, [
          'formatted_total' => $formattedTotal,
          'token_payment' => $tokenization->token,
          'tokenization_id' => $tokenization->id,
          'words' => $words,
          'session_id' => $sessionId,
        ]);
      }
    }

    $res = array_merge($res, [
      'orderID' => $order->id,
      'amount' => $formattedTotal,
      'MALLID' => DokuInitializer::$mallId,
      'currency' => 360,
      'words' => $words,
      'opc' => $paymentInformation->code,
      'paymentChannel' => DokuInitializer::CC_CODE,
      'sessionID' => $sessionId,
      'submitURL' => baseUrl('v1/payment/doku/submit'),
      'headers' => 'Bearer ' . $data['bearer'],
      'finish_redirect_url' => $data['finish_redirect_url'],
      'error_redirect_url' => $data['finish_redirect_url'],
      'unfinished_redirect_url' => isset($data['unfinish_redirect_url']) ? $data['unfinish_redirect_url'] : $data['finish_redirect_url']
    ]);


    if (isset($data['platform_id']) && $data['platform_id'] == Platform::WEB_APP) {
      $res['show_return_button'] = true;
    }

    return $listener->response(200, [
        'view_page' => 'payment.doku-v1',
        'view_data' => $res
      ]
    );

  }


  public function doDirectPayment(PipelineListener $listener, $order, $additionalData) {

    $customer = [
      'name' => 'Odeo',
      'data_phone' => '08',
      'data_email' => 'doku@odeo.co.id',
      'data_address' => 'address'
    ];

    $paymentData = [
      'req_mall_id' => DokuInitializer::$mallId,
      'req_chain_merchant' => 'NA',
      'req_amount' => $additionalData['formatted_total'],
      'req_words' => $additionalData['words'],
      'req_purchase_amount' => $additionalData['formatted_total'],
      'req_trans_id_merchant' => $order->id,
      'req_request_date_time' => date('YmdHis', strtotime("now")),
      'req_currency' => '360',
      'req_purchase_currency' => '360',
      'req_session_id' => $additionalData['session_id'],
      'req_name' => $customer['name'],
      'req_payment_channel' => DokuInitializer::CC_CODE,
      'req_basket' => $this->library->formatBasket($order),
      'req_email' => $customer['data_email'],
      'req_mobile_phone' => $customer['data_phone'],
      'req_address' => $customer['data_address'],
      'req_token_payment' => $additionalData['token_payment'],
      'req_customer_id' => $order->user_id
    ];

    $result = $this->api->doDirectPayment($paymentData);

    if ($result->res_response_code != '0000') {
      //\Log::info(\json_encode($result));
      return $listener->response(400, 'Purchase Failed. Contact our Customer Services for further information.');
    }

    $payment = $this->payments->findByOrderId($order->id);

    $dokuPayment = $this->dokuPayments->getNew();

    $dokuPayment->request_date_time = date('Y-m-d H:i:s');
    $dokuPayment->session_id = $paymentData['req_session_id'];
    $dokuPayment->words = $paymentData['req_words'];
    $dokuPayment->amount = $paymentData['req_purchase_amount'];
    $dokuPayment->currency = $paymentData['req_currency'];
    $dokuPayment->mall_id = DokuInitializer::$mallId;


    $dokuCcPayment = app()->make(\Odeo\Domains\Payment\Doku\Creditcard\Repository\PaymentDokuCcPaymentRepository::class);

    $ccPayment = $dokuCcPayment->getNew();

    $ccPayment->amount = $result->res_amount;
    $ccPayment->response_code = $result->res_response_code;
    $ccPayment->response_msg = $result->res_response_msg;
    $ccPayment->approval_code = $result->res_approval_code;
    $ccPayment->bank = $result->res_bank;
    $ccPayment->mcn = $result->res_mcn;
    $ccPayment->mid = $result->res_mid;
    $ccPayment->payment_channel = $result->res_payment_channel;
    $ccPayment->payment_date_time = $result->res_payment_date_time;
    $ccPayment->session_id = $result->res_session_id;
    $ccPayment->verify_id = $result->res_verify_id;
    $ccPayment->verify_score = $result->res_verify_score;
    $ccPayment->verify_status = $result->res_verify_status;
    $ccPayment->words = $result->res_words;
    $ccPayment->tokenization_id = $additionalData['tokenization_id'];

    $dokuCcPayment->save($ccPayment);

    $dokuPayment->reference_id = $ccPayment->id;

    $this->dokuPayments->save($dokuPayment);

    $payment->reference_id = $dokuPayment->id;

    $this->payments->save($payment);

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');

    $fee = $this->feeGenerator->getFee(2.6, $order->total);
    $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);
    $this->charge($order->id, OrderCharge::DOKU_FEE, 1500, OrderCharge::GROUP_TYPE_CHARGE_TO_COMPANY);

    $order->total = $order->total + $fee;

    $this->orders->save($order);

    $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200);

//    return $listener->response(200, [
//        'view_page' => 'payment.force_redirect',
//        'view_data' => [
//          'redirect_url' => $additionalData['finish_redirect_url'],
//          'success' => true
//        ]
//      ]
//    );


  }


}
