<?php


namespace Odeo\Domains\Payment\Doku\Danamon;

use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;

class Notifier extends DokuManager {

  private $danamon;

  public function __construct() {
    parent::__construct();
    $this->danamon = app()->make(\Odeo\Domains\Payment\Doku\Danamon\Repository\PaymentDokuDanamonInternetBankingPaymentRepository::class);
  }

  public function notified(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {

    $this->initializer->switchAccount($paymentInformation->info_id);

    $dokuPayment = $this->dokuPayments->findById($payment->reference_id);

    $words = sha1($dokuPayment->amount . $dokuPayment->mall_id . $this->library->getSharedKey($dokuPayment->mall_id) . $order->id . $data['RESULTMSG'] . $data['VERIFYSTATUS']);

    if (!$dokuPayment || $dokuPayment->amount != $data['AMOUNT'] || $words != $data['WORDS']) {
      return $listener->response(400);
    }

    $danamon = $this->danamon->getNew();

    $danamon->purchase_currency = $data['PURCHASECURRENCY'];
    $danamon->payment_date_time = $data['PAYMENTDATETIME'];
    $danamon->liability = $data['LIABILITY'];
    $danamon->payment_channel = $data['PAYMENTCHANNEL'];
    $danamon->payment_code = $data['PAYMENTCODE'];
    $danamon->mcn = $data['MCN'];
    $danamon->result_msg = $data['RESULTMSG'];
    $danamon->verify_id = $data['VERIFYID'];
    $danamon->bank = $data['BANK'];
    $danamon->status_type = $data['STATUSTYPE'];
    $danamon->approval_code = $data['APPROVALCODE'];
    $danamon->edu_status = $data['EDUSTATUS'];
    $danamon->secured_3ds_status = $data['THREEDSECURESTATUS'];
    $danamon->verify_score = $data['VERIFYSCORE'];
    $danamon->response_code = $data['RESPONSECODE'];
    $danamon->verify_status = $data['VERIFYSTATUS'];
    $danamon->session_id = $data['SESSIONID'];


    $this->danamon->save($danamon);

    $dokuPayment->reference_id = $danamon->id;

    $this->dokuPayments->save($dokuPayment);


    if ($data['RESPONSECODE'] != '0000') {

      return $listener->response(400);

    }

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');

    $fee = $data['AMOUNT'] - $order->total;

    $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

    $order->total = $order->total + $fee;

    $this->orders->save($order);

   // $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200);

  }

}