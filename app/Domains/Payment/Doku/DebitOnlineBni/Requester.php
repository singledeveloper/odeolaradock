<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 3:24 PM
 */

namespace Odeo\Domains\Payment\Doku\DebitOnlineBni;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;

class Requester extends DokuManager {

  private $tokenCreator;

  public function __construct() {
    parent::__construct();
    $this->tokenCreator = app()->make(\Odeo\Domains\Account\Helper\TokenCreator::class);
  }

  public function request(PipelineListener $listener, $order, $paymentInformation, $data) {

    $this->initializer->switchAccount($paymentInformation->info_id);

    if (!isset($data['idt'])) {

      $url = $this->generateUrl([
        'opc' => $paymentInformation->code,
        'order_id' => $order->id,
        'bearer' => (string)$this->tokenCreator->createToken($data['auth']['user_id'], $data['auth']['type'], $data['auth']['platform_id']),
        'finish_redirect_url' => $data['finish_redirect_url']
      ]);

      if(isset($data['direct_payment_id'])) {
        $data['idt'] = substr($url, strpos($url, '=') + 1);
      } else {
        return $listener->response(200, [
          'type' => \Odeo\Domains\Constant\Payment::REQUEST_PAYMENT_TYPE_VIEW,
          'content' => [
            'link' => $url
          ]
        ]);
      }
    }

    $sessionId = $this->library->createSessionId($order->id);

    $total = $this->feeGenerator->getFee(2.6, $order->total) + $order->total;

    $formattedTotal = number_format($total, 2, '.', '');

    $words = $this->library->doCreateWords([
      'amount' => $formattedTotal,
      'invoice' => $order->id,
      'currency' => '360'
    ]);

    return $listener->response(200, [
        'view_page' => 'payment.doku-v1',
        'view_data' => [
          'orderID' => $order->id,
          'amount' => $formattedTotal,
          'MALLID' => DokuInitializer::$mallId,
          'currency' => 360,
          'words' => $words,
          'opc' => $paymentInformation->code,
          'paymentChannel' => DokuInitializer::DEBIT_ONLINE_BNI_CODE,
          'sessionID' => $sessionId,
          'submitURL' => baseUrl('v1/payment/doku/submit'),
          'headers' => 'Bearer ' . $data['bearer'],
          'finish_redirect_url' => $data['finish_redirect_url'],
          'error_redirect_url' => $data['finish_redirect_url'],
          'unfinished_redirect_url' => $data['finish_redirect_url'],
        ]
      ]
    );

  }

}