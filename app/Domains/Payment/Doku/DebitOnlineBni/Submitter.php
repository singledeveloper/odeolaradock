<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 5:19 PM
 */

namespace Odeo\Domains\Payment\Doku\DebitOnlineBni;


use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;

class Submitter extends DokuManager {


  private $dokuDebitOnlineBniPayment;

  public function __construct() {

    parent::__construct();

    $this->dokuDebitOnlineBniPayment = app()->make(\Odeo\Domains\Payment\Doku\DebitOnlineBni\Repository\PaymentDokuDebitOnlineBniPaymentRepository::class);
  }

  public function submit(PipelineListener $listener, $order, $paymentInformation, $data) {

    if ($order->status == OrderStatus::VERIFIED) {
      return $listener->response(200);
    }

    $this->initializer->switchAccount($paymentInformation->info_id);

    $params = [
      'amount' => $data['doku_amount'],
      'invoice' => $order->id,
      'currency' => $data['doku_currency'],
      'pairing_code' => $data['doku_pairing_code'],
      'token' => $data['doku_token']
    ];

    $words = $this->library->doCreateWords($params);


    $basket = $this->library->formatBasket($order);

    $customer = array(
      'name' => 'Odeo',
      'data_phone' => '08',
      'data_email' => 'doku@odeo.co.id',
      'data_address' => 'address'
    );


    $responsePrePayment = $this->api->doPrePayment([
      'req_token_id' => $data['doku_token'],
      'req_pairing_code' => $data['doku_pairing_code'],
      'req_customer' => $customer,
      'req_basket' => $basket,
      'req_words' => $words
    ]);

    if ($responsePrePayment->res_response_code != '0000') { //prepayment success

      return $listener->response(400);

    }

    $requestDateTimeTimeStamp = strtotime("now");

    $requestDateTime = date('YmdHis', $requestDateTimeTimeStamp);
    $sessionId = $this->library->createSessionId($order->id);
    $currency = '360';

    $paymentData = array(
      'req_mall_id' => DokuInitializer::$mallId,
      'req_chain_merchant' => 'NA',
      'req_amount' => $data['doku_amount'],
      'req_words' => $words,
      'req_words_raw' => $this->library->doCreateWordsRaw($params),
      'req_purchase_amount' => $data['doku_amount'],
      'req_trans_id_merchant' => $order->id,
      'req_request_date_time' => $requestDateTime,
      'req_currency' => $currency,
      'req_purchase_currency' => $currency,
      'req_session_id' => $sessionId,
      'req_name' => $customer['name'],
      'req_payment_channel' => DokuInitializer::DEBIT_ONLINE_BNI_CODE,
      'req_basket' => $basket,
      'req_email' => $customer['data_email'],
      'req_mobile_phone' => $customer['data_phone'],
      'req_address' => $customer['data_address'],
      'req_token_id' => $data['doku_token']
    );

    $result = $this->api->doPayment($paymentData);

    if ($result->res_response_code != '0000') {
      return $listener->response(400);
    }

    $payment = $this->payments->findByOrderId($order->id);

    $dokuPayment = $this->dokuPayments->getNew();

    $dokuPayment->request_date_time = date('Y-m-d H:i:s');
    $dokuPayment->session_id = $sessionId;
    $dokuPayment->words = $words;
    $dokuPayment->amount = $data['doku_amount'];
    $dokuPayment->currency = $currency;
    $dokuPayment->mall_id = DokuInitializer::$mallId;

    $debitOnlineBniPayment = $this->dokuDebitOnlineBniPayment->getNew();

    $debitOnlineBniPayment->amount = $result->res_amount;
    $debitOnlineBniPayment->response_code = $result->res_response_code;
    $debitOnlineBniPayment->response_msg = $result->res_response_msg;
    $debitOnlineBniPayment->approval_code = $result->res_approval_code;
    $debitOnlineBniPayment->bank = $result->res_bank;
    $debitOnlineBniPayment->mcn = $result->res_mcn;
    $debitOnlineBniPayment->mid = $result->res_mid;
    $debitOnlineBniPayment->payment_channel = $result->res_payment_channel;
    $debitOnlineBniPayment->payment_date_time = $result->res_payment_date_time;
    $debitOnlineBniPayment->session_id = $result->res_session_id;
    $debitOnlineBniPayment->verify_id = $result->res_verify_id;
    $debitOnlineBniPayment->verify_score = $result->res_verify_score;
    $debitOnlineBniPayment->verify_status = $result->res_verify_status;
    $debitOnlineBniPayment->words = $result->res_words;

    $this->dokuDebitOnlineBniPayment->save($debitOnlineBniPayment);

    $dokuPayment->reference_id = $debitOnlineBniPayment->id;

    $this->dokuPayments->save($dokuPayment);

    $payment->reference_id = $dokuPayment->id;

    $this->payments->save($payment);

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');

    $fee = $this->feeGenerator->getFee(2.6, $order->total);

    $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);
    $this->charge($order->id, OrderCharge::DOKU_FEE, 1500, OrderCharge::GROUP_TYPE_CHARGE_TO_COMPANY);

    $order->total = $order->total + $fee;

    $this->orders->save($order);

    //redirect process to doku
    $result->res_redirect_url = $data['finish_redirect_url'];
    $result->res_show_doku_page = false;

    $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200, [
      'order_id' => $order->id,
      'opc' => $paymentInformation->code,
      'additional' => $result
    ]);

  }


}