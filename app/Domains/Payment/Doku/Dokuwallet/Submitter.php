<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 5:19 PM
 */

namespace Odeo\Domains\Payment\Doku\Dokuwallet;


use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;

class Submitter extends DokuManager {


  private $dokuWalletPayment;

  public function __construct() {
    parent::__construct();
    $this->dokuWalletPayment = app()->make(\Odeo\Domains\Payment\Doku\Dokuwallet\Repository\PaymentDokuWalletPaymentRepository::class);
  }

  public function submit(PipelineListener $listener, $order, $paymentInformation, $data) {


    if ($order->status == OrderStatus::VERIFIED) {
      return $listener->response(200);
    }

    $this->initializer->switchAccount($paymentInformation->info_id);

    $params = [
      'amount' => $data['doku_amount'],
      'invoice' => $order->id,
      'currency' => $data['doku_currency'],
      'pairing_code' => $data['doku_pairing_code'],
      'token' => $data['doku_token']
    ];

    $words = $this->library->doCreateWords($params);

    $basket = $this->library->formatBasket($order);


    $customer = array(
      'name' => 'Odeo',
      'data_phone' => '08',
      'data_email' => 'doku@odeo.co.id',
      'data_address' => 'address'
    );


    $requestDateTime = date('YmdHis');
    $sessionId = $this->library->createSessionId($order->id);
    $currency = '360';

    $paymentData = array(
      'req_mall_id' => DokuInitializer::$mallId,
      'req_chain_merchant' => 'NA',
      'req_amount' => $data['doku_amount'],
      'req_words' => $words,
      'req_words_raw' => $this->library->doCreateWordsRaw($params),
      'req_purchase_amount' => $data['doku_amount'],
      'req_trans_id_merchant' => $order->id,
      'req_request_date_time' => $requestDateTime,
      'req_currency' => $currency,
      'req_purchase_currency' => $currency,
      'req_session_id' => $sessionId,
      'req_name' => $customer['name'],
      'req_payment_channel' => DokuInitializer::DOKU_WALLET_CODE,
      'req_basket' => $basket,
      'req_email' => $customer['data_email'],
      'req_mobile_phone' => $customer['data_phone'],
      'req_address' => $customer['data_address'],
      'req_token_id' => $data['doku_token']
    );


    $result = $this->api->doPayment($paymentData);


    if ($result->res_response_code != '0000') {
      return $listener->response(400, null);
    }


    $payment = $this->payments->findByOrderId($order->id);


    $dokuPayment = $this->dokuPayments->getNew();

    $dokuPayment->request_date_time = date('Y-m-d H:i:s', $requestDateTime);
    $dokuPayment->session_id = $sessionId;
    $dokuPayment->words = $words;
    $dokuPayment->amount = $data['doku_amount'];
    $dokuPayment->currency = $currency;
    $dokuPayment->mall_id = DokuInitializer::$mallId;

    $dokuWalletPayment = $this->dokuWalletPayment->getNew();


    $dokuWalletPayment->approval_code = $result->res_approval_code;
    $dokuWalletPayment->bank = $result->res_bank;
    $dokuWalletPayment->payment_channel_code = $result->res_payment_channel_code;
    $dokuWalletPayment->status = $result->res_status;
    $dokuWalletPayment->response_code = $result->res_response_code;
    $dokuWalletPayment->response_msg = $result->res_response_msg;
    $dokuWalletPayment->tracking_id = $result->res_tracking_id;
    $dokuWalletPayment->dp_mall_id = $result->res_dp_mall_id;

    $this->dokuWalletPayment->save($dokuWalletPayment);

    $dokuPayment->reference_id = $dokuWalletPayment->id;

    $this->dokuPayments->save($dokuPayment);

    $payment->reference_id = $dokuPayment->id;

    $this->payments->save($payment);

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');

    $fee = $this->feeGenerator->getFee(2, $order->total);

    $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

    $order->total = $order->total + $fee;

    $this->orders->save($order);

    //redirect process to doku
    $result->res_redirect_url = $data['finish_redirect_url'];
    $result->res_show_doku_page = false;

    $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200, [
      'order_id' => $order->id,
      'opc' => $paymentInformation->code,
      'additional' => $result
    ]);

  }


}