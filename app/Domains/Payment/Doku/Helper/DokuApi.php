<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 5:30 PM
 */

namespace Odeo\Domains\Payment\Doku\Helper;

class DokuApi {

  public function __construct() {
    $this->library = app()->make(DokuLibrary::class);
  }

  public function doPrePayment($data) {

    $ch = curl_init(DokuInitializer::$prePaymentUrl);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'data=' . json_encode($data));
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
    curl_setopt($ch, CURLOPT_TIMEOUT, 120);

    $responseJson = curl_exec($ch);

    curl_close($ch);

    return json_decode($responseJson);
  }

  public function doPayment($data) {

    $ch = curl_init(DokuInitializer::$paymentUrl);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'data=' . json_encode($data));
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
    curl_setopt($ch, CURLOPT_TIMEOUT, 120);

    $responseJson = curl_exec($ch);

    curl_close($ch);

    if (is_string($responseJson)) {
      return json_decode($responseJson);
    } else {
      return $responseJson;
    }

  }

  public function doDirectPayment($data) {

    $ch = curl_init(DokuInitializer::$directPaymentUrl);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'data=' . json_encode($data));
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
    curl_setopt($ch, CURLOPT_TIMEOUT, 120);

    $responseJson = curl_exec($ch);

    curl_close($ch);

    if (is_string($responseJson)) {
      return json_decode($responseJson);
    } else {
      return $responseJson;
    }

  }

  public function doGeneratePayCode($data) {

    $ch = curl_init(DokuInitializer::$generateCodeUrl);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'data=' . json_encode($data));
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
    curl_setopt($ch, CURLOPT_TIMEOUT, 120);
    $responseJson = curl_exec($ch);

    curl_close($ch);

    if (is_string($responseJson)) {
      return json_decode($responseJson);
    } else {
      return $responseJson;
    }

  }

}
