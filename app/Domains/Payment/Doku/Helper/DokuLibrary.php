<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 3:05 PM
 */

namespace Odeo\Domains\Payment\Doku\Helper;

class DokuLibrary {


  public function doCreateWords($data) {

    if (!empty($data['device_id'])) {

      if (!empty($data['pairing_code'])) {

        return sha1($data['amount'] . DokuInitializer::$mallId . DokuInitializer::$sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'] . $data['device_id']);

      } else {

        return sha1($data['amount'] . DokuInitializer::$mallId . DokuInitializer::$sharedKey . $data['invoice'] . $data['currency'] . $data['device_id']);

      }

    } else if (!empty($data['pairing_code'])) {

      return sha1($data['amount'] . DokuInitializer::$mallId . DokuInitializer::$sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code']);

    } else if (!empty($data['currency'])) {

      return sha1($data['amount'] . DokuInitializer::$mallId . DokuInitializer::$sharedKey . $data['invoice'] . $data['currency']);

    } else {

      return sha1($data['amount'] . DokuInitializer::$mallId . DokuInitializer::$sharedKey . $data['invoice']);


    }
  }

  public function doCreateWordsRaw($data) {

    if (!empty($data['device_id'])) {

      if (!empty($data['pairing_code'])) {

        return $data['amount'] . DokuInitializer::$mallId . DokuInitializer::$sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'] . $data['device_id'];

      } else {

        return $data['amount'] . DokuInitializer::$mallId . DokuInitializer::$sharedKey . $data['invoice'] . $data['currency'] . $data['device_id'];

      }

    } else if (!empty($data['pairing_code'])) {

      return $data['amount'] . DokuInitializer::$mallId . DokuInitializer::$sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'];

    } else if (!empty($data['currency'])) {

      return $data['amount'] . DokuInitializer::$mallId . DokuInitializer::$sharedKey . $data['invoice'] . $data['currency'];

    } else {

      return $data['amount'] . DokuInitializer::$mallId . DokuInitializer::$sharedKey . $data['invoice'];

    }
  }

  public function formatBasket($order) {

    $baskets = [];

    $orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);

    $items = $orderDetailizer->detailizeItem($order);

    foreach ($items as $item) {

      array_push($baskets, [
        'name' => $item['name'],
        'amount' => number_format($item['price']['amount'], 2, '.', ''),
        'quantity' => $item['quantity'],
        'subtotal' => number_format($item['price']['amount'] * $item['quantity'], 2, '.', '')
      ]);

    };

    $parseBasket = '';
    if (is_array($baskets)) {
      foreach ($baskets as $basket) {
        $parseBasket = $parseBasket . $basket['name'] . ',' . $basket['amount'] . ',' . $basket['quantity'] . ',' . $basket['subtotal'] . ';';
      }
    } else if (is_object($baskets)) {
      foreach ($baskets as $basket) {
        $parseBasket = $parseBasket . $basket->name . ',' . $basket->amount . ',' . $basket->quantity . ',' . $basket->subtotal . ';';
      }
    }

    return $parseBasket;
  }

  public function createSessionId($orderId) {
    return sha1($orderId . microtime());
  }

  public static function getSharedKey($mallID) {
    switch ($mallID) {
      case '2983':
        return 'nbb3Ec1B6S8Y';
        break;
      case '1332':
        return 'nbb3Ec1B6S8Y';
        break;
      case '2476':
        return 'nbb3Ec1B6S8Y';
        break;
      case '1333':
        return 'Z67Y3uPghGa6';
        break;
      case '1372';
        return '80VrMkzX61Ng';
        break;
      case '1390':
        return 'JVrCZkXH0urp';
        break;
      default:
        return null;
    }
  }
}
