<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 3:04 PM
 */

namespace Odeo\Domains\Payment\Doku\Helper;

use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Header;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Payment\Doku\Repository\PaymentDokuPaymentRepository;
use Odeo\Domains\Payment\Helper\PaymentManager;
use Odeo\Exceptions\FailException;

class DokuManager extends PaymentManager {

  protected $library, $initializer, $api, $dokuPayments;

  public function __construct() {
    parent::__construct();

    $this->library = app()->make(DokuLibrary::class);
    $this->initializer = app()->make(DokuInitializer::class);
    $this->api = app()->make(DokuApi::class);
    $this->dokuPayments = app()->make(PaymentDokuPaymentRepository::class);

    $this->initializer->init();

  }

  private function getOpcGroup($paymentChannel) {
    switch ($paymentChannel) {
      case 34:
        return Payment::OPC_GROUP_VA_BRI;
      case 35:
        return Payment::OPC_GROUP_ALFA;
        break;
      default:
        return null;
        break;
    }
  }

  protected function validateWords($data) {
    $opcGroup = $this->getOpcGroup($data['PAYMENTCHANNEL']);
    if (!$opcGroup) {
      throw new FailException(trans('doku.unsupported_payment_channel'));
    }

    $this->initializer->switchAccount($opcGroup, ['is_payment_gateway' => true]);

    if (
      isset($data['AMOUNT']) &&
      isset($data['TRANSIDMERCHANT']) &&
      isset($data['RESULTMSG']) &&
      isset($data['VERIFYSTATUS'])
    ) {
      $wordsGenerated = sha1(
        $data['AMOUNT'] .
        DokuInitializer::$mallId .
        DokuInitializer::$sharedKey .
        $data['TRANSIDMERCHANT'] .
        $data['RESULTMSG'] .
        $data['VERIFYSTATUS']
      );
    } else {
      $wordsGenerated = sha1(
        DokuInitializer::$mallId .
        DokuInitializer::$sharedKey .
        $data['PAYMENTCODE']
      );
    }
    if ($wordsGenerated !== $data['WORDS']) {
      throw new FailException(trans('doku.invalid_words'));
    }

  }

}