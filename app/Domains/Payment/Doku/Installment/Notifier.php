<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 10:14 PM
 */

namespace Odeo\Domains\Payment\Doku\Installment;


use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;
use Odeo\Domains\Payment\Doku\Installment\Repository\PaymentDokuCcInstallmentPaymentRepository;

class Notifier extends DokuManager {


  private $ccInstallments;

  public function __construct() {
    parent::__construct();
    $this->ccInstallments = app()->make(PaymentDokuCcInstallmentPaymentRepository::class);
  }

  public function notified(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {

    if ($data['RESPONSECODE'] != '0000') {
      return $listener->response(400);
    }

    $this->initializer->switchAccount($paymentInformation->info_id);

    $dokuPayment = $this->dokuPayments->findById($payment->reference_id);

    $words = sha1($dokuPayment->amount . $dokuPayment->mall_id . $this->library->getSharedKey($dokuPayment->mall_id) . $order->id . $data['RESULTMSG'] . $data['VERIFYSTATUS']);

    if (!$dokuPayment || $dokuPayment->amount != $data['AMOUNT'] || $words != $data['WORDS']) {

      return $listener->response(400);

    }



    $installment = $this->ccInstallments->getNew();

    $installment->purchase_currency = $data['PURCHASECURRENCY'];
    $installment->payment_date_time = $data['PAYMENTDATETIME'];
    $installment->liability = $data['LIABILITY'];
    $installment->payment_channel = $data['PAYMENTCHANNEL'];
    $installment->amount = $data['AMOUNT'];
    $installment->payment_code = $data['PAYMENTCODE'];
    $installment->mcn = $data['MCN'];
    $installment->result_msg = $data['RESULTMSG'];
    $installment->verify_id = $data['VERIFYID'];
    $installment->bank = $data['BANK'];
    $installment->status_type = $data['STATUSTYPE'];
    $installment->approval_code = $data['APPROVALCODE'];
    $installment->edu_status = $data['EDUSTATUS'];
    $installment->secured_3ds_status = $data['THREEDSECURESTATUS'];
    $installment->verify_score = $data['VERIFYSCORE'];
    $installment->response_code = $data['RESPONSECODE'];
    $installment->ch_name = $data['CHNAME'];
    $installment->brand = $data['BRAND'];
    $installment->verify_status = $data['VERIFYSTATUS'];
    $installment->session_id = $data['SESSIONID'];

    $this->ccInstallments->save($installment);

    $dokuPayment->reference_id = $installment->id;

    $this->dokuPayments->save($dokuPayment);

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');

    $fee = $data['AMOUNT'] - $order->total;

    $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);
    if ($data['BANK'] == 'Bank Mandiri') {
      $this->charge($order->id, OrderCharge::DOKU_FEE, 1500, OrderCharge::GROUP_TYPE_CHARGE_TO_COMPANY);
    }

    $order->total = $order->total + $fee;

    $this->orders->save($order);

    $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200);


  }


}
