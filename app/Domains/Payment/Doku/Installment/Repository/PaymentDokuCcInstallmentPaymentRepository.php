<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 10:21 PM
 */

namespace Odeo\Domains\Payment\Doku\Installment\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Doku\Installment\Model\PaymentDokuCcInstallmentPayment;

class PaymentDokuCcInstallmentPaymentRepository extends Repository {


  public function __construct(PaymentDokuCcInstallmentPayment $ccInstallmentPayment) {
    $this->model = $ccInstallmentPayment;
  }

}