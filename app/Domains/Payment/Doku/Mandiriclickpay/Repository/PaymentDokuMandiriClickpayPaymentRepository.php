<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 11/29/16
 * Time: 10:48 PM
 */

namespace Odeo\Domains\Payment\Doku\Mandiriclickpay\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Doku\Mandiriclickpay\Model\PaymentDokuMandiriClickpayPayment;

class PaymentDokuMandiriClickpayPaymentRepository extends Repository {

  public function __construct(PaymentDokuMandiriClickpayPayment $paymentDokuMandiriClickpayPayment) {
    $this->model = $paymentDokuMandiriClickpayPayment;
  }

  public function gets() {

  }
}