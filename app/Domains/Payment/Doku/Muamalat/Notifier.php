<?php


namespace Odeo\Domains\Payment\Doku\Muamalat;

use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;

class Notifier extends DokuManager {

  private $muamalat;

  public function __construct() {
    parent::__construct();
    $this->muamalat = app()->make(\Odeo\Domains\Payment\Doku\Muamalat\Repository\PaymentDokuMuamalatInternetBankingPaymentRepository::class);
  }

  public function notified(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {

    $this->initializer->switchAccount($paymentInformation->info_id);

    $dokuPayment = $this->dokuPayments->findById($payment->reference_id);

    $words = sha1($dokuPayment->amount . $dokuPayment->mall_id . $this->library->getSharedKey($dokuPayment->mall_id) . $order->id . $data['RESULTMSG'] . $data['VERIFYSTATUS']);

    if (!$dokuPayment || $dokuPayment->amount != $data['AMOUNT'] || $words != $data['WORDS']) {
      return $listener->response(400);
    }

    $muamalat = $this->muamalat->getNew();

    $muamalat->purchase_currency = $data['PURCHASECURRENCY'];
    $muamalat->payment_date_time = $data['PAYMENTDATETIME'];
    $muamalat->liability = $data['LIABILITY'];
    $muamalat->payment_channel = $data['PAYMENTCHANNEL'];
    $muamalat->payment_code = $data['PAYMENTCODE'];
    $muamalat->mcn = $data['MCN'];
    $muamalat->result_msg = $data['RESULTMSG'];
    $muamalat->verify_id = $data['VERIFYID'];
    $muamalat->bank = $data['BANK'];
    $muamalat->status_type = $data['STATUSTYPE'];
    $muamalat->approval_code = $data['APPROVALCODE'];
    $muamalat->edu_status = $data['EDUSTATUS'];
    $muamalat->secured_3ds_status = $data['THREEDSECURESTATUS'];
    $muamalat->verify_score = $data['VERIFYSCORE'];
    $muamalat->response_code = $data['RESPONSECODE'];
    $muamalat->verify_status = $data['VERIFYSTATUS'];
    $muamalat->session_id = $data['SESSIONID'];


    $this->muamalat->save($muamalat);

    $dokuPayment->reference_id = $muamalat->id;

    $this->dokuPayments->save($dokuPayment);


    if ($data['RESPONSECODE'] != '0000') {

      return $listener->response(400);

    }

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');

    $fee = $data['AMOUNT'] - $order->total;

    $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

    $order->total = $order->total + $fee;

    $this->orders->save($order);

    $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200);

  }

}