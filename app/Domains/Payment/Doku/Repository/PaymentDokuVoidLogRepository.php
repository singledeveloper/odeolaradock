<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/5/17
 * Time: 7:52 PM
 */

namespace Odeo\Domains\Payment\Doku\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Doku\Model\PaymentDokuVoidLog;

class PaymentDokuVoidLogRepository extends Repository {

  public function __construct(PaymentDokuVoidLog $paymentDokuVoidLog) {
    $this->model = $paymentDokuVoidLog;
  }

}