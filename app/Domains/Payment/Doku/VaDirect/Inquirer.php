<?php

namespace Odeo\Domains\Payment\Doku\VaDirect;


use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Carbon\Carbon;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;

class Inquirer extends DokuManager {

  private $dokuVaDirectPayments, $pgUserPaymentChannel, $odeoPaymentChannel;

  public function __construct() {
    parent::__construct();
    $this->dokuVaDirectPayments = app()->make(\Odeo\Domains\Payment\Doku\VaDirect\Repository\PaymentDokuVaDirectPaymentRepository::class);
    $this->dokuVaInquiry = app()->make(\Odeo\Domains\Payment\Doku\VaDirect\Repository\PaymentDokuVaInquiryRepository::class);
    $this->pgUserPaymentChannel = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
    $this->odeoPaymentChannel = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);
  }

  public function inquiry(PipelineListener $listener, $data) {
    switch ($data['PAYMENTCHANNEL']) {
      case 29:
        $opcGroup = Payment::OPC_GROUP_VA_BCA;
        break;
      case 41:
        $opcGroup = Payment::OPC_GROUP_VA_MANDIRI;
        break;
      default:
        $opcGroup = 0;
        break;
    }
    $this->initializer->switchAccount($opcGroup);

    $mallId = DokuInitializer::$mallId;

    $wordsGenerated = sha1($mallId .
      $this->library->getSharedKey($mallId) .
      $data['PAYMENTCODE']);

    if ($data['WORDS'] == $wordsGenerated) {
      $amount = sprintf('%.2f', $data['total']);
      $orderId = $data['order_id'];
      $serviceName = ucwords(str_replace('_', ' ', strtolower(Service::getConstKeyByValue(ServiceDetail::getServiceId($data['service_detail_id'])))));

      $result = [
        'TRANSIDMERCHANT' => $orderId,
        'PURCHASEAMOUNT' => $amount,
        'WORDS' => $this->library->doCreateWords([
          'amount' => $amount,
          'invoice' => $orderId
        ]),
        'PAYMENTCODE' => $data['virtual_account_number'],
        'REQUESTDATETIME' => Carbon::now()->format('YmdHis'),
        'PURCHASECURRENCY' => 360,
        'SESSIONID' => $this->library->createSessionId($orderId),
        'NAME' => $data['customer_name'],
        'EMAIL' => 'doku@odeo.co.id',
        'ADDITIONALDATA' => $data['name'],
        'BASKET' => $serviceName . ',' . $amount . ',1,' . $amount,
        'AMOUNT' => $amount,
        'CURRENCY' => 360
      ];

      $dokuVaDirectPayment = $this->dokuVaDirectPayments->findByAttributes('order_id', $orderId);

      if (!$dokuVaDirectPayment) {
        $dokuVaDirectPayment = $this->dokuVaDirectPayments->getNew();
        $dokuVaDirectPayment->mall_id = $data['MALLID'];
        $dokuVaDirectPayment->chain_merchant = $data['CHAINMERCHANT'];
        $dokuVaDirectPayment->payment_channel = $data['PAYMENTCHANNEL'];
        $dokuVaDirectPayment->payment_code = $data['PAYMENTCODE'];
        $dokuVaDirectPayment->inquiry_words = $data['WORDS'];
        $dokuVaDirectPayment->amount = $amount;
        $dokuVaDirectPayment->order_id = $orderId;
        $dokuVaDirectPayment->inquiry_response_words = $result['WORDS'];
        $dokuVaDirectPayment->session_id = $result['SESSIONID'];
        $dokuVaDirectPayment->additional_data = $result['ADDITIONALDATA'];
      } else {
        $dokuVaDirectPayment->session_id = $result['SESSIONID'];
      }

      $this->dokuVaDirectPayments->save($dokuVaDirectPayment);

      $payment = $this->payments->findByOrderId($orderId);
      $payment->reference_id = $dokuVaDirectPayment->id;

      $this->payments->save($payment);

      $response = '<?xml version="1.0"?>' . PHP_EOL;
      $response .= '<INQUIRY_RESPONSE>' . PHP_EOL;

      foreach ($result as $key => $value) {
        $response .= '<' . $key . '>' . $value . '</' . $key . '>' . PHP_EOL;
      }
      $response .= '</INQUIRY_RESPONSE>';

      return $listener->response(200, ['xml_response' => $response]);
    } else {
      clog('doku_va', json_encode($data));
      clog('doku_va', $wordsGenerated);
      return $listener->response(400, ['error_code' => 9999]);
    }
  }

  public function vaInquiry(PipelineListener $listener, $data) {
    try {
      $this->validateWords($data);
    } catch (FailException $e) {
      clog('doku_va', $e->getMessage());
      return $listener->response(400, $e->getMessage());
    }

    $pgUserChannel = $this->pgUserPaymentChannel->findByActivePrefix($data['PAYMENTCODE']);
    if (!$pgUserChannel) {
      clog('doku_va', trans('payment.unrecognized_va_number'));
      return $listener->response(400, trans('payment.unrecognized_va_number'));
    }

    if (PaymentGateway::exceedVaCodeLength($data['PAYMENTCODE'], $pgUserChannel->max_length)) {
      return $listener->response(400, trans('payment.va_max_length_exceeded'));
    }

    $channel = $this->odeoPaymentChannel->findByGatewayIdAndGroupId(Payment::PAYMENT_GATEWAY, $pgUserChannel->payment_group_id);
    if (!$channel) {
      clog('doku_va', 'channel not found');
      return $listener->response(400, 'channel not found');
    }

    $sessionId = $this->library->createSessionId($data['PAYMENTCODE']);

    return $listener->response(200, [
      'va_code' => $data['PAYMENTCODE'],
      'va_prefix' => $pgUserChannel->prefix,
      'user_id' => $pgUserChannel->user_id,
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'request_date_time' => Carbon::now()->format('YmdHis'),
      'session_id' => $sessionId,
      'vendor_reference_id' => $sessionId
    ]);
  }

  public function createInquiry(PipelineListener $listener, $data) {
    $inquiry = $this->dokuVaInquiry->getNew();
    $inquiry->va_code = $data['va_code'];
    $inquiry->amount = $data['amount'];
    $inquiry->display_text = $data['display_text'];
    $inquiry->customer_name = $data['customer_name'];
    $inquiry->session_id = $data['session_id'];
    $inquiry->chain_merchant = $data['CHAINMERCHANT'];
    $inquiry->payment_channel = $data['PAYMENTCHANNEL'];
    $this->dokuVaInquiry->save($inquiry);

    return $listener->response(200, [
      'trans_id_merchant' => $inquiry->id,
      'basket' => $data['item_name'] . ',' . $data['amount'] . ',1,' . $data['amount'],
      'words' => $this->library->doCreateWords([
        'amount' => $data['amount'],
        'invoice' => $inquiry->id
      ])
    ]);
  }
}
