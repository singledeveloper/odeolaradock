<?php

namespace Odeo\Domains\Payment\Doku\VaDirect\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendAutoRenotifyStatus extends Job  {
  private $id, $status;

  public function __construct($id, $status) {
    parent::__construct();
    $this->id = $id;
    $this->status = $status;
  }

  public function handle() {

    Mail::send('emails.auto_renotify_doku_payment', [
      'id' => $this->id,
      'status' => $this->status
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('pg@odeo.co.id')->subject('Doku Auto Renotify Status');
    });
  }

}
