<?php
namespace Odeo\Domains\Payment\Doku\VaDirect\Model;

use Odeo\Domains\Core\Entity;

class PaymentDokuVaDirectPayment extends Entity {

  protected $dates = ['created_at', 'updated_at'];

  public $timestamps = true;
  
}
