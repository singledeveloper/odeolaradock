<?php

namespace Odeo\Domains\Payment\Doku\VaDirect;

use Carbon\Carbon;
use Odeo\Domains\Constant\DokuPaymentStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;
use Odeo\Exceptions\FailException;
use Illuminate\Support\Facades\Redis;

class Payer extends DokuManager {

  private $payment, $paymentChannel, $dokuVaPayment, $dokuVaInquiry, $pgUserPaymentChannel, $redis;

  public function __construct() {
    parent::__construct();
    $this->payment = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);
    $this->paymentChannel = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);
    $this->dokuVaPayment = app()->make(\Odeo\Domains\Payment\Doku\Vagroup\Repository\PaymentDokuVaPaymentRepository::class);
    $this->dokuVaInquiry = app()->make(\Odeo\Domains\Payment\Doku\VaDirect\Repository\PaymentDokuVaInquiryRepository::class);
    $this->pgUserPaymentChannel = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
    $this->redis = Redis::connection();
  }

  public function vaPayment(PipelineListener $listener, $data) {
    try {
      $this->validateWords($data);
    } catch (FailException $e) {
      return $listener->response(400, $e->getMessage());
    }

    $inquiry = $this->dokuVaInquiry->findByVaCodeAndSessionId($data['PAYMENTCODE'], $data['SESSIONID']);
    if (!$inquiry) {
      return $listener->response(400, 'inquiry not found');
    }

    $currentDate = Carbon::now()->format('Y-m-d');
    $namespace = $this->getRedisNamespace($currentDate);
    if (!$this->redis->hsetnx($namespace, $inquiry->id, 1)) {
      return $listener->response(400, 'duplicate transaction');
    }
    $this->redis->expire($namespace, 2 * 24 * 60 * 60);

    $tomorrowDate = Carbon::tomorrow()->format('Y-m-d');
    $namespace = $this->getRedisNamespace($tomorrowDate);
    if (!$this->redis->hset($namespace, $inquiry->id, 1)) {
      return $listener->response(400, 'duplicate transaction');
    }
    $this->redis->expire($namespace, 2 * 24 * 60 * 60);

    $pgUserChannel = $this->pgUserPaymentChannel->findByActivePrefix($data['PAYMENTCODE']);
    if (!$pgUserChannel) {
      return $listener->response(400, 'payment.unrecognized_va_number');
    }
    
    $channel = $this->paymentChannel->findByGatewayIdAndGroupId(Payment::PAYMENT_GATEWAY, $pgUserChannel->payment_group_id);
    if (!$channel) {
      return $listener->response(400, 'channel not found');
    }

    $vaPayment = $this->dokuVaPayment->getNew();
    $vaPayment->inquiry_id = $inquiry->id;
    $vaPayment->pay_code = $inquiry->va_code;
    $vaPayment->payment_code = $inquiry->va_code;
    $vaPayment->payment_date_time = $data['PAYMENTDATETIME'];
    $vaPayment->verify_id = $data['VERIFYID'];
    $vaPayment->bank = $data['BANK'];
    $vaPayment->status_type = $data['STATUSTYPE'];
    $vaPayment->approval_code = $data['APPROVALCODE'];
    $vaPayment->edu_status = $data['EDUSTATUS'];
    $vaPayment->secured_3ds_status = $data['THREEDSECURESTATUS'];
    $vaPayment->verify_score = $data['VERIFYSCORE'];
    $vaPayment->amount = $data['AMOUNT'];
    $vaPayment->currency = $data['CURRENCY'];
    $vaPayment->response_code = $data['RESPONSECODE'];
    $vaPayment->verify_status = $data['VERIFYSTATUS'];
    $vaPayment->session_id = $data['SESSIONID'];
    $vaPayment->cost = $pgUserChannel->cost;
    $vaPayment->status = DokuPaymentStatus::PENDING;
    $this->dokuVaPayment->save($vaPayment);

    $inquiry->has_payment = true;
    $this->dokuVaInquiry->save($inquiry);

    $payment = $this->payment->getNew();
    $payment->source_type = PaymentChannel::PAYMENT_GATEWAY;
		$payment->info_id = $pgUserChannel->payment_group_id;
		$payment->opc = $channel->code;
		$payment->vendor_id = $vaPayment->id;
    $payment->vendor_type = 'va_doku';
    $this->payment->save($payment);

    $vaPayment->payment_id = $payment->id;
    $this->dokuVaPayment->save($vaPayment);

    return $listener->response(200, [
      'doku_payment_id' => $vaPayment->id,
      'payment_id' => $payment->id,
      'va_code' => $inquiry->va_code,
      'va_prefix' => $pgUserChannel->prefix,
      'user_id' => $pgUserChannel->user_id,
      'billed_amount' => $data['AMOUNT'],
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'customer_name' => $inquiry->customer_name,
      'vendor_reference_id' => $data['SESSIONID']
    ]);
  }

  public function getRedisNamespace($date) {
    return "odeo_core:doku_used_inquiry_id_$date";
  }

  public function updatePayment(PipelineListener $listener, $data) {
    $vaPayment = $this->dokuVaPayment->findById($data['doku_payment_id']);
    $vaPayment->status = $data['pg_payment_status'];
    $this->dokuVaPayment->save($vaPayment);

    if ($data['pg_payment_status'] != PaymentGateway::ACCEPTED) {
      return $listener->response(400, 'status is not accepted');
    }

    return $listener->response(200, []);
  }
  
}
