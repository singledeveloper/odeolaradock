<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 02/09/19
 * Time: 09.08
 */

namespace Odeo\Domains\Payment\Doku\VaDirect;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Doku\Vagroup\Repository\PaymentDokuVaPaymentRepository;

class PaymentSelector {

  function __construct() {
    $this->dokuVaPaymentRepo = app()->make(PaymentDokuVaPaymentRepository::class);
  }

  private function transforms($item) {
    return $item;
  }

  public function getAll(PipelineListener $listener, $data) {
    $this->dokuVaPaymentRepo->normalizeFilters($data);
    $this->dokuVaPaymentRepo->setSimplePaginate(true);

    $result = [];
    $payments = $this->dokuVaPaymentRepo->get();
    foreach ($payments as $payment) {
      $result[] = $this->transforms($payment);
    }

    if (count($result) > 0) {
      return $listener->response(200, array_merge(
        ['payments' => $result],
        $this->dokuVaPaymentRepo->getPagination()
      ));
    }

    return $listener->response(200, ['payments' => []]);
  }

}