<?php
namespace Odeo\Domains\Payment\Doku\VaDirect\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Doku\VaDirect\Model\PaymentDokuCallbackLog;

class PaymentDokuCallbackLogRepository extends Repository {

  public function __construct(PaymentDokuCallbackLog $dokuCallbackLog) {
    $this->model = $dokuCallbackLog;
  }

}
