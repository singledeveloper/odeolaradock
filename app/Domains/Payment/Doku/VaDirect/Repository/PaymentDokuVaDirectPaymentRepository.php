<?php
namespace Odeo\Domains\Payment\Doku\VaDirect\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Doku\VaDirect\Model\PaymentDokuVaDirectPayment;

class PaymentDokuVaDirectPaymentRepository extends Repository {

  public function __construct(PaymentDokuVaDirectPayment $dokuVaDirectPayment) {
    $this->model = $dokuVaDirectPayment;
  }

  public function gets() {

    $payment = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);

    $query = $payment->getCloneModel();

    if (isset($filters['search'])) {
      if (isset($filters['search']['order_id'])) {
        $id = (int)$filters['search']['order_id'];
        $query = $query->where('order_id', $id);
      }
      if (isset($filters['search']['payment_id'])) {
        $query = $query->where('payment_id', $filters['search']['user_id']);
      }

    }

    $query = $query->with('channel')->whereHas('channel', function ($query) {
      $query->where('info_id', '=', \Odeo\Domains\Constant\Payment::OPC_GROUP_ALFA);
    })->join('payment_doku_payments', 'payments.reference_id', '=', 'payment_doku_payments.id')
      ->join('payment_doku_va_payments', 'payment_doku_payments.reference_id', '=', 'payment_doku_va_payments.id');

    return $this->getResult($query);
  }

  public function getUnsettled($prefix = null) {
    $query = $this->getCloneModel();

    if (isset($prefix)) {
      $query = $query->whereRaw("substr(payment_code, 1, 5) = ?", [$prefix]);
    }
    
    return $query
      ->where('is_reconciled', false)
      ->whereNotNull('payment_date_time')
      ->orderBy('payment_date_time')
      ->get();
  }

}
