<?php
namespace Odeo\Domains\Payment\Doku\VaDirect\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Doku\VaDirect\Model\PaymentDokuVaInquiry;

class PaymentDokuVaInquiryRepository extends Repository {

  public function __construct(PaymentDokuVaInquiry $dokuVaInquiry) {
    $this->model = $dokuVaInquiry;
  }

  public function get() {
    $filter = $this->getFilters();

    $q = $this->model;

    if (isset($filter['search'])) {
      $search = $filter['search'];

      if (isset($search['id'])) {
        $q = $q->where('id', $search['id']);
      }
      if (isset($search['va_code'])) {
        $q = $q->where('va_code', $search['va_code']);
      }
      if (isset($search['customer_name'])) {
        $q = $q->where('customer_name', 'ilike', "%{$search['customer_name']}%");
      }
      if (isset($search['display_text'])) {
        $q = $q->where('display_text', 'ilike', "%{$search['display_text']}%");
      }
      if (isset($search['session_id'])) {
        $q = $q->where('session_id', $search['session_id']);
      }
    }

    return $this->getResult($q->orderBy('id', 'desc'));
  }

  public function findByVaCodeAndSessionId($vaCode, $sessionId) {
    return $this->model
      ->where('va_code', $vaCode)
      ->where('session_id', $sessionId)
      ->first();
  }

  public function getNoPayment() {
    return $this->model
      ->where('has_payment', false)
      ->whereNull('doku_status')
      ->where('created_at', '<', \Carbon\Carbon::now()->subHour())
      ->orderBy('id')
      ->get();
  }

}
