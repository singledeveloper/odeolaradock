<?php

namespace Odeo\Domains\Payment\Doku\VaDirect\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Activity\Helper\HolidayHelper;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\BankTransferInquiries;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Payment\Doku\Vagroup\Model\PaymentDokuVaPayment;
use Odeo\Domains\Payment\Doku\Vagroup\Repository\PaymentDokuVaPaymentRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository;
use Odeo\Domains\Payment\Repository\PaymentSettlementRepository;
use Odeo\Domains\Payment\Repository\PaymentSettlementDetailRepository;
use Odeo\Domains\PaymentGateway\Jobs\SendPaymentGatewayReconMismatchAlert;
use Odeo\Domains\PaymentGateway\Model\PaymentGatewayInvoicePayment;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayInvoicePaymentRepository;

class DokuVaReconScheduler {

  private $bcaRepo, $vaPayment;

  private function init() {
    $this->bcaRepo = app()->make(BankBcaInquiryRepository::class);
    $this->vaPayment = app()->make(PaymentDokuVaPaymentRepository::class);
    $this->paymentSettlement = app()->make(PaymentSettlementRepository::class);
    $this->paymentSettlementDetail = app()->make(PaymentSettlementDetailRepository::class);
    $this->paymentGatewayPayment = app()->make(PaymentGatewayPaymentRepository::class);
    $this->invoicePayment = app()->make(PaymentGatewayInvoicePaymentRepository::class);
    $this->holidayHelper = app()->make(HolidayHelper::class);
  }

  public function run() {
    $this->init();
    
    $this->reconSettlement(true);
    // $this->reconSettlement(false);
  }

  private function reconSettlement($isPaymentGateway) {
    $settlements = $this->bcaRepo->getUnreconciledDokuSettlement();
    if ($isPaymentGateway) {
      $invoicePayments = $this->invoicePayment->getUnsettled('doku_alfamart')->keyBy(function($item) {
        return $item['paid_at']->format('YmdHis') . ':' . $item['id'];
      });
      $pgPayments = $this->vaPayment->getUnsettled($isPaymentGateway)->keyBy(function($item) {
        return $item['payment_date_time'] . ':' . $item['id'];
      });
      $payments = $pgPayments->union($invoicePayments)
        ->sortBy(function ($p, $key) {
          return $key;
        });
    } else {
      $orders = $this->orderRecon->getUnsettledDokuOrders();
    }

    $updatedAt = Carbon::now();
    
    $paymentSettlementDetails = [];
    $paymentUpdate = [];
    $paymentGatewayPaymentUpdate = [];
    $invoicePaymentUpdate = [];
    $settlementUpdates = [];

    $totalPayment = [];
    $groupedPayment = [];
    
    foreach ($payments as $key => $payment) {
      $date = Carbon::createFromFormat('YmdHis', explode(':', $key)[0])->format('Y-m-d');
      $channel = '';

      if ($payment instanceof PaymentDokuVaPayment) {
        $channel = $payment->bank;
      } else if ($payment instanceof PaymentGatewayInvoicePayment) {
        switch ($payment->vendor) {
          case 'doku_alfamart':
          default:
            $channel = 'ALFA';
            break;
        }
      }

      if (!array_key_exists($channel, $totalPayment)) {
        $totalPayment[$channel] = [];
        $groupedPayment[$channel] = [];
      }
      if (!array_key_exists($date, $totalPayment[$channel])) {
        $totalPayment[$channel][$date] = 0;
        $groupedPayment[$channel][$date] = [];
      }
      $totalPayment[$channel][$date] += $payment->amount - $payment->cost;
      $groupedPayment[$channel][$date][] = $payment;
    }

    $lastReconDate = '';
    $lastFailedReconDate = '';

    foreach ($settlements as $settlement) {  
      clog('doku_recon', 'processing settlement ' . $settlement->date . ' ' . $settlement->credit);
      
      if ($settlement->date == $lastReconDate) {
        clog('doku_recon', 'previous recon for date ' . $settlement->date . ' successful');
        continue;
      }

      if ($settlement->date != $lastFailedReconDate) {
        if ($lastReconDate != $settlement->date && !empty($lastFailedReconDate) && $settledAmount > 0) {
          dispatch(new SendPaymentGatewayReconMismatchAlert([
            'date' => $lastFailedReconDate,
            'vendor' => 'Doku VA',
            'description' => '',
            'settlement_amount' => $settledAmount,
            'total_amount' => $totalAmount
          ]));
        }

        $settledAmount = 0;
        $tempPaymentDate = [];
  
        foreach ($this->getChannels($isPaymentGateway) as $channel) {
          if (!isset($tempPaymentDate[$channel])) {
            $tempPaymentDate[$channel] = [];
          }
          foreach ($this->getSettledPaymentDate($channel, Carbon::parse($settlement->date)) as $date) {
            if (isset($totalPayment[$channel][$date])) {
              $settledAmount += $totalPayment[$channel][$date];
            }
            $tempPaymentDate[$channel][] = $date;
          }
        }
      }

      $totalAmount = $settlement->credit;

      if ($settledAmount != $totalAmount) {
        clog('doku_recon', $settlement->date . ' settled amount is: ' . $totalAmount . ' but got: ' . $settledAmount);
        $lastFailedReconDate = $settlement->date;
        continue;
      }
      
      $lastReconDate = $settlement->date;
      $lastFailedReconDate = '';

      $tempPaymentSettlementDetail = [];
      $tempPaymentUpdate = [];
      $tempPaymentGatewayPaymentUpdate = [];
      $tempInvoicePaymentUpdate = [];

      clog('doku_recon', $settlement->date . ' settled amount is: ' . $totalAmount);

      foreach ($tempPaymentDate as $channel => $dates) {
        foreach ($dates as $date) {
          foreach ($groupedPayment[$channel][$date] as $payment) {
            if ($payment instanceof PaymentDokuVaPayment) {
              $tempPaymentSettlementDetail[] = [
                'source_id' => $payment->payment->source_id,
                'source_type' => PaymentChannel::PAYMENT_GATEWAY,
                'vendor_id' => $payment->id,
                'vendor_type' => 'doku_va_close_' . strtolower($channel),
                'amount' => $payment->amount - $payment->cost
              ];
              $tempPaymentUpdate[] = [
                'id' => $payment->id,
                'is_reconciled' => true,
                'updated_at' => $updatedAt
              ];
              $tempPaymentGatewayPaymentUpdate[] = [
                'id' => $payment->payment->source_id,
                'reconciled_at' => Carbon::now()
              ];
            } else if ($payment instanceof PaymentGatewayInvoicePayment) {
              $tempPaymentSettlementDetail[] = [
                'source_id' => $payment->order_id,
                'source_type' => PaymentChannel::ORDER,
                'vendor_id' => $payment->id,
                'vendor_type' => 'doku_va_close_' . strtolower($channel),
                'amount' => $payment->amount - $payment->cost
              ];
              $tempInvoicePaymentUpdate[] = [
                'id' => $payment->id,
                'is_reconciled' => true,
                'updated_at' => $updatedAt
              ];
            }
          }
        }
      }

      $paymentSettlement = $this->paymentSettlement->findByAttributes([
        'bank_id' => Bank::BCA,
        'bank_inquiry_id' => $settlement->id
      ]);

      if (!$paymentSettlement) {
        $paymentSettlement = $this->paymentSettlement->getNew();
        $paymentSettlement->bank_id = Bank::BCA;
        $paymentSettlement->bank_inquiry_id = $settlement->id;
        $paymentSettlement->settlement_amount = $totalAmount;
        $paymentSettlement->settlement_date = $settlement->date;
        $paymentSettlement->settlement_description = $settlement->description;
        $paymentSettlement->save();
      }

      array_walk($tempPaymentSettlementDetail, function(&$detail, $key, $paymentSettlement) {
        $detail['payment_settlement_id'] = $paymentSettlement->id;
      }, $paymentSettlement);
      $paymentSettlementDetails = array_merge($tempPaymentSettlementDetail, $paymentSettlementDetails);
      $paymentUpdate = array_merge($tempPaymentUpdate, $paymentUpdate);
      $paymentGatewayPaymentUpdate = array_merge($tempPaymentGatewayPaymentUpdate, $paymentGatewayPaymentUpdate);
      $invoicePaymentUpdate = array_merge($tempInvoicePaymentUpdate, $invoicePaymentUpdate);

      $settlementUpdates[] = [
        'id' => $settlement->id,
        'reference_type' => BankTransferInquiries::REFERENCE_TYPE_SETTLEMENT_ID,
        'reference' => json_encode(["" . $paymentSettlement->id]),
        'updated_at' => $updatedAt
      ];
    }

    if (!empty($lastFailedReconDate) && $settledAmount > 0) {
      dispatch(new SendPaymentGatewayReconMismatchAlert([
        'date' => $lastFailedReconDate,
        'vendor' => 'Doku VA',
        'description' => '',
        'settlement_amount' => $settledAmount,
        'total_amount' => $totalAmount
      ]));
    }
    
    count($paymentSettlementDetails) && $this->paymentSettlementDetail->saveBulk($paymentSettlementDetails);
    count($settlementUpdates) && $this->bcaRepo->updateBulk($settlementUpdates);
    count($paymentUpdate) && $this->vaPayment->updateBulk($paymentUpdate);
    count($paymentGatewayPaymentUpdate) && $this->paymentGatewayPayment->updateBulk($paymentGatewayPaymentUpdate);
    count($invoicePaymentUpdate) && $this->invoicePayment->updateBulk($invoicePaymentUpdate);
  }

  private function getSettledPaymentDate($channel, $settlementDate) {
    $settlementDay = $settlementDate->dayOfWeek;
    switch($channel) {
      case 'ALFA':
      case 'Alfamart':
        return $this->getSettlementDates($settlementDate, 4);
      case 'PERMATA':
        return $this->getSettlementDates($settlementDate, 1);
      default:
        return $this->getSettlementDates($settlementDate, 1);
    }
  }

  private function getSettlementDates($startDate, $settlementDays) {
    $settlementDates = array();

    $endDate = $this->holidayHelper->subWorkingDays($startDate->copy(), $settlementDays);
    $settlementDates[] = $endDate->format('Y-m-d');
    
    $dateDiff = $endDate->diffInDays($startDate);
    if ($dateDiff > $settlementDays) {
      for ($i = 1; $i < $dateDiff; $i++) {
        $date = $endDate->copy()->addDays($i);

        if ($this->holidayHelper->addWorkingDays($date, $settlementDays)->format('Y-m-d') == $startDate->format('Y-m-d')) {
          $settlementDates[] = $date->format('Y-m-d');
        } else {
          break;
        }
      }
    }

    return $settlementDates;
  }

  private function getChannels($isPaymentGateway) {
    if ($isPaymentGateway) {
      return [
        'ALFA'
      ];
    } else {
      return [
        'ALFA',
        'Alfamart',
        'PERMATA',
      ];
    }
  }
}