<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 25/10/19
 * Time: 16.24
 */

namespace Odeo\Domains\Payment\Doku\VaDirect;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;
use Odeo\Exceptions\FailException;

class Validator extends DokuManager {

  public function validate(PipelineListener $listener, $data) {
    try {
      $this->validateWords($data);
    } catch (FailException $e) {
      clog('doku_va', $e->getMessage());
      return $listener->response(400, $e->getMessage());
    }
    $sessionId = $this->library->createSessionId($data['PAYMENTCODE']);

    return $listener->response(200, ['reference' => $sessionId]);
  }

}