<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 8:35 PM
 */

namespace Odeo\Domains\Payment\Doku\Vagroup;


use Carbon\Carbon;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;
use Odeo\Domains\Account\Helper\CustomerEmailManager;
use Odeo\Domains\Payment\Jobs\SendPaymentPendingAlfa;
use Odeo\Domains\Payment\Jobs\EmailHelper;
use Odeo\Domains\Core\Task;

class Requester extends DokuManager {
  use EmailHelper;

  private $dokuVaPayments;

  public function __construct() {
    parent::__construct();
    $this->dokuVaPayments = app()->make(\Odeo\Domains\Payment\Doku\Vagroup\Repository\PaymentDokuVaPaymentRepository::class);
  }


  public function request(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {
    if ($payment->reference_id) {

      $dokuPayment = $this->dokuPayments->findById($payment->reference_id);

      $vaPayment = $this->dokuVaPayments->findById($dokuPayment->reference_id);

      return $listener->response(200, [
        'type' => Payment::REQUEST_PAYMENT_TYPE_REDIRECT_TO_PAYMENT_DETAIL,
        'content' => [
          'pay_code' => $vaPayment->pay_code,
          'opc_group' => $paymentInformation->info_id
        ]
      ]);
    }

    $this->initializer->switchAccount($paymentInformation->info_id);

    $requestDateTime = date('YmdHis');

    $sessionId = $this->library->createSessionId($order->id);

    $currency = '360';

    $fee = null;

    if ($this->paymentValidator->isAtmTransferVa($paymentInformation->info_id)) {
      $fee = 4500;
    } else if ($this->paymentValidator->isAlfa($paymentInformation->info_id)) {
      $fee = 5000;
    }

    $total = number_format($order->total + $fee, 2, '.', '');

    $words = $this->library->doCreateWords([
      'amount' => $total,
      'invoice' => $order->id,
      'currency' => $currency
    ]);

    $customer = [
      'name' => 'Odeo',
      'data_phone' => '08',
      'data_email' => 'doku@odeo.co.id',
      'data_address' => 'address'
    ];

    $paymentData = array(
      'req_mall_id' => DokuInitializer::$mallId,
      'req_chain_merchant' => 'NA',
      'req_amount' => $total,
      'req_words' => $words,
      'req_trans_id_merchant' => $data['order_id'],
      'req_purchase_amount' => $total,
      'req_request_date_time' => $requestDateTime,
      'req_session_id' => $sessionId,
      'req_email' => $customer['data_email'],
      'req_name' => $customer['name'],
      'req_expiry_time' => Carbon::now()->diffInMinutes(Carbon::parse($order->expired_at), true)
    );

    $result = $this->api->doGeneratePaycode($paymentData);

    if ($result->res_response_code == '0000') {
      if ($this->paymentValidator->isAtmTransferVa($paymentInformation->info_id)) {
        $result->res_pay_code = '89650' . $result->res_pay_code;
      } else if ($this->paymentValidator->isAlfa($paymentInformation->info_id)) {
        $result->res_pay_code = '88888' . $result->res_pay_code;
      }

      $payment = $this->payments->findByOrderId($order->id);

      if ($payment->reference_id) {
        $dokuPayment = $this->dokuPayments->findById($payment->reference_id);
      } else {
        $dokuPayment = $this->dokuPayments->getNew();
      }

      $dokuPayment->request_date_time = date('Y-m-d H:i:s', $requestDateTime);
      $dokuPayment->session_id = $sessionId;
      $dokuPayment->words = $words;
      $dokuPayment->amount = $total;
      $dokuPayment->currency = $currency;
      $dokuPayment->mall_id = DokuInitializer::$mallId;

      if ($payment->reference_id) {
        $vaPayment = $this->dokuVaPayments->findById($dokuPayment->reference_id);
      } else {
        $vaPayment = $this->dokuVaPayments->getNew();
      }

      $vaPayment->pay_code = $result->res_pay_code;
      $vaPayment->pairing_code = $result->res_pairing_code;
      $vaPayment->payment_code = $result->res_payment_code;
      $vaPayment->request_response_msg = $result->res_response_msg;
      $vaPayment->request_response_code = $result->res_response_code;
      $vaPayment->session_id = $sessionId;

      $this->dokuVaPayments->save($vaPayment);

      $dokuPayment->reference_id = $vaPayment->id;

      $this->dokuPayments->save($dokuPayment);

      $payment->reference_id = $dokuPayment->id;

      $this->payments->save($payment);

      $order->status = OrderStatus::OPENED;
      $order->opened_at = date('Y-m-d H:i:s');

      $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

      $order->total = $order->total + $fee;

      $this->orders->save($order);

      $data['pay_code'] = $result->res_pay_code;

//      $email = $this->getEmail($order, $data);
//      $listener->addNext(new Task(CustomerEmailManager::class, 'pushJobIfEmailVerified', [
//        'email' => $email,
//        'job' => new SendPaymentInvoice($order, $paymentInformation, $data)
//      ]));

      $email = $this->getEmail($order, $data);
      $data['opc'] = $payment->opc;
      $listener->addNext(new Task(CustomerEmailManager::class, 'pushJobIfEmailVerified', [
        'email' => $email,
        'job' => new SendPaymentPendingAlfa($order, $data)
      ]));

      return $listener->response(200, [
        'type' => Payment::REQUEST_PAYMENT_TYPE_REDIRECT_TO_PAYMENT_DETAIL,
        'content' => [
          'pay_code' => $result->res_pay_code,
          'opc_group' => $paymentInformation->info_id
        ]
      ]);

    } else {

      return $listener->response(400, null);

    }

  }
}
