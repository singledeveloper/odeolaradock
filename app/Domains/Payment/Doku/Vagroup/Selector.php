<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 11/2/16
 * Time: 10:57 PM
 */

namespace Odeo\Domains\Payment\Doku\Vagroup;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;

class Selector {

  private $alfagroup;

  public function __construct() {
    $this->alfagroup = app()->make(\Odeo\Domains\Payment\Doku\Vagroup\Repository\PaymentDokuVaPaymentRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {

    $this->alfagroup->normalizeFilters($data);

    $alfagroups = [];

    foreach ($this->alfagroup->gets() as $alfagroup) {
      $alfagroups[] = $this->_transforms($alfagroup, $this->alfagroup);
    }

    if (sizeof($alfagroups) > 0) {
      return $listener->response(200, array_merge(
        ["payments" => $this->_extends($alfagroups, $this->alfagroup)],
        $this->alfagroup->getPagination()
      ));
    }

    return $listener->response(204, ["payments" => []]);

  }

}