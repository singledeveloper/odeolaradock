<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 3:44 PM
 */

namespace Odeo\Domains\Payment\Helper;


abstract class PaymentManager {

  use urlGenerator;

  private $orderChargeHelper;

  protected $payments, $orders, $feeGenerator, $paymentValidator;

  public function __construct() {
    $this->orderChargeHelper = app()->make(\Odeo\Domains\Order\Helper\OrderCharger::class);
    $this->payments = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->feeGenerator = app()->make(FeeGenerator::class);
    $this->paymentValidator = app()->make(PaymentValidator::class);
  }


  public function charge($orderId, $type, $amount, $chargeType) {
    $this->orderChargeHelper->charge($orderId, $type, $amount, $chargeType);
  }


}