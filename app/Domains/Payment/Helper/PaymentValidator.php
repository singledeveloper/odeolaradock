<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 10/9/16
 * Time: 4:15 PM
 */

namespace Odeo\Domains\Payment\Helper;

use Odeo\Domains\Constant\Payment;

class PaymentValidator {

  public function isBankTransfer($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_BANK_TRANSFER_BCA ||
      $opcGroup == Payment::OPC_GROUP_BANK_TRANSFER_BNI ||
      $opcGroup == Payment::OPC_GROUP_BANK_TRANSFER_BRI ||
      $opcGroup == Payment::OPC_GROUP_BANK_TRANSFER_MANDIRI ||
      $opcGroup == Payment::OPC_GROUP_BANK_TRANSFER_CIMB ||
      $opcGroup == Payment::OPC_GROUP_BANK_TRANSFER_PERMATA
    );
  }

  public function isDokuVaDirect($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_VA_MANDIRI ||
      $opcGroup == Payment::OPC_GROUP_VA_INDOMARET ||
      $opcGroup == Payment::OPC_GROUP_VA_BRI
    );
  }

  public function isPrismalinkVaDirect($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_VA_BCA
    );
  }

  public function isCc($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_CC
    );
  }

  public function isCcTokenization($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_CC_TOKENIZATION
    );
  }

  public function isCcInstallment($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_CC_INSTALLMENT
    );
  }

  public function isDokuWallet($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_DOKU_WALLET
    );
  }

  public function isAtmTransferVa($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_ATM_TRANSFER_VA
    );
  }

  public function isSinarmasVa($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_SINARMAS
    );
  }

  public function isAlfa($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_ALFA
    );
  }

  public function isMandiriClickPay($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_MANDIRI_CLICKPAY
    );
  }

  public function isKredivo($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_KREDIVO
    );
  }

  public function isCimbClick($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_CIMB_CLICK
    );
  }

  public function isTelkomselCash($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_TELKOMSEL_CASH
    );
  }

  public function isXlTunai($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_XL_TUNAI
    );
  }

  public function isMandiriBillPayment($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_MANDIRI_BILL_PAYMENT
    );
  }

  public function isBbmMoney($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_BBM_MONEY
    );
  }

  public function isIndomaret($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_INDOMARET
    );
  }

  public function isIndosatDompetku($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_INDOSAT_DOMPETKU
    );
  }

  public function isMandiriECash($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_MANDIRI_E_CASH
    );
  }

  public function isBcaClickPay($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_BCA_CLICKPAY
    );
  }

  public function isBriEpay($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_BRI_EPAY
    );
  }

  public function isPostPaid($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_POST_PAID
    );
  }

  public function isOcash($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_OCASH
    );
  }

  public function isCashOnDelivery($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_CASH_ON_DELIVERY
    );
  }

  public function isMandiriMpt($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_MANDIRI_MPT
    );
  }

  public function isDanamonInternetBanking($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_DANAMON_INTERNET_BANKING
    );
  }

  public function isMuamalatInternetBanking($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_MUAMALAT_INTERNET_BANKING
    );
  }

  public function isDebitOnlineBni($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_DEBIT_ONLINE_BNI
    );
  }

  public function isAkulaku($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_AKULAKU
    );
  }


  public function isBniEcollection($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_VA_BNI
    );

  }

  public function isCimbVa($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_VA_CIMB
    );
  }

  public function isPermataVa($opcGroup) {
    return $opcGroup == Payment::OPC_GROUP_VA_PERMATA_DIRECT;
  }

  public function isArtajasaVa($opcGroup) {
    if (isProduction()) return $opcGroup == Payment::OPC_GROUP_VA_ARTAJASA;
    return $opcGroup == 42;
  }

  public function isPax($opcGroup) {
    return (
      $opcGroup == Payment::OPC_GROUP_DEBIT_CREDIT_CARD_PRESENT_PAX_BNI_SWITCHING ||
      $opcGroup == Payment::OPC_GROUP_DEBIT_CREDIT_CARD_PRESENT_PAX_ARTAJASA_SWITCHING
    );
  }

}
