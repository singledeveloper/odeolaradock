<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 3:32 PM
 */

namespace Odeo\Domains\Payment\Helper;


trait urlGenerator {

  function decodeIdt($idt) {
    $paymentUrls = app()->make(\Odeo\Domains\Payment\Repository\PaymentUrlRepository::class);

    return json_decode($paymentUrls->findById(base64_decode($idt))->body, true);
  }

  function generateUrl($body) {

    $paymentUrls = app()->make(\Odeo\Domains\Payment\Repository\PaymentUrlRepository::class);

    if ($url = $paymentUrls->getExisting([
      'order_id' => $body['order_id'],
      'opc' => $body['opc']
    ])->first()
    ) {
      return baseUrl('v1/payment/pay?idt=' . base64_encode($url->id));
    }

    $url = $paymentUrls->getNew();

    $url->order_id = $body['order_id'];
    $url->opc = $body['opc'];
    $url->body = json_encode($body);

    $paymentUrls->save($url);

    return baseUrl('v1/payment/pay?idt=' . base64_encode($url->id));

  }


  public function getUrlData($orderId, $opc) {

    $paymentUrls = app()->make(\Odeo\Domains\Payment\Repository\PaymentUrlRepository::class);

    return json_decode($paymentUrls->getExisting([
      'order_id' => $orderId,
      'opc' => $opc
    ])->first()->body, true);

  }

}