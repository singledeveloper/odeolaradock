<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 5:23 PM
 */

namespace Odeo\Domains\Payment\Jobs;


use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Order\Repository\OrderRepository;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountDetailRepository;
use Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountRepository;
use Odeo\Jobs\Job;

class DevAutoSuccessVAPayment extends Job
{

  use SerializesModels;

  private $data;
  private $userVADetailRepo, $orderRepo, $userVARepo;

  public function __construct($data)
  {
    parent::__construct();
    $this->data = $data;
  }

  public function handle()
  {
    if (isProduction()) {
      return;
    }

    $this->userVARepo = app()->make(UserVirtualAccountRepository::class);
    $this->userVADetailRepo = app()->make(UserVirtualAccountDetailRepository::class);
    $this->orderRepo = app()->make(OrderRepository::class);

    $userVA = $this->userVARepo->findByAttributes([
      'user_id' => $this->data['user_id'] . '',
      'service_detail_id' => $this->data['service_detail_id'],
      'service_reference_id' => $this->data['reference_id'],
    ]);

    if (!$userVA || empty($userVA->details)) {
      return;
    }

    $va = $this->userVADetailRepo->findByNumber($userVA->details[0]->virtual_account_number);

    if (!$va) {
      return;
    }

    $fee = $va->fee;
    $cost = $va->cost;

    $vaData = [
      'opc' => $va->opc,
      'fee' => $fee,
      'cost' => $cost,
      'virtual_account_id' => $va->user_virtual_account_id,
      'platform_id' => Platform::VIRTUAL_ACCOUNT,
      'gateway_id' => Payment::VIRTUAL_ACCOUNT,
      'store_id' => $va->store_id,
      'service_detail_id' => $va->service_detail_id,
      'biller_id' => $va->biller,
      'item_id' => $va->service_reference_id,
      'user_id' => $va->user_id,
      'auth' => [
        'user_id' => $this->data['auth_user_id'],
        'platform_id' => Platform::VIRTUAL_ACCOUNT,
      ],
      'first_name' => $va->display_name,
      'vendor_code' => $va->vendor
    ];

    $pipeline = new Pipeline();
    $pipeline->add(new Task(CartRemover::class, 'clear'));
    $pipeline->add(new Task(CartInserter::class, 'addToCart'));
    $pipeline->add(new Task(CartCheckouter::class, 'checkout'));
    $pipeline->add(new Task(PaymentRequester::class, 'request'));
    $pipeline->execute($vaData);

    if (!$pipeline->success()) {
      \Log::info(json_encode($pipeline->errorMessage));
      return;
    }

    $order = $this->orderRepo->findById($pipeline->data['order_id']);
    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');
    $this->orderRepo->save($order);
    dispatch(new VerifyOrder($order->id));
  }

}
