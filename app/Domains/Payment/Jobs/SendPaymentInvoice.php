<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 5:23 PM
 */

namespace Odeo\Domains\Payment\Jobs;


use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Constant\BillerPundi;
use Odeo\Jobs\Job;

class SendPaymentInvoice extends Job  {

  use SerializesModels, EmailHelper;

  private $order, $data, $paymentInformation;
  private $paymentValidator, $orderDetailizer, $currencyHelper;

  public function __construct($order, $paymentInformation, $data) {
    parent::__construct();
    $this->order = $order;
    $this->orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);
    $this->data = $data;
    $this->paymentInformation = $paymentInformation;
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->paymentValidator = app()->make(\Odeo\Domains\Payment\Helper\PaymentValidator::class);

  }

  public function handle() {

    $response = [];

    if ($this->order->actual_store_id == BillerPundi::ODEO_STORE_ID) return;

    if ($response['email'] = $this->getEmail($this->order, $this->data)) ;
    else return;

    $response['date_opened'] = $this->order->created_at;
    $response['date_expired'] = $this->order->expired_at;
    $response['order_id'] = $this->order->id;
    $response['platform_id'] = $this->order->platform_id;
    $response['cart_data']['items'] = $this->orderDetailizer->detailizeItem($this->order, [
      'mask_phone_number'
    ]);
    $response['cart_data']['charges'] = $this->orderDetailizer->detailizeCharge($this->order->id);
    $response['cart_data']['total'] = $this->currencyHelper->formatPrice($this->order->total);
    $response['cart_data']['subtotal'] = $this->currencyHelper->formatPrice($this->order->subtotal);
    $response['payment_data'] = $this->paymentInformation->information->toArray();
    if ($response['payment_data']['detail']) {
      $response['payment_data']['detail'] = \json_decode($response['payment_data']['detail'], true);
    }
    $response = array_merge($response, $this->getStoreData($this->order->actual_store_id));

    $viewName = null;

    if ($this->paymentValidator->isBankTransfer($this->paymentInformation->info_id)) {
      $response['payment_confirmation_link'] = $this->_generatePaymentConfLink($response['domain']);
      $viewName = 'emails.invoice._bank_detail';
    } else if ($this->paymentValidator->isAlfa($this->paymentInformation->info_id)) {
      $viewName = 'emails.invoice._alfa';
      $response['pay_code'] = $this->data['pay_code'];
    } else if ($this->paymentValidator->isAtmTransferVa($this->paymentInformation->info_id)) {
      $viewName = 'emails.invoice._va_transfer';
      $response['pay_code'] = $this->data['pay_code'];
    } else if ($this->paymentValidator->isMandiriMpt($this->paymentInformation->info_id)) {
      $viewName = 'emails.invoice._mpt_transfer';
      $response['pay_code'] = $this->data['pay_code'];
    }

    $response['payment_section'] = view($viewName, [
      'data' => $response
    ])->render();


    $response['email_info'] = $this->getEmailInfo($this->order->actual_store_id);

    Mail::send('emails.invoice.index', ['data' => $response], function ($m) use ($response) {

      $m->from($response['email_info']['sender_email'], $response['email_info']['sender_name']);

      $m->to($response['email'])->subject(ucfirst($response['email_info']['sender_name']) . ' Invoice - ' . $response['order_id']);
    });

  }

  private function _generatePaymentConfLink($domain) {

    $tokenCreator = app()->make(\Odeo\Domains\Account\Helper\TokenCreator::class);

    $queryString = http_build_query([
      'idt' => base64_encode(
        (string)$tokenCreator->createToken($this->data['auth']['user_id'], $this->data['auth']['type'], $this->data['auth']['platform_id']) . ':' .
        $this->order->id . ':' .
        $this->paymentInformation->code
      )
    ]);

    return $domain . "/order/payment/confirmation?" . $queryString;
  }

}
