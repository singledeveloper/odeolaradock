<?php

namespace Odeo\Domains\Payment\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendVaInquiryFailAlert extends Job {

  use SerializesModels;

  private $data, $errorMessage;

  public function __construct($data, $errorMessage) {
    parent::__construct();
    $this->data = $data;
    $this->errorMessage = $errorMessage;
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

    Mail::send('emails.va_inquiry_fail', [
      'data' => $this->data,
      'errorMessage' => $this->errorMessage
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('pg@odeo.co.id')
        ->subject('VA Inquiry Fail - ' . date('Y-m-d'));
    });


  }

}
