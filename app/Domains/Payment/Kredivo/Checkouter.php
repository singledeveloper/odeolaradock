<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 10:43 PM
 */

namespace Odeo\Domains\Payment\Kredivo;


use Carbon\Carbon;
use GuzzleHttp\Client;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Kredivo\Helper\KredivoApi;
use Odeo\Domains\Payment\Kredivo\Helper\KredivoManager;

class Checkouter extends KredivoManager {

  public function checkout(PipelineListener $listener, $order, $paymentInformation, $data) {

    if ($order->status != OrderStatus::CREATED) {
      return $listener->response(400, 'Please wait, we will verify your order once kredivo has notified us your payment.');
    }

    $basket = array();

    $orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);

    foreach ($orderDetailizer->detailizeItem($order, [
      'order_detail_id'
    ]) as $item) {

      array_push($basket, [
        'id' => $item['order_detail_id'],
        'name' => $item['name'],
        'price' => (float)number_format($item['price']['amount'], 2, '.', ''),
        'quantity' => (integer)$item['quantity'],
        'type' => $item['type']
      ]);
    }

    $mdr = 2.3;

    if (in_array($order->gateway_id, [
      Payment::ANDROID_OCASH, Payment::WEB_APP_OCASH,
      Payment::ANDROID_ODEPOSIT, Payment::WEB_APP_ODEPOSIT
    ])) {
      $mdr = 2.9;
    }

    $total = $this->feeGenerator->getFee($mdr, $order->total) + $order->total;

    $data = [
      'server_key' => KredivoApi::$SERVER_KEY,
      'payment_type' => '30_days',
      'expiration_time' => Carbon::parse($order->expired_at)->timestamp,
      'transaction_details' => [
        'order_id' => $order->id,
        'amount' => (float)$total,
        'items' => $basket
      ],
      'push_uri' => app()->environment() == 'production' ? 'http://api.odeo.co.id/v1/payment/kredivo/notify' : baseUrl('v1/payment/kredivo/notify'),
      'back_to_store_uri' => $data['finish_redirect_url']
    ];

    try {

      $client = new Client();

      $res = $client->post(KredivoApi::$CHECKOUTURL, [
        'json' => $data
      ]);

    } catch (\Exception $exception) {
      return $listener->response(400, 'Unable to connect to kredivo.');
    }

    $data = json_decode($res->getBody());

    if (isset($data->status) && $data->status == 'ERROR') {
      return $listener->response(400, 'Exception from Kredivo.');
    } else if ($res->getStatusCode() != 200 || $data->status == 'ERROR') {
      return $listener->response(400);
    }

    return $listener->response(200, [
      'type' => Payment::REQUEST_PAYMENT_TYPE_URL,
      'content' => [
        'link' => $data->redirect_url
      ],
    ]);
  }
}