<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 10:39 PM
 */

namespace Odeo\Domains\Payment\Kredivo\Helper;


class KredivoApi {

  public static $SERVER_KEY = 'scUCOJGsjw4HeDrDLHSo0gljF8l3v5';

  public static $CHECKOUTURL = 'https://api.kredivo.com/kredivo/v2/checkout_url';
  public static $CONFIRMURL = 'https://api.kredivo.com/kredivo/v2/update';
  public static $CANCELURL = 'https://api.kredivo.com/kredivo/v1/cancel';
  public static $CONFIRMATION_URL = 'https://api.kredivo.com/kredivo/v1/update';
  public static $VOID_URL = 'https://api.kredivo.com/kredivo/v2/cancel_transaction';

  static function init()
  {
    if (app()->environment() != 'production') {
      self::$SERVER_KEY = 'EP7ecuX01ArJ8jOqEacFeg5v7JtYSO';
      self::$CHECKOUTURL = 'https://sandbox.kredivo.com/kredivo/v2/checkout_url';
      self::$CONFIRMURL = 'https://sandbox.kredivo.com/kredivo/v2/update';
      self::$CANCELURL = 'https://sandbox.kredivo.com/kredivo/v1/cancel';
      self::$CONFIRMATION_URL = 'https://sandbox.kredivo.com/kredivo/v1/update';
      self::$VOID_URL = 'https://sandbox.kredivo.com/kredivo/v2/cancel_transaction';
    }
  }


}