<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/1/17
 * Time: 4:54 PM
 */

namespace Odeo\Domains\Payment\Kredivo\Helper;


use Carbon\Carbon;

class KredivoLockManager {

  private $kredivoLockPayments, $currencyHelper;

  public function __construct() {
    $this->kredivoLockPayments = app()->make(\Odeo\Domains\Payment\Kredivo\Repository\PaymentKredivoLockPaymentRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function insert($data) {
    $kredivoLockPayment = $this->kredivoLockPayments->getNew();

    $kredivoLockPayment->user_id = $data['user_id'];
    $kredivoLockPayment->order_id = $data['order_id'];
    $kredivoLockPayment->locked_amount = $data['locked_amount'];

    $this->kredivoLockPayments->save($kredivoLockPayment);
  }

  public function validateWithdraw($userId, $remainOCashBalance, $requestedAmount) {

    $willBeDelete = [];
    $lockedAmount = 0;
    $now = Carbon::now();

    $kredivoLockedPayments = $this->kredivoLockPayments->getByUserId($userId);

    foreach ($kredivoLockedPayments as $locked) {
      if (Carbon::parse($locked->created_at)->diffInDays($now) >= 10) {
        $willBeDelete[] = $locked->order_id;
      } else {
        $lockedAmount += $locked->locked_amount;
      }
    }

    if ($remainOCashBalance < $lockedAmount) {
      app()->make(\Odeo\Domains\Payment\Kredivo\Model\PaymentKredivoLockPayment::class)
        ->getCloneModel()->whereIn('order_id', $kredivoLockedPayments->pluck('order_id')->all())->delete();
      $lockedAmount = 0;
    } else if (count($willBeDelete)) {
      app()->make(\Odeo\Domains\Payment\Kredivo\Model\PaymentKredivoLockPayment::class)
        ->getCloneModel()->whereIn('order_id', $willBeDelete)->delete();
    }

    if ($lockedAmount === 0) return false;

    if ($remainOCashBalance - $requestedAmount < $lockedAmount) {
      return [
        'total_balance' => $this->currencyHelper->formatPrice($remainOCashBalance),
        'locked_amount' => $this->currencyHelper->formatPrice($lockedAmount),
        'withdrawable_amount' => $this->currencyHelper->formatPrice($remainOCashBalance - $lockedAmount),
        'tnc' => [
          [
            '1_text' => trans('payment/kredivo.tnc_withdraw_kredivo_1'),
          ],
          [
            '1_text' => trans('payment/kredivo.tnc_withdraw_kredivo_2'),
          ],
          [
            '1_text' => trans('payment/kredivo.tnc_withdraw_kredivo_3'),
          ]
        ]
      ];
    }

    return false;

  }


}