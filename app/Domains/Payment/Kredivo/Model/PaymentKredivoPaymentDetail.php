<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 10:35 PM
 */

namespace Odeo\Domains\Payment\Kredivo\Model;


use Odeo\Domains\Core\Entity;

class PaymentKredivoPaymentDetail extends Entity {

  public $timestamps = false;

}