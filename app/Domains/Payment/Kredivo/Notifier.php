<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 10:44 PM
 */

namespace Odeo\Domains\Payment\Kredivo;


use GuzzleHttp\Client;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Kredivo\Helper\KredivoApi;
use Odeo\Domains\Payment\Kredivo\Helper\KredivoManager;

class Notifier extends KredivoManager {

  public function notified(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {

    if ($data['status'] != "OK") {
      return $listener->response(200);
    }

    $kredivoPayment = $this->kredivoPayments->getNew();

    $kredivoPayment->transaction_id = $data['transaction_id'];
    $kredivoPayment->signature_key = $data['signature_key'];
    $kredivoPayment->transaction_time = date('Y-m-d H:i:s', $data['transaction_time']);
    $kredivoPayment->amount = $data['amount'];
    $kredivoPayment->payment_type = $data['payment_type'];
    $kredivoPayment->message = $data['message'];
    $kredivoPayment->need_reconfirmation = false;

    $this->kredivoPayments->save($kredivoPayment);

    $payment->reference_id = $kredivoPayment->id;

    $this->payments->save($payment);

    $kredivoPaymentDetail = $this->kredivoPaymentDetails->getNew();

    $kredivoPaymentDetail->kredivo_payment_id = $kredivoPayment->id;
    $kredivoPaymentDetail->process_step = 'NOTIFY';
    $kredivoPaymentDetail->status = $data['status'];
    $kredivoPaymentDetail->message = $data['message'];
    $kredivoPaymentDetail->transaction_status = $data['transaction_status'];
    $kredivoPaymentDetail->transaction_time = date('Y-m-d H:i:s', $data['transaction_time']);

    $this->kredivoPaymentDetails->save($kredivoPaymentDetail);

    $client = new Client();


    try {

      $res = $client->get(KredivoApi::$CONFIRMURL . '?transaction_id=' . $data['transaction_id'] . '&signature_key=' . $data['signature_key'], [
        'timeout' => 60,
      ]);
    } catch (\Exception $exception) {
      clog('kredivo_confirm_error', \json_encode($exception->getMessage()));
      $kredivoPayment->need_reconfirmation = true;
      $this->kredivoPayments->save($kredivoPayment);

      return $listener->response(400);
    }

    $confirmationData = json_decode($res->getBody(), true);

    $kredivoPayment->fraud_status = $confirmationData['fraud_status'];

    $this->kredivoPayments->save($kredivoPayment);

    $kredivoPaymentDetail = $this->kredivoPaymentDetails->getNew();

    $kredivoPaymentDetail->kredivo_payment_id = $kredivoPayment->id;
    $kredivoPaymentDetail->process_step = 'CONFIRM';
    $kredivoPaymentDetail->status = $confirmationData['status'];
    $kredivoPaymentDetail->message = $confirmationData['message'];
    $kredivoPaymentDetail->transaction_status = $confirmationData['transaction_status'];
    $kredivoPaymentDetail->transaction_time = date('Y-m-d H:i:s', $confirmationData['transaction_time']);

    $this->kredivoPaymentDetails->save($kredivoPaymentDetail);


    if ($confirmationData['transaction_status'] != 'settlement' || $confirmationData['fraud_status'] != 'accept') {
      return $listener->response(400);
    }

    $defaultMdr = $mdr = 2.3;

    if (in_array($order->gateway_id, [
      Payment::ANDROID_OCASH, Payment::WEB_APP_OCASH,
      Payment::ANDROID_ODEPOSIT, Payment::WEB_APP_ODEPOSIT
    ])) {
      $mdr = 2.9;
      $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST,
        $this->feeGenerator->getFee($mdr - $defaultMdr, $order->total),
        OrderCharge::GROUP_TYPE_COMPANY_PROFIT);
    }

    $fee = $this->feeGenerator->getFee($mdr, $order->total);
    $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

    $order->total = $order->total + $fee;

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');

    $this->orders->save($order);


    app()
      ->make(\Odeo\Domains\Payment\Kredivo\Helper\KredivoLockManager::class)
      ->insert([
        'order_id' => $order->id,
        'user_id' => $order->user_id,
        'locked_amount' => $order->subtotal
      ]);

    $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200);

  }



}