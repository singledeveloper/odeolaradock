<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 10:36 PM
 */

namespace Odeo\Domains\Payment\Kredivo\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Kredivo\Model\PaymentKredivoPaymentDetail;

class PaymentKredivoPaymentDetailRepository extends Repository {


  public function __construct(PaymentKredivoPaymentDetail $kredivoPaymentDetail) {
    $this->model = $kredivoPaymentDetail;
  }
}