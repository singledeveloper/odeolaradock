<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 10:35 PM
 */

namespace Odeo\Domains\Payment\Kredivo\Repository;


use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Kredivo\Model\PaymentKredivoPayment;

class PaymentKredivoPaymentRepository extends Repository {

  public function __construct(PaymentKredivoPayment $kredivoPayment) {
    $this->model = $kredivoPayment;
  }

  public function findByTransactionId($transaction_id) {
    return $this->model->where('transaction_id', $transaction_id)->first();
  }

  public function gets() {

    $payment = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);

    $query = $payment->getCloneModel();

    if (isset($filters['search'])) {
      if (isset($filters['search']['order_id'])) {
        $id = (int)$filters['search']['order_id'];
        $query = $query->where('order_id', $id);
      }
      if (isset($filters['search']['payment_id'])) {
        $query = $query->where('payment_id', $filters['search']['user_id']);
      }

    }

    $query = $query->with('channel')->whereHas('channel', function ($query) {
      $query->where('info_id', '=', \Odeo\Domains\Constant\Payment::OPC_GROUP_KREDIVO);
    })->join('payment_kredivo_payments', 'payments.reference_id', '=', 'payment_kredivo_payments.id')
      ->join('payment_kredivo_payment_details', 'payment_kredivo_payments.id', '=', 'payment_kredivo_payment_details.kredivo_payment_id');

    return $this->getResult($query);
  }


  public function getNeedReconfirmation() {
    $query = $this->getCloneModel();
    return $query
      ->where('need_reconfirmation', '=', true)
      ->get();
  }
}