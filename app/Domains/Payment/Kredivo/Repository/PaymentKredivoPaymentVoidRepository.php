<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 10:35 PM
 */

namespace Odeo\Domains\Payment\Kredivo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Kredivo\Model\PaymentKredivoPaymentVoid;


class PaymentKredivoPaymentVoidRepository extends Repository {

  public function __construct(PaymentKredivoPaymentVoid $paymentKredivoPaymentVoid) {
    $this->model = $paymentKredivoPaymentVoid;
  }

  public function findByOrderId($orderId) {
    return $this->model->where('order_id', $orderId)->first();
  }

}