<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/29/17
 * Time: 5:20 PM
 */

namespace Odeo\Domains\Payment\Kredivo\Scheduler;


use GuzzleHttp\Client;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Kredivo\Helper\KredivoApi;
use Odeo\Domains\Payment\Kredivo\Helper\KredivoManager;

class ReConfirmTransactionScheduler extends KredivoManager {

  public function run() {

    $needConfirmations = $this->kredivoPayments->getNeedReconfirmation();

    foreach ($needConfirmations as $kredivoPayment) {

      $client = new Client();

      try {

        $res = $client->get(KredivoApi::$CONFIRMURL . '?transaction_id=' . $kredivoPayment->transaction_id . '&signature_key=' . $kredivoPayment->signature_key, [
          'timeout' => 60,
          'verify' => false
        ]);

      } catch (\Exception $exception) {
        continue;
      }

      $confirmationData = json_decode($res->getBody(), true);

      $kredivoPayment->fraud_status = $confirmationData['fraud_status'];
      $kredivoPayment->need_reconfirmation = false;
      $this->kredivoPayments->save($kredivoPayment);

      $kredivoPaymentDetail = $this->kredivoPaymentDetails->getNew();

      $kredivoPaymentDetail->kredivo_payment_id = $kredivoPayment->id;
      $kredivoPaymentDetail->process_step = 'CONFIRM';
      $kredivoPaymentDetail->status = $confirmationData['status'];
      $kredivoPaymentDetail->message = $confirmationData['message'];
      $kredivoPaymentDetail->transaction_status = $confirmationData['transaction_status'];
      $kredivoPaymentDetail->transaction_time = date('Y-m-d H:i:s', $confirmationData['transaction_time']);

      $this->kredivoPaymentDetails->save($kredivoPaymentDetail);

      if ($confirmationData['transaction_status'] != 'settlement' || $confirmationData['fraud_status'] != 'accept') {
        continue;
      }

      $order = $this->orders->findById($confirmationData['order_id']);

      if ($order->status >= OrderStatus::VERIFIED && $order->status != OrderStatus::CANCELLED) {
        continue;
      }

      $defaultMdr = $mdr = 2.3;

      if (in_array($order->gateway_id, [
        Payment::ANDROID_OCASH, Payment::WEB_APP_OCASH,
        Payment::ANDROID_ODEPOSIT, Payment::WEB_APP_ODEPOSIT
      ])) {
        $mdr = 2.9;
        $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST,
          $this->feeGenerator->getFee($mdr - $defaultMdr, $order->total),
          OrderCharge::GROUP_TYPE_COMPANY_PROFIT);
      }

      $fee = $this->feeGenerator->getFee($mdr, $order->total);
      $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

      $order->total = $order->total + $fee;

      $order->status = OrderStatus::VERIFIED;
      $order->paid_at = date('Y-m-d H:i:s');

      $this->orders->save($order);

      app()
        ->make(\Odeo\Domains\Payment\Kredivo\Helper\KredivoLockManager::class)
        ->insert([
          'order_id' => $order->id,
          'user_id' => $order->user_id,
          'locked_amount' => $order->subtotal
        ]);

      dispatch(new VerifyOrder($order->id));


    }

  }

}
