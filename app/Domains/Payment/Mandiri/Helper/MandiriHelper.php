<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 22/04/19
 * Time: 17.42
 */

namespace Odeo\Domains\Payment\Mandiri\Helper;


class MandiriHelper {

  const COMPANY_CODE = '89110';
  const PREFIX_TOPUP_OCASH = '1';

  const STATUS_OK = '00';
  const STATUS_NOT_FOUND = 'B5';
  const STATUS_ERROR = '01';
  const STATUS_PAID = 'B8';
  const STATUS_TIMEOUT = '89';

  const STATUS_CODES = [
    self::STATUS_OK => [
      'errorCode' => '00',
      'statusDescription' => 'Sukses'
    ],
    self::STATUS_ERROR => [
      'errorCode' => '01',
      'isError' => 'true',
      'statusDescription' => 'System tidak bisa melayani transaksi'
    ],
    self::STATUS_NOT_FOUND => [
      'errorCode' => 'B5',
      'statusDescription' => 'Tagihan tidak ditemukan / Nomor Referensi salah'
    ],
    self::STATUS_PAID => [
      'errorCode' => 'B8',
      'statusDescription' => 'Tagihan sudah dibayar'
    ],
    'C0' => [
      'errorCode' => 'C0',
      'statusDescription' => 'Tagihan diblokir, harap hubungi perusahaan'
    ],
    '86' => [
      'errorCode' => '86',
      'statusDescription' => 'Transaksi tidak bisa dibatalkan'
    ],
    '87' => [
      'errorCode' => '87',
      'statusDescription' => 'Provider Database Problem'
    ],
    self::STATUS_TIMEOUT => [
      'errorCode' => '89',
      'statusDescription' => 'Time Out'
    ],
    '91' => [
      'errorCode' => '91',
      'statusDescription' => 'Link Down'
    ]
  ];

  public function isOurCompany($billKey) {
    return strpos($billKey, self::COMPANY_CODE) === 0;
  }

  public function getPrefix($billKey) {
//    return substr($billKey, 5, 1);
    return self::PREFIX_TOPUP_OCASH;
  }

  public function getPhoneNo($billKey) {
    return '628' . substr($billKey, 5);
  }

  public function getBillName($prefix) {
    switch ($prefix) {
      case self::PREFIX_TOPUP_OCASH:
        return 'Topup oCash';
      default:
        return '';
    }
  }

  public static function getBillPaymentFee() {
    return 2000;
  }

  public function formatObject($data) {
    $object = new \stdClass;
    foreach ($data as $key => $val) {
      $object->$key = $val;
    }

    return $object;
  }

  public function formatInquiryResponseXml($data) {
    return
      '<inquiryResult>
        <currency>' . $data['currency'] . '</currency>
        <billInfo1>' . $data['billInfo1'] . '</billInfo1>
        <billInfo2>' . $data['billInfo2'] . '</billInfo2>
        <billDetails>
          <BillDetail>
            <billCode>' . $data['billCode'] . '</billCode>
            <billName>' . $data['billName'] . '</billName>
            <billShortName>OCS</billShortName>
            <billAmount>' . $data['billAmount'] . '</billAmount>
          </BillDetail>
        </billDetails>
        <status>
          <isError>' . $data['isError'] . '</isError>
          <errorCode>' . $data['errorCode'] . '</errorCode>
          <statusDescription>' . $data['statusDescription'] . '</statusDescription>
        </status>
      </inquiryResult>';
  }

  public function formatPaymentResponseXml($data) {
    return
      '<paymentResult>
        <billInfo1>' . $data['billInfo1'] . '</billInfo1>
        <billInfo2>' . $data['billInfo2'] . '</billInfo2>
        <billInfo3>' . $data['billInfo3'] . '</billInfo3>
        <billInfo4>' . $data['billInfo4'] . '</billInfo4>
        <status>
          <isError>' . $data['isError'] . '</isError>
          <errorCode>' . $data['errorCode'] . '</errorCode>
          <statusDescription>' . $data['statusDescription'] . '</statusDescription>
        </status>
      </paymentResult>';
  }

}