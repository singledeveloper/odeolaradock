<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/10/19
 * Time: 15.24
 */

namespace Odeo\Domains\Payment\Mandiri\Jobs;


use Carbon\Carbon;
use Odeo\Domains\Payment\Mandiri\Repository\PaymentMandiriVaPaymentRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository;
use Odeo\Jobs\Job;

class Reconciler extends Job {

  private $data, $vaPayment, $pgPayment, $mandiriInquiry;

  function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->vaPayment = app()->make(PaymentMandiriVaPaymentRepository::class);
    $this->pgPayment = app()->make(PaymentGatewayPaymentRepository::class);
    $this->mandiriInquiry = app()->make(BankMandiriGiroInquiryRepository::class);
  }

  public function handle() {
    \DB::transaction(function() {
      $bankInquiry = $this->mandiriInquiry->findById($this->data['bank_inquiry_id']);

      if (isset($bankInquiry->reference_type) && $bankInquiry->reference_type != 'note') return;

      $bankInquiry->reference_type = 'payment_gateway_payment_id';
      $bankInquiry->reference = json_encode(["" . $this->data['pg_payment_id']]);
      $this->mandiriInquiry->save($bankInquiry);

      $pgPayment = $this->pgPayment->findById($this->data['pg_payment_id']);
      $pgPayment->reconciled_at = Carbon::now();
      $this->pgPayment->save($pgPayment);

      $vaPayment = $this->vaPayment->findById($this->data['va_payment_id']);
      $vaPayment->is_reconciled = true;
      $this->vaPayment->save($vaPayment);
    });
  }

}