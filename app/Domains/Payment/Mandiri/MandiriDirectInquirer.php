<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 22/04/19
 * Time: 15.54
 */

namespace Odeo\Domains\Payment\Mandiri;


use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Mandiri\Helper\MandiriHelper;
use Odeo\Domains\Payment\Mandiri\Helper\MandiriManager;

class MandiriDirectInquirer extends MandiriManager {

  public function __construct() {
    parent::__construct();
  }

  public function vaInquiry(PipelineListener $listener, $data) {
    $pgUserChannel = $this->pgUserPaymentChannel->findByActivePrefix($data['billKey1']);
    if (!$pgUserChannel) {
      return $listener->response(400, ['status_code' => 'B5', 'message' => 'no active prefix']);
    }

    $channel = $this->odeoPaymentChannel->findByGatewayIdAndGroupId(Payment::PAYMENT_GATEWAY, $pgUserChannel->payment_group_id);
    if (!$channel) {
      return $listener->response(400, ['status_code' => 'B5', 'message' => 'channel not found']);
    }

    $referenceId = round(microtime(true) * 10000);

    return $listener->response(200, [
      'va_code' => $data['billKey1'],
      'va_prefix' => $pgUserChannel->prefix,
      'user_id' => $pgUserChannel->user_id,
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'vendor_reference_id' => $referenceId,
      'trx_date_time' => $data['trxDateTime'],
      'trans_date_time' => $data['transmissionDateTime'],
      'company_code' => $data['companyCode'],
      'channel_id' => $data['channelID'],
      'bill_key_1' => $data['billKey1'],
      'bill_key_2' => empty($data['billKey2']) ? '' : $data['billKey2'],
      'bill_key_3' => empty($data['billKey3']) ? '' : $data['billKey3'],
      'reference_1' => empty($data['reference1']) ? '' : $data['reference1'],
      'reference_2' => empty($data['reference2']) ? '' : $data['reference2'],
      'reference_3' => empty($data['reference3']) ? '' : $data['reference3'],
    ]);
  }

  public function createInquiry(PipelineListener $listener, $data) {

    $inquiry = $this->mandiriVaInquiry->getNew();

    $inquiry->trx_date_time = $data['trx_date_time'];
    $inquiry->transmission_date_time = $data['trans_date_time'];
    $inquiry->company_code = $data['company_code'];
    $inquiry->channel_id = $data['channel_id'];
    $inquiry->bill_key_1 = $data['bill_key_1'];
    $inquiry->bill_key_2 = $data['bill_key_2'];
    $inquiry->bill_key_3 = $data['bill_key_3'];
    $inquiry->reference_1 = $data['reference_1'];
    $inquiry->reference_2 = $data['reference_2'];
    $inquiry->reference_3 = $data['reference_3'];

    $inquiry->bill_name = $data['display_text'];
    $inquiry->bill_amount = $data['amount'];
    $inquiry->bill_info_1 = $data['customer_name'];
    $inquiry->bill_info_2 = $data['item_name'];
    $inquiry->reference_number = $data['vendor_reference_id'];
    
    $this->mandiriVaInquiry->save($inquiry);

    return $listener->response(200, [
      'bill_code' => '01',
      'bill_name' => $data['display_text'],
      'bill_amount' => $data['amount'],
      'status_code' => MandiriHelper::STATUS_OK,
      'currency' => '360',
      'bill_info_1' => $data['customer_name'],
      'bill_info_2' => $data['item_name'],
    ]);
  }

  public function inquiry(PipelineListener $listener, $data) {

    $inquiry = $this->mandiriVaInquiry->getNew();
    $inquiry->language = $data['language'];
    $inquiry->trx_date_time = $data['trxDateTime'];
    $inquiry->transmission_date_time = $data['transmissionDateTime'];
    $inquiry->company_code = $data['companyCode'];
    $inquiry->channel_id = $data['channelID'];
    $inquiry->bill_key_1 = $data['billKey1'];
    $inquiry->bill_key_2 = empty($data['billKey2']) ? '' : $data['billKey2'];
    $inquiry->bill_key_3 = empty($data['billKey3']) ? '' : $data['billKey3'];
    $inquiry->reference_1 = empty($data['reference1']) ? '' : $data['reference1'];
    $inquiry->reference_2 = empty($data['reference1']) ? '' : $data['reference1'];;
    $inquiry->reference_3 = empty($data['reference1']) ? '' : $data['reference1'];;

    $phone = $this->helper->getPhoneNo($data['billKey1']);
    $prefix = $this->helper->getPrefix($data['billKey1']);
    $billName = $this->helper->getBillName($prefix);

    $this->mandiriVaInquiry->save($inquiry);

    if ($billName == ''
      || !$this->helper->isOurCompany($data['billKey1'])
      || !($user = $this->users->findByTelephone(purifyTelephone($phone)))
    ) {
      return $listener->response(400);
    }

    $result = [
      'bill_code' => '01',
      'bill_name' => $billName . ' ' . $phone,
      'bill_amount' => $data['billKey2'],
      'status_code' => MandiriHelper::STATUS_OK,
      'currency' => '360',
      'bill_info_1' => $user->name,
      'bill_info_2' => '',
    ];

    $inquiry->inquiry_response = json_encode($result);
    $this->mandiriVaInquiry->save($inquiry);

    return $listener->response(200, $result);
  }
}