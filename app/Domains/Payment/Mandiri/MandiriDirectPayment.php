<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 22/04/19
 * Time: 17.34
 */

namespace Odeo\Domains\Payment\Mandiri;


use Carbon\Carbon;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Payment\Mandiri\Helper\MandiriHelper;
use Odeo\Domains\Payment\Mandiri\Helper\MandiriManager;
use Odeo\Domains\Payment\Repository\PaymentRepository;
use Odeo\Domains\PaymentGateway\Jobs\NotifyPaymentGatewayPaymentStatus;
use Odeo\Domains\Transaction\TopupRequester;

class MandiriDirectPayment extends MandiriManager {

  private $payment;

  function __construct() {
    parent::__construct();
    $this->payment = app()->make(PaymentRepository::class);
  }

  public function vaPayment(PipelineListener $listener, $data) {
    $pgUserChannel = $this->pgUserPaymentChannel->findByActivePrefix($data['billKey1']);

    if (!$pgUserChannel) {
      return $listener->response(400, ['status_code' => 'B5', 'message' => 'channel not found']);
    }

    $channel = $this->odeoPaymentChannel->findByGatewayIdAndGroupId(Payment::PAYMENT_GATEWAY, $pgUserChannel->payment_group_id);
    if (!$channel) {
      return $listener->response(400, ['status_code' => 'B5', 'message' => 'channel not found']);
    }

    $inquiry = $this->mandiriVaInquiry->findLastInquiry($data['billKey1']);
    if (!$inquiry || $inquiry->created_at->diffInMinutes(Carbon::now()) > 10) {
      return $listener->response(400, ['status_code' => 'B5', 'message' => 'channel not found']);
    }

    $now = date('Y-m-d');
    $namespace = $this->getRedisNamespace($now);
    if (!$this->redis->hsetnx($namespace, $inquiry->id, 1)) {
      return $listener->response(400, ['status_code' => 'C0', 'message' => 'channel not found']);
    }

    $this->redis->expire($namespace, 2 * 24 * 60 * 60);
    $tomorrow = Carbon::tomorrow()->format('Y-m-d');
    $namespace = $this->getRedisNamespace($tomorrow);

    if (!$this->redis->hset($namespace, $inquiry->id, 1)) {
      return $listener->response(400, ['status_code' => 'C0', 'message' => 'channel not found']);
    }
    $this->redis->expire($namespace, 2 * 24 * 60 * 60);

    $vaPayment = $this->mandiriVaPayment->getNew();
    $vaPayment->inquiry_id = $inquiry->id;
    $vaPayment->trx_date_time = $data['trxDateTime'];
    $vaPayment->transmission_date_time = $data['transmissionDateTime'];
    $vaPayment->company_code = $data['companyCode'];
    $vaPayment->channel_id = $data['channelID'];
    $vaPayment->bill_key_1 = $data['billKey1'];
    $vaPayment->bill_key_2 = $this->getValue($data['billKey2']);
    $vaPayment->bill_key_3 = $this->getValue($data['billKey3']);
    $vaPayment->paid_bills = $data['paidBills']['string'];
    $vaPayment->payment_amount = $this->getValue($data['paymentAmount']);
    $vaPayment->transaction_id = $this->getValue($data['transactionID']);
    $vaPayment->reference_1 = $this->getValue($data['reference1']);
    $vaPayment->reference_2 = $this->getValue($data['reference2']);
    $vaPayment->reference_3 = $this->getValue($data['reference3']);
    $vaPayment->status = PaymentGateway::ACCEPTED;
    $vaPayment->reference_number = $inquiry->reference_number;
    $this->mandiriVaPayment->save($vaPayment);

    $payment = $this->payment->getNew();
    $payment->source_type = PaymentChannel::PAYMENT_GATEWAY;
    $payment->info_id = $pgUserChannel->payment_group_id;
    $payment->opc = $channel->code;
    $payment->vendor_id = $vaPayment->id;
    $payment->vendor_type = 'va_mandiri';
    $this->payment->save($payment);

    return $listener->response(200, [
      'mandiri_payment_id' => $vaPayment->id,
      'payment_id' => $payment->id,
      'va_code' => $data['billKey1'],
      'va_prefix' => $pgUserChannel->prefix,
      'user_id' => $pgUserChannel->user_id,
      'payment_gateway_payment_id' => $payment->source_id,
      'billed_amount' => $data['paymentAmount'],
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'customer_name' => $inquiry->bill_info_1,
      'vendor_reference_id' => $data['transactionID'],
      'bill_name' => $inquiry->bill_name,
      'bill_info_1' => $inquiry->bill_info_1,
      'bill_info_2' => $inquiry->bill_info_2,
      'status_code' => '00'
    ]);
  }

  public function updatePayment(PipelineListener $listener, $data) {
    $vaPayment = $this->mandiriVaPayment->findById($data['mandiri_payment_id']);
    $vaPayment->payment_id = $data['payment_id'];
    $this->mandiriVaPayment->save($vaPayment);

    dispatch(new NotifyPaymentGatewayPaymentStatus($vaPayment, $vaPayment->transaction_id, 'mandiri_va'));

    return $listener->response(200);
  }

  public function doPayment(PipelineListener $listener, $data) {

    $payment = $this->mandiriVaPayment->getNew();
    $payment->language = $data['language'];
    $payment->trx_date_time = $data['trxDateTime'];
    $payment->transmission_date_time = $data['transmissionDateTime'];
    $payment->company_code = $data['companyCode'];
    $payment->channel_id = $data['channelID'];
    $payment->bill_key_1 = $data['billKey1'];
    $payment->bill_key_2 = $data['billKey2'];
    $payment->bill_key_3 = empty($data['billKey3']) ? '' : $data['billKey3'];
    $payment->paid_bills = $data['paidBills']['string'];
    $payment->payment_amount = $data['paymentAmount'];
    $payment->currency = $data['currency'];
    $payment->transaction_id = empty($data['transactionID']) ? '' : $data['transactionID'];
    $payment->reference_1 = empty($data['reference1']) ? '' : $data['reference1'];
    $payment->reference_2 = empty($data['reference2']) ? '' : $data['reference2'];
    $payment->reference_3 = empty($data['reference3']) ? '' : $data['reference3'];

    $this->mandiriVaPayment->save($payment);

    $phone = $this->helper->getPhoneNo($data['billKey1']);
    $prefix = $this->helper->getPrefix($data['billKey1']);
    $billName = $this->helper->getBillName($prefix);

    if ($billName == ''
      || $data['paymentAmount'] < 10000
      || !$this->helper->isOurCompany($data['billKey1'])
      || !($user = $this->users->findByTelephone(purifyTelephone($phone)))
    ) {
      return $listener->response(400);
    }

    $vaFee = MandiriHelper::getBillPaymentFee();

    $data = [
      'fee' => $vaFee,
      'auth' => [
        'user_id' => $user->id,
        'platform_id' => Platform::VIRTUAL_ACCOUNT,
      ],
      'skip_check_topup' => true,
      'gateway_id' => Payment::OPC_GROUP_VA_MANDIRI,
      'mandiri_bill_id' => $payment->id
    ];

    switch ($prefix) {

      case MandiriHelper::PREFIX_TOPUP_OCASH:

        $data = array_merge($data, [
          'cash' => [
            'amount' => $payment->payment_amount - $vaFee,
            'currency' => 'IDR'
          ],
          'service_detail_id' => ServiceDetail::TOPUP_ODEO,
          'bill_info_1' => 'Topup oCash ' . $phone,
          'bill_info_2' => $user->name,
          'bill_info_3' => 'Transaction Ref ' . $payment->transaction_id,
        ]);
        $listener->addNext(new Task(CartRemover::class, 'clear'));
        $listener->addNext(new Task(TopupRequester::class, 'request'));
        $listener->addNext(new Task(CartInserter::class, 'addToCart'));
        $listener->addNext(new Task(CartCheckouter::class, 'checkout'));
        $listener->addNext(new Task(MandiriDirectConfirmer::class, 'confirm'));
        break;

      default:
        break;
    }


    return $listener->response(200, $data);
  }

  private function getRedisNamespace($date) {
    return "odeo_core:mandiri_used_inquiry_id_$date";
  }

  private function getValue($val) {
    return empty($val) ? '' : $val;
  }

}