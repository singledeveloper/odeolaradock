<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 28/08/19
 * Time: 14.08
 */

namespace Odeo\Domains\Payment\Mandiri;


use Carbon\Carbon;
use Odeo\Domains\Constant\MandiriVaPaymentStatus;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Mandiri\Helper\MandiriManager;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;

class MandiriDirectReverser extends MandiriManager {

  public function __construct() {
    parent::__construct();
  }

  public function vaReverse(PipelineListener $listener, $data) {
    $vaPayment = $this->mandiriVaPayment->findByBillKey1AndTrxId($data['billKey1'], $data['transactionID']);

    if (!$vaPayment) {
      return $listener->response(400, ['status_code' => '01']);
    }

    if (isProduction()) {
      return $listener->response(400, ['status_code' => '86']);
    }

    if (in_array($vaPayment->status, [
      MandiriVaPaymentStatus::FAILED,
      MandiriVaPaymentStatus::FAILED_PAYMENT_REVERSAL,
      MandiriVaPaymentStatus::SUSPECT
    ])) {
      return $listener->response(400, ['status_code' => '86']);
    }

    $vaPayment->status = MandiriVaPaymentStatus::FAILED_PAYMENT_REVERSAL;
    $this->mandiriVaPayment->save($vaPayment);

    $listener->addNext(new Task(PaymentGatewayNotifier::class, 'notifyPaymentStatus', [
      'pg_payment_id' => $vaPayment->payment->source_id,
      'pg_payment_status' => MandiriVaPaymentStatus::getPaymentGatewayStatus($vaPayment->status),
      'receive_notify_time' => Carbon::now(),
      'vendor_reference_id' => $vaPayment->trace_number
    ]));

    return $listener->response(200);
  }

}