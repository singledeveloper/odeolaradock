<?php

namespace Odeo\Domains\Payment\Mandiri;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Core\PipelineListener;

class MandiriTester {

  public function testInquiry(PipelineListener $listener, $data) {
    $redis = Redis::connection();
    $vaCode = $data['billKey1'];

    if ($redis->get('odeo-core:mandiri_va_test_' . $vaCode)) {
      return $listener->response(400, ['status_code' => 'B8', 'message' => 'bill already paid']);
    }

    switch($vaCode) {
      case '899100100011':
      case '899100100012':
      case '899100100013':
      case '899100100014':
      case '899100100015':
        return $listener->response(200);
      case '899100200011':
      default:
        return $listener->response(400, ['status_code' => 'B5', 'message' => 'bill not found']);

    }
  }

  public function testPayment(PipelineListener $listener, $data) {
    $redis = Redis::connection();
    $vaCode = $data['billKey1'];

    switch($vaCode) {
      case '899100100011':
        $redis->set('odeo-core:mandiri_va_test_' . $vaCode, 1);
        return $listener->response(200);
      case '899100100014':
        return $listener->response(400, ['status_code' => '01', 'message' => 'general error']);
      case '899100100015':
        sleep(30);
        return $listener->response(400, ['status_code' => '89', 'message' => 'timeout']);
      case '899100100012':
      case '899100100013':
      default:
        return $listener->response(400, ['status_code' => 'B5', 'message' => 'bill not found']);
    }
  }
}