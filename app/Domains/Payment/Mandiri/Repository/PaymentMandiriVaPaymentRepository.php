<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/04/19
 * Time: 17.55
 */

namespace Odeo\Domains\Payment\Mandiri\Repository;


use Odeo\Domains\Constant\MandiriVaPaymentStatus;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Mandiri\Model\PaymentMandiriVaPayment;

class PaymentMandiriVaPaymentRepository extends Repository {

  function __construct(PaymentMandiriVaPayment $billPayment) {
    $this->model = $billPayment;
  }

  function findByTransactionId($transactionId) {
    return $this->model
      ->where('transaction_id', '=', $transactionId)
      ->first();
  }

  function findByBillKey1AndTrxId($billKey1, $transactionId) {
    return $this->model
      ->where('bill_key_1', $billKey1)
      ->where('transaction_id', $transactionId)
      ->first();
  }

  function findLatestBillKey1Payment($billKey1) {
    return $this->model
      ->where('bill_key_1', $billKey1)
      ->whereDate('created_at', date('Y-m-d'))
      ->first();
  }

  function getPendingPayment() {
    return $this->model->whereIn('status', [
      MandiriVaPaymentStatus::ACCEPTED,
      MandiriVaPaymentStatus::PENDING,
      MandiriVaPaymentStatus::FAILED_NEED_UPDATE,
      MandiriVaPaymentStatus::SUSPECT_FOR_TESTING,
      MandiriVaPaymentStatus::SUSPECT_TIMEOUT,
      MandiriVaPaymentStatus::SUSPECT_NEED_STATUS_RECHECK,
      MandiriVaPaymentStatus::SUSPECT_IS_CANCELLED_BUT_NEED_STATUS_RECHECK
    ])->get();
  }

}