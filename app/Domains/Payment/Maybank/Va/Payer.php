<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/10/19
 * Time: 10.52
 */

namespace Odeo\Domains\Payment\Maybank\Va;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Maybank\Va\Repository\PaymentMaybankVaInquiryRepository;
use Odeo\Domains\Payment\Maybank\Va\Repository\PaymentMaybankVaPaymentRepository;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository;
use Odeo\Domains\Payment\Repository\PaymentRepository;
use Odeo\Domains\PaymentGateway\Jobs\NotifyPaymentGatewayPaymentStatus;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;

class Payer {

  function __construct() {
    $this->paymentRepo = app()->make(PaymentRepository::class);
    $this->maybankVaInquiry = app()->make(PaymentMaybankVaInquiryRepository::class);
    $this->maybankVaPayment = app()->make(PaymentMaybankVaPaymentRepository::class);
    $this->paymentChannelRepo = app()->make(PaymentOdeoPaymentChannelRepository::class);
    $this->pgUserChannelRepo = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
    $this->odeoPaymentChannel = app()->make(PaymentOdeoPaymentChannelRepository::class);
    $this->redis = Redis::connection();
  }

  public function vaPayment(PipelineListener $listener, $data) {

    $inquiry = $this->maybankVaInquiry->findByVaNo($data['vaNo'], $data['reference']);
    if (!$inquiry
//      || $inquiry->created_at->diffInMinutes(Carbon::now()) > 5
    ) {
      \Log::info(json_encode($inquiry));
      return $listener->response(400, ['message' => 'Inquiry not found', 'code' => '14']);
    }

    if ($inquiry->amount != $data['amount']) {
      return $listener->response(400, ['message' => 'Invalid payment amount', 'code' => '13']);
    }

    $duplicatePayment = $this->maybankVaPayment->findByVaNo($data['vaNo']);
    if ($duplicatePayment) {
      return $listener->response(400, ['message' => 'Bill Already Paid', 'code' => '88']);
    }

    $currDate = Carbon::now()->format('Y-m-d');
    $namespace = $this->getRedisNamespace($currDate);
    if (!$this->redis->hsetnx($namespace, $inquiry->id, 1)) {
      return $listener->response(400, ['message' => 'Duplicate Transaction', 'code' => '88']);
    }

    $this->redis->expire($namespace, 2 * 24 * 60 * 60);
    $pgUserChannel = $this->pgUserChannelRepo->findByActivePrefix($data['vaNo']);
    if (!$pgUserChannel) {
      return $listener->response(400, ['message' => trans('payment.unrecognized_va_number'), 'code' => '14']);
    }

    $channel = $this->odeoPaymentChannel->findByGatewayIdAndGroupId(Payment::PAYMENT_GATEWAY, $pgUserChannel->payment_group_id);
    if (!$channel) {
      return $listener->response(400, ['message' => trans('payment.unrecognized_va_number'), 'code' => '14']);
    }

    $vaPayment = $this->maybankVaPayment->getNew();
    $vaPayment->inquiry_id = $inquiry->id;
    $vaPayment->bin_number = $data['binNumber'];
    $vaPayment->va_no = $data['vaNo'];
    $vaPayment->amount = $data['amount'];
    $vaPayment->currency = $data['currency'];
    $vaPayment->delivery_channel_type = $data['deliveryChannelType'];
    $vaPayment->trace_no = $data['traceNo'];
    $vaPayment->transmission_date_time = $data['transmissionDateTime'];
    $vaPayment->terminal_id = $data['terminalId'];
    $vaPayment->reference = $data['reference'];
    $vaPayment->status = PaymentGateway::ACCEPTED;
    $this->maybankVaPayment->save($vaPayment);

    $payment = $this->paymentRepo->getNew();
    $payment->source_type = PaymentChannel::PAYMENT_GATEWAY;
    $payment->info_id = $pgUserChannel->payment_group_id;
    $payment->opc = $channel->code;
    $payment->vendor_id = $vaPayment->id;
    $payment->vendor_type = 'va_maybank';
    $this->paymentRepo->save($payment);

    return $listener->response(200, [
      'maybank_payment_id' => $vaPayment->id,
      'payment_id' => $payment->id,
      'va_code' => $data['vaNo'],
      'va_prefix' => $pgUserChannel->prefix,
      'user_id' => $pgUserChannel->user_id,
      'billed_amount' => $data['amount'],
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'vendor_reference_id' => $payment->reference
    ]);

  }

  private function getRedisNameSpace($date) {
    return "odeo_core:maybank_used_inquiry_id_$date";
  }

  public function updatePayment(PipelineListener $listener, $data) {
    $vaPayment = $this->maybankVaPayment->findById($data['maybank_payment_id']);
    $vaPayment->payment_id = $data['payment_id'];
    $this->maybankVaPayment->save($vaPayment);

    dispatch(new NotifyPaymentGatewayPaymentStatus($vaPayment, $vaPayment->reference, 'maybank_va'));

    return $listener->response(200);
  }
}