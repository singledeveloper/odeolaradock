<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/10/19
 * Time: 10.53
 */

namespace Odeo\Domains\Payment\Maybank\Va\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Maybank\Va\Model\PaymentMaybankCallbackLog;

class PaymentMaybankCallbackLogRepository extends Repository {

  function __construct(PaymentMaybankCallbackLog $callbackLog) {
    $this->model = $callbackLog;
  }

}