<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 9:32 PM
 */

namespace Odeo\Domains\Payment\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\Order\Model\Order;
use Odeo\Domains\Payment\Odeo\BankTransfer\Model\PaymentOdeoBankTransferDetail;

class Payment extends Entity {

  public function channel() {
    return $this->belongsTo(PaymentOdeoPaymentChannel::class, 'opc', 'code');
  }

  public function information() {
    return $this->belongsTo(PaymentOdeoPaymentChannelInformation::class, 'info_id');
  }

  public function bankTransferDetail() {
    return $this->belongsTo(PaymentOdeoBankTransferDetail::class, 'reference_id', 'id');
  }

  public function order() {
    return $this->belongsTo(Order::class);
  }

}