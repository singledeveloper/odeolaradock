<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 9:33 PM
 */

namespace Odeo\Domains\Payment\Model;


use Odeo\Domains\Core\Entity;

class PaymentOdeoPaymentChannelInformation extends Entity {

  public function channels() {
    return $this->hasMany(PaymentOdeoPaymentChannel::class, 'info_id', 'id');
  }

}