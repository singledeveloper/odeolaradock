<?php

namespace Odeo\Domains\Payment\Model;

use Odeo\Domains\Core\Entity;

class PaymentSettlementDetail extends Entity {

  public function info() {
    return $this->belongsTo(PaymentSettlementDetail::class, 'payment_settlement_id');
  }

}