<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 9:32 PM
 */

namespace Odeo\Domains\Payment\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class PaymentUserDirectPayment extends Entity {

  use SoftDeletes;

  public function user() {
    $this->belongsTo(User::class);
  }

  public function information() {
    $this->belongsTo(PaymentOdeoPaymentChannelInformation::class);
  }

}