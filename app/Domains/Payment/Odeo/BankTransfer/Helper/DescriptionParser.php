<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Helper;

use Odeo\Domains\Constant\Payment;

class DescriptionParser {

  public function getUserName($string, $infoId) {
    try {
      switch($infoId) {
        case Payment::OPC_GROUP_BANK_TRANSFER_BCA:
          preg_match("/(([A-Z.]+\s)*[A-Z.]+)$/", $string, $matches);
          return trim($matches[0]);
        case Payment::OPC_GROUP_BANK_TRANSFER_MANDIRI:
          preg_match("/DARI (([A-Z.]+\s)*[A-Z.]+)$/", $string, $matches);
          if (count($matches) > 0) return trim($matches[1]);
          preg_match("/DARI (([A-Z.]+\s)*(?=(SA|Transfer)))/", $string, $matches);
          return trim($matches[1]);
        case Payment::OPC_GROUP_BANK_TRANSFER_BRI:
          preg_match("/\s([A-Z.]+\s?)*(?=\s+TO)/", $string, $matches);
          return trim($matches[0]);
        case Payment::OPC_GROUP_BANK_TRANSFER_CIMB:
          preg_match("/\s([A-Z.]+\s?)*(?=\s)/", $string, $matches);
          return trim($matches[0]);
        case Payment::OPC_GROUP_BANK_TRANSFER_BNI:
          preg_match("/(Sdri\s?|Sdr\s?|Ibu\s?|Bpk\s?)([A-Z ]*)\|?.*$/", $string, $matches);
          return trim($matches[2]);
        case Payment::OPC_GROUP_BANK_TRANSFER_PERMATA:
          preg_match("/TRF DARI\s([A-Z ]*)\s(BANK|B|ATM)\s/", $string, $matches);
          return trim($matches[1]);
      }
    }
    catch(\Exception $e) {
      return '';
    }
  }

}
