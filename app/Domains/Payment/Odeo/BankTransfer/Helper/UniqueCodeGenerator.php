<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/1/17
 * Time: 4:20 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Helper;


class UniqueCodeGenerator {

  private $configs, $codes;

  public function __construct() {
    $this->configs = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Repository\PaymentOdeoBankTransferCodeConfigRepository::class);
    $this->codes = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Repository\PaymentOdeoBankTransferCodeRepository::class);
  }

  public function generate($order, $amount, $bank) {

    $uniqueCode = null;
    $code = null;

    $config = $this->configs->getConfig($amount, $bank);

    if (!$config) {

      $config = $this->configs->getNew();

      $config->amount = $amount;
      $config->bank = strtolower($bank);
      $config->increment = 0;
      $config->increment_amplifier = 1;

    }

    $countTried = 0;

    if (
      ($tCode = $this->codes->getAnExpiredCode($amount, $bank)) &&
      !$this->codes->isUsed($amount + $tCode->code, $bank)
    ) {

      $uniqueCode = $tCode->code;
      $tCode->order_id = $order->id;
      $tCode->created_at = $order->created_at;
      $tCode->updated_at = $order->updated_at;
      $tCode->expired_at = $order->expired_at;

      $this->codes->save($tCode);

    } else {

      do {

        $isNotAvailable = false;

        do {

          if ($config->increment == 999) {

            $config->increment = 0;
            $config->increment_amplifier = 1;

          }

          $uniqueCode = $config->increment + $config->increment_amplifier;
          $config->increment = $uniqueCode;

        } while (($amount + $uniqueCode) % 10 == 0);


        if ($this->codes->isUsed($amount + $uniqueCode, $bank)) {
          $isNotAvailable = true;
          ++$countTried;
        }

        if ($countTried > 30) {
          $this->configs->save($config);
          throw new \Exception('unique code has reached maximum for amount: ' . $amount . ' for bank ' . $bank);
        }

      } while ($isNotAvailable);


      $this->configs->save($config);

      $tCode = $this->codes->getNew();


      $tCode->code = $uniqueCode;
      $tCode->amount = $amount;
      $tCode->total = $amount + $uniqueCode;
      $tCode->bank = $bank;
      $tCode->config_id = $config->id;
      $tCode->order_id = $order->id;
      $tCode->created_at = $order->created_at;
      $tCode->updated_at = $order->updated_at;
      $tCode->expired_at = $order->expired_at;

      $this->codes->save($tCode);

    }


    return $uniqueCode;

  }


}