<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/1/17
 * Time: 4:20 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Helper;


use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;

class UniqueCodeRefunder {

  private $orderCharges, $users, $inquiries;

  public function __construct() {
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->orderCharges = app()->make(\Odeo\Domains\Order\Repository\OrderChargeRepository::class);
    $this->inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
  }

  public function refund(PipelineListener $listener, $data) {

    if (isset($data['skip_unique_code_refund'])) return $listener->response(200);

    $order = $data['order'];
    $payment = $data['payment'];

    $transferredAmount = (function () use ($payment, $order) {

      $this->inquiries = Payment::getBankInquiryRepository($payment->info_id);
      
      $inquiry = $this->inquiries->findByOrderId($order->id);

      if (!$inquiry && $payment->info_id == Payment::OPC_GROUP_BANK_TRANSFER_MANDIRI) {
        $bankInquiries = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository::class);
        $inquiry = $bankInquiries->findByOrderId($order->id);
      }

      if ($this->inquiries && $inquiry) {
        return $inquiry->credit;
      }
      return $order->total;

    })();

    $charge = $this->orderCharges->getExisting([
      'type' => OrderCharge::UNIQUE_CODE,
      'group_type' => OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER,
      'order_id' => $order->id
    ])->first();

    $totalWithoutUniqueCode = $order->total - $charge->amount;

    $rest = $transferredAmount - $totalWithoutUniqueCode;

    $amountTobeRefunded = min(max($rest, 0), $charge->amount);

    $user = $this->users->findById($order->user_id);

    if ($user->type != 'seller' && $order->actual_store_id) {

      $this->_refundToStoreOwner($amountTobeRefunded, $order);

    } else {
      $this->_refundToCustomer($amountTobeRefunded, $order);
    }

    return $listener->response(200);

  }

  private function _refundToStoreOwner($amountTobeRefunded, $order) {

    if(!$amountTobeRefunded) return;

    $userStores = app()->make(\Odeo\Domains\Subscription\Repository\UserStoreRepository::class);

    $store = $userStores->findOwner($order->actual_store_id)->first();

    $this->inserter->add([
      'user_id' => $store->user_id,
      'trx_type' => TransactionType::UNIQUE_CODE_REFUND_BY_CUSTOMER,
      'cash_type' => CashType::OCASH,
      'amount' => $amountTobeRefunded,
      'data' => json_encode([
        'order_id' => $order->id,
      ])
    ]);

    $this->inserter->run();

  }

  private function _refundToCustomer($amountTobeRefunded, $order) {

    if(!$amountTobeRefunded) return;

    $this->inserter->add([
      'user_id' => $order->user_id,
      'trx_type' => TransactionType::UNIQUE_CODE_REFUND,
      'cash_type' => CashType::OCASH,
      'amount' => $amountTobeRefunded,
      'data' => json_encode([
        'order_id' => $order->id,
      ])
    ]);

    $this->inserter->run();

  }

}
