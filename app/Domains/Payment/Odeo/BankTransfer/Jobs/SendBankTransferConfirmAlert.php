<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 5:23 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendBankTransferConfirmAlert extends Job  {

  use SerializesModels;

  public function __construct() {
    parent::__construct();
  }

  public function handle() {

    $orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class)->getConfirmedAlert();

    if (!($count = $orders->count())) {
      return;
    }

    if (app()->environment() == 'production') {

      Mail::send('emails.payment_bank_transfer_not_verified', [
        'data' => $orders,
        'count' => $count
      ], function ($m) {

        $m->from('noreply@odeo.co.id', 'odeo');

        $m->to('ops@odeo.co.id')->subject('Confirmed Order - ' . time());
      });

    }


  }


}
