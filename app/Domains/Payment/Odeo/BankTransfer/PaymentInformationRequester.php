<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:29 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer;


use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Helper\PaymentManager;

class PaymentInformationRequester extends PaymentManager {
//  use EmailHelper;

  private $uniqueCodeGenerator, $bankTransferTnc;

  public function __construct() {

    parent::__construct();

    $this->uniqueCodeGenerator = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Helper\UniqueCodeGenerator::class);
    $this->bankTransferTnc = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Helper\BankTransferTnc::class);
  }

  private function isOperational($paymentGroupId) {
    switch ($paymentGroupId) {
      case Payment::OPC_GROUP_BANK_TRANSFER_BCA && BcaDisbursement::isOperational(1, 20, 30):
        return true;
        break;
      case Payment::OPC_GROUP_BANK_TRANSFER_BRI && BcaDisbursement::isOperational(6, 22, 30):
        return true;
        break;
      case Payment::OPC_GROUP_BANK_TRANSFER_BNI:
        return true;
        break;
      case Payment::OPC_GROUP_BANK_TRANSFER_MANDIRI && BcaDisbursement::isOperational(3, 22, 0):
        return true;
        break;
      default:
        return false;
    }
  }

  public function request(PipelineListener $listener, $order, $paymentInformation, $data) {
    if ($order->status != OrderStatus::OPENED && (!isset($data['auth']) || $data['auth']['platform_id'] != Platform::ADMIN)) {
      if (!$this->isOperational($paymentInformation->info_id)) {
        return $listener->response(400, 'Pembayaran ' . $paymentInformation->information->name . ' tidak dapat digunakan sekarang. Mohon coba lagi besok.');
      }
    }

    if ($order->status == OrderStatus::CREATED) {
      try {
        $uniqueCode = $this->uniqueCodeGenerator->generate($order, $order->total, str_replace('bank transfer ', '', (function () use ($paymentInformation) {
          switch ($paymentInformation->info_id) {
            case Payment::OPC_GROUP_BANK_TRANSFER_BCA:
              return 'bca';
            case Payment::OPC_GROUP_BANK_TRANSFER_MANDIRI:
              return 'mandiri';
            case Payment::OPC_GROUP_BANK_TRANSFER_BNI:
              return 'bni';
            case Payment::OPC_GROUP_BANK_TRANSFER_BRI:
              return 'bri';
            case Payment::OPC_GROUP_BANK_TRANSFER_CIMB:
              return 'cimb';
          }
          return '';

        })()));

      } catch (\Exception $e) {

        return $listener->response(400);

      }

      $this->charge($order->id, OrderCharge::UNIQUE_CODE, $uniqueCode, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

      $order->total = $order->total + $uniqueCode;

      if (isset($data['instant_open']) && $data['instant_open']) {

        $order->status = OrderStatus::OPENED;
        $order->opened_at = date('Y-m-d H:i:s');

//        $email = $this->getEmail($order, $data);
//        $listener->addNext(new Task(CustomerEmailManager::class, 'pushJobIfEmailVerified', [
//          'email' => $email,
//          'job' => new SendPaymentInvoice($order, $paymentInformation, $data)
//        ]));

      }

      $this->orders->save($order);

    }

    $information = $paymentInformation->information;

    $detail = \json_decode($information->detail);

    $detail->logo = $information->logo;
    $detail->name = $information->name;

    return $listener->response(200, [
      'type' => Payment::REQUEST_PAYMENT_TYPE_SHOW_DETAILS,
      'tnc' => $this->bankTransferTnc->getTnc($paymentInformation->info_id),
      'content' => $detail,
      'total' => $order->total,
      'expired_at' => $order->expired_at
    ]);

  }


}
