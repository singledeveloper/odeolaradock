<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 11:54 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Model\PaymentOdeoBankTransferCodeConfig;

class PaymentOdeoBankTransferCodeConfigRepository extends Repository {


  public function __construct(PaymentOdeoBankTransferCodeConfig $bankTransferCodeConfig) {
    $this->model = $bankTransferCodeConfig;
  }

  public function getConfig($amount, $bank) {
    return $this->model
      ->where('amount', $amount)
      ->where('bank', $bank)
      ->first();
  }

}