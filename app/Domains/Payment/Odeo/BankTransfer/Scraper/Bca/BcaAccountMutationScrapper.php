<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/22/16
 * Time: 11:02 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca;


use Facebook\WebDriver\Exception\ElementNotVisibleException;
use Facebook\WebDriver\Exception\UnexpectedJavascriptException;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Carbon\Carbon;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Helper\ActivatorMarker;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeAccount;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeAccountNumber;
use DB;


class BcaAccountMutationScrapper {

  use ActivatorMarker;

  private $bcaInquiryRepository, $bankScrapeAccountNumberRepository;

  public function __construct() {
    $this->bcaInquiryRepository = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository::class);
    $this->bankScrapeAccountNumberRepository = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository::class);
  }

  public function _goToMutation(RemoteWebDriver $driver) {
    $driver->switchTo()->defaultContent();

    $driver->switchTo()->frame($driver->findElement(WebDriverBy::cssSelector('frameset frameset frame[name="leftFrame"]')));

    $isNotVisible = null;

    do {

      $driver->findElement(WebDriverBy::cssSelector('#divFold0 > a'))
        ->click();

      try {

        $driver->findElement(WebDriverBy::cssSelector('#divFoldSub0_1 > a:nth-child(1)'))
          ->click();

        $isNotVisible = false;

      } catch (ElementNotVisibleException $exception) {

        $isNotVisible = true;

      }
    } while ($isNotVisible);

  }

  public function _fillMutationForm(RemoteWebDriver $driver, BankScrapeAccountNumber $acc, Carbon $lastScrapedDate, Carbon $targetMaxScrapedDate) {

    $this->markAsActive('bca');

    $driver->switchTo()->defaultContent();

    $driver->switchTo()->frame($driver->findElement(WebDriverBy::cssSelector('html > frameset > frameset > frame:nth-child(3)')));

    $driver->findElement(WebDriverBy::cssSelector('body > form > table > tbody > tr:nth-child(2) > td:nth-child(2) > a:nth-child(11)'))->click();

    sleep(2);

    $driver->findElement(WebDriverBy::cssSelector('input[name="accountList"]'))->click();

    $driver->findElement(WebDriverBy::cssSelector('#Add'))->click();

    $driver->findElement(WebDriverBy::cssSelector("#from_day > option[value='" . sprintf('%02d', $lastScrapedDate->day) . "'"))->click();
    $driver->findElement(WebDriverBy::cssSelector("#from_mth > option[value='" . $lastScrapedDate->month . "'"))->click();
    $driver->findElement(WebDriverBy::cssSelector("#from_year > option[value='" . $lastScrapedDate->year . "'"))->click();

    $driver->findElement(WebDriverBy::cssSelector("#to_day > option[value='" . sprintf('%02d', $targetMaxScrapedDate->day) . "'"))->click();
    $driver->findElement(WebDriverBy::cssSelector("#to_mth > option[value='" . $targetMaxScrapedDate->month . "'"))->click();
    $driver->findElement(WebDriverBy::cssSelector("#to_year > option[value='" . $targetMaxScrapedDate->year . "'"))->click();

    $acc->last_time_scrapped = Carbon::now();
    $this->bankScrapeAccountNumberRepository->save($acc);

    $driver->findElement(WebDriverBy::cssSelector('#Submit'))->click();

    sleep(2);

    try {

      $driver = $driver->findElement(WebDriverBy::cssSelector('body h3.clsErrorMsg'));


      if ($driver->getText() == 'Tidak ada transaksi') {

        $acc->last_scrapped_date = $targetMaxScrapedDate;

        $this->bankScrapeAccountNumberRepository->save($acc);

        return -1;
      }


    } catch (\Exception $exception) {
    }


    return 1;


  }

  public function _scrapMutation(RemoteWebDriver $driver, BankScrapeAccountNumber $acc, Carbon $lastScrapedDate, Carbon $targetMaxScrapedDate) {

    $scrappedData = [];

    $now = Carbon::now();

    $hasMetPendingStatus = false;
    $pendingScrapedDate = $lastScrapedDate;


    $pendingInquiries = $this->bcaInquiryRepository->getPendingInquiry();
    $shouldCheckInquiriesCount = $pendingInquiries->count();
    $shouldCheckInquiriesPointer = 0;
    $shouldCheckInquiries = [];


    if ($shouldCheckInquiriesCount) {
      $shouldCheckInquiries = $pendingInquiries;
    } else if ($count = $acc->bcaInquiries()->count()) {
      $shouldCheckInquiriesCount = $count;
      $shouldCheckInquiries[0] = $acc->bcaInquiries()->orderBy('date', 'desc')->first();
    }

    $abortAndSave = false;
    $isEndOfScrapping = false;

    do {

      $driver->switchTo()->defaultContent();

      $driver->switchTo()->frame($driver->findElement(WebDriverBy::cssSelector('html > frameset > frameset > frame:nth-child(3)')));

      $tableRow = $driver->findElements(WebDriverBy::cssSelector('body > form > table.clsForm > tbody > tr'));


      for ($i = 1, $length = count($tableRow); $i < $length; $i++) {

        $tableCell = $tableRow[$i]->findElements(WebDriverBy::cssSelector('td'));

        $text = [];

        for ($j = 0, $lengthj = count($tableCell); $j < $lengthj; $j++) {
          $text[$j] = $this->_clearFormattingText($tableCell[$j]->getText());
        }


        if ($text[0] == 'Tidak ada data' || starts_with($text[0], 'Saldo Awal')) {
          $abortAndSave = true;
          break;
        }

        $current_date = $text[0];

        if ($current_date != 'PEND') {

          $current_date = Carbon::createFromFormat('d/m', $text[0]);

          $current_date->year = $now->year;
        }


//        if ($shouldCheckInquiriesCount) {
//
//          $inquiry = $shouldCheckInquiries[$shouldCheckInquiriesPointer];
//          if ($current_date != 'PEND' && $inquiry->date != 'PEND' && $current_date->lt(Carbon::parse($inquiry->date))) {
//            continue;
//          } else if ($inquiry->bank_scrape_account_number_id == $acc->id &&
//            $inquiry->description == $text[1] &&
//            $inquiry->branch == $text[2] &&
//            $inquiry->amount == $text[3] &&
//            $inquiry->balance == $text[4]
//          ) {
//
//            if ($inquiry->date == 'PEND' && $inquiry->date != $current_date) {
//              $inquiry->date = $current_date->toDateString();
//              $inquiry->pending_scrapped_date = null;
//              $this->bcaInquiryRepository->save($inquiry);
//
//              if ($shouldCheckInquiriesPointer + 1 < $shouldCheckInquiriesCount) {
//                $shouldCheckInquiriesPointer++;
//              }
//
//              continue;
//
//            } else if ($inquiry->date == $text[0]) {
//
//              if ($shouldCheckInquiriesPointer + 1 < $shouldCheckInquiriesCount) {
//                $shouldCheckInquiriesPointer++;
//              }
//
//              continue;
//            }
//
//          }
//        }


        $temp = [
          'bank_scrape_account_number_id' => $acc->id,
          'date' => $current_date == 'PEND' ? 'PEND' : $current_date->toDateString(),
          //pending_scrapped_date' => $current_date == 'PEND' ? $lastScrapedDate->toDateString() : null,
          'description' => $text[1],
          'branch' => $text[2],
          'amount' => $text[3],
          'balance' => $text[4],
          "created_at" => $now,
          "updated_at" => $now,
        ];

        if ($this->bcaInquiryRepository->findIdentical($temp)) {
          continue;
        }

        $temp = [
          'bank_scrape_account_number_id' => $acc->id,
          //'date' => $current_date == 'PEND' ? 'PEND' : $current_date->toDateString(),
          //pending_scrapped_date' => $current_date == 'PEND' ? $lastScrapedDate->toDateString() : null,
          'description' => $text[1],
          'branch' => $text[2],
          'amount' => $text[3],
          'balance' => $text[4],
          "created_at" => $now,
          "updated_at" => $now,
        ];


        $skip = false;

        if ($this->bcaInquiryRepository->findIdentical($temp)) {

          foreach ($shouldCheckInquiries as $inquiry) {

            if ($current_date != 'PEND' && $inquiry->date != 'PEND' && $current_date->lt(Carbon::parse($inquiry->date))) {
              continue;
            } else if ($inquiry->bank_scrape_account_number_id == $acc->id &&
              $inquiry->description == $text[1] &&
              $inquiry->branch == $text[2] &&
              $inquiry->amount == $text[3] &&
              $inquiry->balance == $text[4]
            ) {

              if ($inquiry->date == 'PEND' && $inquiry->date != $current_date) {
                $inquiry->date = $current_date->toDateString();
                $inquiry->pending_scrapped_date = null;
                $this->bcaInquiryRepository->save($inquiry);
                $skip = true;
                break;
              }

            }

          }

          if ($skip) continue;

        }


        if (!$hasMetPendingStatus && $current_date != 'PEND') {
          $pendingScrapedDate = $current_date;
        } else {
          $hasMetPendingStatus = true;
        }


        $scrappedData[] = [
          'bank_scrape_account_number_id' => $acc->id,
          'date' => $current_date == 'PEND' ? 'PEND' : $current_date->toDateString(),
          'pending_scrapped_date' => $current_date == 'PEND' ? $pendingScrapedDate->toDateString() : null,
          'description' => $text[1],
          'branch' => $text[2],
          'amount' => $text[3],
          'balance' => $text[4],
          "created_at" => $now,
          "updated_at" => $now,
        ];

      }

      if ($abortAndSave) {
        break;
      }

      try {

        $driver->executeScript("Javascript:reset_session_timer();", array());
        $driver->executeScript("blnSubmitted = true;", array());
        $driver->executeScript("document.frmParam.PgIndex.value = parseInt(document.frmParam.PgIndex.value) + 1;", array());
        $driver->executeScript("CSCOPut_action(document.frmParam,\"ap_CorpAcctTrxn_detail.jsp\");", array());
        $driver->executeScript("document.frmParam.submit();", array());

        sleep(2);

        $isLoop = true;

      } catch (UnexpectedJavascriptException $exception) {
        $isLoop = false;
      }


    } while ($isLoop);

    if (count($scrappedData)) {
      $this->bcaInquiryRepository->saveBulk($scrappedData);
    }

    $acc->last_scrapped_date = $targetMaxScrapedDate;


    $this->bankScrapeAccountNumberRepository->save($acc);

  }

  public function _clearFormattingText($text) {
    return trim($text);
  }

  public function syncMutation(RemoteWebDriver $driver, BankScrapeAccount $account) {


    for ($i = 0; ;) {

      DB::beginTransaction();


      $lastScrapedDate = $this->_getLastScrappedDate($account->numbers[$i]);
      $targetMaxScrapedDate = Carbon::now();


      $this->_goToMutation($driver);


      if ($this->_fillMutationForm($driver, $account->numbers[$i], $lastScrapedDate, $targetMaxScrapedDate) == -1) {
        sleep(5);
      } else {
        $this->_scrapMutation($driver, $account->numbers[$i], $lastScrapedDate, $targetMaxScrapedDate);
      }

//      \Log::info('commit start');
//
//
//      \Log::info('commit start');

      if (!DB::commit()) {
        //\Log::info('commit gagal');
      }


    }
  }

  private function _getLastScrappedDate(BankScrapeAccountNumber $acc) {

    $inquiries = $this->bcaInquiryRepository->getPendingInquiry();

    $lastScrapedDate = $acc->last_scrapped_date;

    if ($inquiries->count()) {
      return Carbon::parse($inquiries[0]->pending_scrapped_date);
    }

    return $lastScrapedDate;
  }


}