<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/22/16
 * Time: 10:51 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Helper\TokenManager;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeAccount;
use PHPUnit_Framework_TestCase;


class BcaLoginner extends PHPUnit_Framework_TestCase {

  protected $driver;
  protected $tokenManager;


  public function login(RemoteWebDriver $driver, BankScrapeAccount $acc) {
    $account = json_decode($acc->account);

    if (!$this->tokenManager) {
      $this->tokenManager = app()->make(TokenManager::class);
    }

    $driver->get('https://vpn.tarumanagara.com/+CSCOE+/logon.html');

    $driver->findElement(WebDriverBy::name('username'))
      ->sendKeys($account->cooperate_id . $account->user_id);

    $driver->findElement(WebDriverBy::name('password'))
      ->sendKeys($this->tokenManager->appli1());


    $driver->findElement(WebDriverBy::cssSelector('#form_table > tbody > tr:nth-child(2) > td > div > table > tbody > tr:nth-child(4) > td > a'))->click();

    sleep(3);

    $driver->findElement(WebDriverBy::id('menu-1'))->click();

    $driver->findElement(WebDriverBy::name('corp_cd'))
      ->sendKeys($account->cooperate_id);
    $driver->findElement(WebDriverBy::name('user_cd'))
      ->sendKeys($account->user_id);
    $driver->findElement(WebDriverBy::name('pswd'))
      ->sendKeys($this->tokenManager->appli1());

    sleep(1);

    $driver->findElement(WebDriverBy::cssSelector('body > form > table > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(10) > td > a'))
      ->click();

    sleep(2);

}


public function logout(RemoteWebDriver $driver) {
  $driver->switchTo()->defaultContent();

  $driver->switchTo()->frame($driver->findElement(WebDriverBy::cssSelector('html > frameset > frame:nth-child(1)')));

  $driver->findElement(WebDriverBy::cssSelector('body > table:nth-child(2) > tbody > tr > td:nth-child(1) > a'))
    ->click();

  sleep(1);

  $driver->navigate()->to('https://vpn.tarumanagara.com/+CSCOE+/logon.html');

  sleep(1);
}


}
