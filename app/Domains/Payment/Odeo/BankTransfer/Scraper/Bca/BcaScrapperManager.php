<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca;

use DB;
use Facebook\WebDriver\Exception\UnexpectedAlertOpenException;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Database\QueryException;
use LogicException;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Helper\ActivatorMarker;


class BcaScrapperManager {

  use ActivatorMarker;

  public static $driver;
  public static $information;

  private $bcaLoginner, $bcaAccountMutationScrapper, $bankScrapeInformationRepository;


  public function __construct() {

    $this->bcaLoginner = app()->make(BcaLoginner::class);
    $this->bcaAccountMutationScrapper = app()->make(BcaAccountMutationScrapper::class);
    $this->bankScrapeInformationRepository = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeInformationRepository::class);
  }


  public function initialize() {
    self::$information = $this->bankScrapeInformationRepository->getActiveAccount('bca');

    set_time_limit(0);

  }

  public function run() {

    $this->initialize();

    $driver = RemoteWebDriver::create('http://localhost:3333/wd/hub', DesiredCapabilities::chrome(), 600 * 1000, 600 * 1000);
    $driver->manage()->timeouts()->implicitlyWait(60);
    $driver->get(self::$information->url);

    $pointer = 0;
    $length = self::$information->accounts->count();
    $isFirstRound = true;


    while (true) {

      try {

        try {

          if ($isFirstRound) $this->bcaLoginner->login($driver, self::$information->accounts[$pointer]);

          $this->bcaAccountMutationScrapper->syncMutation($driver, self::$information->accounts[$pointer]);

          if ($length > 1) {
            $this->bcaLoginner->logout($driver);
          } else {
            $isFirstRound = false;
          }

          if ($pointer == $length - 1) {
            $pointer = 0;
          }

          sleep(3);


        } catch (LogicException $exception) {

          \Log::info($exception->getMessage());


          \DB::purge();
          \DB::connection();

          \Log::info($exception->getMessage());
          \Log::info($exception->getTraceAsString());

        } catch (QueryException $exception) {

          \Log::info($exception->getMessage());


          try {
            DB::rollback();
          } catch (\Exception $exception) {
            \Log::info(\json_encode($exception));
          }

          \DB::purge();
          \DB::connection();

          \Log::info($exception->getMessage());
          \Log::info($exception->getTraceAsString());

        } catch (\PDOException $exception) {

          \Log::info($exception->getMessage());


          try {
            DB::rollback();
          } catch (\Exception $exception) {
            \Log::info(\json_encode($exception));
          }


          \DB::purge();
          \DB::connection();


          \Log::info($exception->getMessage());
          \Log::info($exception->getTraceAsString());

        } catch (UnexpectedAlertOpenException $exception) {

          \Log::info($exception->getMessage());
          \Log::info($exception->getTraceAsString());

          try {
            DB::rollback();
          } catch (\Exception $exception) {
            \Log::info(\json_encode($exception));
          }

          $driver->switchTo()->alert()->accept();
          $driver->switchTo()->defaultContent();

          sleep(60);

        } catch (\Exception $exception) {

          \Log::info($exception->getMessage());
          \Log::info($exception->getTraceAsString());

          try {
            DB::rollback();
          } catch (\Exception $exception) {
            \Log::info(\json_encode($exception));
          }

          $this->bcaLoginner->logout($driver);

          sleep(5);

        }
      } catch (\Exception $exception) {

        \Log::info($exception->getMessage());


        try {
          DB::rollback();
        } catch (\Exception $exception) {
          \Log::info(\json_encode($exception));
        }

        if ($this->setAndGetOfflineDuration('bca') >= 30) {
          $driver->quit();
          exit();
        }

        \Log::info($exception->getMessage());
        \Log::info($exception->getTraceAsString());

        sleep(3);
      }
    }
  }
}