<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;

class BankBcaInquiry extends Entity {

  use SoftDeletes;

  protected $fillable = [
    'date',
    'description',
    'branch',
    'amount',
    'balance',
    'created_at',
    'updated_at',
    'order_ids',
    'bank_scrape_account_number_id'
  ];

  protected $hidden = ['updated_at'];



}
