<?php
namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri;

use DB;
use Exception;
use Log;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Helper\LoadingWaiter;
use SimpleXMLElement;
use Carbon\Carbon;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy as By;
use Facebook\WebDriver\WebDriverExpectedCondition as ExpectedCondition;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Support\Facades\Storage;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Repository\BankBriInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Helper\ActivatorMarker;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeAccount;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeAccountNumber;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;

class BriAccountStatementScraper {

  use ActivatorMarker;

  /**
   * @var BankBriInquiryRepository
   */
  private $bankBriInquiryRepo;

  /**
   * @var BankBriInquiryRepository
   */
  private $bankScrapeAccountNumberRepository;

  /**
   * @var LoadingWaiter
   */
  private $loadingWaiter;


  public function __construct() {
    $this->bankBriInquiryRepo = app()->make(BankBriInquiryRepository::class);
    $this->bankScrapeAccountNumberRepository = app()->make(BankScrapeAccountNumberRepository::class);
    $this->loadingWaiter = app()->make(LoadingWaiter::class);
  }

  public function scrape(RemoteWebDriver $driver, BankScrapeAccount $account, Carbon $targetEndDate) {
    $this->openAccountStatement($driver);

    foreach ($account->numbers as $number) {
      $number->last_time_scrapped = Carbon::now();
      $this->bankScrapeAccountNumberRepository->save($number);

      list($startDate, $endDate) = $this->getScrapDate($number, $targetEndDate);

      $this->submitAccountStatementForm($driver, $number->number, $startDate, $endDate);

      try {
        $inquiryList = $this->getAccountInquiryListByExportXml($driver, $number, $startDate, $endDate);
      }
      catch (Exception $e) {
        $url = $driver->getCurrentURL();
        $urlNoQs = substr($url, 0, strpos($url, "?"));
        clog('bri', 'current url is: ' . (empty($urlNoQs) ? $url : $urlNoQs));

        if (str_contains($e->getMessage(), 'no such frame')) {
          Storage::disk('local')->put('bri-account-statement.html', $driver->getPageSource());
          Storage::disk('local')->put('bri-account-statement-default.html',
            $driver->switchTo()->defaultContent()->getPageSource());
        }

        clog('bri', 'By XML error (fallback to by HTML Report): ' .
          $e->getMessage() . ': ' . $e->getTraceAsString());
        $inquiryList = $this->getAccountInquiryListByHtmlReport($driver, $number, $startDate, $endDate);
      }

      list($minDate, $maxDate) = $this->findMinMaxDate($inquiryList);

      $existingInquiryList = $this->bankBriInquiryRepo
        ->getListByAccountNumberIdBetweenDate($number->id,
          $minDate->format('Y-m-d'), $maxDate->format('Y-m-d'))
        ->map(function($inquiry) { return [$this->getInquiryKey($inquiry), $inquiry]; })
        ->pluck(1, 0);

      $newInquiryList = [];
      $updateCount = 0;

      DB::beginTransaction();

      foreach ($inquiryList as $inquiry) {
        $key = $this->getInquiryKey($inquiry);

        if ($existingInquiryList->has($key)) {
          continue;
        }

        preg_match_all('/(?P<desc>.*) (FROM|DARI)\\d* (TO|KE)\\d*\\w*/', $inquiry['description'], $matches);

        if (isset($matches['desc'][0])) {
          $possibleKey = $this->getInquiryKey([
            'date' => $inquiry['date'],
            'description' => $matches['desc'][0],
            'teller_id' => $inquiry['teller_id'],
            'debit' => $inquiry['debit'],
            'credit' => $inquiry['credit'],
          ]);

          if ($existingInquiryList->has($possibleKey)) {
            $old = $existingInquiryList[$possibleKey];
            $old['description'] = $inquiry['description'];
            $old->save();

            $updateCount++;

            continue;
          }
        }

        $newInquiryList[] = $inquiry;
      }

      $this->bankBriInquiryRepo->saveBulk($newInquiryList);
      $number->last_scrapped_date = $endDate;
      $this->bankScrapeAccountNumberRepository->save($number);
      DB::commit();

      $inquiryCount = count($inquiryList);
      $insertedCount = count($newInquiryList);
      clog('bri', "scraped #$number->number (" .
        $startDate->format('Y-m-d') . ' - ' . $endDate->format('Y-m-d') .
        ") inquiry: $inquiryCount ($insertedCount inserted, $updateCount updated)");
    }
  }

  private function findMinMaxDate($inquiryList) {
    $inquiryDateList = array_map(function($inquiry) {
      return $inquiry['date'];
    }, $inquiryList);

    return [
      min($inquiryDateList)->copy()->addDay(-1),
      max($inquiryDateList)->copy()->addDay(1)
    ];
  }

  private function getAccountInquiryListByExportXml(RemoteWebDriver $driver, BankScrapeAccountNumber $number,
    $startDate, $endDate) {
    $now = Carbon::now();
    $result = [];
    $cookies = [];

    foreach ($driver->manage()->getCookies() as $c) {
      $cookies[$c->getName()] = $c->getValue();
    }

    $client = new Client([
      'base_uri' => 'https://ibank.bri.co.id/',
    ]);

    $cookieJar = CookieJar::fromArray($cookies, 'ibank.bri.co.id');

    $this->markAsActive('bri');
    $this->loadingWaiter->wait($driver);

    $driver->switchTo()->defaultContent()
      ->switchTo()->frame('channel');

    $downloadUrl = $driver
      ->executeScript('return window.$find ? ($find("ctl00_TransactionForm_ReportViewer1")._getInternalViewer().ExportUrlBase + "xml") : ' .
        '(document.getElementById("ctl00_TransactionForm_ReportViewer1").ClientController.m_exportUrlBase + "xml")');

    $response = $client->request('GET', $downloadUrl, [
      'cookies' => $cookieJar
    ]);

    $xmlString = $response->getBody()->getContents();
    $xmlString = str_replace('xmlns="DD_ONLINE_CMS_NLedger"', 'xmlns="http://schema.com"', $xmlString);

    $xml = new SimpleXMLElement($xmlString);

    if (count($xml->table2->Detail_Collection->Detail) == 0) {
      return [];
    }

    $runningBalance = $this->parseNumber($xml->table3['textbox90']);
    $details = $xml->table2->Detail_Collection->Detail;

    for ($i = count($details) - 1; $i >= 0; $i--) {
      $detail = $details[$i];
      $dateString = trim($detail['textbox56']) . ' ' . ($detail['textbox59']);
      $date = Carbon::createFromFormat('d/m/y H:i:s', $dateString);

      $debit = $this->parseNumber($detail['textbox72']);
      $credit = $this->parseNumber($detail['textbox104']);
      $runningBalance += $credit - $debit;

      $result[] = [
        'bank_scrape_account_number_id' => $number->id,
        'date' => $date,
        'description' => trim($detail['textbox18']),
        'debit' => $debit,
        'credit' => $credit,
        'balance' => $runningBalance,
        'teller_id' => trim($detail['textbox14']),
        'created_at' => $now,
        'updated_at' => $now,
      ];
    }

    return $result;
  }

  private function getAccountInquiryListByHtmlReport(RemoteWebDriver $driver, BankScrapeAccountNumber $number,
    $startDate, $endDate) {
    $now = Carbon::now();
    $result = [];

    while(true) {
      $this->markAsActive('bri');
      $this->loadingWaiter->wait($driver);

      $driver->switchTo()->defaultContent()
        ->switchTo()->frame('channel')
        ->switchTo()->frame('ReportFramectl00_TransactionForm_ReportViewer1')
        ->switchTo()->frame('report');

      try {
        $rows = $driver->findElements(By::cssSelector('#oReportCell > table > tbody > tr:nth-child(1) > td > table > tbody > tr > td > table > tbody > tr:nth-child(13) > td:nth-child(2) > table tr')); 
      }
      catch (Exception $e) {
        try {
          clog('bri', $driver->findElement(By::cssSelector('body>div>ul>li:only-of-type'))->getText());
        }
        catch (Exception $e2) { }
        
        throw $e;
      }

      array_shift($rows);
      array_shift($rows);

      if (count($rows) == 0) {
        break;
      }

      $rows = array_reverse($rows);

      $openingBalanceEl = $driver->findElement(By::cssSelector('#oReportCell > table > tbody > tr:nth-child(1) > td > table > tbody > tr > td > table > tbody > tr:last-child > td:nth-child(2) > table > tbody > tr:nth-child(3) > td.a481c > div'));

      $runningBalance = $this->parseNumber($openingBalanceEl->getText());
      
      foreach ($rows as $row) {
        $columns = $row->findElements(By::tagName('td'));

        $dateString = trim($columns[0]->getText()) . ' ' . ($columns[1]->getText());
        $date = Carbon::createFromFormat('d/m/y H:i:s', $dateString);

        $debit = $this->parseNumber($columns[3]->getText());
        $credit = $this->parseNumber($columns[4]->getText());
        $runningBalance += $credit - $debit;

        $result[] = [
          'bank_scrape_account_number_id' => $number->id,
          'date' => $date,
          'description' => trim($columns[2]->getText()),
          'debit' => $debit,
          'credit' => $credit,
          'balance' => $runningBalance,
          'teller_id' => trim($columns[5]->getText()),
          'created_at' => $now,
          'updated_at' => $now,
        ];
      }

      $driver->switchTo()->defaultContent()
        ->switchTo()->frame('channel');

      $nextPageButton = $driver->findElement(
        By::cssSelector('input[name="ctl00$TransactionForm$ReportViewer1$ctl01$ctl01$ctl05$ctl01$ctl00"]'));

      if ($nextPageButton->isEnabled()) {
        $nextPageButton->click();
      }
      else {
        break;
      }
    }

    $driver->switchTo()->defaultContent();

    return $result;
  }

  private function getInquiryKey($inquiry) {
    return $inquiry['date']->format('YmdHis') . trim($inquiry['description']) . $inquiry['teller_id'] . 
      number_format($inquiry['debit']) . number_format($inquiry['credit']);
  }

  private function getScrapDate(BankScrapeAccountNumber $number, Carbon $targetEndDate) {
    $startDate = min($number->last_scrapped_date, $targetEndDate);
    $endDate = min($targetEndDate, $startDate->copy()->addDays(30));

    return [$startDate, $endDate];
  }


  private function openAccountStatement(RemoteWebDriver $driver) {
    clog('bri', 'Opening account statement, url: ' . $driver->getCurrentURL());
    $accountStatementLinkSelector = By::cssSelector('#mainLeftMenu a[href$="id=249"]');

    // click account Information link
    $driver->switchTo()->defaultContent();
    $driver->switchTo()->frame('head');
    $driver->findElement(By::cssSelector('div.menu-header a[href$="id=4"]'))
      ->click();

    // click account Statement link
    $driver->switchTo()->defaultContent();
    $driver->switchTo()->frame('menu');
    $driver->findElement(By::cssSelector('#mainLeftMenu tr:nth-child(1) > td'))->click();
    $driver->wait()->until(ExpectedCondition::visibilityOfElementLocated($accountStatementLinkSelector));
    $driver->findElement($accountStatementLinkSelector)->click();

    $driver->switchTo()->defaultContent();
    $driver->switchTo()->frame('channel');

    sleep(2);
    $driver->wait()->until(ExpectedCondition::presenceOfElementLocated(By::id('ctl00_TransactionForm_txtNoRek')));

    clog('bri', 'Opened account statement');
  }

  private function parseNumber($str) {
    return intval(str_replace(',', '', str_replace('.00', '', trim($str))));
  }

  private function submitAccountStatementForm(RemoteWebDriver $driver, $number, $startDate, $endDate) {
    clog('bri', 'submiting account statement form');

    $driver->switchTo()->defaultContent();
    $driver->switchTo()->frame('channel');

    // $driver->findElement(By::id('ctl00_TransactionForm_btnList'))
    //   ->click();

    // $mainWindowHandle = $driver->getWindowHandle();
    // $handles = $driver->getWindowHandles();

    // $driver->switchTo()->window($handles[count($handles) - 1]);

    // $driver->findElement(By::id('txtSearch'))
    //   ->clear()
    //   ->sendKeys($number);

    // $driver->findElement(By::id('btnSearch'))
    //   ->click();

    // $driver->findElement(By::id('cbSelect'))
    //   ->click();

    // $driver->findElement(By::id('btnConfirm'))
    //   ->click();

    // $driver->switchTo()->window($mainWindowHandle);
    // $driver->switchTo()->frame('channel');

    $driver->findElement(By::id('ctl00_TransactionForm_txtNoRek'))
      ->clear()
      ->sendKeys($number);

    $driver->findElement(By::id('ctl00_TransactionForm_txtstartdate'))
      ->clear()
      ->sendKeys($startDate->format('d/m/Y'));

    $driver->findElement(By::id('ctl00_TransactionForm_txtfindate'))
      ->clear()
      ->sendKeys($endDate->format('d/m/Y'));

    $driver->findElement(By::id('ctl00_TransactionForm_btnSubmit'))
      ->click();

    clog('bri', 'submitted account statement form');
    $this->loadingWaiter->wait($driver);

    $url = $driver->getCurrentURL();
    $urlNoQs = substr($url, 0, strpos($url, "?"));
    clog('bri', 'current url is: ' . (empty($urlNoQs) ? $url : $urlNoQs));
  }
}