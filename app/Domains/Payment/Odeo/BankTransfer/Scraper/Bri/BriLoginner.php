<?php
namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri;

use Exception;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy as By;
use Facebook\WebDriver\WebDriverExpectedCondition as ExpectedCondition;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Helper\LoadingWaiter;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeAccount;

class BriLoginner {

  const LOGIN_LOOP_COUNT = 3;
  private $loadingWaiter;

  public function __construct() {
    $this->loadingWaiter = app()->make(LoadingWaiter::class);
  }

  public function login(RemoteWebDriver $driver, BankScrapeAccount $acc) {
    $account = json_decode($acc->account);

    for ($i = 1; $i <= self::LOGIN_LOOP_COUNT; $i++) {
      clog('bri', 'logging in to ' . $account->user_id . ' attempt=' . $i);
      $this->fillLoginForm($driver, $account, true);

      if (!$this->needToClearSession($driver)) {
        if ($i == 1) {
          clog('bri', 'no need to clear session from BRI anymore when login');
        }
        break;
      }

      $this->clearBriSession($driver, $account);
    }

    return $this->isLoggedIn($driver);
  }

  public function logout(RemoteWebDriver $driver) {
    if (!$this->isLoggedIn($driver)) {
      return;
    }

    clog('bri', 'Logouting');
    $driver->switchTo()->defaultContent();
    $driver->switchTo()->frame('head');
    $driver->findElement(By::id('btnLogout'))->click();
    $driver->switchTo()->defaultContent();
    clog('bri', 'logged out');
  }


  private function clearBriSession(RemoteWebDriver $driver, $account) {
    clog('bri', 'clearing bri session');
    $this->fillLoginForm($driver, $account, true);
    $this->fillLoginForm($driver, $account, false);

    try {
      $driver->findElement(By::id('btnCleanAll'))->click();
    }
    catch (Exception $e) {
      $driver->executeScript('btnCleanAll.click()');
    }
  }

  private function fillLoginForm(RemoteWebDriver $driver, $account, $submitForm) {
    $driver->wait()->until(ExpectedCondition::presenceOfElementLocated(By::id('btnLogin')));

    $driver->findElement(By::id('ClientID'))
      ->clear()
      ->sendKeys($account->cooperate_id);

    $driver->findElement(By::id('UserID'))
      ->clear()
      ->sendKeys($account->user_id);

    $driver->findElement(By::id('Password'))
      ->clear()
      ->sendKeys($account->password);

    if ($submitForm) {
      try {
        $driver->findElement(By::id('btnLogin'))->click();
      }
      catch (Exception $e) {
        $driver->executeScript('btnLogin.click()');
      }
    }
  }


  private function needToClearSession(RemoteWebDriver $driver) {
    $accountSummaryLinkSelector = By::cssSelector('#mainLeftMenu a[href$="id=247"]');

    // click account Information link
    $driver->switchTo()->defaultContent();
    $driver->switchTo()->frame('head');
    $driver->findElement(By::cssSelector('div.menu-header a[href$="id=4"]'))
      ->click();

    // click account Statement link
    $driver->switchTo()->defaultContent();
    $driver->switchTo()->frame('menu');
    $driver->findElement(By::cssSelector('#mainLeftMenu tr:nth-child(1) > td'))->click();
    $driver->wait()->until(ExpectedCondition::visibilityOfElementLocated($accountSummaryLinkSelector));
    $driver->findElement($accountSummaryLinkSelector)->click();

    $driver->switchTo()->defaultContent();
    $driver->switchTo()->frame('channel');
    $driver->findElement(By::id('ctl00_TransactionForm_btnOk'))->click();

    $this->loadingWaiter->wait($driver);

    $url = $driver->getCurrentURL();
    $urlNoQs = substr($url, 0, strpos($url, "?"));
    clog('bri', 'current url is: ' . (empty($urlNoQs) ? $url : $urlNoQs));

    return str_contains($url, 'Logon.aspx');
  }

  private function isLoggedIn(RemoteWebDriver $driver) {
    $url = $driver->getCurrentURL();

    if (stripos($url, 'Logon.aspx') !== FALSE) {
      $msgEl = $driver->findElement(By::id('Msg'));
      clog('bri', 'Currently not logged in at: ' . $url . '. msg: ' . $msgEl->getText());
      return false;
    }
    else {
      clog('bri', 'Currently logged in at ' . $url);
      return true;
    }
  }
}
