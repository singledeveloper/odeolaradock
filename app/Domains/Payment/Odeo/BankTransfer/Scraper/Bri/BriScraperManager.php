<?php
/**
 * Created by PhpStorm.
 * User: Hadi Setiawan
 * Date: 7/4/2017
 * Time: 1:21 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri;

use Exception;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy as By;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeAccount;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeInformation;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeInformationRepository;

class BriScraperManager
{
    /**
   * @var BriLoginner
   */
  private $loginner;

  /**
   * @var BankScrapeInformationRepository
   */
  private $bankScrapeInformationRepo;

  /**
   * @var RemoteWebDriver
   */
  private $driver;

  /**
   * @var BankScrapeInformation
   */
  private $information;

  /**
   * @var BriAccountStatementScraper
   */
  private $accountStatementScraper;

  /**
   * @var Carbon
   */
  private $endDate;

  public function __construct()
  {
    $this->loginner = app()->make(BriLoginner::class);
    $this->accountStatementScraper = app()->make(BriAccountStatementScraper::class);
    $this->bankScrapeInformationRepo = app()->make(BankScrapeInformationRepository::class);
  }

  private function initialize() {
    // $cap = DesiredCapabilities::phantomjs();
    $cap = DesiredCapabilities::chrome();

    $options = new ChromeOptions();
    $options->addArguments(['--headless', '--disable-gpu', '--no-sandbox']);
    $cap->setCapability(ChromeOptions::CAPABILITY, $options);

    $this->driver = RemoteWebDriver::create('http://localhost:4445/wd/hub', $cap, 120 * 1000, 120 * 1000);
    $this->driver->manage()->timeouts()->implicitlyWait(30);

    $this->information = $this->bankScrapeInformationRepo->getActiveAccount('bri');
    $this->endDate = Carbon::now();

    set_time_limit(0);
  }

  public function run() {
    $this->initialize();
    
    if (!$this->information) {
      clog('bri', 'BRI Scraper: no bank scrape information');
    }

    try {
      clog('bri', 'start');
      foreach ($this->information->accounts as $account) {
        $this->scrape($account);
      }
    }
    finally {
      clog('bri', 'end');
      try {
        $this->driver->close();
        $this->driver->quit();
      }
      catch (Exception $e) {
        clog('bri', $e->getMessage() . ': ' . $e->getTraceAsString());
      }
    }
  }

  private function scrape(BankScrapeAccount $account) {
    try {
      $this->driver->get($this->information->url);

      if ($this->loginner->login($this->driver, $account)) {
        clog('bri', 'start scraping account statement for #' . $account->id);
        $this->accountStatementScraper->scrape($this->driver, $account, $this->endDate);
        clog('bri', 'end scraping account statement for #' . $account->id);
      }
    } catch (Exception $e) {
      if (str_contains($e->getMessage(), 'no such frame')) {
        Storage::disk('local')->put('bri.html', $this->driver->getPageSource());
        Storage::disk('local')->put('bri-default.html',
          $this->driver->switchTo()->defaultContent()->getPageSource());

        try {
          $el = $this->driver->findElement(By::id('Msg'));
          clog('bri', 'Error message: ' . $el->getText());
        } catch (Exception $ex) {
        }
      }

      throw $e;
    }
    finally {
      try {
        $this->loginner->logout($this->driver);
      } 
      catch(Exception $e) {
        clog('bri', $e->getMessage() . ': ' . $e->getTraceAsString());
      }
    }
  }

}
