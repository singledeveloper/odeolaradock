<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Helper;

use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy as By;
use Facebook\WebDriver\WebDriverExpectedCondition as ExpectedCondition;

class LoadingWaiter {

  public function wait(RemoteWebDriver $driver) {
    sleep(2);

    $url = $driver->getCurrentURL();
    clog('bri', 'waiting at ' . $url);

    if (stripos($url, 'Logon.aspx') !== false) {
      clog('bri', 'skip waiting at login page');
      return;
    }

    try {
      $driver->switchTo()->defaultContent()
        ->switchTo()->frame('channel');
      $driver->wait(15)
        ->until(ExpectedCondition::invisibilityOfElementLocated(By::id('ctl00_TransactionForm_ReportViewer1_AsyncWait_Wait')));
    } catch (NoSuchElementException $e) {
      clog('bri', 'loading not found: ' . $e->getMessage());
    } catch (\Exception $e) {
      clog('bri', $e->getMessage());
    }

    clog('bri', 'finished waiting');
  }
}