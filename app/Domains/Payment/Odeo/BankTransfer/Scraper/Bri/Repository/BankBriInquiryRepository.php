<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/27/16
 * Time: 8:22 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Repository;

use Illuminate\Support\Collection;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Model\BankBriInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankInquiryRepository;

class BankBriInquiryRepository extends BankInquiryRepository {

  public function __construct(BankBriInquiry $briInquiry) {
    $this->setModel($briInquiry);
  }

  /**
   * @return Collection<BankBriInquiry>
   */
  public function getListByAccountNumberIdBetweenDate($accountNumberId, $startDate, $endDate) {
    return $this->model
      ->whereBetween('date', [$startDate, $endDate])
      ->where('bank_scrape_account_number_id', $accountNumberId)
      ->orderBy('id', 'desc')
      ->get();
  }

  function getInquiries($from, $to) {
    return $this->model->getmodel()
      ->orderBy('date', 'asc')
      ->orderBy('id', 'asc')
      ->whereDate('date', '>=', $from)
      ->whereDate('date', '<=', $to)
      ->get();
  }

  public function getLastRecordedBalance($accountNumberId) {
    return $this->model->whereNotNull('balance')->where('balance', '<>', 0)
      ->where('bank_scrape_account_number_id', $accountNumberId)
      ->orderBy('date', 'desc')->first();
  }

}
