<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 29/08/18
 * Time: 15.02
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2;


use Carbon\Carbon;
use GuzzleHttp\Client;
use Odeo\Domains\Constant\BankAccount;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Repository\BankBriInquiryBalanceSummaryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Repository\BankBriInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeAccountNumber;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Symfony\Component\DomCrawler\Crawler;

class BriAccountStatementScraper {

  private $bankBriInquiryRepo;

  private $bankScrapeAccNoRepo;

  private $bankBalanceSummaryRepo;

  public function initialize() {
    $this->bankBriInquiryRepo = app()->make(BankBriInquiryRepository::class);
    $this->bankScrapeAccNoRepo = app()->make(BankScrapeAccountNumberRepository::class);
    $this->bankBalanceSummaryRepo = app()->make(BankBriInquiryBalanceSummaryRepository::class);
  }

  public function scrapeAccountStatement(Client $client, BankScrapeAccountNumber $number, $startDate, $endDate) {
    clog('recon_bri', "scrape from $startDate to $endDate");
    $query = $this->getReportSessionParams($client, $number->number, $startDate, $endDate);
    $mutationData = $summaryData = [];
    $now = Carbon::now();
    clog('recon_bri', json_encode($query));
    while (true) {
      $response = $client->request('GET', 'https://ibank.bri.co.id/cms/Reserved.ReportViewerWebControl.axd', [
        'query' => $query
      ]);

      $page = $query['PageNumber'];
      $crawler = new Crawler($response->getBody()->getContents());

      $crawler->filter('table.a159 tr[valign="top"]')->each(function (Crawler $node, $i) use (&$mutationData, $now, $number, $page) {
        if (($page == 1 && $i != 0) || $page > 1) {
          $nodes = $node->filter('div');
          $values = [
            'date' => Carbon::createFromFormat('d/m/y H:i:s', $nodes->eq(0)->text() . ' ' . $nodes->eq(1)->text()),
            'description' => str_replace("'", '', $nodes->eq(2)->text()),
            'debit' => $this->parseNumber($nodes->eq(3)->text()),
            'credit' => $this->parseNumber($nodes->eq(4)->text()),
            'balance' => $this->parseNumber($nodes->eq(5)->text()),
            'teller_id' => trim($nodes->eq(6)->text()),
            'bank_scrape_account_number_id' => BankAccount::BRI_ODEO,
            'created_at' => $now,
            'updated_at' => $now
          ];
          $mutationData[] = $values;
        }
      });

      $crawler->filter('table.a204 tr[valign="top"]')->each(function (Crawler $node, $i) use (&$summaryData) {
        $nodes = $node->filter('div');
        $summaryData = [
          'opening_balance' => $this->parseNumber($nodes->eq(0)->text()),
          'total_debit' => $this->parseNumber($nodes->eq(1)->text()),
          'total_credit' => $this->parseNumber($nodes->eq(2)->text()),
          'closing_balance' => $this->parseNumber($nodes->eq(3)->text()),
        ];
      });

      if (!empty($summaryData) && is_numeric($summaryData['opening_balance'])) {
        break;
      }

      $query['PageNumber']++;
    }
    return [$mutationData, $summaryData];
  }

  private function getReportSessionParams(Client $client, $accountNo, $startDate, $endDate) {
    $response = $client->request('GET', 'https://ibank.bri.co.id/cms/master/Goto.aspx?id=249');
    $crawler = new Crawler($response->getBody()->getContents(), 'https://ibank.bri.co.id/cms/');
    $form = $crawler->filter('form')->form();

    $params = [
      '__VIEWSTATE' => $form['__VIEWSTATE']->getValue(),
      '__VIEWSTATEGENERATOR' => $form['__VIEWSTATEGENERATOR']->getValue(),
      '__EVENTVALIDATION' => $form['__EVENTVALIDATION']->getValue(),
      'ctl00$TransactionForm$txtNoRek' => $accountNo,
      'ctl00$TransactionForm$txtstartdate' => $startDate->format('d/m/Y'),
      'ctl00$TransactionForm$txtfindate' => $endDate->format('d/m/Y'),
      'ctl00$TransactionForm$HidRadioLegder' => 'L',
      'ctl00$TransactionForm$btnSubmit.y' => 10,
      'ctl00$TransactionForm$btnSubmit.x' => 12
    ];

    $res = $client->request('POST', 'https://ibank.bri.co.id/cms/transaction/account/AccStatement.aspx?id=249', [
      'form_params' => $params,
      'timeout' => 60
    ]);

    $body = $res->getBody()->getContents();
    preg_match('/(ReportSession)%3d(.*?)%26(.*?)%3d(.*?)%26(.*?)%3d(.*?)%26(.*?)%3d(.*?)%26(.*?)%3d(.*?)%26(.*?)%3d(.*?)%26(.*?)%3d(.*?)%26(.*?)%3d(.*?)%26(.*?)%3d(.*?)%26(.*?)%3d(.*?)%26(.*?)%3d(.*?)%26(.*?)%3d(.*?)%26(.*?)%3d(.*?)"/', $body, $matches);
    list(, $key1, $val1, $key2, $val2, $key3, $val3, $key4, $val4, $key5, $val5, $key6, $val6, $key7, $val7, $key8, $val8, $key9, $val9, $key10, $val10) = $matches;

    return [
      $key1 => $val1,
      $key2 => $val2,
      $key3 => $val3,
      $key4 => $val4,
      $key5 => $val5,
      $key6 => $val6,
      $key7 => $val7,
      $key8 => $val8,
      $key9 => $val9,
      $key10 => $val10,
    ];
  }

  private function parseNumber($str) {
    return str_replace(',', '', trim($str));
  }

}
