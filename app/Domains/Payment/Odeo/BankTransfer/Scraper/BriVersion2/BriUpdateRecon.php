<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2019-02-21
 * Time: 14:17
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2;


use Carbon\Carbon;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Repository\BankBriInquiryRepository;

class BriUpdateRecon {

  private $startDate;
  private $endDate;
  private $briInquiry;

  public function __construct($startDate, $endDate) {
    $this->startDate = $startDate;
    $this->endDate = $endDate;
    $this->briInquiry = app()->make(BankBriInquiryRepository::class);
  }

  public function check() {
    $countCul = 0;
    $notfoundCul = [];
    list($notfound, $count) = $this->checkDefault();
    $countCul += $count;
    $notfoundCul = array_merge($notfoundCul, $notfound);
    list($notfound, $count) = $this->checkByDescription($notfoundCul);
    $countCul += $count;
    $notfoundCul = array_intersect($notfoundCul, $notfound);
    return [$notfoundCul, $countCul];
  }

  public function checkDefault() {
    $updates = [];
    $notfound = [];

    $existingInquiries = $this->briInquiry->getListByAccountNumberIdBetweenDate($this->startDate, $this->endDate)
      ->keyBy(function ($inquiry) {
        return $this->getInquiryKey($inquiry);
      });

    $deletedInquiries = $this->briInquiry->getDeletedListByAccountNumberIdBetweenDate($this->startDate, $this->endDate)
      ->keyBy(function ($inquiry) {
        return $this->getInquiryKey($inquiry);
      });

    foreach ($deletedInquiries as $key => $inquiry) {
      if ($existingInquiries->has($key)) {
        $updates[] = [
          'id' => $existingInquiries[$key]['id'],
          'reference_type' => $deletedInquiries[$key]['reference_type'],
          'reference' => $deletedInquiries[$key]['reference'],
          'updated_at' => Carbon::now(),
        ];
      } else {
        $notfound[] = $inquiry['id'];
      }
    }

    if (!empty($updates)) {
      $this->briInquiry->updateBulk($updates);
    }

    return [$notfound, count($updates)];
  }

  public function checkByDescription($notfoundId) {
    $updates = [];
    $notfound = [];

    $existingInquiries = $this->briInquiry->getListByAccountNumberIdBetweenDate($this->startDate, $this->endDate)
      ->keyBy(function ($inquiry) {
        return $this->getInquiryKeyByDesc($inquiry);
      });

    $deletedInquiries = $this->briInquiry->getModel()->onlyTrashed()->whereIn('id', $notfoundId)
      ->get()
      ->keyBy(function ($inquiry) {
        return $this->getInquiryKeyByDesc($inquiry);
      });

    foreach ($deletedInquiries as $key => $inquiry) {
      if ($existingInquiries->has($key)) {
        $updates[] = [
          'id' => $existingInquiries[$key]['id'],
          'reference_type' => $deletedInquiries[$key]['reference_type'],
          'reference' => $deletedInquiries[$key]['reference'],
          'updated_at' => Carbon::now(),
        ];
      } else {
        $notfound[] = $inquiry['id'];
      }
    }

    if (!empty($updates)) {
      $this->briInquiry->updateBulk($updates);
    }

    return [$notfound, count($updates)];
  }

  private function getInquiryKey($inquiry) {
    return trim($inquiry['date']->format('YmdHis') . $inquiry['debit'] . $inquiry['credit'] . intval($inquiry['balance']));
  }

  private function getInquiryKeyByDesc($inquiry) {
    return trim($inquiry['date']->format('YmdHis') . $inquiry['debit'] . $inquiry['credit'] . $inquiry['description']);
  }

}
