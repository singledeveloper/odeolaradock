<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 06/09/18
 * Time: 18.17
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Helper;


class BriScrapeHelper {

  const STATUS_OK = 50000;
  const STATUS_DIFF_UNKNOWN = 90000;
  const REDIS_RECON_MUTATION_LOCK = 'odeo_core::bri_recon_account_statement';
  const REDIS_RECON_ERROR_COUNT = 'odeo_core::bri_recon_error_count';

}