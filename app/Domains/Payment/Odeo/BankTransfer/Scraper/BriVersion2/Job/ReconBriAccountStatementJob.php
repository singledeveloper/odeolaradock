<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 17/10/18
 * Time: 15.12
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Job;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Notification\Helper\InternalNoticer;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\BriAccountStatementScraper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\AccountManager;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Helper\BriScrapeHelper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Repository\BankBriInquiryBalanceSummaryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Repository\BankBriInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Job\SendMismatchMutationAlert;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeInformationRepository;
use Odeo\Jobs\Job;

class ReconBriAccountStatementJob extends Job {

  const BANK_TARGET = 'bri';

  /**
   * @var BankBriInquiryRepository
   */
  private $briInquiry;

  /**
   * @var AccountManager
   */
  private $accountManager;

  /**
   * @var BankBriInquiryBalanceSummaryRepository
   */
  private $briInquirySummary;

  /**
   * @var BankScrapeAccountNumberRepository
   */
  private $scrapeAccountNoRepo;

  /**
   * @var BankScrapeInformationRepository
   */
  private $bankScrapeInformationRepo;

  /**
   * @var BriAccountStatementScraper
   */
  private $scraper;

  /**
   * @var Redis
   */
  private $redis;

  /**
   * @var Carbon
   */
  private $startDate, $endDate;

  private $information, $shouldRecon;

  public function __construct($startDate, $endDate, $shouldRecon = false) {
    parent::__construct();
    $this->startDate = $startDate;
    $this->endDate = $endDate;
    $this->shouldRecon = $shouldRecon;
  }

  private function initialize() {
    $this->briInquiry = app()->make(BankBriInquiryRepository::class);
    $this->accountManager = app()->make(AccountManager::class);
    $this->briInquirySummary = app()->make(BankBriInquiryBalanceSummaryRepository::class);
    $this->scrapeAccountNoRepo = app()->make(BankScrapeAccountNumberRepository::class);
    $this->scraper = app()->make(BriAccountStatementScraper::class);
    $this->bankScrapeInformationRepo = app()->make(BankScrapeInformationRepository::class);
    $this->redis = Redis::connection();

    $this->information = $this->bankScrapeInformationRepo->getActiveAccount(self::BANK_TARGET);
  }

  // if scraped is less than existing and diff is more than 5: fail
  private function checkIfScrapedLessThanExisting($scrapedInquiries, $existingInquiries) {
    $countScraped = count($scrapedInquiries);
    $countExisting = count($existingInquiries);

    return $this->startDate->diffInDays($this->endDate) <= 1
      && $countScraped < $countExisting
      && abs($countScraped - $countExisting) >= 5;
  }

  public function handle() {
    $this->initialize();

    if (!$this->redis->setnx(BriScrapeHelper::REDIS_RECON_MUTATION_LOCK, 1)) {
      return;
    }

    $this->redis->expire(BriScrapeHelper::REDIS_RECON_MUTATION_LOCK, 5 * 60);

    $latestSummary = $this->briInquirySummary->findLatestSummary();
    $errorCount = $this->redis->get(BriScrapeHelper::REDIS_RECON_ERROR_COUNT) ?? 0;

    foreach ($this->information->accounts as $account) {
      $number = $account->numbers[0];
      $scrapeAccount = json_decode($account->account);

      try {
        $client = $this->accountManager->login($scrapeAccount);
        list($scrapedInquiries, $scrapedSummary) = $this->scraper->scrapeAccountStatement($client, $number, $this->startDate, $this->endDate);
        clog('recon_bri', 'scraped: ' . count($scrapedInquiries));

        if (empty($scrapedInquiries)) {
          continue;
        }

        $existingInquiries = $this->briInquiry->getListByAccountNumberIdBetweenDate($this->startDate, $this->endDate);

        if ($this->checkIfScrapedLessThanExisting($scrapedInquiries, $existingInquiries)) {
          continue;
        }

        $groupedScrapeInq = $this->groupInquiries($scrapedInquiries);
        $groupedExistingInq = $this->groupInquiries($existingInquiries);

        $newInquiries = $updates = $removeIds = [];
        $netMovement = 0;

        foreach ($groupedScrapeInq as $key => $scrapeInquiries) {
          foreach ($scrapeInquiries as $inquiry) {
            // update existing inquiry / insert if not exists
            if ($existingInquiry = $this->getInquiryByKey($groupedExistingInq, $key)) {
              $hasUpdates = $existingInquiry['balance'] != $inquiry['balance']
                || $existingInquiry['description'] != $inquiry['description']
                || $existingInquiry['teller_id'] != $inquiry['teller_id'];
              if ($hasUpdates) {
                $updates[] = [
                  'id' => $existingInquiry['id'],
                  'description' => $inquiry['description'],
                  'balance' => $inquiry['balance'],
                  'teller_id' => $inquiry['teller_id'],
                  'updated_at' => Carbon::now()
                ];
              }
              continue;
            }
            $newInquiries[] = $inquiry;
            $netMovement += $inquiry['credit'] - $inquiry['debit'];
          }
        }

//        foreach ($groupedExistingInq as $key => $existingInquiries) {
//          foreach ($existingInquiries as $inquiry) {
//            $removeIds[] = $inquiry['id'];
//            $netMovement -= $inquiry['credit'] - $inquiry['debit'];
//          }
//        }

        $newSummary = $this->briInquirySummary->getNew();
        $newSummary->previous_balance = $latestSummary->balance ?? 0;
        $newSummary->balance_change = $netMovement;
        $newSummary->balance = isset($latestSummary) ? $latestSummary->balance + $netMovement : $scrapedSummary['closing_balance'];
        $newSummary->scrape_total_debit = $scrapedSummary['total_debit'];
        $newSummary->scrape_total_credit = $scrapedSummary['total_credit'];
        $newSummary->scrape_opening_balance = $scrapedSummary['opening_balance'];
        $newSummary->scrape_closing_balance = $this->getClosingBalance($scrapedSummary, $latestSummary);
        $newSummary->scrape_start_date = $this->startDate;
        $newSummary->scrape_end_date = $this->endDate;

        list($newSummary->status, $newSummary->first_distinct_id) = $this->getDifferenceStatus($newSummary, $latestSummary);
        $this->briInquirySummary->save($newSummary);

        if ($newSummary->status == BriScrapeHelper::STATUS_OK && $this->briInquirySummary->findDistinctRow()) {
          $this->briInquirySummary->updateDistinctRow();
        }

        if (!empty($removeIds)) {
          clog('recon_bri', 'removed: ' . count($removeIds));
//          $this->briInquiry->deleteById($removeIds);
        }
        if (!empty($updates)) {
          clog('recon_bri', 'updated: ' . count($updates));
          $this->briInquiry->updateBulk($updates);
        }
        if (!empty($newInquiries)) {
          clog('recon_bri', 'new: ' . count($newInquiries));
          $this->briInquiry->saveBulk(array_reverse($newInquiries));
        }

        $number->last_scrapped_date = Carbon::now();
        $number->last_time_scrapped = Carbon::now();
        $this->scrapeAccountNoRepo->save($number);

        $errorCount && $this->resetErrorCount();
      } catch (\Exception $e) {
        $this->handleInternalNotice($errorCount, $scrapeAccount);
        clog('recon_bri', 'error msg: ' . $e->getMessage());
        clog('recon_bri', 'error trace: ' . $e->getTraceAsString());
      } finally {
        $this->accountManager->logout($client);
        $this->redis->del(BriScrapeHelper::REDIS_RECON_MUTATION_LOCK);
      }

      if ($this->shouldRecon && $this->briInquirySummary->findDistinctRow()) {
        if ($this->startDate > Carbon::now()->subDay(28)) {
          $endDate = $this->startDate->subDay(1);
          $startDate = $endDate->copy()->subDay(1);
//          dispatch(new self($startDate, $endDate, true));
        } else {
          dispatch(new SendMismatchMutationAlert(self::BANK_TARGET, $this->startDate));
        }
      }
    }
  }

  private function getClosingBalance($scrapedSummary, $latestSummary) {
    if (!$this->endDate->isSameDay(Carbon::now()) && !is_null($latestSummary['first_distinct_id'])) {
      $distinctSummary = $this->briInquirySummary->findById($latestSummary['first_distinct_id']);
      return $distinctSummary['scrape_closing_balance'];
    }
    return $scrapedSummary['closing_balance'] ?? $latestSummary['scrape_closing_balance'];
  }

  private function getInquiryKey($inquiry) {
    return trim($inquiry['date']->format('YmdHis') . $inquiry['debit'] . $inquiry['credit']);
  }

  private function getDifferenceStatus($summary, $prevSummary) {
    if ($summary->balance != $summary->scrape_closing_balance) {
      if ($prevSummary->status != BriScrapeHelper::STATUS_OK) {
        return [BriScrapeHelper::STATUS_DIFF_UNKNOWN, $prevSummary->first_distinct_id ?? $prevSummary->id];
      }
      return [BriScrapeHelper::STATUS_DIFF_UNKNOWN, null];
    }
    return [BriScrapeHelper::STATUS_OK, null];
  }

  private function groupInquiries($inquiries) {
    $inquiries = collect($inquiries);
    $groupInquiries = $inquiries->groupBy(function ($inquiry) {
      return $this->getInquiryKey($inquiry);
    });

    return $groupInquiries->transform(function ($inquiries) {
      return $inquiries->sortBy('date');
    });
  }

  private function getInquiryByKey(&$groupedInquiries, $key) {
    if (!isset($groupedInquiries[$key])) {
      return null;
    }

    $inquiries = $groupedInquiries[$key];
    $selectedInquiry = $inquiries->shift();

    if (empty($inquiries)) {
      unset($groupedInquiries[$key]);
    }

    return $selectedInquiry;
  }

  private function resetErrorCount($count = 0) {
    $this->redis->set(BriScrapeHelper::REDIS_RECON_ERROR_COUNT, $count);
  }

  private function handleInternalNotice($count, $scrapeAccount) {
    if ($count == 14) {
      $noticer = app()->make(InternalNoticer::class);
      $noticer->saveMessage('BRI Scrape Failed: ' . $scrapeAccount->user_id . ' failed to scrape in the last 15 minutes.');
      $this->resetErrorCount();
    } else {
      $count += 1;
      $this->resetErrorCount($count);
    }
  }

}
