<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 05/09/18
 * Time: 13.07
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Model;


use Odeo\Domains\Core\Entity;

class BankBriInquiryBalanceSummary extends Entity {

  protected $dates = ['scrape_start_date', 'scrape_end_date', 'created_at', 'updated_at'];

}