<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 05/09/18
 * Time: 13.08
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Helper\BriScrapeHelper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Model\BankBriInquiryBalanceSummary;

class BankBriInquiryBalanceSummaryRepository extends Repository {

  function __construct(BankBriInquiryBalanceSummary $balanceSummary) {
    $this->setModel($balanceSummary);
  }

  function findLatestSummary() {
    return $this->model->orderBy('created_at', 'desc')->first();
  }

  function findDistinctRow() {
    return $this->model
      ->whereNull('first_distinct_id')
      ->where('status', '=', BriScrapeHelper::STATUS_DIFF_UNKNOWN)
      ->orderBy('id', 'asc')
      ->first();
  }

  function updateDistinctRow() {
    return $this->getModel()
      ->where('status', '=', BriScrapeHelper::STATUS_DIFF_UNKNOWN)
      ->update([
        'status' => BriScrapeHelper::STATUS_OK
      ]);
  }

  function findByScrapeEnd($scrapeEndDate, $scrapeEndBalance) {
    return $this->getModel()
      ->where('scrape_end_date', $scrapeEndDate)
      ->where('scrape_closing_balance', $scrapeEndBalance)
      ->orderBy('id', 'desc')
      ->first();
  }

}