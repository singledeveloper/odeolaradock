<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 16/10/18
 * Time: 15.30
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Repository\BankBriInquiryBalanceSummaryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Repository\BankBriInquiryRepository;

class BriInquirySequenceScheduler {

  /**
   * @var BankBriInquiryRepository
   */
  private $briInquiry;

  /**
   * @var BankBriInquiryBalanceSummaryRepository
   */
  private $briSummary;

  private $currBalance;

  private $currSequence;

  private $date;

  private function initiate() {
    $this->briInquiry = app()->make(BankBriInquiryRepository::class);
    $this->briSummary = app()->make(BankBriInquiryBalanceSummaryRepository::class);
    $this->currSequence = 0;
    $this->currBalance = number_format(0);
    $this->date = Carbon::parse('2017-06-19');
  }

  public function run() {
    $this->initiate();

    if (!$this->briSummary->findDistinctRow()) {
      if ($unassignedIq = $this->briInquiry->findFirstUnsignedSequenceInquiry()) {
        $prevSequence = $this->briInquiry->findLatestSequenceBeforeDate($unassignedIq->date);
        $this->date = $unassignedIq->date;

        if ($prevSequence) {
          $this->currSequence = $prevSequence->sequence_number;
          $this->currBalance = number_format($prevSequence->balance);
        }
      }

      $inquiries = $this->briInquiry->getInquiryFromDate($this->date);

      if (!empty($inquiries->where('sequence_number', null))) {
        $groupedInquiries = $this->groupInquiries($inquiries);

        $updates = [];

        for ($i = 0; $i < count($inquiries); $i++) {
          $selectedInquiry = $this->getInquiryByBalance($groupedInquiries, $this->currBalance);
          if (!$selectedInquiry) {
            break;
          }

          $this->currSequence++;
          $updates[] = [
            'id' => $selectedInquiry['id'],
            'sequence_number' => $this->currSequence,
          ];

          $this->currBalance = number_format($selectedInquiry['balance']);
        }

        if (!empty($updates)) {
          $this->briInquiry->updateBulk($updates);
        }
      }
    }
  }

  private function groupInquiries($inquiries) {
    $groupInquiries = $inquiries->groupBy(function ($inquiry) {
      return number_format($inquiry['balance'] - $inquiry['credit'] + $inquiry['debit']);
    });

    return $groupInquiries->transform(function ($inquiries) {
      return $inquiries->sortBy('date');
    });
  }

  private function getInquiryByBalance(&$groupedInquiries, $balance) {
    if (!isset($groupedInquiries[$balance])) {
      return null;
    }

    $inquiries = $groupedInquiries[$balance];
    $selectedInquiry = $inquiries->shift();

    if (empty($inquiries)) {
      unset($groupedInquiries[$balance]);
    }

    return $selectedInquiry;
  }

}
