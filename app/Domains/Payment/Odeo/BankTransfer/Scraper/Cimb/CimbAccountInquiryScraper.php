<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 21/09/18
 * Time: 12.43
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb;


use Carbon\Carbon;
use GuzzleHttp\Client;
use Odeo\Domains\Constant\BankAccount;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Helper\CimbHelper;
use Symfony\Component\DomCrawler\Crawler;

class CimbAccountInquiryScraper {

  public function scrapeAccountStatement(Client $client, $startDate, $endDate) {
    $client->request('GET', 'https://bizchannel.cimbniaga.co.id/corp/common2/login.do', [
      'query' => [
        'action' => 'menuRequest'
      ]
    ]);

    $client->request('GET', 'https://bizchannel.cimbniaga.co.id/corp/front/balanceinquiry.do', [
      'query' => CimbHelper::getReportAccountInquiryQuery()
    ]);

    $res = $client->request('POST', 'https://bizchannel.cimbniaga.co.id/corp/front/balanceinquiry.do', [
      'query' => CimbHelper::getAccountDetailsQuery(),
      'form_params' => [
        'accountNo' => CimbHelper::ACCOUNT_NO
      ]
    ]);

    $crawler = new Crawler($res->getBody()->getContents());
    $summaryRows = $crawler->filter('tr.clsEven');
    $scrapeYesterdayBalance = str_replace(',', '', $summaryRows->eq(1)->filter('td')->eq(1)->text());
    $scrapeLedger = str_replace(',', '', $summaryRows->eq(9)->filter('td')->eq(1)->text());

    $res = $client->request('GET', CimbHelper::TRANSACTION_INQUIRY_URL, [
      'query' => CimbHelper::getTransactionByDateRequestQuery()
    ]);
    $crawler = new Crawler($res->getBody()->getContents(), CimbHelper::TRANSACTION_INQUIRY_URL);
    $form = $crawler->filter('form')->form();
    $client->request('POST', CimbHelper::TRANSACTION_INQUIRY_URL, CimbHelper::doCheckValidityParams($startDate, $endDate, $form, 1));

    $scrapeStatements = [];

    for ($page = 1;; $page++) {
      $res = $client->request('POST', CimbHelper::TRANSACTION_INQUIRY_URL, CimbHelper::getListTransactionParams($startDate, $endDate, $form, $page));
      $crawler = new Crawler($res->getBody()->getContents());
      $rows = $crawler->filter('table.clsAprismaTable tr.clsOdd, table.clsAprismaTable tr.clsEven');

      if ($rows->count() == 1 && $rows->text() == CimbHelper::TEXT_NO_RECORD_FOUND) {
        break;
      }

      $rows->each(function(Crawler $row) use (&$scrapeStatements) {
        $now = Carbon::now();
        $elements = $row->filter('td');
        $cols = [
          'post_date' => Carbon::parse($elements->eq(1)->text()),
          'eff_date' => Carbon::parse($elements->eq(2)->text()),
          'date' => Carbon::parse($elements->eq(2)->text()),
          'cheque_no' => $elements->eq(3)->text(),
          'description' => $elements->eq(4)->text(),
          'debit' => str_replace(',', '', $elements->eq(5)->text()),
          'credit' => str_replace(',', '', $elements->eq(6)->text()),
          'balance' => str_replace(',', '', $elements->eq(7)->text()),
          'transaction_code' => $elements->eq(8)->text(),
          'bank_reff_no' => $elements->eq(9)->text(),
          'bank_scrape_account_number_id' => BankAccount::CIMB_ODEO,
          'created_at' => $now,
          'updated_at' => $now,
        ];

        $scrapeStatements[] = $cols;
      });

      if ($rows->count() < 10) break;
    }

    return [$scrapeStatements, $scrapeLedger, $scrapeYesterdayBalance];
  }

}
