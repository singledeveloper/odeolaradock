<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 19/09/18
 * Time: 20.55
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Helper;


use Carbon\Carbon;
use Symfony\Component\DomCrawler\Form;

class CimbHelper {

  const ACCOUNT_NO = '800146081300 / ODEO TEKNOLOGI INDONESIA';
  const TEXT_NO_RECORD_FOUND = 'Record Not Found';
  const STATUS_OK = '50000';
  const STATUS_DIFF_BALANCE = '90000';
  const TRANSACTION_INQUIRY_URL = 'https://bizchannel.cimbniaga.co.id/corp/front/transactioninquiry.do';
  const LOGIN_URL = 'https://bizchannel.cimbniaga.co.id/corp/common2/login.do';
  const REDIS_RECON_MUTATION_LOCK = 'odeo_core::cimb_recon_account_statement';
  const REDIS_RECON_ERROR_COUNT = 'odeo_core::cimb_recon_error_count';

  static function getReportAccountInquiryQuery() {
    return [
      'action' => 'getReportAccountInquiry',
      'accountPick' => 'multiple',
      'selectby' => 3,
      'hierarchy' => ' ',
      'accountType' => ' ',
      'accountBranch' => ' ',
      'menu' => 'search',
      'screenAction' => ' ',
      'menuCode' => 'MNU_GCME_040800'
    ];
  }

  static function getAccountDetailsQuery() {
    return [
      'action' => 'getAccountDetails',
      'menu' => 'search',
      'tabName' => 'Account Balance'
    ];
  }

  static function getTransactionByDateRequestQuery() {
    return [
      'action' => 'transactionByDateRequest',
      'menuCode' => 'MNU_GCME_040200',
      'accountNo' => '800146081300 / ODEO TEKNOLOGI INDONESIA'
    ];
  }

  static function doCheckValidityParams($startDate, $endDate, Form $form, $page) {
    $startDate = Carbon::parse($startDate);
    $endDate = Carbon::parse($endDate);
    return [
      'query' => [
        'action' => 'doCheckValidity',
        'day1' => $startDate->day,
        'mon1' => $startDate->month,
        'year1' => $startDate->year,
        'day2' => $endDate->day,
        'mon2' => $endDate->month,
        'year2' => $endDate->year,
        'accountNumber' => '800146081300',
        'type' => 'show'
      ],
      'form_params' => [
        'org.apache.struts.taglib.html.TOKEN' => $form['org.apache.struts.taglib.html.TOKEN']->getValue(),
        'screenState' => $form['screenState']->getValue(),
        'accountType' => $form['accountType']->getValue(),
        'processAccountIndividually' => $form['processAccountIndividually']->getValue(),
        'transferDateDay1' => $startDate->day,
        'transferDateMonth1' => $startDate->month,
        'transferDateYear1' => $startDate->year,
        'transferDateDay2' => $endDate->day,
        'transferDateMonth2' => $endDate->month,
        'transferDateYear2' => $endDate->year,
        'accountNumber' => '800146081300',
        'sortByDateDesc' => 'Y',
        'currentPage' => $page
      ]
    ];
  }

  static function getListTransactionParams($startDate, $endDate, Form $form, $page) {
    $startDate = Carbon::parse($startDate);
    $endDate = Carbon::parse($endDate);
    return [
      'query' => [
        'action' => 'getListTransactionByDate',
        'accountNumber' => '800146081300',
        'accountNm' => 'ODEO TEKNOLOGI INDONESIA',
        'currDisplay' => 'IDR',
        'day1' => $startDate->day,
        'mon1' => $startDate->month,
        'year1' => $startDate->year,
        'day2' => $endDate->day,
        'mon2' => $endDate->month,
        'year' => $endDate->year,
        'trxFilter' => ''
      ],
      'form_params' => [
        'org.apache.struts.taglib.html.TOKEN' => $form['org.apache.struts.taglib.html.TOKEN']->getValue(),
        'screenState' => $form['screenState']->getValue(),
        'accountType' => $form['accountType']->getValue(),
        'transferDateDay1' => $startDate->day,
        'transferDateMonth1' => $startDate->month,
        'transferDateYear1' => $startDate->year,
        'transferDateDay2' => $endDate->day,
        'transferDateMonth2' => $endDate->month,
        'transferDateYear2' => $endDate->year,
        'accountNumber' => '800146081300',
        'sortByDateDesc' => 'Y',
        'currentPage' => $page,
        'checkBtn' => '',
      ]
    ];
  }

  static function getInquiryKey($inquiry) {
    return $inquiry['bank_reff_no'] .
      Carbon::parse($inquiry['date'])->format('Y-m-d') .
      number_format($inquiry['debit']) .
      number_format($inquiry['credit']);
  }

}
