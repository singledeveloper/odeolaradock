<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/29/17
 * Time: 7:31 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Model;


use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;

class BankCimbInquiry extends Entity {

  use SoftDeletes;
  protected $dates = ['date', 'created_at', 'updated_at'];

}