<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/10/18
 * Time: 13.54
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Job\ScrapeReconCimbInquiryJob;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;

class ScrapeReconCimbInquiryScheduler {

  /**
   * @var BankScrapeAccountNumberRepository
   */
  private $bankScrapeAccountNo;

  private function initialize() {
    $this->bankScrapeAccountNo = app()->make(BankScrapeAccountNumberRepository::class);
  }

  public function run() {
    $this->initialize();
    $account = $this->bankScrapeAccountNo->findByName('cimb');
    $startDate = $account->last_scrapped_date->subDay(1);
    $endDate = $this->getEndDate($startDate);

    dispatch(new ScrapeReconCimbInquiryJob($startDate, $endDate, $endDate->isSameDay(Carbon::now())));
  }

  private function getEndDate(Carbon $startDate) {
    if ($startDate->diffInDays(Carbon::now()) > 30) {
      return $startDate->copy()->addDay(30);
    }
    return Carbon::now();
  }

}
