<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/1/16
 * Time: 6:16 PM
 */
namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Commands;

use Illuminate\Console\Command;

class DbConnector extends Command
{

  protected $signature = 'scrape:db-connector';

  protected $description = 'run DB Connector';


  public function __construct()
  {
    parent::__construct();
  }

  public function handle()
  {
    while (true) {

      exec("echo connecting");
      exec('autossh -M 0 -o ServerAliveInterval=10 -o ServerAliveCountMax=1 -Ng -L 1122:odeo-postgres.csxidbaysz1u.ap-southeast-1.rds.amazonaws.com:5436 bank-scrapper-mgr@52.74.36.212 -p 2207');
      exec("echo reconnecting");

    }
  }
}
