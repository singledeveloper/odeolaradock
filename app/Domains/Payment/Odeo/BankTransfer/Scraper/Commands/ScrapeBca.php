<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/1/16
 * Time: 6:16 PM
 */
namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Commands;

use Illuminate\Console\Command;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\BcaScrapperManager;

class ScrapeBca extends Command {

  protected $signature = 'scrape:bank-bca';

  protected $description = 'run BCA Inquiry Scraper';


  public function __construct() {
    parent::__construct();
  }

  public function handle() {
    app()->make(BcaScrapperManager::class)->run();
  }
}
