<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Helper\ActivatorMarker;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Helper\SeleniumStarter;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Mandiri\MandiriScrapperManager;

class ScrapeMandiri extends Command {

  use ActivatorMarker;

  protected $signature = 'scrape:bank-mandiri';

  protected $description = 'run Mandiri Inquiry Scraper';

  private $seleniumStarter;

  public function __construct() {
    parent::__construct();

    $this->seleniumStarter = app()->make(SeleniumStarter::class);
  }

  public function handle() {
    while (true) {
      try {
        $this->seleniumStarter->start(4445, 10, '/usr/local/bin/selenium-server-standalone.jar');

        app()->make(MandiriScrapperManager::class)->run();
      } catch (\Exception $exception) {
        \Log::info($exception->getMessage());
      }
    }
  }
}
