<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 12/27/16
 * Time: 3:19 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Helper;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

trait ActivatorMarker {

  public function setAndGetOfflineDuration($name) {
    $contents = Storage::disk('local')->get($name . '-scraper.txt');
    if (!$contents) {
      return 0;
    } else {
      return Carbon::parse($contents)->diffInMinutes(Carbon::now());
    }
  }

  public function markAsActive($name) {
    Storage::disk('local')->put($name . '-scraper.txt', Carbon::now()->toDateTimeString());
  }

}