<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/17/16
 * Time: 5:49 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Mandiri;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeAccount;

class MandiriLoginner {


  public function login(RemoteWebDriver $driver, BankScrapeAccount $acc) {

    $account = json_decode($acc->account);

    sleep(1);

    $driver->findElement(WebDriverBy::name('corpId'))
      ->clear()
      ->sendKeys($account->cooperate_id);


    $driver->findElement(WebDriverBy::name('userName'))
      ->clear()
      ->sendKeys($account->user_id);

    $driver->findElement(WebDriverBy::name('passwordEncryption'))
      ->clear()
      ->sendKeys($account->password);

    $driver->findElement(WebDriverBy::id('button'))
      ->click();

    sleep(1);
  }


  public function logout(RemoteWebDriver $driver) {
    $driver->switchTo()->defaultContent();

    $driver->switchTo()->frame($driver->findElement(WebDriverBy::cssSelector('html > frameset > frame:nth-child(1)')));

    $driver->findElement(WebDriverBy::cssSelector('body > table > tbody > tr > td > table:nth-child(2) > tbody > tr > td > table > tbody > tr > td.top_nav_right > a'))
      ->click();

  }

}