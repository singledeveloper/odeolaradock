<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/17/16
 * Time: 12:08 AM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Mandiri;


use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Exception\UnexpectedAlertOpenException;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use DB;
use Illuminate\Database\QueryException;
use LogicException;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Helper\ActivatorMarker;

class MandiriScrapperManager {

  use ActivatorMarker;

  public static $driver;
  public static $information;

  private $mandiriLoginner, $mandiriTransactionInquiryScrapper, $bankScrapeInformationRepository;

  public function __construct() {
    $this->bankScrapeInformationRepository = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeInformationRepository::class);
    $this->mandiriLoginner = app()->make(MandiriLoginner::class);
    $this->mandiriTransactionInquiryScrapper = app()->make(MandiriTransactionInquiryScrapper::class);
  }

  public function initialize() {

    self::$information = $this->bankScrapeInformationRepository->getActiveAccount('mandiri');
    set_time_limit(0);

  }

  public function run() {
    $this->initialize();

//    $driver = RemoteWebDriver::create('http://localhost:4444/wd/hub', DesiredCapabilities::chrome(), 600 * 1000, 600 * 1000);
//    $cap = DesiredCapabilities::phantomjs();
//    $driver = RemoteWebDriver::create('http://localhost:4444/wd/hub', $cap, 120 * 1000, 120 * 1000);

    $cap = DesiredCapabilities::chrome();

    $options = new ChromeOptions();
    $options->addArguments(['--headless', '--disable-gpu', '--no-sandbox']);
    $cap->setCapability(ChromeOptions::CAPABILITY, $options);
    $driver = RemoteWebDriver::create('http://localhost:4445/wd/hub', $cap, 120 * 1000, 120 * 1000);

    $driver->manage()->timeouts()->implicitlyWait(30);

    $driver->get(self::$information->url);

    $pointer = 0;
    $length = self::$information->accounts->count();
    $isFirstRound = true;

    try {

      try {

        if ($isFirstRound) $this->mandiriLoginner->login($driver, self::$information->accounts[$pointer]);

        $this->mandiriTransactionInquiryScrapper->transactionInquiry($driver, self::$information->accounts[$pointer]);

        if ($length > 1) {
          $this->mandiriLoginner->logout($driver);
        } else {
          $isFirstRound = false;
        }


        if ($pointer == $length - 1) {
          $pointer = 0;
        }

        sleep(1);

      } catch (UnexpectedAlertOpenException $exception) {

        try {
          DB::rollback();
        } catch (\Exception $exception) {
          \Log::info(\json_encode($exception));
        }

        $driver->switchTo()->alert()->accept();
        $driver->switchTo()->defaultContent();
        //sleep(60);

      } catch (LogicException $exception) {


        \DB::purge();
        \DB::connection();

      } catch (QueryException $exception) {

        \Log::info($exception->getTraceAsString());
        \Log::info($exception->getMessage());

        try {
          DB::rollback();
        } catch (\Exception $exception) {
          \Log::info(\json_encode($exception));
        }

        \DB::purge();
        \DB::connection();

      } catch (\PDOException $exception) {

        \Log::info($exception->getTraceAsString());
        \Log::info($exception->getMessage());

        try {
          DB::rollback();

        } catch (\Exception $exception) {
          \Log::info(\json_encode($exception));
        }

        \DB::purge();
        \DB::connection();

      } catch (\Exception $exception) {

        \Log::info($exception->getTraceAsString());
        \Log::info($exception->getMessage());

        try {
          DB::rollback();
        } catch (\Exception $exception) {
          \Log::info(\json_encode($exception));
        }

        try {
          DB::rollback();
        } catch (\Exception $exception) {
          \Log::info(\json_encode($exception));
        }

        $this->mandiriLoginner->logout($driver);

        //sleep(5);

      }

    } catch (\Exception $exception) {

      \Log::info($exception->getTraceAsString());
      \Log::info($exception->getMessage());

      try {
        DB::rollback();
      } catch (\Exception $exception) {
      }
    }

    try {
      $this->mandiriLoginner->logout($driver);
    } catch (\Exception $exception) {
    }
    $driver->close();
    $driver->quit();
    \Log::info('quit');

  }
}
