<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Mandiri\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;

class BankMandiriInquiry extends Entity {
  use SoftDeletes;

  protected $dates = ['date'];

}
