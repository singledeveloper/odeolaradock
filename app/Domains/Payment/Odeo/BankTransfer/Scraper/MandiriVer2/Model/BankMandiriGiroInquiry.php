<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 18/07/19
 * Time: 14.58
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Model;


use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;

class BankMandiriGiroInquiry extends Entity {

  use SoftDeletes;

  protected $dates = ['date', 'date_value'];

}