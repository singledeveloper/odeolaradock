<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 28/09/18
 * Time: 18.11
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Model;


use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;

class BankMandiriInquiry extends Entity {

  use SoftDeletes;

  protected $dates = ['date'];

}