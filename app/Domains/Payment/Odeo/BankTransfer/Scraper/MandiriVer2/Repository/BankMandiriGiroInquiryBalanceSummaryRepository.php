<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 18/07/19
 * Time: 15.25
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Helper\MandiriHelper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Model\BankMandiriGiroInquiryBalanceSummary;

class BankMandiriGiroInquiryBalanceSummaryRepository extends Repository {

  function __construct(BankMandiriGiroInquiryBalanceSummary $balanceSummary) {
    $this->model = $balanceSummary;
  }

  function findLatestSummary() {
    return $this->model
      ->orderBy('id', 'desc')
      ->first();
  }

  function findDistinctSummary() {
    return $this->model
      ->where('status', '=', MandiriHelper::DIFF_UNKNOWN)
      ->whereNull('first_distinct_id')
      ->first();
  }

  function updateDistinctRows() {
    return $this->model
      ->where('status', '=', MandiriHelper::DIFF_UNKNOWN)
      ->update(['status' => MandiriHelper::DIFF_OK]);
  }
}