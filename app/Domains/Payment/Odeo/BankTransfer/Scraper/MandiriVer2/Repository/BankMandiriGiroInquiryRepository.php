<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 18/07/19
 * Time: 14.58
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository;


use Illuminate\Support\Collection;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Model\BankMandiriGiroInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankInquiryRepository;

class BankMandiriGiroInquiryRepository extends BankInquiryRepository {

  function __construct(BankMandiriGiroInquiry $giroInquiry) {
    $this->model = $giroInquiry;
  }

  /**
   * @return Collection
   */
  function getInquiries($from, $to) {
    return $this->model->getModel()
      ->orderBy('date', 'asc')
      ->orderBy('id', 'asc')
      ->whereDate('date', '>=', $from)
      ->whereDate('date', '<=', $to)
      ->get();
  }

  function getSequencedInquiries($from, $to) {
    return $this->model->getModel()
      ->whereDate('date', '>=', $from)
      ->whereDate('date', '<=', $to)
      ->whereNotNull('sequence_number')
      ->orderBy('sequence_number', 'asc')
      ->get();
  }

  function findFirstUnsignedSequenceInquiry() {
    return $this->model
      ->whereNull('sequence_number')
      ->whereNotNull('balance')
      ->orderBy('date', 'asc')
      ->first();
  }

  function findLatestSequenceByDate($date) {
    return $this->model
      ->whereDate('date', '=', $date)
      ->whereNotNull('sequence_number')
      ->orderBy('sequence_number', 'desc')
      ->first();
  }

  function getInquiryByDate($date) {
    return $this->model
      ->whereDate('date', '>=', $date)
      ->whereNotNull('balance')
      ->get();
  }

  function getUnreconciledArtajasaSettlement() {
    return $this->model
      ->whereRaw("description LIKE 'HAK TFP%'")
      ->where('reference_type', 'note')
      ->orderBy('date', 'asc')
      ->get();
  }

  public function getUnreconciledVaPayment($vaCode) {
    return $this->model
      ->where('description', 'LIKE', "%$vaCode")
      ->where('credit', '>', 0)
      ->where(function($subquery){
        $subquery->whereNull('reference_type')
          ->orWhere('reference_type', 'note');
      })
      ->orderBy('id', 'asc')
      ->first();
  }
}