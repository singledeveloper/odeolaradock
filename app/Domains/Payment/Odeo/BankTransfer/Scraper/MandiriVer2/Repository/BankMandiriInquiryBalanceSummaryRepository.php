<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 02/10/18
 * Time: 12.50
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Helper\MandiriHelper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Model\BankMandiriInquiryBalanceSummary;

class BankMandiriInquiryBalanceSummaryRepository extends Repository {

  function __construct(BankMandiriInquiryBalanceSummary $balanceSummary) {
    $this->setModel($balanceSummary);
  }

  function findLatestSummary() {
    return $this->model
      ->orderBy('id', 'desc')
      ->first();
  }

  function findDistinctSummary() {
    return $this->model
      ->where('status', '=', MandiriHelper::DIFF_UNKNOWN)
      ->whereNull('first_distinct_id')
      ->first();
  }

  function updateDistinctRows() {
    return $this->model
      ->where('status', '=', MandiriHelper::DIFF_UNKNOWN)
      ->update(['status' => MandiriHelper::DIFF_OK]);
  }

}