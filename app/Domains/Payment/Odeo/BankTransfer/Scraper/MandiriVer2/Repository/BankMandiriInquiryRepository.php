<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 28/09/18
 * Time: 18.11
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository;


use Illuminate\Support\Collection;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Model\BankMandiriInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankInquiryRepository;

class BankMandiriInquiryRepository extends BankInquiryRepository {

  public function __construct(BankMandiriInquiry $mandiriInquiry) {
    $this->setModel($mandiriInquiry);
  }

  /**
   * @return Collection
   */
  function getInquiries($from, $to) {
    return $this->model->getModel()
      ->orderBy('date', 'asc')
      ->orderBy('id', 'asc')
      ->whereDate('date', '>=', $from)
      ->whereDate('date', '<=', $to)
      ->get();
  }

  function getSequencedInquiries($from, $to) {
    return $this->model->getModel()
      ->whereDate('date', '>=', $from)
      ->whereDate('date', '<=', $to)
      ->whereNotNull('sequence_number')
      ->orderBy('sequence_number', 'asc')
      ->get();
  }

  function findFirstUnsignedSequenceInquiry() {
    return $this->model
      ->whereNull('sequence_number')
      ->whereNotNull('balance')
      ->orderBy('date', 'asc')
      ->first();
  }

  function findLatestSequenceByDate($date) {
    return $this->model
      ->whereDate('date', '=', $date)
      ->whereNotNull('sequence_number')
      ->orderBy('sequence_number', 'desc')
      ->first();
  }

  function getInquiryByDate($date) {
    return $this->model
      ->whereDate('date', '>=', $date)
      ->whereNotNull('balance')
      ->get();
  }

}