<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/10/18
 * Time: 14.36
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Helper\MandiriHelper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriInquiryBalanceSummaryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriInquiryRepository;

class MandiriInquirySequenceScheduler {

  /**
   * @var BankMandiriInquiryRepository
   */
  private $mandiriInquiry;

  /**
   * @var BankMandiriInquiryBalanceSummaryRepository
   */
  private $mandiriSummary;
  private $currSequence;
  private $currBalance;
  private $date;

  private function initialize() {
    $this->mandiriInquiry = app()->make(BankMandiriInquiryRepository::class);
    $this->mandiriSummary = app()->make(BankMandiriInquiryBalanceSummaryRepository::class);
    $this->currSequence = 0;
    $this->currBalance = MandiriHelper::formatNumber(93098339.47);
    $this->date = Carbon::parse('2018-11-11');
  }

  public function run() {
    $this->initialize();

    if (!$this->mandiriSummary->findDistinctSummary()) {
      if ($unassignedIq = $this->mandiriInquiry->findFirstUnsignedSequenceInquiry()) {
        $this->date = $unassignedIq->date;

        if ($prevSequence = $this->mandiriInquiry->findLatestSequenceByDate($unassignedIq->date->subday(1))) {
          $this->currSequence = $prevSequence->sequence_number;
          $this->currBalance = MandiriHelper::formatNumber($prevSequence->balance);
        }
      }

      $inquiries = $this->mandiriInquiry->getInquiryByDate($this->date);

      if (count($inquiries->where('sequence_number', null))) {
        $groupedInquiries = $this->groupInquiries($inquiries);

        $updates = [];

        for ($i = 0; $i < count($inquiries); $i++) {
          $selectedInquiry = $this->getInquiryByBalance($groupedInquiries, $this->currBalance);
          if (!$selectedInquiry) {
            break;
          }

          $this->currSequence++;
          $this->currBalance = MandiriHelper::formatNumber($selectedInquiry['balance']);

          if ($selectedInquiry['sequence_number'] == $this->currSequence) {
            continue;
          }

          $updates[] = [
            'id' => $selectedInquiry['id'],
            'sequence_number' => $this->currSequence,
          ];
        }

        if (count($updates)) {
          $this->mandiriInquiry->updateBulk($updates);
        }

        return $this->currBalance;
      }
    }
  }

  private function groupInquiries($inquiries) {
    $groupInquiries = $inquiries->groupBy(function ($inquiry) {
      return MandiriHelper::formatNumber($inquiry['balance'] - $inquiry['credit'] + $inquiry['debit']);
    });

    return $groupInquiries->transform(function ($inquiries) {
      return $inquiries->sortBy('date');
    });
  }

  private function getInquiryByBalance(&$groupedInquiries, $balance) {
    if (!isset($groupedInquiries[$balance])) {
      return null;
    }

    $inquiries = $groupedInquiries[$balance];
    $selectedInquiry = $inquiries->shift();

    if (count($inquiries) == 0) {
      unset($groupedInquiries[$balance]);
    }

    return $selectedInquiry;
  }



}