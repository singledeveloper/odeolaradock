<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/10/18
 * Time: 15.51
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Scheduler;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Helper\MandiriHelper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Job\ReconMandiriAccountStatementJob;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;

class ReconMandiriStatementScheduler {

  /**
   * @var BankScrapeAccountNumberRepository
   */
  private $scrapeAccountNo;

  private function initialize() {
    $this->scrapeAccountNo = app()->make(BankScrapeAccountNumberRepository::class);
  }

  public function run() {
    $this->initialize();

    $number = $this->scrapeAccountNo->findByName('mandiri');
    $startDate = $number->last_scrapped_date->subDay(1);
    $endDate = $this->getEndDate($startDate);

    dispatch(new ReconMandiriAccountStatementJob($startDate, $endDate, MandiriHelper::$Account_Default, $endDate->isSameDay(Carbon::now())));
  }

  private function getEndDate(Carbon $startDate) {
    if ($startDate->diffInDays(Carbon::now()) > 30) {
      return $startDate->copy()->addDays(30);
    }
    return Carbon::now();
  }

}
