<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 13/12/19
 * Time: 12.49
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Maybank\Repository;


use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Maybank\Model\BankMaybankInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankInquiryRepository;

class BankMaybankInquiryRepository extends BankInquiryRepository {

  function __construct(BankMaybankInquiry $maybankInquiry) {
    $this->model = $maybankInquiry;
  }

}