<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Model\BankBcaInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bni\Model\BankBniInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Model\BankBriInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Model\BankCimbInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Mandiri\Model\BankMandiriInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Model\BankMandiriGiroInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Maybank\Model\BankMaybankInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Model\BankPermataInquiry;

class BankScrapeAccountNumber extends Entity {

  protected $dates = ['last_time_scrapped', 'last_scrapped_date'];

  protected $hidden = ['created_at', 'updated_at', 'active', 'last_scrapped_date'];

  public function bankScrapeAccount() {
    return $this->belongsTo(BankScrapeAccount::class);
  }

  public function mandiriInquiries() {
    return $this->hasMany(BankMandiriInquiry::class);
  }

  public function mandiri_giroInquiries() {
    return $this->hasMany(BankMandiriGiroInquiry::class);
  }

  public function bcaInquiries() {
    return $this->hasMany(BankBcaInquiry::class);
  }

  public function briInquiries() {
    return $this->hasMany(BankBriInquiry::class);
  }

  public function cimbInquiries() {
    return $this->hasMany(BankCimbInquiry::class);
  }

  public function bniInquiries() {
    return $this->hasMany(BankBniInquiry::class);
  }

  public function permataInquiries() {
    return $this->hasMany(BankPermataInquiry::class);
  }

  public function maybankInquiries() {
    return $this->hasMany(BankMaybankInquiry::class);
  }
}
