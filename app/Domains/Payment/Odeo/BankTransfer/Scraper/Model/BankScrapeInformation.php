<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model;

use Odeo\Domains\Core\Entity;

class BankScrapeInformation extends Entity
{
  protected $primaryKey = 'target';

  public $incrementing = false;

  protected $hidden = ['created_at', 'updated_at', 'url'];

  public function accounts()
  {
    return $this->hasMany(BankScrapeAccount::class, 'referred_target', 'target');
  }
  
}
