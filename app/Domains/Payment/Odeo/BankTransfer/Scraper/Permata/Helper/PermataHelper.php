<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/04/19
 * Time: 15.49
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Helper;


class PermataHelper {

  const URL_REPORT = 'https://www.permatae-business.com/reporting/html';
  const URL_TRX_INQUIRY = 'https://www.permatae-business.com/corp/front/transactioninquiry.do';
  const URL_BALANCE_INQUIRY = 'https://www.permatae-business.com/corp/front/balanceinquiry.do';

  const PERMATA_REDIS_KEY = 'odeo_core:permata_recon_account_statement';
  const REDIS_RECON_ERROR_COUNT = 'odeo_core:permata_recon_error_count';

  const STATUS_OK = '50000';
  const STATUS_DIFF = '90000';

}