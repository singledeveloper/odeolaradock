<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/04/19
 * Time: 15.10
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Model;


use Odeo\Domains\Core\Entity;

class BankPermataInquiry extends Entity {

  protected $dates = ['post_date', 'eff_date'];

}