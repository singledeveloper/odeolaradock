<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/04/19
 * Time: 14.31
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata;


use Carbon\Carbon;
use GuzzleHttp\Client;
use Odeo\Domains\Constant\BankAccount;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Helper\PermataHelper;
use Symfony\Component\DomCrawler\Crawler;

class PermataAccountStatementScraper {

  public function scrapeAccountStatement(Client $client, Carbon $startDate, Carbon $endDate, $accountNo) {
    $client->request('GET', PermataHelper::URL_TRX_INQUIRY, [
      'query' => [
        'action' => 'transactionByDateRequest',
        'menuCode' => 'MNU_GCME_040200',
      ]
    ]);

    clog('recon_permata', 'getting transactions');

    $res = $client->request('GET', PermataHelper::URL_TRX_INQUIRY, [
      'query' => [
        'action' => 'getListTransactionByDate',
        'accountNumber' => $accountNo,
        'accountNm' => 'ODEO TEKNOLOGI INDON',
        'currDisplay' => 'IDR',
        'day1' => $startDate->day,
        'mon1' => $startDate->month,
        'year1' => $startDate->year,
        'day2' => $endDate->day,
        'mon2' => $endDate->month,
        'year2' => $endDate->year,
        'trxFilter' => '%',
      ]
    ]);

    preg_match('/jrprint = \'(.*)\'/', $res->getBody(), $matches); // transaction list

    $res = $client->request('POST', PermataHelper::URL_REPORT, [
      'form_params' => [
        'jrprint' => $matches[1],
      ],
      'timeout' => 50
    ]);

    $crawler = new Crawler($res->getBody()->getContents());
    $result = [];
    $balance = 0;

    $now = Carbon::now();
    
    clog('recon_permata', 'scraping transactions');

    $crawler->filter('tr[valign="top"]')->each(function(Crawler $row) use (&$result, &$balance, $now) {

      $columns = $row->filter('td');
      $count = $columns->count();
      $text = $count > 1 ? $this->getNodeValue($columns, 1) : '';
      $validText =  in_array($text, ['Opening Ledger Balance per', 'Ledger Balance per']);

      if ($count == 5 && $validText) {
        $balance = $this->formatNumber($this->getNodeValue($columns, 3));
      } else if ($count == 7 && $validText) {
        $balance = $this->formatNumber($this->getNodeValue($columns, 5));
      } else if ($count == 12 && $text != 'No.') {
        $debit = $this->formatNumber($this->getNodeValue($columns, 9));
        $credit = $this->formatNumber($this->getNodeValue($columns, 10));
        $balance += $credit - $debit;
        $result[] = [
          'scrape_sequence_no' => $text,
          'post_date' => Carbon::parse($this->getNodeValue($columns, 2)),
          'eff_date' => Carbon::parse($this->getNodeValue($columns, 3)),
          'trx_code' => $this->getNodeValue($columns, 4),
          'cheque_no' => $this->getNodeValue($columns, 5),
          'reff_no' => $this->getNodeValue($columns, 6),
          'cust_reff_no' => $this->getNodeValue($columns, 7),
          'description' => $this->getNodeValue($columns, 8),
          'debit' => $debit,
          'credit' => $credit,
          'balance' => number_format($balance, 2, '.', ''),
          'bank_scrape_account_number_id' => BankAccount::PERMATA_ODEO,
          'created_at' => $now,
          'updated_at' => $now
        ];
      }
    });

    return collect($result);
  }

  public function scrapeBalanceStatement(Client $client, Carbon $date, $accountNo) {
    $res = $client->request('GET', PermataHelper::URL_BALANCE_INQUIRY, [
      'query' => [
        'action' => 'getReportAccountInquiryByDate',
        'accountPick' => 'single',
        'accountNumber' => $accountNo,
        'accountType' => 'D',
        'accountCurrency' => 'IDR',
        'accountAlias' => 'ODEO TEKNOLOGI INDON',
        'accountBranch' => 'DEF',
        'accountCorpName' => 'ODEO TEKNOLOGI INDONESIA PT',
        'acctTypeNm' => 'Giro',
        'menu' => 'search',
        'tabName' => 'Balance Inquiry',
        'fromDateYear' => $date->year,
        'fromDateMonth' => $date->month,
        'fromDateDay' => $date->day,
        'toDateYear' => $date->year,
        'toDateMonth' => $date->month,
        'toDateDay' => $date->day,
        'chooseGroupBy' => 'Date',
      ]
    ]);

    preg_match('/jrprint = "(.*)"/', $res->getBody(), $matches);
    $res = $client->request('POST', PermataHelper::URL_REPORT, [
      'form_params' => [
        'jrprint' => $matches[1],
      ]
    ]);

    $summary = [];

    $crawler = new Crawler($res->getBody()->getContents());
    $crawler->filter('tr[valign="top"]')->each(function(Crawler $row) use (&$summary) {
      if (empty($summary)) {
        $columns = $row->filter('td');
        $validCount = $columns->count() == 9;
        $validRow = $validCount && $this->getNodeValue($columns, 1) == 'Total';

        if ($validCount && $validRow) {
          $summary = [
            'scrape_ledger_balance' => $this->formatNumber($this->getNodeValue($columns, 3)),
            'scrape_effective_balance' => $this->formatNumber($this->getNodeValue($columns, 4)),
            'scrape_ineffective_balance' => $this->formatNumber($this->getNodeValue($columns, 6))
          ];
        }
      }
    });

    return $summary;
  }

  private function formatNumber($string) {
    return str_replace(',', '', $string);
  }

  private function getNodeValue(Crawler $node, $index) {
    return $node->getNode($index)->textContent;
  }

}