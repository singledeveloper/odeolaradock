<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/04/19
 * Time: 18.33
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Helper\PermataHelper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Model\BankPermataInquiryBalanceSummary;

class BankPermataInquiryBalanceSummaryRepository extends Repository {

  function __construct(BankPermataInquiryBalanceSummary $balanceSummary) {
    $this->model = $balanceSummary;
  }

  public function findLatestSummary() {
    return $this->model->orderBy('id', 'desc')->first();
  }

  public function findDistinctSummary() {
    return $this->model->where('status', '=', PermataHelper::STATUS_DIFF)
      ->whereNull('first_distinct_id')
      ->first();
  }

  public function updateDistinctRow() {
    $this->getModel()
      ->where('status', '=', PermataHelper::STATUS_DIFF)
      ->update([
        'status' => PermataHelper::STATUS_OK
      ]);
  }

}