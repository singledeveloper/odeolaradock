<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 02/04/19
 * Time: 16.22
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Job\ReconPermataAccountStatement;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Job\ReconPermataApiAccountStatement;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;

class ReconPermataStatementScheduler {

  /**
   * @var BankScrapeAccountNumberRepository
   */
  private $scrapeAccountNo;

  private function init() {
    $this->scrapeAccountNo = app()->make(BankScrapeAccountNumberRepository::class);
  }

  public function run() {
    $this->init();
    $number = $this->scrapeAccountNo->findByName('permata');
    $startDate = $number->last_scrapped_date->subday(1);
    $endDate = $this->getEndDate($startDate);

//    dispatch(new ReconPermataAccountStatement($startDate, $endDate, $endDate->isSameDay(Carbon::now())));
    dispatch(new ReconPermataApiAccountStatement($startDate, $endDate));
  }

  private function getEndDate(Carbon $date) {
    if ($date->diffInDays(Carbon::now()) > 90) {
      return $date->copy()->addDay(90);
    }
    return Carbon::now();
  }

}