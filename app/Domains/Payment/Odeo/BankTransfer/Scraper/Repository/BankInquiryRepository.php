<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;

class BankInquiryRepository extends Repository {

  public function findByOrderId($orderId) {
    return $this->model
      ->where('reference_type', 'order_id')
      ->where('reference', 'like', '%"' . $orderId . '"%')
      ->first();
  }

  function findLastSequenceDataBeforeDate($date) {
    return $this->model->getModel()
      ->orderBy('sequence_number', 'desc')
      ->whereNotNull('sequence_number')
      ->whereNotNull('balance')
      ->whereDate('date', '<', Carbon::parse($date))
      ->first();
  }

  function getLastSequenceData() {
    return $this->model->getModel()
      ->orderBy('sequence_number', 'desc')
      ->whereNotNull('sequence_number')
      ->whereNotNull('balance')
      ->first();
  }

  function getLastSequenceDataBeforeDate($date) {
    return $this->model->getModel()
      ->orderBy('sequence_number', 'desc')
      ->where('date', '<', Carbon::parse($date)->endOfDay())
      ->whereNotNull('sequence_number')
      ->whereNotNull('balance')
      ->first();
  }

  function hasUnassignSequenceNumberBeforeDate($date) {
    return $this->model->getModel()
      ->where('date', '<', Carbon::parse($date)->endOfDay())
      ->whereNull('sequence_number')
      ->whereNotNull('balance')
      ->orderBy('date', 'asc')
      ->first();
  }

}
