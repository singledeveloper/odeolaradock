<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 12/27/16
 * Time: 12:44 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Job\SendBankMutationScraperAlert;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;

class CheckBankMutationScraperScheduler {

  private $bankScrapeAccountNumbers;

  public function __construct() {
    $this->bankScrapeAccountNumbers = app()->make(BankScrapeAccountNumberRepository::class);
  }

  public function run() {
    $accountNumberList = $this->bankScrapeAccountNumbers->getModel()
      ->join('bank_scrape_accounts', 'bank_scrape_accounts.id', '=', 'bank_scrape_account_numbers.bank_scrape_account_id')
      ->where('bank_scrape_accounts.active', true)
      ->get();

    $now = Carbon::now();
    foreach ($accountNumberList as $account) {
      if (Carbon::parse($account->last_time_scrapped)->diffInMinutes(Carbon::now()) >= 20) {
        if ($account->referred_target == 'bca'
          && ($now->hour >= 21 || $now->hour <= 1)) {
          continue;
        } else if ($account->referred_target == 'mandiri'
          && (($now->hour >= 22 && $now->minute >= 30) || $now->hour < 3)) {
          continue;
        } else if ($account->referred_target == 'bri'
          && (($now->hour >= 23 && $now->minute >= 30) || $now->hour < 6)) {
          continue;
        }

        dispatch(new SendBankMutationScraperAlert($accountNumberList->toArray()));

        return;
      }

    }
  }

}
