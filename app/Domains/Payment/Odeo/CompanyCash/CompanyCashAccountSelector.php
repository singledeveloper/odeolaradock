<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 06/06/18
 * Time: 15.32
 */

namespace Odeo\Domains\Payment\Odeo\CompanyCash;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Payment\Odeo\CompanyCash\Repository\CompanyCashAccountRepository;

class CompanyCashAccountSelector implements SelectorListener {

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function get(PipelineListener $listener, $data) {

    $companyCashAccount = app()->make(CompanyCashAccountRepository::class);

    $companyAccounts = array();

    foreach ($companyCashAccount->gets() as $item) {
      $account = $this->_transforms($item, $companyCashAccount);
      $account->user_telephone = revertTelephone($account->user_telephone);
      $companyAccounts[] = $account;
    }

    if (count($companyAccounts) > 0) {
      return $listener->response(200, array_merge(['accounts' => $this->_extends($companyAccounts, $companyCashAccount)], $companyCashAccount->getPagination()));
    }

    return $listener->response(204);
  }
}