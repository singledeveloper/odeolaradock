<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/06/18
 * Time: 15.07
 */

namespace Odeo\Domains\Payment\Odeo\CompanyCash;

use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Carbon\Carbon;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Accounting\CompanyTransactionInformation\Repository\CompanyTransactionAdditionalInformationRepository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class CompanyCashTransactionSelector implements SelectorListener {

  private $cashTransactionRepo, $userRepo, $companyAdditionalRepo;

  public function __construct() {
    $this->cashTransactionRepo = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
    $this->userRepo = app()->make(UserRepository::class);
    $this->companyAdditionalRepo = app()->make(CompanyTransactionAdditionalInformationRepository::class);
  }

  public function _extends($data, Repository $repository) {
    if ($cashTransactionIds = $repository->beginExtend($data, "id")) {
      $result = [];
      foreach ($this->companyAdditionalRepo->getInformationCountByTransactionIds($cashTransactionIds) as $key => $item) {
        $result[$key] = $item->total_count;
      }
      $repository->addExtend('information_count', $result);
    }
    return $repository->finalizeExtend($data);
  }

  public function _transforms($item, Repository $repository) {
    return [
      'id' => $item->id,
      'user_id' => $item->user_id,
      'store_id' => $item->store_id,
      'trx_type' => $item->trx_type,
      'cash_type' => $item->cash_type,
      'credit' => $item->amount > 0 ? $item->amount : 0,
      'debit' => $item->amount < 0 ? abs($item->amount) : 0,
      'data' => $item->data,
      'balance_before' => $item->balance_before,
      'balance_after' => $item->balance_after,
      'trx_month' => $item->trx_month,
      'trx_year' => $item->trx_year,
      'order_id' => $item->order_id,
      'reference_type' => $item->reference_type,
      'reference_id' => $item->reference_id,
      'created_at' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
      'updated_at' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : ''
    ];
  }

  public function getTransactions(PipelineListener $listener, $data) {
    $companyCashAccount = app()->make('Odeo\\Domains\\Payment\\Odeo\\CompanyCash\\Repository\\CompanyCashAccountRepository');

    $this->cashTransactionRepo->normalizeFilters($data);
    $this->cashTransactionRepo->setSimplePaginate(true);

    if (!$account = $companyCashAccount->findById($data['id'])) {
      return $listener->response(400);
    }

    $user = $this->userRepo->findById($account->user_id);

    $transactions = [];
    foreach ($this->cashTransactionRepo->getByUserId($account->user_id) as $item) {
      $transactions[] = $this->_transforms($item, $this->cashTransactionRepo);
    }

    if (count($transactions) > 0) {
      return $listener->response(200, array_merge([
        'transactions' => $this->_extends($transactions, $this->cashTransactionRepo),
        'company_name' => $user->name
      ], $this->cashTransactionRepo->getPagination()));
    }

    return $listener->response(204);
  }

  public function exportTransaction($data) {

    $companyCashAccount = app()->make('Odeo\\Domains\\Payment\\Odeo\\CompanyCash\\Repository\\CompanyCashAccountRepository');

    $this->cashTransactionRepo->normalizeFilters($data);
    $account = $companyCashAccount->findById($data['id']);
    $transactions = $this->cashTransactionRepo->getByUserId($account->user_id);

    $user = $this->userRepo->findById($account->user_id);

    $writer = WriterFactory::create(Type::CSV);
    $writer->openToBrowser($user->name . ' ' . ($data['start_date'] ?? '0000-00-00') . ' to ' . ($data['end_date'] ?? Carbon::now()->toDateString()) . '.csv');

    $headers = [
      'id', 'user_id', 'store_id', 'trx_type', 'cash_type', 'credit', 'debit', 'data', 'balance_before', 'balance_after', 'trx_month', 'trx_year', 'order_id', 'reference_type', 'reference_id', 'created_at', 'updated_at'
    ];

    $writer->addRow($headers);

    foreach ($transactions as $item) {
      $temp = [];
      $transaction = $this->_transforms($item, $this->cashTransactionRepo);

      foreach ($headers as $h) {
        $temp[] = $transaction[$h];
      }

      $writer->addRow($temp);
    }

    $writer->close();
  }
}