<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 08/06/18
 * Time: 20.00
 */

namespace Odeo\Domains\Payment\Odeo\CompanyCash;


use Carbon\Carbon;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Core\PipelineListener;

class CompanyCashTransactionUpdater {

  private $cashTransactionRepo;

  public function __construct() {
    $this->cashTransactionRepo = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
  }

  public function updateTransactions(PipelineListener $listener, $data) {
    $companyCashAccounts = app()->make('Odeo\\Domains\\Payment\\Odeo\\CompanyCash\\Repository\\CompanyCashAccountRepository');
    $cashTransactions = app()->make('Odeo\\Domains\\Transaction\\Repository\\CashTransactionRepository');

    $accounts = $companyCashAccounts->gets();
    $now = Carbon::now();

    foreach ($accounts as $account) {
      $transactions = $cashTransactions->getCloneModel()->where('user_id', '=', $account->user_id);

      if (isset($account->last_transaction_update)) {
        $transactions = $transactions->where('created_at', '>=', $account->last_transaction_update);
      }

      $transactions = $transactions
        ->where('cash_type', CashType::OCASH)
        ->selectRaw($account->id . " AS company_cash_account_id,
          cash_transactions.id as cash_transaction_id,
          cash_transactions.user_id,
          store_id,
          trx_type,
          cash_type,
          CASE WHEN amount > 0 THEN amount ELSE 0 END AS credit,
          CASE WHEN amount < 0 THEN abs(amount) ELSE 0 END AS debit,
          data,
          balance_before,
          balance_after,
          trx_month,
          trx_year,
          order_id,
          CASE WHEN order_id ISNULL THEN NULL ELSE 'order_id' END AS reference_type,
          order_id AS reference,
          created_at AS transaction_date,
          '" . $now . "' AS created_at,
          '" . $now . "' AS updated_at")
        ->orderBy('cash_transaction_id', 'asc');

      \DB::insert('INSERT INTO company_cash_account_transactions (
        company_cash_account_id,
        cash_transaction_id,
        user_id,
        store_id,
        trx_type,
        cash_type,
        credit,
        debit,
        data,
        balance_before,
        balance_after,
        trx_month,
        trx_year,
        order_id,
        reference_type,
        reference,
        transaction_date,
        created_at,
        updated_at
      )' . $transactions->toSql(), $transactions->getBindings());
    }

    \DB::table('company_cash_accounts')->update(['last_transaction_update' => $now]);

    return $listener->response(200);

  }

  public function updateTransactionReference(PipelineListener $listener, $data) {
    if (!$transaction = $this->cashTransactionRepo->findById($data['transaction_id'])) {
      return $listener->response(400);
    }

    $transaction->reference_id = $data['reference'];
    $transaction->reference_type = $data['reference_type'];

    $transaction->save();

    return $listener->response(200);
  }

}