<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 08/06/18
 * Time: 20.04
 */

namespace Odeo\Domains\Payment\Odeo\CompanyCash\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\CompanyCash\Model\CompanyCashAccountTransaction;

class CompanyCashAccountTransactionRepository extends Repository {

  public function __construct(CompanyCashAccountTransaction $companyCashAccountTransaction) {
    $this->model = $companyCashAccountTransaction;
  }

  public function getTransactions($cashAccountId) {
    $filters = $this->getFilters();

    $query = $this->getCloneModel()->where('company_cash_account_id', '=', $cashAccountId);

    if (isset($filters['start_date'])) $query->whereDate('transaction_date', '>=', $filters['start_date']);
    if (isset($filters['end_date'])) $query->whereDate('transaction_date', '<=', $filters['end_date']);

    if (isset($filters['search'])) {
      if (isset($filters['search']['id'])) {
        $query->where('id', $filters['search']['id']);
      }

      if (isset($filters['search']['reference_type'])) {
        $query->where('reference_type', $filters['search']['reference_type']);
      }

      if (isset($filters['search']['reference'])) {
        $query->where('reference', 'ilike', '%' . $filters['search']['reference'] . '%');
      }

      if (isset($filters['search']['data'])) {
        $query->where('data', 'ilike', '%' . $filters['search']['data'] . '%');
      }

      if (isset($filters['search']['trx_type'])) {
        $query->where('trx_type', 'ilike', '%' . $filters['search']['trx_type'] . '%');
      }

    }

    return $this->getResult($query->orderBy('cash_transaction_id', 'desc'));
  }

}