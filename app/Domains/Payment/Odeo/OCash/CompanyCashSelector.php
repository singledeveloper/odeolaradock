<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 06/06/18
 * Time: 13.47
 */

namespace Odeo\Domains\Payment\Odeo\OCash;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class CompanyCashSelector implements SelectorListener {

  public function __construct() {
    $this->cashTransactions = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function get(PipelineListener $listener, $data) {
    $this->cashTransactions->normalizeFilters($data);
    $this->cashTransactions->setSimplePaginate(true);

    foreach ($this->cashTransactions->getCompanyCashMutation() as $item) {

    }
  }

}