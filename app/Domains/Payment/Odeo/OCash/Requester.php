<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 12/7/16
 * Time: 8:50 PM
 */

namespace Odeo\Domains\Payment\Odeo\OCash;


use Odeo\Domains\Constant\ErrorStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\PipelineListener;

class Requester {

  public function request(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {
    if ($data['auth']['type'] == 'guest') {
      return $listener->response(400, 'Login terlebih dahulu untuk menggunakan metode pembayaran pay with oCash');
    }

    $cashes = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);

    $balance = $cashes->getCashBalance($data["auth"]["user_id"]);
    if (isset($balance[$data["auth"]["user_id"]]) && $balance[$data["auth"]["user_id"]] < $order->total) {
      return $listener->responseWithErrorStatus(ErrorStatus::BALANCE_NOT_ENOUGH, trans('errors.cash.insufficient_cash', ['cash' => 'oCash']));
    }

    $cashes = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

    return $listener->response(200, [
      'type' => Payment::REQUEST_PAYMENT_TYPE_VERIFY_PIN,
      'content' => [
        'current_cash_balance' => $cashes->formatPrice($balance[$data["auth"]["user_id"]])
      ],
      //temp
      'current_cash_balance' => $cashes->formatPrice($balance[$data["auth"]["user_id"]])
    ]);


  }

}
