<?php

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Doku;

use Carbon\Carbon;
use Goutte\Client as GoutteClient;
use GuzzleHttp\Client;
use Odeo\Exceptions\FailException;
use Symfony\Component\DomCrawler\Crawler;

class DokuTransactionScraper {

  private $client;

  public function __construct() {
    $this->client = new GoutteClient();
    $this->client->setClient(new Client(['cookies' => true]));
  }

  public function login() {
    $this->client->request('POST', 'https://bo.doku.com/v2/signin', [
      'continues' => 'https://merchant.doku.com/acc/',
      'login' => env('DOKU_USERID'),
      'password' => env('DOKU_PASSWORD')
    ]);

    $isLoginFailed = $this->client->getInternalResponse()->getStatus() !== 200
      || json_decode($this->client->getResponse()->getContent())->code !== '000';

    if ($isLoginFailed) {
      throw new FailException('Doku Login Error');
    }
  }

  private function parseTransactionDetailValue($str) {
    if ($str === '-') {
      return null;
    }
    return $str;
  }

  public function scrape($startDate, $fetchedTransactions) {
    $endDate = Carbon::now()->format('Y-m-d');
    $startDate = $startDate->format('Y-m-d');

    $response = [
      'mutations_insert' => collect([]),
      'mutations_update' => collect([])
    ];

    $this->login();

    $this->client->request('GET', 'https://merchant.doku.com/acc/dashboard');

    $this->client->request(
      'POST',
      'https://merchant.doku.com/acc/ajax/search-new',
      [
        'account_id' => '',
        'edate' => $endDate,
        'email' => '',
        'invoice' => '',
        'name' => '',
        'params' => '',
        'payment_method' => '',
        'scurrency' => '',
        'sdate' => $startDate,
        'service' => '',
        'settlement_date' => '',
        'store' => '',
        'transaction_status' => ['success', 'refund_success', 'void'],
        'type' => 'summary-transactions'
      ]
    );

    $res = $this->client->getResponse()->getContent();
    clog('doku_scraper', json_decode($res));
    $transactionSummary = json_decode($res)[0];
    $lastPage = $transactionSummary->last_page;
    $page = 1;

    do {
      $pageParams = urldecode(http_build_query([
        'ttype' => 'all',
        'page' => $page,
        'lastpage' => $lastPage,
        's_invoice' => '',
        's_name' => '',
        's_sdate' => $startDate,
        's_edate' => $endDate,
        's_transaction_status' => 'success,refund_success,void',
        's_payment_method' => 'null',
        's_email' => '',
        's_store' => '',
        's_currency' => 'null'
      ]));

      getTransactions:
      $crawler = $this->client->request(
        'POST',
        'https://merchant.doku.com/acc/ajax/transactions-more-new',
        [
          'params' => $pageParams,
          'type' => 'transactions'
        ]
      );

      if ($crawler->filter('tr')->count() == 0) {
        \Log::info($page);
        goto getTransactions;
      }

      $crawler->filter('tr')->each(function (Crawler $node) use ($fetchedTransactions, &$response) {
        $nodeValues = $node->filter('td');
        $transactionId = $nodeValues->eq(0)->text();
        $status = $nodeValues->eq(1)->text();
        preg_match('/data-id="(.*?)"/', $node->html(), $detailId);
        $detailId = $detailId[1];
        $isUpdate = false;

        if (isset($fetchedTransactions[$transactionId])) {
          if ($fetchedTransactions[$transactionId]->status != $status) {
            return;
          } else {
            $isUpdate = true;
          }
        }

        getTransactionDetail:
        $crawler = $this->client->request(
          'POST',
          'https://merchant.doku.com/acc/ajax/transactions-detail-new',
          [
            'id' => $detailId
          ]
        );

        $detailNodes = $crawler->filter('div#transactiondata div.row:first-child div.value-item');

        if ($detailNodes->count() == 0) {
          goto getTransactionDetail;
        }

        $now = Carbon::now();
        $transactionDetail = [
          'order_id' => $transactionId,
          'status' => $status,
          'response_code' => $nodeValues->eq(2)->text(),
          'currency' => $nodeValues->eq(3)->text(),
//          'store_id' => $detailNodes->eq(0)->text(),
          'transaction_date_time' => $detailNodes->eq(1)->text(),
          'payment_channel' => $detailNodes->eq(2)->text(),
          'transaction_account' => $detailNodes->eq(3)->text(),
          'issuing_bank' => $detailNodes->eq(4)->text(),
          'approval_code' => $detailNodes->eq(5)->text(),
          'response_message' => $detailNodes->eq(6)->text(),
          'void_response_code' => $this->parseTransactionDetailValue($detailNodes->eq(7)->text()),
          'chain_merchant_id' => $detailNodes->eq(8)->text(),
          'services' => $detailNodes->eq(9)->text(),
          'session_id' => $detailNodes->eq(10)->text(),
          'settlement_status' => $detailNodes->eq(11)->text(),
          'settlement_date' => $this->parseTransactionDetailValue($detailNodes->eq(12)->text()),
          'payment_date_time' => $detailNodes->eq(13)->text(),
          'total_payment' => filter_var($crawler->filter('.big-value-stat div.value-stat.font-reg')->text(), FILTER_SANITIZE_NUMBER_INT),
          'completion_date' => $this->parseTransactionDetailValue($detailNodes->eq(14)->text()),
          'redirect_date_time' => $this->parseTransactionDetailValue($detailNodes->eq(15)->text()),
          'expiry_date' => $detailNodes->eq(16)->text(),
          'inquiry_date_time' => $this->parseTransactionDetailValue($detailNodes->eq(17)->text()),
          'void_date_time' => $this->parseTransactionDetailValue($detailNodes->eq(18)->text()),
          'additional_data' => $this->parseTransactionDetailValue($detailNodes->eq(19)->text()),
          'updated_at' => $now
        ];

        if (!$isUpdate) {
          $transactionDetail['created_at'] = $now;
        }

        $selectedKey = $isUpdate ? 'mutations_update' : 'mutations_insert';
        $response[$selectedKey]->prepend($transactionDetail);
      });

      $page++;

    } while ($page <= $lastPage);

    return $response;

  }
}


