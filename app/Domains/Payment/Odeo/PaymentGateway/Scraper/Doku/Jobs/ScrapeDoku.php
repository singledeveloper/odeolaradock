<?php

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Doku\Jobs;

use Carbon\Carbon;
use Odeo\Domains\Constant\PaymentGatewayAccount;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Doku\DokuTransactionScraper;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Doku\Repository\PaymentGatewayDokuMutationsRepository;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Repository\PaymentGatewayAccountRepository;
use Odeo\Jobs\Job;

class ScrapeDoku extends Job {

  private $dokuTransactionScraper;
  private $paymentGatewayAccountRepo;
  private $paymentGatewayDokuMutationsRepo;

  public function __construct() {
    parent::__construct();
  }

  private function initialize() {
    $this->dokuTransactionScraper = app()->make(DokuTransactionScraper::class);
    $this->paymentGatewayAccountRepo = app()->make(PaymentGatewayAccountRepository::class);
    $this->paymentGatewayDokuMutationsRepo = app()->make(PaymentGatewayDokuMutationsRepository::class);
  }

  public function handle() {
    $this->initialize();

    $account = $this->paymentGatewayAccountRepo->findById(PaymentGatewayAccount::DOKU);

    if (isset($account->last_scraped_time)) {
      $lastScrapped = (new Carbon($account->last_scraped_time))->subDays(1);
    } else {
      $lastScrapped = Carbon::createFromDate(2018, 4, 1);
    }

    $fetchedTransactions = $this->paymentGatewayDokuMutationsRepo->getAllFromDate($lastScrapped->startOfDay())
      ->keyBy('order_id');

    $response = $this->dokuTransactionScraper->scrape($lastScrapped, $fetchedTransactions);

    if ($response) {

      list ($mutationInserts, $mutationUpdates) = array_values($response);

      if (!$mutationInserts->isEmpty()) {
        $this->paymentGatewayDokuMutationsRepo->saveBulk($mutationInserts->all());
      }

      if (!$mutationUpdates->isEmpty()) {
        $this->paymentGatewayDokuMutationsRepo->updateBulk($mutationUpdates->all(), 'order_id');
      }

      $account->last_scraped_time = Carbon::now();
      $this->paymentGatewayAccountRepo->save($account);
    }
  }

}
