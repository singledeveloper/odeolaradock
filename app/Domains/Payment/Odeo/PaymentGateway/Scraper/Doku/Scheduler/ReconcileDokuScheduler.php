<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 25/10/18
 * Time: 12.21
 */

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Doku\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Doku\Jobs\SendEmailDokuReconciliationAlert;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Doku\Repository\PaymentGatewayDokuMutationsRepository;

class ReconcileDokuScheduler {

  /**
   * @var PaymentGatewayDokuMutationsRepository
   */
  private $dokuMutationRepo;

  private function initialize() {
    $this->dokuMutationRepo = app()->make(PaymentGatewayDokuMutationsRepository::class);
  }

  public function run() {
    $this->initialize();
    $startDate = Carbon::now()->subDay(14)->format('Y-m-d');
    $orders = $this->dokuMutationRepo->getReconciliationFromDate($startDate);

    $odeoOnly = $orders->where('doku_order_id', null);
    $dokuOnly = $orders->where('odeo_order_id', null);

    $res = $orders->reduce(function ($prev, $curr) {

      if ($this->statusMismatch($curr)) {
        $prev['status_not_match'][] = $curr;
      }

      if ($this->amountMismatch($curr)) {
        $prev['wrong_amount'][] = $curr;
      }

      return $prev;
    }, []);


    if (!empty($odeoOnly) || !empty($dokuOnly) || !empty($res)) {
      clog('doku', json_encode($odeoOnly));
      clog('doku', json_encode($dokuOnly));
      clog('doku', json_encode($res));

      dispatch(new SendEmailDokuReconciliationAlert([
        'odeoOnly' => $odeoOnly ?? [],
        'dokuOnly' => $dokuOnly ?? [],
        'wrongAmounts' => $res['wrong_amount'] ?? [],
        'statusNotMatch' => $res['status_not_match'] ?? []
      ]));
//      return view('emails.email_doku_reconciliation_alert', [
//        'odeoOnly' => $odeoOnly ?? [],
//        'dokuOnly' => $dokuOnly ?? [],
//        'wrongAmounts' => $res['wrong_amount'] ?? [],
//        'statusNotMatch' => $res['status_not_match'] ?? []
//      ]);
    }
  }

  private function statusMismatch($curr) {
    return isset($curr->doku_order_id) &&
      ($curr->odeo_status == OrderStatus::COMPLETED && $curr->doku_status != 'Success') ||
      ($curr->odeo_status != OrderStatus::COMPLETED && $curr->doku_status == 'Success');
  }

  private function amountMismatch($curr) {
    return isset($curr->doku_total) && ($curr->odeo_total != $curr->doku_total);
  }

}