<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 10/04/18
 * Time: 17.17
 */

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Jobs;

use Carbon\Carbon;
use Odeo\Domains\Constant\PaymentGatewayAccount;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\MandiriEcashMutationScraper;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Repository\PaymentGatewayMandiriEcashMutationRepository;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Repository\PaymentGatewayMandiriEcashSettlementRepository;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Repository\PaymentGatewayAccountRepository;
use Odeo\Jobs\Job;

class ScrapeMandiriEcash extends Job {

  public function __construct() {
    parent::__construct();
  }

  public function handle() {
    $paymentGatewayAccountRepo = app()->make(PaymentGatewayAccountRepository::class);
    $mandiriEcashMutationScraper = app()->make(MandiriEcashMutationScraper::class);
    $mandiriEcashMutationRepo = app()->make(PaymentGatewayMandiriEcashMutationRepository::class);
    $mandiriEcashSettlementRepo = app()->make(PaymentGatewayMandiriEcashSettlementRepository::class);

    $account = $paymentGatewayAccountRepo->findById(PaymentGatewayAccount::MANDIRI_ECASH);

    if ($account->last_scraped_time) {
      $lastScraped = new Carbon($account->last_scraped_time);
    } else {
      $lastScraped = Carbon::createFromDate(2017, 1, 1);
    }

    $fetchedMutations = $mandiriEcashMutationRepo->getAllFromDate($lastScraped->startOfDay());
    $fetchedMutationCodes = $fetchedMutations->keyBy('transaction_ref_no');

    $fetchedSettlements = $mandiriEcashSettlementRepo->getAllFromDate($lastScraped->startOfDay());
    $fetchedSettlementCodes = $fetchedSettlements->keyBy('transaction_ref_no');

    $response = $mandiriEcashMutationScraper
      ->scrape($lastScraped, $fetchedMutationCodes, $fetchedSettlementCodes);

    if ($response) {

      list($purchases, $settlements) = array_values($response);

      if (!$purchases->isEmpty()) {
        $mandiriEcashMutationRepo->saveBulk($purchases->all());
      }

      if (!$settlements->isEmpty()) {
        $mandiriEcashSettlementRepo->saveBulk($settlements->all());
      }

      $account->last_scraped_time = Carbon::now();
      $paymentGatewayAccountRepo->save($account);

    }
  }

}
