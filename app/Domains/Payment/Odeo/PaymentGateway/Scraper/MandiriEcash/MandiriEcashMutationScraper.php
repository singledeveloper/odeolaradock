<?php

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash;

use Carbon\Carbon;
use Goutte\Client as GoutteClient;
use GuzzleHttp\Client;
use Mockery\Exception;
use Odeo\Exceptions\FailException;
use phpseclib\Crypt\RSA;
use phpseclib\Math\BigInteger;

class MandiriEcashMutationScraper {

  const MEMBER_ID = '5480507';
  const TYPE_ID = 23;
  const PAYMENT_FILTER_PURCHASE = 9;
  const PAYMENT_FILTER_SETTLEMENT = 10;
  const BASE_URL = 'https://www.mandiriecash.com/merchant/do';

  private $client;

  public function __construct() {
    $this->client = new GoutteClient();
    $this->client->setClient(new Client([
      'cookies' => true,
      'curl' => [CURLOPT_SSL_VERIFYPEER => false],
    ]));
  }

  private function login() {
    $rsa = new RSA();

    $rsa->loadKey([
      'n' => new BigInteger('00a4b4ecf2d28d8a053244214caef4638cbc2ac143f2de0c6ec90498d7edc3c77ccf8529b601baabfa766ffd5eda01930e2695f8fc462d16a333e468d27becfae5', 16),
      'e' => new BigInteger('10001', 16)
    ]);

    $rsa->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
    $encryptedPassword = base64_encode($rsa->encrypt(env('MANDIRI_ECASH_PASSWORD')));

    $this->client->request(
      'POST',
      self::BASE_URL . '/login',
      [
        'principal' => env('MANDIRI_ECASH_USERID'),
        'password' => $encryptedPassword,
        'principalType' => 'USER',
        'operatorLogin' => false
      ]
    );

    $isLoginFailed = $this->client->getInternalResponse()->getStatus() !== 200
      || strpos($this->client->getHistory()->current()->getUri(), 'do/error');

    if ($isLoginFailed) {
      throw new FailException('Mandiri Ecash Login Error');
    }
  }

  private function logout() {
    $this->client->request(
      'GET',
      self::BASE_URL . '/logout?fromMenu=true'
    );
  }

  private function parseAmount($amountText) {
    // Rp xxxxx -> xxxxx
    $amount = substr($amountText, 3);
    $amount = str_replace('.', '', $amount);
    $amount = str_replace(',', '.', $amount);

    return $amount;
  }

  private function getDetails($transferId, $description, $paymentFilterId) {

    $crawler = $this->client->request(
      'GET',
      self::BASE_URL . '/member/viewTransaction?memberId=' . self::MEMBER_ID . '&typeId=' . self::TYPE_ID . '&transferId=' . $transferId
    );

    $columns = $crawler->filter('table.defaultTable')->first()->filter('td');

    $transactionDateTime = Carbon::createFromFormat('d/m/Y H:i:s', $columns->eq(1)->text());
    $transactionRefNo = $columns->eq(3)->text();

    $amount = $this->parseAmount($columns->eq(11)->text());

    $orderId = null;

    $response = [
      'transaction_date_time' => $transactionDateTime->format('Y-m-d H:i:s'),
      'transaction_date' => $transactionDateTime->format('Y-m-d'),
      'transaction_ref_no' => $transactionRefNo,
      'amount' => $amount
    ];

    if ($paymentFilterId == self::PAYMENT_FILTER_PURCHASE) {
      $secondColumns = $crawler->filter('table.defaultTable')->eq(1)->filter('td');

      $mdr = $this->parseAmount($secondColumns->eq(3)->text());

      $response['merchant_discount_rate'] = $mdr;
      $response['amount'] = $amount - $mdr;
      $response['order_id'] = explode('|', $description)[1];
    }

    return $response;
  }

  private function getPage($fromDate, $fetchedCodes, $paymentFilterId) {
    $now = Carbon::now();
    $response = collect([]);

    $page = 0;

    do {

      $crawler = $this->client->request(
        'POST',
        self::BASE_URL . '/member/accountHistory',
        [
          'advanced' => true,
          'memberId' => self::MEMBER_ID,
          'query(owner)' => self::MEMBER_ID,
          'query(paymentFilter)' => $paymentFilterId,
          'query(pageParameters).currentPage' => $page,
          'query(period).begin' => $fromDate,
          'query(type)' => self::TYPE_ID,
          'typeId' => self::TYPE_ID
        ]
      );

      $rows = $crawler->filter('tr.ClassColor1, tr.ClassColor2');

      $rows->each(function ($row) use ($fetchedCodes, &$response, $now, $paymentFilterId) {

        $columns = $row->filter('td');

        $name = trim($columns->eq(1)->text());
        $description = $columns->eq(2)->text();
        $transferId = $columns->eq(4)->filter('img')->attr('transferid');

        $mutation = $this->getDetails($transferId, $description, $paymentFilterId);

        if (!isset($fetchedCodes[$mutation['transaction_ref_no']])) {

          $mutation['name'] = $name;
          $mutation['description'] = $description;
          $mutation['transfer_id'] = $transferId;
          $mutation['created_at'] = $now;
          $mutation['updated_at'] = $now;

          $response->prepend($mutation);
        }
      });

      $page++;
    } while (count($rows) == 30);

    return $response;
  }

  public function scrape($fromDate, $fetchedMutationCodes, $fetchedSettlementCodes) {

    $response = [];
    $fromDate = $fromDate->format('d/m/Y');

    try {

      $this->login();
      $response['purchaces'] = $this->getPage($fromDate, $fetchedMutationCodes, self::PAYMENT_FILTER_PURCHASE);
      $response['$settlements'] = $this->getPage($fromDate, $fetchedSettlementCodes, self::PAYMENT_FILTER_SETTLEMENT);

    } catch (Exception $e) {
      $response = null;
      throw $e;
    } finally {
      $this->logout();
      return $response;
    }
  }

}
