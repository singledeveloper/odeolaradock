<?php

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Model;

use Odeo\Domains\Core\Entity;

class PaymentGatewayMandiriEcashMutation extends Entity
{
  public $timestamps = true;
}
