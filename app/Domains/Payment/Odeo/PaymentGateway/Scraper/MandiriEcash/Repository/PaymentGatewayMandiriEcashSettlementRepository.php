<?php

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Model\PaymentGatewayMandiriEcashSettlement;

class PaymentGatewayMandiriEcashSettlementRepository extends Repository {

  public function __construct(PaymentGatewayMandiriEcashSettlement $bankMandiriEcashInquiry) {
    $this->model = $bankMandiriEcashInquiry;
  }

  function getAllFromDate($date) {
    return $this->model->where('transaction_date', '>=', $date)->get();
  }

}
