<?php

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Scheduler;

use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Repository\PaymentGatewayMandiriEcashMutationRepository;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Jobs\SendEmailWarningMandiriEcashMismatchSettlement;

class MandiriEcashReconMutationSettlementScheduler {

  private $mandiriEcashMutationRepo;

  public function __construct() {
    $this->mandiriEcashMutationRepo = app()->make(PaymentGatewayMandiriEcashMutationRepository::class);
  }

  public function run() {
    $mutations = $this->mandiriEcashMutationRepo->getMappingWithSettlement()
      ->groupBy('settlement_id');

    $settledMutations = [];
    $mismatchSettlements = [];
    foreach ($mutations as $settlementId => $mutation) {
      $mutation->total_amount = $mutation->sum('amount');

      if ($mutation[0]->settlement_amount == $mutation->total_amount) {
        foreach ($mutation as $mutationDetail) {
          $settledMutations[] = [
            'id' => $mutationDetail->id,
            'settlement_id' => $mutationDetail->settlement_id
          ];
        }
      } else {
        $mismatchSettlements[$settlementId] = $mutation;
      }
    }

    if (!empty($settledMutations)) {
      $this->mandiriEcashMutationRepo->updateBulk($settledMutations);
    }

    if (!empty($mismatchSettlements)) {
      dispatch(new SendEmailWarningMandiriEcashMismatchSettlement([
        'settlements' => $mismatchSettlements,
      ]));
    }

  }
}
