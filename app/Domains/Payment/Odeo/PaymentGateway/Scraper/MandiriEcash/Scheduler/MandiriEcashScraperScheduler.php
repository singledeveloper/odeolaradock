<?php

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Scheduler;

use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Jobs\ScrapeMandiriEcash;

class MandiriEcashScraperScheduler {

  public function run() {
    dispatch(new ScrapeMandiriEcash());
  }
}
