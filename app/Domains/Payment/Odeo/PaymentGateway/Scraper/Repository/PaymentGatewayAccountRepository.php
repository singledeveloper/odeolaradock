<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 08-Apr-18
 * Time: 3:06 PM
 */

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Model\PaymentGatewayAccount;

class PaymentGatewayAccountRepository extends Repository {

  public function __construct(PaymentGatewayAccount $paymentGatewayAccount) {
    $this->model = $paymentGatewayAccount;
  }

}
