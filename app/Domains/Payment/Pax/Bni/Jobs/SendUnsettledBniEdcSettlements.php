<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 07/08/19
 * Time: 14.59
 */

namespace Odeo\Domains\Payment\Pax\Bni\Jobs;


use Illuminate\Support\Facades\Mail;

class SendUnsettledBniEdcSettlements {

  private $settlementIds, $date;
  function __construct($settlementIds, $date) {
    $this->settlementIds = $settlementIds;
    $this->date = $date;
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

    Mail::send('emails.unsettled_bni_edc_settlements', [
      'ids' => $this->settlementIds,
      'date' => $this->date
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id')
        ->to('pg@odeo.co.id')
        ->subject('Unsettled BNI EDC Settlements - ' . $this->date);
    });
  }

}