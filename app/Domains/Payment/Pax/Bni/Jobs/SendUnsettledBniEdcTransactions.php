<?php

namespace Odeo\Domains\Payment\Pax\Bni\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendUnsettledBniEdcTransactions extends Job {

  private $counts, $date;

  public function __construct($counts, $date) {
    $this->counts = $counts;
    $this->date = $date;
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

    Mail::send('emails.unsettled_bni_edc_transactions', [
      'counts' => $this->counts,
      'date' => $this->date,
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id')
        ->to('pg@odeo.co.id')
        ->subject('Unsettled BNI EDC Transaction - ' . $this->date);
    });
  }

}