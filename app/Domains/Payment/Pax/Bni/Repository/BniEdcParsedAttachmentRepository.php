<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 02/08/19
 * Time: 16.00
 */

namespace Odeo\Domains\Payment\Pax\Bni\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Pax\Bni\Model\BniEdcParsedAttachment;
use Odeo\Domains\Payment\Pax\Helper\ParseHelper;

class BniEdcParsedAttachmentRepository extends Repository {

  function __construct(BniEdcParsedAttachment $parsedAttachment) {
    $this->model = $parsedAttachment;
  }

  function getBniEdcSettlementParseList() {
    return $this->model
      ->where('parsed', false)
      ->where('subject', ParseHelper::PARSE_SUBJECT_BNI_EDC)
      ->get();
  }
}