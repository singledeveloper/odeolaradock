<?php


namespace Odeo\Domains\Payment\Pax\Bni\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Pax\Bni\Model\BniEdcTransaction;

class BniEdcTransactionRepository extends Repository {

  public function __construct(BniEdcTransaction $model) {
    $this->model = $model;
  }

  public function updateSettlementIdByTrxDate($date, $settlementId) {
    return $this->getCloneModel()
      ->where('trx_date', $date)
      ->whereNull('bni_edc_settlement_id')
      ->update([
        'bni_edc_settlement_id' => $settlementId,
      ]);
  }

  public function findLastSettledDate() {
    return $this->getCloneModel()
      ->orderByDesc('trx_date')
      ->select('trx_date')
      ->first();
  }

  public function listNoPaxPayments() {
    return $this->getCloneModel()
      ->where('amount', '>=', '1000')
      ->whereNotExists(function ($query) {
        $query
          ->selectRaw(1)
          ->from('pax_payments')
          ->whereRaw('pax_payments.trx_date=bni_edc_transactions.trx_date')
          ->whereRaw('pax_payments.total_amount=bni_edc_transactions.amount')
          ->whereRaw('pax_payments.approval_code=bni_edc_transactions.approval_code');
      })
      ->get();
  }

  public function listUnsettledByProcDateBefore($date) {
    return $this->getCloneModel()
      ->where('bni_edc_settlement_id', null)
      ->where('proc_date', '<', $date)
      ->get();
  }

}
