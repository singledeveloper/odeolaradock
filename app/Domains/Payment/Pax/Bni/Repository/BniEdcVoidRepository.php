<?php


namespace Odeo\Domains\Payment\Pax\Bni\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Pax\Bni\Model\BniEdcVoid;

class BniEdcVoidRepository extends Repository {

  public function __construct(BniEdcVoid $model) {
    $this->model = $model;
  }

  public function listNewVoids($date) {
    return $this->model->newQuery()
      ->where('created_at', '>=', $date)
      ->get();
  }

}
