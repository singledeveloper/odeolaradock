<?php


namespace Odeo\Domains\Payment\Pax\Bni\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Payment\Pax\Bni\Jobs\SendNewBniEdcVoidAlert;
use Odeo\Domains\Payment\Pax\Bni\Repository\BniEdcVoidRepository;

class CheckNewBniEdcVoid {

  private $bniEdcVoidRepo;

  public function __construct() {
    $this->bniEdcVoidRepo = app()->make(BniEdcVoidRepository::class);
  }

  public function run() {
    $date = Carbon::yesterday();
    $voids = $this->bniEdcVoidRepo->listNewVoids($date);

    if ($voids->isEmpty()) {
      return;
    }

    dispatch(new SendNewBniEdcVoidAlert($voids->toArray(), date('Y-m-d')));
  }

}