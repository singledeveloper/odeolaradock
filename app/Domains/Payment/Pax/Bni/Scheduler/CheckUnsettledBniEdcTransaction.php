<?php


namespace Odeo\Domains\Payment\Pax\Bni\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Payment\Pax\Bni\Jobs\SendUnsettledBniEdcTransactions;
use Odeo\Domains\Payment\Pax\Bni\Repository\BniEdcTransactionRepository;

class CheckUnsettledBniEdcTransaction {

  private $bniEdcTransactionRepo;

  public function __construct() {
    $this->bniEdcTransactionRepo = app()->make(BniEdcTransactionRepository::class);
  }

  public function run() {
    $date = Carbon::today()->subDays(2);
    $transactions = $this->bniEdcTransactionRepo->listUnsettledByProcDateBefore($date);

    if ($transactions->isEmpty()) {
      return;
    }

    $counts = $transactions
      ->groupBy('proc_date')
      ->map(function ($transactions) {
        return $transactions->count();
      })
      ->all();

    dispatch(new SendUnsettledBniEdcTransactions($counts, date('Y-m-d')));
  }

}