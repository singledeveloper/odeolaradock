<?php


namespace Odeo\Domains\Payment\Pax\Bni\Scheduler;


use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bni\Repository\BankBniInquiryRepository;
use Odeo\Domains\Payment\Pax\Bni\Jobs\SendReconBniEdcSettlementResult;
use Odeo\Domains\Payment\Pax\Bni\Repository\BniEdcSettlementRepository;
use Odeo\Domains\Payment\Pax\Bni\Repository\BniEdcTransactionRepository;
use Odeo\Domains\Payment\Pax\Helper\ParseHelper;

class ReconBankBniEdcSettlement {

  private $bankBniInquiryRepo;
  private $bniEdcSettlementRepo;
  private $bniEdcTransactionRepo;

  public function __construct() {
    $this->bankBniInquiryRepo = app()->make(BankBniInquiryRepository::class);
    $this->bniEdcSettlementRepo = app()->make(BniEdcSettlementRepository::class);
    $this->bniEdcTransactionRepo = app()->make(BniEdcTransactionRepository::class);
  }

  public function run() {
    \DB::transaction(function () {
      $this->handle();
    });
  }

  private function handle() {
    $inquiries = $this->bankBniInquiryRepo->getUnreconciledEdcSettlement();
    if ($inquiries->isEmpty()) {
      clog('bni-edc', "there's no unreconciled settlements");
      return;
    }

    $dupeInquiries = $inquiries->groupBy('credit');
    $settlementsByAmount = $this->bniEdcSettlementRepo
      ->listByTotalAmounts($inquiries->pluck('credit'))
      ->groupBy('total_amount');

    $results = [];

    foreach ($inquiries as $inq) {
      if ($dupeInquiries->get($inq->credit)->count() > 1) {
        $results[] = $this->mapToResult($inq, 'Transferred amount is not unique');
        continue;
      }

      if (!$settlementsByAmount->has($inq->credit)) {
        $results[] = $this->mapToResult($inq, 'Settlement with this amount not found');
        continue;
      }

      $settlements = $settlementsByAmount->get($inq->credit);
      if ($settlements->count() > 1) {
        $results[] = $this->mapToResult($inq, 'More than 1 settlement with this amount found');
        continue;
      }

      $settlement = $settlements->first();
      $inq->reference_type = ParseHelper::REFERENCE_BNI_EDC_SETTLEMENT;
      $inq->reference = json_encode(['' . $settlement->id]);
      $inq->save();
      $this->bniEdcTransactionRepo->updateSettlementIdByTrxDate($settlement->trx_date, $settlement->id);

      $results[] = $this->mapToResult($inq, 'OK');
    }

    if (empty($results)) {
      return;
    }

    dispatch(new SendReconBniEdcSettlementResult($results, date('Y-m-d')));
  }

  private function mapToResult($inq, $message) {
    return [
      'id' => $inq->id,
      'date' => $inq->date,
      'description' => $inq->description,
      'amount' => $inq->credit,
      'message' => $message,
    ];
  }

}