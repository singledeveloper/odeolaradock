<?php


namespace Odeo\Domains\Payment\Pax\Helper;


class Signer {

  public function sign($orderId, $terminalId, $paymentData) {
    $key = env('ODEO_TERMINAL_SIGNING_KEY');
    $message = $orderId . '|' . $terminalId . '|' . $paymentData;
    return hash_hmac('sha256', $message, $key);
  }

}