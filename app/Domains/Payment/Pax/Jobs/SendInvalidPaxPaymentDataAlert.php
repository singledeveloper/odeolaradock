<?php


namespace Odeo\Domains\Payment\Pax\Jobs;


use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendInvalidPaxPaymentDataAlert extends Job {

  private $terminalId, $orderId, $data;

  public function __construct($terminalId, $orderId, $data) {
    parent::__construct();
    $this->terminalId = $terminalId;
    $this->orderId = $orderId;
    $this->data = $data;
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

    Mail::send('emails.invalid_pax_payment_data', [
      'terminalId' => $this->terminalId,
      'orderId' => $this->orderId,
      'paxPaymentData' => $this->data,
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id')
        ->to('pg@odeo.co.id')
        ->subject('Invalid Pax Payment Data - ' . date('Y-m-d'));
    });
  }

}