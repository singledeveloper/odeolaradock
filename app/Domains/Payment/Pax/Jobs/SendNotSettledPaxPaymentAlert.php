<?php


namespace Odeo\Domains\Payment\Pax\Jobs;


use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Payment\Pax\Repository\PaxPaymentRepository;
use Odeo\Jobs\Job;

class SendNotSettledPaxPaymentAlert extends Job {

  private $beforeDate;

  public function __construct($beforeDate) {
    parent::__construct();
    $this->beforeDate = $beforeDate;
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

    $paxPaymentRepo = app()->make(PaxPaymentRepository::class);
    $payments = $paxPaymentRepo->findNotSettled($this->beforeDate);

    if (empty($payments)) {
      return;
    }

    Mail::send('emails.no_settlement_pax_payment_alert', [
      'payments' => $payments,
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id')
        ->to('pg@odeo.co.id')
        ->subject('No Settlement Pax Payment Alert - ' . date('Y-m-d'));
    });
  }

}