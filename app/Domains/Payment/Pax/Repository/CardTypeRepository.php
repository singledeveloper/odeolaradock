<?php


namespace Odeo\Domains\Payment\Pax\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Pax\Model\CardType;

class CardTypeRepository extends Repository {

  public function __construct(CardType $model) {
    $this->setModel($model);
  }

  public function findByCardNumber($cardNumber) {
    return $this->getCloneModel()
      ->where('pan_low', '<=', $cardNumber)
      ->where('pan_high', '>=', $cardNumber)
      ->orderByDesc('pan_low')
      ->first();
  }

}