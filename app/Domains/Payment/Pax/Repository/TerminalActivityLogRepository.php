<?php


namespace Odeo\Domains\Payment\Pax\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Pax\Model\TerminalActivityLog;

class TerminalActivityLogRepository extends Repository {

  public function __construct(TerminalActivityLog $model) {
    $this->setModel($model);
  }

}