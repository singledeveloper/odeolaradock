<?php


namespace Odeo\Domains\Payment\Pax\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Pax\Model\TerminalTransaction;

class TerminalTransactionRepository extends Repository {

  public function __construct(TerminalTransaction $model) {
    $this->setModel($model);
  }

  public function findByLatestRequestDate() {
    return $this->model
      ->orderByDesc('request_date')
      ->first();
  }
}