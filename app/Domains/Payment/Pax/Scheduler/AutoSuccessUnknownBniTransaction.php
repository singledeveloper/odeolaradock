<?php

namespace Odeo\Domains\Payment\Pax\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Constant\BillerASG;
use Odeo\Domains\Constant\EdcTransactionVendor;
use Odeo\Domains\Constant\PaxPayment;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Order\OrderUpdater;
use Odeo\Domains\Order\Repository\OrderRepository;
use Odeo\Domains\Payment\Odeo\Terminal\Repository\UserTerminalRepository;
use Odeo\Domains\Payment\Pax\Bni\Repository\BniEdcTransactionRepository;
use Odeo\Domains\Payment\Pax\Helper\Signer;
use Odeo\Domains\Payment\Pax\Jobs\SendAutoSuccessUnknownBniTransactionResult;
use Odeo\Domains\Payment\Pax\Repository\PaxPaymentRepository;
use Odeo\Domains\Payment\PaymentOpenRequester;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository;

class AutoSuccessUnknownBniTransaction {

  private $bniEdcTransactionRepo, $paxPaymentRepo, $orderRepo, $paymentOdeoPaymentChannelRepo, $signer,
    $userTerminalRepo;

  public function __construct() {
    $this->bniEdcTransactionRepo = app()->make(BniEdcTransactionRepository::class);
    $this->paxPaymentRepo = app()->make(PaxPaymentRepository::class);
    $this->orderRepo = app()->make(OrderRepository::class);
    $this->paymentOdeoPaymentChannelRepo = app()->make(PaymentOdeoPaymentChannelRepository::class);
    $this->signer = app()->make(Signer::class);
    $this->userTerminalRepo = app()->make(UserTerminalRepository::class);
  }

  public function run() {
    $opc = $this->getOpc();
    if (!$opc) {
      return;
    }

    $payments = $this->bniEdcTransactionRepo->listNoPaxPayments();
    if ($payments->isEmpty()) {
      return;
    }

    $terminals = $this->userTerminalRepo
      ->listEnabledEdcTransaction()
      ->keyBy(function ($t) {
        return $this->getMidTidKey($t->mid, $t->tid);
      });

    $result = [];
    $parseFuncs = [
      [$this, 'handleByAsgOrder'],
      [$this, 'handleByDirectTransaction'],
    ];

    foreach ($payments as $payment) {
      $parsed = false;
      foreach ($parseFuncs as $func) {
        $res = $func($terminals, $opc, $payment);
        if (!$res) {
          continue;
        }

        $result[] = $res;

        if ($res['status'] == PaxPayment::BNI_PARSE_STATUS_SUCCESS) {
          $parsed = true;
          break;
        }
      }

      if ($parsed) {
        continue;
      }

      $result[] = [
        'id' => $payment->id,
        'status' => PaxPayment::BNI_PARSE_STATUS_FAILED,
        'message' => 'unknown bni transaction',
      ];
    }

    dispatch(new SendAutoSuccessUnknownBniTransactionResult($result, date('Y-m-d')));
  }

  private function handleByDirectTransaction($terminals, $opc, $payment) {
    $terminal = $terminals->get($this->getMidTidKey($payment->mid, $payment->tid));
    if (!$terminal) {
      return null;
    }

    $paymentData = $this->mapToPaymentData($payment);

    $pipeline = new Pipeline();
    $pipeline->add(new Task(CartRemover::class, 'clear'));
    $pipeline->add(new Task(CartInserter::class, 'addToCart'));
    $pipeline->add(new Task(CartCheckouter::class, 'checkout'));
    $pipeline->add(new Task(PaymentRequester::class, 'request'));
    $pipeline->add(new Task(PaymentOpenRequester::class, 'open'));
    $pipeline->enableTransaction();
    $pipeline->execute([
      'auth' => [
        'user_id' => $terminal->user_id,
        'platform_id' => Platform::TERMINAL,
        'type' => UserType::SELLER,
      ],
      'terminal_id' => $terminal->terminal_id,
      'info_id' => Payment::OPC_GROUP_DEBIT_CREDIT_CARD_PRESENT_PAX_BNI_SWITCHING,
      'gateway_id' => Payment::DEBIT_CREDIT_CARD_PRESENT_BNI_SWITCHING,
      'service_detail_id' => ServiceDetail::EDC_TRANSACTION,
      'item_id' => $payment->id,
      'opc' => $opc->code,
      'payment_data' => $paymentData,
      'skip_signature' => true,
      'amount' => $payment->amount,
      'card_number' => $payment->card_no,
      'edc_transaction_vendor' => EdcTransactionVendor::BNI_EDC_TRANSACTIONS,
      'edc_transaction_id' => $payment->id,
      'trx_date' => $payment->trx_date,
    ]);

    if ($pipeline->fail()) {
      return [
        'id' => $payment->id,
        'status' => PaxPayment::BNI_PARSE_STATUS_FAILED,
        'message' => 'pipeline failed:' . $pipeline->errorMessage,
      ];
    }

    return [
      'id' => $payment->id,
      'status' => PaxPayment::BNI_PARSE_STATUS_SUCCESS,
      'message' => 'ok',
    ];
  }

  private function handleByAsgOrder($terminals, $opc, $payment) {
    $orders = $this->orderRepo->listCancelledByTotalAndDate($payment->amount, $payment->trx_date);
    $orders = $this->getUniqueOrders($orders);
    if (count($orders) == 0) {
      return null;
    }

    if (count($orders) > 1) {
      return [
        'id' => $payment->id,
        'status' => PaxPayment::BNI_PARSE_STATUS_FAILED,
        'message' => 'more than one matching orders: ' . implode(', ', $orders->pluck('id')->all()),
      ];
    }

    $order = $orders[0];
    if ($order->user_id != BillerASG::getOdeoUserId()) {
      return [
        'id' => $payment->id,
        'status' => PaxPayment::BNI_PARSE_STATUS_FAILED,
        'message' => 'user id can only be asg for now, order id ' . $order->id,
      ];
    }

    $terminalId = $this->findTerminalId($payment->tid);
    if (!$terminalId) {
      return [
        'id' => $payment->id,
        'status' => PaxPayment::BNI_PARSE_STATUS_FAILED,
        'message' => 'can not determine terminal id',
      ];
    }

    $paymentData = $this->mapToPaymentData($payment);

    $pipeline = new Pipeline();
    $pipeline->add(new Task(OrderUpdater::class, 'reopen'));
    $pipeline->add(new Task(PaymentRequester::class, 'request'));
    $pipeline->add(new Task(PaymentOpenRequester::class, 'open'));
    $pipeline->enableAntiSpam($order->id);
    $pipeline->enableTransaction();
    $pipeline->execute([
      'order_id' => $order->id,
      'phone_number' => $order->phone_number,
      'terminal_id' => $terminalId,
      'opc' => $opc->code,
      'payment_data' => $paymentData,
      'signature' => $this->signer->sign($order->id, $terminalId, $paymentData),
    ]);

    if ($pipeline->fail()) {
      return [
        'id' => $payment->id,
        'status' => PaxPayment::BNI_PARSE_STATUS_FAILED,
        'message' => 'pipeline failed: ' . $pipeline->errorMessage,
      ];
    }

    return [
      'id' => $payment->id,
      'status' => PaxPayment::BNI_PARSE_STATUS_SUCCESS,
      'message' => 'ok, order id: ' . $order->id,
    ];
  }

  private function getUniqueOrders($orders) {
    if (count($orders) <= 1) {
      return $orders;
    }

    $orders->load('details.userInvoice');
    $isUserInvoiceAndEDC = $orders->every(function ($order) {
      return $order->details->count() == 1
        && $order->details[0]->service_detail_id == ServiceDetail::USER_INVOICE_ODEO
        && $order->platform_id == Platform::TERMINAL
        && $order->gateway_id == Payment::DEBIT_CREDIT_CARD_PRESENT_BNI_SWITCHING;
    });

    if (!$isUserInvoiceAndEDC) {
      return $orders;
    }

    return $orders->unique(function ($order) {
      $detail = $order->details[0];
      $inv = $detail->userInvoice;

      return implode('|', [$order->name, $order->phone_number, $inv->invoice_number, $inv->biller_id]);
    });
  }

  private function getOpc() {
    return $this->paymentOdeoPaymentChannelRepo->findByAttributes([
      'gateway_id' => Payment::DEBIT_CREDIT_CARD_PRESENT_BNI_SWITCHING,
      'info_id' => Payment::OPC_GROUP_DEBIT_CREDIT_CARD_PRESENT_PAX_BNI_SWITCHING,
      'active' => true,
    ]);
  }

  private function findTerminalId($tid) {
    $payment = $this->paxPaymentRepo->findByTID($tid);
    if (!$payment) {
      return null;
    }
    $ut = $this->userTerminalRepo->findActiveByTerminalId($payment->terminal_id);
    if (!$ut) {
      return null;
    }
    return $ut->terminal_id;
  }

  private function mapToPaymentData($payment) {
    return implode('|', [
      $payment->tid,
      $payment->mid,
      'NULL',
      $payment->card_no,
      'NULL',
      'NULL',
      'NULL',
      'NULL',
      'NULL',
      Carbon::parse($payment->trx_date)->format('Y/m/d'),
      'NULL',
      'NULL',
      $payment->approval_code,
      +$payment->amount,
      '',
    ]);
  }

  private function getMidTidKey($mid, $tid) {
    if (starts_with($mid, '000100')) {
      $mid = substr($mid, strlen('000100'));
    }
    return "$mid|$tid";
  }

}