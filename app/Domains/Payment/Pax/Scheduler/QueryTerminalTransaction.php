<?php

namespace Odeo\Domains\Payment\Pax\Scheduler;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Odeo\Domains\Payment\Pax\Repository\TerminalTransactionRepository;

class QueryTerminalTransaction {

  const BASE_URL = '182.23.25.76:8088';
  const API_URL = self::BASE_URL . '/cyber-iatoms-web/api/transaction/record';

  public function __construct() {
    $this->terminalTransactionRepo = app()->make(TerminalTransactionRepository::class);
  }

  public function run() {
    $client = new Client();
    $today = date('Y/m/d');
    $yesterday = date('Y/m/d', strtotime("-1 day"));

    $response = $client->request('GET', self::API_URL . '?sortName=requestDate&sortOrder=asc&startDate=' . $yesterday . '&endDate=' . $today, [
      'headers' => [
        'token' => env('PAX_API_TOKEN'),
      ],
    ]);

    $responseJson = json_decode($response->getBody());

    if ($responseJson->code != '1') {
      // Query Failed, write to log
      clog('pax-query-transaction', $response->getBody());
      return $responseJson->message;
    }

    $results = $responseJson->results;
    if ($results != null) {
      $lastRequestDateRow = $this->terminalTransactionRepo->findByLatestRequestDate();
      if ($lastRequestDateRow != null) {
        $lastRequestDate = json_decode($lastRequestDateRow)->request_date;
        $lastRequestDate = Carbon::parse($lastRequestDate);
      } else {
        $lastRequestDate = null;
      }

      $now = Carbon::now();

      foreach ($results as $result) {
        $requestDate = Carbon::parse($result->requestDate);
        if ($lastRequestDate != null) {
          if ( ($requestDate->lte($lastRequestDate)) || ($requestDate->gte($now)) ) {
            continue;
          }
        }

        $terminalTransaction = $this->terminalTransactionRepo->getNew();
  
        $terminalTransaction->mti = $result->mti;
        $terminalTransaction->trx_type = $result->trxType;
        $terminalTransaction->f3_processing_code = $result->f3ProcessingCode;
        $terminalTransaction->f4_transaction_amount = str_replace('.', '', $result->f4TransactionAmount);
        $terminalTransaction->f11_stan = $result->f11Stan;
        $terminalTransaction->f24_nii = $result->f24Nii;
        $terminalTransaction->f35_card_number = $result->f35CardNumber;
        $terminalTransaction->f38_auth_id_response = $result->f38AuthIdResponse;
        $terminalTransaction->f39_response_code = $result->f39ResponseCode;
        $terminalTransaction->f41_tid = $result->f41Tid;
        $terminalTransaction->f42_mid = $result->f42Mid;
        $terminalTransaction->f60_batch = $result->f60Batch;
        $terminalTransaction->f62 = $result->f62;
        $terminalTransaction->f63a_sale_count = $result->f63aSaleCount;
        $terminalTransaction->f63b_sale_amount = str_replace('.', '', $result->f63bSaleAmount);
        $terminalTransaction->f63c_refund_count = $result->f63cRefundCount;
        $terminalTransaction->f63d_refund_amount = str_replace('.', '', $result->f63dRefundAmount);
        $terminalTransaction->request_date = $result->requestDate;
        
        $terminalTransaction->save();
      }
    }
  }

}