<?php


namespace Odeo\Domains\Payment\Pax;


use Carbon\Carbon;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\PaxPayment;
use Odeo\Domains\Constant\PaxPaymentStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Helper\PaymentManager;
use Odeo\Domains\Payment\Pax\Helper\CardTypeHelper;
use Odeo\Domains\Payment\Pax\Jobs\SendInvalidPaxPaymentDataAlert;
use Odeo\Domains\Payment\Pax\Repository\CardTypeRepository;
use Odeo\Domains\Payment\Pax\Repository\PaxPaymentRepository;

class Submitter extends PaymentManager {

  private $paxPaymentRepo, $cardTypeRepo, $cardTypeHelper;

  public function __construct() {
    parent::__construct();

    $this->paxPaymentRepo = app()->make(PaxPaymentRepository::class);
    $this->cardTypeRepo = app()->make(CardTypeRepository::class);
    $this->cardTypeHelper = app()->make(CardTypeHelper::class);
  }

  public function submit(PipelineListener $listener, $order, $data) {
    if ($order->status != OrderStatus::CREATED) {
      clog('pax', "order #{$order->id} status is not " . OrderStatus::CREATED . " got {$order->status} instead");
      return $listener->response(400);
    }

    $parts = explode('|', $data['payment_data']);
    $parts = array_map(function ($s) {
      return trim($s);
    }, $parts);
    $isValid = count($parts) == 15;

    if (!$isValid) {
      $listener->pushQueue(new SendInvalidPaxPaymentDataAlert($data['terminal_id'], $data['order_id'], $data['payment_data']));
      return $listener->response(400);
    }

    $paxPayment = $this->paxPaymentRepo->getNew();
    $paxPayment->order_id = $data['order_id'];
    $paxPayment->terminal_id = $data['terminal_id'];
    $paxPayment->payment_data = $data['payment_data'];
    $paxPayment->status = PaxPaymentStatus::COMPLETED;
    $paxPayment->tid = $parts[0];
    $paxPayment->merchant_id = $parts[1];
    $paxPayment->card_type = $parts[2];
    $paxPayment->pan_number = $parts[3];
    $paxPayment->expiry_date = $parts[4];
    $paxPayment->entry_mode = $parts[5];
    $paxPayment->transaction_type = $parts[6];
    $paxPayment->batch_number = $parts[7];
    $paxPayment->trace_number = $parts[8];
    $paxPayment->payment_date = $parts[9];
    $paxPayment->payment_time = $parts[10];
    $paxPayment->reference_number = $parts[11];
    $paxPayment->approval_code = $parts[12];
    $paxPayment->total_amount = str_replace(",", "", $parts[13]);

    $paxPayment->trx_date = Carbon::parse($parts[9])->format('Y-m-d');
    $paxPayment->actual_card_type = $this->cardTypeHelper->getCardType($paxPayment->pan_number);
    $paxPayment->mdr_pct = $this->getMDRPercentage($paxPayment->actual_card_type);
    $paxPayment->mdr = round($paxPayment->total_amount * $paxPayment->mdr_pct / 100);
    $paxPayment->payment_gateway_cost = $this->getPaymentGatewayCost($paxPayment);

    if (isset($data['edc_transaction_vendor']) && isset($data['edc_transaction_id'])) {
      $paxPayment->edc_transaction_reference_type = $data['edc_transaction_vendor'];
      $paxPayment->edc_transaction_reference_id = $data['edc_transaction_id'];
    }

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');
    $order->save();
    $paxPayment->save();

    $listener->pushQueue(new VerifyOrder($data['order_id']));
    return $listener->response(200, ['order_id' => $data['order_id']]);
  }

  private function getMDRPercentage($cardType) {
    switch ($cardType) {
      case PaxPayment::TYPE_CREDIT_ON_US:
        return PaxPayment::CREDIT_ON_US_BNI_MDR_PCT;
      case PaxPayment::TYPE_DEBIT_OFF_US:
        return PaxPayment::DEBIT_OFF_US_BNI_MDR_PCT;
      case PaxPayment::TYPE_DEBIT_ON_US:
        return PaxPayment::DEBIT_ON_US_BNI_MDR_PCT;
      default:
        return PaxPayment::CREDIT_OFF_US_BNI_MDR_PCT;
    }
  }

  private function getPaymentGatewayCost($paxPayment) {
    $count = $this->paxPaymentRepo->countByDateAndTerminalId($paxPayment->trx_date, $paxPayment->terminal_id);

    foreach (PaxPayment::PAYMENT_GATEWAY_COST_TIERING as $tier) {
      if ($count > $tier['trx']) return $tier['cost'];
    }

    return PaxPayment::PAYMENT_GATEWAY_COST_DEFAULT;
  }
}