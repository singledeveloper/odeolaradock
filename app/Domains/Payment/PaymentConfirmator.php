<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 10:48 PM
 */

namespace Odeo\Domains\Payment;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Helper\PaymentManager;

class PaymentConfirmator extends PaymentManager {


  private $paymentInformations;

  private $bankTransferRequester;

  public function __construct() {

    parent::__construct();

    $this->paymentInformations = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);

    $this->bankTransferRequester = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\PaymentConfirmator::class);

  }

  public function confirm(PipelineListener $listener, $data) {


    $order = $this->orders->findById($data['order_id']);

    $payment = $this->payments->findByOrderId($order->id);

    $paymentInformation = $this->paymentInformations->findById($payment->opc);

    if ($this->paymentValidator->isBankTransfer($paymentInformation->info_id)) {

      return $this->bankTransferRequester->confirm($listener, $order, $paymentInformation, $data);

    }

    return $listener->response(400);

  }
}
