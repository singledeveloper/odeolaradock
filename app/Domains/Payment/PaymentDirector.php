<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 11/19/16
 * Time: 11:59 PM
 */

namespace Odeo\Domains\Payment;


use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Helper\PaymentManager;

class PaymentDirector extends PaymentManager{

  private $paymentInformations;
  protected $damMandiriECash;

  public function __construct() {

    parent::__construct();

    $this->paymentInformations = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);
    $this->damMandiriECash = app()->make(\Odeo\Domains\Payment\Dam\Mandiriecash\Director::class);

  }

  public function redirect(PipelineListener $listener, $data) {


    $order = $this->orders->findById($data['order_id']);

    $payment = $this->payments->findByOrderId($order->id);
    $paymentInformations = $this->paymentInformations->findById($payment->opc);

    if ($this->paymentValidator->isMandiriECash($paymentInformations->info_id)) {
      return $this->damMandiriECash->redirect($listener, $order, $data);
    }

    return $listener->response(400);

  }


}