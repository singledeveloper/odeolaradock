<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:08 PM
 */

namespace Odeo\Domains\Payment;


use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class PaymentInformationSelector implements SelectorListener {


  private $paymentInformations, $paymentValidator;

  public function __construct() {
    $this->paymentInformations = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelInformationRepository::class);
    $this->paymentValidator = app()->make(\Odeo\Domains\Payment\Helper\PaymentValidator::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $output = [];

    $output['name'] = $item->actual_name;
    $output['min_transaction'] = $item->min_transaction;
    $output['max_transaction'] = $item->max_transaction;

    $infoId = null;

    foreach ($item->channels as $channel) {

      $temp['opc'] = $channel->code;
      $temp['name'] = Payment::getGatewayName($channel->gateway_id);
      $temp['active'] = (bool)$channel->active;

      $output['channels'][] = $temp;

      $infoId = $channel->info_id;

    }

    if ($this->paymentValidator->isBankTransfer($infoId)) {

      $output['detail'] = json_decode($item->detail, true);

    }

    return $output;
  }


  public function getAll(PipelineListener $listener, $data) {

    $this->paymentInformations->normalizeFilters($data);

    $paymentList = [];

    foreach ($this->paymentInformations->gets() as $item) {
      $paymentList[] = $this->_transforms($item, $this->paymentInformations);
    }

    if (count($paymentList) > 0) {
      return $listener->response(200, array_merge(['channels' => $this->_extends($paymentList, $this->paymentInformations)],
          $this->paymentInformations->getPagination()
      ));
    }
    return $listener->response(204);
  }

  public function getFilter(PipelineListener $listener, $data) {
    $paymentInformations = $this->paymentInformations->getFilter();
    return $listener->response(200, array_merge(['payments' => $paymentInformations, 'products' => $data['products'], 'status' => $data['status'], 'trx_types' => $data['trx_types']]));
  }

  public function getDirect(PipelineListener $listener, $data) {
    $data['is_paginate'] = false;
    $this->paymentInformations->normalizeFilters($data);

    $paymentList = [];

    foreach ($this->paymentInformations->getDirectPayments() as $item) {
      if($item->id == Payment::OPC_GROUP_OCASH) continue;
      $paymentList[] = [
        'id' => $item->id,
        'name' => $item->name
      ];
    }

    if (count($paymentList) > 0) {
      return $listener->response(200, ['channels' => $this->_extends($paymentList, $this->paymentInformations)]);
    }
    return $listener->response(204);
  }

}
