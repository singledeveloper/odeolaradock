<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 10:48 PM
 */

namespace Odeo\Domains\Payment;

use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Cimb\Helper\CimbHelper;
use Odeo\Domains\Payment\Helper\PaymentManager;

class PaymentNotificator extends PaymentManager {


  private $paymentInformations;

  private $ccInstallment, $va, $briEpay, $danamon, $muamalat, $cimbClick, $cimbVa;

  private $kredivo, $akulaku;


  public function __construct() {

    parent::__construct();

    $this->paymentInformations = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);

    $this->ccInstallment = app()->make(\Odeo\Domains\Payment\Doku\Installment\Notifier::class);

    $this->va = app()->make(\Odeo\Domains\Payment\Doku\Vagroup\Notifier::class);

    $this->kredivo = app()->make(\Odeo\Domains\Payment\Kredivo\Notifier::class);

    $this->briEpay = app()->make(\Odeo\Domains\Payment\Doku\Briepay\Notifier::class);

    $this->danamon = app()->make(\Odeo\Domains\Payment\Doku\Danamon\Notifier::class);

    $this->muamalat = app()->make(\Odeo\Domains\Payment\Doku\Muamalat\Notifier::class);

    $this->dokuVaDirect = app()->make(\Odeo\Domains\Payment\Doku\VaDirect\Notifier::class);

    $this->prismalinkVaDirect = app()->make(\Odeo\Domains\Payment\Prismalink\VaDirect\Notifier::class);

    $this->akulaku = app()->make(\Odeo\Domains\Payment\Akulaku\Notifier::class);

    $this->cimbClick = app()->make(\Odeo\Domains\Payment\Doku\Cimb\Notifier::class);

    $this->cimbVa = app()->make(\Odeo\Domains\Payment\Cimb\Notifier::class);

  }


  public function notify(PipelineListener $listener, $data) {
    if (!$order = $this->orders->findById($data['order_id'])) return $listener->response(400, ['error_code' => CimbHelper::PAYMENT_CODE_BILL_NOT_FOUND]);

    $payment = $this->payments->findByOrderId($order->id);
    $paymentInformation = $this->paymentInformations->findById($payment->opc);

    if (!$order) {
      return $listener->response(400, 'Order tidak ditemukan');
    }

    if ($order->status >= OrderStatus::VERIFIED && $order->status != OrderStatus::CANCELLED) {

      if($this->paymentValidator->isCimbVa($paymentInformation->info_id)) {
        return $listener->response(400, [
          'error_code' => CimbHelper::PAYMENT_CODE_BILL_PAID
        ]);
      }

      return $listener->response(200);
    }

    if ($this->paymentValidator->isAtmTransferVa($paymentInformation->info_id)) {

      return $this->va->notified($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isAlfa($paymentInformation->info_id)) {

      return $this->va->notified($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isKredivo($paymentInformation->info_id)) {

      return $this->kredivo->notified($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isCcInstallment($paymentInformation->info_id)) {

      return $this->ccInstallment->notified($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isMandiriECash($paymentInformation->info_id)) {
      $damMandiriECash = app()->make(\Odeo\Domains\Payment\Dam\Mandiriecash\Notifier::class);
      return $damMandiriECash->notified($listener, $order, $data);

    } else if ($this->paymentValidator->isMandiriMpt($paymentInformation->info_id)) {
      $damMandiriMpt = app()->make(\Odeo\Domains\Payment\Dam\Mandirimpt\Notifier::class);
      return $damMandiriMpt->notified($listener, $order, $payment, $data);

    } else if ($this->paymentValidator->isBriEpay($paymentInformation->info_id)) {

      return $this->briEpay->notified($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isDanamonInternetBanking($paymentInformation->info_id)) {

      return $this->danamon->notified($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isMuamalatInternetBanking($paymentInformation->info_id)) {

      return $this->muamalat->notified($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isDokuVaDirect($paymentInformation->info_id)) {

      return $this->dokuVaDirect->notified($listener, $order, $paymentInformation, $payment, $data);

    } else if($this->paymentValidator->isAkulaku($paymentInformation->info_id)) {

      return $this->akulaku->notify($listener, $order, $paymentInformation, $payment, $data);

    } else if($this->paymentValidator->isCimbClick($paymentInformation->info_id)) {

      return $this->cimbClick->notify($listener, $order, $paymentInformation, $payment, $data);

    }
//    else if($this->paymentValidator->isCimbVa($paymentInformation->info_id)) {
//
//      return $this->cimbVa->notify($listener, $order, $paymentInformation, $payment, $data);
//
//    }

    return $listener->response(400);

  }

  public function backToMerchant(PipelineListener $listener, $data) {

    $order = $this->orders->findById($data['order_id']);

    $payment = $this->payments->findByOrderId($order->id);

    $urls = $this->getUrlData($order->id, $payment->opc);

    if ($data['status'] == 'success' || $order->status == OrderStatus::VERIFIED) {
      return $listener->response(200, [
        'redirect_to' => $urls['finish_redirect_url']
      ]);
    } else {
      return $listener->response(200, [
        'redirect_to' => $urls['unfinish_redirect_url']
      ]);
    }
  }

}