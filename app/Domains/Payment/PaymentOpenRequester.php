<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 10:48 PM
 */

namespace Odeo\Domains\Payment;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Helper\PaymentManager;

class PaymentOpenRequester extends PaymentManager {


  private $paymentInformations, $oCash;

  private $bankTransferRequester;

  private $cc, $dokuWallet, $mandiriClickpay, $debitOnlineBni;
  private $pax;

  public function __construct() {

    parent::__construct();

    $this->paymentInformations = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);

    $this->bankTransferRequester = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\PaymentOpenRequester::class);

    $this->cc = app()->make(\Odeo\Domains\Payment\Doku\Creditcard\Submitter::class);

    $this->dokuWallet = app()->make(\Odeo\Domains\Payment\Doku\Dokuwallet\Submitter::class);

    $this->mandiriClickpay = app()->make(\Odeo\Domains\Payment\Doku\Mandiriclickpay\Submitter::class);

    $this->oCash = app()->make(\Odeo\Domains\Payment\Odeo\OCash\Submitter::class);

    $this->debitOnlineBni = app()->make(\Odeo\Domains\Payment\Doku\DebitOnlineBni\Submitter::class);

    $this->pax = app()->make(\Odeo\Domains\Payment\Pax\Submitter::class);
  }

  public function open(PipelineListener $listener, $data) {

    if (isset($data['tokenize'])) unset($data['tokenize']);

    $order = $this->orders->findById($data['order_id']);

    $payment = $this->payments->findByOrderId($order->id);

    if (!$payment) {
      return $listener->response(400);
    }

    if (!$paymentInformation = $this->paymentInformations->findById($payment->opc)) {
      return $listener->response(400);
    }

    if ($this->paymentValidator->isCc($paymentInformation->info_id)) {

      return $this->cc->submit($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isCcTokenization($paymentInformation->info_id)) {

      $data['tokenize'] = true;

      return $this->cc->submit($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isDokuWallet($paymentInformation->info_id)) {

      return $this->dokuWallet->submit($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isBankTransfer($paymentInformation->info_id)) {
      return $this->bankTransferRequester->open($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isMandiriClickPay($paymentInformation->info_id)) {

      return $this->mandiriClickpay->submit($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isMandiriClickPay($paymentInformation->info_id)) {

      return $this->mandiriClickpay->submit($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isOcash($paymentInformation->info_id)) {

      return $this->oCash->submit($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isDebitOnlineBni($paymentInformation->info_id)) {

      return $this->debitOnlineBni->submit($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isPax($paymentInformation->info_id)) {

      return $this->pax->submit($listener, $order, $data);

    }

    return $listener->response(400);

  }
}
