<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 10:48 PM
 */

namespace Odeo\Domains\Payment;


use Carbon\Carbon;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Helper\PaymentManager;

class PaymentRequester extends PaymentManager {

  private $paymentInformations, $orderCharger;

  private $bankTransferRequester, $oCash;

  private $cc, $dokuWallet, $va, $ccInstallment, $debitOnlineBni;

  private $kredivo, $mandiriClickPay, $briEpay, $danamonInternetBanking, $muamalatInternetBanking, $akulaku, $cimbClick, $cimbVa;

  private $pax;

  public function __construct() {

    parent::__construct();

    $this->paymentInformations = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);

    $this->paymentDetailizer = app()->make(\Odeo\Domains\Payment\Helper\PaymentDetailizer::class);

    $this->bankTransferRequester = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\PaymentInformationRequester::class);

    $this->orderCharger = app()->make(\Odeo\Domains\Order\Helper\OrderCharger::class);

    $this->cc = app()->make(\Odeo\Domains\Payment\Doku\Creditcard\Requester::class);

    $this->ccInstallment = app()->make(\Odeo\Domains\Payment\Doku\Installment\Requester::class);

    $this->dokuWallet = app()->make(\Odeo\Domains\Payment\Doku\Dokuwallet\Requester::class);

    $this->kredivo = app()->make(\Odeo\Domains\Payment\Kredivo\Checkouter::class);

    $this->va = app()->make(\Odeo\Domains\Payment\Doku\Vagroup\Requester::class);

    $this->mandiriClickPay = app()->make(\Odeo\Domains\Payment\Doku\Mandiriclickpay\Requester::class);

    $this->oCash = app()->make(\Odeo\Domains\Payment\Odeo\OCash\Requester::class);

    $this->briEpay = app()->make(\Odeo\Domains\Payment\Doku\Briepay\Requester::class);

    $this->danamonInternetBanking = app()->make(\Odeo\Domains\Payment\Doku\Danamon\Requester::class);

    $this->muamalatInternetBanking = app()->make(\Odeo\Domains\Payment\Doku\Muamalat\Requester::class);

    $this->debitOnlineBni = app()->make(\Odeo\Domains\Payment\Doku\DebitOnlineBni\Requester::class);

    $this->dokuVaDirect = app()->make(\Odeo\Domains\Payment\Doku\VaDirect\Requester::class);

    $this->prismalinkVaDirect = app()->make(\Odeo\Domains\Payment\Prismalink\VaDirect\Requester::class);

    $this->akulaku = app()->make(\Odeo\Domains\Payment\Akulaku\Checkouter::class);

    $this->cimbClick = app()->make(\Odeo\Domains\Payment\Doku\Cimb\Requester::class);

    //$this->cimbVa = app()->make(\Odeo\Domains\Payment\Cimb\Requester::class);

    $this->pax = app()->make(\Odeo\Domains\Payment\Pax\Requester::class);
  }

  public function request(PipelineListener $listener, $data) {

    if (isset($data['tokenize'])) unset($data['tokenize']);

    if (isset($data['idt'])) {
      $idt = $this->decodeIdt($data['idt']);
      $data = array_merge($data, $idt);
    }

    $order = $this->orders->findById($data['order_id']);
    if (!$paymentInformation = $this->paymentInformations->findById($data['opc'])) {
      return $listener->response(400, "OPC is not valid.");
    }

    if ($order->status >= OrderStatus::VERIFIED) {
      throw new \Exception('payment already processed');
    }

    // \Log::info('PAYMENT INFORMATION: '. $paymentInformation->info_id);

    $payment = null;

    if ($order->status == OrderStatus::CREATED) {

      $this->orderCharger->removeCharge($order, OrderCharge::UNIQUE_CODE);

      if ($payment = $this->payments->findByOrderId($order->id)) {
        $payment->reference_id = null;
      } else {
        $payment = $this->payments->getNew();
        $payment->order_id = $order->id;
      }

      $payment->opc = $paymentInformation->code;
      $payment->info_id = $paymentInformation->info_id;

      $this->payments->save($payment);

      if (isset($data['switch_user'])) {
        $order->user_id = $data['switch_user'];
        $this->orders->save($order);
      }

      if (isset($data['user_information'])) {

        if (isset($data['user_information']['phone_number']) && !$this->paymentValidator->isOcash($paymentInformation->info_id)) {

          $data['user_information']['phone_number'] = purifyTelephone($data['user_information']['phone_number']);

          if ($user = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class)->findByTelephone($data['user_information']['phone_number'])) {

            return $listener->response(400, [
              'phone_number_already_used' => true
            ]);

          }

        }

        if (isset($data['user_information']['email']) && (!filter_var($data['user_information']['email'], FILTER_VALIDATE_EMAIL))) {
          return $listener->response(400, 'Your email format is not valid');
        }


        $order->name = $data['user_information']['name'] ?? null;
        $order->email = $data['user_information']['email'] ?? null;
        $order->phone_number = purifyTelephone($data['user_information']['phone_number']) ?? null;
        $this->orders->save($order);
      }

      if (!isset($data['finish_redirect_url'])) {
        if ($order->platform_id == Platform::ANDROID) {
          $data['finish_redirect_url'] = baseUrl('v1/payment/redirect');
          $data['unfinish_redirect_url'] = baseUrl('v1/payment/redirect');
        }
      }

      if (isset($data['finish_redirect_url']) && false === strpos($data['finish_redirect_url'], '://')) {
        $data['finish_redirect_url'] = 'http://' . $data['finish_redirect_url'];
      }
      if (isset($data['unfinish_redirect_url']) && false === strpos($data['unfinish_redirect_url'], '://')) {
        $data['unfinish_redirect_url'] = 'http://' . $data['unfinish_redirect_url'];
      }


      if (!$this->paymentValidator->isOcash($paymentInformation->info_id) && !$this->paymentValidator->isCashOnDelivery($paymentInformation->info_id)) {

        $paymentData = $paymentInformation->information;

        // cloning paymentData to prevent error due to changing data in PaymentDetailizer
        $clonedPaymentData = clone $paymentData;
        $clonedPaymentData->opc_group = $paymentInformation->info_id;
        $paymentFee = $this->paymentDetailizer->detailize($order, $clonedPaymentData);
        if (isset($paymentFee->detail)
          && !is_object($paymentFee->detail) // to skip bank transfers' details
          && isset($paymentFee->detail['fee'])
        ) {
          $paymentFee = $paymentFee->detail['fee']['amount'];
        } else {
          $paymentFee = 0;
        }

        if ($order->total + $paymentFee < $paymentData->min_transaction && $data['auth']['platform_id'] != Platform::ADMIN) {
          return $listener->response(400,
            $paymentData->name . ' payment only accept minimum ' .
            app()->make(\Odeo\Domains\Transaction\Helper\Currency::class)
              ->formatPrice($paymentData->min_transaction)['formatted_amount']
          );
        } else if ($order->total + $paymentFee > $paymentData->max_transaction && $data['auth']['platform_id'] != Platform::ADMIN) {
          return $listener->response(400,
            $paymentData->name . ' payment only accept maximum ' .
            app()->make(\Odeo\Domains\Transaction\Helper\Currency::class)
              ->formatPrice($paymentData->max_transaction)['formatted_amount']
          );
        }
      }

    }

    if ($this->paymentValidator->isCc($paymentInformation->info_id)) {

      return $this->cc->request($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isCcTokenization($paymentInformation->info_id)) {

      $data['tokenize'] = true;

      return $this->cc->request($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isDokuWallet($paymentInformation->info_id)) {
      return $this->dokuWallet->request($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isAtmTransferVa($paymentInformation->info_id)) {
      $payment = $payment ? $payment : $this->payments->findByOrderId($order->id);
      return $this->va->request($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isAlfa($paymentInformation->info_id)) {
      $payment = $payment ? $payment : $this->payments->findByOrderId($order->id);
      return $this->va->request($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isKredivo($paymentInformation->info_id)) {
      return $this->kredivo->checkout($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isBankTransfer($paymentInformation->info_id)) {

      return $this->bankTransferRequester->request($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isCcInstallment($paymentInformation->info_id)) {

      return $this->ccInstallment->request($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isMandiriECash($paymentInformation->info_id)) {

      $mandiriECash = app()->make(\Odeo\Domains\Payment\Dam\Mandiriecash\Requester::class);
      return $mandiriECash->request($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isOcash($paymentInformation->info_id)) {

      return $this->oCash->request($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isCashOnDelivery($paymentInformation->info_id)) {

      $order->paid_at = Carbon::now();

      $this->orders->save($order);

      return $listener->response(200);

    } else if ($this->paymentValidator->isMandiriClickPay($paymentInformation->info_id)) {

      return $this->mandiriClickPay->request($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isMandiriMpt($paymentInformation->info_id)) {
      $mandiriMpt = app()->make(\Odeo\Domains\Payment\Dam\Mandirimpt\Requester::class);
      return $mandiriMpt->request($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isBriEpay($paymentInformation->info_id)) {

      return $this->briEpay->request($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isDanamonInternetBanking($paymentInformation->info_id)) {

      return $this->danamonInternetBanking->request($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isMuamalatInternetBanking($paymentInformation->info_id)) {

      return $this->muamalatInternetBanking->request($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isDebitOnlineBni($paymentInformation->info_id)) {

      return $this->debitOnlineBni->request($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isDokuVaDirect($paymentInformation->info_id)) {

      return $this->dokuVaDirect->request($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isPrismalinkVaDirect($paymentInformation->info_id)) {

      return $this->prismalinkVaDirect->request($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isAkulaku($paymentInformation->info_id)) {

      return $this->akulaku->checkout($listener, $order, $paymentInformation, $data);

    } else if ($this->paymentValidator->isCimbClick($paymentInformation->info_id)) {

      return $this->cimbClick->request($listener, $order, $paymentInformation, $payment, $data);

    } else if ($this->paymentValidator->isPax($paymentInformation->info_id)) {

      return $this->pax->request($listener, $order, $data);

    }

    /*else if($this->paymentValidator->isCimbVa($paymentInformation->info_id)) {
      return $this->cimbVa->request($listener, $order, $paymentInformation, $data);
    }*/

    return $listener->response(400, 'Something happen with out server, contact ODEO customer service for further information');


  }
}
