<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:08 PM
 */

namespace Odeo\Domains\Payment;


use Odeo\Domains\Constant\AssetAws;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Doku\Creditcard\Repository\PaymentDokuCcTokenizationRepository;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelInformationRepository;
use Odeo\Domains\Payment\Repository\PaymentUserDirectPaymentRepository;

class PaymentUserDirectSelector {

  private $directPayments, $users;

  public function __construct() {
    $this->directPayments = app()->make(PaymentUserDirectPaymentRepository::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->userCash = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function get(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];
    $user = $this->users->findById($userId);
    $payments = app()->make(PaymentOdeoPaymentChannelInformationRepository::class);
    $directPayments = $payments->getDirectPayments()->keyBy('id');
    $cashLimiter = app()->make(\Odeo\Domains\Transaction\Helper\CashLimiter::class);

    $ccTokenizationLists = null;
    $response[] = [
      'id' => Payment::OPC_GROUP_OCASH,
      'opc_group' => Payment::OPC_GROUP_OCASH,
      'name' => 'oCash',
      'group_name' => Payment::getPaymentMethodGroup(Payment::OPC_GROUP_OCASH)['name'],
      'fee' => [
        'amount' => 0,
        'type' => 'value'
      ],
      'balance' => $this->currency->formatPrice($this->userCash->getCashBalance($userId)[$userId]),
      'active' => $user->direct_payment_reference_type == Payment::OPC_GROUP_OCASH,
      'description' => isNewDesign() ? $cashLimiter->getCashLimit($user->id) : null,
      'accountName' => $user->name,
      'accountNumber' => revertTelephone($user->telephone),
      'logo' => AssetAws::getAssetPath(AssetAws::PAYMENT, $directPayments[Payment::OPC_GROUP_OCASH]['logo'])
    ];

    if (!$ccTokenizationLists) {
      $ccTokenizationLists = app()->make(PaymentDokuCcTokenizationRepository::class)
        ->findByUserId($userId)
        ->groupBy('id')
        ->toArray();
    }

    foreach ($this->directPayments->findByUserId($userId) as $p) {
      switch ($p->info_id) {
        case Payment::OPC_GROUP_CC_TOKENIZATION:
          if (isset($ccTokenizationLists[$p->reference_id])) {
            $token = $ccTokenizationLists[$p->reference_id][0];

            $response[] = [
              'id' => $p->id,
              'tokenization_id' => $p->reference_id,
              'opc_group' => Payment::OPC_GROUP_CC_TOKENIZATION,
              'name' => $directPayments[Payment::OPC_GROUP_CC_TOKENIZATION]['name'],
              'group_name' => Payment::getPaymentMethodGroup(Payment::OPC_GROUP_CC_TOKENIZATION)['name'],
              'fee' => [
                'amount' => 2.6,
                'type' => 'percentage'
              ],
              'active' => $user->direct_payment_reference_type == Payment::OPC_GROUP_CC_TOKENIZATION && $user->direct_payment_reference_id == $p->id,
              'description' => [
                'mcn' => $token['mcn'],
                'bank' => $token['bank'],
              ],
              'accountName' => $ccTokenizationLists[$p->reference_id][0]['user_cc_name'],
              'accountNumber' => $token['mcn'],
              'logo' => AssetAws::getAssetPath(AssetAws::PAYMENT, $directPayments[Payment::OPC_GROUP_CC_TOKENIZATION]['logo'])
            ];
          }
          break;
      }
    }
    $listener->response(200, $response);
  }


}
