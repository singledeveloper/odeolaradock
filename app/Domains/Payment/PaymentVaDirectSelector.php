<?php

namespace Odeo\Domains\Payment;

use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Payment\Bni\BniRequester;

class PaymentVaDirectSelector implements SelectorListener {

  public function __construct() {
    $this->virtualAccounts = app()->make(\Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountRepository::class);
    $this->virtualAccountDetails = app()->make(\Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountDetailRepository::class);
    $this->virtualAccountVendors = app()->make(\Odeo\Domains\VirtualAccount\Repository\VirtualAccountVendorRepository::class);
  }

  public function _extends($items, Repository $repository)
  {
    if ($accountNumbers = $repository->beginExtend($items, 'account_number')) {
      if ($repository->hasExpand('va_info')) {
        $infoList = $this->virtualAccountDetails->getVirtualAccountInfoByAccountNumber($accountNumbers);
        $results = [];

        if (count($infoList) != count($accountNumbers)) {
          clog('va-selector', 'infoList mismatch: ' . json_encode([
              'info_list' => $infoList,
              'account_numbers' => $accountNumbers,
            ]));
        }

        foreach ($infoList as $info) {
          $results[$info->virtual_account_number] = [
            'fee' => $info->fee,
            'channel_id' => $info->channel_id,
          ];
        }
        $repository->addExtend('va_info', $results);
      }
    }

    return $repository->finalizeExtend($items);
  }

  public function _transforms($item, Repository $repository) {
    $result = [];

    $result['bank_name'] = $item['vendor_name'];
    $result['account_number'] = $item['virtual_account_number'];
    // $result['fee'] = round($item['fee']);
    // $result['vendor_id'] = $item['vendor_id'];

    if ($repository->hasExpand('channel') && isset($item['channel_id'])) {
      $result['channel_id'] = $item['channel_id'];
    }

    return $result;
  }

  public function get(PipelineListener $listener, $data) {
    $this->virtualAccounts->normalizeFilters($data);
    $virtualAccount = $this->virtualAccounts->findByAttributes(array_merge([
      'user_id' => $data['user_id'] ?? $data['auth']['user_id'],
      'biller' => $data['biller_id'] ?? VirtualAccount::BILLER_ODEO,
      'service_detail_id' => $data['service_detail_id']
    ], (isset($data['store_id']) ? ['store_id' => $data['store_id']] : [])));

    $displayName = substr($data['user_name'], 0, 30);
    if (!$virtualAccount) {
      $virtualAccount = $this->virtualAccounts->getNew();
      $virtualAccount->user_id = $data['user_id'] ?? $data['auth']['user_id'];
      $virtualAccount->biller = $data['biller_id'] ?? VirtualAccount::BILLER_ODEO;
      $virtualAccount->store_id = $data['store_id'] ?? NULL;
      $virtualAccount->display_name = $displayName;
      $virtualAccount->service_detail_id = $data['service_detail_id'];
      $virtualAccount->service_reference_id = $data['reference_id'] ?? NULL;
      $virtualAccount->amount = $data['amount'];

      $virtualAccount->save();
      $existingVirtualAccounts = [];
    } else {
      $virtualAccount->display_name = $displayName;
      $virtualAccount->service_reference_id = $data['reference_id'] ?? NULL;
      $virtualAccount->amount = $data['amount'];
      $virtualAccount->order_id = NULL;

      $virtualAccount->save();
      $existingVirtualAccounts = $this->virtualAccountDetails->getByUserVirtualAccountId($virtualAccount->id)->keyBy('vendor')->toArray();
    }

    if (isset($data['virtual_account_vendors'])) {
      $vendors = $this->virtualAccountVendors->getAffiliatePaymentVendorsByCode(array_keys($data['virtual_account_vendors']));
    } else {
      $vendors = $this->virtualAccountVendors->getAffiliatePaymentVendors();
    }

    $virtualAccounts = [];
    $newVirtualAccounts = [];

    foreach ($vendors as $vendor) {
      $availableLength = max(8 - strlen($vendor->prefix), 2);

      $update = isset($existingVirtualAccounts[$vendor->code]) && $this->vendorNeedUpdate($vendor->code);

      if (!$update && isset($existingVirtualAccounts[$vendor->code])) continue;

      $va = [];
      $va['user_virtual_account_id'] = $virtualAccount->id;
      $va['virtual_account_number'] = $vendor->prefix 
        . ($availableLength ? sprintf('%0' . $availableLength . "s", 0) : '')
        . sprintf('%08s', $virtualAccount->id);
      $va['vendor'] = $vendor->code;
      $va['created_at'] = \Carbon\Carbon::now();
      $va['updated_at'] = \Carbon\Carbon::now();

      !$update && $virtualAccounts[$vendor->code] = $va;

      $va['vendor_name'] = $vendor->name;
      $va['vendor_code'] = $vendor->code;

      if (isset($data['virtual_account_vendors'])) {
        $va['fee'] = $data['virtual_account_vendors'][$vendor->code];
      } else {
        $va['fee'] = $vendor->fee;
      }

      $va['vendor_id'] = $vendor->id;

      !$update && $newVirtualAccounts[] = $va;

      switch ($vendor->code) {
        case VirtualAccount::VENDOR_BNI_ECOLLECTION:
          dispatch(new BniRequester(array_merge($data, [
            'amount' => $data['amount'] + $vendor->fee
          ]), $va));
          break;
        default:
          break;
      }
    }

    if (count($virtualAccounts)) $this->virtualAccountDetails->saveBulk($virtualAccounts);

    $userVirtualAccounts = array_merge($existingVirtualAccounts, $newVirtualAccounts);
    $results = [];

    foreach ($userVirtualAccounts as $userVirtualAccount) {
      $results[] = $this->_transforms($userVirtualAccount, $this->virtualAccounts);
    }
    $results = array_values(array_unique($results, SORT_REGULAR));
    return $listener->response(200, ['virtual_accounts' => $this->_extends($results, $this->virtualAccounts)]);
  }

  private function vendorNeedUpdate($code) {
    return in_array($code, [
      VirtualAccount::VENDOR_BNI_ECOLLECTION
    ]);
  }
}
