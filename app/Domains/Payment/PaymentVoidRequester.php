<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 10:48 PM
 */

namespace Odeo\Domains\Payment;


use Carbon\Carbon;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Odeo\Domains\Payment\Helper\PaymentManager;

class PaymentVoidRequester extends PaymentManager {

  private $paymentInformations, $dokuVoidRequester, $kredivoVoidRequester, $akulakuVoidRequester, $cashInserter;

  public function __construct() {

    parent::__construct();

    $this->paymentInformations = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);

    $this->dokuVoidRequester = app()->make(\Odeo\Domains\Payment\Doku\VoidRequester::class);

    $this->kredivoVoidRequester = app()->make(\Odeo\Domains\Payment\Kredivo\VoidRequester::class);

    $this->akulakuVoidRequester = app()->make(\Odeo\Domains\Payment\Akulaku\VoidRequester::class);

    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
  }

  public function void($data) {

    $payment = $this->payments->findByOrderId($data['order_id']);

    $paymentInformation = $this->paymentInformations->findById($payment->opc);

    if ($this->paymentValidator->isCc($paymentInformation->info_id)) {

      return $this->voidDoku($data, $payment);


    } else if ($this->paymentValidator->isCcTokenization($paymentInformation->info_id)) {

      return $this->voidDoku($data, $payment);


    } else if ($this->paymentValidator->isDokuWallet($paymentInformation->info_id)) {

      return $this->voidDoku($data, $payment);

    } else if ($this->paymentValidator->isKredivo($paymentInformation->info_id)) {

      return $this->kredivoVoidRequester->void($payment, [
        'order_id' => $data['order_id']
      ]);

    } else if ($this->paymentValidator->isAkulaku($paymentInformation->info_id)) {
      return $this->akulakuVoidRequester->void($data, $payment) || $this->oCashRefund($data);
    } else {
      return $this->oCashRefund($data);
    }

  }


  public function voidDoku($data, $payment) {

    $paidAt = Carbon::parse($data['paid_at']);
    $now = Carbon::now();

    $tressHold = Carbon::create($now->year, $now->month, $now->day, 21, 0, 0);
    if ($now->hour < 21) $tressHold = $tressHold->subDay(1);

    if ($paidAt->gte($tressHold)) {

      $res = $this->dokuVoidRequester->void(array_merge($data, [
        'payment_channel_code' => DokuInitializer::CC_CODE,
        'payment_reference_id' => $payment->reference_id,
        'payment_info_id' => $payment->info_id
      ]));

      if ($res) return true;
    }

    return $this->oCashRefund($data);

  }


  public function oCashRefund($data) {

    $this->cashInserter->add([
      'user_id' => $data['user_id'],
      'trx_type' => TransactionType::ORDER_REFUND,
      'cash_type' => CashType::OCASH,
      'amount' => $data['amount'],
      'data' => json_encode([
        'order_id' => $data['order_id']
      ])
    ]);

    $this->cashInserter->run();

    return true;
  }
}