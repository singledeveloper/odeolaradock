<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 20/08/19
 * Time: 13.24
 */

namespace Odeo\Domains\Payment\Permata\Va;


use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Permata\Va\Repository\PaymentPermataVaInquiryRepository;
use Odeo\Domains\Payment\Permata\Va\Repository\PaymentPermataVaPaymentRepository;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;

class Inquirer {

  private $pgUserPaymentchannel;

  function __construct() {
    $this->permataVaInquiry = app()->make(PaymentPermataVaInquiryRepository::class);
//    $this->permataVaPayment = app()->make(PaymentPermataVaPaymentRepository::class);
    $this->pgUserPaymentchannel = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
    $this->odeoPaymentChannel = app()->make(PaymentOdeoPaymentChannelRepository::class);
  }

  public function vaInquiry(PipelineListener $listener, $data) {

    if (!isset($data['GetBillRq'])) {
      return $listener->response(400, ['message' => 'Unknown data format', 'code'=> '01']);
    }

    $data = $data['GetBillRq'];

    $pgUserChannel = $this->pgUserPaymentchannel->findByActivePrefix($data['VI_VANUMBER']);
    if (!$pgUserChannel) {
      clog('permata_va', trans('payment.unrecognized_va_number'));
      return $listener->response(400, ['message' => trans('payment.unrecognized_va_number'), 'code' => '14']);
    }

    $channel = $this->odeoPaymentChannel->findByGatewayIdAndGroupId(Payment::PAYMENT_GATEWAY, $pgUserChannel->payment_group_id);
    if (!$channel) {
      clog('permata_va', 'channel not found');
      return $listener->response(400, ['message' => 'channel not found', 'code' => '14']);
    }

//    $payment = $this->permataVaPayment->findByPaidVaCode($data['VI_VANUMBER']);
//    if ($payment) {
//      clog('permata_va', 'duplicate inquiry');
//      return $listener->response(400, ['message' => 'duplicate inquiry', 'code' => '88']);
//    }

    $inquiry = $this->permataVaInquiry->findDuplicateInquiryWithTrace($data['VI_VANUMBER'], $data['VI_TRACENO']);
    if ($inquiry) {
      clog('permata_va', 'duplicate inquiry and trace');
      return $listener->response(400, ['message' => 'duplicate inquiry', 'code' => '95']);
    }

    $referenceId = round(microtime(true) * 10000);

    return $listener->response(200, [
      'va_code' => $data['VI_VANUMBER'],
      'va_prefix' => $pgUserChannel->prefix,
      'user_id' => $pgUserChannel->user_id,
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'trace_no' => $data['VI_TRACENO'],
      'vendor_reference_id' => $referenceId,
      'trn_date' => $data['VI_TRNDATE'],
      'del_channel' => $data['VI_DELCHANNEL'],
      'instcode' => $data['INSTCODE']
    ]);
  }

  public function createInquiry(PipelineListener $listener, $data) {

    $inquiry = $this->permataVaInquiry->getNew();
    $inquiry->instcode = $data['instcode'];
    $inquiry->va_number = $data['va_code'];
    $inquiry->trace_no = $data['trace_no'];
    $inquiry->trn_date = $data['trn_date'];
    $inquiry->del_channel = $data['del_channel'];
    $inquiry->cust_name = strtoupper($data['customer_name']);
    $inquiry->bill_amount = $data['amount'];
    $inquiry->reference_number = $data['vendor_reference_id'];
    $this->permataVaInquiry->save($inquiry);

    return $listener->response(200);
  }

}