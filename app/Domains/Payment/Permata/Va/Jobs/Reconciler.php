<?php

namespace Odeo\Domains\Payment\Permata\Va\Jobs;

use Carbon\Carbon;
use Odeo\Jobs\Job;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Repository\BankPermataInquiryRepository;
use Odeo\Domains\Payment\Permata\Va\Repository\PaymentPermataVaPaymentRepository;

class Reconciler extends Job {

  private $data, $vaPayment, $pgPayment, $permataInquiry;

  function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->vaPayment = app()->make(PaymentPermataVaPaymentRepository::class);
    $this->pgPayment = app()->make(PaymentGatewayPaymentRepository::class);
    $this->permataInquiry = app()->make(BankPermataInquiryRepository::class);
  }

  public function handle() {
    \DB::transaction(function() {
      $bankInquiry = $this->permataInquiry->findById($this->data['bank_inquiry_id']);
  
      if (isset($bankInquiry->reference_type) && $bankInquiry->reference_type != 'note') return;
      
      $bankInquiry->reference_type = 'payment_gateway_payment_id';
      $bankInquiry->reference = json_encode(["" . $this->data['pg_payment_id']]);
      $this->permataInquiry->save($bankInquiry);

      $pgPayment = $this->pgPayment->findById($this->data['pg_payment_id']);
      $pgPayment->reconciled_at = Carbon::now();
      $this->pgPayment->save($pgPayment);

      $vaPayment = $this->vaPayment->findById($this->data['va_payment_id']);
      $vaPayment->is_reconciled = true;
      $this->vaPayment->save($vaPayment);
    });
  }

}