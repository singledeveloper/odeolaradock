<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 20/08/19
 * Time: 13.15
 */

namespace Odeo\Domains\Payment\Permata\Va\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\Payment\Model\Payment;

class PaymentPermataVaPayment extends Entity {

  public function payment() {
    return $this->belongsTo(Payment::class);
  }

}