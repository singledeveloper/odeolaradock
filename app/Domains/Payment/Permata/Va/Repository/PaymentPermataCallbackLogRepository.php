<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 29/08/19
 * Time: 15.55
 */

namespace Odeo\Domains\Payment\Permata\Va\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Permata\Va\Model\PaymentPermataCallbackLog;

class PaymentPermataCallbackLogRepository extends Repository {

  function __construct(PaymentPermataCallbackLog $callbackLog) {
    $this->model = $callbackLog;
  }

}