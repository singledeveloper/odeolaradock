<?php

namespace Odeo\Domains\Payment\Prismalink\Helper;

class PrismalinkLibrary {

  public function generateSignature($data) {
    $partnerId = env("PRISMALINK_PARTNER_ID");
    $sharedKey = env("PRISMALINK_SHARED_KEY");

    if (!empty($data['netAmount'])) {
      return md5($partnerId . $data['accountNo'] . sprintf('%.2f', $data['amount']) . sprintf('%.2f', $data['feeAmount']) .
        sprintf('%.2f', $data['netAmount']) . $data['transDate'] . $data['traceNo'] . $sharedKey);
    } else if (!empty($data['amount'])) {
      return md5($partnerId . $data['accountNo'] . sprintf('%.2f', $data['amount']) . $data['transDate'] .
        $data['traceNo'] . $sharedKey);
    } else if (!empty($data['traceNo']) && $data['code'] != '01') {
      return md5($partnerId . $data['traceNo'] . $sharedKey);
    } else {
      return md5($partnerId . $data['accountNo'] . $sharedKey);
    }
  }

}
