<?php
namespace Odeo\Domains\Payment\Prismalink\VaDirect\Model;

use Odeo\Domains\Core\Entity;

class PaymentPrismalinkVaDirectPayment extends Entity {

  protected $dates = ['created_at', 'updated_at', 'paid_at', 'void_at'];

  public $timestamps = true;
  
}
