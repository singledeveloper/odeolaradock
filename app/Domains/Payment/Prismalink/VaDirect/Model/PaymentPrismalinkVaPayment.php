<?php
namespace Odeo\Domains\Payment\Prismalink\VaDirect\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Payment\Model\Payment;

class PaymentPrismalinkVaPayment extends Entity {

  public function payment() {
    return $this->belongsTo(Payment::class);
  }
}
