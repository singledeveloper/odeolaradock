<?php

namespace Odeo\Domains\Payment\Prismalink\VaDirect;

use Carbon\Carbon;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Prismalink\Helper\PrismalinkManager;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;
use Odeo\Exceptions\FailException;
use Illuminate\Support\Facades\Redis;

class Payer extends PrismalinkManager {

  private $payment, $paymentChannel, $prismalinkVaPayment, $prismalinkVaInquiry, $pgUserPaymentChannel, $redis;

  public function __construct() {
    parent::__construct();
    $this->payment = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);
    $this->paymentChannel = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);
    $this->prismalinkVaPayment = app()->make(\Odeo\Domains\Payment\Prismalink\VaDirect\Repository\PaymentPrismalinkVaPaymentRepository::class);
    $this->prismalinkVaInquiry = app()->make(\Odeo\Domains\Payment\Prismalink\VaDirect\Repository\PaymentPrismalinkVaInquiryRepository::class);
    $this->pgUserPaymentChannel = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
    $this->redis = Redis::connection();
  }

  public function vaPayment(PipelineListener $listener, $data) {
    try {
      $this->validateCodeAndPartnerID($data, ['02', '04']);
      $this->matchSignature($data);
    } catch (FailException $e) {
      return $listener->response(400, $e->getMessage());
    }

    $inquiry = $this->prismalinkVaInquiry->findByVirtualAccountCodeAndTraceNo($data['accountNo'], $data['traceNo']);
    if (!$inquiry) {
      return $listener->response(400, 'inquiry not found');
    }

    if ($data['code'] == '02') {
      $currentDate = Carbon::now()->format('Y-m-d');
      $namespace = $this->getRedisNamespace($currentDate);
      if (!$this->redis->hsetnx($namespace, $inquiry->id, 1)) {
        return $listener->response(400, 'duplicate transaction');
      }
      $this->redis->expire($namespace, 2 * 24 * 60 * 60);
  
      $tomorrowDate = Carbon::tomorrow()->format('Y-m-d');
      $namespace = $this->getRedisNamespace($tomorrowDate);
      if (!$this->redis->hset($namespace, $inquiry->id, 1)) {
        return $listener->response(400, 'duplicate transaction');
      }
      $this->redis->expire($namespace, 2 * 24 * 60 * 60);
    }

    $vaPayment = $this->prismalinkVaPayment->findByAttributes([
      'va_code' => $data['accountNo'],
      'trace_number' => $data['traceNo']
    ]);

    $pgUserChannel = $this->pgUserPaymentChannel->findByActivePrefix($data['accountNo']);
    if (!$pgUserChannel) {
      return $listener->response(400, 'payment.unrecognized_va_number');
    }

    $channel = $this->paymentChannel->findByGatewayIdAndGroupId(Payment::PAYMENT_GATEWAY, $pgUserChannel->payment_group_id);
    if (!$channel) {
      return $listener->response(400, 'channel not found');
    }

    if (!$vaPayment) {
      $vaPayment = $this->prismalinkVaPayment->getNew();
      $vaPayment->va_code = $data['accountNo'];
      $vaPayment->amount = $data['amount'];
      $vaPayment->net_amount = $data['netAmount'];
      $vaPayment->fee = $data['feeAmount'];
      $vaPayment->code_currency = $data['codeCurrency'];
      $vaPayment->payment_date = Carbon::now()->format('Y-m-d H:i:s');
      $vaPayment->trace_number = $data['traceNo'];
      $vaPayment->status = PaymentGateway::PENDING;
      $vaPayment->inquiry_id = $inquiry->id;
      $vaPayment->cost = $pgUserChannel->cost;
      $this->prismalinkVaPayment->save($vaPayment);

      $payment = $this->payment->getNew();
      $payment->source_type = PaymentChannel::PAYMENT_GATEWAY;
      $payment->info_id = $pgUserChannel->payment_group_id;
      $payment->opc = $channel->code;
      $payment->vendor_id = $vaPayment->id;
      $payment->vendor_type = 'va_prismalink';
      $this->payment->save($payment);
    } else {
      if ($vaPayment->status == PaymentGateway::SUCCESS) {
        return $listener->response(400, 'duplicate transaction');
      }

      $vaPayment->status = PaymentGateway::PENDING;
      $payment = $vaPayment->payment;
      
      if (!$payment) {
        $payment = $this->payment->findByAttributes('vendor_id', $vaPayment->id);
        $vaPayment->payment_id = $payment->id;
      }
      
      $this->prismalinkVaPayment->save($vaPayment);
    }

    return $listener->response(200, [
      'prismalink_payment_id' => $vaPayment->id,
      'payment_id' => $payment->id,
      'va_code' => $data['accountNo'],
      'va_prefix' => $pgUserChannel->prefix,
      'user_id' => $pgUserChannel->user_id,
      'payment_gateway_payment_id' => $payment->source_id,
      'billed_amount' => $data['amount'],
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'customer_name' => $inquiry->customer_name,
      'vendor_reference_id' => $data['traceNo']
    ]);
  }

  public function getRedisNamespace($date) {
    return "odeo_core:prismalink_used_inquiry_id_$date";
  }

  public function updatePayment(PipelineListener $listener, $data) {
    $vaPayment = $this->prismalinkVaPayment->findById($data['prismalink_payment_id']);
    $vaPayment->payment_id = $data['payment_id'];

    if ($data['code'] == '04' && PaymentGateway::getMerchantStatusMessage($data['pg_payment_status'], false) == 'api_failed') {
      $vaPayment->status = PaymentGateway::ACCEPTED;
      $this->prismalinkVaPayment->save($vaPayment);

      return $listener->response(200, []);
    } 
    
    $vaPayment->status = $data['pg_payment_status'];
    $this->prismalinkVaPayment->save($vaPayment);

    if ($data['pg_payment_status'] != PaymentGateway::ACCEPTED) {
      return $listener->response(400, 'status is not accepted');
    }

    return $listener->response(200, []);
  }
  
}
