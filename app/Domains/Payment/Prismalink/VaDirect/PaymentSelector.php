<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2018-12-14
 * Time: 22:54
 */

namespace Odeo\Domains\Payment\Prismalink\VaDirect;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Prismalink\VaDirect\Repository\PaymentPrismalinkVaPaymentRepository;

class PaymentSelector {

  private $paymentPrismalinkVaPaymentRepo;

  public function __construct() {
    $this->paymentPrismalinkVaPaymentRepo = app()->make(PaymentPrismalinkVaPaymentRepository::class);
  }

  public function transforms($item) {
    return $item;
  }

  public function getAll(PipelineListener $listener, $data) {
    $this->paymentPrismalinkVaPaymentRepo->normalizeFilters($data);
    $this->paymentPrismalinkVaPaymentRepo->setSimplePaginate(true);

    $result = [];
    $payments = $this->paymentPrismalinkVaPaymentRepo->get();

    foreach ($payments as $item) {
      $temp = $this->transforms($item);
      $result[] = $temp;
    }

    if (!empty($result)) {
      return $listener->response(200, array_merge(
        ['payments' => $result],
        $this->paymentPrismalinkVaPaymentRepo->getPagination()
      ));
    }

    return $listener->response(204, ['payments' => []]);
  }

  public function getPaidTransactionCount(PipelineListener $listener, $data) {
    $count = $this->paymentPrismalinkVaPaymentRepo->getPaidTransactionCount($data['start_date'], $data['end_date']);
    return $listener->response(200, ['va_payment_count' => $count]);
  }
}

