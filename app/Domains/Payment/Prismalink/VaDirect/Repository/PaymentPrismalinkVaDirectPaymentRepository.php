<?php
namespace Odeo\Domains\Payment\Prismalink\VaDirect\Repository;

use Illuminate\Support\Facades\DB;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Prismalink\VaDirect\Model\PaymentPrismalinkVaDirectPayment;

class PaymentPrismalinkVaDirectPaymentRepository extends Repository {

  public function __construct(PaymentPrismalinkVaDirectPayment $prismalinkVaDirectPayment) {
    $this->model = $prismalinkVaDirectPayment;
  }

  public function get() {
    $filter = $this->getFilters();
    $query = $this->model;

    if (isset($filter['search'])) {
      $search = $filter['search'];

      $dateCol = $search['date_type'] ?? 'created_at';
      if (isset($search['start_date'])) $query = $query->whereDate($dateCol, '>=', $search['start_date']);
      if (isset($search['end_date'])) $query = $query->whereDate($dateCol, '<=', $search['end_date']);

      if (isset($search['id'])) {
        $query = $query->whereId($search['id']);
      }

      if (isset($search['order_id'])) {
        $query = $query->where('order_id', $search['order_id']);
      }

      if (isset($search['bank_reference_id'])) {
        $query = $query->where('bank_reference_id', $search['bank_reference_id']);
      }

      if (isset($search['status'])) {
        switch ($search['status']) {
          case 'unpaid':
            $query = $query->whereNull('paid_at');
            break;
          case 'paid':
            $query = $query->whereNotNull('paid_at');
            break;
          case 'unsettled':
            $query = $query->whereNull('bank_reference_id')
              ->whereNotNull('paid_at');
            break;
          case 'settled':
            $query = $query->whereNotNull('bank_reference_id');
            break;
        }
      }
    }

    return $this->getResult($query->orderBy('order_id', 'desc'));
  }

  public function getUnsettled($prefix = null) {
    $query = $this->getCloneModel();

    if (isset($prefix)) {
      $query = $query->whereRaw("substr(virtual_account_number, 1, 5) = ?", [$prefix]);
    }
    
    return $query
      ->where('is_reconciled', false)
      ->whereNull('void_at')
      ->orderBy('paid_at')
      ->get();
  }

  public function getFirstSettlementPayment() {
    return $this->model
      ->whereDate('paid_at', '<', '2019-04-23')
      ->select(DB::raw('order_id::varchar'))
      ->get();
  }

  public function getSettlementSummary() {
    $totalPayment = $this->getModel()
      ->whereNotNull('paid_at')
      ->select(DB::raw('sum(amount) as total'))->first()->total;

    $totalSettled = $this->getModel()
      ->where('is_reconciled', true)
      ->select(DB::raw('sum(amount) as total'))->first()->total;

    $totalUnsettled = $this->getModel()
      ->whereNotNull('paid_at')
      ->where('is_reconciled', false)
      ->select(DB::raw('sum(amount) as total'))->first()->total;

    return [
      'total_payment' => $totalPayment,
      'total_settled' => $totalSettled,
      'total_unsettled' => $totalUnsettled
    ];
  }

  public function getPaidTransactionCount($startDate, $endDate) {
    $q = $this->getModel()
      ->where('is_reconciled', true)
      ->whereNotNull('paid_at');
    if ($startDate != '') $q = $q->whereDate('paid_at', '>=', $startDate);
    if ($endDate != '') $q = $q->whereDate('paid_at', '<=', $endDate);
    return $q->count();
  }

}
