<?php
namespace Odeo\Domains\Payment\Prismalink\VaDirect\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Prismalink\VaDirect\Model\PaymentPrismalinkVaInquiry;

class PaymentPrismalinkVaInquiryRepository extends Repository {

  public function __construct(PaymentPrismalinkVaInquiry $prismalinkVaInquiry) {
    $this->model = $prismalinkVaInquiry;
  }

  public function get() {
    $filter = $this->getFilters();
    $query = $this->model;

    if (isset($filter['search'])) {
      $search = $filter['search'];

      if (isset($search['id'])) {
        $query = $query->where('id', $search['id']);
      }
      if (isset($search['va_code'])) {
        $query = $query->where('va_code', $search['va_code']);
      }
      if (isset($search['display_text'])) {
        $query = $query->where('display_text', 'ilike', '%' . $search['display_text'] . '%');
      }
      if (isset($search['customer_name'])) {
        $query = $query->where('customer_name', 'ilike', '%' . $search['customer_name'] . '%');
      }
      if (isset($search['trace_number'])) {
        $query = $query->where('trace_number', $search['trace_number']);
      }
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function findByVirtualAccountCodeAndAmount($vaCode, $amount) {
    return $this->model->where('va_code', $vaCode)->where('amount', $amount)->first();
  }

  public function findByVirtualAccountCodeAndTraceNo($vaCode, $traceNo) {
    return $this->model->where('va_code', $vaCode)->where('trace_number', $traceNo)->first();
  }

}
