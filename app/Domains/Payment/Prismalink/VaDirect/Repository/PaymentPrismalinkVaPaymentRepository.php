<?php
namespace Odeo\Domains\Payment\Prismalink\VaDirect\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Constant\PrismalinkPaymentStatus;
use Odeo\Domains\Payment\Prismalink\VaDirect\Model\PaymentPrismalinkVaPayment;

class PaymentPrismalinkVaPaymentRepository extends Repository {

  public function __construct(PaymentPrismalinkVaPayment $prismalinkVaPayment) {
    $this->model = $prismalinkVaPayment;
  }

  public function get() {
    $filter = $this->getFilters();

    $query = $this
      ->model
      ->join('payments', 'payments.id', '=', 'payment_prismalink_va_payments.payment_id')
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payments.info_id')
      ->join('payment_prismalink_va_inquiries', 'payment_prismalink_va_inquiries.id', '=', 'payment_prismalink_va_payments.inquiry_id')
      ->select([
        'payment_prismalink_va_payments.id',
        'payment_odeo_payment_channel_informations.name as payment_method',
        'payment_prismalink_va_payments.va_code',
        'display_text',
        'customer_name',
        'payment_prismalink_va_payments.amount',
        'payment_prismalink_va_payments.trace_number',
        'status',
        'source_id',
        'source_type',
        'payment_prismalink_va_payments.created_at',
      ]);

    if (isset($filter['search'])) {
      $search = $filter['search'];

      if (isset($search['id'])) {
        $query->where('payment_prismalink_va_payments.id', $search['id']);
      }

      if (isset($search['va_code'])) {
        $query->where('payment_prismalink_va_payments.va_code', $search['va_code']);
      }

      if (isset($search['payment_group_id'])) {
        $query->where('payment_odeo_payment_channel_informations.id', $search['payment_group_id']);
      }

      if (isset($search['trace_number'])) {
        $query->where('payment_prismalink_va_payments.trace_number', $search['trace_number']);
      }

      if (isset($search['status'])) {
        $query->where('payment_prismalink_va_payments.status', $search['status']);
      }
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function getUnsettled($prefix = null) {
    $query = $this->getCloneModel();
    
    if (isset($prefix)) {
      $query = $query->whereRaw("substr(va_code, 1, 5) = ?", [$prefix]);
    }
    
    return $query
      ->where('is_reconciled', false)
      ->where('status', PrismalinkPaymentStatus::SUCCESS)
      ->orderBy('payment_date')
      ->get();
  }

  public function getPendingPayment() {
    return $this->model
      ->whereNotNull('payment_id')
      ->whereIn('status', [
        PrismalinkPaymentStatus::ACCEPTED,
        PrismalinkPaymentStatus::PENDING,
        PrismalinkPaymentStatus::SUSPECT_FOR_TESTING,
        PrismalinkPaymentStatus::SUSPECT_TIMEOUT,
        PrismalinkPaymentStatus::SUSPECT_NEED_STATUS_RECHECK,
        PrismalinkPaymentStatus::SUSPECT_IS_CANCELLED_BUT_NEED_STATUS_RECHECK
      ])
      ->orderBy('id')
      ->limit(40)
      ->get();
  }

  public function getPaidTransactionCount($startDate, $endDate) {
    $q = $this
      ->getModel()
      ->where('is_reconciled', true)
      ->whereIn('status', [PrismalinkPaymentStatus::ACCEPTED, PrismalinkPaymentStatus::SUCCESS]);
    if ($startDate != '') $q = $q->whereDate('payment_date', '>=', $startDate);
    if ($endDate != '') $q = $q->whereDate('payment_date', '<=', $endDate);
    return $q->count();
  }

}
