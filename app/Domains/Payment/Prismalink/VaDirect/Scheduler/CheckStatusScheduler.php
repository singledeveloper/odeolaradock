<?php

namespace Odeo\Domains\Payment\Prismalink\VaDirect\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Payment\Prismalink\VaDirect\Repository\PaymentPrismalinkVaPaymentRepository;
use Odeo\Domains\Payment\Prismalink\VaDirect\Jobs\InquiryStatus;

class CheckStatusScheduler {

  private $prismalinkVaPayment;

  public function __construct() {
    $this->prismalinkVaPayment = app()->make(PaymentPrismalinkVaPaymentRepository::class);
  }

  public function run() {
    $pendingPayments = $this->prismalinkVaPayment->getPendingPayment();
    foreach($pendingPayments as $pendingPayment) {
      dispatch(new InquiryStatus($pendingPayment));
    }
  }

}
