<?php
namespace Odeo\Domains\Payment\Doku\VaDirect;

use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Prismalink\Helper\PrismalinkManager;

class VoidRequester extends PrismalinkManager {

  private $prismalinkVaDirectPayments;

  public function __construct() {
    parent::__construct();
    $this->prismalinkVaDirectPayments = app()->make(\Odeo\Domains\Payment\Doku\Prismalink\Repository\PaymentPrismalinkVaDirectPaymentRepository::class);
  }

  public function void(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {

    $prismalinkVaDirectPayment = $this->dokuVaDirectPayments->findById($payment->reference_id);

    if ($data['signature'] == $this->library->generateSignature($data)) {

      $prismalinkVaDirectPayment->payment_code = $data['code'];
      $prismalinkVaDirectPayment->payment_fee = $data['feeAmount'];
      $prismalinkVaDirectPayment->payment_net_amount = $data['netAmount'];
      $prismalinkVaDirectPayment->currency_code = $data['codeCurrency'];
      $prismalinkVaDirectPayment->payment_date = $data['transDate'];
      $prismalinkVaDirectPayment->reference_number = $data['traceNo'];

      $this->prismalinkVaDirectPayments->save($prismalinkVaDirectPayment);

      $order->status = OrderStatus::VERIFIED;
      $order->paid_at = date('Y-m-d H:i:s');

      $this->orders->save($order);

      $listener->pushQueue(new VerifyOrder($order->id));

      return $listener->response(200, [
        'responseCode' => '00',
        'responseMessage' => 'Success',
        'customerUsername' => $order->name
        ]);
      } else {
        return $listener->response(200, [
          'responseCode' => '01',
          'responseMessage' => 'Invalid Signature',
          'customerUsername' => ''     
      ]);
    }
  }
}
