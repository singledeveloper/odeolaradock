<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 3:27 PM
 */

namespace Odeo\Domains\Payment\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Model\PaymentDirectUrl;

class PaymentDirectUrlRepository extends Repository {

  public function __construct(PaymentDirectUrl $paymentUrl) {
    $this->model = $paymentUrl;
  }

  public function getExisting($data) {

    $query = $this->getCloneModel();

    if (isset($data['opc_group'])) $query = $query->where('opc_group', $data['opc_group']);
    if (isset($data['reference_id'])) $query = $query->where('reference_id', $data['reference_id']);

    return $query;
  }

}