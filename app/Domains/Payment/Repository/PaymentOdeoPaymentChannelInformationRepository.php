<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 9:36 PM
 */

namespace Odeo\Domains\Payment\Repository;


use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Salesreport\Helper\Initializer;
use Odeo\Domains\Payment\Model\PaymentOdeoPaymentChannelInformation;

class PaymentOdeoPaymentChannelInformationRepository extends Repository {


  public function __construct(PaymentOdeoPaymentChannelInformation $paymentOdeoPaymentChannelInformation) {
    $this->model = $paymentOdeoPaymentChannelInformation;
  }

  public function gets() {

    $query = $this->getCloneModel();

    if (isset($filters['search'])) {
      if (isset($filters['search']['chanel_list'])) {
        $id = $filters['search']['chanel_list'];
        $query = $query->whereHas('channels', function ($query) use ($id) {
          $query->where('payment_odeo_payment_channels.gateway_id', $id);
        });
      }
    }

    $query = $query->with(['channels']);

    return $this->getResult($query);

  }


  public function getDirectPayments() {

    $query = $this->getCloneModel();

    $query = $query->where('is_direct_payment', true);

    return $this->getResult($query);

  }

  public function getFilter() {
    return $this->model->whereIn('id', [
      Payment::OPC_GROUP_CC_TOKENIZATION,
      Payment::OPC_GROUP_OCASH,
    ])->select('id', 'name')->get();
  }

  public function getByPaymentProduct() {

    $query = $this->getCloneModel();

    $query = $query->whereIn("id", Initializer::OPC_GROUP_ID);

    return $query->get();

  }

}
