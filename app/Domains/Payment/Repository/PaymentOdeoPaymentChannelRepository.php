<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 9:36 PM
 */

namespace Odeo\Domains\Payment\Repository;


use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Model\PaymentOdeoPaymentChannel;

class PaymentOdeoPaymentChannelRepository extends Repository {

  private $orders;

  public function __construct(PaymentOdeoPaymentChannel $paymentOdeoPaymentChannel) {
    $this->model = $paymentOdeoPaymentChannel;

    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
  }

  public function getEnabled($gateway_id) {

    $filters = $this->getFilters();

    return $this->model
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channels.info_id', '=', 'payment_odeo_payment_channel_informations.id')
      ->where('gateway_id', $gateway_id)
      ->where(function ($query) use ($filters) {

        $query->where('active', true);

        if (isset($filters['enable_beta_payment']) && $filters['enable_beta_payment']) {
          $query->orWhereIn('info_id', [
            Payment::OPC_GROUP_CIMB_CLICK
          ]);
        }
      })
      ->orderBy('payment_odeo_payment_channel_informations.priority', 'asc')
      ->get();
  }

  public function findByGatewayIdAndGroupId($gatewayId, $groupId) {
    return $this->model->where('gateway_id', $gatewayId)->where('info_id', $groupId)->first();
  }  

}
