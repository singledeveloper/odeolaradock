<?php

namespace Odeo\Domains\Payment\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Model\PaymentSettlementDetail;

class PaymentSettlementDetailRepository extends Repository {

  public function __construct(PaymentSettlementDetail $paymentSettlementDetail) {
    $this->model = $paymentSettlementDetail;
  }

  public function gets() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel();

    if (isset($filters['search'])) {
      if (isset($filters['search']['payment_settlement_id'])) {
        $query = $query->where('payment_settlement_id', $filters['search']['payment_settlement_id']);
      }
    }

    return $this->getResult($query->orderBy($filters['sort_by'], $filters['sort_type']));
  }
}