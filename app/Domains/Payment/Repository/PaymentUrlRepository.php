<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 3:27 PM
 */

namespace Odeo\Domains\Payment\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Model\PaymentUrl;

class PaymentUrlRepository extends Repository {

  public function __construct(PaymentUrl $paymentUrl) {
    $this->model = $paymentUrl;
  }

  public function getExisting($data) {

    $query = $this->getCloneModel();

    if (isset($data['opc'])) $query = $query->where('opc', $data['opc']);
    if (isset($data['order_id'])) $query = $query->where('order_id', $data['order_id']);

    return $query;
  }

}