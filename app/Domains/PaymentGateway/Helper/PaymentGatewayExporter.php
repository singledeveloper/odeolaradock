<?php

namespace Odeo\Domains\PaymentGateway\Helper;


use Box\Spout\Writer\WriterFactory;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository;

class PaymentGatewayExporter {

  private $paymentGatewayPaymentRepo;

  public function __construct() {
    $this->paymentGatewayPaymentRepo = app()->make(PaymentGatewayPaymentRepository::class);
  }

  public function exportPaymentGatewayPayment($filter) {
    $this->paymentGatewayPaymentRepo->normalizeFilters(array_merge($filter, [
      'is_paginate' => false,
    ]));

    $list = $this->paymentGatewayPaymentRepo->getAllPaymentByUser($filter['user_id']);
    $writer = WriterFactory::create($filter['file_type']);
    ob_start();

    $writer->openToFile('php://output');
    $writer->addRow([
      'created_at',
      'id',
      'payment_method',
      'status',
      'status_message',
      'reference_id',
      'amount',
      'fee',
      'settlement_amount',
      'settlement_date',
      'va_code'
    ]);

    foreach ($list as $item) {
      $writer->addRow([
        $item['created_at']->format('Y-m-d H:i:s'),
        $item['id'],
        $item['payment_method'],
        PaymentGateway::getMerchantStatusCode($item['status']),
        PaymentGateway::getMerchantStatusMessage($item['status']),
        $item['external_reference_id'],
        $item['amount'],
        $item['fee'],
        $item['amount'] - $item['fee'],
        $item['settlement_at']->format('Y-m-d H:i:s'),
        $item['va_code']
      ]);
    }

    $writer->close();

    return ob_get_clean();
  }
}
