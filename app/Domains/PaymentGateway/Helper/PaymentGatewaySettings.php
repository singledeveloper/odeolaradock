<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/07/19
 * Time: 10.53
 */

namespace Odeo\Domains\PaymentGateway\Helper;


use Odeo\Domains\Constant\Payment;

class PaymentGatewaySettings {

  const CHANNEL_TYPE_PG = 'payment_gateway';

  const SETTLEMENT_TYPE_DEFAULT = '2111113';
  const SETTLEMENT_TYPE_WEEKDAY = '2000003';
  const SETTLEMENT_TYPE_REALTIME = '0000000';

  const PG_CHANNEL_ALFA = 'alfa';
  const PG_CHANNEL_BCA_CLOSED = 'bca_closed';
  const PG_CHANNEL_BCA_OPEN = 'bca_open';

  const VENDOR_BCA_VA_CLOSED = 6;
  const VENDOR_BCA_VA_OPEN = 7;
  const VENDOR_ALFA = 10;

  public static function getDefaultVendorId(){
    if (isProduction()) {
      return self::VENDOR_BCA_VA_OPEN;
    }
    return 9;
  }

  public static function getAvailablePaymentGroup() {
    return [
      Payment::OPC_GROUP_ALFA,
      Payment::OPC_GROUP_VA_BCA
    ];
  }

  public static function getChannelPrefix($prefix) {
    if (in_array($prefix, [
      '10108',
      '10698', // BCA Production
      '74014'
    ])) {
      return $prefix * 100;
    }
    return $prefix * 100;
  }

}