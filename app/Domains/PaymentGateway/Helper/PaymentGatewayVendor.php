<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 30/09/19
 * Time: 15.00
 */

namespace Odeo\Domains\PaymentGateway\Helper;


use Odeo\Domains\Payment\Artajasa\Va\Repository\PaymentArtajasaVaPaymentRepository;
use Odeo\Domains\Payment\Cimb\Repository\PaymentCimbVaPaymentRepository;
use Odeo\Domains\Payment\Doku\Vagroup\Repository\PaymentDokuVaPaymentRepository;
use Odeo\Domains\Payment\Mandiri\Repository\PaymentMandiriVaPaymentRepository;
use Odeo\Domains\Payment\Permata\Va\Repository\PaymentPermataVaPaymentRepository;
use Odeo\Domains\Payment\Prismalink\VaDirect\Repository\PaymentPrismalinkVaPaymentRepository;

class PaymentGatewayVendor {

  public static function getVendorPaymentRepo($vendorType) {
    switch ($vendorType) {
      case 'va_prismalink':
        return app()->make(PaymentPrismalinkVaPaymentRepository::class);
      case 'va_doku':
        return app()->make(PaymentDokuVaPaymentRepository::class);
      case 'va_artajasa':
        return app()->make(PaymentArtajasaVaPaymentRepository::class);
      case 'va_permata':
        return app()->make(PaymentPermataVaPaymentRepository::class);
      case 'va_mandiri':
        return app()->make(PaymentMandiriVaPaymentRepository::class);
      case 'va_cimb':
        return app()->make(PaymentCimbVaPaymentRepository::class);
    }

    return null;
  }

}