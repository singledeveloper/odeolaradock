<?php

namespace Odeo\Domains\PaymentGateway\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendPaymentGatewayReconMismatchAlert extends Job  {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    Mail::send('emails.pg_recon_mismatch', [
      'data' => $this->data
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('pg@odeo.co.id')->subject($this->data['vendor'] . ' Recon Mismatch - ' . date('Y-m-d'));
    });
  }

}
