<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 26/07/19
 * Time: 19.17
 */

namespace Odeo\Domains\PaymentGateway\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\VirtualAccount\Model\VirtualAccountVendor;

class PaymentGatewayChannel extends Entity {

  public function vaVendor() {
    return $this->belongsTo(VirtualAccountVendor::class);
  }

}