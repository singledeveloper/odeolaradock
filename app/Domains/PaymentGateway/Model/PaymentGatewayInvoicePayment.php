<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 17/10/19
 * Time: 18.34
 */

namespace Odeo\Domains\PaymentGateway\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\Order\Model\Order;

class PaymentGatewayInvoicePayment extends Entity {

  protected $dates = ['created_at', 'updated_at', 'settlement_at', 'paid_at'];

  function __construct() {
    parent::__construct();
  }

  function order() {
    return $this->belongsTo(Order::class);
  }

}