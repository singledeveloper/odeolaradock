<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 27/11/18
 * Time: 19.46
 */

namespace Odeo\Domains\PaymentGateway\Model;

use Odeo\Domains\Core\Entity;

class PaymentGatewayPayment extends Entity {

  protected $dates = ['settlement_at', 'notify_at', 'reconciled_at'];
  
  public function __construct() {
    parent::__construct();
  }

  public function pgUser() {
    return $this->belongsTo(PaymentGatewayUser::class, 'pg_user_id');
  }

}
