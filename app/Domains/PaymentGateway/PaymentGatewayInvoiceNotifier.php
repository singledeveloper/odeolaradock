<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 18/10/19
 * Time: 13.22
 */

namespace Odeo\Domains\PaymentGateway;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Settlement;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceRepository;
use Odeo\Domains\Invoice\Repository\BusinessInvoiceUserRepository;
use Odeo\Domains\Invoice\Repository\BusinessInvoiceUserSettingRepository;
use Odeo\Domains\Order\OrderVerificator;
use Odeo\Domains\Order\Repository\OrderRepository;
use Odeo\Domains\Payment\Repository\PaymentRepository;
use Odeo\Domains\PaymentGateway\Helper\SettlementConfig;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayChannelRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayInvoicePaymentRepository;

class PaymentGatewayInvoiceNotifier {

  function __construct() {
    $this->pgInvoicePayment = app()->make(PaymentGatewayInvoicePaymentRepository::class);
    $this->userInvoice = app()->make(AffiliateUserInvoiceRepository::class);
    $this->orders = app()->make(OrderRepository::class);
    $this->payments = app()->make(PaymentRepository::class);
    $this->invoiceUsers = app()->make(BusinessInvoiceUserRepository::class);
    $this->invoiceUserSettings = app()->make(BusinessInvoiceUserSettingRepository::class);
    $this->redis = Redis::connection();
  }

  public function notify(PipelineListener $listener, $data) {
    if (!$order = $this->orders->findById($data['order_id'])) {
      return $listener->response(400);
    }

    if (!$payment = $this->payments->findByOrderId($data['order_id'])) {
      return $listener->response(400);
    }

    if (!$invoicePayment = $this->pgInvoicePayment->findById($payment->reference_id)) {
      return $listener->response(400);
    }

    $invoiceUser = $this->invoiceUsers->findByUserId($order->user_id);

    $invoicePayment->paid_at = Carbon::now();
    $invoicePayment->settlement_at = $this->getSettlementAt($invoiceUser, $data['virtual_account_number']);
    $invoicePayment->settlement_status = Settlement::ON_HOLD;
    $invoicePayment->business_invoice_user_id = $invoiceUser ? $invoiceUser->id : null;
    $this->pgInvoicePayment->save($invoicePayment);

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = $invoicePayment->paid_at;
    $order->settlement_at = $invoicePayment->settlement_at;

    $this->orders->save($order);

    if ($invoicePayment->invoice_id != null && $invoice = $this->userInvoice->findById($invoicePayment->invoice_id)) {
      // business invoice payment
      $invoice->paid_at = $invoicePayment->paid_at;
      $invoice->status = UserInvoice::VERIFIED;
      $this->userInvoice->save($invoice);
    }

    $listener->addNext(new Task(OrderVerificator::class, 'verify'));
    $listener->addNext(new Task(OrderVerificator::class, 'completeOrder'));

    $namespace = $this->getRedisNamespace($data['vendor_code'], $data['virtual_account_number']);
    $this->redis->setex($namespace, 10 * 60, 1);
    $description = isset($invoice) && $invoice->description ? $invoice->description : $order->name;

    return $listener->response(200, [
      'order_id' => $order->id,
      'customer_name' => $order->name,
      'description' => $description
    ]);
  }

  private function getRedisNamespace($vendor, $vaNo) {
    return "odeo_core:invoice_payment_{$vendor}_{$vaNo}";
  }

  private function getSettlementAt($invoiceUser, $vaCode) {
    $settlementConfig = app()->make(SettlementConfig::class);
    if ($invoiceUser) {
      $setting = $this->invoiceUserSettings->findByBusinessInvoiceUserIdAndVaCode($invoiceUser->id, $vaCode);
      $settlementType = $setting->settlement_type;
      $paymentGroupId = $setting->payment_group_id;
    } else {
      $paymentGatewayChannels = app()->make(PaymentGatewayChannelRepository::class);
      $setting = $paymentGatewayChannels->findByVaCode($vaCode);
      $settlementType = $setting->default_settlement_type;
      $paymentGroupId = $setting->payment_group_id;
    }

    return $settlementConfig->getSettlementAt($paymentGroupId, $settlementType, Carbon::now());
  }

}