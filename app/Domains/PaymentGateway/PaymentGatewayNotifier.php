<?php

namespace Odeo\Domains\PaymentGateway;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Odeo\Domains\Activity\Helper\HolidayHelper;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Constant\Settlement;
use Odeo\Domains\Constant\WebhookNotifyType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelInformationRepository;
use Odeo\Domains\Payment\Repository\PaymentRepository;
use Odeo\Domains\PaymentGateway\Helper\SettlementConfig;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayNotifyLogRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;
use Odeo\Domains\OAuth2\Helper\SignatureGenerator;
use Odeo\Exceptions\FailException;

class PaymentGatewayNotifier {

  private $paymentGatewayPaymentRepo, $paymentGatewayNotifyLogRepo, $paymentGatewayUserRepo,
    $paymentGatewayUserPaymentChannelRepo, $settlementConfig, $signatureGenerator;

  public function __construct() {
    $this->paymentRepo = app()->make(PaymentRepository::class);
    $this->paymentGatewayPaymentRepo = app()->make(PaymentGatewayPaymentRepository::class);
    $this->paymentGatewayNotifyLogRepo = app()->make(PaymentGatewayNotifyLogRepository::class);
    $this->paymentGatewayUserRepo = app()->make(PaymentGatewayUserRepository::class);
    $this->paymentGatewayUserPaymentChannelRepo = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
    $this->settlementConfig = app()->make(SettlementConfig::class);
    $this->signatureGenerator = app()->make(SignatureGenerator::class);
    $this->holidayHelper = app()->make(HolidayHelper::class);
  }

  public function notifyVAInquiry(PipelineListener $listener, $data) {
    try {
      $paymentGatewayUser = $this->getAndValidatePaymentGatewayUser($data['user_id']);

      if (isProduction()) {
        $redis = Redis::connection();
        $key = 'odeo_core:payment_gateway_va_' . $data['va_code'];

        if (!$redis->setnx($key, 1)) {
          throw new FailException('VA payment in process. Please try again later');
        }
        $redis->expire($key, 5 * 60);
      }
      
      $this->validateVA($paymentGatewayUser->channels, $data['va_code'], $data['va_prefix']);
      $paymentGatewayUserPaymentChannel = $this->getAndValidateUserPaymentChannel($paymentGatewayUser->id, $data['payment_group_id']);
      
      $url = $paymentGatewayUser->notify_url;
      $response = $this->notify(
        $url, 
        $data['user_id'], 
        [
          'notify_type' => WebhookNotifyType::VA_INQUIRY,
          'va_code' => $data['va_code']
        ],
        $paymentGatewayUser->id,
        null,
        $data['vendor_reference_id'],
        $paymentGatewayUserPaymentChannel->id
      );
    } catch (FailException $e) {
      return $listener->response(400, $e->getMessage());
    }

    if (!$response || !isset($response->amount) || !isset($response->customer_name) || !isset($response->display_text) || !isset($response->item_name)) {
      return $listener->response(400, 'Invalid response from client');
    }
    if ($response->amount == 0 || $response->customer_name == '' || $response->display_text == '' || $response->item_name == '') {
      return $listener->response(400, 'Response has empty field');
    }
    return $listener->response(200, [
      'amount' => intval($response->amount),
      'customer_name' => $response->customer_name,
      'display_text' => $response->display_text,
      'item_name' => $response->item_name,
    ]);
  }

  public function notifyVAPayment(PipelineListener $listener, $data) {
    try {
      $pgUser = $this->getAndValidatePaymentGatewayUser($data['user_id']);
      $this->validateVA($pgUser->channels, $data['va_code'], $data['va_prefix']);
      $pgUserPaymentChannel = $this->getAndValidateUserPaymentChannel($pgUser->id, $data['payment_group_id']);
    } catch (FailException $e) {
      return $listener->response(400, $e->getMessage());
    }

    $notifyAt = Carbon::now();
    $defaultSettlementAt = $this->settlementConfig->getSettlementAt($pgUserPaymentChannel->payment_group_id, $pgUserPaymentChannel->default_settlement_type, $notifyAt);
    if (!$defaultSettlementAt) {
      return $listener->response(400, 'Settlement duration not found');
    }

    $notifyAt = Carbon::now();

    list($settlementFee, $settlementAt) = $this->getSettlementDetail($notifyAt, $defaultSettlementAt, $pgUserPaymentChannel, $data['billed_amount']);

    if (isset($data['payment_gateway_payment_id'])) {
      $paymentGatewayPayment = $this->paymentGatewayPaymentRepo->findById($data['payment_gateway_payment_id']);
    }
    
    if (!isset($paymentGatewayPayment)) {
      $paymentGatewayPayment = $this->paymentGatewayPaymentRepo->getNew();
      $paymentGatewayPayment->pg_user_id = $pgUser->id;
      $paymentGatewayPayment->amount = $data['billed_amount'];
      $paymentGatewayPayment->fee = $pgUserPaymentChannel->fee;
      $paymentGatewayPayment->status = PaymentGateway::WAITING_FOR_ACCEPTANCE;
      $paymentGatewayPayment->payment_group_id = $data['payment_group_id'];
      $paymentGatewayPayment->settlement_fee = $settlementFee;
      $paymentGatewayPayment->settlement_status = Settlement::ON_HOLD;
      $paymentGatewayPayment->settlement_at = $settlementAt;
      $paymentGatewayPayment->notify_at = $notifyAt->toDateTimeString();
      $paymentGatewayPayment->cost = $pgUserPaymentChannel->cost;
      $paymentGatewayPayment->va_code = $data['va_code'];
      $paymentGatewayPayment->vendor_reference_id = $data['vendor_reference_id'];
      $this->paymentGatewayPaymentRepo->save($paymentGatewayPayment);
  
      $payment = $this->paymentRepo->findById($data['payment_id']);
      $payment->source_id = $paymentGatewayPayment->id;
      $this->paymentRepo->save($payment);
    }
    
    $paymentGatewayPayment->status = PaymentGateway::ACCEPTED;

    try {
      $url = $pgUser->notify_url;
      $response = $this->notify(
        $url, 
        $data['user_id'], 
        [
          'billed_amount' => $data['billed_amount'],
          'notify_type' => WebhookNotifyType::VA_PAYMENT,
          'payment_id' => $paymentGatewayPayment->id,
          'va_code' => $data['va_code']
        ],
        $pgUser->id,
        $paymentGatewayPayment->id,
        $data['vendor_reference_id'],
        $pgUserPaymentChannel->id
      );
    } catch (FailException $e) {
      $response = null;
    }

    if (!$response) {
      $paymentGatewayPayment->notify_status = PaymentGateway::SUSPECT_TIMEOUT_PAYMENT_ACCEPTANCE;
    } else if (!isset($response->reference_id) || $response->reference_id == '') {
      $paymentGatewayPayment->notify_status = PaymentGateway::FAILED_PAYMENT_MALFORMED_MERCHANT_RESPONSE;
    } else if ($this->paymentGatewayPaymentRepo->existsByPaymentGatewayUserIdAndExternalReferenceId($pgUser->id, $response->reference_id)) {
      $paymentGatewayPayment->notify_status = PaymentGateway::FAILED_PAYMENT_EXTERNAL_REFERENCE_ID_IS_DUPLICATE;
    } else {
      $paymentGatewayPayment->external_reference_id = $response->reference_id;
      $paymentGatewayPayment->notify_status = PaymentGateway::SUCCESS;
    }

    $this->paymentGatewayPaymentRepo->save($paymentGatewayPayment);

    return $listener->response(200, [
      'pg_payment_id' => $paymentGatewayPayment->id,
      'pg_payment_status' => $paymentGatewayPayment->status
    ]);
  }

  public function notifyPaymentStatus(PipelineListener $listener, $data) {
    $paymentGatewayPayment = $this->paymentGatewayPaymentRepo->findById($data['pg_payment_id']);
    
    $notifyAt = new Carbon($paymentGatewayPayment->notify_at);
    $receiveNotifyTime = new Carbon($data['receive_notify_time']);
    if ($notifyAt >= $receiveNotifyTime) {
      return $listener->response(400, 'Receive deprecated notify');
    }
    
    $paymentGatewayPayment->status = $data['pg_payment_status'];
    $this->paymentGatewayPaymentRepo->save($paymentGatewayPayment);

    if (!isset($paymentGatewayPayment->external_reference_id)) {
      return $listener->response(400, 'Merchant reference id is null');
    }

    $paymentGatewayUser = $this->paymentGatewayUserRepo->findById($paymentGatewayPayment->pg_user_id);

    $pgUserPaymentChannel = $this->getAndValidateUserPaymentChannel($paymentGatewayUser->id, $paymentGatewayPayment->payment_group_id);

    try {
      $url = $paymentGatewayUser->notify_url;
      $response = $this->notify(
        $url, 
        $paymentGatewayUser->user_id, 
        [
          'notify_type' => WebhookNotifyType::PAYMENT_STATUS,
          'status' => PaymentGateway::STATUS_CODE_TO_MERCHANT_CODE[$paymentGatewayPayment->status],
          'reference_id' => $paymentGatewayPayment->external_reference_id,
          'payment_id' => $data['pg_payment_id'],
          'amount' => $paymentGatewayPayment->amount,
          'fee' => $paymentGatewayPayment->fee
        ],
        $paymentGatewayUser->id,
        $paymentGatewayPayment->id,
        $data['vendor_reference_id'],
        $pgUserPaymentChannel->id
      );
    } catch (FailException $e) {
      return $listener->response(400, $e->getMessage());
    }

    if (!$response || !isset($response->success) || !$response->success) {
      return $listener->response(400, 'Invalid response from client');
    }
  }

  private function notify($url, $userId, $payload, $pgUserId, $refId, $vendorRefId, $pgUserChannelId) {
    $timestamp = Carbon::now()->timestamp;

    $stringToSign = $this->generateStringToSign('POST', $url, '', $timestamp, $payload);
    if (!$stringToSign) {
      throw new FailException('Method not found');
    }
    $signature = $this->signatureGenerator->generateSignatureByUserId($userId, $stringToSign);

    $client = new Client();
    $request = new Request('POST', $url,
      [
        'Content-Type' => 'application/json',
        'X-Odeo-Signature' => $signature,
        'X-Odeo-Timestamp' => $timestamp
      ], json_encode($payload));

    try {
      $response = $client->send($request, [
        'timeout' => 20
      ]);
      $responseContentString = $response->getBody()->getContents();
    }
    catch (\GuzzleHttp\Exception\RequestException $e) {
      $errorMessage = $e->getMessage();
      $response = $e->getResponse();
      $responseContentString = '';
    }

    $responseContent = json_decode($responseContentString);

    $requestDump = $this->getDump($request->getHeaders(), json_encode($payload));

    if ($response) {
      $responseDump = $this->getDump($response->getHeaders(), $responseContentString);
    } else {
      $responseDump = '';
    }

    $paymentGatewayNotifyLog = $this->paymentGatewayNotifyLogRepo->getNew();
    $paymentGatewayNotifyLog->pg_user_id = $pgUserId;
    $paymentGatewayNotifyLog->notify_type = $payload['notify_type'];
    $paymentGatewayNotifyLog->request_url = $url;
    $paymentGatewayNotifyLog->request_dump = $requestDump;
    $paymentGatewayNotifyLog->response_dump = $responseDump;
    $paymentGatewayNotifyLog->status_code = $response ? $response->getStatusCode() : '500';
    $paymentGatewayNotifyLog->error_message = $errorMessage ?? null;
    $paymentGatewayNotifyLog->reference_id = $refId;
    $paymentGatewayNotifyLog->vendor_reference_id = empty($vendorRefId) ? NULL : $vendorRefId;
    $paymentGatewayNotifyLog->pg_user_channel_id = $pgUserChannelId;
    $this->paymentGatewayNotifyLogRepo->save($paymentGatewayNotifyLog);

    if ($paymentGatewayNotifyLog->status_code != 200) {
      throw new FailException('Bad Response');
    }

    return $responseContent;
  }

  private function getDump($headers, $stringBody) {
    $dump = '';
    foreach ($headers as $key => $val) {
      $dump = $dump.$key.': '.$val[0]."\n";
    }
    return $dump."\n".$stringBody;
  }

  private function getAndValidatePaymentGatewayUser($userId) {
    $paymentGatewayUser = $this->paymentGatewayUserRepo->findByUserId($userId);
    if (!$paymentGatewayUser) {
      throw new FailException('User not found');
    }
    return $paymentGatewayUser;
  }

  private function validateVA($pgUserChannels, $code, $prefix) {
    if ($code === '') {
      throw new FailException('VA code is empty');
    }
    if ($prefix === '') {
      throw new FailException('Expected prefix is empty');
    }
    if (substr($code, 0, strlen($prefix)) !== $prefix) {
      throw new FailException('VA code and prefix does not match');
    }

    foreach ($pgUserChannels as $pgUserChannel) {
      if ($pgUserChannel->prefix == $prefix) {
        return;
      }
    }
    throw new FailException('Prefix not found for this user');
  }

  private function getAndValidateUserPaymentChannel($paymentGatewayUserId, $paymentGroupId) {
    $paymentGatewayUserPaymentChannel = $this->paymentGatewayUserPaymentChannelRepo
      ->findByActivePaymentGatewayUserIdAndPaymentGroupId($paymentGatewayUserId, $paymentGroupId);
    if (!$paymentGatewayUserPaymentChannel) {
      throw new FailException('Payment channel not found');
    }
    if ($paymentGatewayUserPaymentChannel->active === false) {
      throw new FailException('Payment Channel is not active');
    }
    return $paymentGatewayUserPaymentChannel;
  }

  public function generateStringToSign($method, $url, $accessToken, $timestamp, $payload) {
    $allowedMethod = ['GET', 'PUT', 'POST', 'DELETE'];
    if (!in_array($method, $allowedMethod)) {
      return null;
    }

    $parsedUrl = parse_url($url);

    $bodyHash = base64_encode(hash('sha256', json_encode($payload), true));

    return $method . ':' . 
      (isset($parsedUrl['path']) ? $parsedUrl['path'] : '') . ':' . 
      (isset($parsedUrl['query']) ? $parsedUrl['query'] : '') . ':' . 
      $accessToken . ':' . 
      $timestamp . ':' .
      $bodyHash;
  }

  public function getSettlementDetail($notifyAt, $settlementAt, $pgUserChannel, $paymentAmount) {
    $feePct = $pgUserChannel->settlement_fee;

    $actualSettlementAt = $this->settlementConfig->getSettlementAt($pgUserChannel->payment_group_id, $pgUserChannel->settlement_type, $notifyAt);
    $dateDiff = $settlementAt->diffInDays($actualSettlementAt);

    $settlementFee = $paymentAmount * $dateDiff * $feePct / 100;

    $settlementDayCount = $this->settlementConfig->getConstantSettlementDay($pgUserChannel->payment_group_id);
    
    if ($pgUserChannel->payment_group_id == Payment::OPC_GROUP_VA_BCA) {
      $actualSettlementAt = $this->holidayHelper->addWorkingDays($notifyAt->copy()->subDay(), $settlementDayCount);
      $actualSettlementAt->addDay();
    } else {
      $actualSettlementAt = $this->holidayHelper->addWorkingDays($notifyAt->copy(), $settlementDayCount);
    }
    $actualSettlementAt->subDays($dateDiff);

    return [max(0, round($settlementFee)), $actualSettlementAt];
  }

  public function validateNotify(PipelineListener $listener, $data) {
    if ($data['password'] != env('PG_NOTIFY_PG_PAYMENT_PASSWORD')) {
      return $listener->response(400, trans('errors.invalid_password'));
    }

    if (!in_array($data['payment_gateway_payment_status'], [
      PaymentGateway::SUCCESS,
      PaymentGateway::SUSPECT,
      PaymentGateway::SUSPECT_TIMEOUT_PAYMENT_ACCEPTANCE,
      PaymentGateway::FAILED_PAYMENT_MALFORMED_MERCHANT_RESPONSE,
      PaymentGateway::FAILED_PAYMENT_EXTERNAL_REFERENCE_ID_IS_DUPLICATE
    ])) {
      return $listener->response(400, trans('errors.payment_gateway.error_update'));
    }

    return $listener->response(200);
  }

  public function renotifyPayment(PipelineListener $listener, $data) {
    if ($paymentGatewayPayment = $this->paymentGatewayPaymentRepo->findById($data['pg_payment_id'])) {
      $paymentGatewayUser = $paymentGatewayPayment->pgUser;
      if ($paymentGatewayUser->user_id != $data['auth']['user_id'])
        return $listener->response(400, 'No access');

      if ($this->checkAlreadyRenotified($paymentGatewayPayment->id)) {
        return $listener->response(400, trans('api_users.pg_already_notified'));
      }

      $pgUserPaymentChannel = $this->getAndValidateUserPaymentChannel($paymentGatewayUser->id, $paymentGatewayPayment->payment_group_id);
      if ($paymentGatewayPayment->external_reference_id) {
        try {
          $response = $this->notify(
            $paymentGatewayUser->notify_url,
            $paymentGatewayUser->user_id,
            [
              'notify_type' => WebhookNotifyType::PAYMENT_STATUS,
              'status' => PaymentGateway::STATUS_CODE_TO_MERCHANT_CODE[$paymentGatewayPayment->status],
              'reference_id' => $paymentGatewayPayment->external_reference_id,
              'payment_id' => $data['pg_payment_id'],
              'amount' => $paymentGatewayPayment->amount,
              'fee' => $paymentGatewayPayment->fee
            ],
            $paymentGatewayUser->id,
            $paymentGatewayPayment->id,
            $paymentGatewayPayment->vendor_reference_id,
            $pgUserPaymentChannel->id
          );

          if (!$response || !isset($response->success) || !$response->success) {
            return $listener->response(400, 'Invalid response from client');
          }
        } catch (FailException $e) {
          return $listener->response(400, $e->getMessage());
        }
      }
      else {
        try {
          $response = $this->notify(
            $paymentGatewayUser->notify_url,
            $paymentGatewayUser->user_id,
            [
              'billed_amount' => $paymentGatewayPayment->amount,
              'notify_type' => WebhookNotifyType::VA_PAYMENT,
              'payment_id' => $paymentGatewayPayment->id,
              'va_code' => $paymentGatewayPayment->va_code
            ],
            $paymentGatewayUser->id,
            $paymentGatewayPayment->id,
            $paymentGatewayPayment->vendor_reference_id,
            $pgUserPaymentChannel->id
          );
        } catch (FailException $e) {
          $response = null;
        }

        if (!$response) {
          $paymentGatewayPayment->notify_status = PaymentGateway::SUSPECT_TIMEOUT_PAYMENT_ACCEPTANCE;
        } else if (!isset($response->reference_id) || $response->reference_id == '') {
          $paymentGatewayPayment->notify_status = PaymentGateway::FAILED_PAYMENT_MALFORMED_MERCHANT_RESPONSE;
        } else if ($this->paymentGatewayPaymentRepo->existsByPaymentGatewayUserIdAndExternalReferenceId($paymentGatewayUser->id, $response->reference_id)) {
          $paymentGatewayPayment->notify_status = PaymentGateway::FAILED_PAYMENT_EXTERNAL_REFERENCE_ID_IS_DUPLICATE;
        } else {
          $paymentGatewayPayment->external_reference_id = $response->reference_id;
          $paymentGatewayPayment->notify_status = PaymentGateway::SUCCESS;
        }

        $paymentGatewayPayment->status = PaymentGateway::ACCEPTED;
        $this->paymentGatewayPaymentRepo->save($paymentGatewayPayment);
      }

      $paymentOdeoPaymentChannelInformationRepo = app()->make(PaymentOdeoPaymentChannelInformationRepository::class);
      $paymentOdeoPaymentChannelInformation = $paymentOdeoPaymentChannelInformationRepo->findById($paymentGatewayPayment->payment_group_id);
      return $listener->response(200, [
        'id' => $paymentGatewayPayment->id,
        'payment_method' => $paymentOdeoPaymentChannelInformation->name,
        'amount' => $paymentGatewayPayment->amount,
        'fee' => $paymentGatewayPayment->fee,
        'settlement_fee' => $paymentGatewayPayment->settlement_fee,
        'external_reference_id' => $paymentGatewayPayment->external_reference_id,
        'settlement_at' => $paymentGatewayPayment->settlement_at->format('Y-m-d H:i:s'),
        'va_code' => $paymentGatewayPayment->va_code,
        'created_at' => $paymentGatewayPayment->created_at->format('Y-m-d H:i:s'),
        'merchant_status' => PaymentGateway::getMerchantStatusCode($paymentGatewayPayment->status),
        'merchant_status_message' => PaymentGateway::getMerchantStatusMessage($paymentGatewayPayment->status),
        'settlement_status' => Settlement::getSettlementStatusCode($paymentGatewayPayment->status, $paymentGatewayPayment->settlement_status),
        'settlement_status_message' => Settlement::getSettlementStatusMessage($paymentGatewayPayment->status, $paymentGatewayPayment->settlement_status)
      ]);
    }
    return $listener->response(400, 'ID not exists');
  }

  private function checkAlreadyRenotified($pgId) {
    $redis = Redis::connection();
    $namespace = "odeo_core:pg_renotify_id_$pgId";

    if ($redis->get($namespace)) {
      return true;
    }

    $redis->set($namespace, 1);
    $redis->expire($namespace, 10 * 60);
    return false;
  }

}