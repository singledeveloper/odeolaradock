<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 04/12/18
 * Time: 20.14
 */

namespace Odeo\Domains\PaymentGateway;

use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Carbon\Carbon;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Constant\Settlement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\PaymentGateway\Helper\PaymentGatewayVendor;
use Odeo\Domains\PaymentGateway\Jobs\SendPaymentGatewayInvoice;
use Odeo\Domains\PaymentGateway\Jobs\SendPaymentGatewayReport;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayNotifyLogRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository;
use Odeo\Domains\Transaction\Helper\Currency;

class PaymentGatewayPaymentSelector {

  private $paymentGatewayPaymentRepo;
  private $currencyHelper;

  public function __construct() {
    $this->paymentGatewayPaymentRepo = app()->make(PaymentGatewayPaymentRepository::class);
    $this->pgNotifyRepo = app()->make(PaymentGatewayNotifyLogRepository::class);
    $this->currencyHelper = app()->make(Currency::class);
  }

  public function transforms($item) {
    $item->merchant_status = PaymentGateway::getMerchantStatusCode($item->status);
    $item->merchant_status_message = PaymentGateway::getMerchantStatusMessage($item->status);
    $item->settlement_status = Settlement::getSettlementStatusCode($item->status, $item->settlement_status);
    $item->settlement_status_message = Settlement::getSettlementStatusMessage($item->status, $item->settlement_status);
    //$item->created_at = date('Y-m-d H:i', strtotime($item->created_at));
    return $item;
  }

  public function transformsServices($item) {
    $payment = [];
    $payment['payment_id'] = $item->id;
    $payment['amount'] = $item->amount;
    $payment['fee'] = $item->fee;
    $payment['status'] = $item->status;
    $payment['reference_id'] = $item->external_reference_id;

    return $payment;
  }

  public function getAll(PipelineListener $listener, $data) {
    $this->paymentGatewayPaymentRepo->normalizeFilters($data);
    $this->paymentGatewayPaymentRepo->setSimplePaginate(true);

    $result = [];
    $payments = $this->paymentGatewayPaymentRepo->get();

    foreach ($payments as $item) {
      $temp = $this->transforms($item);
      $result[] = $temp;
    }

    if (!empty($result)) {
      return $listener->response(200, array_merge(
        ['payments' => $result],
        $this->paymentGatewayPaymentRepo->getPagination()
      ));
    }

    return $listener->response(204, ['payments' => []]);
  }

  public function getByUser(PipelineListener $listener, $data) {
    $this->paymentGatewayPaymentRepo->normalizeFilters($data);
    $this->paymentGatewayPaymentRepo->setSimplePaginate(true);

    $result = [];
    $payments = $this->paymentGatewayPaymentRepo->getAllPaymentByUser($data['auth']['user_id']);

    foreach ($payments as $item) {
      $temp = $this->transforms($item);
      unset($temp['status']);
      $result[] = $temp;
    }

    if (!empty($result)) {
      return $listener->response(200, array_merge(
        ['payments' => $result],
        $this->paymentGatewayPaymentRepo->getPagination()
      ));
    }

    return $listener->response(204, ['payments' => []]);
  }

  public function export(PipelineListener $listener, $data) {
    $filters = [
      'start_date' => $data['start_date'],
      'end_date' => $data['end_date'],
      'file_type' => $data['file_type'] ?? 'csv',
      'user_id' => getUserId()
    ];

    $user = app()->make(UserRepository::class)->findById(getUserId());

    if (!$user->email) {
      return $listener->response(400, trans('user.email_not_set'));
    }

    if (!$user->is_email_verified) {
      return $listener->response(400, 'Your email is not verified');
    }

    $exportId = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class)->create($data);

    $listener->pushQueue(new SendPaymentGatewayReport($filters, $user->email, $exportId));

    return $listener->response(200, [
      'export_id' => $exportId
    ]);
  }

  public function exportInvoice(PipelineListener $listener, $data) {

    $user = app()->make(UserRepository::class)->findById(getUserId());

    if (!$user->email) {
      return $listener->response(400, trans('user.email_not_set'));
    }

    if (!$user->is_email_verified) {
      return $listener->response(400, 'Your email is not verified');
    }

    $exportId = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class)->create($data);

    $listener->pushQueue(new SendPaymentGatewayInvoice($user->id, $data['periode'], $exportId));

    return $listener->response(200, [
      'export_id' => $exportId
    ]);
  }

  public function getPayment(PipelineListener $listener, $data) {
    $pgPayment = $this->paymentGatewayPaymentRepo->findByIdAndPaymentGatewayUserId($data['payment_gateway_payment_id'], $data['oauth2']['payment_gateway_user_id']);
    if (!$pgPayment) {  
      return $listener->response(400, trans('payment_gateway.data_not_found'));
    } 

    $response = $this->transformsServices($pgPayment);
    return $listener->response(200, $response);
  }

  public function getPaymentByReferenceId(PipelineListener $listener, $data) {
    $pgPayment = $this->paymentGatewayPaymentRepo->findByPaymentGatewayUserIdAndExternalReferenceId($data['oauth2']['payment_gateway_user_id'], $data['external_reference_id']);
    if (!$pgPayment) {
      return $listener->response(400, trans('payment_gateway.data_not_found'));
    } 

    $response = $this->transformsServices($pgPayment);
    return $listener->response(200, $response);
  }
  
  public function getNotifyLog(PipelineListener $listener, $data) {
    $this->pgNotifyRepo->normalizeFilters($data);
    $this->pgNotifyRepo->setSimplePaginate(true);

    $logs = [];
    foreach( $this->pgNotifyRepo->get() as $log) {
      $logs[] = [
        'id' => $log->id,
        'virtual_account_prefix' => $log->virtual_account_prefix,
        'notify_type' => $log->notify_type,
        'request_dump' => $log->request_dump,
        'response_dump' => $log->response_dump,
        'status_code' => $log->status_code,
        'error_message' => $log->error_message,
        'reference_id' => $log->reference_id,
        'created_at' => $log->created_at
      ];
    }

    if (count($logs) > 0) {
      return $listener->response(200, array_merge(
        ['logs' => $logs],
        $this->paymentGatewayPaymentRepo->getPagination()
      ));
    }
    return $listener->response(200, ['logs' => []]);
  }

  public function getLogByPayment(PipelineListener $listener, $data) {
    $pgp = $this->paymentGatewayPaymentRepo->findById($data['payment_id']);
    if (!isAdmin($data)) {
      if ($pgp) {
        $pgUser = $pgp->pgUser;
        if (!($pgUser && $pgUser->user_id == $data['auth']['user_id'])) {
          return $listener->response(400, 'No access');
        }
      }
      else return $listener->response(400, 'No access');
    }
    $logs = [];
    foreach ($this->pgNotifyRepo->findAllByAttributes('vendor_reference_id', $pgp->vendor_reference_id) as $log) {
      $logs[] = [
        'notify_type' => $log->notify_type,
        'request_dump' => $log->request_dump,
        'response_dump' => $log->response_dump,
        'status_code' => $log->status_code,
        'error_message' => $log->error_message
      ];
    }

    return $listener->response(200, ["logs" => $logs]);
  }

  public function getExported($data) {
    $this->paymentGatewayPaymentRepo->normalizeFilters($data);

    $reports = $this->paymentGatewayPaymentRepo->getReport();

    $writer = WriterFactory::create(Type::CSV);
    $writer->openToBrowser('Payment Gateway Report ' . ($data['start_date'] ?? '0000-00-00') . ' to ' . ($data['end_date'] ?? Carbon::now()->toDateString()) . '.csv');

    $headers = [
      'id',
      'user_id',
      'user_name',
      'payment_method',
      'payment_amount',
      'fee',
      'settlement_amount',
      'merchant_reference_id',
      'status',
      'payment_date',
      'vendor',
      'vendor_reference_id',
    ];

    $writer->addRow($headers);

    foreach ($reports as $row) {
      $temp = [];

      foreach ($headers as $h) {
        if ($h == 'vendor_reference_id') {
          $temp[] = $row[$row['vendor'] . '_reference_id'];
        } else {
          $temp[] = $row[$h];
        }
      }

      $writer->addRow($temp);
    }

    $writer->close();
  }

  public function getPaymentNotifyDetail(PipelineListener $listener, $data) {
    $paymentRepo = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);
    $pgUserPaymentChannelRepo = app()->make(\Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository::class);
    $paymentGatewayPayment = $this->paymentGatewayPaymentRepo->findById($data['payment_gateway_payment_id']);

    if (!$paymentGatewayPayment) {
      return $listener->response(400, 'Payment Id not found');
    }

    $payment = $paymentRepo->findByAttributes([
      'source_type' => PaymentChannel::PAYMENT_GATEWAY,
      'source_id' => $paymentGatewayPayment->id
    ]);

    $extraData = [];

    if (!$paymentDetailRepo = PaymentGatewayVendor::getVendorPaymentRepo($payment->vendor_type)) {
      return $listener->response(400, 'Vendor not found');
    }

    switch ($payment->vendor_type) {
      case 'va_prismalink':
        $paymentDetail = $paymentDetailRepo->findById($payment->vendor_id);
        $pgUserChannel = $pgUserPaymentChannelRepo->findByActivePrefix($paymentDetail->va_code);

        $extraData = [
          'va_code' => $paymentDetail->va_code,
          'vendor_reference_id' => $paymentDetail->trace_number,
        ];
        break;
      case 'va_doku':
        $paymentDetail = $paymentDetailRepo->findById($payment->vendor_id);
        $pgUserChannel = $pgUserPaymentChannelRepo->findByActivePrefix($paymentDetail->payment_code);

        $extraData = [
          'va_code' => $paymentDetail->payment_code,
          'vendor_reference_id' => $paymentDetail->session_id,
        ];
        break;
      case 'va_mandiri':
        $paymentDetail = $paymentDetailRepo->findById($payment->vendor_id);
        $pgUserChannel = $pgUserPaymentChannelRepo->findByActivePrefix($paymentDetail->bill_key_1);

        $extraData = [
          'va_code' => $paymentDetail->bill_key_1,
          'vendor_reference_id' => $paymentDetail->reference_number
        ];
        break;
      case 'va_permata':
        $paymentDetail = $paymentDetailRepo->findById($payment->vendor_id);
        $pgUserChannel = $pgUserPaymentChannelRepo->findByActivePrefix($paymentDetail->va_number);

        $extraData = [
          'va_code' => $paymentDetail->va_number,
          'vendor_reference_id' => $paymentDetail->reference_number
        ];
        break;
      case 'va_cimb':
        $paymentDetail = $paymentDetailRepo->findById($payment->vendor_id);
        $vaCode = $paymentDetail->company_code . $paymentDetail->customer_key_1;
        $pgUserChannel = $pgUserPaymentChannelRepo->findByActivePrefix($vaCode);

        $extraData = [
          'va_code' => $vaCode,
          'vendor_reference_id' => $paymentDetail->reference_number_transaction
        ];
        break;

      case 'va_artajasa':
        $paymentDetail = $paymentDetailRepo->findById($payment->vendor_id);
        $pgUserChannel = $pgUserPaymentChannelRepo->findByActivePrefix($paymentDetail->inquiry->va_id);

        $extraData = [
          'va_code' => $paymentDetail->inquiry->va_id,
          'vendor_reference_id' => $paymentDetail->booking_id
        ];
        break;
      default:
        return $listener->response(400, 'Payment not found');

    }

    return $listener->response(200, array_merge([
      'payment_id' => $payment->id,
      'va_prefix' => $pgUserChannel->prefix,
      'user_id' => $pgUserChannel->user_id,
      'payment_gateway_payment_id' => $paymentGatewayPayment->id,
      'payment_gateway_payment_status' => $paymentGatewayPayment->status,
      'billed_amount' => $paymentGatewayPayment->amount,
      'payment_group_id' => $pgUserChannel->payment_group_id
    ], $extraData));
  }
}
