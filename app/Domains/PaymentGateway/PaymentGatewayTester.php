<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 25/07/19
 * Time: 18.23
 */

namespace Odeo\Domains\PaymentGateway;


use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;

class PaymentGatewayTester {

  function __construct() {
    $this->virtualAccountVendor = app()->make(\Odeo\Domains\VirtualAccount\Repository\VirtualAccountVendorRepository::class);
    $this->odeoPaymentChannel = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);
    $this->payment = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);
    $this->pgUserPaymentChannel =  app()->make(PaymentGatewayUserPaymentChannelRepository::class);
  }

  public function validateVirtualAccount(PipelineListener $listener, $data) {
    $vaNumber = $data['virtual_account_number'];
    $traceNo = $data['trace_no'] ?? '' . time();
    $pgUserChannel = $this->pgUserPaymentChannel->findByActivePrefix($vaNumber);

    if (!$pgUserChannel) {
      return $listener->response('Virtual account vendor not found');
    }

    $channel = $this->odeoPaymentChannel->findByGatewayIdAndGroupId(Payment::PAYMENT_GATEWAY, $pgUserChannel->payment_group_id);
    if (!$channel) {
      return $listener->response(400, 'channel not found');
    }

    return $listener->response(200, [
      'va_code' => $vaNumber,
      'va_prefix' => $pgUserChannel->prefix,
      'user_id' => $pgUserChannel->user_id,
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'vendor_reference_id' => $traceNo,
      'opc_code' => $channel->code
    ]);
  }

  public function createPayment(PipelineListener $listener, $data) {
    $payment = $this->payment->getNew();
    $payment->source_type = PaymentChannel::PAYMENT_GATEWAY;
    $payment->info_id = $data['payment_group_id'];
    $payment->opc = $data['opc_code'];
    $payment->vendor_id = $data['vendor_reference_id'];
    $payment->vendor_type = 'va_test';
    $this->payment->save($payment);
    return $listener->response(200, [
      'payment_id' => $payment->id
    ]);
  }

}