<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 09/07/19
 * Time: 14.00
 */

namespace Odeo\Domains\PaymentGateway;

use Odeo\Domains\Activity\Repository\HolidayRepository;
use Odeo\Domains\Constant\AssetAws;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayChannelRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserRepository;

class PaymentGatewayUserSelector {

  private $pgUserRepo, $pgUserChannel, $pgChannel, $holidayRepo;

  public function __construct() {
    $this->pgUserRepo = app()->make(PaymentGatewayUserRepository::class);
    $this->pgUserChannel = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
    $this->pgChannel = app()->make(PaymentGatewayChannelRepository::class);
    $this->holidayRepo = app()->make(HolidayRepository::class);
  }

  public function getPaymentGatewayUser(PipelineListener $listener, $data) {
    if (!$pgUser = $this->pgUserRepo->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('api_users.unauthorized'));
    }

    $userChannels = $this->pgChannel->getPgChannels($pgUser->id);
    $channels = [];
    foreach ($userChannels as $channel) {
      if (isProduction() && in_array($channel->id, [5,8,9]) && $data['auth']['user_id'] != 19) {
        continue;
      }

      $channels[] = [
        'channel_id' => $channel->id,
        'name' => trans('payment_gateway.channel_' . $channel->channel_name),
        'logo' => AssetAws::getAssetPath(AssetAws::PAYMENT, $channel->logo),
        'fee' => $channel->fee ?? $channel->default_fee,
        'settlement_type' => $channel->settlement_type ?? $channel->default_settlement_type,
        'default_settlement_type' => $channel->default_settlement_type,
        'settlement_fee' => $channel->settlement_fee ?? $channel->default_settlement_fee,
        'prefix' => $channel->prefix ?? '',
        'min_length' => $channel->min_length,
        'max_length' => $channel->max_length,
        'active' => $channel->active ?? false
      ];
    }

    $holidays = [];
    foreach ($this->holidayRepo->getNextMonth(date('Y-m-d'), 2) as $item) {
      $holidays[] = [
        'date' => $item->event_date,
        'description' => trans('holiday.' . $item->description,
          $item->translation_data ? json_decode($item->translation_data, true) : []),
        'settlement_increment' => $item->settlement_increment
      ];
    }

    return $listener->response(200, [
      'notify_url' => $pgUser->notify_url,
      'email_report' => $pgUser->email_report,
      'channels' => $channels,
      'holidays' => $holidays
    ]);
  }

}