<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 26/07/19
 * Time: 19.17
 */

namespace Odeo\Domains\PaymentGateway\Repository;


use Odeo\Domains\Constant\ForBusiness;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\PaymentGateway\Model\PaymentGatewayChannel;

class PaymentGatewayChannelRepository extends Repository {

  function __construct(PaymentGatewayChannel $channel) {
    $this->model = $channel;
  }

  public function getActivePaymentGroups() {
    return $this->getCloneModel()
      ->join('virtual_account_vendors', 'virtual_account_vendors.id', '=', 'payment_gateway_channels.va_vendor_id')
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'virtual_account_vendors.payment_group_id')
      ->select('virtual_account_vendors.payment_group_id', 'payment_odeo_payment_channel_informations.name')->distinct()
      ->orderBy('payment_odeo_payment_channel_informations.name', 'asc')->get();
  }

  public function getPgChannels($pgUserId) {
    return $this->model
      ->leftJoin('virtual_account_vendors', 'virtual_account_vendors.id', '=', 'payment_gateway_channels.va_vendor_id')
      ->leftJoin('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'virtual_account_vendors.payment_group_id')
      ->leftJoin('payment_gateway_user_payment_channels', function($join) use ($pgUserId) {
        $join->on('payment_gateway_user_payment_channels.payment_gateway_channel_id', '=', 'payment_gateway_channels.id')
          ->where('payment_gateway_user_payment_channels.pg_user_id', $pgUserId);
      })
      ->select(
        'payment_gateway_channels.id', 
        'payment_gateway_channels.channel_name', 
        'payment_gateway_channels.default_fee', 
        'payment_gateway_channels.default_settlement_type', 
        'payment_gateway_channels.default_settlement_fee',
        'payment_odeo_payment_channel_informations.logo', 
        'payment_gateway_user_payment_channels.prefix', 
        'payment_gateway_user_payment_channels.fee', 
        'payment_gateway_user_payment_channels.settlement_type',
        'virtual_account_vendors.min_length',
        'virtual_account_vendors.max_length',
        'active')
      ->orderBy('payment_gateway_channels.id', 'asc')
      ->get();
  }

  public function getPgInvoiceChannels($businessInvoiceUserId) {
    return $this->model
      ->leftJoin('virtual_account_vendors', 'virtual_account_vendors.id', '=', 'payment_gateway_channels.va_vendor_id')
      ->leftJoin('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'virtual_account_vendors.payment_group_id')
      ->leftJoin('business_invoice_user_settings', function($join) use ($businessInvoiceUserId) {
        $join->on('business_invoice_user_settings.payment_gateway_channel_id', '=', 'payment_gateway_channels.id')
          ->where('business_invoice_user_settings.business_invoice_user_id', '=',  $businessInvoiceUserId);
      })->select(
        'payment_gateway_channels.id',
        'payment_gateway_channels.channel_name',
        'payment_gateway_channels.default_fee',
        'payment_gateway_channels.default_settlement_type',
        'payment_gateway_channels.default_settlement_fee',
        'payment_odeo_payment_channel_informations.logo',
        'business_invoice_user_settings.fee',
        'business_invoice_user_settings.settlement_type',
        'business_invoice_user_settings.settlement_fee',
        'active')
      ->orderBy('payment_gateway_channels.id', 'asc')
      ->get();
  }

  public function getLastTenDaysPaymentGatewayUsages() {
    return $this->getCloneModel()
      ->join('virtual_account_vendors', 'payment_gateway_channels.va_vendor_id', '=', 'virtual_account_vendors.id')
      ->join('payment_odeo_payment_channel_informations', 'virtual_account_vendors.payment_group_id', '=', 'payment_odeo_payment_channel_informations.id')
      ->leftJoin('business_reseller_transactions', 'business_reseller_transactions.payment_group_id', '=', 'virtual_account_vendors.payment_group_id')
      ->where(function($query){
        $query->where('business_reseller_transactions.type', ForBusiness::TRANSACTION_PAYMENT_GATEWAY)
          ->whereRaw("cast(business_reseller_transactions.transaction_date as date) >= date_trunc('day', CURRENT_TIMESTAMP - interval '10 days')")
          ->orWhereNull('business_reseller_transactions.type');
      })->where('virtual_account_vendors.is_active', true)
      ->groupBy('payment_odeo_payment_channel_informations.id', 'payment_odeo_payment_channel_informations.name')
      ->orderByRaw('sum(business_reseller_transactions.total_amount) desc nulls last')
      ->select(
        'payment_odeo_payment_channel_informations.id',
        'payment_odeo_payment_channel_informations.name',
        \DB::raw('sum(business_reseller_transactions.total_amount) as amount'),
        \DB::raw('sum(business_reseller_transactions.qty) as qty')
      )->get();
  }

  public function findByVaVendorId($vendorId) {
    return $this->getCloneModel()
      ->where('va_vendor_id', $vendorId)
      ->first();
  }

  public function findByVaCode($vaCode) {
    return $this->model->join('virtual_account_vendors', 'payment_gateway_channels.va_vendor_id', '=', 'virtual_account_vendors.id')
      ->whereRaw("left('$vaCode', length(virtual_account_vendors.prefix)) = virtual_account_vendors.prefix")
      ->first();
  }

}