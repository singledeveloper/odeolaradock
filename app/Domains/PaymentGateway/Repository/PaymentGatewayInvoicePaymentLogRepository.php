<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 23/10/19
 * Time: 13.08
 */

namespace Odeo\Domains\PaymentGateway\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\PaymentGateway\Model\PaymentGatewayInvoicePaymentLog;

class PaymentGatewayInvoicePaymentLogRepository extends Repository {

  function __construct(PaymentGatewayInvoicePaymentLog $invoicePaymentLog) {
    $this->model = $invoicePaymentLog;
  }

}