<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 17/10/19
 * Time: 18.37
 */

namespace Odeo\Domains\PaymentGateway\Repository;


use Carbon\Carbon;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Settlement;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\PaymentGateway\Model\PaymentGatewayInvoicePayment;

class PaymentGatewayInvoicePaymentRepository extends Repository {

  function __construct(PaymentGatewayInvoicePayment $invoicePayment) {
    $this->model = $invoicePayment;
  }

  function findByVaNo($vendor, $vaNo) {
    return $this->getCloneModel()
      ->where('vendor', $vendor)
      ->where('virtual_account_number', $vaNo)
      ->orderBy('id', 'desc')
      ->first();
  }

  function findByVaNoAndReference($vendor, $vaNo, $reff) {
    return $this->getCloneModel()
      ->where('vendor', $vendor)
      ->where('virtual_account_number', $vaNo)
      ->where('reference', $reff)
      ->first();
  }

  function findByReferenceAndAmount($vendor, $reference, $amount) {
    return $this->getCloneModel()
      ->where('vendor', $vendor)
      ->where('reference', $reference)
      ->where('amount', $amount)
      ->whereDate('created_at', Carbon::now()->format('Y-m-d'))
      ->first();
  }

  function getUnsettled($vendor, $prefix = '') {
    $query = $this->getCloneModel()
      ->where('vendor', $vendor)
      ->whereNotNull('paid_at')
      ->where('is_reconciled', false)
      ->orderBy('paid_at');

    if (!empty($prefix)) $query = $query->where('virtual_account_number', 'LIKE', $prefix . '%');

    return $query->get();
  }

  public function getUnsettledPayment() {
    return $this->model
      ->join('orders', 'orders.id', '=', 'payment_gateway_invoice_payments.order_id')
      ->whereNotNull('orders.paid_at')
      ->where('payment_gateway_invoice_payments.settlement_at', '<=', Carbon::now())
      ->where('settlement_status', Settlement::ON_HOLD)
      ->where('orders.status', OrderStatus::COMPLETED)
      ->select('payment_gateway_invoice_payments.*')
      ->get();
  }

  public function getPaidTransactionCountByVendor($vendor, $startDate, $endDate) {
    $q = $this
      ->getCloneModel()
      ->where('is_reconciled', true)
      ->where('vendor', $vendor)
      ->whereNotNull('paid_at');
    if ($startDate != '') $q = $q->whereDate('created_at', '>=', $startDate);
    if ($endDate != '') $q = $q->whereDate('created_at', '<=', $endDate);
    return $q->count();
  }
}