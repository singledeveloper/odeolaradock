<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 05/07/19
 * Time: 18.22
 */

namespace Odeo\Domains\PaymentGateway\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\PaymentGateway\Model\PaymentGatewayNotifyLog;

class PaymentGatewayNotifyLogRepository extends Repository {

  function __construct(PaymentGatewayNotifyLog $notifyLog) {
    $this->model = $notifyLog;
  }

  public function get() {
    $filters = $this->getFilters();

    $query = $this->model
      ->join('payment_gateway_user_payment_channels', 'payment_gateway_user_payment_channels.id', '=', 'payment_gateway_notify_logs.pg_user_channel_id')
      ->select('payment_gateway_notify_logs.id',
        'payment_gateway_user_payment_channels.prefix as virtual_account_prefix',
        'notify_type',
        'request_dump',
        'response_dump',
        'status_code',
        'error_message',
        'reference_id',
        'payment_gateway_notify_logs.created_at');

    if (isset($filters['search']) && $search = $filters['search']) {
      if (isset($search['id']) && $search['id'] != '') {
        $query = $query->where('payment_gateway_notify_logs.id', $search['id']);
      }
      if (isset($search['va_prefix']) && $search['va_prefix'] != '') {
        $query = $query->where('payment_gateway_user_payment_channels.prefix', 'ilike','%' . $search['va_prefix'] . '%');
      }
      if (isset($search['request_dump']) && $search['request_dump'] != '') {
        $query = $query->where('request_dump', 'ilike', '%' . $search['request_dump'] . '%');
      }
      if (isset($search['reff_id']) && $search['reff_id'] != '') {
        $query = $query->where('reference_id', $search['reff_id']);
      }
      if (isset($search['notify_type']) && $search['notify_type'] != '') {
        $query = $query->where('notify_type', $search['notify_type']);
      }
    }

    return $this->getResult( $query->orderBy('id', 'desc'));
  }

  public function getByPaymentGatewayPaymentId($pgPaymentId) {
    return $this->model
      ->where('reference_id', $pgPaymentId)
      ->orderBy('id', 'desc')
      ->get();
  }

}