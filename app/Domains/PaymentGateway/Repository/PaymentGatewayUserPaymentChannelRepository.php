<?php

namespace Odeo\Domains\PaymentGateway\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\PaymentGateway\Model\PaymentGatewayUserPaymentChannel;

class PaymentGatewayUserPaymentChannelRepository extends Repository {

  public function __construct(PaymentGatewayUserPaymentChannel $paymentGatewayUserPaymentChannel) {
    $this->model = $paymentGatewayUserPaymentChannel;
  }

  public function findByActivePaymentGatewayUserIdAndPaymentGroupId($paymentGatewayUserId, $paymentGroupId) {
    return $this->model
      ->join('payment_gateway_channels', 'payment_gateway_user_payment_channels.payment_gateway_channel_id', '=', 'payment_gateway_channels.id')
      ->join('virtual_account_vendors', 'payment_gateway_channels.va_vendor_id', '=', 'virtual_account_vendors.id')
      ->where('pg_user_id', $paymentGatewayUserId)
      ->where('payment_group_id', $paymentGroupId)
      ->where('active', true)
      ->select('payment_gateway_user_payment_channels.*', 
        'payment_gateway_channels.*', 
        'virtual_account_vendors.*', 
        'payment_gateway_user_payment_channels.id',
        'payment_gateway_user_payment_channels.fee')
      ->first();
  }

  public function findByPgUserId($pgUserId) {
    return $this->model->where('pg_user_id', $pgUserId)->first();
  }

  public function findByPgUserIdAndPgChannelId($pgUserId, $pgChannelId) {
    return $this->model->where('pg_user_id', $pgUserId)
      ->where('payment_gateway_channel_id', $pgChannelId)
      ->first();
  }

  public function findByActivePrefix($vaCode) {
    return $this->model
      ->join('payment_gateway_users', 'payment_gateway_user_payment_channels.pg_user_id', 'payment_gateway_users.id')
      ->join('payment_gateway_channels', 'payment_gateway_user_payment_channels.payment_gateway_channel_id', '=', 'payment_gateway_channels.id')
      ->join('virtual_account_vendors', 'payment_gateway_channels.va_vendor_id', 'virtual_account_vendors.id')
      ->where('active', true)
      ->whereRaw("left('$vaCode', length(payment_gateway_user_payment_channels.prefix)) = payment_gateway_user_payment_channels.prefix")
      ->select('payment_gateway_users.user_id', 'payment_gateway_user_payment_channels.prefix', 'payment_group_id', 'va_vendor_id', 'virtual_account_vendors.max_length', 'virtual_account_vendors.cost')
      ->first();
  }

  public function findLatestPrefix($prefix) {
    return $this->model->where('prefix', 'like', "$prefix%")
      ->orderBy('prefix', 'DESC')
      ->first();
  }

  public function getActiveChannels() {
    return $this->model->where('active', true)
      ->get();
  }

}