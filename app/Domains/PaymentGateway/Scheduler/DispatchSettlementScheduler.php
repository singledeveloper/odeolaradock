<?php

namespace Odeo\Domains\PaymentGateway\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\PaymentGateway\Jobs\SettlePayment;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository;

class DispatchSettlementScheduler {

  private $paymentGatewayPaymentRepo;

  public function __construct() {
    $this->paymentGatewayPaymentRepo = app()->make(PaymentGatewayPaymentRepository::class);
  }

  public function run() {
    $unsettledPayments = $this->paymentGatewayPaymentRepo->getUnsettledPayment();
    
    foreach ($unsettledPayments as $payment) {
      dispatch(new SettlePayment($payment->id));
    }
  }

}
