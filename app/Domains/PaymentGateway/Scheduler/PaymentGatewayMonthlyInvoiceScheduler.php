<?php

namespace Odeo\Domains\PaymentGateway\Scheduler;

use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\SendDisbursementInvoice;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Domains\PaymentGateway\Jobs\SendPaymentGatewayInvoice;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserRepository;

class PaymentGatewayMonthlyInvoiceScheduler {

  private $paymentGatewayUserRepo;

  public function __construct() {
    $this->paymentGatewayUserRepo = app()->make(PaymentGatewayUserRepository::class);
  }

  public function run() {
    $pgUser = $this->paymentGatewayUserRepo->getAll();
    $period = date("Y-m", time() - 15 * 24 * 60 * 60);

    foreach ($pgUser as $user) {
      dispatch(new SendPaymentGatewayInvoice($user->user_id, $period));
    }
  }
}
