<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2018-12-19
 * Time: 17:29
 */

namespace Odeo\Domains\PaymentGateway\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\PaymentGateway\Jobs\SendPaymentGatewayUsageReport;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserRepository;

class PaymentGatewayUsageScheduler {

  private $paymentGatewayUserRepo;

  public function __construct() {
    $this->paymentGatewayUserRepo = app()->make(PaymentGatewayUserRepository::class);
  }

  public function run() {
    $pgUsers = $this->paymentGatewayUserRepo->getAll();
    $period = Carbon::now()->subDay(1);

    foreach ($pgUsers as $user) {
      if (!$user->email_report) {
        continue;
      }

      dispatch(new SendPaymentGatewayUsageReport([
        'user_id' => $user->user_id,
        'file_type' => 'csv',
        'start_date' => $period->toDateString(),
        'end_date' => $period->toDateString(),
      ], explode(';', $user->email_report)));
    }

  }

}
