<?php

namespace Odeo\Domains\Reminder;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Reminder\Helper\BillReminderHelper;
use Odeo\Domains\Reminder\Repository\BillReminderRepository;

class BillReminderCreator {

  private $billReminders;
  private $billReminderHelper;

  public function __construct() {
    $this->billReminders = app()->make(BillReminderRepository::class);
    $this->billReminderHelper = app()->make(BillReminderHelper::class);
  }

  public function create(PipelineListener $listener, $data) {
    $billReminder = $this->billReminders->getNew();
    $billReminder->name = $data['name'];
    $billReminder->pulsa_odeo_id = $data['postpaid_type'];
    $billReminder->user_id = $data['auth']['user_id'];
    $billReminder->number = purifyTelephone($data['customer_number']);
    $billReminder->date = $data['reminder_date'];
    $billReminder->expired_at = $data['expired_date'] ?? null;
    $billReminder->next_reminder = $this->billReminderHelper->getNextReminder($data['reminder_date']);
    $this->billReminders->save($billReminder);

    return $listener->response(201);
  }

}
