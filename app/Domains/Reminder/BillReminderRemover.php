<?php

namespace Odeo\Domains\Reminder;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Reminder\Repository\BillReminderRepository;

class BillReminderRemover {

  private $billReminders;

  public function __construct() {
    $this->billReminders = app()->make(BillReminderRepository::class);
  }

  public function remove(PipelineListener $listener, $data) {
    $this->billReminders->deleteById($data['bill_reminder_id']);

    return $listener->response(201);
  }

}
