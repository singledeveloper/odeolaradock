<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 10/3/17
 * Time: 16:40
 */

namespace Odeo\Domains\Subscription\DistributionChannel;

use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Core\PipelineListener;

class ChannelSelector {

  private $distChannelRepos;

  public function __construct() {
    $this->storeRepos = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->distChannelRepos = app()->make(\Odeo\Domains\Subscription\DistributionChannel\Repository\StoreDistributionChannelRepository::class);
  }

  public function get(PipelineListener $listener, $data) {

    $store = $this->storeRepos->findById($data['store_id']);

    $channel = $this->distChannelRepos->findByAttributes('store_id', $data['store_id']);

    $content = [];

    if ($store->plan_id != Plan::FREE) {
      $content[] = [
        'key' => 'marketplace',
        'name' => 'Market Place',
        'active' => $channel->marketplace,
      ];
    }

    return $listener->response(200, [
      'id' => $channel->id,
      'content' => array_merge($content, [
        [
          'key' => 'webstore',
          'name' => 'Web Store',
          'active' => $channel->webstore,
        ]
      ])
    ]);

  }

}