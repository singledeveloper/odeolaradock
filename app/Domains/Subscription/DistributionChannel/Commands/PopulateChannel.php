<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/1/16
 * Time: 6:16 PM
 */

namespace Odeo\Domains\Subscription\DistributionChannel\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Subscription\DistributionChannel\Repository\StoreDistributionChannelRepository;

class PopulateChannel extends Command {

  protected $signature = 'dist-channel:populate';

  protected $description = 'populate distribution channel';

  private $storeRepos, $distChannelRepos;

  public function __construct() {
    parent::__construct();
    $this->storeRepos = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->distChannelRepos = app()->make(StoreDistributionChannelRepository::class);
  }

  public function handle() {

    $stores = $this->storeRepos->getModel()
      ->whereIn('status', StoreStatus::ACTIVE)
      ->get();


    $dataToBeInsert = [];
    $now = Carbon::now();

    foreach ($stores as $store) {
      $dataToBeInsert[] = [
        'store_id' => $store->id,
        'marketplace' => $store->plan_id != Plan::FREE,
        'webstore' => true,
        'created_at' => $now,
        'updated_at' => $now
      ];
    }

    $this->distChannelRepos->saveBulk($dataToBeInsert);

  }
}
