<?php

namespace Odeo\Domains\Subscription\Helper;

class PlanManager {
  
  public function __construct() {
    $this->plan = app()->make(\Odeo\Domains\Subscription\Repository\PlanRepository::class);
  }
  
  function getPlans($planIds = []) {
    if (sizeof($planIds) == 0) $plans = $this->plan->getAll();
    
    $arryplans = [];
    
    foreach ($plans as $item) {
      $arryplans[$item->id] = $item;
    }
    
    return $arryplans;
  }
  
  function getSuggestionPotential($achievement_value, $max_potential, $achievement_pairs = 0, $max_pairs = 0, $rules = []) {
    if (sizeof($rules) == 0)
      $rules = $this->getPlans();
    
    $base = [
      "is_needed" => true,
      "plan_id" => 0,
      "plan_name" => "",
      "cost_per_month" => ""
    ];
    
    if ((($achievement_value <= $max_potential && $max_potential != "0") && ($achievement_pairs == 0 || $max_pairs == 0)) || (($achievement_pairs != 0 && $max_pairs != 0) && $achievement_pairs <= $max_pairs)) {
      $base = ["is_needed" => false];
    }
    else if ($achievement_pairs != 0 && $max_pairs != 0) {
      $temp_pair = 0;
      foreach ($rules as $key => $item) {
        if (($temp_pair > $item->max_pairing || $temp_pair == "0") && ($item->max_pairing > $achievement_pairs || $achievement_pairs == "0")) {
          $temp_pair = $item->max_pairing;
          $base["plan_id"] = $key;
          $base["plan_name"] = $item->name;
          $base["cost_per_month"] = "$" . ($item->sales_volume * 10);
        }
      }
    }
    else {
      $temp_potential = 0;
      foreach ($rules as $key => $item) {
        if (($temp_potential > $item->max_pairing_potential || $temp_potential == "0") && ($item->max_pairing_potential > $achievement_value || $achievement_value == "0")) {
          $temp_potential = $item->max_pairing_potential;
          $base["plan_id"] = $key;
          $base["plan_name"] = $item->name;
          $base["cost_per_month"] = "$" . ($item->sales_volume * 10);
        }
      }
    }
    
    if (isset($base["plan_name"]) && $base["plan_name"] == "") {
      $base = ["is_needed" => false];
    }
    
    return $base;
  }
}
