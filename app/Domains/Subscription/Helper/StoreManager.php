<?php

namespace Odeo\Domains\Subscription\Helper;

class StoreManager {

  public function __construct() {
    $this->userStore = app()->make(\Odeo\Domains\Subscription\Repository\UserStoreRepository::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->storeNetwork = app()->make(\Odeo\Domains\Network\Repository\TreeRepository::class);
    $this->service = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->selector = app()->make(\Odeo\Domains\Subscription\StoreSelector::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  function getOwner($storeIds) {
    $stores = $this->userStore->findOwner($storeIds);
    $storeNetworks = $this->storeNetwork->findByStoreIds($storeIds)->keyBy('store_id');
    $parentNetworks = $this->storeNetwork->findByStoreIds($storeNetworks->pluck('referred_store_id')->toArray())->keyBy('store_id');

    $owners = [];
    foreach ($stores as $item) {
      $mentor = $storeNetworks[$item->store_id]->referredStore;
      $leader = isset($mentor) ? $parentNetworks[$mentor->id]->referredStore : NULL;

      $owners[$item->store_id] = [
        "id" => $item->user->id,
        "name" => $item->user->name,
        "mentor" => isset($mentor) ? $mentor->name : '',
        "leader" => isset($leader) ? $leader->name : '',
        "email" => $item->user->email,
        "phone_number" => $item->user->telephone
      ];
    }

    return $owners;
  }

  function getStoreInventories($storeIds) {
    $this->store->addFilter("fields", ["id", "plan_id", "current_data"]);
    $storeTemp = $this->store->findByStoreIds($storeIds);
    $stores = [];
    $this->store->addFilter('expand', 'current_data');
    foreach ($storeTemp as $item) {
      $stores[] = $this->selector->_transforms($item, $this->store);
    }

    $services = $this->service->getDetails();

    $servicesTemp = [];
    foreach ($services as $item) {
      if (!isset($servicesTemp[$item->service->name])) $servicesTemp[$item->service->name] = [];
      $servicesTemp[$item->service->name][] = $item;
    }

    $storeInventories = [];
    foreach ($stores as $item) {
      foreach ($item["current_data"] as $serviceName => $serviceDetailId) {
        $inventories = [];
        if (app()->environment() == 'production' && in_array($serviceName, ['pulsa_postpaid', 'pln_postpaid', 'credit_bill'])) continue;
        if ($serviceDetailId != "0") {
          $inventory = [];
          foreach ($servicesTemp[$serviceName] as $output) {
            $inventory_detail = [];

            //$inventory_detail["provider_id"] = $output->vendor->id;
            $inventory_detail["service_detail_id"] = $output->id;
            $inventory_detail["provider_name"] = $output->vendor->name;
            $inventory_detail["provider_image_icon"] = "";
            if (file_exists(public_path('images/icon_provider_' . $output->vendor->id . '.png'))) {
              $inventory_detail["provider_image_icon"] = baseUrl('images/icon_provider_' . $output->vendor->id . '.png');
            }

            $inventory_detail["margin_multipliers"] = $output->marginMultipliers->pluck('multiplier','margin')->toArray();
            $inventory_detail["is_currently_used"] = $output->id == $serviceDetailId;

            $inventory_detail["markup_type"] = $output->margin_type;

            $margin["min"] = abs($output->margin) / 8;
            $margin["max"] = abs($output->margin);
            $margin["multiplier"] = (abs($output->margin) > 0 ? 8 : 0);
            $inventory_detail["provider_margin"] = $margin;
            $inventory[] = $inventory_detail;
          }
          $inventories['providers'] = $inventory;
        }
        $inventories['type'] = str_replace("_", " ", $serviceName);
        $inventories['service_type'] = $serviceName;
        $storeInventories[$item["store_id"]][] = $inventories;
      }
    }

    return $storeInventories;
  }
}
