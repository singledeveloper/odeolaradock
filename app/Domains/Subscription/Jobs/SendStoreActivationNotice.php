<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 5:23 PM
 */

namespace Odeo\Domains\Subscription\Jobs;


use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Jobs\Job;

class SendStoreActivationNotice extends Job {

  use SerializesModels;

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    if (app()->environment() != 'production') {
      return;
    }

    $storeParser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);

    $this->data['store']['store_domain'] = $storeParser->domainName((object) $this->data['store']);

    $status = $this->data['status_before'];
    $subject = 'New Store Activated - ' . time();
    $body = 'A new store has been activated';
    if(isset($status)) {
      if ($status == StoreStatus::WAIT_FOR_UPGRADE_VERIFY) {
        $subject = 'Store Upgraded - ' . time();
        $body = 'A store has been upgraded';
      } else if ($status == StoreStatus::WAIT_FOR_RENEWAL_VERIFY || $status == StoreStatus::WAIT_FOR_EXP_RENEWAL_VERIFY) {
        $subject = 'Store Renewed - ' . time();
        $body = 'A store has been renewed';
      }
    }

    Mail::send('emails.store_activation_notice', [
     'data' => $this->data,
     'body' => $body
   ], function ($m) use ($subject) {

      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('report@odeo.co.id')->subject($subject);

    });


  }

}
