<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/30/17
 * Time: 17:27
 */

namespace Odeo\Domains\Subscription\ManageInventory\Jobs;

use Odeo\Domains\Subscription\ManageInventory\Scheduler\StoreInventorySyncInventory;
use Odeo\Jobs\Job;

class ManualSyncInventory extends Job {

  public function handle() {

    (new StoreInventorySyncInventory)->run();

  }

}
