<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/30/17
 * Time: 17:27
 */

namespace Odeo\Domains\Subscription\ManageInventory\Jobs;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Jobs\Job;

class PopulateStoreInventory extends Job {

  private $data;
  private $storeInventories, $storeInventoryDetails, $pulsaOdeos, $currencyHelper,
    $serviceDetails, $stores, $storeParser;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->storeInventoryDetails = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryDetailRepository::class);
    $this->pulsaOdeos = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->storeParser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);

  }

  public function handle() {

    $data = $this->data;

    $redis = Redis::connection();

    $redisNamespace = 'odeo_core:populate_store_inventory_lock';

    //if ($redis->hsetnx($redisNamespace, $data['store_id'], 1) === 0) return;

    $store = $this->stores->findById($data['store_id']);

    $usedServiceDetailIds = $this->storeInventories->getModel()
      ->where('store_id', $store->id)
      ->select('service_detail_id')
      ->get()->pluck('service_detail_id')->all();

    $storeAvailableService = json_decode($this->storeParser->currentData($store->plan_id, $store->current_data, true), true);
    $storeInventoryDataToBeInsert = [];
    $storeInventoryData = [];
    $nowDateString = Carbon::now()->toDateTimeString();

    foreach ($storeAvailableService as $key => $val) {

      if ($val === 0) continue;

      if (!empty($usedServiceDetailIds) && in_array($val, $usedServiceDetailIds)) {
        continue;
      }

      $temp = [
        'store_id' => $data['store_id'],
        'service_id' => ServiceDetail::getServiceId($val),
        'service_detail_id' => $val,
      ];


      $temp['created_at'] = "'" . $nowDateString . "'";
      $temp['updated_at'] = "'" . $nowDateString . "'";

      $storeInventoryDataToBeInsert[] = '(' . implode(',', $temp) . ')';
    }


    try {

      if (!empty($storeInventoryDataToBeInsert)) {

        \DB::beginTransaction();


        \DB::select(\DB::raw('
          INSERT INTO store_inventories (store_id, service_id, service_detail_id, created_at, updated_at) 
          VALUES ' . implode(',', $storeInventoryDataToBeInsert)));
      }


      if ($store->plan_id != Plan::FREE) {

        $storeInventoryIds = [];

        $unPopulatedStoreInventories = $this->storeInventories->getModel()
          ->leftJoin('store_inventory_details', 'store_inventories.id', '=', 'store_inventory_details.store_inventory_id')
          ->where('store_id', $data['store_id'])
          ->select(
            'store_inventories.id',
            'service_detail_id',
            \DB::raw('count(store_inventory_details.id) as inventories')
          )->groupBy('store_inventories.id', 'service_detail_id')
          ->get()->keyBy('service_detail_id');


        foreach ($unPopulatedStoreInventories as $key => $d) {

          if ($d->inventories > 0) {
            continue;
          }

          $storeInventoryData[$key]['store_inventory_id'] = $d->id;
          $storeInventoryIds[] = $d->id;
        }

        $inventories = $this->pulsaOdeos->getModel()
          ->where('pulsa_odeos.status', Pulsa::STATUS_OK)
          ->whereNull('pulsa_odeos.owner_store_id')
          ->whereIn('service_detail_id', array_keys($storeInventoryData))
          ->get();

        $storeInventoryDetailTobeInsert = [];

        foreach ($inventories as $inv) {
          $storeInventoryDetailTobeInsert[] = [
            'store_inventory_id' => $storeInventoryData[$inv->service_detail_id]['store_inventory_id'],
            'inventory_id' => $inv->id,
            'created_at' => $nowDateString,
            'updated_at' => $nowDateString
          ];
        }

        !empty($storeInventoryDataToBeInsert) && $this->storeInventoryDetails->saveBulk($storeInventoryDetailTobeInsert);

        !empty($storeInventoryIds) && dispatch(new PopulateStoreInventoryPricing([
          'store_id' => $data['store_id'],
          'store_plan_id' => $store->plan_id
        ]));

      }

      \DB::commit();


    } catch (\Exception $exception) {

      \DB::rollback();
      throw $exception;

    } finally {

      $redis->hdel($redisNamespace, $data['store_id']);

    }


  }

}
