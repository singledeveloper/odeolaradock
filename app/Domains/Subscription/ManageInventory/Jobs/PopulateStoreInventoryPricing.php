<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/3/17
 * Time: 17:18
 */

namespace Odeo\Domains\Subscription\ManageInventory\Jobs;

use Carbon\Carbon;
use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Jobs\Job;

class PopulateStoreInventoryPricing extends Job {

  private $data;

  private $pulsaOdeos, $storeInventories, $storeInventoryGroupPrices,
    $storeInventoryGroupPriceDetails, $marginFormatter, $serviceDetail;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;

    $this->pulsaOdeos = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->storeInventoryGroupPrices = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository::class);
    $this->storeInventoryGroupPriceDetails = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceDetailRepository::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->serviceDetail = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
  }

  public function handle() {
    $now = Carbon::now()->toDateTimeString();
    $data = $this->data;

    $inventoryGroupPrices = $this->storeInventories->getModel()
      ->leftJoin('store_inventory_group_prices', 'store_inventory_group_prices.store_inventory_id', '=', 'store_inventories.id')
      ->where('store_inventories.store_id', $data['store_id'])
      ->select(
        'store_inventory_group_prices.type as type',
        'store_inventories.id as store_inventory_id',
        'service_detail_id'
      );

    if (isset($data['store_inventory_ids'])) {
      $inventoryGroupPrices->whereIn('store_inventories.id', is_array($data['store_inventory_ids']) ? $data['store_inventory_ids'] : [$data['store_inventory_ids']]);
    }

    $inventoryGroupPrices = $inventoryGroupPrices->get();

    $storeWithGroupPriceType = [];

    foreach ($inventoryGroupPrices as $invGP) {
      if (!isset($storeWithGroupPriceType[$invGP->store_inventory_id])) {

        if ($data['store_plan_id'] != Plan::FREE) {
          $storeWithGroupPriceType[$invGP->store_inventory_id][InventoryGroupPrice::MARKET_PLACE] = InventoryGroupPrice::MARKET_PLACE;
        }

        $storeWithGroupPriceType[$invGP->store_inventory_id][InventoryGroupPrice::AGENT_DEFAULT] = InventoryGroupPrice::AGENT_DEFAULT;
        $storeWithGroupPriceType[$invGP->store_inventory_id][InventoryGroupPrice::SELF] = InventoryGroupPrice::SELF;
      }

      if ($invGP->type === null) continue;

      if (isset($storeWithGroupPriceType[$invGP->store_inventory_id][$invGP->type])) {
        unset($storeWithGroupPriceType[$invGP->store_inventory_id][$invGP->type]);
      }
    }

    $inventoryGroupPriceTobeInsert = [];

    foreach ($storeWithGroupPriceType as $storeInvId => $types) {
      foreach ($types as $type) {
        $temp = [
          'store_inventory_id' => $storeInvId,
          'type' => "'" . $type . "'",
          'keep_profit_consistent' => $type != InventoryGroupPrice::MARKET_PLACE ? 'true' : 'false',
          'created_at' => "'" . $now . "'",
          'updated_at' => "'" . $now . "'"
        ];
        $inventoryGroupPriceTobeInsert[] = '(' . implode(',', $temp) . ')';
      }
    }

    if (!count($inventoryGroupPriceTobeInsert)) {
      return;
    }

    \DB::beginTransaction();

    try {

      $groupPriceInserted = \DB::select(\DB::raw('
        INSERT INTO store_inventory_group_prices (store_inventory_id, type, keep_profit_consistent, created_at, updated_at) 
        VALUES ' . implode(',', $inventoryGroupPriceTobeInsert) . ' RETURNING id, type, store_inventory_id
      '));

      $storeInvDetails = $this->storeInventories->getModel()
        ->select(
          'store_inventory_details.id as store_inventory_detail_id',
          'store_inventory_details.store_inventory_id',
          'pulsa_odeos.srp_price',
          'pulsa_odeos.price as base_price',
          'vendor_details.sell_price as vendor_price',
          'store_inventories.service_detail_id'
        )
        ->join('store_inventory_details', 'store_inventories.id', '=', 'store_inventory_details.store_inventory_id')
        ->leftJoin('store_inventory_group_price_details as vendor_details', 'vendor_details.id', '=', 'store_inventory_details.vendor_group_price_detail_id')
        ->join('pulsa_odeos', 'store_inventory_details.inventory_id', '=', 'pulsa_odeos.id')
        ->where('pulsa_odeos.status', Pulsa::STATUS_OK)
        ->whereIn('store_inventory_id', array_pluck($groupPriceInserted, 'store_inventory_id'))
        ->get();

      $inventoryGroupPriceDetailTobeInsert = [];
      $serviceDetail = [];

      foreach ($groupPriceInserted as $gp) {
        foreach ($storeInvDetails as $sInvDetail) {
          if ($sInvDetail->store_inventory_id != $gp->store_inventory_id) {
            continue;
          }

          if ($gp->type == InventoryGroupPrice::MARKET_PLACE) {
            $plannedPrice = $sellPrice = null;
          } else {

            if ($data['store_plan_id'] != Plan::FREE) {

              if (!isset($serviceDetail[$sInvDetail->service_detail_id])) {
                $serviceDetail[$sInvDetail->service_detail_id] = $this->serviceDetail->findById($sInvDetail->service_detail_id);
              }

              $margin = $this->marginFormatter->formatMargin($sInvDetail->base_price, [
                'service_detail' => $serviceDetail[$sInvDetail->service_detail_id]
              ]);

              $plannedPrice = $sellPrice = $margin['sale_price'];

            }
            else {
              $plannedPrice = $sellPrice = $sInvDetail->vendor_price + 50;
            }

          }

          $inventoryGroupPriceDetailTobeInsert[] = [
            'store_inventory_detail_id' => $sInvDetail->store_inventory_detail_id,
            'store_inventory_group_price_id' => $gp->id,
            'sell_price' => $sellPrice,
            'planned_price' => $plannedPrice,
            'created_at' => $now,
            'updated_at' => $now
          ];

        }

      }

      $this->storeInventoryGroupPriceDetails->saveBulk($inventoryGroupPriceDetailTobeInsert);


      \DB::commit();

      isset($data['link_related_user_to_group_price']) && dispatch(new PopulateUserGroupPrice([
        'plan_id' => $data['store_plan_id'],
        'store_id' => $data['store_id'],
        'service_ids' => [
          ServiceDetail::getServiceId($data['service_detail_id'])
        ],
      ]));

    } catch (\Exception $e) {
      \DB::rollback();
      throw $e;
    }

  }
}
