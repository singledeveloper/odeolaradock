<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/15/17
 * Time: 7:33 PM
 */

namespace Odeo\Domains\Subscription\ManageInventory\Jobs;

use Odeo\Jobs\Job;
use Redis;

class RemoveDeletedProductInStoreInventory extends Job {

  private $data;
  private $storeInventories, $storesInventoryDetails, $pulsaOdeos,
    $currencyHelper, $serviceDetails;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->storesInventoryDetails = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryDetailRepository::class);
    $this->pulsaOdeos = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
  }

  public function handle() {

    $data = $this->data;
    $redis = Redis::connection();
    if (($val = $redis->get('odeo_core:remove_store_inventory_lock')) && $val == 1) return;

    $redis->set('odeo_core:remove_store_inventory_lock', 1);

    $this->storesInventoryDetails->getModel()
      ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_details.store_inventory_id')
      ->whereIn('inventory_id', $data['inventory_ids'])
      ->where('service_detail_id', $data['service_detail_id'])
      ->delete();

    $redis->del('odeo_core:remove_store_inventory_lock');


  }

}
