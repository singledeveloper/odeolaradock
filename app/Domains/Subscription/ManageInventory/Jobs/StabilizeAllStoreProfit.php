<?php

namespace Odeo\Domains\Subscription\ManageInventory\Jobs;

use Carbon\Carbon;
use Odeo\Jobs\Job;

class StabilizeAllStoreProfit extends Job {

  private $storeInventoryGroupPrices;
  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->storeInventoryGroupPrices = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository::class);
  }

  public function handle() {

    $data = $this->data;

    foreach ($data['inventories'] as $key => $inv) {
      $manualUpdateGroupPriceIds = $this->storeInventoryGroupPrices->getAllStoreManualUpdateIds($key);
      $this->processUpdate($inv, $manualUpdateGroupPriceIds);
    }
  }

  private function processUpdate($inventories, $manualUpdateGroupPriceIds) {

    $tempTables = [];
    $manualUpdateGroupPriceQuery = '';
    $now = Carbon::now()->toDateTimeString();

    foreach ($inventories as $inv) {
      $temp = join(',', [
        $inv['inventory_id'],
        $inv['price_after'] - $inv['price_before']
      ]);
      $tempTables[] = '(' . $temp . ')';
    }

    $tempTables = join(',', $tempTables);

    if (!empty($manualUpdateGroupPriceIds)) {
      $manualUpdateGroupPriceIds = join(',', array_pluck($manualUpdateGroupPriceIds, 'store_inventory_group_price_id'));
      $manualUpdateGroupPriceQuery = " store_inventory_group_prices.id NOT IN ($manualUpdateGroupPriceIds) AND ";
    }

    \DB::statement("
      WITH inventories AS (
        SELECT * FROM
        (values $tempTables) AS d(inventory_id, profit)
      )
      UPDATE store_inventory_group_price_details AS dtl
      SET 
        sell_price = store_inventory_group_price_details.sell_price + inventories.profit,
        planned_price = store_inventory_group_price_details.planned_price + inventories.profit,
        updated_at = '$now'
      FROM store_inventory_group_price_details
        JOIN store_inventory_details
          ON store_inventory_details.id = store_inventory_group_price_details.store_inventory_detail_id
        JOIN store_inventory_group_prices
          ON store_inventory_group_price_details.store_inventory_group_price_id = store_inventory_group_prices.id
        JOIN inventories ON store_inventory_details.inventory_id = inventories.inventory_id
        WHERE store_inventory_details.id = dtl.store_inventory_detail_id AND
            store_inventory_group_prices.id = dtl.store_inventory_group_price_id AND
            $manualUpdateGroupPriceQuery
            store_inventory_group_prices.type NOT IN ('market_place')
    ");
  }


}
