<?php

namespace Odeo\Domains\Subscription\ManageInventory\Jobs;

use Carbon\Carbon;
use Odeo\Jobs\Job;

class StabilizeStoreChildrenProfit extends Job {

  private $storeInventoryGroupPrices;
  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->storeInventoryGroupPrices = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository::class);
  }

  public function handle() {
    $data = $this->data;

    switch ($data['filter']) {
      case 'store':
        $this->filterByStore($data);
        break;
      case 'group_price':
        $this->filterByGroupPrice($data);
        break;
    }
  }

  public function filterByGroupPrice($data) {
    foreach ($data['inventories'] as $groupPriceId => $items) {
      $autoUpdateIds = $this->storeInventoryGroupPrices->getStoreChildrenKeepProfitConsistentGroupPriceId($groupPriceId);
      !empty($autoUpdateIds) && $this->processUpdate($items, $autoUpdateIds);
    }
  }

  public function filterByStore($data) {
    foreach ($data['inventories'] as $storeId => $inventories) {
      foreach ($inventories as $serviceDetailId => $items) {
        $autoUpdateIds = $this->storeInventoryGroupPrices->getStoreChildrenKeepProfitConsistentByStoreId([
          'store_id' => $storeId,
          'service_detail_id' => $serviceDetailId
        ]);
        !empty($autoUpdateIds) && $this->processUpdate($items, $autoUpdateIds);
      }
    }
  }

  private function processUpdate($inventories, $autoUpdateIds) {
    $tempTables = [];
    $now = Carbon::now()->toDateTimeString();

    foreach ($inventories as $inv) {
      $temp = join(',', [
        $inv['inventory_id'],
        $inv['price_after'] - $inv['price_before']
      ]);
      $tempTables[] = '(' . $temp . ')';
    }

    $tempTables = join(',', $tempTables);
    $autoUpdateIds = join(',', array_pluck($autoUpdateIds, 'store_inventory_group_price_id'));

    \DB::statement("
      WITH inventories AS (
        SELECT * FROM
        (values $tempTables) AS d(inventory_id, profit)
      )
      UPDATE store_inventory_group_price_details AS dtl
      SET 
        sell_price = dtl.sell_price + inventories.profit,
        planned_price = dtl.planned_price + inventories.profit,
        updated_at = '$now'
      FROM
        store_inventory_details
        JOIN inventories ON store_inventory_details.inventory_id = inventories.inventory_id
      WHERE store_inventory_details.id = dtl.store_inventory_detail_id AND
            dtl.store_inventory_group_price_id IN ($autoUpdateIds)
    ");
  }


}
