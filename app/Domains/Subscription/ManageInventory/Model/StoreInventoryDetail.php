<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/14/17
 * Time: 6:54 PM
 */

namespace Odeo\Domains\Subscription\ManageInventory\Model;


use Odeo\Domains\Core\Entity;

class StoreInventoryDetail extends Entity {

  public function storeInventory() {
    return $this->belongsTo(StoreInventory::class, 'store_inventory_id');
  }

}