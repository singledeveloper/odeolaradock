<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/1/17
 * Time: 15:57
 */

namespace Odeo\Domains\Subscription\ManageInventory\Model;


use Odeo\Domains\Core\Entity;

class StoreInventoryGroupPriceDetail extends Entity {

  public function storeInventoryDetail() {
    return $this->belongsTo(StoreInventoryDetail::class, 'store_inventory_detail_id');
  }

  public function groupPrice() {
    return $this->belongsTo(StoreInventoryGroupPrice::class, 'store_inventory_group_price_id');
  }

}