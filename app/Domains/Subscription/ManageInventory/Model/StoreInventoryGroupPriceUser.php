<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/26/17
 * Time: 13:59
 */

namespace Odeo\Domains\Subscription\ManageInventory\Model;


use Odeo\Domains\Core\Entity;

class StoreInventoryGroupPriceUser extends Entity {

  public function storeInventoryGroupPrice() {
    return $this->belongsTo(StoreInventoryGroupPrice::class, 'store_inventory_group_price_id');
  }

}