<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/14/17
 * Time: 6:53 PM
 */

namespace Odeo\Domains\Subscription\ManageInventory\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\ManageInventory\Model\StoreInventoryDetail;

class StoreInventoryDetailRepository extends Repository {

  public function __construct(StoreInventoryDetail $storeInventoryDetail) {
    $this->model = $storeInventoryDetail;
  }

  public function getByInventoryIds($storeInventoryIds, $inventoryIds) {
    return $this->model->with(['storeInventory'])->whereIn('store_inventory_id', $storeInventoryIds)
      ->whereIn('inventory_id', $inventoryIds)->get();
  }

  public function getByStoreInventoryId($storeInventoryId) {
    return $this->model->where('store_inventory_id', $storeInventoryId)->get();
  }

}