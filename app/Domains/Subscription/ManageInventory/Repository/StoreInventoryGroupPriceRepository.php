<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/1/17
 * Time: 15:59
 */

namespace Odeo\Domains\Subscription\ManageInventory\Repository;


use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\ManageInventory\Model\StoreInventoryGroupPrice;

class StoreInventoryGroupPriceRepository extends Repository {

  public function __construct(StoreInventoryGroupPrice $storeInventoryGroupPrice) {
    $this->model = $storeInventoryGroupPrice;
  }

  public function getDefaultGroupPrice($userId, $storeId, $serviceDetailId) {
    return $this->model
      ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_group_prices.store_inventory_id')
      ->join('store_inventory_group_price_users', 'store_inventory_group_price_users.store_inventory_id', '=', 'store_inventory_group_prices.id')
      ->where('store_id', $storeId)
      ->where('service_detail_id', $serviceDetailId)
      ->where('user_id', $userId)
      ->first();
  }

  public function getGroupPriceByType($storeId, $serviceIds, $type = InventoryGroupPrice::AGENT_DEFAULT) {
    return $this->model
      ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_group_prices.store_inventory_id')
      ->where('store_id', $storeId)
      ->whereIn('service_id', is_array($serviceIds) ? $serviceIds : [$serviceIds])
      ->where('type', $type)
      ->select(
        \DB::raw('store_inventories.id as store_inventory_id'),
        \DB::raw('store_inventory_group_prices.id as store_inventory_group_price_id'),
        'service_detail_id'
      )
      ->get();
  }

  public function getAllGroupPriceByStore($storeId, $serviceId) {
    return $this->model
      ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_group_prices.store_inventory_id')
      ->where('store_id', $storeId)
      ->where('service_id',  $serviceId)
      ->select(
        \DB::raw('store_inventories.id as store_inventory_id'),
        \DB::raw('store_inventory_group_prices.id as store_inventory_group_price_id'),
        'service_id',
        'store_inventory_group_prices.type'
      )
      ->get();
  }

  public function getAllStoreManualUpdateIds($serviceDetailId) {
    return \DB::select(\DB::raw("
      WITH RECURSIVE vendor_inventories (store_id, store_inventory_group_price_id) AS (
      SELECT
        store_inventories.store_id,
        store_inventory_group_prices.id
      FROM store_inventories
        JOIN store_inventory_group_prices ON store_inventories.id = store_inventory_group_prices.store_inventory_id
      WHERE 
        store_inventory_group_prices.keep_profit_consistent = false AND 
        store_inventory_group_prices.type NOT IN ('market_place') AND
        store_inventories.service_detail_id = $serviceDetailId
      UNION ALL
      SELECT DISTINCT
        store_inventories.store_id,
        store_inventory_group_prices.id
      FROM vendor_inventories vi
        JOIN store_inventory_vendors ON store_inventory_vendors.vendor_id = vi.store_id
        JOIN store_inventories ON store_inventories.id = store_inventory_vendors.store_inventory_id
        JOIN store_inventory_group_prices ON store_inventories.id = store_inventory_group_prices.store_inventory_id
        JOIN user_stores ON user_stores.store_id = store_inventories.store_id
        JOIN store_inventory_group_price_users
          ON store_inventory_group_price_users.store_inventory_group_price_id = vi.store_inventory_group_price_id AND
             store_inventory_group_price_users.user_id = user_stores.user_id
      WHERE 
        store_inventory_group_prices.type NOT IN ('market_place')
      )
      SELECT *
      FROM vendor_inventories
    "));
  }


  public function getStoreChildrenKeepProfitConsistentByStoreId($data) {
    return \DB::select(\DB::raw("
      WITH RECURSIVE vendor_inventories (store_id, store_inventory_group_price_id) AS (
        SELECT
          store_inventories.store_id,
          store_inventory_group_prices.id
        from   store_inventories
          JOIN store_inventory_group_prices
            ON store_inventory_group_prices.store_inventory_id = store_inventories.id
        WHERE
          store_inventory_group_prices.type NOT IN ('market_place') AND
          store_inventories.store_id = " . $data['store_id'] . " and
          service_detail_id = " . $data['service_detail_id'] . " and
          keep_profit_consistent = true
        UNION ALL
        SELECT DISTINCT
          store_inventories.store_id,
          store_inventory_group_prices.id
        FROM vendor_inventories vi
          JOIN store_inventory_vendors ON store_inventory_vendors.vendor_id = vi.store_id
          JOIN store_inventories ON store_inventories.id = store_inventory_vendors.store_inventory_id
          JOIN store_inventory_group_prices ON store_inventories.id = store_inventory_group_prices.store_inventory_id
          JOIN user_stores ON user_stores.store_id = store_inventories.store_id
          JOIN store_inventory_group_price_users
            ON store_inventory_group_price_users.store_inventory_group_price_id = vi.store_inventory_group_price_id AND
               store_inventory_group_price_users.user_id = user_stores.user_id
        WHERE
          store_inventory_group_prices.type NOT IN ('market_place') AND
          store_inventory_group_prices.keep_profit_consistent = TRUE
      )
      SELECT *
      FROM vendor_inventories
    "));
  }

  public function getStoreChildrenKeepProfitConsistentGroupPriceId($groupPriceId) {
    return \DB::select(\DB::raw("
        WITH RECURSIVE vendor_inventories (store_id, store_inventory_group_price_id) AS (
          SELECT
            store_inventories.store_id,
            store_inventory_group_prices.id
          FROM store_inventory_group_price_users
           JOIN store_inventory_group_prices
            ON store_inventory_group_price_users.store_inventory_group_price_id = store_inventory_group_prices.id
           JOIN store_inventories ON store_inventory_group_prices.store_inventory_id = store_inventories.id
        WHERE
          store_inventory_group_prices.type NOT IN ('market_place') AND
          store_inventory_group_price_users.store_inventory_group_price_id = " . $groupPriceId . "
        UNION ALL
        SELECT DISTINCT
          store_inventories.store_id,
          store_inventory_group_prices.id
        FROM vendor_inventories vi
          JOIN store_inventory_vendors ON store_inventory_vendors.vendor_id = vi.store_id
          JOIN store_inventories ON store_inventories.id = store_inventory_vendors.store_inventory_id
          JOIN store_inventory_group_prices ON store_inventories.id = store_inventory_group_prices.store_inventory_id
          JOIN user_stores ON user_stores.store_id = store_inventories.store_id
          JOIN store_inventory_group_price_users
            ON store_inventory_group_price_users.store_inventory_group_price_id = vi.store_inventory_group_price_id AND
               store_inventory_group_price_users.user_id = user_stores.user_id
        WHERE
          store_inventory_group_prices.type NOT IN ('market_place') AND
          store_inventory_group_prices.keep_profit_consistent = TRUE
        )
        SELECT *
        FROM vendor_inventories 
        WHERE store_inventory_group_price_id <> " . $groupPriceId . "
    "));
  }

  public function getAllSupplyGroupPriceByStore($data) {
    $query = $this->model
      ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_group_prices.store_inventory_id')
      ->whereNotNull('store_inventory_group_prices.supply_group_id')
      ->where('store_id', $data['store_id'])
      ->orderBy('store_inventory_group_prices.name', 'asc')
      ->distinct()
      ->select(
        'store_inventory_group_prices.supply_group_id',
        'store_inventory_group_prices.name',
        'store_inventory_group_prices.description'
      );
    if (isset($data['all'])) return $query->get();
    return $this->getResult($query);
  }

  public function getBySupplyGroupId($supplyGroupId) {
    return $this->model->with(['storeInventory'])
      ->where('supply_group_id', $supplyGroupId)->get();
  }

  public function getBySupplyStoreInventoryId($storeInventoryId) {
    return $this->model
      ->where('store_inventory_id', $storeInventoryId)
      ->whereNotNull('supply_group_id')
      ->get();
  }

  public function getByStoreInventoryId($storeInventoryId) {
    return $this->model
      ->where('store_inventory_id', $storeInventoryId)
      ->get();
  }

  public function generateSupplyGroupId() {
    $row = $this->model->whereNotNull('supply_group_id')
      ->orderBy('supply_group_id', 'desc')->first();

    if ($row) return $row->supply_group_id + 1;
    return 1;
  }

}
