<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/14/17
 * Time: 6:53 PM
 */

namespace Odeo\Domains\Subscription\ManageInventory\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\ManageInventory\Model\StoreInventoryVendor;

class StoreInventoryVendorRepository extends Repository {

  public function __construct(StoreInventoryVendor $storeInventoryVendor) {
    $this->model = $storeInventoryVendor;
  }

  public function isUpLine($serviceDetailId, $myStoreId, $vendorStoreId) {
    $query = \DB::select(\DB::raw('
      WITH RECURSIVE vendor_inventories (store_id) AS (
        SELECT ' . $myStoreId . ' :: BIGINT
        UNION ALL
        SELECT store_inventory_vendors.vendor_id
        FROM vendor_inventories vi
          JOIN store_inventories ON store_inventories.store_id = vi.store_id
          JOIN store_inventory_vendors ON store_inventories.id = store_inventory_vendors.store_inventory_id
        WHERE service_detail_id = ' . $serviceDetailId . ' and store_inventories.store_id != ' . $vendorStoreId . '
      )
      SELECT store_id
      FROM vendor_inventories where store_id = ' . $vendorStoreId . ';    
    '));

    return !empty($query);

  }

  public function isDownLine($serviceDetailId, $myStoreId, $vendorStoreId) {
    $query = \DB::select(\DB::raw('
      WITH RECURSIVE vendor_inventories (store_id) AS (
        SELECT ' . $myStoreId . ' :: BIGINT
        UNION ALL
        SELECT store_inventories.store_id
        FROM vendor_inventories vi join store_inventory_vendors on vi.store_id = store_inventory_vendors.vendor_id
        join store_inventories on store_inventories.id = store_inventory_vendors.store_inventory_id
        WHERE service_detail_id =' . $serviceDetailId . ' and store_inventory_vendors.vendor_id != ' . $vendorStoreId . '
      )
      SELECT store_id
      FROM vendor_inventories where store_id = ' . $vendorStoreId . ';
    '));

    return !empty($query);

  }

  public function getVendorIds($data) {
    return $this->model
      ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_vendors.store_inventory_id')
      ->join('stores', 'stores.id', '=', 'store_inventories.store_id')
      ->where('service_detail_id', $data['service_detail_id'])
      ->where('store_id', $data['store_id'])
      ->select(
        'store_inventory_vendors.vendor_id',
        'store_inventory_vendors.id',
        'stores.can_supply'
      )
      ->get();
  }

  public function hasVendor($storeId, $serviceId) {
    return $this->model
      ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_vendors.store_inventory_id')
      ->where('service_id',$serviceId)
      ->where('store_id', $storeId)
      ->select('store_inventory_vendors.vendor_id')
      ->count();
  }


  public function getUserDirectDownline($storeId) {

    $query = $this->model
      ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_vendors.store_inventory_id')
      ->join('user_stores', 'user_stores.store_id', '=', 'store_inventories.store_id')
      ->where('store_inventory_vendors.vendor_id', $storeId)
      ->where('user_stores.type', 'owner')
      ->select(
        'user_id',
        \DB::raw('min(store_inventory_vendors.created_at) as joined_at')
      )
      ->groupBy('user_id');

    return $this->getResult($query);
  }


}
