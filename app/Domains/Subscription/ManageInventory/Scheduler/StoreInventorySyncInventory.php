<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 28/02/18
 * Time: 13.06
 */

namespace Odeo\Domains\Subscription\ManageInventory\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Inventory\Helper\MarginFormatter;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository;
use Odeo\Domains\Inventory\Repository\ServiceDetailRepository;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryDetailRepository;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceDetailRepository;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceUserRepository;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryVendorRepository;

class StoreInventorySyncInventory {

  private $storeInventoryRepo, $pulsaOdeoRepo, $vendorStoreRepo,
    $groupPriceUserRepo, $storeInventoryDetailRepo, $storeInventoryGroupPriceRepo,
    $storeInventoryGroupPriceDetailRepo, $marginFormatter, $serviceDetailRepo;

  private $inventories = [];
  private $serviceDetail = [];

  public function __construct() {
    $this->storeInventoryRepo = app()->make(StoreInventoryRepository::class);
    $this->pulsaOdeoRepo = app()->make(PulsaOdeoRepository::class);
    $this->vendorStoreRepo = app()->make(StoreInventoryVendorRepository::class);
    $this->groupPriceUserRepo = app()->make(StoreInventoryGroupPriceUserRepository::class);
    $this->storeInventoryDetailRepo = app()->make(StoreInventoryDetailRepository::class);
    $this->storeInventoryGroupPriceRepo = app()->make(StoreInventoryGroupPriceRepository::class);
    $this->storeInventoryGroupPriceDetailRepo = app()->make(StoreInventoryGroupPriceDetailRepository::class);
    $this->marginFormatter = app()->make(MarginFormatter::class);
    $this->serviceDetailRepo = app()->make(ServiceDetailRepository::class);
  }

  public function run() {
    $storeInventories = $this->getAllActiveStores();
    foreach ($storeInventories as $str) {
      $inventoryDetails = $this->storeInventoryDetailRepo->getByStoreInventoryId($str->id);
      if (!$inventoryDetails || $inventoryDetails->isEmpty()) {
        continue;
      }

      $inventoryDetailTobeInsert = $str->plan_id == Plan::FREE
        ? $this->getInventoryDetailToInsertByPlanFree($str, $inventoryDetails)
        : $this->getInventoryDetailToInsertByPlanPaid($str, $inventoryDetails);

      if (!empty($inventoryDetailTobeInsert)) {
        \DB::beginTransaction();

        $insertedDetail = $this->saveStoreInventoryDetail($inventoryDetailTobeInsert);

        $insertedDetail = json_decode(json_encode($insertedDetail), true);
        $insertedDetail = combineArrayByMatchedValue($insertedDetail, $inventoryDetailTobeInsert);

        $this->populateUserGroupPriceDetail($str, $insertedDetail);

        \DB::commit();
      }
    }
  }

  private function populateUserGroupPriceDetail($str, $insertedinventoryDetail) {
    $now = Carbon::now();
    $inventoryGroupPriceDetailTobeInsert = [];
    $groupPrices = $this->storeInventoryGroupPriceRepo
      ->getAllGroupPriceByStore($str->store_id, $str->service_id);

    foreach ($groupPrices as $gp) {
      foreach ($insertedinventoryDetail as $inv) {
        $sellPrice = $gp->type === InventoryGroupPrice::MARKET_PLACE
          ? null
          : $this->getPricing($str, $inv);

        $inventoryGroupPriceDetailTobeInsert[] = [
          'store_inventory_detail_id' => $inv['id'],
          'store_inventory_group_price_id' => $gp->store_inventory_group_price_id,
          'sell_price' => $sellPrice,
          'planned_price' => $sellPrice,
          'created_at' => $now,
          'updated_at' => $now
        ];
      }
    }
    $this->storeInventoryGroupPriceDetailRepo
      ->saveBulk($inventoryGroupPriceDetailTobeInsert);

  }

  private function getPricing($str, $inv) {
    if ($str->plan_id == Plan::FREE) {
      return $inv['sell_price'] + 50;
    }

    $serviceDetailId = $str->service_detail_id;

    $sellPrice = $this->inventories[$serviceDetailId][$inv['inventory_id']]['price'];

    if (!isset($this->serviceDetail[$serviceDetailId])) {
      $this->serviceDetail[$serviceDetailId] = $this->serviceDetailRepo
        ->findById($serviceDetailId);
    }

    $margin = $this->marginFormatter->formatMargin($sellPrice, [
      'service_detail' => $this->serviceDetail[$serviceDetailId]
    ]);

    return $margin['sale_price'];
  }


  private function saveStoreInventoryDetail($inventoryDetailTobeInsert) {
    $data = array_map(function ($item) {
      return array_except($item, 'sell_price');
    }, $inventoryDetailTobeInsert);

    return $this->storeInventoryDetailRepo
      ->saveBulkReturningId($data, [
        'id', 'inventory_id'
      ]);
  }


  private function getInventories($serviceDetailid) {
    $inventories = $this->inventories;

    if (isset($inventories[$serviceDetailid])) {
      return $inventories[$serviceDetailid];
    }

    $inventories[$serviceDetailid] = $this->pulsaOdeoRepo
      ->findByServiceDetailId($serviceDetailid)
      ->keyBy('id')
      ->all();

    return $this->inventories[$serviceDetailid] = $inventories[$serviceDetailid];
  }

  private function getUnSyncedInventories($inventoryDetails, $inventories, $key = 'id') {
    return array_filter($inventories, function ($inv) use ($inventoryDetails, $key) {
      $filtered = array_filter($inventoryDetails, function ($detail) use ($inv, $key) {
        return $detail['inventory_id'] == $inv[$key];
      });
      return empty($filtered);
    });
  }

  private function getAllActiveStores() {
    return $this->storeInventoryRepo->getModel()
      ->join('stores', 'store_inventories.store_id', '=', 'stores.id')
      ->join('user_stores', 'user_stores.store_id', '=', 'stores.id')
      ->whereIn('stores.status', StoreStatus::ACTIVE)
      ->where('user_stores.type', 'owner')
      ->where('stores.can_supply', false)
      ->where('store_inventories.active', true)
      ->select(
        'stores.plan_id',
        'user_stores.user_id',
        'user_stores.store_id',
        'store_inventories.id',
        'store_inventories.service_detail_id',
        'store_inventories.service_id'
      )
      ->get();
  }

  private function getInventoryDetailToInsertByPlanFree($str, $inventoryDetails) {
    $now = Carbon::now();
    $inventoryDetailTobeInsert = [];

    $vendorIds = $this->vendorStoreRepo->getVendorIds([
      'service_detail_id' => $str->service_detail_id,
      'store_id' => $str->store_id
    ]);

    $firstVendorStoreId = $vendorIds->first();
    $userGroupPrice = null;

    $userGroupPrice = $this->groupPriceUserRepo->getUserGroupPrice(
      $str->user_id, $firstVendorStoreId->vendor_id, $str->service_detail_id
    )->first();

    $vendorInventories = $this->storeInventoryRepo->getStorePricing([
      'store_id' => $firstVendorStoreId->vendor_id,
      'service_detail_id' => $str->service_detail_id,
      'group_price_id' => $userGroupPrice ? $userGroupPrice->store_inventory_group_price_id : null,
      'store_can_supply' => $firstVendorStoreId->can_supply
    ], [
      'store_inventory_details.inventory_id as inventory_id',
      'store_inventory_group_price_details.id as store_inventory_group_price_detail_id',
      'store_inventory_group_price_details.sell_price as sell_price',
    ], false);


    $unSynced = $this->getUnSyncedInventories($inventoryDetails->all(),
      $vendorInventories->all(), 'inventory_id');

    foreach ($unSynced as $venInv) {
      $inventoryDetailTobeInsert[] = [
        'store_inventory_id' => $str->id,
        'inventory_id' => $venInv->inventory_id,
        'store_inventory_vendor_id' => $firstVendorStoreId->id,
        'vendor_group_price_detail_id' => $venInv->store_inventory_group_price_detail_id,
        'sell_price' => $venInv->sell_price,
        'updated_at' => $now,
        'created_at' => $now,
      ];
    }

    return $inventoryDetailTobeInsert;
  }

  private function getInventoryDetailToInsertByPlanPaid($str, $inventoryDetails) {
    $now = Carbon::now();
    $inventoryDetailTobeInsert = [];

    $inventories = $this->getInventories($str->service_detail_id);

    $unSynced = $this
      ->getUnSyncedInventories($inventoryDetails->all(), $inventories);

    foreach ($unSynced as $uns) {
      $inventoryDetailTobeInsert[] = [
        'store_inventory_id' => $str->id,
        'inventory_id' => $uns->id,
        'store_inventory_vendor_id' => null,
        'vendor_group_price_detail_id' => null,
        'updated_at' => $now,
        'created_at' => $now
      ];
    }
    return $inventoryDetailTobeInsert;
  }

}
