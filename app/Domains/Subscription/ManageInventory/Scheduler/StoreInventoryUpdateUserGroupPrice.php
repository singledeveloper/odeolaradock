<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/5/17
 * Time: 16:43
 */

namespace Odeo\Domains\Subscription\ManageInventory\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Subscription\ManageInventory\Jobs\StabilizeStoreChildrenProfit;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceUserRepository;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository;

class StoreInventoryUpdateUserGroupPrice {

  public function __construct() {
    $this->groupPriceUsers = app()->make(StoreInventoryGroupPriceUserRepository::class);
    $this->storeInventories = app()->make(StoreInventoryRepository::class);
  }

  public function run() {

    $userToBeApply = $this->groupPriceUsers->getGroupPricePricingToApply();
    $now = Carbon::now()->toDateTimeString();

    foreach ($userToBeApply as $apply) {

      try {

        $toBeUpdate = [];
        $profitToBeUpdate = [];

        $currInvenDetails = $this->getCurrentInvenDetail($apply);

        $newInvenDetails = $this->getNewInvenDetail($apply, $currInvenDetails->pluck('inventory_id'));

        foreach ($currInvenDetails as $curr) {

          if (!isset($newInvenDetails[$curr->inventory_id])) continue;

          $new = $newInvenDetails[$curr->inventory_id];

          if (!isset($toBeUpdate[$curr->inventory_detail_id])) {
            $temp = [
              'id' => $curr->inventory_detail_id,
              'vendor_group_price_detail_id' => $new->id
            ];
            $toBeUpdate[$curr->inventory_detail_id] = '(' . implode(',', $temp) . ')';
          }

          if ($curr->sell_price == $new->sell_price) continue;

          $profitToBeUpdate[$curr->store_id][$apply->service_detail_id][] = [
            'inventory_id' => $curr->inventory_id,
            'price_before' => $curr->sell_price,
            'price_after' => $new->sell_price
          ];

        }

        \DB::beginTransaction();

        !empty($toBeUpdate) && \DB::statement("
          UPDATE store_inventory_details AS invd SET
          vendor_group_price_detail_id = d.vendor_group_price_detail_id,
          updated_at = '$now'
          FROM (VALUES
            " . join(',', $toBeUpdate) . "
          ) AS d(id, vendor_group_price_detail_id)
          WHERE d.id = invd.id 
       ");

        $this->groupPriceUsers->getModel()
          ->where('id', $apply->group_price_user_id)
          ->update([
            'group_price_apply_at' => null,
            'store_inventory_group_price_id' => \DB::raw('new_store_inventory_group_price_id'),
            'updated_at' => $now
          ]);

        !empty($profitToBeUpdate) && dispatch(new StabilizeStoreChildrenProfit([
          'inventories' => $profitToBeUpdate,
          'filter' => 'store'
        ]));

        \DB::commit();

      } catch (\Exception $exception) {
        \DB::rollback();

        throw $exception;
      }

    }


  }

  private function getCurrentInvenDetail($apply) {

    return $this->storeInventories->getModel()
      ->join('user_stores', 'user_stores.store_id', '=', 'store_inventories.store_id')
      ->join('store_inventory_vendors', 'store_inventory_vendors.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_details', 'store_inventory_details.store_inventory_vendor_id', '=', 'store_inventory_vendors.id')
      ->join('store_inventory_group_price_details', 'store_inventory_group_price_details.id', '=', 'store_inventory_details.vendor_group_price_detail_id')
      ->leftJoin('store_inventory_group_price_details as my', 'my.store_inventory_detail_id', '=', 'store_inventory_details.id')
      ->where('store_inventory_vendors.vendor_id', $apply->store_id)
      ->where('user_stores.user_id', $apply->user_id)
      ->where('user_stores.type', 'owner')
      ->where('service_detail_id', $apply->service_detail_id)
      ->select(
        'store_inventory_details.id as inventory_detail_id',
        'store_inventory_details.inventory_id',
        'my.store_inventory_group_price_id',
        'store_inventories.store_id',
        'store_inventory_group_price_details.sell_price'
      )
      ->get();
  }

  private function getNewInvenDetail($apply, $inventoryIds) {
    return $this->storeInventories->getModel()
      ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_group_price_details', 'store_inventory_group_price_details.store_inventory_detail_id', '=', 'store_inventory_details.id')
      ->where('store_inventory_group_price_details.store_inventory_group_price_id', $apply->new_store_inventory_group_price_id)
      ->whereIn('store_inventory_details.inventory_id', $inventoryIds)
      ->select(
        'store_inventory_details.inventory_id',
        'store_inventory_group_price_details.id',
        'store_inventory_group_price_details.sell_price',
        'store_inventory_group_price_details.store_inventory_group_price_id'
      )
      ->get()->keyBy('inventory_id');
  }

}