<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/19/17
 * Time: 17:26
 */

namespace Odeo\Domains\Subscription\ManageInventory;


use Odeo\Domains\Core\PipelineListener;

class StoreInventoryDisabler {


  public function __construct() {
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
  }

  public function toggle(PipelineListener $listener, $data) {

    $this->storeInventories->getModel()
      ->where('store_id', $data['store_id'])
      ->where('service_detail_id', $data['service_detail_id'])
      ->update(['active' => \DB::raw('not active')]);


    return $listener->response(200);

  }
}