<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/11/17
 * Time: 19:10
 */

namespace Odeo\Domains\Subscription\ManageInventory;


use Carbon\Carbon;
use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\PipelineListener;

class StoreInventoryGroupPriceCreator {

  public function __construct() {
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->inventoryGroupPrices = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository::class);
    $this->storeInventoryGroupPriceDetails = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceDetailRepository::class);
  }

  public function create(PipelineListener $listener, $data) {

    $store = $this->stores->findById($data['store_id']);
    if ($store->can_supply)
      return $listener->response(400, 'Error. Anda hanya dapat membuat grup dari Seller Center.');

    $inventoryGroupPriceDetailTobeInsert = [];

    $groupPrice = $this->inventoryGroupPrices->getNew();

    $pricing = $this->getPricing($data);

    if ($pricing->isEmpty()) {
      return $listener->response(400);
    }

    $groupPrice->store_inventory_id = $pricing->first()->store_inventory_id;
    $groupPrice->type = InventoryGroupPrice::CUSTOM;
    $groupPrice->name = title_case($data['name']);
    $groupPrice->description = $data['description'];
    $groupPrice->keep_profit_consistent = true;

    $this->inventoryGroupPrices->save($groupPrice);

    $now = Carbon::now();

    foreach ($pricing as $p) {
      $inventoryGroupPriceDetailTobeInsert[] = [
        'store_inventory_detail_id' => $p->inventory_detail_id,
        'store_inventory_group_price_id' => $groupPrice->id,
        'sell_price' => $p->sell_price,
        'planned_price' => $p->sell_price,
        'created_at' => $now,
        'updated_at' => $now,
      ];
    }

    //clog('theheck', json_encode($inventoryGroupPriceDetailTobeInsert));
    $this->storeInventoryGroupPriceDetails->saveBulk($inventoryGroupPriceDetailTobeInsert);

    return $listener->response(200, [
      'group_price_id' => $groupPrice->id,
      'group_price_type' => $groupPrice->type
    ]);

  }

  private function getPricing($data) {
    if ($data['store']['plan_id'] !== Plan::FREE && $data['selected_group_price_type'] == InventoryGroupPrice::MARKET_PLACE) {
      return $this->getMarketPlacePricing($data);

    } else if ($data['store']['plan_id'] !== Plan::FREE && (!isset($data['selected_group_price_id']) || !$data['selected_group_price_id'])) {
      return $this->storeInventories->getOdeoBasePriceByInventory($data['store_id'], $data['service_detail_id']);

    } else if (!isset($data['selected_group_price_id']) || !$data['selected_group_price_id']) {
      return $this->storeInventories->getBasePriceByInventory($data['store_id'], $data['service_detail_id']);

    } else {
      return $this->storeInventories->getGroupPricePricingByInventory($data['selected_group_price_id'], $data['store_id'], $data['service_detail_id']);

    }
  }


  private function getMarketPlacePricing($data) {
    $marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $serviceDetail = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class)->findById($data['service_detail_id']);

    return $this->storeInventories->getModel()
      ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'store_inventory_details.inventory_id')
      ->where('store_id', $data['store_id'])
      ->where('store_inventories.service_detail_id', $data['service_detail_id'])
      ->where('pulsa_odeos.status', Pulsa::STATUS_OK)
      ->select(
        'store_inventory_details.store_inventory_id',
        'store_inventory_details.id as inventory_detail_id',
        'store_inventory_details.inventory_id',
        'pulsa_odeos.price'
      )
      ->get()->map(function ($item) use ($marginFormatter, $serviceDetail) {

        $item->sell_price = $item->planned_price = $marginFormatter->formatMargin($item->price, [
          'service_detail' => $serviceDetail
        ])['sale_price'];

        return $item;

      });
  }

}
