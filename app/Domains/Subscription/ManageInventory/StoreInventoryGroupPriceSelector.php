<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/27/17
 * Time: 23:26
 */

namespace Odeo\Domains\Subscription\ManageInventory;

use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository;

class StoreInventoryGroupPriceSelector {

  public function __construct() {
    $this->storeInventories = app()->make(StoreInventoryRepository::class);
  }

  public function getByServiceDetailId(PipelineListener $listener, $data) {

    $response = $this->storeInventories->getModel()
      ->join('store_inventory_group_prices', 'store_inventory_group_prices.store_inventory_id', '=', 'store_inventories.id')
      ->where('store_id', $data['store_id'])
      ->where('service_detail_id', $data['service_detail_id'])
      ->whereIn('type', [
        InventoryGroupPrice::CUSTOM, InventoryGroupPrice::AGENT_DEFAULT
      ])
      ->select(
        'store_inventory_group_prices.id',
        'store_inventory_group_prices.name',
        'store_inventory_group_prices.description',
        'store_inventory_group_prices.pricing_apply_at'
      )->get()->map(function ($item) {

        if (!$item->name) {
          $item->name = trans('inventory/inventory_group_price.group_price_type_agent_default');
          $item->description = trans('inventory/inventory_group_price.group_price_description_agent_default');
        }
        $item->disabled = !!$item->pricing_apply_at;
        unset($item->pricing_apply_at);

        return $item;

      })->all();

    return $listener->response(200, $response);

  }


  public function compareGroupPrice(PipelineListener $listener, $data) {

    $response = [];

    $priceFormatter = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

    $this->storeInventories->getModel()
      ->join('store_inventory_group_prices', 'store_inventory_group_prices.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_group_price_details', function ($join) {
        $join->on('store_inventory_group_price_details.store_inventory_detail_id', '=', 'store_inventory_details.id');
        $join->on('store_inventory_group_price_details.store_inventory_group_price_id', '=', 'store_inventory_group_prices.id');
      })
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'store_inventory_details.inventory_id')
      ->where('store_inventories.store_id', $data['store_id'])
      ->where('store_inventories.service_detail_id', $data['service_detail_id'])
      ->where('pulsa_odeos.status', Pulsa::STATUS_OK)
      ->whereIn('store_inventory_group_prices.id', [
        $data['left_side'], $data['right_side']
      ])
      ->orderBy('pulsa_odeos.operator_id', 'asc')
      ->orderBy('pulsa_odeos.category', 'asc')
      ->orderBy('pulsa_odeos.price', 'asc')
      ->select(
        'pulsa_odeos.name',
        'store_inventory_group_price_details.sell_price',
        'store_inventory_group_prices.id as group_price_id'
      )
      ->get()
      ->each(function ($item) use ($data, &$response, $priceFormatter) {

        $key = (function () use ($item, $data) {
          return $item->group_price_id == $data['left_side'] ? 'left_side' : 'right_side';
        })();

        $item->sell_price = $priceFormatter->formatPrice($item->sell_price);

        $response[$key][] = $item;

      });

    return $listener->response(200, $response);

  }
}
