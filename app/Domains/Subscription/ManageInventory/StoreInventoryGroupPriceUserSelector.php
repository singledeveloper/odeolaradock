<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/27/17
 * Time: 23:26
 */

namespace Odeo\Domains\Subscription\ManageInventory;


use Carbon\Carbon;
use Odeo\Domains\Constant\AgentConfig;
use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Subscription\ManageInventory\Jobs\PopulateUserGroupPrice;

class StoreInventoryGroupPriceUserSelector {

  public function __construct() {
    $this->groupPriceUsers = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceUserRepository::class);
    $this->storeParser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);
    $this->groupPriceVendors = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryVendorRepository::class);
    $this->agents = app()->make(\Odeo\Domains\Agent\Repository\AgentRepository::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
  }

  public function getByUser(PipelineListener $listener, $data) {

    $storeAvailableService = json_decode($this->storeParser->currentData($data['store']['plan_id'], $data['store']['current_data'], true), true);

    $response = [];

    $groupPrice = $this->groupPriceUsers->getModel()
      ->join('store_inventory_group_prices as current_group_price', 'current_group_price.id', '=', 'store_inventory_group_price_users.store_inventory_group_price_id')
      ->leftJoin('store_inventory_group_prices as new_group_price', 'new_group_price.id', '=', 'store_inventory_group_price_users.new_store_inventory_group_price_id')
      ->join('store_inventories', 'store_inventories.id', '=', 'current_group_price.store_inventory_id')
      ->where('store_inventory_group_price_users.user_id', $data['agent_user_id'])
      ->where('store_inventories.store_id', $data['store_id'])
      ->select(
        'current_group_price.id',
        'current_group_price.name',
        'current_group_price.type',
        'new_group_price.name as new_name',
        'new_group_price.type as new_type',
        'store_inventory_group_price_users.group_price_apply_at',
        'service_detail_id'
      )
      ->get()->keyBy('service_detail_id');

    $agent = $this->agents->findAgentFromStore($data['agent_user_id'], $data['store_id']);

    foreach ($storeAvailableService as $key => $val) {

      $temp = [];

      if ($val === 0) continue;

      if (!isset($groupPrice[$val])) {
        if ($agent) {
          $myStore = $myStore ?? $this->stores->findById($data['store_id']);
          $storeOwner = $this->stores->findOwner($agent->store_referred_id);

          dispatch(new PopulateUserGroupPrice([
            'plan_id' => $myStore->plan_id,
            'user_id' => $data['agent_user_id'],
            'store_id' => $data['store_id'],
            'group_price_type' => $storeOwner->user_id == $agent->user_id ? InventoryGroupPrice::SELF : InventoryGroupPrice::AGENT_DEFAULT
          ]));
        }
        continue;
      }

      $temp['service_name'] = str_replace('_', ' ', $key);
      $temp['service_detail_id'] = $val;

      $temp['group_price_id'] = $groupPrice[$val]['id'];
      $temp['group_price_name'] = $groupPrice[$val]['name'] ? $groupPrice[$val]['name'] : trans('inventory/inventory_group_price.group_price_type_' .  $groupPrice[$val]['type']);
      $temp['group_price_type'] = $groupPrice[$val]['type'];
      $temp['disable_update'] = isset($groupPrice[$val]['group_price_apply_at']) || $groupPrice[$val]['type'] == InventoryGroupPrice::SELF;

      if ($groupPrice[$val]['group_price_apply_at']) {
        $temp['apply_at'] = Carbon::parse($groupPrice[$val]['group_price_apply_at'])->format('d F Y H:i:s');
        $temp['new_group_price_name'] = $groupPrice[$val]['new_name'] ? $groupPrice[$val]['new_name'] : trans('inventory/inventory_group_price.group_price_type_' .  $groupPrice[$val]['new_type']);
      }

      $response[] = $temp;

    }

    return $listener->response(200, $response);

  }


}