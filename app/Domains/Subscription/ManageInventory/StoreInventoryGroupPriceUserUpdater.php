<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/27/17
 * Time: 23:26
 */

namespace Odeo\Domains\Subscription\ManageInventory;


use Carbon\Carbon;
use Odeo\Domains\Constant\InventoryPricing;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Subscription\ManageInventory\Jobs\UpdateAgentPricingBlastNotification;

class StoreInventoryGroupPriceUserUpdater {


  public function __construct() {
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->groupPriceUsers = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceUserRepository::class);
  }

  public function update(PipelineListener $listener, $data) {

    $tobeUpdate = [];
    $serviceDetailIds = array_pluck($data['group_price'], 'service_detail_id');

    if (!in_array($data['selected_schedule'], array_keys(InventoryPricing::getUpdateTime()))) {
      clog('group_price_not_updating_issue', 'schedule has not been selected. ' . \json_encode('$data'));
      return $listener->response(400, 'schedule has not been selected.');
    }

    $currentGroupPriceIds = array_pluck($data['group_price'], 'current_group_price_id');
    $newGroupPriceIds = array_pluck($data['group_price'], 'new_group_price_id');
    $groupPriceTobeCheck = array_merge($currentGroupPriceIds, $newGroupPriceIds);

    if ($this->storeInventories->getModel()
        ->join('store_inventory_group_prices', 'store_inventory_group_prices.store_inventory_id', '=', 'store_inventories.id')
        ->whereIn('store_inventories.service_detail_id', $serviceDetailIds)
        ->whereIn('store_inventory_group_prices.id', $groupPriceTobeCheck)
        ->where('store_id', $data['store_id'])
        ->select(
          'store_inventory_group_prices.id'
        )->count() != count($groupPriceTobeCheck)
    ) {
      clog('group_price_not_updating_issue', 'the group price could not be found ' . \json_encode('$data'));

      return $listener->response(400, 'the group price could not be found');
    }

    $resultToValidate = $this->groupPriceUsers->getModel()
      ->whereIn('store_inventory_group_price_id', $currentGroupPriceIds)
      ->where('user_id', $data['agent_user_id'])
      ->select(
        'user_id',
        'id',
        'group_price_apply_at',
        'store_inventory_group_price_id'
      )->get()->keyBy('store_inventory_group_price_id');

    foreach ($data['group_price'] as $gp) {

      if ($resultToValidate[$gp['current_group_price_id']]->group_price_apply_at) {
        clog('group_price_not_updating_issue', 'this group price has been scheduled ' . \json_encode('$data'));

        return $listener->response(400, 'this group price has been scheduled');
      }

      if ($gp['current_group_price_id'] != $gp['new_group_price_id']) {

        $temp = [
          'id' => $resultToValidate[$gp['current_group_price_id']]->id,
          'new_group_price_id' => $gp['new_group_price_id'],
        ];

        $tobeUpdate[] = '(' . implode(',', $temp) . ')';

      }
    }


    if (empty($tobeUpdate)) {
      clog('group_price_not_updating_issue','Cannot re-update with the same current group price. ' . \json_encode($tobeUpdate));
      return $listener->response(400, 'Cannot re-update with the same current group price');
    }

    $applyAt = Carbon::now()->addMinute($data['selected_schedule']);

    \DB::statement("
          UPDATE store_inventory_group_price_users AS invd SET
          new_store_inventory_group_price_id = d.new_group_price_id,
          group_price_apply_at = '" . $applyAt . "'
          FROM (VALUES
            " . implode(',', $tobeUpdate) . "
          ) AS d(id, new_group_price_id)
          WHERE d.id = invd.id 
       ");

    $formattedDate = $applyAt->format('d F Y H:i:s');

    $listener->pushQueue(new UpdateAgentPricingBlastNotification([
      'vendor_id' => $data['store_id'],
      'reflected_date' => $formattedDate,
      'agent_user_id' => $resultToValidate->pluck('user_id')->all(),
      'group_price_id' => array_pluck($data['group_price'], 'current_group_price_id')
    ]));

    return $listener->response(200, [
      'apply_at' => $formattedDate
    ]);

  }

}