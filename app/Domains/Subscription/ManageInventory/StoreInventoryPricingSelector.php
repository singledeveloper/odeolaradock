<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/13/17
 * Time: 7:51 PM
 */

namespace Odeo\Domains\Subscription\ManageInventory;

use Carbon\Carbon;
use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;

class StoreInventoryPricingSelector {

  public function __construct() {
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->storeInventoryDetails = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryDetailRepository::class);
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->storeInventoryGroupPriceUsers = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceUserRepository::class);
  }

  public function get(PipelineListener $listener, $data) {

    $response = [];

    $selectedGroupPriceId = $selectedGroupPriceType = null;

    $store = $this->stores->findById($data['store_id']);

    if (!$store) return $listener->response(400);

    if (isset($data['group_price']) && $data['group_price']) {

      $groupPrices = $this->storeInventories->getGroupPrice($data['store_id'], $data['service_detail_id']);


      if ($groupPrices->isEmpty()) {
        return $listener->response(204);
      }

      if (!isset($data['selected_group_price_id'])) {
        $selectedGroupPrice = $groupPrices->where('type', (function () use ($store) {
          return $store->plan_id == Plan::FREE ? InventoryGroupPrice::AGENT_DEFAULT : InventoryGroupPrice::MARKET_PLACE;
        })())->first();

        $selectedGroupPriceId = $selectedGroupPrice->id;
        $selectedGroupPriceType = $selectedGroupPrice->type;
      }

      foreach ($groupPrices as $group) {

        if ($group->name == null) {
          $group->name = trans('inventory/inventory_group_price.group_price_type_' . $group->type);
          $group->description = trans('inventory/inventory_group_price.group_price_description_' . $group->type);
        }

        if ($group->pricing_apply_at) {
          $group->pricing_apply_at = Carbon::parse($group->pricing_apply_at)->format('d F Y H:i:s');
        } else {
          unset($group->pricing_apply_at);
        }

      }

      $response['group_price'] = $groupPrices;
      $selectedGroupPriceId = $data['selected_group_price_id'] ?? $selectedGroupPriceId;
      $selectedGroupPriceType = $data['selected_group_price_type'] ?? $selectedGroupPriceType;

    } else {
      $selectedGroupPriceId = $data['selected_group_price_id'];
      $selectedGroupPriceType = $data['selected_group_price_type'];
    }

    $response['selected_group_price_id'] = $selectedGroupPriceId;
    $response['selected_group_price_type'] = $selectedGroupPriceType;

    /*if ($store->can_supply) $inventoryDetails = [];
    else*/ $inventoryDetails = $this->getPricing([
      'selected_group_price_id' => $selectedGroupPriceId,
      'selected_group_price_type' => $selectedGroupPriceType,
      'store_id' => $store->id,
      'service_detail_id' => $data['service_detail_id'],
      'plan_id' => $store->plan_id
    ]);

    $inventoriesResponse = [];

    foreach ($inventoryDetails as $detail) {

      $temp = [];

      $temp['id'] = $detail->group_price_detail_id;
      $temp['inventory_name'] = $detail->inventory_name;
      $temp['inventory_id'] = $detail->inventory_id;


      $temp['price'] = $this->currencyHelper->formatPrice($detail->sell_price);
      $temp['diff_price'] = $this->currencyHelper->formatPrice($detail->sell_price - $detail->base_price);

      if ($detail->sell_price != $detail->planned_price) {
        $temp['planned_price'] = $this->currencyHelper->formatPrice($detail->planned_price);
        $temp['planned_diff_price'] = $this->currencyHelper->formatPrice($detail->planned_price - $detail->base_price);
      }

      $temp['vendor'] = [
        'name' => $detail->vendor_name ?? 'ODEO',
        'price' => $this->currencyHelper->formatPrice($detail->base_price)
      ];

      $inventoriesResponse[] = $temp;

    }

    $response['inventories'] = $inventoriesResponse;

    if (empty($inventoriesResponse)) {
      //return $listener->response(204);
    }


    $response['is_admin_fee_price'] = in_array($data['service_detail_id'], ServiceDetail::groupPostpaid());

    $response['disabled_for_user_update'] = false;
    if ($selectedGroupPriceType != InventoryGroupPrice::MARKET_PLACE) {
      $response['disabled_for_user_update'] = !!$this->storeInventoryGroupPriceUsers->checkIsGroupPriceHasUserOnProgressUpdate(
        $selectedGroupPriceId
      );
    }


    return $listener->response(200, $response);
  }


  public function getPricing($data) {

    if ($data['plan_id'] == Plan::FREE) {

      return $this->storeInventories->getModel()
        ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
        ->join('store_inventory_vendors', 'store_inventory_details.store_inventory_vendor_id', '=', 'store_inventory_vendors.id')
        ->join('store_inventory_group_prices', 'store_inventories.id', '=', 'store_inventory_group_prices.store_inventory_id')
        ->join('store_inventory_group_price_details as my_price_details', function ($join) {
          $join->on('my_price_details.store_inventory_detail_id', '=', 'store_inventory_details.id');
          $join->on('my_price_details.store_inventory_group_price_id', '=', 'store_inventory_group_prices.id');
        })
        ->leftJoin('store_inventory_group_price_details as vendor_price_details', 'vendor_price_details.id', '=', 'store_inventory_details.vendor_group_price_detail_id')
        ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'store_inventory_details.inventory_id')
        ->join('stores', 'stores.id', '=', 'store_inventory_vendors.vendor_id')
        ->where('store_inventories.active', true)
        ->where('store_inventory_group_prices.id', $data['selected_group_price_id'])
        ->where('store_inventories.store_id', $data['store_id'])
        ->where('store_inventories.service_detail_id', $data['service_detail_id'])
        ->whereNotNull('vendor_price_details.sell_price')
        ->where('pulsa_odeos.status', Pulsa::STATUS_OK)
        ->orderBy('pulsa_odeos.operator_id', 'asc')
        ->orderBy('pulsa_odeos.category', 'asc')
        ->orderBy('pulsa_odeos.price', 'asc')
        ->select(
          'my_price_details.id as group_price_detail_id',
          'pulsa_odeos.id as inventory_id',
          'pulsa_odeos.name as inventory_name',
          'my_price_details.sell_price as sell_price',
          'my_price_details.planned_price as planned_price',
          'vendor_price_details.sell_price as base_price',
          'stores.name as vendor_name'
        )
        ->get();

    }

    $marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $serviceDetail = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $serviceDetail = $serviceDetail->findById($data['service_detail_id']);

    if ($data['plan_id'] != Plan::FREE && $data['selected_group_price_type'] == InventoryGroupPrice::MARKET_PLACE) {

      return $this->storeInventories->getModel()
        ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
        ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'store_inventory_details.inventory_id')
        ->where('store_inventories.store_id', $data['store_id'])
        ->where('store_inventories.service_detail_id', $data['service_detail_id'])
        ->where('store_inventories.active', true)
        ->where('pulsa_odeos.status', Pulsa::STATUS_OK)
        ->orderBy('pulsa_odeos.operator_id', 'asc')
        ->orderBy('pulsa_odeos.category', 'asc')
        ->orderBy('pulsa_odeos.price', 'asc')
        ->select(
          'pulsa_odeos.id as inventory_id',
          'pulsa_odeos.name as inventory_name',
          'pulsa_odeos.price as base_price'
        )
        ->get()->map(function ($item) use ($marginFormatter, $serviceDetail) {

          $odeoPrice = $marginFormatter->formatMargin($item->base_price, [
            'service_detail' => $serviceDetail
          ]);

          $item->base_price = $odeoPrice['merchant_price'];
          $item->sell_price = $item->planned_price = $odeoPrice['sale_price'];

          $item->group_price_detail_id = 0;

          return $item;
        });
    }


    return $this->storeInventories->getModel()
      ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_group_prices', 'store_inventories.id', '=', 'store_inventory_group_prices.store_inventory_id')
      ->join('store_inventory_group_price_details as my_price_details', function ($join) {
        $join->on('my_price_details.store_inventory_detail_id', '=', 'store_inventory_details.id');
        $join->on('my_price_details.store_inventory_group_price_id', '=', 'store_inventory_group_prices.id');
      })
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'store_inventory_details.inventory_id')
      ->where('store_inventories.active', true)
      ->where('pulsa_odeos.status', Pulsa::STATUS_OK)
      ->where('store_inventory_group_prices.id', $data['selected_group_price_id'])
      ->where('store_inventories.store_id', $data['store_id'])
      ->where('store_inventories.service_detail_id', $data['service_detail_id'])
      ->orderBy('pulsa_odeos.operator_id', 'asc')
      ->orderBy('pulsa_odeos.category', 'asc')
      ->orderBy('pulsa_odeos.price', 'asc')
      ->select(
        'my_price_details.id as group_price_detail_id',
        'pulsa_odeos.id as inventory_id',
        'pulsa_odeos.name as inventory_name',
        'my_price_details.sell_price',
        'my_price_details.planned_price',
        'pulsa_odeos.price as base_price'
      )
      ->get();
  }


}
