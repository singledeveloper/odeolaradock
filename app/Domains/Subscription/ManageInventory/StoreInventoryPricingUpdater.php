<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/15/17
 * Time: 10:51 PM
 */

namespace Odeo\Domains\Subscription\ManageInventory;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Constant\InventoryPricing;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Subscription\ManageInventory\Jobs\UpdateAgentPricingBlastNotification;

class StoreInventoryPricingUpdater {

  public function __construct() {

    $this->redis = Redis::connection();
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->storeInventoryGroupPrices = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository::class);

  }

  public function updateInventoryPrice(PipelineListener $listener, $data) {

    $redisNamespace = 'odeo_core:store_inventory_group_price_update_pricing_lock';

    if (!$this->redis->hsetnx($redisNamespace, 'group_price_id_' . $data['group_price_id'] . '_in_progress_lock', 1)) {
      return $listener->response(400, 'Update on progress');
    }

    if (!($groupPrice = $this->storeInventoryGroupPrices->findById($data['group_price_id']))) {
      return $listener->response(400, 'You have selected deleted group price');
    }

    if ($groupPrice->type == InventoryGroupPrice::MARKET_PLACE) {
      return $listener->response(400, 'Market Place Group price is not allow to update.');
    }

    if ($groupPrice->pricing_apply_at) {
      return $listener->response(400, 'Update on progress');
    }

    if (!in_array($data['selected_schedule'], array_keys(InventoryPricing::getUpdateTime()))) {
      return $listener->response(400, 'schedule has not been selected.');
    }

    if ($inventoryDetails = $this->storeInventories->getModel()
        ->join('store_inventory_group_prices', 'store_inventory_group_prices.store_inventory_id', '=', 'store_inventories.id')
        ->join('store_inventory_group_price_details', 'store_inventory_group_price_details.store_inventory_group_price_id', '=', 'store_inventory_group_prices.id')
        ->where('store_inventories.store_id', '=', $data['store_id'])
        ->where('store_inventories.service_detail_id', '=', $data['service_detail_id'])
        ->where('store_inventory_group_prices.id', '=', $data['group_price_id'])
        ->whereIn('store_inventory_group_price_details.id', array_pluck($data['to_update'], 'id'))
        ->select(
          'store_inventory_group_price_details.id'
        )->count() !== count($data['to_update'])
    ) {
      return $listener->response(400, 'Something when wrong, please try again later');
    }


    $inventoryDetailsForUpdate = [];

    foreach ($data['to_update'] as $key => $item) {

      if (!is_numeric($item['price']) || $item['price'] < 0) {
        return $listener->response(400, 'Something when wrong, please try again later');
      }

      $temp = [
        'id' => $item['id'],
        'planned_price' => $item['price']
      ];

      $inventoryDetailsForUpdate[] = '(' . implode(',', $temp) . ')';

    }

    $applyAt = Carbon::now()->addMinute($data['selected_schedule']);

    if (!empty($inventoryDetailsForUpdate)) {

      \DB::beginTransaction();

      try {

        \DB::statement("
          UPDATE store_inventory_group_price_details AS invd SET
          planned_price = d.planned_price
          FROM (VALUES
            " . implode(',', $inventoryDetailsForUpdate) . "
          ) AS d(id, planned_price)
          WHERE d.id = invd.id 
       ");

        $groupPrice->pricing_apply_at = $applyAt;

        $this->storeInventoryGroupPrices->save($groupPrice);

        \DB::commit();

      } catch (\Exception $exception) {

        $this->redis->hdel($redisNamespace, 'group_price_id_' . $data['group_price_id'] . '_in_progress_lock');

        \DB::rollback();

        return $listener->response(400, 'Something when wrong, please try again later');

      }

    }

    $formattedDate = $applyAt->format('d F Y H:i:s');

    $this->redis->hdel($redisNamespace, 'group_price_id_' . $data['group_price_id'] . '_in_progress_lock');

    $listener->pushQueue(new UpdateAgentPricingBlastNotification([
      'vendor_id' => $data['store_id'],
      'reflected_date' => $formattedDate,
      'group_price_id' => $data['group_price_id']
    ]));

    return $listener->response(200, [
      'apply_at' => $formattedDate
    ]);


  }


}