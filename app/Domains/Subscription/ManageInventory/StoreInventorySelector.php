<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/13/17
 * Time: 7:51 PM
 */

namespace Odeo\Domains\Subscription\ManageInventory;

use Carbon\Carbon;
use Odeo\Domains\Constant\InventoryStore;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryVendorRepository;

class StoreInventorySelector {

  public function __construct() {
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->storeParser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->pulsaOdeos = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->storeInventoryVendors = app()->make(StoreInventoryVendorRepository::class);
    $this->groupPriceUsers = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceUserRepository::class);
  }

  public function get(PipelineListener $listener, $data) {

    $store = $this->stores->findById($data['store_id']);

    if (!$store) return $listener->response(400);

    $vendor = $pricing = $results = [];
    $userGroupPriceWillUpdateData = [];

    $inventories = $this->getProductInventories($data);

    $status = 'active';

    if ($inventories->first() && !$inventories->first()->active) {
      return $listener->response(200, [
        'status' => 'disabled'
      ]);
    }

    if ($store->plan_id !== \Odeo\Domains\Constant\Plan::FREE) {

      $vendor[] = [
        'name' => $store->can_supply ? $store->name : 'ODEO',
        'store_id' => null,
      ];

    }

    if (!$inventories->isEmpty()) {

      $vendorIds = $this->storeInventoryVendors->getVendorIds($data)->pluck('vendor_id')->all();

      if (!empty($vendorIds)) {

        $userGroupPrice = $this->groupPriceUsers->getUserGroupPrice(
          $data['auth']['user_id'],
          $vendorIds,
          $data['service_detail_id']);

        if (($isUpdatingUserGroupPrice = $userGroupPrice->filter(function ($item) {
            $item->group_price_apply_at;
          })) && !$isUpdatingUserGroupPrice->isEmpty()
        ) {

          $userGroupPriceWillUpdateData = $this->storeInventories
            ->getGroupPricePricingByInventory($isUpdatingUserGroupPrice->pluck('store_inventory_group_price_id'))
            ->keyBy(function ($item) {
              return $item->store_inventory_group_price_id . '#' . $item->inventory_id;
            });

        }

        $results = $this->storeInventories->getMyVendorWithPriceLists([
          'store_id' => $vendorIds,
          'service_detail_id' => $data['service_detail_id'],
          'group_price_id' => $userGroupPrice ? $userGroupPrice->pluck('store_inventory_group_price_id')->all() : null
        ]);
      }

      foreach ($results as $temp) {

        if (!isset($vendor[$temp->vendor_store_id])) {

          $vendor[$temp->vendor_store_id] = [
            'name' => $temp->vendor_name,
            'store_id' => $temp->vendor_store_id,
            'vendor_pricing_apply_at' => $temp->group_price_apply_at,
          ];
        }

        $vendor[$temp->vendor_store_id][$temp->inventory_id] = [
          'vendor_price' => $temp->vendor_price,
          'vendor_planned_price' => $temp->vendor_planned_price,
          'vendor_group_price_detail_id' => $temp->inventory_group_price_detail_id,
          'vendor_group_price_id' => $temp->inventory_group_price_id
        ];

      }
    }

    $excludeInventoryIds = [];

    foreach ($vendor as $v) {

      $temp = [];

      foreach ($inventories as $inventory) {

        $temp['inventory_id'] = $inventory->inventory_id;

        if ($v['store_id'] === null) {

          $temp['price'] = $inventory->base_price;

          $temp['checked'] = $inventory->vendor_group_price_detail_id == null;

        } else {

          if (!isset($vendor[$v['store_id']][$inventory->inventory_id])) {
            if (!in_array($inventory->inventory_id, $excludeInventoryIds)) {
              $excludeInventoryIds[] = $inventory->inventory_id;
            }
            continue;
          }

          $item = $vendor[$v['store_id']][$inventory->inventory_id];

          $temp['price'] = $item['vendor_price'];
          $temp['vendor_price_id'] = $item['vendor_group_price_detail_id'];

          $temp['planned_price'] = null;

          if ($item['vendor_planned_price'] != $temp['price']) {
            $temp['planned_price'] = $item['vendor_planned_price'];
          } else if (isset($userGroupPriceWillUpdateData[$item['vendor_group_price_id'] . '#' . $inventory->inventory_id])) {
            $groupPriceData = $userGroupPriceWillUpdateData[$item['vendor_group_price_id'] . '#' . $inventory->inventory_id];
            $temp['planned_price'] = $groupPriceData->sell_price;
            $v['vendor_pricing_apply_at'] = $groupPriceData->group_price_apply_at;
          }

          $temp['checked'] = $inventory->vendor_group_price_detail_id == $item['vendor_group_price_detail_id'];
          $temp['planned_price'] = $temp['planned_price'] ? $this->currencyHelper->formatPrice($item['vendor_planned_price']) : $temp['planned_price'];

        }

        $temp['price'] = $this->currencyHelper->formatPrice($temp['price']);

        $v['inventories'][] = $temp;

      }

      $pricing[] = array_merge([
        'name' => $v['name'],
        'store_id' => $v['store_id'],
        'inventories' => isset($v['inventories']) ? $v['inventories'] : []
      ], (function () use ($v) {
        if (isset($v['vendor_pricing_apply_at'])) {
          return [
            'pricing_apply_at' => Carbon::parse($v['vendor_pricing_apply_at'])->format('d F Y H:i:s')
          ];
        }

        return [];

      })());

    }

    $availableVendorSlot = max(5 - count($pricing), 0);

    if ($inventories->isEmpty()) {
      $product = $this->getProductOnly($data);
    } else {
      $product = [];
      foreach ($inventories as $item) {
        if (in_array($item->inventory_id, $excludeInventoryIds)) continue;
        $product[] = [
          'inventory_name' => $item->inventory_name,
          'inventory_id' => $item->inventory_id,
          'inventory_detail_id' => $item->inventory_detail_id
        ];
      }
    }


    return $listener->response(200, [
      'status' => $status,
      'product' => $product,
      'pricing' => $pricing,
      'available_vendor_slot' => $availableVendorSlot,
      'message' => empty($pricing) ? 'Anda belum memiliki vendor. Tambah vendor untuk dapat berjualan.' : 'Centang sumber produk yang anda inginkan, tambah slot vendor baru untuk memperkaya variasi harga.'
    ]);
  }


  private function getProductOnly($data) {
    return $this->pulsaOdeos->getModel()
      ->where('pulsa_odeos.service_detail_id', $data['service_detail_id'])
      ->where('pulsa_odeos.status', Pulsa::STATUS_OK)
      ->whereNull('pulsa_odeos.owner_store_id')
      ->select(
        'pulsa_odeos.name as inventory_name',
        'pulsa_odeos.id as inventory_id'
      )
      ->orderBy('pulsa_odeos.operator_id', 'asc')
      ->orderBy('pulsa_odeos.category', 'asc')
      ->orderBy('pulsa_odeos.price', 'asc')
      ->get();
  }

  private function getProductInventories($data) {
    return $this->storeInventories->getModel()
      ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'store_inventory_details.inventory_id')
      ->where('store_id', $data['store_id'])
      ->where('store_inventories.service_detail_id', $data['service_detail_id'])
      ->where('pulsa_odeos.status', Pulsa::STATUS_OK)
      ->select(
        'pulsa_odeos.name as inventory_name',
        'pulsa_odeos.id as inventory_id',
        'pulsa_odeos.price as base_price',
        'store_inventory_details.id as inventory_detail_id',
        'store_inventory_details.vendor_group_price_detail_id',
        'store_inventories.active'
      )
      ->orderBy('pulsa_odeos.operator_id', 'asc')
      ->orderBy('pulsa_odeos.category', 'asc')
      ->orderBy('pulsa_odeos.price', 'asc')->get();
  }


}
