<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/19/17
 * Time: 10:04 PM
 */

namespace Odeo\Domains\Subscription\ManageInventory;

use Carbon\Carbon;
use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Subscription\ManageInventory\Jobs\PopulateStoreInventoryPricing;
use Odeo\Domains\Subscription\ManageInventory\Jobs\PopulateUserGroupPrice;

class StoreInventoryVendorRequester {

  private $serviceDetails, $storeInventoryDetails, $storeInventoryVendors, $stores;

  public function __construct() {
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->storeInventoryDetails = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryDetailRepository::class);
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->storeInventoryVendors = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryVendorRepository::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->groupPriceUsers = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceUserRepository::class);
    $this->groupPrices = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository::class);
  }

  public function addVendor(PipelineListener $listener, $data) {
    if ($data['store_id'] == $data['vendor_store_id']) {
      return $listener->response(400, 'You cannot add your own vendor');
    }

    $store = $this->stores->findById($data['store_id']);
    if ($store->can_supply) return $listener->response(400, 'Toko Anda tidak dapat menggunakan vendor.');

    $populateDetail = false;
    $userGroupPrice = null;
    $vendorStore = $this->stores->getModel()
      ->join('user_stores', 'user_stores.store_id', '=', 'stores.id')
      ->where('stores.id', $data['vendor_store_id'])
      ->where('user_stores.type', 'owner')
      ->select(
        'stores.id', 'stores.plan_id', 'user_id', 'stores.status', 'subdomain_name', 'can_supply'
      )->first();

    if (!in_array($vendorStore->status, StoreStatus::ACTIVE)) {
      return $listener->response(400, 'Unable to add ' . $vendorStore->subdomain_name . ' due to store inactivation');
    }

    if ($vendorStore->plan_id == Plan::FREE) {

      $vendorCount = $this->storeInventories->getModel()
        ->join('store_inventory_vendors', 'store_inventory_vendors.store_inventory_id', '=', 'store_inventories.id')
        ->where('store_id', $data['vendor_store_id'])
        ->where('service_detail_id', $data['service_detail_id'])
        ->select(['store_inventories.id'])
        ->count();

      if ($vendorCount == 0) {
        $serviceId = ServiceDetail::getServiceId($data['service_detail_id']);
        $name = title_case(str_replace('_', ' ', Service::getConstKeyByValue($serviceId)));
        return $listener->response(400, 'Unable to add ' . $vendorStore->subdomain_name . '. vendor cannot supply ' . $name);
      }
    }

    $result = $this->storeInventories->getModel()
      ->leftJoin('store_inventory_vendors', 'store_inventories.id', '=', 'store_inventory_vendors.store_inventory_id')
      ->leftJoin('store_inventory_details', 'store_inventories.id', '=', 'store_inventory_details.store_inventory_id')
      ->where('service_detail_id', '=', $data['service_detail_id'])
      ->where('store_inventories.store_id', '=', $data['store_id'])
      ->select(
        'store_inventories.id as store_inventory_id',
        'store_inventory_vendors.vendor_id as vendor_store_id',
        'store_inventory_details.store_inventory_id as store_inventory_id_from_detail'
      )
      ->get();

    if (!$result || !count($result)) {

      return $listener->response(400, 'An error occurred, please try again later');

    } else {

      if (!$result->where('vendor_store_id', $data['vendor_store_id'])->isEmpty()) {
        return $listener->response(400, 'You have added this vendor');
      }

      if ($result->where('store_inventory_id_from_detail', $result->first()->store_inventory_id)->isEmpty()) {
        $populateDetail = true;
      }

      $result = $result->first();

    }

    if ($this->storeInventoryVendors->isDownLine($data['service_detail_id'], $data['store_id'], $vendorStore->id)) {
      return $listener->response(400, title_case($vendorStore->subdomain_name) . ' vendor cannot be added.');
    }

    $vendor = $this->storeInventoryVendors->getNew();
    $vendor->store_inventory_id = $result->store_inventory_id;
    $vendor->vendor_id = $vendorStore->id;

    $this->storeInventoryVendors->save($vendor);

    if (!$vendorStore->can_supply) {
      $groupPriceType = $data['auth']['user_id'] == $vendorStore->user_id ? InventoryGroupPrice::SELF : InventoryGroupPrice::AGENT_DEFAULT;
    }
    else $groupPriceType = InventoryGroupPrice::CUSTOM;

    if ($populateDetail) {

      $inventoryDetailTobeInsert = [];
      $nowDateString = Carbon::now();
      $userGroupPrice = $this->groupPriceUsers->getUserGroupPrice($data['auth']['user_id'], $vendorStore->id, $data['service_detail_id'])->first();

      if ($vendorStore->can_supply && !$userGroupPrice) {
        return $listener->response(400, 'Anda harus menunggu vendor Anda melakukan setting harga untuk Anda.');
      }

      $userGroupPriceParams = [
        'store_id' => $vendorStore->id,
        'service_detail_id' => $data['service_detail_id'],
        'group_price_id' => $userGroupPrice ? $userGroupPrice->store_inventory_group_price_id : null,
        'group_price_type' => $groupPriceType
      ];

      $vendorInventories = $this->storeInventories->getStorePricing($userGroupPriceParams, [
        'store_inventory_details.inventory_id as inventory_id',
        'store_inventory_group_price_details.id as store_inventory_group_price_detail_id'
      ], false);

      foreach ($vendorInventories as $venInv) {
        $inventoryDetailTobeInsert[] = [
          'store_inventory_id' => $result->store_inventory_id,
          'inventory_id' => $venInv->inventory_id,
          'store_inventory_vendor_id' => $vendor->id,
          'vendor_group_price_detail_id' => $venInv->store_inventory_group_price_detail_id,
          'updated_at' => $nowDateString,
          'created_at' => $nowDateString
        ];
      }

      $this->storeInventoryDetails->saveBulk($inventoryDetailTobeInsert);

      dispatch(new PopulateStoreInventoryPricing([
        'store_id' => $data['store_id'],
        'store_inventory_ids' => [
          $result->store_inventory_id
        ],
        'store_plan_id' => $data['store']['plan_id'],
        'link_related_user_to_group_price' => true,
        'service_detail_id' => $data['service_detail_id']
      ]));

    }

    !$userGroupPrice && dispatch(new PopulateUserGroupPrice([
      'plan_id' => $vendorStore->plan_id,
      'user_id' => $data['auth']['user_id'],
      'store_id' => $vendorStore->id,
      'service_ids' => [
        ServiceDetail::getServiceId($data['service_detail_id'])
      ],
      'group_price_type' => $groupPriceType
    ]));


    return $listener->response(200);

  }

  public function replaceVendor(PipelineListener $listener, $data) {
    $tobeUpdate = [];

    if ($data['store_id'] == $data['vendor_store_id']) {
      return $listener->response(400, 'You cannot add your own vendor');
    } else if ($data['prev_vendor_store_id'] == $data['vendor_store_id']) {
      return $listener->response(400, 'You have to replace with different vendor');
    }

    $vendorStore = $this->stores->getModel()
      ->join('user_stores', 'user_stores.store_id', '=', 'stores.id')
      ->where('stores.id', $data['vendor_store_id'])
      ->where('user_stores.type', 'owner')
      ->select(
        'stores.id', 'stores.plan_id', 'user_id', 'status', 'subdomain_name'
      )->first();


    if (!in_array($vendorStore->status, StoreStatus::ACTIVE)) {
      return $listener->response(400, 'Unable to add ' . $vendorStore->subdomain_name . ' due to store inactivation');
    }

    if (!$vendorStore->can_supply) {
      $groupPriceType = $data['auth']['user_id'] == $vendorStore->user_id ? InventoryGroupPrice::SELF : InventoryGroupPrice::AGENT_DEFAULT;
    }
    else $groupPriceType = InventoryGroupPrice::CUSTOM;

    if ($vendorStore->plan_id == Plan::FREE) {
      $vendorCount = $this->storeInventories->getModel()
        ->join('store_inventory_vendors', 'store_inventory_vendors.store_inventory_id', '=', 'store_inventories.id')
        ->where('store_id', $data['vendor_store_id'])
        ->where('service_detail_id', $data['service_detail_id'])
        ->select(['store_inventories.id'])
        ->count();

      if ($vendorCount == 0) {
        $serviceId = ServiceDetail::getServiceId($data['service_detail_id']);
        $name = title_case(str_replace('_', ' ', Service::getConstKeyByValue($serviceId)));
        return $listener->response(400, 'Unable to add ' . $vendorStore->subdomain_name . '. vendor cannot supply ' . $name);
      }
    }

    $currentVendors = $this->storeInventories->getModel()
      ->join('store_inventory_vendors', 'store_inventory_vendors.store_inventory_id', '=', 'store_inventories.id')
      ->where('service_detail_id', $data['service_detail_id'])
      ->where('store_id', $data['store_id'])
      ->select(
        'store_inventory_vendors.vendor_id',
        'store_inventory_vendors.id'
      )
      ->get();

    $currentVendor = $currentVendors->first();

    if ($currentVendors->count() != 1) {
      return $listener->response(400);
    } else if ($currentVendor->vendor_id != $data['prev_vendor_store_id']) {
      return $listener->response(400);
    }

    if ($this->storeInventoryVendors->isDownLine($data['service_detail_id'], $data['store_id'], $vendorStore->id)) {
      return $listener->response(400, title_case($vendorStore->subdomain_name) . ' vendor cannot be added.');
    }

    $this->storeInventoryVendors->getModel()
      ->where('id', $currentVendor->id)
      ->update([
        'vendor_id' => $data['vendor_store_id']
      ]);

    $userGroupPrice = $this->groupPriceUsers->getUserGroupPrice($data['auth']['user_id'], $vendorStore->id, $data['service_detail_id'])->first();

    $vendorInventories = $this->storeInventories->getModel()
      ->join('store_inventory_details', 'store_inventories.id', '=', 'store_inventory_details.store_inventory_id')
      ->join('store_inventory_group_prices', 'store_inventory_group_prices.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_group_price_details as details', function ($join) {
        $join->on('details.store_inventory_detail_id', '=', 'store_inventory_details.id');
        $join->on('details.store_inventory_group_price_id', '=', 'store_inventory_group_prices.id');
      })
      ->where('store_id', $data['vendor_store_id'])
      ->where('service_detail_id', $data['service_detail_id']);

    if ($userGroupPrice) {
      $vendorInventories->where('store_inventory_group_prices.id', $userGroupPrice->store_inventory_group_price_id);
    } else {
      $vendorInventories->where('store_inventory_group_prices.type', $groupPriceType);
    }

    $vendorInventories = $vendorInventories->select(
      'inventory_id',
      'details.id'
    )->get();


    foreach ($vendorInventories as $inv) {
      $temp = [
        'inventory_id' => $inv->inventory_id,
        'vendor_group_price_detail_id' => $inv->id
      ];

      $tobeUpdate[] = '(' . implode(',', $temp) . ')';
    }

    \DB::statement("        
      WITH inventories AS (
        SELECT d.vendor_group_price_detail_id, store_inventory_details.id
        FROM
          store_inventories
          JOIN store_inventory_details ON store_inventories.id = store_inventory_details.store_inventory_id
          JOIN
          (VALUES
            " . implode(',', $tobeUpdate) . "
          ) AS D(inventory_id, vendor_group_price_detail_id) ON D.inventory_id = store_inventory_details.inventory_id
        WHERE 
          D.inventory_id = store_inventory_details.inventory_id AND
          store_inventories.store_id = " . $data['store_id'] . " AND
          store_inventories.service_detail_id = " . $data['service_detail_id'] . "    
      )
      UPDATE store_inventory_details AS invd
      SET
        vendor_group_price_detail_id = d.vendor_group_price_detail_id
      FROM
        inventories AS d
      WHERE d.id = invd.id
    ");

    dispatch(new PopulateUserGroupPrice([
      'plan_id' => $vendorStore->plan_id,
      'user_id' => $data['auth']['user_id'],
      'store_id' => $vendorStore->id,
      'service_ids' => [
        ServiceDetail::getServiceId($data['service_detail_id'])
      ],
      'group_price_type' => $groupPriceType
    ]));

    return $listener->response(200);

  }

  public function getVendorPricing(PipelineListener $listener, $data) {

    $serviceId = ServiceDetail::getServiceId($data['service_detail_id']);
    $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

    $vendorStore = $this->stores->getModel()
      ->join('user_stores', 'user_stores.store_id', '=', 'stores.id')
      ->where('subdomain_name', strtolower($data['vendor_code']))
      ->where('type', 'owner')
      ->whereIn("status", StoreStatus::ACTIVE)
      ->select(
        'user_stores.user_id', 'current_data',
        'stores.id', 'plan_id', 'name', 'can_supply'
      )
      ->first();

    if (!$vendorStore) {
      return $listener->response(400, 'Invalid vendor code');
    }

    if (array_search($data['service_detail_id'], \json_decode(app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class)
        ->currentData($vendorStore->plan_id, $vendorStore->current_data), true)) === false
    ) {
      $name = title_case(str_replace('_', ' ', Service::getConstKeyByValue($serviceId)));
      return $listener->response(400, 'This vendor cannot supply ' . $name);
    }

    $userGroupPrice = $this->groupPriceUsers->getUserGroupPrice($data['auth']['user_id'], $vendorStore->id, $data['service_detail_id'])->first();

    if ($vendorStore->can_supply && !$userGroupPrice) {
      return $listener->response(400, 'Anda harus menunggu vendor Anda melakukan setting harga untuk Anda.');
    }

    $userGroupPriceParams = [
      'store_id' => $vendorStore->id,
      'service_detail_id' => $data['service_detail_id'],
      'group_price_id' => $userGroupPrice ? $userGroupPrice->store_inventory_group_price_id : null
    ];
    if (!$vendorStore->can_supply) {
      $userGroupPriceParams['group_price_type'] = $data['auth']['user_id'] == $vendorStore->user_id ? InventoryGroupPrice::SELF : InventoryGroupPrice::AGENT_DEFAULT;
    }
    else $userGroupPriceParams['group_price_type'] = InventoryGroupPrice::CUSTOM;

    $inventoryDetails = $this->storeInventories->getStorePricing($userGroupPriceParams, [
      'pulsa_odeos.id as inventory_id',
      'pulsa_odeos.name as inventory_name',
      'store_inventory_group_price_details.sell_price as sell_price',
      'store_inventory_group_price_details.store_inventory_group_price_id'
    ], true)->transform(function ($item) use ($data, $currencyHelper) {

      $item->sell_price = $currencyHelper->formatPrice($item->sell_price);

      return $item;

    })->all();


    if (empty($inventoryDetails)) {
      return $listener->response(204, [], false, true);
    }

    return $listener->response(200, [
      'inventories' => $inventoryDetails,
      'vendor' => [
        'id' => $vendorStore->id,
        'name' => $vendorStore->name,
      ],
    ]);

  }

  public function removeVendor(PipelineListener $listener, $data) {

    $inventories = $this->storeInventories->getModel()
      ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_vendors', 'store_inventory_vendors.store_inventory_id', '=', 'store_inventories.id')
      ->where('service_detail_id', $data['service_detail_id'])
      ->where('store_id', $data['store_id'])
      ->where('vendor_id', $data['vendor_store_id'])
      ->select(
        'store_inventory_details.store_inventory_id',
        'store_inventory_vendor_id as vendor_reference_id',
        'store_inventory_vendors.id as vendor_id'
      )->get();

    if (!$inventories->filter(function ($item) {

      return $item->vendor_id == $item->vendor_reference_id;

    })->isEmpty()
    ) {
      return $listener->response(400, 'Could not remove vendor. vendor still been used');
    }

    $inventory = $inventories->first();

    if (!$inventory) {
      return $listener->response(400, 'Could not remove vendor. vendor not exists');
    }

    $this->storeInventoryVendors->getModel()
      ->where('store_inventory_id', '=', $inventory->store_inventory_id)
      ->where('id', '=', $inventory->vendor_id)
      ->where('vendor_id', '=', $data['vendor_store_id'])
      ->delete();

    return $listener->response(200);

  }


}
