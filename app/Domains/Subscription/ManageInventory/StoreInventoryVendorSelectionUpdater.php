<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/13/17
 * Time: 7:51 PM
 */

namespace Odeo\Domains\Subscription\ManageInventory;

use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Core\PipelineListener;

class StoreInventoryVendorSelectionUpdater {

  public function __construct() {
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->storeParser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->pulsaOdeos = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
  }

  public function saveSelectedVendorPrice(PipelineListener $listener, $data) {

    $vendorIds = array_unique(array_column($data['selected'], 'vendor_store_id'));
    $vendorIds = array_filter($vendorIds);

    $tobeUpdate = [];

    $storeInventoryVendorIds = $this->storeInventories
      ->getModel()
      ->select(
        'store_inventory_vendors.vendor_id as vendor_store_id',
        'store_inventory_vendors.id as store_inventory_vendor_id'
      )
      ->join('store_inventory_vendors', 'store_inventory_vendors.store_inventory_id', '=', 'store_inventories.id')
      ->where('store_inventories.service_detail_id', '=', $data['service_detail_id'])
      ->where('store_inventories.store_id', '=', $data['store_id'])
      ->get()->keyBy('vendor_store_id')->all();


    foreach ($data['selected'] as $key => $selected) {

      $selectedVendorId = $selected['vendor_store_id'];

      if ($selectedVendorId) {

        if (!isset($storeInventoryVendorIds[$selected['vendor_store_id']])) {
          return $listener->response(400);
        }

        $data['selected'][$key]['store_inventory_vendor_id'] = $storeInventoryVendorIds[$selectedVendorId]['store_inventory_vendor_id'];
      }
      else if ($data['store']['plan_id'] != Plan::FREE) {

        $data['selected'][$key]['vendor_price_id'] = null;
        $data['selected'][$key]['store_inventory_vendor_id'] = null;

      }
      else {
        return $listener->response(400);
      }
    }

    $storeInventoryGroupPriceIds = $this->storeInventories
      ->getModel()
      ->select(
        'store_inventory_group_price_details.id'
      )
      ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_group_price_details', 'store_inventory_group_price_details.store_inventory_detail_id', '=', 'store_inventory_details.id')
      ->where('store_inventories.service_detail_id', '=', $data['service_detail_id'])
      ->whereIn('store_inventories.store_id', $vendorIds)
      ->get()->pluck('id')->all();

    if (!empty(array_filter($data['selected'], function ($p) use ($storeInventoryGroupPriceIds) {

      return $p['store_inventory_vendor_id'] && !in_array($p['vendor_price_id'], $storeInventoryGroupPriceIds);

    }))
    ) {
      return $listener->response(400);
    }

    foreach ($data['selected'] as $key => $selected) {

      $temp = [
        'id' => (int)$selected['inventory_detail_id'],
        'vendor_price_id' => $selected['vendor_price_id'] ?? 'NULL',
        'store_inventory_vendor_id' => $selected['store_inventory_vendor_id'] ?? 'NULL'
      ];

      $tobeUpdate[] = '(' . implode(',', $temp) . ')';

    }


    !empty($tobeUpdate) && \DB::statement("
          UPDATE store_inventory_details AS invd SET
          vendor_group_price_detail_id = d.vendor_price_id,
          store_inventory_vendor_id = d.store_inventory_vendor_id
          FROM (VALUES
            (
              (SELECT id FROM store_inventory_details LIMIT 0), 
              (SELECT vendor_group_price_detail_id FROM store_inventory_details LIMIT 0), 
              (SELECT store_inventory_vendor_id FROM store_inventory_details  LIMIT 0)
            ),
            " . join(',', $tobeUpdate) . "
          ) AS d(id, vendor_price_id, store_inventory_vendor_id)
          WHERE d.id = invd.id 
       ");

    return $listener->response(200);

  }


}