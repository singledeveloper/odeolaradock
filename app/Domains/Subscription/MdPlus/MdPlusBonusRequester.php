<?php

namespace Odeo\Domains\Subscription\MdPlus;

use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\MdPlusStatus;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;

class MdPlusBonusRequester {

  public function __construct() {
    $this->mdPlus = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\StoreMdPlusRepository::class);
    $this->mdPlusPlan = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\MdPlusPlanRepository::class);
    $this->userReferral = app()->make(\Odeo\Domains\Marketing\Userreferral\Repository\UserReferralCodeRepository::class);
    $this->userStore = app()->make(\Odeo\Domains\Subscription\Repository\UserStoreRepository::class);
    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
  }

  public function referralBonus(PipelineListener $listener, $data) {
    $userReferral = $this->userReferral->findByAttributes('user_id', $data['user_id']);

    if ($userReferral) {
      $storeId = $userReferral->store_referred_id;
      $mdPlus = $this->mdPlus->findByAttributes('store_id', $storeId);

      if ($mdPlus && $mdPlus->status == MdPlusStatus::OK) {
        $referralUser = $userReferral->user;
        $referredUserId = $userReferral->user_referred_id;

        if ($referredUserId == $data['user_id']) return $listener->response(200);

        $mdPlusPlan = $this->mdPlusPlan->findById($data['md_plus_plan_id']);

        $this->cashInserter->clear();

        $this->cashInserter->add([
          'user_id' => $referredUserId,
          'trx_type' => TransactionType::MD_PLUS_REFERRAL_BONUS,
          'cash_type' => CashType::OCASH,
          'amount' => $mdPlusPlan->referral_bonus,
          'data' => json_encode([
            'from_user_id' => $data['user_id']
          ]),
        ]);

        $this->cashInserter->run();

        $this->notification->setup($referredUserId, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
        $this->notification->mdPlusReferralBonus(
          $this->currency->getFormattedOnly($mdPlusPlan->referralBonus),
          $referralUser->name,
          $mdPlusPlan->subscription_months
        );

        $listener->pushQueue($this->notification->queue());
      }
    }

    return $listener->response(200);
  }
}
