<?php

namespace Odeo\Domains\Subscription\MdPlus;

use Carbon\Carbon;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\MdPlusStatus;
use Odeo\Domains\Subscription\MdPlus\Jobs\GenerateMdPlusReferralBonus;

class MdPlusRequester {

  public function __construct() {
    $this->mdPlus = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\StoreMdPlusRepository::class);
    $this->mdPlusPlan = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\MdPlusPlanRepository::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->orderDetail = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\OrderDetailMdPlusRepository::class);
    $this->selector = app()->make(MdPlusSelector::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
  }

  public function create(PipelineListener $listener, $data) {
    $mdPlus = $this->mdPlus->findByAttributes('store_id', $data['store_id']);

    if (!$mdPlus) {
      $mdPlus = $this->mdPlus->getNew();
      $mdPlus->store_id = $data['store_id'];
      $mdPlus->md_plus_plan_id = $data['md_plus_plan_id'];
      $mdPlus->is_auto_renew = $data['is_auto_renew'];
      $mdPlus->status = MdPlusStatus::PENDING;

      $this->mdPlus->save($mdPlus);
    } else if ($mdPlus->status == MdPlusStatus::OK_FREE) {
      return $listener->response(400, 'Free MD Plus cannot be renewed.');
    }

    return $listener->response(201);
  }

  public function createFree(PipelineListener $listener, $data) {
    $store = $this->store->findById($data['store_id']);
    $mdPlus = $this->mdPlus->findByAttributes('store_id', $data['store_id']);

    if ($store->plan_id != Plan::FREE) {
      if (!$mdPlus) {
        $mdPlus = $this->mdPlus->getNew();
        $mdPlus->store_id = $store->id;
        $mdPlus->md_plus_plan_id = 3;
        $mdPlus->is_auto_renew = false;
      }
      $mdPlus->status = MdPlusStatus::OK_FREE;
      $mdPlus->expired_at = $store->renewal_at;

      $this->mdPlus->save($mdPlus);
    }

    return $listener->response(200);
  }

  public function verify(PipelineListener $listener, $data) {
    $mdPlus = $this->mdPlus->findByAttributes('store_id', $data['store_id']);

    if ($mdPlus) {
      $mdPlus->status = MdPlusStatus::OK;
      $mdPlus->md_plus_plan_id = $data['md_plus_plan_id'];
      $mdPlus->expired_at = $data['expired_at'];
      $mdPlus->is_auto_renew = $data['is_auto_renew'];

      $this->mdPlus->save($mdPlus);

      $mdPlus->load('store.owner');
      $userId = $mdPlus->store->owner[0]->id;

      $response['store_name'] = $mdPlus->store->name;
      $response['user_id'] = $userId;

      $this->notification->setup($userId, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
      $this->notification->mdPlus($mdPlus->store_id, $mdPlus->store->name, 
        $data['subscription_months'], $data['type'] == 'new');

      $listener->pushQueue($this->notification->queue());
      $listener->pushQueue(new GenerateMdPlusReferralBonus($userId, $data['md_plus_plan_id']));

      return $listener->response(200, $response);
    }

    return $listener->response(400, 'MD Plus is not active for this store.');
  }

  public function checkout(PipelineListener $listener, $data) {
    $mdPlus = $this->mdPlus->findByAttributes('store_id', $data['item']['item_detail']['store_id']);
    $mdPlusPlan = $this->mdPlusPlan->findById($data['item']['item_detail']['md_plus_plan_id']);

    if ($mdPlus) {
      $orderDetail = $this->orderDetail->getNew();
      $beginDate = isset($mdPlus->expired_at) ? Carbon::parse($mdPlus->expired_at)->startOfDay() : Carbon::today();

      $orderDetail->store_id = $mdPlus->store_id;
      $orderDetail->md_plus_plan_id = $mdPlusPlan->id;
      $orderDetail->order_detail_id = $data['order_detail']['id'];
      $orderDetail->type = $mdPlus->status == MdPlusStatus::PENDING ? 'new' : 'renew';
      $orderDetail->expired_at = $beginDate->addMonth($mdPlusPlan->subscription_months);
      $orderDetail->subscription_months = $mdPlusPlan->subscription_months;
      $orderDetail->is_auto_renew = $data['item']['item_detail']['is_auto_renew'];

      $this->orderDetail->save($orderDetail);
      return $listener->response(201);
    }
    return $listener->response(400, "Unable to checkout the MD Plus.");
  }

  public function toggleAutoRenew(PipelineListener $listener, $data) {
    $mdPlus = $this->mdPlus->findByAttributes('store_id', $data['store_id']);

    $mdPlus->is_auto_renew = !$mdPlus->is_auto_renew;

    $mdPlus->save();

    return $listener->response(200);
  }
}
