<?php

namespace Odeo\Domains\Subscription\MdPlus\Model;

use Odeo\Domains\Core\Entity;

class MdPlusPlan extends Entity {

  protected $dates = ['settlement_at'];

}