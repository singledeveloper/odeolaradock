<?php

namespace Odeo\Domains\Subscription\MdPlus\Model;

use Odeo\Domains\Core\Entity;

class OrderDetailMdPlus extends Entity {

  protected $table = 'order_detail_md_plus';
  
  protected $dates = ['expired_at'];

}