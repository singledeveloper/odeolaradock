<?php

namespace Odeo\Domains\Subscription\MdPlus\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Store;
use Odeo\Domains\Subscription\MdPlus\Model\MdPlusPlan;

class StoreMdPlus extends Entity {

  protected $table = 'store_md_plus';

  protected $dates = ['expired_at'];

  public function store() {
    return $this->belongsTo(Store::class);
  }

  public function plan() {
    return $this->belongsTo(MdPlusPlan::class, 'md_plus_plan_id');
  }

}