<?php

namespace Odeo\Domains\Subscription\MdPlus;

use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\MdPlusStatus;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;

class ReferralCashbackRequester {

  public function __construct() {
    $this->mdPlus = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\StoreMdPlusRepository::class);
    $this->userReferral = app()->make(\Odeo\Domains\Marketing\Userreferral\Repository\UserReferralCodeRepository::class);
    $this->orderCharges = app()->make(\Odeo\Domains\Order\Repository\OrderChargeRepository::class);
    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
  }
  
  public function insertCashback(PipelineListener $listener, $data) {

    if ($data['referral_cashback'] == 0) return $listener->response(200);

    $userId = $data['order']['user_id'];

    $userReferral = $this->userReferral->findByAttributes('user_id', $userId);
    
    $charge = $this->orderCharges->getNew();

    $charge->type = OrderCharge::REFERRAL_CASHBACK;
    $charge->group_type = $userReferral ? OrderCharge::GROUP_TYPE_CHARGE_TO_COMPANY : OrderCharge::GROUP_TYPE_COMPANY_PROFIT;
    $charge->amount = $data['referral_cashback'];
    $charge->order_id = $data['order_id'];
    
    if ($userReferral) {
      $mdPlus = $this->mdPlus->findByAttributes('store_id', $userReferral->store_referred_id);

      if ($mdPlus && in_array($mdPlus->status, [MdPlusStatus::OK, MdPlusStatus::OK_FREE])) {
        $this->cashInserter->clear();
        $this->cashInserter->add([
          'user_id' => $userReferral->user_referred_id,
          'store_id' => NULL,
          'trx_type' => TransactionType::REFERRAL_CASHBACK,
          'cash_type' => CashType::OCASH,
          'amount' => $data['referral_cashback'],
          'data' => json_encode([
            'order_id' => $data['order_id'],
            'from_user_id' => $userId,
            'settlement_at' => \Carbon\Carbon::now()->addDay()->toDateTimeString()
          ]),
          'settlement_at' => \Carbon\Carbon::now()->addDay()->toDateTimeString(),
        ]);
        $this->cashInserter->run();
      } else {
        $charge->group_type = OrderCharge::GROUP_TYPE_COMPANY_PROFIT;
      }

      $listener->addNext(new Task(ReferralCashbackUpdater::class, 'update', [
        'user_id' => $userId,
        'store_id' => $userReferral->store_referred_id,
        'amount' => $data['referral_cashback'],
        'service_id' => $data['service_id'],
        'is_claimed' => $charge->group_type == OrderCharge::GROUP_TYPE_CHARGE_TO_COMPANY
      ]));
    } 

    $this->orderCharges->save($charge);

    return $listener->response(200);
  }
  
}
