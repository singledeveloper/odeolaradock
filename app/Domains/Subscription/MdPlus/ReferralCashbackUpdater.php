<?php

namespace Odeo\Domains\Subscription\MdPlus;

use Odeo\Domains\Core\PipelineListener;

class ReferralCashbackUpdater {

  public function __construct() {
    $this->referralCashback = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\ReferralCashbackRepository::class);
  }
  
  public function update(PipelineListener $listener, $data) {

    $date = $data['date'] ?? \Carbon\Carbon::today()->toDateString();

    $referralCashback = $this->referralCashback->getCloneModel()->lockForUpdate()
      ->where('user_id', $data['user_id'])
      ->where('store_id', $data['store_id'])
      ->where('service_id', $data['service_id'])
      ->where('date', $date)
      ->first();
    
    if (!$referralCashback) {
      $referralCashback = $this->referralCashback->getNew();

      $referralCashback->date = $date;
      $referralCashback->user_id = $data['user_id'];
      $referralCashback->store_id = $data['store_id'];
      $referralCashback->service_id = $data['service_id'];
      $referralCashback->sales_quantity = 0;
      $referralCashback->claimed_amount = 0;
      $referralCashback->unclaimed_amount = 0;
    }

    $referralCashback->sales_quantity++;
    
    if ($data['is_claimed']) {
      $referralCashback->claimed_amount += $data['amount'];
    } else {
      $referralCashback->unclaimed_amount += $data['amount'];
    }

    $this->referralCashback->save($referralCashback);

    return $listener->response(200);
  }
  
}
