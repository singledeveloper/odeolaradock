<?php

namespace Odeo\Domains\Subscription\MdPlus\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\MdPlus\Model\MdPlusPlan;

class MdPlusPlanRepository extends Repository {

  public function __construct(MdPlusPlan $mdPlusPlan) {
    $this->model = $mdPlusPlan;
  }

  public function gets() {

    $filters = $this->getFilters();

    $query = $this->getCloneModel()->where('is_active', true)->orderBy('subscription_months', 'asc');

    return $this->getResult($query);
  }
}