<?php

namespace Odeo\Domains\Subscription\MdPlus\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\MdPlus\Model\OrderDetailMdPlus;

class OrderDetailMdPlusRepository extends Repository {

  public function __construct(OrderDetailMdPlus $orderDetailMdPlus) {
    $this->model = $orderDetailMdPlus;
  }

}