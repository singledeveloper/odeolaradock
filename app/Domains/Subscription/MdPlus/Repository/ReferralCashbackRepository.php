<?php

namespace Odeo\Domains\Subscription\MdPlus\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\MdPlus\Model\ReferralCashback;

class ReferralCashbackRepository extends Repository {

  public function __construct(ReferralCashback $referralCashback) {
    $this->model = $referralCashback;
  }

  public function getByDateAndUser($dates, $userIds) {
    $query = $this->model->selectRaw('user_id, date, sum(claimed_amount) as total_claimed, sum(unclaimed_amount) as total_unclaimed,
      sum(sales_quantity) as total_sales')->whereIn('date', $dates)
      ->groupBy('user_id', 'date');

    if (is_array($userIds)) {
      return $query->whereIn('user_id', $userIds)->get();
    } else {
      return $query->where('user_id', $userIds)->get();
    }
  }

  public function getByDateAndStore($dates, $storeIds) {
    $query = $this->model->selectRaw('store_id, date, sum(claimed_amount) as total_claimed, sum(unclaimed_amount) as total_unclaimed,
      sum(sales_quantity) as total_sales')->whereIn('date', $dates)
      ->groupBy('store_id', 'date');

    if (is_array($storeIds)) {
      return $query->whereIn('store_id', $storeIds)->get();
    } else {
      return $query->where('store_id', $storeIds)->get();
    }
  }
}