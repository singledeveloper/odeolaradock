<?php

namespace Odeo\Domains\Subscription\MdPlus\Scheduler;

use Odeo\Domains\Subscription\MdPlus\Jobs\RenewMdPlus;

class AutoRenewScheduler {

  private $mdPlus;

  public function __construct() {
    $this->mdPlus = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\StoreMdPlusRepository::class);
  }

  public function run() {
    foreach($this->mdPlus->getRenewable() as $item) {
      $userStores = app()->make(\Odeo\Domains\Subscription\Repository\UserStoreRepository::class);

      $storeOwner = $userStores->findSingleOwner($item->store_id);
      dispatch(new RenewMdPlus($storeOwner->user_id, $item->store_id, $item->md_plus_plan_id));
    }
  }

}
