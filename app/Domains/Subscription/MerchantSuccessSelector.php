<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/21/16
 * Time: 11:42 PM
 */

namespace Odeo\Domains\Subscription;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Transaction\Helper\Currency;


class MerchantSuccessSelector implements SelectorListener {

  private $userStore, $store, $parser, $users, $storeManager, $cashManager, $service, $storeInv;
  private $services;

  public function __construct() {
    $this->userStore = app()->make(\Odeo\Domains\Subscription\Repository\UserStoreRepository::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->parser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->service = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    //$this->storeInv = app()->make(\Odeo\Domains\Subscription\Repository\StoreInventoryRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    $store = [];

    $store["store_id"] = $item->id;
    $store["store_name"] = $item->name;
    $store["is_upload_logo"] = $item->logo_path ? true : false;

    if ($repository->hasExpand('plan') && $plan = $item->plan) {
      $store["plan"] = [
        "id" => $item->plan_id,
        "name" => $plan->name,
        "color" => $plan->color
      ];
    } else if ($item->plan_id) $store['plan_id'] = $item->plan_id;

    if ($item->subdomain_name) {
      $store["subdomain_name"] = ($item->subdomain_name != null ? ($item->subdomain_name . (app()->environment('production') ? ".odeo.co.id" : '.' . env('APP_STAGING_BASE_URL'))) : '');
      $store["domain_link"] = "http://" . $store["subdomain_name"];
    }
    if ($item->domain_name) {
      $store["domain_name"] = $item->domain_name;
      $store["domain_link"] = "http://" . $store["domain_name"];
    }
    if ($item->renewal_at) $store["renewal_at"] = strtotime($item->renewal_at);

    $store["status"] = $this->parser->status($item);
    $store["status_message"] = $this->parser->statusMessage($store["status"]);

    if ($item->renewal_at && $repository->hasExpand('plan')) $store["warranty_expired_at"] = strtotime($item->renewal_at . " +" . ($item->plan->minimal_months * 30) . " day");
    if (isset($item->created_at) && $item->created_at) $store['established_at'] = $item->created_at->toDateTimeString();
    if ($repository->hasExpand('invitator')) $store['invitator'] =  isset($item->network->referredStore->name) ? $item->network->referredStore->name : '';

    if ($repository->hasExpand('store_inventories')) {

      $servicesTemp = [];
      foreach ($this->services as $data) {
        $servicesTemp[$data->service->name][] = $data;
      }

      $storeInventories = $this->storeInv->findByStoreId($item->id);

      $inventoriesTemp = [];
      foreach ($storeInventories as $data) {
        $inventoriesTemp[$data->store_id][$data->service_detail_id] = $data;
      }

      $item["current_data"] = json_decode($this->parser->currentData($item->plan_id, $item->current_data));

      $storeInventories = [];
      foreach ($item["current_data"] as $serviceName => $serviceDetailId) {
        $inventories = [];
        if ($serviceDetailId != "0") {
          $inventory = [];
          foreach ($servicesTemp[$serviceName] as $output) {
            if ($output->id == $serviceDetailId) {
              $inventory_detail = [];
              $inventory_detail["markup_type"] = $output->margin_type;
              $inventory_detail["max_discount"] = ($output->default_discount) ? $output->default_discount : 0;

              if (!isset($inventoriesTemp[$item["id"]][$output->id])) {
                $inventory_detail["current_discount"] = $inventory_detail["max_discount"];
              } else {
                $discount = $inventoriesTemp[$item["id"]][$output->id]->discount;
                $inventory_detail["current_discount"] = $discount ? $discount : 0;
              }
              $inventory = $inventory_detail;
              break;
            }
          }
          $inventories['providers'] = $inventory;
          $inventories['type'] = $serviceName;
          $inventories['service_type'] = $serviceName;
          $storeInventories[] = $inventories;
        }
      }
      $store['store_inventories'] = $storeInventories;
    }

    return $store;
  }

  public function _extends($data, Repository $repository) {
    $this->cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $this->storeManager = app()->make(\Odeo\Domains\Subscription\Helper\StoreManager::class);

    if ($store_ids = $repository->beginExtend($data, "store_id")) {
      if ($repository->hasExpand('deposit') || $repository->hasExpand('locked_deposit')) {
        $lockeds = null;
        $result = $this->cashManager->getStoreDepositBalance($store_ids, Currency::IDR, $lockeds);
        if ($repository->hasExpand('deposit')) $repository->addExtend('deposit', $result);
        if ($repository->hasExpand('locked_deposit')) $repository->addExtend('locked_deposit', $lockeds);
      }

      if ($repository->hasExpand('owner'))
        $repository->addExtend('owner', $this->storeManager->getOwner($store_ids));

    }

    return $repository->finalizeExtend($data);
  }

  public function getMerchantStore(PipelineListener $listener, $data) {
    $this->store->normalizeFilters($data);

    $stores = [];

    $this->services = $this->service->getDetails();

    foreach ($this->store->get() as $item) {
      $stores[] = $this->_transforms($item, $this->store);
    }

    if (sizeof($stores) > 0)
      return $listener->response(200, array_merge(
        ["stores" => $this->_extends($stores, $this->store)],
        $this->store->getPagination()
      ));
    return $listener->response(204);
  }


}