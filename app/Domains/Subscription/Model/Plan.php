<?php

namespace Odeo\Domains\Subscription\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;

class Plan extends Entity {

  use SoftDeletes;

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
  
  public $additionalAttributes = ['sub_name', 'description', 'features', 'business_opportunity'];
}
