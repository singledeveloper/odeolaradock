<?php

namespace Odeo\Domains\Subscription\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Channel\Model\StoreMdr;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Core\Entity;
use Odeo\Domains\Marketing\Model\StoreRevenue;
use Odeo\Domains\Network\Model\NetworkTree;
use Odeo\Domains\Subscription\DistributionChannel\Model\StoreDistributionChannel;
use Odeo\Domains\Subscription\ManageInventory\Model\StoreInventory;
use Odeo\Domains\Subscription\MdPlus\Model\StoreMdPlus;

class Store extends Entity {

  protected $dates = ['renewal_at'];

  protected $fillable = ['plan_id', 'name', 'subdomain_name', 'domain_name'];

  protected $hidden = ['updated_at'];

  public function store(array $data) {
    $this->fill($data);
    $this->status = StoreStatus::PENDING;
  }

  public function plan() {
    return $this->belongsTo(Plan::class);
  }

  public function storeAccount() {
    return $this->hasOne(StoreAccount::class);
  }

  public function network() {
    return $this->hasOne(NetworkTree::class);
  }

  public function storeInventories() {
    return $this->hasMany(StoreInventory::class);
  }

  public function userStores() {
    return $this->hasMany(UserStore::class);
  }

  public function owner() {
    return $this->belongsToMany(User::class, 'user_stores')->wherePivot('type', 'owner');
  }

  public function mdr() {
    return $this->hasOne(StoreMdr::class);
  }

  public function revenues() {
    return $this->hasMany(StoreRevenue::class);
  }

  public function distribution() {
    return $this->hasOne(StoreDistributionChannel::class);
  }

  public function mdPlus() {
    return $this->hasOne(StoreMdPlus::class);
  }
}
