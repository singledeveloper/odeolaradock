<?php

namespace Odeo\Domains\Subscription\Model;

use Odeo\Domains\Core\Entity;

class Warranty extends Entity
{
    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['created_at', 'updated_at'];
   
    public $timestamps = true;
    
    public function store() {
        return $this->belongsTo(Store::class);
    }
}
