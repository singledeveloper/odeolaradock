<?php

namespace Odeo\Domains\Subscription;

use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Helper\Currency;

class PlanSelector implements SelectorListener {

  private $plans, $currency, $stores, $lang;

  public function __construct() {
    $this->plans = app()->make(\Odeo\Domains\Subscription\Repository\PlanRepository::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->lang = app('translator');
  }

  public function _transforms($item, Repository $repository) {

    $plan = [];

    $plan["plan_id"] = $item->id;
    $plan["plan_name"] = $item->name;
    $plan["plan_sub_name"] = ($this->lang->has("plan.sub_name_of_id_" . $item->id) ? trans("plan.sub_name_of_id_" . $item->id) : "");
    $plan["plan_color"] = $item->color;

    $plan["minimum_subscription_in_months"] = $item->minimal_months;

    $plan["description"] = ($this->lang->has("plan.description_of_id_" . $item->id) ? trans("plan.description_of_id_" . $item->id) : "");

    $plan["monthly_price"] = $this->currency->formatPrice($item->cost_per_month, Currency::IDR, true);
    $plan["daily_price"] = $this->currency->formatPrice(ceil($item->cost_per_month / 30));

    $plan["monthly_price_before"] = $this->currency->formatPrice($item->cost_per_month_before, Currency::IDR, true);
    $plan["daily_price_before"] = $this->currency->formatPrice(ceil($item->cost_per_month_before / 30));

    if ($plan['monthly_price']['amount'] == 0) $plan['monthly_price']['formatted_amount'] = trans('misc.free');

    $plan["advance_features"] = $plan["basic_features"] = [];

    foreach (Plan::getAvailableServices($item->id) as $output) {
      $serviceName = strtolower(Service::getConstKeyByValue($output));
      $plan["basic_features"][] = trans('plan.' . $serviceName . '_inventory');
    }

    $plan["advance_features"] = ($this->lang->has("plan.advance_features_id_" . $item->id) ?
      trans('plan.advance_features_id_' . $item->id) : []);

    $plan["business_opportunity"] = ($this->lang->has("plan.bo_of_id_" . $item->id) ?
      trans("plan.bo_of_id_" . $item->id) : []);

    if ($item->id) {
      $availableServices = Plan::getAvailableServices($item->id);
      foreach ($availableServices as $output) {
        $serviceName = strtolower(Service::getConstKeyByValue($output));
        if (in_array($serviceName, ['pln_postpaid', 'pulsa_postpaid'])) continue;
        $plan["features"][] = $serviceName . '_inventory';
      }
    }

    $plan["business_opportunity"] = ($this->lang->has("plan.bo_of_id_" . $item->id) ?
      (["title" => trans("plan.bo_title"), "kinds" => trans("plan.bo_of_id_" . $item->id)]) : "");


    return $plan;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getPlan(PipelineListener $listener, $data) {
    $plans = [];

    $difference = 0;
    if (isset($data['store_id']) && $store = $this->stores->findById($data['store_id'])) {
      $store->load('plan');
      $data['current_cost'] = $store->plan->cost_per_month;
      if ($store->renewal_at != null) {
        $difference = ceil(floor((strtotime($store->renewal_at) - time()) / (60 * 60 * 24)) /
          (30 * $store->plan->minimal_months) * $store->plan->cost_per_month);
      }
    }
    if ($difference > 0) $data['current_cost_difference'] = $difference;

    $this->plans->normalizeFilters($data);


    foreach ($this->plans->get() as $item) {

      $plans[] = $this->_transforms($item, $this->plans);


    }
    if (sizeof($plans) > 0) {
      $result['plans'] = $this->_extends($plans, $this->plans);
      if (isset($data['current_cost'])) {
        $result['tnc'] = [
          ['1_text' => trans('plan.tnc_upgrade_plan_1')],
          ['1_text' => trans('plan.tnc_upgrade_plan_2')],
          ['1_text' => trans('plan.tnc_upgrade_plan_3')]
        ];
      } else {
        $result['tnc'] = [
          ['1_text' => trans('plan.tnc_plan_1')],
          ['1_text' => trans('plan.tnc_plan_2')],
          ['1_text' => trans('plan.tnc_plan_3')]
        ];
      }
      return $listener->response(200, array_merge($result, $this->plans->getPagination()));
    }
    return $listener->response(204, ["plans" => []]);
  }
}

