<?php

namespace Odeo\Domains\Subscription\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\Model\OrderDetailPlan;

class OrderDetailPlanRepository extends Repository
{

  public function __construct(OrderDetailPlan $orderDetailPlan) {
    $this->model = $orderDetailPlan;
  }

  public function findByOrderDetailId($order_detail_id) {
    return $this->model
      ->where('order_detail_id', $order_detail_id)
      ->first();
  }
  
  public function findByStoreId($storeId, $type = '') {
    $query = $this->model->where('store_id', $storeId);
    if ($type != '') $query = $query->where('type', $type);
    
    return $query->orderBy('id', 'DESC')->first();
  }
}
