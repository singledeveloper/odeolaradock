<?php

namespace Odeo\Domains\Subscription\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\Model\OrderDetailWarranty;

class OrderDetailWarrantyRepository extends Repository
{

  public function __construct(OrderDetailWarranty $orderDetailW) {
    $this->model = $orderDetailW;
  }

  public function findByOrderDetailId($order_detail_id) {
    return $this->model
      ->where('order_detail_id', $order_detail_id)
      ->first();
  }

}
