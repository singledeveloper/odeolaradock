<?php

namespace Odeo\Domains\Subscription\Repository;

use Illuminate\Http\Request;
use Odeo\Domains\Constant\Header;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\Model\UserStore;

class UserStoreRepository extends Repository {

  public function __construct(UserStore $userStore) {
    $this->model = $userStore;
  }

  public function get() {
    $filters = $this->getFilters();
    $query = $this->model->join('stores', 'user_stores.store_id', '=', 'stores.id');

    if ($this->hasExpand('plan')) $query = $query->with('plan');
    if (isset($filters["type"])) $query = $query->where('type', $filters["type"]);
    if ($this->disableOldAndroidVersion()) {
      $query = $query->where('stores.plan_id', '<>', Plan::FREE);
    }

    $userId = getUserId($filters);
    if (!isAdmin($filters) && $userId) {
      $query = $query->where(function ($query) use ($userId, $filters) {
        $query->where('user_id', '=', $userId);
        if (isset($filters["is_active"]) && $filters["is_active"]) {
          $query->where("status", "<>", StoreStatus::CANCELLED);
        }
      });
    }

    return $this->getResult($query->orderBy('store_id'));
  }

  public function getHustlers() {
    $filters = $this->getFilters();
    $query = $this->model->join('stores', 'user_stores.store_id', '=', 'stores.id');

    if ($this->hasExpand('plan')) $query = $query->with('plan');
    if (isset($filters["type"])) $query = $query->where('type', $filters["type"]);
    if ($this->disableOldAndroidVersion()) {
      $query = $query->where('stores.plan_id', '<>', Plan::FREE);
    }

    $userId = getUserId($filters);
    if (!isAdmin($filters) && $userId) {
      $tempQuery = $this->getCloneModel();

      $storeNetwork = $tempQuery
        ->select(\DB::raw('string_agg(network_trees.hustler_store_ids, \',\') as hustler_store_ids'))
        ->from('network_trees')
        ->join('user_stores', 'network_trees.store_id', '=', 'user_stores.store_id')
        ->join('stores', 'network_trees.store_id', '=', 'stores.id')
        ->where('stores.status', '<>', StoreStatus::CANCELLED)
        ->where('user_stores.user_id', '=', $userId)->first();

      $hustlerStoreIds = array_filter(explode(',', $storeNetwork->hustler_store_ids));

      if (empty($hustlerStoreIds)) return [];

      $this->addFilter('hustler_store_ids', $hustlerStoreIds);

      $query = $query->whereIn('store_id', $hustlerStoreIds)
        ->where('stores.status', '<>', StoreStatus::CANCELLED)
        ->where('user_stores.user_id', '<>', $userId);
    }

    return $this->getResult($query->orderBy('store_id'));
  }

  public function getGrandHustlers() {
    $filters = $this->getFilters();
    $query = $this->model->join('stores', 'user_stores.store_id', '=', 'stores.id');

    if ($this->hasExpand('plan')) $query = $query->with('plan');
    if (isset($filters["type"])) $query = $query->where('type', $filters["type"]);

    if ($this->disableOldAndroidVersion()) {
      $query = $query->where('stores.plan_id', '<>', Plan::FREE);
    }

    $userId = getUserId($filters);
    if (!isAdmin($filters) && $userId) {
      $tempQuery = $this->getCloneModel();

      $storeNetwork = $tempQuery
        ->select(\DB::raw('string_agg(network_trees.grand_hustler_store_ids, \',\') as grand_hustler_store_ids'))
        ->from('network_trees')
        ->join('user_stores', 'network_trees.store_id', '=', 'user_stores.store_id')
        ->join('stores', 'network_trees.store_id', '=', 'stores.id')
        ->where('stores.status', '<>', StoreStatus::CANCELLED)
        ->where('user_stores.user_id', '=', $userId)->first();

      $grandHustlerStoreIds = array_filter(explode(',', $storeNetwork->grand_hustler_store_ids));

      if (empty($grandHustlerStoreIds)) return [];

      $this->addFilter('grand_hustler_store_ids', $grandHustlerStoreIds);

      $query = $query->whereIn('store_id', $grandHustlerStoreIds)
        ->where('stores.status', '<>', StoreStatus::CANCELLED)
        ->where('user_stores.user_id', '<>', $userId);
    }

    return $this->getResult($query->orderBy('store_id'));
  }

  public function getTeams() {
    $filters = $this->getFilters();
    $query = $this->model->join('stores', 'user_stores.store_id', '=', 'stores.id');

    if ($this->hasExpand('plan')) $query = $query->with('plan');
    if (isset($filters["type"])) $query = $query->where('type', $filters["type"]);

    $userId = getUserId($filters);
    if (!isAdmin($filters) && $userId) {
      $tempQuery = $this->getCloneModel();

      $storeNetwork = $tempQuery
        ->select(\DB::raw('string_agg(network_trees.hustler_store_ids, \',\') as hustler_store_ids,
          string_agg(network_trees.grand_hustler_store_ids, \',\') as grand_hustler_store_ids,
          string_agg(network_trees.team_store_ids, \',\') as team_store_ids'))
        ->from('network_trees')
        ->join('user_stores', 'network_trees.store_id', '=', 'user_stores.store_id')
        ->join('stores', 'network_trees.store_id', '=', 'stores.id')
        ->where('stores.status', '<>', StoreStatus::CANCELLED)
        ->where('user_stores.user_id', '=', $userId)->first();

      $hustlerStoreIds = array_filter(explode(',', $storeNetwork->hustler_store_ids));
      $grandHustlerStoreIds = array_filter(explode(',', $storeNetwork->grand_hustler_store_ids));
      $teamStoreIds = array_filter(explode(',', $storeNetwork->team_store_ids));

      if (empty($teamStoreIds)) return [];

      $query = $query->whereIn('store_id', $teamStoreIds)
        ->whereNotIn('store_id', $hustlerStoreIds)
        ->whereNotIn('store_id', $grandHustlerStoreIds)
        ->where('stores.status', '<>', StoreStatus::CANCELLED)
        ->where('user_stores.user_id', '<>', $userId);
    }

    return $this->getResult($query->orderBy('store_id'));
  }

  public function checkCredibilityAllowMentor($storeId, $userId) {
    return $this->model
      ->join('stores', 'stores.id', '=', 'user_stores.store_id')
      ->where('user_id', $userId)
      ->whereIn('status', StoreStatus::ACTIVE)
      ->whereExists(function($query) use ($storeId) {
        $query
          ->selectRaw('1')
          ->from('network_trees')
          ->where('store_id', $storeId)
          ->whereRaw("user_stores.store_id::varchar(255) = any(regexp_split_to_array(referred_store_ids, ','))");
      })->first();
  }

  public function checkCredibility($storeId, $userId, $role = null) {
    return $this->model
      ->where("store_id", $storeId)
      ->where("user_id", $userId)
      ->where(function ($query) use ($role) {
        $query->where('type', 'owner');
        if ($role) {
          $query->orWhere('type', $role);
        }
      })->first();
  }

  public function findOwner($storeIds) {
    $query = $this->model->with("user")->where("type", "owner");

    if (is_array($storeIds)) $query = $query->whereIn("store_id", $storeIds);
    else $query = $query->where("store_id", $storeIds);

    return $query->get();
  }

  public function findSingleOwner($storeId) {
    return $this->model->where('type', 'owner')->where('store_id', $storeId)->first();
  }

  public function findByStoreId($storeId) {
    return $this->model->where("store_id", $storeId)->get();
  }

  public function hasFreeStore($userId) {
    return $this->model->join('stores', 'stores.id', '=', 'user_stores.store_id')
      ->where('user_id', $userId)
      ->where('type', 'owner')
      ->where('stores.plan_id', Plan::FREE)
      ->whereIn('status', StoreStatus::ACTIVE)
      ->select(
        'stores.id'
      )
      ->count();
  }

  public function hasPayedStore($userId) {
    return $this->model->join('stores', 'stores.id', '=', 'user_stores.store_id')
      ->where('user_id', $userId)
      ->where('type', 'owner')
      ->where('stores.plan_id', '<>', Plan::FREE)
      ->whereIn('status', StoreStatus::ACTIVE)
      ->select(
        'stores.id'
      )
      ->count();
  }

  public function findSupplyStore($userId) {
    return $this->model->join('stores', 'stores.id', '=', 'user_stores.store_id')
      ->where('user_id', $userId)
      ->where('type', 'owner')
      ->where('stores.can_supply', true)
      ->whereIn('status', StoreStatus::ACTIVE)
      ->select(
        'stores.id'
      )
      ->first();
  }

  private function disableOldAndroidVersion() {
    $version = app()->make(Request::class)->header(Header::ANDROID_HEADER_VER);
    return strlen($version) > 0 && substr($version, 0, 1) < '2';
  }

  public function getFirstActiveStore($userId) {
    return $this->model->join('stores', 'stores.id', '=', 'user_stores.store_id')
      ->where('user_id', $userId)
      ->where('type', 'owner')
      ->whereIn('status', StoreStatus::ACTIVE)
      ->orderBy('stores.plan_id', 'desc')
      ->select(
        'stores.id'
      )->first();
  }

  public function findOwnerByStoreId($storeId) {
    return $this->model->where('store_id', $storeId)->where('type', 'owner')->first();
  }
}
