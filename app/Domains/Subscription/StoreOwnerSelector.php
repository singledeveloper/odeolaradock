<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/24/17
 * Time: 5:27 PM
 */

namespace Odeo\Domains\Subscription;

use Odeo\Domains\Core\PipelineListener;

class StoreOwnerSelector {

  public function getStoreOwnerInformation(PipelineListener $listener, $data) {

    $storeManager = app()->make(\Odeo\Domains\Subscription\Helper\StoreManager::class);

    $ownerInformation = $storeManager->getOwner($data['store_id'])[$data['store_id']];
    $ownerInformation['user_id'] = $ownerInformation['id'];
    unset($ownerInformation['id']);

    return $listener->response(200, $ownerInformation);

  }
  
}