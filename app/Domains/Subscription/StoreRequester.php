<?php

namespace Odeo\Domains\Subscription;

use Odeo\Domains\Constant\ChannelConfig;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;

class StoreRequester {

  public function __construct() {
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->userStore = app()->make(\Odeo\Domains\Subscription\Repository\UserStoreRepository::class);
    $this->parser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);
    $this->selector = app()->make(\Odeo\Domains\Subscription\StoreSelector::class);
    $this->orderDetail = app()->make(\Odeo\Domains\Subscription\Repository\OrderDetailPlanRepository::class);
  }

  public function create(PipelineListener $listener, $data) {

    if (app()->environment() == 'production') {
      if ($data['plan_id'] == Plan::FREE && $this->userStore->hasFreeStore($data['auth']['user_id'])) {
        return $listener->response(400, 'Anda hanya boleh memiliki 1 store FREE');
      }
    }

    $store = $this->store->getNew();
    $store->store($data);
    $store->can_supply = false;
    $store->current_data = $this->parser->currentData($data["plan_id"]);
    $this->store->save($store);

    $userStore = $this->userStore->getNew();
    $userStore->storeData(isset($data['user_id']) ? $data['user_id'] : $data['auth']['user_id'], $store->id, 'owner');
    $this->userStore->save($userStore);

    return $listener->response(201, $this->selector->_transforms($store, $this->store));
  }

  public function changePlan(PipelineListener $listener, $data) {

    $store = $this->store->findById($data["store_id"]);

    if ($store->status == StoreStatus::PENDING) {
      return $listener->response(400, 'errors.store_not_verified');
    }

    if ($store->plan_id == $data['plan_id']) {
      return $listener->response(200, $this->selector->_transforms($store, $this->store));
    }

    $store->status = StoreStatus::WAIT_FOR_UPGRADE_VERIFY;
    $this->store->save($store);

    return $listener->response(200);
  }

  public function renew(PipelineListener $listener, $data) {

    $store = $this->store->findById($data["store_id"]);

    if ($store->status == StoreStatus::OK) {
      $store->status = StoreStatus::WAIT_FOR_RENEWAL_VERIFY;
    } else if ($store->status == StoreStatus::EXPIRED) {
      $store->status = StoreStatus::WAIT_FOR_EXP_RENEWAL_VERIFY;
    }
    $this->store->save($store);

    return $listener->response(200, ['plan_id' => $store->plan_id]);
  }

  public function terminate(PipelineListener $listener, $data) {
    $store = $this->store->findById($data["store_id"]);

    if ($store->status != StoreStatus::OK) {
      return $listener->response(400, 'errors.store_not_verified');
    }

    $this->store->delete($store);

    return $listener->response(200);
  }

  public function rollback(PipelineListener $listener, $data) {
    $store = $this->store->findById($data["store_id"]);

    if ($store->status == StoreStatus::PENDING) $store->status = StoreStatus::CANCELLED;
    else if (StoreStatus::isActive($store->status)) $store->status = StoreStatus::OK;
    else if ($store->status == StoreStatus::WAIT_FOR_EXP_RENEWAL_VERIFY) $store->status = StoreStatus::EXPIRED;

    $this->store->save($store);
    return $listener->response(200);
  }

  public function checkout(PipelineListener $listener, $data) {
    $storeId = $data['item']['item_detail']['store_id'];

    if ($store = $this->store->findById($storeId)) {

      $orderDetail = $this->orderDetail->getNew();
      $orderDetail->store_id = $storeId;
      $orderDetail->plan_id = $data['item']['inventory_id'];
      $orderDetail->order_detail_id = $data['order_detail']['id'];

      if ($store->status == StoreStatus::PENDING) {
        $orderDetail->type = 'new';
      } else if ($store->status == StoreStatus::WAIT_FOR_UPGRADE_VERIFY) {
        $orderDetail->type = 'upgrade';
      } else if ($store->status == StoreStatus::WAIT_FOR_RENEWAL_VERIFY || $store->status == StoreStatus::WAIT_FOR_EXP_RENEWAL_VERIFY)
        $orderDetail->type = 'renewal';
      else {
        $listener->response(400, "Invalid store status for checkout.");
      }

      $this->orderDetail->save($orderDetail);

      if ($orderDetail->plan_id == Plan::FREE && $orderDetail->type == 'new') {
        $listener->addNext(new Task(StoreVerificator::class, 'verifyStoreFree', [
          'store_id' => $storeId
        ]));
      }

      return $listener->response(201);
    }
    return $listener->response(400, "Unable to checkout the store.");
  }
}
