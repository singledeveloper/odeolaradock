<?php

namespace Odeo\Domains\Subscription;

use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Agent\Repository\AgentPriorityRepository;
use Odeo\Domains\Constant\AssetAws;
use Odeo\Domains\Constant\ChannelConfig;
use Odeo\Domains\Constant\MdPlusStatus;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Inventory\Repository\ServiceRepository;
use Odeo\Domains\Subscription\Helper\StoreParser;
use Odeo\Domains\Subscription\Repository\StoreRepository;
use Odeo\Domains\Subscription\Repository\UserStoreRepository;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Constant\CashType;
use Carbon\Carbon;

class StoreSelector implements SelectorListener {

  private $userStore, $store, $parser, $users, $service, $storeManager, $cashManager, $agentPriorities ;

  public function __construct() {
    $this->userStore = app()->make(UserStoreRepository::class);
    $this->store = app()->make(StoreRepository::class);
    $this->parser = app()->make(StoreParser::class);
    $this->users = app()->make(UserRepository::class);
    $this->service = app()->make(ServiceRepository::class);
    $this->agentPriorities = app()->make(AgentPriorityRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    $store = [];
    if ($item->id) $store["store_id"] = $item->id;
    if ($item->user_id) $store["user_id"] = $item->user_id;

    if ($repository->hasExpand('plan') && $plan = $item->plan) {
      $store["plan"] = [
        "id" => $item->plan_id,
        "name" => $plan->name,
        "color" => $plan->color
      ];
    } else if ($item->plan_id) $store['plan_id'] = $item->plan_id;

    if ($item->name) $store["store_name"] = $item->name;
    if ($item->subdomain_name || $item->domain_name) {
      $store["domain_name"] = $this->parser->domainName($item);
      $store["domain_link"] = "http://" . $store["domain_name"];
    }
    if ($item->subdomain_name) {
      $store["referral_code"] = strtoupper($item->subdomain_name);
    }

    if (isNewDesign()) {
      $logoPath = AssetAws::getAssetPath(AssetAws::BUSINESS_LOGO, AssetAws::DEFAULT_STORE_LOGO);
      $store["logo_path"] = $logoPath;
    } else if ($repository->hasField("logo_path")) {
      $store = array_merge($store, parseFilePath($item->logo_path, "logo"));
    }

    if ($repository->hasField("logo_no_odeo_path")) $store = array_merge($store, parseFilePath($item->logo_no_odeo, "logo_no_odeo"));
    if ($repository->hasField("favicon_path")) $store = array_merge($store, parseFilePath($item->favicon_path, "favicon"));
    if ($repository->hasField("opay_logo_path")) $store = array_merge($store, parseFilePath($item->opay_logo_path, "opay_logo"));

    if ($repository->hasExpand('current_data') && $item->plan_id) {
      $overwriteCurrentData = isset($repository->getFilters()['current_data_set_default']);
      $store["current_data"] = json_decode($this->parser->currentData($item->plan_id, $item->current_data, $overwriteCurrentData, $item->can_supply));
    }

    if ($item->renewal_at) $store["renewal_at"] = strtotime($item->renewal_at);

    if ($item->status) {
      $store["status_code"] = $item->status;
      $store["status"] = $this->parser->status($item);
      $store["status_message"] = $this->parser->statusMessage($store["status"]);
      $store["is_active"] = StoreStatus::isActive($item->status);
    }

    if ($repository->hasExpand('account')) {
      $account = $item->storeAccount;
      $store["accounts"] = [
        "facebook" => isset($account->facebook) ? $account->facebook : "",
        "twitter" => isset($account->twitter) ? $account->twitter : "",
        "instagram" => isset($account->instagram) ? $account->instagram : "",
        "path" => isset($account->path) ? $account->path : "",
        "google_plus" => isset($account->google_plus) ? $account->google_plus : "",
        "linkedin" => isset($account->linkedin) ? $account->linkedin : "",
        "telephone" => isset($account->telephone) ? $account->telephone : ""
      ];
    }

    if ($repository->hasExpand('invitator') && $item->network) {
      $invName = $this->parser->domainName($item->network->referredStore);
      if ($invName != "") {
        $store['invitator'] = [
          "store_name" => $item->network->referredStore->name,
          "domain_name" => $invName,
          "domain_link" => "http://" . $invName
        ];
      }
    }

    if ($repository->hasExpand('network_status') && $item->network) {
      $store['is_network_enable'] = $item->network->is_network_enable;
    }
    if ($repository->hasExpand('md_plus')) {
      if ($item->mdPlus) {
        switch ($item->mdPlus->status) {
          case MdPlusStatus::OK:
          case MdPlusStatus::OK_FREE:
            $status = 'ACTIVE';
            break;
          default:
            $status = 'INACTIVE';
            break;
        }
        $store['md_plus'] = [
          'status' => $status,
          'expired_at' => $item->mdPlus->expired_at ? $item->mdPlus->expired_at->format('Y-m-d') : null
        ];
      } else {
        $store['md_plus'] = [
          'status' => 'INACTIVE',
          'expired_at' => null
        ];
      }
    }
    if ($item->renewal_at && $repository->hasExpand('plan')) {
      $store["warranty_expired_at"] = strtotime($item->renewal_at . " +" . ($item->plan->minimal_months * 30) . " day");
    }
    if (isset($item->created_at) && $item->created_at) $store['established_at'] = $item->created_at->toDateTimeString();

    if (in_array($item->id, ChannelConfig::COMPANY_STORE_IDS)) {
      $store['opay_enabled'] = true;
    }

    $store['can_supply'] = $item->can_supply;

    return $store;
  }

  public function _extends($data, Repository $repository) {
    $this->cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $this->storeManager = app()->make(\Odeo\Domains\Subscription\Helper\StoreManager::class);
    $this->revenueManager = app()->make(\Odeo\Domains\Marketing\Helper\RevenueManager::class);

    if ($store_ids = $repository->beginExtend($data, "store_id")) {
      if ($repository->hasExpand('deposit') || $repository->hasExpand('locked_deposit')) {
        $lockeds = null;
        $result = $this->cashManager->getStoreDepositBalance($store_ids, Currency::IDR, $lockeds);
        if ($repository->hasExpand('deposit')) $repository->addExtend('deposit', $result);
        if ($repository->hasExpand('locked_deposit')) $repository->addExtend('locked_deposit', $lockeds);
      }
//      if ($repository->hasExpand('inventory')) {
//        $repository->addExtend('inventory', $this->storeManager->getStoreInventories($store_ids));
//      }
      if ($repository->hasExpand('orush')) {
        $repository->addExtend('orush', $this->cashManager->getStoreBalance($store_ids, CashType::ORUSH, Currency::IDR));
      }
      if ($repository->hasExpand('oads')) {
        $repository->addExtend('oads', $this->cashManager->getStoreBalance($store_ids, CashType::OADS, Currency::IDR));
      }
      if ($repository->hasExpand('revenue') || $repository->hasExpand('profit') || $repository->hasExpand('order_count')) {
        $result = $this->revenueManager->getStoreRevenue($store_ids, Carbon::now(), Carbon::now(), $profits, $orderCounts);
        if ($repository->hasExpand('revenue')) $repository->addExtend('revenue', $result);
        if ($repository->hasExpand('profit')) $repository->addExtend('profit', $profits);
        if ($repository->hasExpand('order_count')) $repository->addExtend('order_count', $orderCounts);
      }
      if ($repository->hasExpand('owner')) {
        $repository->addExtend('owner', $this->storeManager->getOwner($store_ids));
      }
      if ($repository->hasExpand('order')) {
        $repository->addExtend('order', app()->make(\Odeo\Domains\Order\Helper\OrderManager::class)->getByStoreId($store_ids));
      }
    }

    return $repository->finalizeExtend($data);
  }


  public function getUserStore(PipelineListener $listener, $data) {
    $this->userStore->setSimplePaginate(true);
    $this->userStore->normalizeFilters($data);

    $stores = [];

    foreach ($this->userStore->get() as $item) {
      $stores[] = $this->_transforms($item, $this->userStore);
    }

    $stores = $this->_extractStoreRole($stores, $data);

    if (sizeof($stores) > 0) {
      return $listener->response(200, array_merge(
        ["stores" => $this->_extends($stores, $this->userStore)],
        $this->userStore->getPagination()
      ));
    }
    return $listener->response(204, ["stores" => []]);
  }

  public function getHustlerStore(PipelineListener $listener, $data) {
    $this->userStore->normalizeFilters($data);
    $this->userStore->setSimplePaginate(true);

    $stores = [];

    foreach ($this->userStore->getHustlers() as $item) {
      $stores[] = $this->_transforms($item, $this->userStore);
    }

    $stores = $this->_extractStoreRole($stores, $data);

    if (sizeof($stores) > 0) {
      return $listener->response(200, array_merge(
        ["stores" => $this->_extends($stores, $this->userStore)],
        $this->userStore->getPagination()
      ));
    }
    return $listener->response(204, ["stores" => []]);
  }

  public function getTeamStore(PipelineListener $listener, $data) {
    $this->userStore->normalizeFilters($data);
    $this->userStore->setSimplePaginate(true);

    $stores = [];

    foreach ($this->userStore->getGrandHustlers() as $item) {
      $stores[] = $this->_transforms($item, $this->userStore);
    }

    $stores = $this->_extractStoreRole($stores, $data);

    if (sizeof($stores) > 0) {
      return $listener->response(200, array_merge(
        ["stores" => $this->_extends($stores, $this->userStore)],
        $this->userStore->getPagination()
      ));
    }
    return $listener->response(204, ["stores" => []]);
  }

  public function getStore(PipelineListener $listener, $data) {
    $this->store->normalizeFilters($data);
    $this->store->setSimplePaginate(true);

    $stores = [];

    foreach ($this->store->get() as $item) {
      $stores[] = $this->_transforms($item, $this->store);
    }

    if (sizeof($stores) > 0)
      return $listener->response(200, array_merge(
        ["stores" => $this->_extends($stores, $this->store)],
        $this->store->getPagination()
      ));
    return $listener->response(204, ["stores" => []]);
  }

  public function getUsersHaveNoStore(PipelineListener $listener, $data) {
    $this->store->normalizeFilters($data);

    $stores = [];

    foreach ($this->store->getUsersHaveNoStore() as $item) {
      $stores[] = $this->_transforms($item, $this->store);
    }

    if (sizeof($stores) > 0)
      return $listener->response(200, array_merge(
        ["stores" => $this->_extends($stores, $this->store)]
      ));
    return $listener->response(204, ["stores" => []]);
  }

  public function getStoreDetail(PipelineListener $listener, $data) {
    $this->store->normalizeFilters($data);
    $filters = $this->store->getFilters();
    if ($item = $this->store->findById($data["store_id"], $filters["fields"])) {
      $store = $this->_transforms($item, $this->store);
      if (isset($data['auth']['user_id'])) {
        $owners = $item->owner->pluck('id')->all();
        if (in_array($data['auth']['user_id'], $owners)) {
          $store['owner'] = 'self';
        } else {
          $store['owner'] = 'hustler';
        }
      }

      return $listener->response(200, $this->_extends($store, $this->store));
    }
    return $listener->response(204);
  }


  private function _extractStoreRole($stores, $data) {
    $filters = $this->userStore->getFilters();

    $extracted = [];

    foreach ($stores as $item) {
      if ($item['user_id'] == $data['auth']['user_id']) {
        $item['owner'] = 'self';
        $extracted['self'][] = $item;
      } else if (isset($filters['hustler_store_ids']) && in_array($item['store_id'], $filters['hustler_store_ids'])) {
        $item['owner'] = 'hustler';
        $extracted['hustler'][] = $item;
      } else if (isset($filters['grand_hustler_store_ids']) && in_array($item['store_id'], $filters['grand_hustler_store_ids'])) {
        $item['owner'] = 'grand hustler';
        $extracted['grand_hustler'][] = $item;
      } else {
        $item['owner'] = 'team';
        $extracted['team'][] = $item;
      }
    }
    return array_merge(isset($extracted['self']) ? $extracted['self'] : [], isset($extracted['hustler']) ? $extracted['hustler'] : [],
      isset($extracted['grand_hustler']) ? $extracted['grand_hustler'] : [], isset($extracted['team']) ? $extracted['team'] : []);
  }

  public function getFirstActiveStore(PipelineListener $listener, $data) {
    $store = $this->userStore->getFirstActiveStore($data['auth']['user_id']);

    return $listener->response(200, [
      'store_id' => $store ? $store->id : null
    ]);
  }

  public function getAvailableStores(PipelineListener $listener, $data) {
    $defaultStoreID = $data['store_id'];

    $stores = $this->store->getAvailableStores($data['auth']['user_id'], $data['service_id']);

    $selectedStore = $stores->where('is_selected', true)->first();
    $selectedId = $selectedStore ? $selectedStore->id : $defaultStoreID;

    $stores = $stores->map(function ($item) use ($defaultStoreID, &$selectedId) {

      $currentData = json_decode($this->parser->currentData($item->plan_id, $item->current_data));
      $serviceName = $item->service_name;

      $item = $this->_transforms($item, $this->store);
      $item["is_default_store"] = false;
      $item["is_disabled"] = false;
      if (
        !StoreStatus::isActive($item['status_code']) ||
        !isset($currentData) ||
        !isset($currentData->$serviceName) ||
        $currentData->$serviceName <= 0
      ) {
        $item["is_disabled"] = true;
        if ($selectedId == $item['store_id']) {
          $selectedId = $defaultStoreID;
        }
      }

      return $item;
    })->toArray();

    $defaultStore = $this->store->findById($defaultStoreID);
    $defaultStore = $this->_transforms($defaultStore, $this->store);
    $defaultStore["is_default_store"] = true;
    $defaultStore["is_disabled"] = false;
    $defaultStore = $this->changeDefaultStoreDetail($defaultStore);
    array_unshift($stores, $defaultStore);

    return $listener->response(200, [
      'stores' => $stores,
      'selected_id' => $selectedId
    ]);
  }

  public function getSupplierStores(PipelineListener $listener, $data) {
    $stores = [];

    foreach ($this->store->getSupplierStores() as $item) {
      $stores[] = [
        'id' => $item->id,
        'name' => $item->name
      ];
    }

    if (sizeof($stores) > 0)
      return $listener->response(200, array_merge(
        ["stores" => $this->_extends($stores, $this->store)]
      ));
    return $listener->response(204, ["stores" => []]);
  }

  public function getAllMasterStores(PipelineListener $listener, $data) {
    $defaultStoreId = StoreStatus::getStoreDefault();
    $storeIds = $this->store->getAllMasterStoreIds(getUserId())->toArray();
    $storeIds[] = $defaultStoreId;

    $stores = $this->store->findByStoreIds($storeIds);

    $stores = $stores->map(function ($item) use ($defaultStoreId) {
      $item = $this->_transforms($item, $this->store);
      $item["is_default_store"] = $item["store_id"] == $defaultStoreId;

      if ($item['is_default_store']) {
        $item = $this->changeDefaultStoreDetail($item);
      }

      return $item;
    });

    return $listener->response(200, [
      'stores' => $this->reorderStores($stores)
    ]);
  }

  private function reorderStores($stores) {
    return $stores->sortByDesc('is_default_store')->values()->all();
  }

  private function changeDefaultStoreDetail($store) {
    $store['store_name'] = 'Top';
    $store['logo_path'] = AssetAws::getAssetPath(AssetAws::BUSINESS_LOGO, AssetAws::TOP_STORE_LOGO);;
    return $store;
  }

  public function getUserMasterId($serviceId) {
    $user = $this->users->findById(getUserId());
    $userMasterId = $user->purchase_preferred_store_id ?? null;

    // If user has no master / the master is default store, return null
    if (!$userMasterId || $userMasterId == StoreStatus::STORE_DEFAULT) {
      return null;
    }

    // parse the data of user's master and get the service item
    $currentData = null;
    if ($store = $this->store->findById($userMasterId)) {
      $currentData = json_decode($this->parser->currentData($store->plan_id, $store->current_data));
    }
    $service = $this->service->findById($serviceId);
    $serviceName = $service->name ?? '';

    // if master's data is empty / master is not selling that item, return null
    if (!isset($currentData) || !isset($currentData->$serviceName) || $currentData->$serviceName <= 0) {
      return null;
    }

    // return user master's store id, if all above passed
    return $userMasterId;
  }

  public function getSellerUserId($storeId) {
    if (!$storeId) {
      return null;
    }
    
    $userStore = $this->userStore->findOwnerByStoreId($storeId);
    if (!$userStore) {
      return null;
    }
    return $userStore->user_id;
  }

  public function isStoreCanSupply($storeId) {
    if (!$storeId) {
      return null;
    }
    
    return $this->store->findById($storeId)->can_supply;
  }

}
