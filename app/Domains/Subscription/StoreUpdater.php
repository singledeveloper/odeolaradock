<?php

namespace Odeo\Domains\Subscription;

use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\StoreStatus;
use Illuminate\Support\Facades\File;

class StoreUpdater {

  public function __construct() {
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    //$this->storeInventory = app()->make(\Odeo\Domains\Subscription\Repository\StoreInventoryRepository::class);
    $this->storeAccount = app()->make(\Odeo\Domains\Subscription\Repository\StoreAccountRepository::class);
    $this->service = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->plan = app()->make(\Odeo\Domains\Subscription\Repository\PlanRepository::class);
    $this->selector = app()->make(\Odeo\Domains\Subscription\StoreSelector::class);
  }

  public function update(PipelineListener $listener, $data) {
    $store = $this->store->findById($data["store_id"]);

    if (!StoreStatus::isActive($store->status)) {
      return $listener->response(400, 'errors.store_not_verified');
    }

    $availableServices = Plan::getAvailableServices($store->plan_id);
    $currentPlanData = json_decode($store->current_data, true);

    if (isset($data["service_detail_id"])) $inven = $this->service->findDetail($data["service_detail_id"]);

    if (isset($inven) && (
        ($inven->service->name == 'flight' && !in_array(Service::FLIGHT, $availableServices)) ||
        ($inven->service->name == 'hotel' && !in_array(Service::HOTEL, $availableServices)))
    ) {
      return $listener->response(400, trans('errors.invalid_plan'));
    }

    if (isset($data['store_name'])) $store->name = $data['store_name'];
    if (isset($data['logo'])) $store->logo_path = $data['logo'];
    if (isset($data['logo_no_odeo'])) $store->logo_no_odeo_path = $data['logo_no_odeo'];
    if (isset($data['opay_logo'])) $store->opay_logo_path = $data['opay_logo'];
    if (isset($data['favicon'])) $store->favicon_path = $data['favicon'];

    if (isset($data['phone_number'])) $data['telephone'] = $data['phone_number']; // TODO might be deleted later
    if (isset($data['linked_in'])) $data['linkedin'] = $data['linked_in']; // TODO might be deleted later

    if (isset($inven)) {
      if (isset($data['active'])) {
        $currentPlanData[$inven->service->name] = ($data['active']) ? $inven->id : -1;
        $store->current_data = json_encode($currentPlanData);
      }

      $storeInventory = $this->storeInventory->findByStoreService($data['store_id'], $data['service_detail_id']);
      if (!$storeInventory) {
        $storeInventory = $this->storeInventory->getNew();
        $storeInventory->store_id = $data['store_id'];
        $storeInventory->service_detail_id = $data['service_detail_id'];
      }

      if (isset($data['discount'])) {
        $serviceDetail = $storeInventory->serviceDetail;
        if ($data['discount'] >= abs($serviceDetail->margin)) {
          return $listener->response(400, "Discount must be lower than " . abs($serviceDetail->margin));
        }
        $storeInventory->discount = $serviceDetail->margin_type == 'value' ? ceil($data['discount']) : $data['discount'];
        if (!isset($storeInventory->margin)) {
          $storeInventory->margin_type = $serviceDetail->margin_type;
          $storeInventory->margin = $serviceDetail->default_hustler_margin;
        }
      }

      $this->storeInventory->save($storeInventory);
    }

    if (isset($data['facebook']) || isset($data['twitter']) || isset($data['instagram']) || isset($data['path']) ||
      isset($data['google_plus']) || isset($data['linkedin']) || isset($data['telephone'])
    ) {

      $storeAccount = $this->storeAccount->findByStoreId($data['store_id']);

      if (!$storeAccount) {
        $storeAccount = $this->storeAccount->getNew();
        $storeAccount->store_id = $data['store_id'];
      }

      $storeAccount->fill($data);
      $this->storeAccount->save($storeAccount);
    }

    $this->store->save($store);

    return $listener->response(200, $this->selector->_transforms($store, $this->store));
  }

  private function response($method, $base_uri, $uri, $data, $headers = []) {
    $client = new \GuzzleHttp\Client([
      'base_uri' => $base_uri,
      'headers' => $headers,
      'http_errors' => false,
      'timeout' => 3600.0,
      'verify' => false,
    ]);

    if ($method == "GET") $response = $client->request('GET', $uri . (sizeof($data) > 0 ? ("?" . http_build_query($data)) : ""));
    else $response = $client->request($method, $uri, ["json" => $data]);

    return [$response->getStatusCode(), json_decode($response->getBody()->getContents())];
  }

  public function updateDomain(PipelineListener $listener, $data) {
    $store = $this->store->findById($data["store_id"]);
    if ($store->domain_name != null) {
      return $listener->response(400, "This store already had a domain name.");
    }

    $godaddy_header = ["Authorization" => "sso-key dKP1J9vvKraM_VJuVJoju6C9tjKKPzKcW1f:Dpp6YJvyAvN8DCgYtPFf9N"];
    list($status, $r) = $this->response('GET', 'https://api.godaddy.com/v1/', 'domains/' . $data["domain_name"], [], $godaddy_header);

    $error = "";
    if ($status == 200) {
      list($status, $r) = $this->response('POST', 'https://api.cloudflare.com/client/v4/', 'zones', ["name" => $data["domain_name"]], [
        "X-Auth-Key" => "ff34a6213e330546585830bc9c59a98c50352",
        "X-Auth-Email" => "gerard.edwin.t@gmail.com",
        "Content-Type" => "application/json"
      ]);

      if ($status == 200 && (isset($r->result) && isset($r->result->name_servers))) {
        $overwrite = [];
        foreach ($r->result->name_servers as $item) {
          $overwrite[] = ["type" => "NS", "name" => "@", "data" => $item, "ttl" => 600];
        }

        list($status, $r) = $this->response('PUT', 'https://api.godaddy.com/v1/', 'domains/' . $data["domain_name"] . '/records', $overwrite, $godaddy_header);

        if ($status == 200) {
          File::append("../domain_waiting.txt", "\n" . $data["domain_name"]);

          $store->domain_name = $data["domain_name"];
          $this->store->save($store);

          return $listener->response(200, "Please wait the domain update in 1x24 hours");
        } else $error = $r;
      } else $error = $r;
    } else $error = $r;

    $errors = [];
    if (isset($r->errors)) {
      foreach ($r->errors as $item) {
        if (isset($item->message)) {
          $errors[] = $item->message;
        } else $errors[] = $item;
      }
      if (sizeof($errors) > 0) $errors = implode(",", $errors);
    } else if (isset($r->message)) {
      $errors = $r->message;
    } else $errors = json_encode($r);

    return $listener->response(400, "Can't setup the domain for now." . ($errors != "" ? (" Refer: " . $errors) : ""));
  }
}
