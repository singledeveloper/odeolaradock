<?php

namespace Odeo\Domains\Subscription;

use Carbon\Carbon;
use Odeo\Domains\Constant\ChannelConfig;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Constant\Vendor;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Marketing\RevenueUpdater;
use Odeo\Domains\Subscription\DistributionChannel\ChannelUpdater;
use Odeo\Domains\Subscription\Jobs\SendStoreActivationNotice;
use Odeo\Domains\Subscription\ManageInventory\Jobs\PopulateStoreInventory;

class StoreVerificator {

  public function __construct() {
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->userStore = app()->make(\Odeo\Domains\Subscription\Repository\UserStoreRepository::class);
    $this->plan = app()->make(\Odeo\Domains\Subscription\Repository\PlanRepository::class);
    $this->selector = app()->make(\Odeo\Domains\Subscription\StoreSelector::class);
    $this->parser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->revenueInserter = app()->make(\Odeo\Domains\Transaction\Helper\RevenueInserter::class);
    $this->orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
  }

  public function guard(PipelineListener $listener, $data) {
    if (!is_numeric($data["store_id"])) {
      return $listener->response(400);
    }

    $store = $this->store->findById($data['store_id']);

    if (!$store) {
      return $listener->response(400, "You don't have access to this store.");
    }

    if (isAdmin()) {
      return $listener->response(200);
    }

    if (isset($data['not_available_for_free_store']) && $store->plan_id == Plan::FREE) {
      return $listener->response(400, "Your store cannot access this content.");
    }

    if (isset($data['must_status'])) {
      if ((is_array($data['must_status']) && !in_array($store->status, $data['must_status'])) || (!is_array($data['must_status']) && $store->status != $data['must_status'])) {
        return $listener->response(400, "You don't have access to this store.");
      }
    }

    if (isset($data['store_must_active']) && !StoreStatus::isActive($store->status)) {
      return $listener->response(400, "You don't have access to this store.");
    }

    if (!$this->userStore->checkCredibility($data["store_id"], $data['auth']['user_id'], null)) {
      if (!isset($data['allow_mentor']) || !$this->userStore->checkCredibilityAllowMentor($data['store_id'], $data['auth']['user_id'])) {
        return $listener->response(400, "You don't have access to this store.");
      }
    }

    return $listener->response(200, isset($data['response_with_store_data']) ? [
      'store' => $store->toArray()
    ] : []);
  }

  public function guardService(PipelineListener $listener, $data) {
    $serviceDetail = $this->serviceDetails->findById($data['service_detail_id']);

    if ($serviceDetail->vendor->id != Vendor::ODEO_INTERNAL) {
      if (!isset($data["store_id"])) return $listener->response(400, "Store ID is required.");
      if ($data["store_id"] == StoreStatus::STORE_DEFAULT) return $listener->response(200);

      if ($store = $this->store->findById($data["store_id"])) {
        $currentData = json_decode($this->parser->currentData($store->plan_id, $store->current_data));
      }
      $serviceName = $serviceDetail->service->name;

      if (!isset($currentData) || !isset($currentData->$serviceName) || (isset($currentData->$serviceName) && $currentData->$serviceName <= 0)) {
        return $listener->response(400, "You don't have credential to access this item.");
      }
    }

    return $listener->response(200);
  }

  public function verify(PipelineListener $listener, $data) {

    $store = $this->store->findById($data["store_id"]);

    $statusBefore = $store->status;

    switch ($store->status) {
      case StoreStatus::PENDING:
        $store->status = StoreStatus::OK;
        break;

      case StoreStatus::WAIT_FOR_UPGRADE_VERIFY:
        $store->status = StoreStatus::OK;
        $storeUpgradeHistory = app()->make(\Odeo\Domains\Subscription\Repository\OrderDetailPlanRepository::class);
        if ($upgrade_history = $storeUpgradeHistory->findByStoreId($data["store_id"], 'upgrade')) {
          $upgradeRequestDatetime = $upgrade_history->created_at;
          $previousPlanId = $store->plan_id;
          $store->plan_id = $upgrade_history->plan_id;
          $store->current_data = $this->parser->currentData($store->plan_id, $store->current_data);

          if ($previousPlanId == Plan::FREE) {
            $channelRepos = app()->make(\Odeo\Domains\Subscription\DistributionChannel\Repository\StoreDistributionChannelRepository::class);

            $channel = $channelRepos->findByAttributes('store_id', $store->id);
            $channel->marketplace = true;

            $channelRepos->save($channel);

          }

        }

        break;

      case StoreStatus::WAIT_FOR_RENEWAL_VERIFY:
      case StoreStatus::WAIT_FOR_EXP_RENEWAL_VERIFY:
        $store->status = StoreStatus::OK;
        break;

      default:
        return $listener->response(200, $this->selector->_transforms($store, $this->store));
    }

    $plan = $this->plan->findById($store->plan_id);

    if (!$store->renewal_at || $statusBefore == StoreStatus::WAIT_FOR_UPGRADE_VERIFY || $statusBefore == StoreStatus::WAIT_FOR_EXP_RENEWAL_VERIFY) {
      $store->renewal_at = Carbon::now()->addDays($plan->minimal_months * 30)->toDateTimeString();
    } else if ($statusBefore == StoreStatus::WAIT_FOR_RENEWAL_VERIFY) {
      $store->renewal_at = Carbon::parse($store->renewal_at)->addDays($plan->minimal_months * 30)->toDateTimeString();
    }

    $this->store->save($store);

    $this->store->addFilter('expand', 'plan');
    $storeTrans = $this->selector->_transforms($store, $this->store);

    foreach ($this->userStore->findByStoreId($data["store_id"]) as $item) {
      $this->notification->setup($item->user_id, NotificationType::CHECK, NotificationGroup::TRANSACTION);
      $this->notification->store_alter($data["store_id"], $store->name, $store->plan->name, $statusBefore);
    }

    $listener->pushQueue(new PopulateStoreInventory([
      'store_id' => $store->id
    ]));

    $listener->pushQueue(new SendStoreActivationNotice([
      'store' => $store->toArray(),
      'refer_by' => (function () use ($store) {
        if ($val = $store->network) {
          return $val->referredStore ? $val->referredStore->name : '-';
        }
        return null;
      })(),
      'plan' => $plan->toArray(),
      'status_before' => $statusBefore,
      'previous_plan' => isset($previousPlanId) ? $this->plan->findById($previousPlanId)->toArray() : ''
    ]));

    $listener->pushQueue($this->notification->queue());

    $storeTrans["status_before"] = $statusBefore;

    if (isset($upgradeRequestDatetime)) {
      $storeTrans["upgrade_request_datetime"] = $upgradeRequestDatetime;
    }

    if (isset($previousPlanId)) {
      $storeTrans["previous_plan_id"] = $previousPlanId;
      app()->make(\Odeo\Domains\Network\Helper\ChildInserter::class)->check($store->network);
    }

    if (isset($data['order_detail_id']) && $orderDetail = $this->orderDetails->findById($data["order_detail_id"])) {
      $this->revenueInserter->insertRevenue($data["store_id"], $orderDetail->order_id, $orderDetail->sale_price, false, false, true);
    }

    if ($statusBefore != StoreStatus::WAIT_FOR_RENEWAL_VERIFY) {
      app()->make(\Odeo\Domains\Notification\Repository\NotificationRepository::class)->findLikeAndDelete("upgrade_store_id", $data["store_id"]);
    }

    return $listener->response(200, $storeTrans);
  }


  public function verifyStoreFree(PipelineListener $listener, $data) {

    $store = $this->store->findById($data["store_id"]);

    if (app()->environment() == 'production') {
      if ($store->plan_id == Plan::FREE && $this->userStore->hasFreeStore($data['auth']['user_id'])) {
        return $listener->response(400, 'Anda hanya boleh memiliki 1 store FREE');
      }
    }

    $statusBefore = $store->status;

    $store->status = StoreStatus::OK;

    $this->store->save($store);

    $this->notification->setup($data['auth']['user_id'], NotificationType::CHECK, NotificationGroup::TRANSACTION);
    $this->notification->store_alter($data["store_id"], $store->name, $store->plan->name, $statusBefore);

    $listener->pushQueue($this->notification->queue());

    $listener->pushQueue(new PopulateStoreInventory([
      'store_id' => $store->id
    ]));

    $listener->addNext(new Task(ChannelUpdater::class, 'createChannel', [
      'store_id' => $store->id
    ]));
    $listener->addNext(new Task(RevenueUpdater::class, 'initRevenue', [
      'store_id' => $store->id
    ]));

    return $listener->response(200, [
      'skip_payment' => true,
      'additional' => [
        'store_id' => $store->id
      ]
    ]);
  }
}
