<?php

namespace Odeo\Domains\Subscription\Vendor\Jabber;

use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Jabber\JabberManager;

class HelpLauncher extends JabberManager {

  private $store;

  public function __construct() {
    parent::__construct();
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {

    if (!isset($data['store_id']) || (isset($data['store_id']) &&
        (isset($data['store_id']) == null || isset($data['store_id']) == '')))
      $storeName = 'ODEO';
    else $storeName = $this->store->findById($data['store_id'])->name;

    $replies = ['Berikut list command yang dapat anda gunakan:'];
    foreach (InlineJabber::getCommandDescriptions() as $item) {
      $replies[] = strtoupper($item['command']) . ' - ' . $item['description'];
    }
    $replies[] = "\nBerikut contoh parsing yang dapat anda gunakan:";
    foreach (InlineJabber::getReplyExamples() as $item) {
      $replies[] = strtoupper($item['title']) . ' - ' . $item['reply'];
    }

    $message = "Selamat datang di " . $storeName . " Jabber.\n\n" . implode("\n", $replies);

    $listener->appendResponseMessage($message, InlineJabber::RES_PARAM);

    return $listener->response(200);
  }

}