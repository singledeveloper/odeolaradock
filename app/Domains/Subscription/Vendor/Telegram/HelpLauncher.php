<?php

namespace Odeo\Domains\Subscription\Vendor\Telegram;

use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Constant\InlineTelegram;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Telegram\TelegramManager;

class HelpLauncher extends TelegramManager {

  private $store;

  public function __construct() {
    parent::__construct();
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $this->initialize($data['chat_id'], $data['token']);

    if (!isset($data['store_id']) || (isset($data['store_id']) &&
        (isset($data['store_id']) == null || isset($data['store_id']) == '')))
      $storeName = 'ODEO';
    else $storeName = $this->store->findById($data['store_id'])->name;

    $replies = ['Berikut list command yang dapat anda gunakan:'];
    foreach (InlineTelegram::getCommandDescriptions() as $item) {
      $replies[] = strtoupper($item['command']) . ' - ' . $item['description'];
    }

    $this->reply("Selamat datang di " . $storeName . " Telegram.\n\n" . implode("\n", $replies), InlineTelegram::KEYBOARD_DEFAULT);

    return $listener->response(200);
  }

}