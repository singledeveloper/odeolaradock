<?php

namespace Odeo\Domains\Subscription;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\WarrantyStatus;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;

class WarrantyRequester {

  public function __construct() {
    $this->warranty = app()->make(\Odeo\Domains\Subscription\Repository\WarrantyRepository::class);
    $this->selector = app()->make(\Odeo\Domains\Subscription\WarrantySelector::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->orderDetail = app()->make(\Odeo\Domains\Subscription\Repository\OrderDetailWarrantyRepository::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
  }
  
  public function create(PipelineListener $listener, $data) {
    if ($warranty = $this->warranty->findExists($data["store_id"])) {
      $warranty->status = WarrantyStatus::OVERWRITTEN;
      $this->warranty->save($warranty);
    }
    
    $store = $this->store->findById($data["store_id"]);
    
    $warranty = $this->warranty->getNew();
    $warranty->store_id = $data["store_id"];
    $warranty->user_data = json_encode($data["info"]);
    $warranty->expired_at = date("Y-m-d H:i:s", strtotime($store->created_at . ' +720 day'));
    $warranty->status = WarrantyStatus::PENDING;
    $this->warranty->save($warranty);
    
    return $listener->response(201, $this->selector->_transforms($warranty, $this->warranty));
  }
  
  public function verify(PipelineListener $listener, $data) {
    if ($warranty = $this->warranty->findById($data["warranty_id"])) {
      $warranty->status = WarrantyStatus::OK;
      $this->warranty->save($warranty);
      
      $owner = $this->store->findOwner($warranty->store_id);
      $response["store_name"] = $warranty->store->name;
      $response["user_id"] = $owner->user_id;
      
      $this->notification->setup($owner->user_id, NotificationType::CHECK, NotificationGroup::ACCOUNT);
      $this->notification->warranty($warranty->store_id, $warranty->store->name);
      $listener->pushQueue($this->notification->queue());
      
      return $listener->response(200, $response);
    }
    
    return $listener->response(400, "Warranty not exists.");
  }
  
  public function cancel(PipelineListener $listener, $data) {
    
    if ($warranty = $this->warranty->findPending($data["store_id"])) {
      $warranty->status = WarrantyStatus::OVERWRITTEN;
      $this->warranty->save($warranty);
    }
    
    return $listener->response(200, $warranty->toArray());
  }
  
  public function checkout(PipelineListener $listener, $data) {
    $warrantyId = $data['item']['item_detail']['warranty_id'];
    if ($warranty = $this->warranty->findById($warrantyId)) {
      $orderDetail = $this->orderDetail->getNew();
      $orderDetail->warranty_id = $warrantyId;
      $orderDetail->plan_id = $warranty->store->plan->id;
      $orderDetail->order_detail_id = $data['order_detail']['id'];
      
      $this->orderDetail->save($orderDetail);
      return $listener->response(201);
    }
    return $listener->response(400, "Unable to checkout the warranty.");
  }
}
