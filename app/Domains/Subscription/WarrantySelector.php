<?php

namespace Odeo\Domains\Subscription;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Constant\WarrantyStatus;

class WarrantySelector implements SelectorListener {

  private $warranty, $store, $currency;

  public function __construct(Currency $currency) {
    $this->warranty = app()->make(\Odeo\Domains\Subscription\Repository\WarrantyRepository::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->currency = $currency;
  }
  
  public function _transforms($item, Repository $repository) {
    $warranty = [];
    if ($item->id) $warranty["warranty_id"] = $item->id;
    $warranty["type"] = "exists";
    if ($item->store_id) {
      $warranty["store_id"] = $item->store_id;
      $filters = $repository->getFilters();
      $buy = true;
      if (isset($filters['currency'])) $currency = $filters['currency'];
      else {
        $currency = Currency::IDR;
        $buy = false;
      }
      $warranty["price"] = $this->currency->formatPrice($item->store->plan->warranty * $item->store->plan->minimal_months, $currency, $buy);
    }
    if ($item->user_data) $warranty["info"] = json_decode($item->user_data);
    if ($item->expired_at) $warranty["expired_at"] = strtotime($item->expired_at);
    if ($item->status) $warranty["status"] = ($item->status == WarrantyStatus::OK) ? "OK" : "PENDING";
    return $warranty;
  }
  
  public function _extends($data, Repository $repository) {
    if ($warranty_ids = $repository->beginExtend($data, "warranty_id")) {
      if ($repository->hasExpand('order')) {
        $order = app()->make(\Odeo\Domains\Order\Helper\OrderManager::class);
        $repository->addExtend('order', $order->getByWarrantyId($warranty_ids));
      }
    }
    return $repository->finalizeExtend($data);
  }
  
  public function getDetail(PipelineListener $listener, $data) {
    if ($store = $this->store->findById($data["store_id"])) {
      $this->warranty->normalizeFilters($data);
      if (!$warranty = $this->warranty->findExists($data["store_id"])) {
        $plan = $store->plan;
        $response = [
          "type" => "new",
          "description" => "GARANSI odeoSAFE+\n\nAnda juga bisa membeli tambahan garansi melalui program odeoSAFE+.\n\nBersama odeoSAFE+, Anda dapat menikmati tambahan garansi selama 12 bulan + garansi balik modal, sehingga selama 24 bulan bisnis Anda terjamin.\n\n*Peningkatan Paket akan otomatis menghilangkan odeoSAFE Anda.",
          "price" => $this->currency->formatPrice($plan->warranty * $plan->minimal_months, $data['currency'] ?? Currency::IDR, true),
          "expired_at" => strtotime($store->renewal_at . " +" . ($plan->minimal_months * 30) . " day")
        ];
      }
      else $response = $this->_transforms($warranty, $this->warranty);
      
      return $listener->response(200, $this->_extends($response, $this->warranty));
    }
    
    return $listener->response(204);
  }
}
