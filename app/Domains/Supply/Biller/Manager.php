<?php

namespace Odeo\Domains\Supply\Biller;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\Contract\BillerContract;

class Manager implements BillerContract {

  private $purchaser, $inventoryValidator, $inquirer, $postpaidPurchaser;

  public function __construct() {
    $this->purchaser = app()->make(Purchaser::class);
    $this->inventoryValidator = app()->make(InventoryValidator::class);
    $this->inquirer = app()->make(Inquirer::class);
    $this->postpaidPurchaser = app()->make(PostpaidPurchaser::class);
  }

  public function purchase(PipelineListener $listener, $data) {
    return $this->purchaser->purchase($listener, $data);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    return $this->inventoryValidator->validateInventory($listener, $data);
  }

  public function purchasePostpaid(PipelineListener $listener, $data) {
    return $this->postpaidPurchaser->purchase($listener, $data);
  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {
    return $this->inquirer->inquiry($listener, $data);
  }

}