<?php

namespace Odeo\Domains\Supply\Biller;

use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Supply\Helper\BillerStatusUpdater;
use Odeo\Domains\Supply\Jobs\SaveLog;

class Notifier extends BillerStatusUpdater {

  private $request, $inquirer;

  public function __construct() {
    parent::__construct();
    $this->request = app()->make(\Illuminate\Http\Request::class);
    $this->inquirer = app()->make(Inquirer::class);
  }

  public function notify(PipelineListener $listener, $data) {

    if ($connection = $this->vendorSwitcherConnections->findByVendorSwitcherNotifySlug($data['slug'])) {
      $notifyData = '';
      foreach ($this->vendorSwitcherTemplateResponseDetails->getResponseList($connection->template_id, $connection->vendor_switcher_id, Supplier::ROUTE_REPORT) as $item) {
        $extracts = $this->typeFormatter->setPath($item->format_type)->getRequestData($this->request);
        if ((is_array($extracts) && count($extracts) > 0) || (!is_array($extracts) && $extracts != ''))
          $notifyData = $extracts;
        else continue;

        if ($this->translateResponse($item, $notifyData)) break;
      }

      if ($notifyData == '') $notifyData = $data;
      $notifyData = is_array($notifyData) ? ($this->request->method() == 'GET' ? urldecode(http_build_query($notifyData)) : json_encode($notifyData)) : $notifyData;

      if (isset($this->translateResult[Supplier::SF_PARAM_TRXID])) {
        if (strpos($this->translateResult[Supplier::SF_PARAM_TRXID], Supplier::POSTPAID_INQ_PREFIX) !== false
          && $this->inquirer->complete([
            'postpaid_inquiry_id' => $this->translateResult[Supplier::SF_PARAM_TRXID],
            'notify' => $notifyData,
            'translate_result' => $this->translateResult
          ])) return $listener->response(200, ['status' => 'OK']);
        $this->currentRecon = $this->reconRepository->findById(str_replace(Supplier::POSTPAID_INQ_PREFIX, '', $this->translateResult[Supplier::SF_PARAM_TRXID]));
      }
      else if (isset($this->translateResult[Supplier::SF_PARAM_REFID]))
        $this->currentRecon = $this->reconRepository->findByBillerTransactionId($this->translateResult[Supplier::SF_PARAM_REFID], $connection->vendor_switcher_id);
      else if (isset($this->translateResult[Supplier::SF_PARAM_MSISDN]) && isset($this->translateResult[Supplier::SF_PARAM_CODE]))
        $this->currentRecon = $this->reconRepository->findByInventoryCodeAndNumber([
          'code' => trim($this->translateResult[Supplier::SF_PARAM_CODE]),
          'no' => trim($this->translateResult[Supplier::SF_PARAM_MSISDN]),
          'vendor_switcher_id' => $connection->vendor_switcher_id
        ]);
      else if ($notifyData != '') {
        dispatch(new SaveLog($connection->vendor_switcher_id, Supplier::LOG_UNKNOWN_REPORT, $notifyData));
        return $listener->response(200, ['status' => 'Unknown report']);
      }
      else return $listener->response(200, ['status' => 'OK']);

      if ($this->currentRecon && in_array($this->currentRecon->status, [SwitcherConfig::BILLER_IN_QUEUE, SwitcherConfig::BILLER_SUSPECT])) {
        if ($this->redis->hget(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_PULSA_CHECK_LOCK . $this->currentRecon->id)) {
          dispatch(new SaveLog($connection->vendor_switcher_id, Supplier::LOG_UNKNOWN_FATAL_TRANSACTION, $this->currentRecon->notify != null ? $this->currentRecon->notify : $notifyData));
          return $listener->response(200, ['status' => 'Unknown fatal transaction']);
        }

        $this->redis->hset(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_PULSA_CHECK_LOCK . $this->currentRecon->id, 1);

        $this->expandSwitcher($this->currentRecon->switcher);

        if ($this->currentSwitcher->vendor_switcher_id != $this->currentRecon->vendor_switcher_id) {
          dispatch(new SaveLog($connection->vendor_switcher_id, Supplier::LOG_UNMATCH_TRANSACTION, $this->currentRecon->notify != null ? $this->currentRecon->notify : $notifyData));
          return $listener->response(200, ['status' => 'Unknown switcher transaction']);
        }

        $this->currentRecon->notify = $notifyData;
        $this->statusBefore = $this->currentRecon->status;

        return $this->supplyFinalize($listener);
      }
      else {
        dispatch(new SaveLog($connection->vendor_switcher_id, Supplier::LOG_UNKNOWN_TRANSACTION, $notifyData));
        return $listener->response(200, ['status' => 'Unknown transaction']);
      }
    }
    else if ($biller = $this->vendorSwitcherRepository->findBySlug($data['slug'])) {
      $notifyData = $this->request->getContent();
      if (trim($notifyData) == '') $notifyData = $data;
      $notifyData = is_array($notifyData) ? ($this->request->method() == 'GET' ? urldecode(http_build_query($notifyData)) : json_encode($notifyData)) : $notifyData;
      dispatch(new SaveLog($biller->id, Supplier::LOG_UNKNOWN_REPORT, $notifyData));
    }
    return $listener->response(400, 'Unknown path');

  }

}
