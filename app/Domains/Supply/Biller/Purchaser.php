<?php

namespace Odeo\Domains\Supply\Biller;

use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Supply\Helper\BillerStatusUpdater;
use Odeo\Domains\Vendor\Jabber\Jobs\ReplySocket;

class Purchaser extends BillerStatusUpdater {

  private $client, $userProxies;

  public function __construct() {
    parent::__construct();
    $this->client = app()->make(\Odeo\Domains\Supply\Formatter\ClientParser::class);
    $this->userProxies = app()->make(\Odeo\Domains\Account\Repository\UserProxyRepository::class);
  }

  private function getInventoryCount($pulsaOdeoId) {
    $key = Supplier::REDIS_BILLER_POI_COUNT . $pulsaOdeoId;
    if (!$count = $this->redis->get($key)) {
      $count = $this->pulsaInventoryRepository->getByPulsaOdeoIdCount($this->currentPulsa->id);
      if ($this->redis->setnx($key, $count)) $this->redis->expire($key, 24 * 60 * 60);
    }
    return $count;
  }

  private function getPoiQueue($pulsaOdeoId) {
    if ($queues = $this->redis->hget(Supplier::REDIS_BILLER, Supplier::REDIS_BILLER_POI_QUEUE . $pulsaOdeoId))
      $queues = explode(',', $queues);
    else $queues = [];
    return $queues;
  }

  private function addPoiQueue($pulsaOdeoId) {
    $queues = $this->getPoiQueue($pulsaOdeoId);
    $queues[] = $this->currentSwitcher->current_inventory_id;
    $this->redis->hset(Supplier::REDIS_BILLER, Supplier::REDIS_BILLER_POI_QUEUE . $pulsaOdeoId, implode(',', $queues));
  }

  private function removePoiQueue($pulsaOdeoId) {
    $this->redis->hdel(Supplier::REDIS_BILLER, Supplier::REDIS_BILLER_POI_QUEUE . $pulsaOdeoId);
  }

  public function purchase(PipelineListener $listener, $data) {

    if ($this->initiate($listener, $data)) {
      if ($this->currentBiller->status != SwitcherConfig::BILLER_STATUS_OK) {
        $data['denom'] = $this->currentPulsaInventory->code;
        return $this->customPurchase($listener, $data);
      }
      else if ($connection = $this->vendorSwitcherConnections->findCacheByVendorSwitcherId($this->currentPulsaInventory->vendor_switcher_id)) {

        if ($this->currentBiller->can_duplicate_without_warning) {
          $key = Pulsa::REDIS_PULSA_SUCCESS_LOCK . $this->currentSwitcher->number . '_' . $this->currentBiller->id;
          if ($this->redis->setnx($key, 0)) $this->redis->expire($key, 24 * 60 * 60);
        }

        $template = $connection->template;

        $this->currentRecon->destination = $connection->server_destination;
        $this->currentRecon->format_type = $template->format_type;

        $dataToTranslate = [
          'user_id' => $connection->server_user_id,
          'password' => $connection->server_password ? Crypt::decrypt($connection->server_password) : null,
          'pin' => $connection->server_pin ? Crypt::decrypt($connection->server_pin) : null,
          'token' => $connection->server_token ? Crypt::decrypt($connection->server_token) : null,
          'denom' => $this->currentPulsaInventory->code,
          'base_price' => $this->currentPulsaInventory->base_price,
          'service_detail_id' => $this->currentPulsa->service_detail_id
        ];
        $encryptedBody = [];

        if ($template->format_type == Supplier::TYPE_TEXT) {
          $body = $this->translateRequest($template->body_details, $dataToTranslate);
          $this->currentRecon->request = $body;

          $socketPath = '';
          if ($this->currentBiller->owner_store_id) {
            $jabberStores = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberStoreRepository::class);
            if ($jabberStore = $jabberStores->findByStoreId($this->currentBiller->owner_store_id)) {
              $socketPath = $jabberStore->socket_path;
            }
          }

          dispatch(new ReplySocket(InlineJabber::JAXL_CHAT, [
            'to' => $connection->server_destination,
            'message' => $body
          ], $socketPath != '' ? $socketPath : '', $this->currentRecon->id));

          $this->currentRecon->status = SwitcherConfig::BILLER_IN_QUEUE;
        } else {
          $body = json_decode($template->body_details, true);
          $headers = json_decode($template->headers, true);
          if (!$headers) $headers = [];

          foreach ($body as $key => $value) {
            $body[$key] = $this->translateRequest($value, $dataToTranslate);
            $encryptedBody[$key] = $this->translateRequest($value, array_merge($dataToTranslate, ['encrypt' => true]));
          }
          foreach ($headers as $key => $value) $headers[$key] = $this->translateRequest($value, $dataToTranslate);

          $body = $this->typeFormatter->setPath($template->format_type)->constructRequest($body, $template->body_title);
          $encryptedBody = $this->typeFormatter->setPath($template->format_type)->constructRequest($encryptedBody, $template->body_title);

          $this->currentRecon->request = is_array($encryptedBody) ? http_build_query($encryptedBody) : $encryptedBody;

          $proxyURL = '';
          if ($this->currentBiller->owner_store_id && $this->currentBiller->id == 109) { // TO DO TEST PROXY
            if ($proxy = $this->userProxies->findIpByStoreId($this->currentBiller->owner_store_id)) {
              $proxyURL = Pulsa::getUserProxyURL($proxy->ip);
              $this->currentRecon->request = '[PROXY: ' . $proxy->ip . '] ' . $this->currentRecon->request;
            }
          }
          list($isValid, $response) = $this->client->parse($template->format_type, $connection->server_destination, $body, $headers, $proxyURL);

          if (!$isValid) {
            $this->currentRecon->response = $response;
            if ($this->currentBiller->id != SwitcherConfig::BLACKHAWK && (
                strpos($this->currentRecon->response, 'Failed to connect to') !== false ||
                strpos($this->currentRecon->response, 'Could not resolve host') !== false))
              $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;
            else $this->currentRecon->status = SwitcherConfig::BILLER_TIMEOUT;
          } else if ($response) {
            $this->currentRecon->response = $response;
            foreach ($this->vendorSwitcherTemplateResponseDetails->getResponseList($template->id, $this->currentBiller->id, Supplier::ROUTE_RESPONSE) as $item) {
              if ($this->translateResponse($item, $response)) break;
            }

            if (!isset($this->translateResult[Supplier::SF_PARAM_STATUS]))
              $this->currentRecon->status = SwitcherConfig::BILLER_SUSPECT;
          } else {
            $this->currentRecon->response = 'No response from biller';
            $this->currentRecon->status = SwitcherConfig::BILLER_SUSPECT;
          }
        }
      } else {
        $this->currentRecon->response = 'No connection available';
        $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;
      }

      $this->removePoiQueue($this->currentPulsa->id);

      return $this->supplyFinalize($listener);
    }
    $this->finalizeSwitcher($listener);
    return $listener->response(200);

  }

  public function customPurchase(PipelineListener $listener, $data) {

    $purchaser = SwitcherConfig::getPluginPurchaser($this->currentBiller->status);
    if ($purchaser == '') {
      $this->currentRecon->response = 'No plugin available';
      $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;
    }
    else {
      $result = $purchaser->purchase($listener, [
        'order_id' => $this->currentOrder->id,
        'order_detail_pulsa_switcher_id' => $this->currentSwitcher->id,
        'store_id' => $this->currentBiller->owner_store_id,
        'vendor_switcher_id' => $this->currentBiller->id,
        'msisdn' => $this->currentSwitcher->number,
        'amount' => $data['denom']
      ]);

      if (isset($result['destination'])) $this->currentRecon->destination = $result['destination'];
      if (isset($result['ref'])) $this->currentSwitcher->serial_number = strtoupper($result['ref']);
      if (isset($result['request'])) $this->currentRecon->request = $result['request'];
      if (isset($result['response'])) $this->currentRecon->response = $result['response'];
      if (isset($result['status'])) $this->currentRecon->status = $result['status'];
      if (isset($result['switcher_status'])) $this->currentSwitcher->status = $result['switcher_status'];
      if (isset($result['response_code'])) $this->currentSwitcher->current_response_status_code = $result['response_code'];
      if (isset($result['balance'])) $this->currentBiller->current_balance = $result['balance'];
    }

    if ($this->currentBiller->owner_store_id != null) {
      if ($this->currentRecon->status != SwitcherConfig::BILLER_FAIL) {
        if ($this->currentBiller->status == SwitcherConfig::BILLER_STATUS_HIDDEN_GOJEK) {
          $this->supplySettlementAt = Carbon::now()->addMinutes(10)->toDateTimeString();
        }

        $fee = SwitcherConfig::getPluginFee($this->currentBiller->status);
        $this->currentSwitcher->subsidy_amount -= $fee;
        $this->currentRecon->sale_amount = -$fee;

        $orderChargeHelper = app()->make(\Odeo\Domains\Order\Helper\OrderCharger::class);
        $orderDetail = $this->currentSwitcher->orderDetail;
        $orderChargeHelper->charge($orderDetail->order_id, OrderCharge::AFFILIATE_FEE, $fee, OrderCharge::GROUP_TYPE_COMPANY_PROFIT);
      }

      if ($this->currentPulsa->owner_store_id == null && $this->currentSwitcher->status != SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER) {
        if ($this->getInventoryCount($this->currentPulsa->id) <= count($this->getPoiQueue($this->currentPulsa->id)) + 2)
          $this->removePoiQueue($this->currentPulsa->id);
        else $this->addPoiQueue($this->currentPulsa->id);
      }
    }
    else if ($this->currentPulsa->owner_store_id == null)
      $this->removePoiQueue($this->currentPulsa->id);

    return $this->supplyFinalize($listener);
  }

}
