<?php

namespace Odeo\Domains\Supply\Biller\Scheduler;

use Illuminate\Support\Facades\Crypt;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\SupplierTemplate;
use Odeo\Domains\Supply\Jobs\CheckTransactionStatus;

class TransactionChecker {

  private $reconRepository;

  public function __construct() {
    $this->reconRepository = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository::class);
  }

  public function run() {

    if (Pulsa::IS_MAINTANANCE) return;

    $data = [];
    foreach ($this->reconRepository->getPendingTransactionByTemplateIds(SupplierTemplate::getTemplateIds()) as $item) {
      if (!isset($data[$item->template_id])) $data[$item->template_id] = [];
      if (!isset($data[$item->template_id][$item->connection_id]))
        $data[$item->template_id][$item->connection_id] = [
          'connection' => [
            'destination' => $item->server_destination,
            'user_id' => $item->server_user_id,
            'user_name' => $item->server_user_name,
            'password' => $item->server_password != null ? Crypt::decrypt($item->server_password) : null,
            'pin' => $item->server_pin != null ? Crypt::decrypt($item->server_pin) : null,
            'token' => $item->server_token != null ? Crypt::decrypt($item->server_token) : null
          ]
        ];
      if (!isset($data[$item->template_id][$item->connection_id]['recon_ids']))
        $data[$item->template_id][$item->connection_id]['recon_ids'] = [];
      $data[$item->template_id][$item->connection_id]['recon_ids'][] = $item->id;
    }

    foreach ($data as $key => $item) {
      foreach ($item as $output) {
        dispatch(new CheckTransactionStatus($key, $output));
      }
    }
  }

}
