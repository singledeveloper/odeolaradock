<?php

namespace Odeo\Domains\Supply\Biller\Scheduler;

use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\SwitcherConfig;

class TransactionResender {

  private $jabberManager, $jabberStores, $reconRepository;

  public function __construct() {
    $this->jabberManager = app()->make(\Odeo\Domains\Vendor\Jabber\JabberManager::class);
    $this->jabberStores = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberStoreRepository::class);
    $this->reconRepository = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository::class);
  }

  public function run() {

    if (Pulsa::IS_MAINTANANCE) return;

    $sockets = [
      'default' => InlineJabber::getDefaultSocket()
    ];

    foreach ($this->reconRepository->getJabberNoReplies() as $item) {
      if ($item->response == null && time() - strtotime($item->created_at) > 600) {
        $item->response = 'No response from biller within 10 minutes. Biller may has new reply I don\'t understand, if so then please check Connection > View Logs. Also please check web report or ask them directly.';
        $item->status = SwitcherConfig::BILLER_SUSPECT;
        $this->reconRepository->save($item);
      }
      else if (time() - strtotime($item->created_at) >= 90) {
        if ($item->vendor_switcher_owner_store_id != null && !isset($sockets['store_' . $item->vendor_switcher_owner_store_id])) {
          $jabberStore = $this->jabberStores->findByStoreId($item->vendor_switcher_owner_store_id);
          $sockets['store_' . $item->vendor_switcher_owner_store_id] = $jabberStore ? $jabberStore->socket_path : $sockets['default'];
        }

        $socket = $item->vendor_switcher_owner_store_id == null ? $sockets['default'] : $sockets['store_' . $item->vendor_switcher_owner_store_id];

        clog('_jabber_resend', 'try to resend to ' . $item->destination . ', message: ' . $item->request . ' with socket ' . $socket);

        $this->jabberManager->reply($socket, InlineJabber::JAXL_CHAT, [
          'to' => $item->destination,
          'message' => $item->request
        ]);
      }
    }
  }

}
