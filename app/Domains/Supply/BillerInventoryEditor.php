<?php

namespace Odeo\Domains\Supply;

use Carbon\Carbon;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\TCash;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Subscription\ManageInventory\Jobs\StabilizeAllStoreProfit;

class BillerInventoryEditor {

  private $supplyValidator, $vendorSwitchers, $pulsaInventories, $pulsaOdeo, $internalNoticer,
    $storeInventories, $currency, $storeInventoryDetails, $storeInventoryGroupPrices, $stores;

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->storeInventoryDetails = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryDetailRepository::class);
    $this->storeInventoryGroupPrices = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository::class);
    $this->storeInventoryGroupPriceUsers = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceUserRepository::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->internalNoticer = app()->make(\Odeo\Domains\Notification\Helper\InternalNoticer::class);
  }

  public function add(PipelineListener $listener, $data) {
    $biller = $this->vendorSwitchers->findById($data['vendor_switcher_id']);

    list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    if (isset($data['supply_code']) && $this->pulsaOdeo->findSameCodeFromStore(strtoupper($data['supply_code']), $biller->owner_store_id))
      return $listener->response(400, 'Anda tidak dapat menginput kode yang sedang digunakan di toko ini.');

    $pulsa = false;
    if (isset($data['pulsa_odeo_id'])) {
      $pulsa = $this->pulsaOdeo->findById($data['pulsa_odeo_id']);
      $data['pulsa_category'] = $pulsa->category;
    }
    else if ($biller->status != SwitcherConfig::BILLER_STATUS_OK)
      return $listener->response(400, 'Anda tidak dapat membuat produk baru menggunakan biller ini.');

    if (isset($data['inventory_code']) && in_array($data['pulsa_category'], PostpaidType::getAll())) {
      $temp = explode('.', $data['inventory_code']);
      if (sizeof($temp) != 2) return $listener->response(400, 'Anda menggunakan input yang salah untuk kode tagihan');
    }

    if (!$pulsa) {
      if (!isset($data['supply_code'])) return $listener->response(400, 'Mohon isi kode produk untuk barang baru.');
      $inventoryManager = app()->make(\Odeo\Domains\Inventory\Helper\InventoryManager::class);
      $pulsaData = $inventoryManager->getServiceDataFromCategory($data['pulsa_category']);

      if (!isset($pulsaData['service_detail_id'])) return $listener->response(400, 'Kategori tidak valid.');

      $pulsa = $this->pulsaOdeo->getNew();
      $pulsa->service_detail_id = $pulsaData['service_detail_id'];
      if (isset($pulsaData['operator_id'])) $pulsa->operator_id = $pulsaData['operator_id'];

      $pulsa->category = $data['pulsa_category'];
      $pulsa->nominal = 0;
      $pulsa->price = 0;
      $pulsa->is_active = true;
      $pulsa->name = $data['pulsa_name'];
      $pulsa->inventory_code = $data['supply_code'];
      $pulsa->status = Pulsa::STATUS_OK_SUPPLIER;

      $this->pulsaOdeo->save($pulsa);

      $pulsa->replicate_flag = $pulsa->id;
      $this->pulsaOdeo->save($pulsa);
    }

    if (!isset($data['inventory_code']) || (isset($data['inventory_code']) && $data['inventory_code'] == '')) {
      if ($biller->status != SwitcherConfig::BILLER_STATUS_OK) {
        if ($biller->status == SwitcherConfig::BILLER_STATUS_HIDDEN_GOJEK) {
          if ($pulsa->nominal == null) return $listener->response(400, 'Anda meng-input produk yang salah');
          $data['inventory_code'] = $data['store_base_price'] = $pulsa->nominal;
        }
        else if ($biller->status == SwitcherConfig::BILLER_STATUS_HIDDEN_TCASH) {
          if ($pulsa->nominal == null || $pulsa->nominal == '0')
            return $listener->response(400, 'Data error. Mohon kontak @odeo_it');

          if ($pulsa->category == PulsaCode::TSELTCASH) $prefix = TCash::MENU_ID_TRANSFER;
          else if ($pulsa->category == PulsaCode::TSEL) {
            $airTimeDenomCodes = TCash::MENU_ID_AIRTIME_DENOMS;
            if (!isset($airTimeDenomCodes[$pulsa->nominal]))
              return $listener->response(400, 'Produk ini belum dapat berfungsi menggunakan TCash');
            $prefix = $airTimeDenomCodes[$pulsa->nominal];
          }
          else return $listener->response(400, 'Anda meng-input produk yang salah/belum dapat menggunakan TCash');
          $data['store_base_price'] = $pulsa->nominal;
          $data['inventory_code'] = $prefix . '.' . $pulsa->nominal;
        }
        else return $listener->response(400, 'Anda harus menginput kode biller');
      }
      else return $listener->response(400, 'Anda harus menginput kode biller');
    }

    if (!isset($data['store_base_price']) || (isset($data['store_base_price']) && $data['store_base_price'] == ''))
      return $listener->response(400, 'Mohon input harga yang dijual oleh biller');

    if ($this->pulsaInventories->findSame($pulsa->id, $data['vendor_switcher_id'], $data['inventory_code']))
      return $listener->response(400, 'Anda sudah pernah meng-input produk ini.');

    $inventory = $this->pulsaInventories->getNew();
    $inventory->vendor_switcher_id = $data['vendor_switcher_id'];
    $inventory->code = $data['inventory_code'];
    $inventory->pulsa_odeo_id = $pulsa->id;

    if (!$storeInventory = $this->storeInventories->findByStoreService($biller->owner_store_id, $pulsa->service_detail_id)) {
      $storeInventory = $this->storeInventories->getNew();
      $storeInventory->store_id = $biller->owner_store_id;
      $storeInventory->service_detail_id = $pulsa->service_detail_id;
      $storeInventory->service_id = $pulsa->serviceDetail->service_id;
      $storeInventory->active = true;
      $this->storeInventories->save($storeInventory);

      $pulsaStoreInventory = $this->storeInventories->findByStoreService($biller->owner_store_id, ServiceDetail::PULSA_ODEO);
      $groupUserToBeInsert = [];
      $now = Carbon::now()->toDateTimeString();
      foreach($this->storeInventoryGroupPrices->getBySupplyStoreInventoryId($pulsaStoreInventory->id) as $item) {
        $newGroup = $item->replicate();
        $newGroup->store_inventory_id = $storeInventory->id;
        $this->storeInventoryGroupPrices->save($newGroup);

        foreach ($this->storeInventoryGroupPriceUsers->getByGroupPriceId($item->id) as $output) {
          $groupUserToBeInsert[] = [
            'user_id' => $output->user_id,
            'store_inventory_group_price_id' => $newGroup->id,
            'created_at' => $now,
            'updated_at' => $now
          ];
        }
      }

      if (sizeof($groupUserToBeInsert) > 0)
        $this->storeInventoryGroupPriceUsers->saveBulk($groupUserToBeInsert);
    }

    if (isset($data['base_price']) && $data['base_price'] != '')
      $inventory->base_price = $data['base_price'];
    $inventory->is_seeded = true;
    $inventory->is_active = true;

    $newPulsa = $pulsa->replicate();
    $newPulsa->owner_store_id = $biller->owner_store_id;
    $newPulsa->price = $data['store_base_price'];
    $newPulsa->status = Pulsa::STATUS_OK;
    $newPulsa->last_succeeded_at = null;
    if (isset($data['supply_code'])) $newPulsa->inventory_code = strtoupper($data['supply_code']);
    if (isset($data['pulsa_name']) && $data['pulsa_name'] != '') $newPulsa->name = $data['pulsa_name'];

    $this->pulsaOdeo->save($newPulsa);

    $inventory->pulsa_odeo_id = $newPulsa->id;
    $this->pulsaInventories->save($inventory);

    $storeInventoryDetail = $this->storeInventoryDetails->getNew();
    $storeInventoryDetail->store_inventory_id = $storeInventory->id;
    $storeInventoryDetail->inventory_id = $newPulsa->id;
    $this->storeInventoryDetails->save($storeInventoryDetail);

    if ($biller->status == SwitcherConfig::BILLER_STATUS_HIDDEN_GOJEK && $data['auth']['user_id'] != '131313') { // TO DO HARDCODE TEMP
      $data['pulsa_odeo_id'] = $newPulsa->id;
      return $this->sellToMarketPlace($listener, $data);
    }

    return $listener->response(200);
  }

  public function edit(PipelineListener $listener, $data) {

    if ($pulsa = $this->pulsaOdeo->findById($data['pulsa_odeo_id'])) {
      $inventory = $this->pulsaInventories->findByPulsaOdeoId($pulsa->id);
      $biller = $inventory->vendorSwitcher;
      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      if (isset($data['supply_code'])) {
        $existing = $this->pulsaOdeo->findSameCodeFromStore(strtoupper($data['supply_code']), $biller->owner_store_id);
        if ($existing && $existing->id != $inventory->pulsa_odeo_id)
          return $listener->response(400, 'Anda tidak dapat menginput kode yang sedang digunakan di toko ini.');
      }

      if (isset($data['base_price']) && $data['base_price'] != '')
        $inventory->base_price = $data['base_price'];

      if ($pulsa->category != PulsaCode::GOJEK) {
        $inventory->code = $data['inventory_code'];
        if ($pulsa->price != $data['store_base_price']) {
          $listener->pushQueue(new StabilizeAllStoreProfit([
            'inventories' => [
              $pulsa->service_detail_id => [
                [
                  'inventory_id' => $pulsa->id,
                  'price_before' => $pulsa->price,
                  'price_after' => $data['store_base_price']
                ]
              ]
            ]
          ]));
        }
        $pulsa->price = $data['store_base_price'];
      }

      $this->pulsaInventories->save($inventory);

      if (isset($data['supply_code'])) $pulsa->inventory_code = strtoupper($data['supply_code']);
      if (isset($data['pulsa_name']) && $data['pulsa_name'] != '') $pulsa->name = $data['pulsa_name'];

      $this->pulsaOdeo->save($pulsa);

      foreach($this->pulsaInventories->getMarketplaceInventories($data['pulsa_odeo_id']) as $item) {
        $item->code = $data['inventory_code'];
        $item->base_price += $data['store_base_price'] - $pulsa->price;
        $item->is_active = $item->pulsa->price >= $item->base_price;
        $this->pulsaInventories->save($item);
      }

      return $listener->response(200, [
        'id' => $inventory->id,
        'biller' => [
          'id' => $biller->id,
          'name' => $biller->name
        ],
        'pulsa' => [
          'id' => $pulsa->id,
          'name' => $pulsa->name
        ],
        'biller_code' => $inventory->code,
        'store_code' => $pulsa->inventory_code,
        'base_price' => $this->currency->formatPrice($pulsa->price),
        'marketplace_base_price' => $inventory->base_price ? $this->currency->formatPrice($inventory->base_price) : null,
        'is_active' => $inventory->is_active
      ]);
    }

    return $listener->response(400, 'Data tidak tersedia');

  }

  public function toggle(PipelineListener $listener, $data) {

    if ($pulsa = $this->pulsaOdeo->findById($data['pulsa_odeo_id'])) {
      $inventory = $this->pulsaInventories->findByPulsaOdeoId($pulsa->id);
      $biller = $inventory->vendorSwitcher;
      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      $inventory->is_active = !$inventory->is_active;
      $inventory->is_seeded = $inventory->is_active;
      $this->pulsaInventories->save($inventory);

      $isWarning = false;
      foreach($this->pulsaInventories->getMarketplaceInventories($data['pulsa_odeo_id']) as $item) {
        $isActive = $inventory->is_active && ($item->pulsa->price >= $item->base_price || $biller->status != SwitcherConfig::BILLER_STATUS_OK);
        $item->is_active = $isActive;
        $item->is_seeded = $isActive;
        $this->pulsaInventories->save($item);
        if (!$item->is_active) $isWarning = true;
      }

      if ($biller->status != SwitcherConfig::BILLER_STATUS_OK && $isWarning) {
        $stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
        $store = $stores->findById($biller->owner_store_id);
        $internalNoticer = app()->make(\Odeo\Domains\Notification\Helper\InternalNoticer::class);
        $internalNoticer->saveMessage($store->name . ' men-disable produk ' . $pulsa->name);
      }

      if ($pulsa->owner_store_id == $biller->owner_store_id) {
        $pulsa->is_active = $inventory->is_active;
        $this->pulsaOdeo->save($pulsa);
      }

      return $listener->response(200);
    }

    return $listener->response(400, 'Data tidak tersedia');

  }

  public function sellToMarketPlace(PipelineListener $listener, $data) {

    if (isset($data['price_details'])) {
      list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
      if (!$isValid) return $listener->response(400, $message);

      $excludes = [];
      foreach ($this->pulsaInventories->getAllPulsaSupply($data['store_id']) as $item) {
        $excludes[] = $item->supply_pulsa_id;
      }

      $inventoryIds = [];
      $priceDetails = [];
      foreach ($data['price_details'] as $item) {
        if (in_array($item['id'], $excludes)) continue;
        $inventoryIds[] = $item['id'];
        $priceDetails[$item['id']] = $item['price'];
      }

      if (count($inventoryIds) <= 0)
        return $listener->response(400, 'Tidak ada data yang bisa disimpan');

      foreach($this->pulsaOdeo->getByIds($inventoryIds) as $item) {
        $inventory = $this->pulsaInventories->findByPulsaOdeoId($item->id);
        $newInventory = $inventory->replicate();
        $newInventory->supply_pulsa_id = $inventory->pulsa_odeo_id;
        $newInventory->pulsa_odeo_id = $item->replicate_flag;
        $newInventory->is_active = false;
        $newInventory->base_price = $priceDetails[$item->id];
        $this->pulsaInventories->save($newInventory);
      }

      return $listener->response(200);
    }
    else if ($pulsa = $this->pulsaOdeo->findById($data['pulsa_odeo_id'])) {
      $inventory = $this->pulsaInventories->findByPulsaOdeoId($pulsa->id);
      $biller = $inventory->vendorSwitcher;
      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      if ($this->pulsaInventories->findSameCodeFromDifferentPulsaId($inventory->vendor_switcher_id, $inventory->code, $pulsa->id))
        return $listener->response(400, 'Anda sudah menjual produk ini di marketplace');

      $newInventory = $inventory->replicate();
      $newInventory->supply_pulsa_id = $inventory->pulsa_odeo_id;
      $newInventory->pulsa_odeo_id = $pulsa->replicate_flag;
      $newInventory->priority_sort = 1;
      $newInventory->is_active = false;

      if ($biller->status == SwitcherConfig::BILLER_STATUS_HIDDEN_GOJEK) {
        if ($pulsa->price < 40000) $baseMultiplier = 40000;
        else if ($pulsa->price > 200000) $baseMultiplier = 200000;
        else $baseMultiplier = $pulsa->price;
        $newInventory->base_price = $pulsa->price + ceil(0.002 * $baseMultiplier) + SwitcherConfig::getPluginFee($biller->status);
        $newInventory->is_active = true;
      }
      else $newInventory->base_price = $data['base_price'];

      $this->pulsaInventories->save($newInventory);

      return $listener->response(200);
    }

    return $listener->response(400, 'Data tidak tersedia');

  }

  public function editMarketplace(PipelineListener $listener, $data) {
    if ($inventory = $this->pulsaInventories->findById($data['inventory_id'])) {
      $biller = $inventory->vendorSwitcher;
      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      if ($biller->status != SwitcherConfig::BILLER_STATUS_OK)
        return $listener->response(400, 'Anda tidak punya akses untuk mengubah data ini.');

      $isActiveBefore = $inventory->is_active;
      $inventory->is_active = $inventory->base_price >= $data['sell_price'];
      $inventory->base_price = $data['sell_price'];
      $this->pulsaInventories->save($inventory);

      if ($isActiveBefore && !$inventory->is_active) {
        $pulsa = $inventory->pulsa;
        $store = $this->stores->findById($biller->owner_store_id);
        $this->internalNoticer->saveMessage($pulsa->name . ' dari ' . $store->name . ' di-nonaktifkan di marketplace karena adanya kenaikan harga.');
      }

      $supplyPulsa = $this->pulsaOdeo->findById($inventory->supply_pulsa_id);

      return $listener->response(200, [
        'sell_price' => $this->currency->formatPrice($inventory->base_price),
        'price_diff' => $inventory->base_price - $supplyPulsa->price
      ]);
    }

    return $listener->response(400, 'Data tidak tersedia');
  }

  public function removeMarketplace(PipelineListener $listener, $data) {
    if ($inventory = $this->pulsaInventories->findById($data['inventory_id'])) {
      $biller = $inventory->vendorSwitcher;
      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      if ($biller->status != SwitcherConfig::BILLER_STATUS_OK)
        return $listener->response(400, 'Anda tidak punya akses untuk mengubah data ini.');

      $inventory->is_removed = true;
      $inventory->is_active = false;
      $inventory->is_seeded = false;

      $this->pulsaInventories->save($inventory);

      return $listener->response(200);
    }

    return $listener->response(400, 'Data tidak tersedia');
  }

}
