<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:54 PM
 */

namespace Odeo\Domains\Supply;

use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class BillerInventorySelector implements SelectorListener {

  private $currency, $supplyValidator, $pulsaOdeoInventories;

  public function __construct() {
    $this->pulsaOdeoInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getBaseInventories(PipelineListener $listener, $data) {

    list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
    if (!$isValid) return $listener->response(400, $message);

    $inventories = [];

    $this->pulsaOdeoInventories->normalizeFilters($data);
    $this->pulsaOdeoInventories->setSimplePaginate(true);

    foreach($this->pulsaOdeoInventories->getByVendorStoreOwner() as $item) {
      $inventories[] = [
        'id' => $item->id,
        'biller' => [
          'id' => $item->vendor_switcher_id,
          'name' => $item->biller_name
        ],
        'pulsa' => [
          'id' => $item->pulsa_odeo_id,
          'name' => $item->pulsa_name
        ],
        'biller_code' => $item->biller_code,
        'store_code' => $item->supply_code,
        'base_price' => $this->currency->formatPrice($item->store_base_price),
        'is_active' => $item->is_active
      ];
    }

    if (sizeof($inventories) > 0)
      return $listener->response(200, array_merge(
        ["inventories" => $inventories],
        $this->pulsaOdeoInventories->getPagination()
      ));

    return $listener->response(204, ["orders" => []]);
  }

  public function getUnpopulateInventories(PipelineListener $listener, $data) {

    list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
    if (!$isValid) return $listener->response(400, $message);

    $data['ids_not_in'] = [];
    if ($data['supply_group_id'] != '0') {
      $storeInventoryGroupPriceDetails = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceDetailRepository::class);
      foreach($storeInventoryGroupPriceDetails->getAllInventoryIdsInGroup($data['supply_group_id']) as $item) {
        $data['ids_not_in'][] = $item->inventory_id;
      }
    }
    else {
      foreach ($this->pulsaOdeoInventories->getAllPulsaSupply($data['store_id']) as $item) {
        $data['ids_not_in'][] = $item->supply_pulsa_id;
      }
    }

    $inventories = [];

    $this->pulsaOdeoInventories->normalizeFilters($data);

    foreach($this->pulsaOdeoInventories->getByVendorStoreOwner(true) as $item) {
      $inventories[] = [
        'id' => $item->id,
        'biller' => [
          'id' => $item->vendor_switcher_id,
          'name' => $item->biller_name
        ],
        'pulsa' => [
          'id' => $item->pulsa_odeo_id,
          'name' => $item->pulsa_name
        ],
        'biller_code' => $item->biller_code,
        'store_code' => $item->supply_code,
        'base_price' => $this->currency->formatPrice($item->store_base_price)
      ];
    }

    if (sizeof($inventories) > 0)
      return $listener->response(200, ["inventories" => $inventories]);

    return $listener->response(204, ["inventories" => []]);
  }

  public function getMarketplaceInventories(PipelineListener $listener, $data) {
    list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
    if (!$isValid) return $listener->response(400, $message);

    $inventories = [];
    $this->pulsaOdeoInventories->normalizeFilters($data);

    foreach($this->pulsaOdeoInventories->getSupplierMarketplaceInventories() as $item) {
      $inventories[] = [
        'id' => $item->id,
        'biller' => [
          'id' => $item->vendor_switcher_id,
          'name' => $item->biller_name
        ],
        'pulsa' => [
          'id' => $item->pulsa_odeo_id,
          'name' => $item->name
        ],
        'biller_code' => $item->biller_code,
        'base_price' => $this->currency->formatPrice($item->store_base_price),
        'sell_price' => $this->currency->formatPrice($item->sell_price),
        'price_diff' => $item->sell_price - $item->store_base_price - SwitcherConfig::getPluginFee($item->biller_status),
        'is_editable' => $item->status == Pulsa::STATUS_OK,
        'is_in_use' => $item->is_active
      ];
    }

    if (sizeof($inventories) > 0)
      return $listener->response(200, ["group_details" => $inventories]);

    return $listener->response(204, ["group_details" => []]);
  }

}
