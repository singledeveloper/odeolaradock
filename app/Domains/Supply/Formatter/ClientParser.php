<?php

namespace Odeo\Domains\Supply\Formatter;

use Odeo\Domains\Constant\Supplier;

class ClientParser {

  private function getClient() {
    return new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ]
    ]);
  }

  public function parse($methodType, $url, $data, $headers = [], $proxy = '') {
    try {
      $request = ['timeout' => 45];
      if (sizeof($headers) > 0) $request['headers'] = $headers;
      if ($proxy != '') $request['proxy'] = $proxy;

      if ($methodType == Supplier::TYPE_HTTP_QUERY) {
        $data = urldecode(http_build_query($data));
        if ($data != '') $url .= '?' . $data;
        $method = 'get';
      }
      else {
        $method = 'post';
        if ($methodType == Supplier::TYPE_JSON) $request['json'] = json_decode($data, true);
        else if ($methodType == Supplier::TYPE_FORM) $request['form_params'] = $data;
        else $request['body'] = $data;
      }

      $response = $this->getClient()->request($method, $url, $request);
      return [true, $response->getBody()->getContents()];
    }
    catch (\Exception $e) {
      return [false, $e->getMessage()];
    }
  }

}
