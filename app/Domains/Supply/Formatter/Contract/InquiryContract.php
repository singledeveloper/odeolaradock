<?php

namespace Odeo\Domains\Supply\Formatter\Contract;

interface InquiryContract {

  public function toString($data, $currentSerialNumber = '');

  public function toJsonInquiry($data);

  public function toOrderInquiry($item, $orderDetail);

  public function record($switcherId, $detail);

}