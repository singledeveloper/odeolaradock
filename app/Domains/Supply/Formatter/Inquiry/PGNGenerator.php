<?php

namespace Odeo\Domains\Supply\Formatter\Inquiry;

use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Supply\Formatter\Contract\InquiryContract;

class PGNGenerator implements InquiryContract {

  public function toString($data, $currentSerialNumber = '') {
    return $currentSerialNumber;
  }

  public function toJsonInquiry($data) {

    return [
      'name' => strtoupper(PostpaidType::PGN),
      'subscriber_id' => isset($data[Supplier::SF_PARAM_NUMBER]) ? $data[Supplier::SF_PARAM_NUMBER] : '-',
      'subscriber_name' => isset($data[Supplier::SF_PARAM_OWNER_NAME]) ? $data[Supplier::SF_PARAM_OWNER_NAME] : '-',
      'multiplier' => 1,
      'ref_id' => isset($data[Supplier::SF_PARAM_SN]) ? $data[Supplier::SF_PARAM_SN] : '-',
      'biller_price' => Supplier::checkPostpaidTotal($data),
      'details' => [
        'period' => isset($data[Supplier::SF_PARAM_POSTPAID_PERIODE]) ? $data[Supplier::SF_PARAM_POSTPAID_PERIODE] : '-',
        'meter_changes' => isset($data[Supplier::SF_PARAM_POSTPAID_METER_CHANGES]) ? $data[Supplier::SF_PARAM_POSTPAID_METER_CHANGES] : '-',
        'usages' => isset($data[Supplier::SF_PARAM_POSTPAID_USAGES]) ? $data[Supplier::SF_PARAM_POSTPAID_USAGES] : '-',
        'base_price' => isset($data[Supplier::SF_PARAM_POSTPAID_PRICE]) ? $data[Supplier::SF_PARAM_POSTPAID_PRICE] : '-',
        'admin_fee' => isset($data[Supplier::SF_PARAM_POSTPAID_ADMIN]) ? $data[Supplier::SF_PARAM_POSTPAID_ADMIN] : 0
      ]
    ];
  }

  public function toOrderInquiry($item, $orderDetail) {
    $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $pgnDetail = $item && $item->pgnDetail ? $item->pgnDetail : false;

    $inquiry = [
      'subscriber_id' => $pgnDetail ? $pgnDetail->subscriber_id : "",
      'subscriber_name' => $pgnDetail ? $pgnDetail->subscriber_name : "",
      'discount' => $currencyHelper->formatPrice(0)
    ];

    if ($pgnDetail) {
      $inquiry = array_merge($inquiry, [
        'period' => $pgnDetail->period,
        'meter_changes' => $pgnDetail->meter_changes,
        'usages' => $pgnDetail->usages,
        'ref_number' => ($item) ? $item->serial_number : "",
        'price' => $currencyHelper->formatPrice($pgnDetail->base_price),
        'admin_fee' => $currencyHelper->formatPrice($pgnDetail->admin_fee),
        'discount' => $currencyHelper->formatPrice($pgnDetail->base_price + $pgnDetail->admin_fee - $orderDetail->sale_price)
      ]);
    }

    return $inquiry;

  }

  public function record($switcherId, $detail) {
    $postpaidDetails = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPostpaidPgnDetailRepository::class);
    $switcherDetail = $postpaidDetails->getNew();
    $switcherDetail->order_detail_pulsa_switcher_id = $switcherId;
    $switcherDetail->subscriber_id = $detail['subscriber_id'];
    $switcherDetail->subscriber_name = $detail['subscriber_name'];
    $switcherDetail->period = $detail['inquiries']['period'];
    $switcherDetail->meter_changes = $detail['inquiries']['meter_changes'];
    $switcherDetail->usages = $detail['inquiries']['usages'];
    $switcherDetail->base_price = $detail['inquiries']['base_price'];
    $switcherDetail->admin_fee = $detail['inquiries']['admin_fee'];
    $postpaidDetails->save($switcherDetail);
  }

}
