<?php

namespace Odeo\Domains\Supply\Formatter\Inquiry;

use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Supply\Formatter\Contract\InquiryContract;

class PlnPostpaidGenerator implements InquiryContract {

  public function toString($data, $currentSerialNumber = '') {
    $currentSerialNumber = (isset($data[Supplier::SF_PARAM_OWNER_NAME]) ? $data[Supplier::SF_PARAM_OWNER_NAME] : '-');
    $currentSerialNumber .= '/PTAG:' . (isset($data[Supplier::SF_PARAM_POSTPAID_PERIODE]) ? $data[Supplier::SF_PARAM_POSTPAID_PERIODE] : '-');
    $currentSerialNumber .= '/JBLN:' . (isset($data[Supplier::SF_PARAM_POSTPAID_MONTH_COUNT]) ? $data[Supplier::SF_PARAM_POSTPAID_MONTH_COUNT] : '-');
    $currentSerialNumber .= '/TD:' . ((isset($data[Supplier::SF_PARAM_PLN_TARIFF]) ? $data[Supplier::SF_PARAM_PLN_TARIFF] : '?') . '-' .
        (isset($data[Supplier::SF_PARAM_PLN_POWER]) ? $data[Supplier::SF_PARAM_PLN_POWER] : '?'));
    $currentSerialNumber .= '/SM:' . (isset($data[Supplier::SF_PARAM_POSTPAID_METER_CHANGES]) ? $data[Supplier::SF_PARAM_POSTPAID_METER_CHANGES] : '-');
    $currentSerialNumber .= '/TAG:' . (isset($data[Supplier::SF_PARAM_POSTPAID_PRICE]) ? $data[Supplier::SF_PARAM_POSTPAID_PRICE] : '-');
    $currentSerialNumber .= '/ADM:' . (isset($data[Supplier::SF_PARAM_POSTPAID_ADMIN]) ? $data[Supplier::SF_PARAM_POSTPAID_ADMIN] : 0);
    $currentSerialNumber .= '/TTAG:' . Supplier::checkPostpaidTotal($data);
    $currentSerialNumber .= '/REF:' . (isset($data[Supplier::SF_PARAM_SN]) ? $data[Supplier::SF_PARAM_SN] : '-');
    return $currentSerialNumber;
  }

  private function _toJsonInquiryDetail($item) {
    return [
      'period' => isset($item[Supplier::SF_PARAM_POSTPAID_PERIODE]) ? $item[Supplier::SF_PARAM_POSTPAID_PERIODE] : '-',
      'base_price' => isset($item[Supplier::SF_PARAM_POSTPAID_PRICE]) ? $item[Supplier::SF_PARAM_POSTPAID_PRICE] : '-',
      'fine' => isset($item[Supplier::SF_PARAM_POSTPAID_FINE]) ? $item[Supplier::SF_PARAM_POSTPAID_FINE] : 0,
      'admin_fee' => isset($item[Supplier::SF_PARAM_POSTPAID_ADMIN]) ? $item[Supplier::SF_PARAM_POSTPAID_ADMIN] : 0
    ];
  }

  public function toJsonInquiry($data) {

    if (isset($data[Supplier::SF_PARAM_DETAILS_LOOP])) {
      $details = [];
      foreach ($data[Supplier::SF_PARAM_DETAILS_LOOP] as $item) $details[] = $this->_toJsonInquiryDetail($item);
    }
    else $details = [$this->_toJsonInquiryDetail($data)];

    return [
      'name' => strtoupper(PostpaidType::PLN_POSTPAID),
      'subscriber_id' => isset($data[Supplier::SF_PARAM_NUMBER]) ? $data[Supplier::SF_PARAM_NUMBER] : '-',
      'subscriber_name' => isset($data[Supplier::SF_PARAM_OWNER_NAME]) ? $data[Supplier::SF_PARAM_OWNER_NAME] : '-',
      'tariff' => (isset($data[Supplier::SF_PARAM_PLN_TARIFF]) ? $data[Supplier::SF_PARAM_PLN_TARIFF] : '-') . '/' .
        (isset($data[Supplier::SF_PARAM_PLN_POWER]) ? ($data[Supplier::SF_PARAM_PLN_POWER] . 'VA') : '-'),
      'multiplier' => isset($data[Supplier::SF_PARAM_DETAILS_LOOP]) ? count($data[Supplier::SF_PARAM_DETAILS_LOOP]) : 1,
      'ref_id' => isset($data[Supplier::SF_PARAM_SN]) ? $data[Supplier::SF_PARAM_SN] : '-',
      'meter_changes' => isset($data[Supplier::SF_PARAM_POSTPAID_METER_CHANGES]) ? $data[Supplier::SF_PARAM_POSTPAID_METER_CHANGES] : '-',
      'biller_price' => Supplier::checkPostpaidTotal($data),
      'details' => $details
    ];
  }

  public function toOrderInquiry($item, $orderDetail) {
    $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $firstRow = '';
    $monthly = [];
    $grossTotal = $totalMonth = $totalFine = $totalAdminFee = $totalBasePrice = 0;
    $plnDetails = $item && $item->plnDetails ? $item->plnDetails : [];
    if (count($plnDetails) > 0) {
      $firstRow = $plnDetails[0];
      foreach ($plnDetails as $output) {
        $subtotal = $output->base_price + $output->fine + $output->admin_fee;
        $monthly[] = [
          'period' => $output->period,
          'tariff' => $output->tariff,
          'power' => $output->power,
          'price' => $currencyHelper->formatPrice($output->base_price),
          'fine' => $currencyHelper->formatPrice($output->fine),
          'admin_fee' => $currencyHelper->formatPrice($output->admin_fee),
          'meter_changes' => $output->meter_changes,
          'subtotal' => $currencyHelper->formatPrice($subtotal)
        ];
        $totalMonth++;
        $totalAdminFee += $output->admin_fee;
        $grossTotal += $subtotal;
        $totalBasePrice += $output->base_price;
        $totalFine += $output->fine;
      }
    }

    return [
      'subscriber_id' => ($firstRow != '') ? $firstRow->subscriber_id : "",
      'subscriber_name' => ($firstRow != '') ? $firstRow->subscriber_name : "",
      'ref_number' => ($item) ? $item->serial_number : "",
      'monthly' => $monthly,
      'total_month' => $totalMonth,
      'total_admin_fee' => $currencyHelper->formatPrice($totalAdminFee),
      'total_fine' => $currencyHelper->formatPrice($totalFine),
      'total_bill' => $currencyHelper->formatPrice($totalBasePrice),
      'discount' => $currencyHelper->formatPrice($item && $grossTotal != 0 ? $grossTotal - $orderDetail->sale_price : 0)
    ];

  }

  public function record($switcherId, $detail) {
    $postpaidDetails = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPostpaidPlnDetailRepository::class);
    foreach ($detail['inquiries'] as $item) {
      $switcherDetail = $postpaidDetails->getNew();
      $switcherDetail->order_detail_pulsa_switcher_id = $switcherId;
      $switcherDetail->subscriber_id = $detail['subscriber_id'];
      $switcherDetail->subscriber_name = $detail['subscriber_name'];
      if (isset($detail['meter_changes'])) $switcherDetail->meter_changes = $detail['meter_changes'];
      list($switcherDetail->tariff, $switcherDetail->power) = explode('/', $detail['tariff']);
      $switcherDetail->period = $item['period'];
      $switcherDetail->base_price = $item['base_price'];
      $switcherDetail->fine = $item['fine'];
      $switcherDetail->admin_fee = $item['admin_fee'];
      $postpaidDetails->save($switcherDetail);
    }
  }

}
