<?php

namespace Odeo\Domains\Supply\Formatter\Inquiry;

use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Supply\Formatter\Contract\InquiryContract;

class PlnTokenGenerator implements InquiryContract {

  public function toString($data, $currentSerialNumber = '') {
    if (isset($data[Supplier::SF_PARAM_PLN_INFO]))
      $currentSerialNumber .= '/!/' . $data[Supplier::SF_PARAM_PLN_INFO];
    else {
      if (isset($data[Supplier::SF_PARAM_OWNER_NAME])) {
        $currentSerialNumber = explode('/', $currentSerialNumber)[0];
        $currentSerialNumber .= '/' . strtoupper($data[Supplier::SF_PARAM_OWNER_NAME]);
        $currentSerialNumber .= '/' . (isset($data[Supplier::SF_PARAM_PLN_TARIFF]) ? $data[Supplier::SF_PARAM_PLN_TARIFF] : '-');
        $currentSerialNumber .= '/' . (isset($data[Supplier::SF_PARAM_PLN_POWER]) ? $data[Supplier::SF_PARAM_PLN_POWER] : '-');
        $currentSerialNumber .= '/' . (isset($data[Supplier::SF_PARAM_PLN_KWH]) ? $data[Supplier::SF_PARAM_PLN_KWH] : '-');
      }
      if (isset($data[Supplier::SF_PARAM_PLN_REF])) {
        $currentSerialNumber .= '/!/RPTOKEN:' . (isset($data[Supplier::SF_PARAM_PLN_RPTOKEN]) ? $data[Supplier::SF_PARAM_PLN_RPTOKEN] : '-');
        $currentSerialNumber .= '/PPN:' . (isset($data[Supplier::SF_PARAM_PLN_PPN]) ? $data[Supplier::SF_PARAM_PLN_PPN] : '-');
        $currentSerialNumber .= '/PPJ:' . (isset($data[Supplier::SF_PARAM_PLN_PPJ]) ? $data[Supplier::SF_PARAM_PLN_PPJ] : '-');
        $currentSerialNumber .= '/MATERAI:' . (isset($data[Supplier::SF_PARAM_PLN_MATERAI]) ? $data[Supplier::SF_PARAM_PLN_MATERAI] : '-');
        $currentSerialNumber .= '/PLNREF:' . (isset($data[Supplier::SF_PARAM_PLN_REF]) ? $data[Supplier::SF_PARAM_PLN_REF] : '-');
        $currentSerialNumber .= '/IDMETER:' . (isset($data[Supplier::SF_PARAM_NUMBER]) ? $data[Supplier::SF_PARAM_NUMBER] : '-');
        $currentSerialNumber .= '/IDPEL:' . (isset($data[Supplier::SF_PARAM_PLN_ID]) ? $data[Supplier::SF_PARAM_PLN_ID] : '-');
        $currentSerialNumber .= '/ADMIN:' . (isset($data[Supplier::SF_PARAM_POSTPAID_ADMIN]) ? $data[Supplier::SF_PARAM_POSTPAID_ADMIN] : '-');
      }
    }
    return $currentSerialNumber;
  }

  public function toJsonInquiry($data) {
    return [];
  }

  public function toOrderInquiry($item, $orderDetail) {
    $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    if (!$item) return ['discount' => $currencyHelper->formatPrice(0)];

    $plnData = [];

    // If SN exists, we take information from SN
    if ($item->serial_number) {
      $plnSnInfo = explode('/!/', $item->serial_number);
      $tempName = explode('/', $plnSnInfo[0]);

      try {
        $plnData['name'] = $tempName[1];
        $plnData['subscriber_id'] = $item->number;
        $plnData['tariff'] = $tempName[2] . '/' . $tempName[3];
        $plnData['kwh'] = isset($tempName[4]) ? $tempName[4] : null;
      } catch (\Exception $e) {
      }

      $temp['item_detail']['serial_number'] = $tempName[0];
    } // if SN not yet exists, we try to find from PlnInquiry table
    else {
      $plnInquiries = app()->make(\Odeo\Domains\Inventory\Pln\Repository\PlnInquiryRepository::class);
      $plnInquiry = $plnInquiries->findByNumber($item->number);

      $plnData['name'] = $plnInquiry->name ?? "";
      $plnData['subscriber_id'] = $plnInquiry->subscriber_id ?? "";
      $plnData['tariff'] = $plnInquiry->tariff ?? "";
    }

    $plnData['bill'] = $currencyHelper->formatPrice($item->currentInventory->pulsa->nominal);
    $plnData['admin'] = $currencyHelper->formatPrice(2500);
    $plnData['subtotal'] = $currencyHelper->formatPrice($item->currentInventory->pulsa->nominal + 2500);
    $plnData['discount'] = $currencyHelper->formatPrice($item->currentInventory->pulsa->nominal + 2500 - $orderDetail->sale_price);

    return $plnData;
  }

  public function record($switcherId, $detail) {
    return false;
  }

}
