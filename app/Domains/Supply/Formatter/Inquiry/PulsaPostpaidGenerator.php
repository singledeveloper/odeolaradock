<?php

namespace Odeo\Domains\Supply\Formatter\Inquiry;

use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Supply\Formatter\Contract\InquiryContract;

class PulsaPostpaidGenerator implements InquiryContract {

  public function toString($data, $currentSerialNumber = '') {
    return isset($data[Supplier::SF_PARAM_SN]) ? $data[Supplier::SF_PARAM_SN] : '-';
  }

  private function _toJsonInquiryDetail($item) {
    return [
      'period' => isset($item[Supplier::SF_PARAM_POSTPAID_PERIODE]) ? $item[Supplier::SF_PARAM_POSTPAID_PERIODE] : '-',
      'base_price' => isset($item[Supplier::SF_PARAM_POSTPAID_PRICE]) ? $item[Supplier::SF_PARAM_POSTPAID_PRICE] : '-',
      'admin_fee' => isset($item[Supplier::SF_PARAM_POSTPAID_ADMIN]) ? $item[Supplier::SF_PARAM_POSTPAID_ADMIN] : 0
    ];
  }

  public function toJsonInquiry($data) {
    if (isset($data[Supplier::SF_PARAM_DETAILS_LOOP])) {
      $details = [];
      foreach ($data[Supplier::SF_PARAM_DETAILS_LOOP] as $item) $details[] = $this->_toJsonInquiryDetail($item);
    }
    else $details = [$this->_toJsonInquiryDetail($data)];

    return [
      'name' => isset($data[Supplier::SF_PARAM_PRODUCT_NAME]) ? $data[Supplier::SF_PARAM_PRODUCT_NAME] : 'PULSA PASCABAYAR',
      'subscriber_id' => isset($data[Supplier::SF_PARAM_NUMBER]) ? $data[Supplier::SF_PARAM_NUMBER] : '-',
      'subscriber_name' => isset($data[Supplier::SF_PARAM_OWNER_NAME]) ? $data[Supplier::SF_PARAM_OWNER_NAME] : '-',
      'multiplier' => isset($data[Supplier::SF_PARAM_DETAILS_LOOP]) ? count($data[Supplier::SF_PARAM_DETAILS_LOOP]) : 1,
      'ref_id' => isset($data[Supplier::SF_PARAM_SN]) ? $data[Supplier::SF_PARAM_SN] : '-',
      'biller_price' => Supplier::checkPostpaidTotal($data),
      'details' => $details
    ];
  }

  public function toOrderInquiry($item, $orderDetail) {
    $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $firstRow = '';
    $monthly = [];
    $grossTotal = $totalMonth = $totalAdminFee = $totalBasePrice = 0;
    $pulsaDetails = $item && $item->pulsaDetails ? $item->pulsaDetails : [];
    if (count($pulsaDetails) > 0) {
      $firstRow = $pulsaDetails[0];
      foreach ($pulsaDetails as $output) {
        $subtotal = $output->base_price + $output->fine + $output->admin_fee;
        $monthly[] = [
          'period' => $output->period,
          'price' => $currencyHelper->formatPrice($output->base_price),
          'admin_fee' => $currencyHelper->formatPrice($output->admin_fee),
          'subtotal' => $currencyHelper->formatPrice($subtotal)
        ];
        $totalMonth++;
        $totalAdminFee += $output->admin_fee;
        $grossTotal += $subtotal;
        $totalBasePrice += $output->base_price;
      }
    }

    return [
      'subscriber_id' => ($firstRow != '') ? $firstRow->subscriber_id : "",
      'subscriber_name' => ($firstRow != '') ? $firstRow->subscriber_name : "",
      'monthly' => $monthly,
      'total_month' => $totalMonth,
      'total_admin_fee' => $currencyHelper->formatPrice($totalAdminFee),
      'total_bill' => $currencyHelper->formatPrice($totalBasePrice),
      'discount' => $currencyHelper->formatPrice($item && $grossTotal != 0 ? $grossTotal - $orderDetail->sale_price : 0)
    ];

  }

  public function record($switcherId, $detail) {
    $postpaidDetails = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPostpaidPulsaDetailRepository::class);
    foreach ($detail['inquiries'] as $item) {
      $switcherDetail = $postpaidDetails->getNew();
      $switcherDetail->order_detail_pulsa_switcher_id = $switcherId;
      $switcherDetail->subscriber_id = $detail['subscriber_id'];
      $switcherDetail->subscriber_name = $detail['subscriber_name'];
      $switcherDetail->period = $item['period'];
      $switcherDetail->base_price = $item['base_price'];
      $switcherDetail->admin_fee = $item['admin_fee'];
      $postpaidDetails->save($switcherDetail);
    }
  }

}
