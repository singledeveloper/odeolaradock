<?php

namespace Odeo\Domains\Supply\Formatter;

use Odeo\Domains\Constant\ServiceDetail;

class InquiryManager {

  public function setPath($category) {
    switch ($category) {
      case ServiceDetail::PLN_ODEO:
        return app()->make(\Odeo\Domains\Supply\Formatter\Inquiry\PlnTokenGenerator::class);
      case ServiceDetail::PLN_POSTPAID_ODEO:
        return app()->make(\Odeo\Domains\Supply\Formatter\Inquiry\PlnPostpaidGenerator::class);
      case ServiceDetail::BPJS_KES_ODEO:
        return app()->make(\Odeo\Domains\Supply\Formatter\Inquiry\BPJSGenerator::class);
      case ServiceDetail::PDAM_ODEO:
        return app()->make(\Odeo\Domains\Supply\Formatter\Inquiry\PDAMGenerator::class);
      case ServiceDetail::PGN_ODEO:
        return app()->make(\Odeo\Domains\Supply\Formatter\Inquiry\PGNGenerator::class);
      case ServiceDetail::MULTI_FINANCE_ODEO:
        return app()->make(\Odeo\Domains\Supply\Formatter\Inquiry\MultiFinanceGenerator::class);
      case ServiceDetail::PULSA_POSTPAID_ODEO:
      case ServiceDetail::LANDLINE_ODEO:
      case ServiceDetail::BROADBAND_ODEO:
        return app()->make(\Odeo\Domains\Supply\Formatter\Inquiry\PulsaPostpaidGenerator::class);
      default:
        return '';
    }
  }

}
