<?php

namespace Odeo\Domains\Supply\Formatter;

use Odeo\Domains\Constant\Supplier;

class ResponseTranslator {

  public function parse($arrayConditions, $arrayResponses) {
    $result = [];

    if (!is_array($arrayConditions)) $arrayConditions = [Supplier::DEFAULT_PARAM_TEXT => $arrayConditions];
    if (!is_array($arrayResponses)) $arrayResponses = [Supplier::DEFAULT_PARAM_TEXT => $arrayResponses];

    foreach ($arrayConditions as $key => $item) {
      $responseVal = '';
      if (strpos($key, '.') !== false) {
        $currentFlag = $arrayResponses;
        foreach (explode('.', $key) as $output) {
          if (!isset($currentFlag[$output])) break;
          $currentFlag = $currentFlag[$output];
        }
        if (!is_array($currentFlag)) $responseVal = $currentFlag;
      }

      if ($responseVal == '') {
        if (!isset($arrayResponses[$key])) return false;
        else $responseVal = $arrayResponses[$key];
      }

      if (substr(trim($item), 0, strlen(Supplier::CONTAIN_HEADER)) == Supplier::CONTAIN_HEADER) {
        $noHeader = substr(trim($item), strlen(Supplier::CONTAIN_HEADER), strlen(trim($item)) - 1);
        if (strpos($responseVal, $noHeader) === false) return false;
      }
      else {
        preg_match_all(Supplier::SF_PATTERN, $item, $matches);
        if (sizeof($matches[0]) <= 0) {
          if ($item == $responseVal) continue;
          else if (strpos($item, '&') !== false) {
            $temp = explode('&', $item);
            $flag = true;
            foreach($temp as $output) {
              if ($output[0] == '!') $flag = $flag && ltrim($output, '!') != $responseVal;
              else $flag = $flag && $output == $responseVal;
            }
            if ($flag) continue;
          }
          else if (strpos($item, '|') !== false) {
            $temp = explode('|', $item);
            $flag = false;
            foreach($temp as $output) {
              if ($output[0] == '!') $flag = $flag || ltrim($output, '!') != $responseVal;
              else $flag = $flag || $output == $responseVal;
            }
            if ($flag) continue;
          }
          else if ($item[0] == '!' && ltrim($item, '!') != $responseVal) continue;
          return false;
        }

        try {
          $pattern = preg_replace('/([.\[\]\(\)\|\/\!\*])/', '\\\\$1', $item);
          $pattern = str_replace(['\.\.\.', '\*\*\*'], '.*', $pattern);

          foreach($matches[1] as $output) {
            $pattern = str_replace(Supplier::addPattern($output), '(.*)', $pattern);
          }

          preg_match('/' . $pattern . '/', preg_replace('/\s+/', ' ', $responseVal), $match);

          foreach ($matches[1] as $k => $output) {
            if (!isset($match[$k + 1])) return false;
            $result[$output] = trim($match[$k + 1]);
          }
        }
        catch (\Exception $e) {
          clog('response_translator_error', $e->getMessage() . ' within pattern: ' . (isset($pattern) ? $pattern : '') . ' and response ' . $responseVal);
          return false;
        }
      }
    }

    if (isset($result[Supplier::SF_PARAM_PRICE]))
      $result[Supplier::SF_PARAM_PRICE] = str_replace(['Rp.', '.', ',', ' '], '',
        $result[Supplier::SF_PARAM_PRICE]);
    if (isset($result[Supplier::SF_PARAM_BALANCE]))
      $result[Supplier::SF_PARAM_BALANCE] = intval(str_replace(['Rp.', '.', ',', ' '], '',
        $result[Supplier::SF_PARAM_BALANCE]));
    if (isset($result[Supplier::SF_PARAM_UNIT_BALANCE]))
      $result[Supplier::SF_PARAM_UNIT_BALANCE] = str_replace(['.', ',', ' '], '',
        $result[Supplier::SF_PARAM_UNIT_BALANCE]);
    if (isset($result[Supplier::SF_PARAM_DEPOSIT]))
      $result[Supplier::SF_PARAM_DEPOSIT] = intval(str_replace(['Rp.', '.', ',', ' '], '',
        $result[Supplier::SF_PARAM_DEPOSIT]));

    if (sizeof($result) == 0) return ['passed' => true];
    return $result;
  }

}
