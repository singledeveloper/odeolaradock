<?php

namespace Odeo\Domains\Supply\Formatter\Signature;

use Odeo\Domains\Supply\Formatter\Contract\SignatureContract;

class OtomaxGenerator implements SignatureContract {

  public function generate($data) {

    $val = "OtomaX|" . $data['user_id'] . "|" . $data['denom'] . "|" . $data['number'] . "|" . $data['id'] . "|"  . $data['pin'] . "|" . $data['password'];
    return str_replace('/', '_', str_replace('+', '-' , rtrim(base64_encode(sha1($val, true)), '=')));

  }

}
