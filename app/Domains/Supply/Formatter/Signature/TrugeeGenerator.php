<?php

namespace Odeo\Domains\Supply\Formatter\Signature;

use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Supply\Formatter\Contract\SignatureContract;

class TrugeeGenerator implements SignatureContract {

  public function generate($data) {

    return base64_encode(($data['date'][Supplier::SF_PARAM_TIME_HMS] . substr($data['number'], -4)) ^ (strrev(substr($data['number'], -4)) . $data['password']));

  }

}
