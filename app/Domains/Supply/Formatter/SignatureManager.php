<?php

namespace Odeo\Domains\Supply\Formatter;

use Odeo\Domains\Constant\Supplier;

class SignatureManager {

  public function setPath($type) {
    switch ($type) {
      case Supplier::SIGN_CUSTOM_OTOMAX:
        return app()->make(\Odeo\Domains\Supply\Formatter\Signature\OtomaxGenerator::class);
      case Supplier::SIGN_CUSTOM_TRUGEE:
        return app()->make(\Odeo\Domains\Supply\Formatter\Signature\TrugeeGenerator::class);
      case Supplier::SIGN_CUSTOM_KOPNUS:
        return app()->make(\Odeo\Domains\Supply\Formatter\Signature\KopnusGenerator::class);
      default:
        return '';
    }
  }

}
