<?php

namespace Odeo\Domains\Supply\Formatter;

use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\TransactionType;

class SupplySalesInserter {

  private $pulsaSwitchers;

  public function __construct() {
    $this->pulsaSwitchers = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
  }

  public function getCashInserterData($data) {
    $cashInserterData = [];

    $trxData = [
      'user_id' => $data['user_id'],
      'trx_type' => TransactionType::SUPPLIER_CONVERT,
      'cash_type' => CashType::OCASH,
      'data' => json_encode([
        'order_id' => $data['order_id']
      ]),
      'amount' => $data['amount']
    ];

    if (isset($data['settlement_at']) && $data['settlement_at'] != '') {
      $trxData['settlement_at'] = $data['settlement_at'];
      $trxData['data'] = json_encode([
        'order_id' => $data['order_id'],
        'settlement_at' => $data['settlement_at']
      ]);
    }

    $cashInserterData[] = $trxData;

    if ($switcher = $this->pulsaSwitchers->findByOrderId($data['order_id'])) {
      $biller = $switcher->vendorSwitcher;
      $fee = SwitcherConfig::getPluginFee($biller->status);
      if ($fee > 0) {
        $cashInserterData[] = [
          'user_id' => $data['user_id'],
          'trx_type' => TransactionType::AFFILIATE_INQUIRY_FEE,
          'cash_type' => CashType::OCASH,
          'amount' => -$fee,
          'data' => json_encode([
            'type' => SwitcherConfig::getPluginName($biller->status),
            'order_id' => $data['order_id']
          ])
        ];
      }
    }

    return $cashInserterData;
  }

}
