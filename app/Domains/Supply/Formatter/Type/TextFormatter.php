<?php

namespace Odeo\Domains\Supply\Formatter\Type;

use Illuminate\Http\Request;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Supply\Formatter\Contract\TypeContract;

class TextFormatter implements TypeContract {

  public function constructRequest($array, $title = null) {
    return $array;
  }

  public function deconstructResponse($string) {
    if (is_array($string)) {
      $string = str_replace('\/', '/', json_encode($string));
      return $string;
    }
    return $string;
  }

  public function getRequestData(Request $request) {
    $content = $request->getContent();
    if (trim($content) == '') {
      $data = [];
      foreach ($request->input() as $key => $item) $data[$key] = $item;
      foreach ($request->file() as $key => $file) $data[$key] = $file;
      return $data;
    }
    return $content;
  }

  public function constructRequestPreview($array, $title = null) {
    return isset($array[Supplier::DEFAULT_PARAM_TEXT]) ? $array[Supplier::DEFAULT_PARAM_TEXT] : $array;
  }

}
