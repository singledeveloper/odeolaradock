<?php

namespace Odeo\Domains\Supply\Formatter\Type;

use Illuminate\Http\Request;
use Odeo\Domains\Supply\Formatter\Contract\TypeContract;

class XmlRpcFormatter implements TypeContract {

  public function constructRequest($array, $title = null) {
    $xml = '';
    foreach ($array as $key => $item) {
      $xml .= '<member><name>' . $key . '</name><value><string>' . $item . '</string></value></member>';
    }
    $xml = '<params><param><value><struct>' . $xml . '</struct></value></param></params>';
    if ($title) $xml = '<methodName>' . $title . '</methodName>' . $xml;
    return '<?xml version="1.0"?><methodCall>' . $xml . '</methodCall>';
  }

  public function deconstructResponse($string) {
    $dom = new \DOMDocument();
    $string = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $string);
    if (@$dom->loadXML($string)) {
      $results = $dom->getElementsByTagName('member');
      $response = [];
      foreach ($results as $result) {
        $name = $result->getElementsByTagName('name');
        $value = $result->getElementsByTagName('value');
        $response[$name[0]->nodeValue] = trim(str_replace(["\r\n", "\n", "\r"], '', $value[0]->nodeValue));
      }
      return $response;
    }
    return false;
  }

  public function getRequestData(Request $request) {
    return $request->getContent();
  }

  public function constructRequestPreview($array, $title = null) {
    $raw = $this->constructRequest($array, $title);
    $dom = new \DOMDocument();

    $dom->preserveWhiteSpace = false;
    $dom->formatOutput = true;

    $dom->loadXML($raw);
    $out = $dom->saveXML();

    return $out;
  }

}
