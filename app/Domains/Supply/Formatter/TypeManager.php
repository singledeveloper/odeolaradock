<?php

namespace Odeo\Domains\Supply\Formatter;

use Odeo\Domains\Constant\Supplier;

class TypeManager {

  public function setPath($type) {
    switch ($type) {
      case Supplier::TYPE_JSON:
        return app()->make(\Odeo\Domains\Supply\Formatter\Type\JsonFormatter::class);
      case Supplier::TYPE_XML:
        return app()->make(\Odeo\Domains\Supply\Formatter\Type\XmlFormatter::class);
      case Supplier::TYPE_XMLRPC:
        return app()->make(\Odeo\Domains\Supply\Formatter\Type\XmlRpcFormatter::class);
      case Supplier::TYPE_TEXT:
        return app()->make(\Odeo\Domains\Supply\Formatter\Type\TextFormatter::class);
      case Supplier::TYPE_HTTP_QUERY:
        return app()->make(\Odeo\Domains\Supply\Formatter\Type\HttpQueryFormatter::class);
      case Supplier::TYPE_FORM:
        return app()->make(\Odeo\Domains\Supply\Formatter\Type\PostFormFormatter::class);
      default:
        return '';
    }
  }

}
