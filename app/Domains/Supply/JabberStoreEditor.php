<?php

namespace Odeo\Domains\Supply;

use Illuminate\Support\Facades\Crypt;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Jabber\Jobs\ReplySocket;

class JabberStoreEditor {

  private $jabberStores, $supplyValidator;

  public function __construct() {
    $this->jabberStores = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberStoreRepository::class);
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
  }

  public function add(PipelineListener $listener, $data) {

    list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
    if (!$isValid) return $listener->response(400, $message);

    if (strpos($data['email'], InlineJabber::getHost()) !== false) {
      return $listener->response(400, 'Anda tidak perlu menggunakan @jabber.id di kolom input.');
    }

    $data['email'] = strtolower($data['email']);

    if (strlen($data['email']) < 5 && preg_match('/[^a-z0-9]/i', $data['email']))
      return $listener->response(400, 'Anda hanya dapat menggunakan angka atau huruf, minimal 5 karakter');

    if ($this->jabberStores->findByEmail($data['email'] . '@' . InlineJabber::getHost()))
      return $listener->response(400, 'Nama Akun sudah pernah terdaftar.');

    try {

      if (app()->environment() != 'local') {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', InlineJabber::getServer() . '/register', [
          'json' => [
            'user' => $data['email'],
            'host' => InlineJabber::getHost(),
            'password' => $data['password']
          ]
        ]);

        if ($response->getStatusCode() != 200) {
          return $listener->response(400, $response->getBody()->getContents());
        }
      }

      $jabberStore = $this->jabberStores->getNew();
      $jabberStore->store_id = $data['store_id'];
      $jabberStore->email = $data['email'] . '@' . InlineJabber::getHost();
      $jabberStore->password = Crypt::encrypt($data['password']);
      $this->jabberStores->save($jabberStore);

      dispatch(new ReplySocket(InlineJabber::JAXL_SUBSCRIBE, [
        'to' => $jabberStore->email
      ]));

      return $listener->response(200);
    } catch (\GuzzleHttp\Exception\ClientException $e) {
      return $listener->response(400, $e->getResponse()->getBody()->getContents());
    } catch (\GuzzleHttp\Exception\ServerException $e) {
      return $listener->response(400, 'Gagal. Silakan coba lagi.');
    }
  }

  public function editPassword(PipelineListener $listener, $data) {

    if ($jabberStore = $this->jabberStores->findByStoreId($data['store_id'])) {

      list ($isValid, $message) = $this->supplyValidator->checkStore($jabberStore->store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      if (strpos($jabberStore->email, InlineJabber::getHost()) !== false) {

        try {

          if (app()->environment() != 'local') {
            $client = new \GuzzleHttp\Client();
            list($userName, $host) = explode('@', $jabberStore->email);
            $response = $client->request('POST', InlineJabber::getServer() . '/change_password', [
              'json' => [
                'user' => $userName,
                'host' => $host,
                'newpass' => $data['new_password']
              ]
            ]);

            if ($response->getStatusCode() != 200) {
              return $listener->response(400, $response->getBody()->getContents());
            }
          }

          $jabberStore->password = Crypt::encrypt($data['password']);
          $this->jabberStores->save($jabberStore);

        } catch (\GuzzleHttp\Exception\ClientException $e) {
          return $listener->response(400, $e->getResponse()->getBody()->getContents());
        } catch (\GuzzleHttp\Exception\ServerException $e) {
          return $listener->response(400, 'Gagal. Silakan coba lagi.');
        }
        return $listener->response(200);
      }
      return $listener->response(400, 'We don\'t have access to change password of this account. Please contact your respective Jabber provider.');
    }
    return $listener->response(400, 'You haven\'t registered this account to Jabber yet.');
  }

  public function editStatusMessage(PipelineListener $listener, $data) {
    if ($jabberStore = $this->jabberStores->findByStoreId($data['store_id'])) {

      list ($isValid, $message) = $this->supplyValidator->checkStore($jabberStore->store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      $jabberStore->status_message = $data['status_message'];
      $this->jabberStores->save($jabberStore);

      if ($jabberStore->socket_path != null) {
        dispatch(new ReplySocket(InlineJabber::JAXL_CHANGE_STATUS, [
          'new_message' => $data['status_message']
        ], $jabberStore->socket_path));
      }

      return $listener->response(200);
    }
    return $listener->response(400, 'You haven\'t registered this account to Jabber yet.');
  }

}
