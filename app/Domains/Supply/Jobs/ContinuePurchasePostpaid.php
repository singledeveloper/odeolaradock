<?php

namespace Odeo\Domains\Supply\Jobs;

use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Supply\Biller\PostpaidPurchaser;
use Odeo\Jobs\Job;

class ContinuePurchasePostpaid extends Job {

  use SerializesModels;

  private $switcherId;

  public function __construct($switcherId) {
    parent::__construct();
    $this->switcherId = $switcherId;
  }

  public function handle() {

    $pipeline = new Pipeline();
    $pipeline->add(new Task(PostpaidPurchaser::class, 'purchase', [
      'order_detail_pulsa_switcher_id' => $this->switcherId,
      'no_initiate' => true
    ]));
    $pipeline->execute([]);

  }

}
