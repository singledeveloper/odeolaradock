<?php

namespace Odeo\Domains\Supply\Jobs;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Constant\DataExporterType;
use Odeo\Domains\Constant\DataExporterView;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Exporter\Helper\DataExporter;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Jobs\Job;

class SendGroupPriceList extends Job  {

  protected $data;

  function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    $storeInventoryGroupPrices = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository::class);
    $storeInventoryGroupPriceDetails = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceDetailRepository::class);
    $currencyHelper = app()->make(Currency::class);

    $groupPriceIds = [];
    foreach ($storeInventoryGroupPrices->getBySupplyGroupId($this->data['supply_group_id']) as $item)
      $groupPriceIds[] = $item->id;

    $groupDetails = [[
      'pulsa_name' => 'Nama Pulsa',
      'price' => 'Harga'
    ]];

    foreach ($storeInventoryGroupPriceDetails->getAllDetails($groupPriceIds) as $item) {
      $groupDetails[] = [
        'pulsa_name' => $item->pulsa_name,
        'price' => $currencyHelper->formatPrice($item->sell_price)['formatted_amount']
      ];
    }

    $dataExporter = app()->make(DataExporter::class);
    $file = $dataExporter->generate($groupDetails, DataExporterType::CSV, DataExporterView::SUPPLY_GROUP_PRICE_LIST_KEY);

    Mail::send('emails.supply_price_list_mail', [
      'data' => [
        'date' => Carbon::now()->toDateTimeString()
      ]
    ], function ($m) use ($file) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->attachData($file, 'Price List - ' . time() . '.' . DataExporterType::CSV);
      $m->to($this->data['email'])->subject('Price List');
    });

  }
}
