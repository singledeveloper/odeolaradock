<?php

namespace Odeo\Domains\Supply\Jobs;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Constant\DataExporterType;
use Odeo\Domains\Constant\DataExporterView;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Exporter\Helper\DataExporter;
use Odeo\Jobs\Job;

class TestInquiryNotify extends Job  {

  private $trxId;

  function __construct($trxId) {
    parent::__construct();
    $this->trxId = $trxId;
  }

  public function handle() {

    $params = [
      'refid' => $this->trxId,
      'message' => 'PERHATIKAN TIKET BCA GANTI REK 
R# HBYRPLND.522540850545 SUKSES. SN/Ref: TJARYO SETIAJI (AM)/PTAG:122018/JBLN:1/TD:R1-450/SM:00007269-00007373/TAG:46434/ADM:2500/TTAG:48934./Ref:0BNI21G309045924603. Saldo 6.769.203 - 0 = 6.722.369 
ID Center Tlegram chat cs Total hari ini 614 Trx'
    ];

    $client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ]
    ]);

    $client->request('GET', 'https://api.staging.odeo.co.id/v1/pulsa/test-postpaid/notify?' . http_build_query($params));

  }
}
