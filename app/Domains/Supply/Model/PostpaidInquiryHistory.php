<?php

namespace Odeo\Domains\Supply\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\PulsaOdeoInventory;
use Odeo\Domains\Inventory\Model\VendorSwitcher;

class PostpaidInquiryHistory extends Entity
{
  public $timestamps = true;

  public function pulsaInventory() {
    return $this->belongsTo(PulsaOdeoInventory::class, 'inventory_id');
  }
}
