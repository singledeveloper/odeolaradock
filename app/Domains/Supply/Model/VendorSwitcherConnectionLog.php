<?php

namespace Odeo\Domains\Supply\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Model\VendorSwitcher;

class VendorSwitcherConnectionLog extends Entity
{
  public $timestamps = true;

  public function vendorSwitcher() {
    return $this->belongsTo(VendorSwitcher::class);
  }
}
