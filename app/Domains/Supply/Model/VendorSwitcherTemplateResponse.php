<?php

namespace Odeo\Domains\Supply\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Model\VendorSwitcher;

class VendorSwitcherTemplateResponse extends Entity
{
  public $timestamps = true;

  public function vendorSwitcher() {
    return $this->belongsTo(VendorSwitcher::class, 'predefined_vendor_switcher_id');
  }

  public function template() {
    return $this->belongsTo(VendorSwitcherTemplate::class, 'template_id');
  }
}
