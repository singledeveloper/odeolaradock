<?php

namespace Odeo\Domains\Supply\Repository;

use Carbon\Carbon;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Supply\Model\PostpaidInquiryHistory;

class PostpaidInquiryHistoryRepository extends Repository {

  public function __construct(PostpaidInquiryHistory $postpaidInquiryHistory) {
    $this->model = $postpaidInquiryHistory;
  }

  public function findRecentInquiry($number, $userId, $inventoryId, $type, $minutes = 5, $includePending = false) {
    $query = $this->getCloneModel()
      ->where('number', $number)
      ->where('user_id', $userId)
      ->where('inventory_id', $inventoryId)
      ->where('is_processed', false)
      ->where('reference_type', $type)
      ->where('updated_at', '>', Carbon::now()->subMinutes($minutes)->toDateTimeString());

    if ($includePending)
      $query = $query->whereIn('status', [SwitcherConfig::BILLER_SUCCESS, SwitcherConfig::BILLER_IN_QUEUE]);
    else $query = $query->where('status', SwitcherConfig::BILLER_SUCCESS);

    return $query->orderBy('id', 'desc')->first();
  }

  public function updateInquiry($number, $userId) {
    return $this->getCloneModel()
      ->where('number', $number)
      ->where('user_id', $userId)
      ->where('is_processed', false)
      ->update(['is_processed' => true]);
  }

  public function generateLastId() {
    $result = $this->getCloneModel()->orderBy('id', 'desc')->first();
    if (!$result) return 1;
    return $result->id + 1;
  }

}
