<?php

namespace Odeo\Domains\Supply\Repository;

use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Supply\Model\VendorSwitcherTemplateResponseDetail;

class VendorSwitcherTemplateResponseDetailRepository extends Repository {

  public function __construct(VendorSwitcherTemplateResponseDetail $vendorSwitcherTemplateResponseDetail) {
    $this->model = $vendorSwitcherTemplateResponseDetail;
  }

  public function getResponseCount($templateId, $statusType) {
    return $this->model->whereHas('response', function($query) use ($templateId) {
      $query->where('template_id', $templateId);
    })->where('response_status_type', $statusType)->count();
  }

  public function getResponseList($templateId, $vendorSwitcherId, $routeType) {
    $key = $this->model->getTable();

    return $this->runCache($key, $key . '.template.' . $templateId . ($vendorSwitcherId != null && $vendorSwitcherId != '0' ? ('.biller.' . $vendorSwitcherId) : '') . '.' . $routeType,
      60 * 60 * 24 * 30, function () use ($templateId, $vendorSwitcherId, $routeType) {
      return $this->model->join('vendor_switcher_template_responses', 'vendor_switcher_template_response_details.template_response_id', '=', 'vendor_switcher_template_responses.id')
        ->where('template_id', $templateId)
        ->where(function($query3) use ($routeType) {
          $query3->where('route_type', $routeType)->orWhere('route_type', Supplier::ROUTE_BOTH);
        })
        ->where('vendor_switcher_template_responses.is_active', true)
        ->where('vendor_switcher_template_response_details.is_active', true)
        ->where(function($query1) use ($vendorSwitcherId) {
          $query1->whereNull('vendor_switcher_template_responses.predefined_vendor_switcher_id')
            ->orWhere('vendor_switcher_template_responses.predefined_vendor_switcher_id', $vendorSwitcherId);
        })
        ->where(function($query2) use ($vendorSwitcherId) {
          $query2->whereNull('vendor_switcher_template_response_details.predefined_vendor_switcher_id')
            ->orWhere('vendor_switcher_template_response_details.predefined_vendor_switcher_id', $vendorSwitcherId);
        })
        ->orderBy('vendor_switcher_template_response_details.id', 'desc')
        ->get();
    });
  }

  public function getByResponseId($templateResponseId, $vendorSwitcherId = '') {
    return $this->model->where('template_response_id', $templateResponseId)
      //->whereHas('response', function($query){
      //  $query->where('route_type', '<>', Supplier::ROUTE_BOTH);
      //})
      ->where('is_active', true)->where(function($query) use ($vendorSwitcherId) {
        if ($vendorSwitcherId == '') $query->whereNull('predefined_vendor_switcher_id');
        else $query->whereNull('predefined_vendor_switcher_id')->orWhere('predefined_vendor_switcher_id', $vendorSwitcherId);
      })->get();
  }

  public function getBasicResponseList($templateId, $vendorSwitcherId) {
    return $this->model->join('vendor_switcher_template_responses', 'vendor_switcher_template_response_details.template_response_id', '=', 'vendor_switcher_template_responses.id')
      ->where('template_id', $templateId)
      ->where('vendor_switcher_template_responses.is_active', true)
      ->where('vendor_switcher_template_response_details.is_active', true)
      ->where('vendor_switcher_template_responses.format_type', Supplier::TYPE_TEXT)
      ->where('vendor_switcher_template_responses.route_type', Supplier::ROUTE_BOTH)
      ->where('vendor_switcher_template_response_details.predefined_vendor_switcher_id', $vendorSwitcherId)
      ->orderBy('vendor_switcher_template_response_details.id', 'desc')
      ->select('vendor_switcher_template_response_details.id', 'response_status_type', 'response_details', 'vendor_switcher_template_response_details.predefined_vendor_switcher_id')
      ->get();
  }

}
