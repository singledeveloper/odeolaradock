<?php

namespace Odeo\Domains\Supply\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Supply\Model\VendorSwitcherTemplateResponse;

class VendorSwitcherTemplateResponseRepository extends Repository {

  public function __construct(VendorSwitcherTemplateResponse $vendorSwitcherTemplateResponse) {
    $this->model = $vendorSwitcherTemplateResponse;
  }

  public function getByTemplateId($templateId, $vendorSwitcherId = '') {
    return $this->model->where('template_id', $templateId)
      ->where('is_active', true)->where(function($query) use ($vendorSwitcherId) {
        if ($vendorSwitcherId == '') $query->whereNull('predefined_vendor_switcher_id');
        else $query->whereNull('predefined_vendor_switcher_id')->orWhere('predefined_vendor_switcher_id', $vendorSwitcherId);
      })->get();
  }

  public function findByTemplateId($templateId, $vendorSwitcherId, $routeType) {
    return $this->model->where('template_id', $templateId)
      ->where('route_type', $routeType)->where('is_active', true)
      ->where('predefined_vendor_switcher_id', $vendorSwitcherId)
      ->first();
  }

  public function findByFormatType($templateId, $formatType) {
    return $this->model->where('template_id', $templateId)->where('format_type', $formatType)->first();
  }

}
