<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:54 PM
 */

namespace Odeo\Domains\Supply;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Supply\Jobs\SendGroupPriceList;

class SupplyInventoryGroupDetailSelector implements SelectorListener {

  private $storeInventoryGroupPrices, $storeInventoryGroupPriceDetails, $currency;

  public function __construct() {
    $this->storeInventoryGroupPrices = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository::class);
    $this->storeInventoryGroupPriceDetails = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceDetailRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {

    $groupPrices = $this->storeInventoryGroupPrices->getBySupplyGroupId($data['supply_group_id']);

    $supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    list ($isValid, $message) = $supplyValidator->checkStore($groupPrices[0]->storeInventory->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    $groupPriceIds = [];
    foreach ($groupPrices as $item) $groupPriceIds[] = $item->id;

    $groupDetails = [];

    foreach($this->storeInventoryGroupPriceDetails->getAllDetails($groupPriceIds) as $item) {
      $groupDetails[] = [
        'id' => $item->id,
        'pulsa' => [
          'id' => $item->pulsa_id,
          'name' => $item->pulsa_name
        ],
        'biller' => [
          'id' => $item->vendor_switcher_id,
          'name' => $item->vendor_switcher_name
        ],
        'biller_code' => $item->biller_code,
        'supply_code' => $item->supply_code,
        'base_price' => $this->currency->formatPrice($item->store_base_price),
        'sell_price' => $this->currency->formatPrice($item->sell_price),
        'price_diff' => $item->sell_price - $item->store_base_price,
        'is_editable' => true,
        'is_in_use' => true
      ];
    }

    if (sizeof($groupDetails) > 0)
      return $listener->response(200, ["group_details" => $groupDetails]);

    return $listener->response(204, ["group_details" => []]);
  }

  public function exportPriceList(PipelineListener $listener, $data) {
    $groupPrices = $this->storeInventoryGroupPrices->getBySupplyGroupId($data['supply_group_id']);
    $supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    list ($isValid, $message) = $supplyValidator->checkStore($groupPrices[0]->storeInventory->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    dispatch(new SendGroupPriceList($data));

    return $listener->response(200);
  }

}
