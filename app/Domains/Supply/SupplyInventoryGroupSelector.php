<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:54 PM
 */

namespace Odeo\Domains\Supply;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class SupplyInventoryGroupSelector implements SelectorListener {

  private $storeInventoryGroupPrices, $agentSupplyDefaults;

  public function __construct() {
    $this->storeInventoryGroupPrices = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository::class);
    $this->agentSupplyDefaults = app()->make(\Odeo\Domains\Agent\Repository\AgentSupplyDefaultRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {

    $supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    list ($isValid, $message) = $supplyValidator->checkStore($data['store_id'], $data);
    if (!$isValid) return $listener->response(400, $message);

    $groups = [];

    $this->storeInventoryGroupPrices->normalizeFilters($data);
    $this->storeInventoryGroupPrices->setSimplePaginate(true);

    $default = $this->agentSupplyDefaults->findByStoreId($data['store_id']);

    foreach($this->storeInventoryGroupPrices->getAllSupplyGroupPriceByStore($data) as $item) {
      $groups[] = [
        'id' => $item->supply_group_id,
        'name' => $item->name,
        'description' => $item->description,
        'set_as_default' => $default && $default->supply_group_id == $item->supply_group_id
      ];
    }

    if (sizeof($groups) > 0)
      return $listener->response(200, array_merge(
        ["groups" => $groups],
        $this->storeInventoryGroupPrices->getPagination()
      ));

    return $listener->response(204, ["groups" => []]);
  }

}
