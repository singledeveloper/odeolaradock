<?php

namespace Odeo\Domains\Supply;

use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\PipelineListener;

class TemplateRequestMaker {

  private $vendorSwitcherTemplates, $vendorSwitcherConnections, $supplyValidator, $typeFormatter, $vendorSwitcherTemplateResponses, $templateCache;

  public function __construct() {
    $this->vendorSwitcherTemplates = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateRepository::class);
    $this->vendorSwitcherConnections = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherConnectionRepository::class);
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    $this->typeFormatter = app()->make(\Odeo\Domains\Supply\Formatter\TypeManager::class);
    $this->vendorSwitcherTemplateResponses = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateResponseRepository::class);
    $this->templateCache = app()->make(\Odeo\Domains\Supply\Helper\CacheManager::class);
  }

  public function setRequest(PipelineListener $listener, $data) {

    list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
    if (!$isValid) return $listener->response(400, $message);

    $bodyTitle = isset($data['body_title']) && $data['body_title'] != '' ? $data['body_title'] : null;

    if (!in_array($data['format_type'], Supplier::getFormats()))
      return $listener->response(400, 'Format tidak valid.');
    else if ($data['format_type'] == Supplier::TYPE_XML && $bodyTitle == null)
      return $listener->response(400, "Body Title harus diisi jika menggunakan format XML.");

    if (isset($data['headers'])) {
      list ($isValid, $data['headers']) = $this->supplyValidator->checkHeader($data['headers']);
      if (!$isValid) return $listener->response(400, $data['headers']);
    }
    list ($isValid, $data['body_details']) = $this->supplyValidator->checkBody($data['body_details'], $data['format_type']);
    if (!$isValid) return $listener->response(400, $data['body_details']);

    $template = $this->vendorSwitcherTemplates->getNew();
    $template->name = $data['template_name'];
    $template->predefined_store_id = isAdmin() && isset($data['shared']) ? null : $data['store_id'];
    $template->format_type = $data['format_type'];
    $template->headers = isset($data['headers']) ? json_encode($data['headers']) : null;
    $template->body_title = $bodyTitle;
    $template->body_details = is_array($data['body_details']) ? json_encode($data['body_details']) : $data['body_details'];

    $this->vendorSwitcherTemplates->save($template);

    if ($data['format_type'] == Supplier::TYPE_TEXT) {
      $templateResponse = $this->vendorSwitcherTemplateResponses->getNew();
      $templateResponse->template_id = $template->id;
      $templateResponse->predefined_vendor_switcher_id = null;
      $templateResponse->format_type = $data['format_type'];
      $templateResponse->route_type = Supplier::ROUTE_REPORT;

      $this->vendorSwitcherTemplateResponses->save($templateResponse);
    }

    return $listener->response(200);
  }

  public function edit(PipelineListener $listener, $data) {
    if ($template = $this->vendorSwitcherTemplates->findById($data['template_id'])) {
      list ($isValid, $message) = $this->supplyValidator->checkStore($template->predefined_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      if ($this->vendorSwitcherConnections->findActiveBillerUseTemplate($data['template_id']))
        return $listener->response(400, 'Anda harus men-disable biller Anda terlebih dahulu.');

      $bodyTitle = isset($data['body_title']) && $data['body_title'] != '' ? $data['body_title'] : null;

      if (!in_array($data['format_type'], Supplier::getFormats()))
        return $listener->response(400, 'Format tidak valid.');
      else if ($data['format_type'] == Supplier::TYPE_XML && $bodyTitle == null)
        return $listener->response(400, "Body Title harus diisi jika menggunakan format XML.");

      if (isset($data['headers'])) {
        list ($isValid, $data['headers']) = $this->supplyValidator->checkHeader($data['headers']);
        if (!$isValid) return $listener->response(400, $data['headers']);
      }
      list ($isValid, $data['body_details']) = $this->supplyValidator->checkBody($data['body_details'], $data['format_type']);
      if (!$isValid) return $listener->response(400, $data['body_details']);

      $template->name = $data['template_name'];
      $template->format_type = $data['format_type'];
      $template->headers = isset($data['headers']) ? json_encode($data['headers']) : null;
      $template->body_title = $bodyTitle;
      $template->body_details = is_array($data['body_details']) ? json_encode($data['body_details']) : $data['body_details'];
      if (isAdmin()) $template->predefined_store_id = isset($data['shared']) ? null : $data['store_id'];

      $this->vendorSwitcherTemplates->save($template);

      if ($data['format_type'] == Supplier::TYPE_TEXT && !$this->vendorSwitcherTemplateResponses->findByFormatType($template->id, Supplier::TYPE_TEXT)) {
        $templateResponse = $this->vendorSwitcherTemplateResponses->getNew();
        $templateResponse->template_id = $template->id;
        $templateResponse->predefined_vendor_switcher_id = null;
        $templateResponse->format_type = $data['format_type'];
        $templateResponse->route_type = Supplier::ROUTE_REPORT;

        $this->vendorSwitcherTemplateResponses->save($templateResponse);
      }

      foreach ($this->vendorSwitcherConnections->getByTemplateId($template->id) as $item) {
        $this->templateCache->resetConnection($item->id);
      }

      return $listener->response(200);
    }

    return $listener->response(400, 'Data tidak tersedia.');
  }

  public function preview(PipelineListener $listener, $data) {
    if (!in_array($data['format_type'], Supplier::getFormats()))
      return $listener->response(400, 'Format tidak valid.');
    list ($isValid, $data['body_details']) = $this->supplyValidator->checkBody($data['body_details'], $data['format_type']);
    if (!$isValid) return $listener->response(400, $data['body_details']);

    $body = $this->typeFormatter->setPath($data['format_type'])->constructRequestPreview($data['body_details'], isset($data['body_title']) ? $data['body_title'] : null);
    return $listener->response(200, ['preview' => is_array($body) ? http_build_query($body) : $body]);
  }

}
