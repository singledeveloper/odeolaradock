<?php

namespace Odeo\Domains\Support\Qiscus;

use Carbon\Carbon;
use Odeo\Domains\Constant\QiscusUser;
use Odeo\Domains\Core\PipelineListener;

class QiscusUserSelector {

  public function __construct() {
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function getCsOdeoUserIds(PipelineListener $listener, $data) {

    $ids = $this->users->getCsOdeoUsers()->pluck('id');
    
    return $listener->response(200, $ids);

  }

}
