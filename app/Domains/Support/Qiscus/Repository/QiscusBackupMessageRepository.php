<?php

namespace Odeo\Domains\Support\Qiscus\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Support\Qiscus\Model\QiscusBackupMessage;

class QiscusBackupMessageRepository extends Repository {

  public function __construct(QiscusBackupMessage $qiscusBackupMessage) {
    $this->model = $qiscusBackupMessage;
  }

}
