<?php

namespace Odeo\Domains\Support\Qiscus\Scheduler;

use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Support\Qiscus\QiscusBackupRequester;

class QiscusBackupScheduler {

  public function run() {
    $pipeline = new Pipeline;
    $pipeline->add(new Task(QiscusBackupRequester::Class, 'create'));
    $pipeline->add(new Task(QiscusBackupRequester::Class, 'processMessages'));
    $pipeline->enableTransaction();
    $pipeline->execute([]);
  }

}