<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 30/07/19
 * Time: 19.15
 */

namespace Odeo\Domains\Support\Zoho\Helper;


use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class ZohoApi {

  static $CLIENT_ID;
  static $CLIENT_SECRET;

  const API_DOMAIN_URL =  "https://www.zohoapis.com/";
  const BASE_AUTH_URL = 'https://accounts.zoho.com/';
  const REDIRECT_URI = 'https://api.odeo.co.id/v1/zoho-auth';
  const SCOPE_MESSAGES = 'ZohoMail.messages.READ';
  const AUTH_CODE = '1000.73b3712863b0e16a4060786e7ce9b24a.a4c4d6638186b1db7e54b3d6637e4550';
  const REDIS_TOKEN_KEY = 'odeo-core:zoho_access_token';
  const REDIS_REFRESH_KEY = 'odeo-core:zoho_refresh_token';
  const REDIS_REFRESH_COUNT_KEY = 'odeo-core:zoho_refresh_count';
  const ACCOUNT_ID = '3825279000000008001';

  private $client, $redis;

  public static function init() {
    self::$CLIENT_ID = env('ZOHO_CLIENT_ID');
    self::$CLIENT_SECRET = env('ZOHO_CLIENT_SECRET');
  }

  function __construct() {
    self::init();
    $this->client = new Client(['timeout' => 90]);
    $this->redis = Redis::connection();
  }

  public function getAccessCodeUrl() {
    $scope = self::SCOPE_MESSAGES;
    $clientId = self::$CLIENT_ID;
    $accessType = 'offline';
    $uri = self::REDIRECT_URI;
    return self::BASE_AUTH_URL . "/oauth/v2/auth?scope=$scope&client_id=$clientId&response_type=code&access_type=$accessType&redirect_uri=$uri&prompt=consent";
  }

  public function getAccessToken() {
    if ($token = $this->redis->get(self::REDIS_TOKEN_KEY)) {
      return $token;
    }

//    $count = $this->redis->get(self::REDIS_REFRESH_COUNT_KEY) ?? 0;
    if (($refreshToken = $this->redis->get(self::REDIS_REFRESH_KEY))) {
      return $this->refreshAccessToken($refreshToken);
    }

    $res = json_decode($this->createRequest('POST',self::BASE_AUTH_URL . 'oauth/v2/token', [
      'client_id' => self::$CLIENT_ID,
      'client_secret' => self::$CLIENT_SECRET,
      'redirect_uri' => self::REDIRECT_URI,
      'code' => self::AUTH_CODE,
      'grant_type' => 'authorization_code'
    ]), true);

    $this->redis->setnx(self::REDIS_TOKEN_KEY, $res['access_token']);
    $this->redis->expire(self::REDIS_TOKEN_KEY, $res['expires_in_sec']);
    $this->redis->set(self::REDIS_REFRESH_KEY, $res['refresh_token']);
//    $this->redis->set(self::REDIS_REFRESH_COUNT_KEY, 0);

    return $res['access_token'];
  }

  public function getAccountDetails() {
    return $this->createRequest('GET', 'http://mail.zoho.com/api/accounts', [], true);
  }

  public function getAccountBniEdcSettlementMails() {
    $accountId = self::ACCOUNT_ID;
    return $this->createRequest('GET', "http://mail.zoho.com/api/accounts/$accountId/messages/view", [
      'limit' => 20,
      'folderId' => '3825279000000849001', // email folder id
      'sortBy' => 'date'
    ], true);
  }

  public function getAttachmentInfo($messageId, $folderId) {
    $accountId = self::ACCOUNT_ID;
    return $this->createRequest('GET', "http://mail.zoho.com/api/accounts/$accountId/folders/$folderId/messages/$messageId/attachmentinfo", [], true);
  }

  public function getAttachmentFile($folderId, $messageId, $attachmentId) {
    $accountId = self::ACCOUNT_ID;
    return $this->createRequest('GET', "https://mail.zoho.com/api/accounts/$accountId/folders/$folderId/messages/$messageId/attachments/$attachmentId", [], true);
  }

  private function refreshAccessToken($refreshToken) {
//    $refreshCount += 1;

    $res = json_decode($this->createRequest('POST', self::BASE_AUTH_URL . 'oauth/v2/token', [
      'refresh_token' => $refreshToken,
      'grant_type' => 'refresh_token',
      'client_id' => self::$CLIENT_ID,
      'client_secret' => self::$CLIENT_SECRET,
      'redirect_uri' => self::REDIRECT_URI,
      'scope' => self::SCOPE_MESSAGES
    ]), true);

//    $this->redis->set(self::REDIS_REFRESH_COUNT_KEY, $refreshCount);
    $this->redis->setnx(self::REDIS_TOKEN_KEY, $res['access_token']);
    $this->redis->expire(self::REDIS_TOKEN_KEY, $res['expires_in_sec']);

    return $res['access_token'];
  }

  private function createRequest($method, $url, $params = [], $withAuthorization = false) {
    if ($method == 'GET') {
      $paramKey = 'query';
    } else {
      $paramKey = 'form_params';
    }

    if ($withAuthorization) {
      $token = $this->getAccessToken();
      $headers = ['Authorization' => "Zoho-oauthtoken $token"];
    }

    $res = $this->client->request($method, $url, [
      $paramKey => $params,
      'headers' => $withAuthorization ? $headers : []
    ]);
    return $res->getBody()->getContents();
  }

}