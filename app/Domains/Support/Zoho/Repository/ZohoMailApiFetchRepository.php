<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/08/19
 * Time: 17.25
 */

namespace Odeo\Domains\Support\Zoho\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Support\Zoho\Model\ZohoMailApiFetch;

class ZohoMailApiFetchRepository extends Repository {

  function __construct(ZohoMailApiFetch $mailApiFetch) {
    $this->model = $mailApiFetch;
  }

  function getLastFetchedData() {
    return $this->model
      ->orderBy('trx_date', 'desc')
      ->limit(50)
      ->get();
  }

  function getFetchDownloadList() {
    return $this->model
      ->where('is_downloaded', false)
      ->whereNull('filename')
      ->get();
  }
}