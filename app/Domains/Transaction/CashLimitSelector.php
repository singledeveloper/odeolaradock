<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 02-Aug-18
 * Time: 11:40 AM
 */

namespace Odeo\Domains\Transaction;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Helper\CashLimiter;

class CashLimitSelector {

  private $cashLimiter;

  public function __construct() {
    $this->cashLimiter = app()->make(CashLimiter::class);
  }

  public function getTopUpWithdrawLimitAndAmount(PipelineListener $listener, $data) {
    $limitAndAmount = $this->cashLimiter->getCashLimitDetailed($data["auth"]["user_id"]);
    return $listener->response(200, $limitAndAmount);
  }

}