<?php

namespace Odeo\Domains\Transaction;

use Carbon\Carbon;
use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Order\PulsaRecurring\Helper\PulsaRecurringHelper;
use Odeo\Domains\Transaction\Helper\Currency;

class CashRecurringSelector implements SelectorListener {

  private $cashRecurringRepo, $currencyHelper, $recurringHelper, $cashRecurringHistoryRepo;

  public function __construct() {
    $this->cashRecurringRepo = app()->make(\Odeo\Domains\Transaction\Repository\CashRecurringRepository::class);
    $this->cashRecurringHistoryRepo = app()->make(\Odeo\Domains\Transaction\Repository\CashRecurringHistoryRepository::class);
    $this->currencyHelper = app()->make(Currency::class);
    $this->recurringHelper = app()->make(PulsaRecurringHelper::class);
    Carbon::setLocale(checkHeaderLanguage());
  }

  public function _transforms($item, Repository $repository) {
    $output = [];
    $bank = $item->bank;
    $nextDate = $item->next_execution_at ? $item->next_execution_at :
      $this->recurringHelper->getNextPayment($item->type, Carbon::now()->toDateTimeString());
    $output['id'] = $item->id;
    $output['bank_name'] = $bank ? $bank->name : 'Unknown';
    $output['destination'] = $item->destination_number;
    $output['amount'] = $this->currencyHelper->formatPrice($item->amount);
    $output['notes'] = $item->template_notes;
    $output['description'] = Recurring::getTypeName($item->type);
    $output['next_execution_date'] = date('Y-m-d H:i', strtotime($nextDate));
    $output['next_execution_message'] = Carbon::parse($nextDate)->diffForHumans();
    $output['is_enabled'] = $item->is_enabled ? true : false;
    return $output;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getLatest(PipelineListener $listener, $data) {
    $latest = [];
    $userId = $data['auth']['user_id'];
    foreach ($this->cashRecurringRepo->getLatest($userId, $data) as $item) {
      $latest[] = $this->_transforms($item, $this->cashRecurringRepo);
    }
    if (count($latest) > 0) {
      return $listener->response(200, ['cash_recurrings' => $latest]);
    }
    return $listener->response(204);
  }

  public function gets(PipelineListener $listener, $data) {
    $this->cashRecurringRepo->setSimplePaginate(true);
    $this->cashRecurringRepo->normalizeFilters($data);

    $latest = [];
    foreach ($this->cashRecurringRepo->gets($data['auth']['user_id']) as $item) {
      $latest[] = $this->_transforms($item, $this->cashRecurringRepo);
    }
    if (count($latest) > 0) {
      return $listener->response(200, array_merge(
        ["cash_recurrings" => $this->_extends($latest, $this->cashRecurringRepo)],
        $this->cashRecurringRepo->getPagination()
      ));
    }
    return $listener->response(204);
  }

  public function getHistories(PipelineListener $listener, $data) {
    $this->cashRecurringHistoryRepo->setSimplePaginate(true);
    $this->cashRecurringHistoryRepo->normalizeFilters($data);

    $latest = [];
    foreach ($this->cashRecurringHistoryRepo->gets($data['auth']['user_id']) as $item) {
      $latest[] = [
        'id' => $item->id,
        'bank_name' => $item->bank_name,
        'destination' => $item->destination_number,
        'amount' => $this->currencyHelper->formatPrice($item->amount),
        'notes' => $item->template_notes,
        'status' => $item->status,
        'status_message' => Recurring::toStatusMessage($item->status),
        'error_message' => $item->status == Recurring::RETRIED ?
          ('Retry at ' . date('Y-m-d H:i', strtotime($item->retried_at))) : $item->error_message,
        'created_at' => $item->created_at->format('Y-m-d H:i')
      ];
    }
    if (count($latest) > 0) {
      return $listener->response(200, array_merge(
        ["cash_recurring_histories" => $this->_extends($latest, $this->cashRecurringHistoryRepo)],
        $this->cashRecurringHistoryRepo->getPagination()
      ));
    }
    return $listener->response(204);
  }

}