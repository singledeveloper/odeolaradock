<?php

namespace Odeo\Domains\Transaction;

use Carbon\Carbon;
use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Helper\Currency;

class CashRecurringUpdater {

  private $cashRecurringRepo, $cashRecurringHistoryRepo, $currencyHelper;

  public function __construct() {
    $this->cashRecurringRepo = app()->make(\Odeo\Domains\Transaction\Repository\CashRecurringRepository::class);
    $this->cashRecurringHistoryRepo = app()->make(\Odeo\Domains\Transaction\Repository\CashRecurringHistoryRepository::class);
    $this->selector = app()->make(CashRecurringSelector::class);
    $this->currencyHelper = app()->make(Currency::class);
    Carbon::setLocale(checkHeaderLanguage());
  }

  public function toggle(PipelineListener $listener, $data) {
    if ($schedule = $this->cashRecurringRepo->findById($data['schedule_id'])) {
      if ($schedule->user_id != $data['auth']['user_id']) {
        return $listener->response(400, 'Invalid');
      }
      $schedule->is_enabled = !$schedule->is_enabled;
      $this->cashRecurringRepo->save($schedule);
      return $listener->response(200, $this->selector->_transforms($schedule, $this->cashRecurringRepo));
    }
    return $listener->response(400, 'Data not exist');
  }

  public function retry(PipelineListener $listener, $data) {
    if ($history = $this->cashRecurringHistoryRepo->findById($data['history_id'])) {
      if ($history->status != Recurring::FAILED) {
        return $listener->response(400, 'Invalid');
      }
      if ($schedule = $this->cashRecurringRepo->findById($history->cash_recurring_id)) {
        if ($schedule->user_id != $data['auth']['user_id']) {
          return $listener->response(400, 'Invalid');
        }
        $newSchedule = $schedule->replicate();

        $date = Carbon::now()->addHour();
        $h = $date->format('H');
        if ($h < 7) $h = 7;
        else if ($h > 20) {
          $date = Carbon::now()->addDay();
          $h = 7;
        }

        $newSchedule->type = Recurring::TYPE_ONCE . '_' . $date->format('Y-m-d') . '_' . $h;
        $newSchedule->last_executed_at = null;
        $newSchedule->is_active = true;
        $newSchedule->is_enabled = true;
        $newSchedule->created_at = date('Y-m-d H:i:s');
        $newSchedule->updated_at = date('Y-m-d H:i:s');
        $this->cashRecurringRepo->save($newSchedule);

        $history->retried_at = $date->format('Y-m-d') . ' ' . ($h < 10 ? '0' : '') . $h . ':00:00';
        $history->status = Recurring::RETRIED;
        $this->cashRecurringHistoryRepo->save($history);

        $bank = $schedule->bank;
        return $listener->response(200, [
          'id' => $history->id,
          'bank_name' => $bank ? $bank->name : '',
          'destination' => $schedule->destination_number,
          'amount' => $this->currencyHelper->formatPrice($schedule->amount),
          'notes' => $schedule->template_notes,
          'status' => $history->status,
          'status_message' => Recurring::toStatusMessage($history->status),
          'error_message' => 'Retry at ' . date('Y-m-d H:i', strtotime($history->retried_at)),
          'created_at' => $history->created_at->format('Y-m-d H:i')
        ]);
      }
      return $listener->response(400, 'Data not exist');
    }
    return $listener->response(400, 'Data not exist');
  }

}