<?php

namespace Odeo\Domains\Transaction;

use Odeo\Domains\Core\PipelineListener;

class CashRequester {

  public function __construct() {
    $this->tempCash = app()->make(\Odeo\Domains\Transaction\Repository\UserTempCashRepository::class);
    $this->cashTransactions = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
  }

  public function settleOrder(PipelineListener $listener, $data) {
    $tempCashes = $this->tempCash->findAllByAttributes('order_id', $data['order_id']);
    $tempCashes->load('cashTransaction');

    foreach($tempCashes as $tempCash) {
      $cashTransaction = $tempCash->cashTransaction;
      if($data = json_decode($cashTransaction->data, true)) {
        unset($data['settlement_at']);
        $cashTransaction->data = json_encode($data);
        $this->cashTransactions->save($cashTransaction);
      }
      $tempCash->delete();
    }

    return $listener->response(200);
  }

}
