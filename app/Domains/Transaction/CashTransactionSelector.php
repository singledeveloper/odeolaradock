<?php

namespace Odeo\Domains\Transaction;

use Carbon\Carbon;
use Odeo\Domains\Constant\MutationStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Transaction\Helper\CashTransactionDetailizer;
use Odeo\Domains\Transaction\Repository\CashTransactionRepository;

class CashTransactionSelector implements SelectorListener {

  private $transaction, $recentSelector, $detailizer;

  public function __construct() {
    $this->transaction = app()->make(CashTransactionRepository::class);
    $this->recentSelector = app()->make(RecentSelector::class);
    $this->detailizer = app()->make(CashTransactionDetailizer::class);
  }

  public function _transforms($item, Repository $repository) {
    $output = [];
    $output['mutation'] = $item;

    $isNotSettled = isset($item['settlement_at']) && $item['settlement_at'] >= Carbon::now()->toDateTimeString();
    $output['status'] = $isNotSettled ? MutationStatus::PENDING :
      ($output['mutation']['transaction_amount']['amount'] > 0 ? MutationStatus::IN : MutationStatus::OUT);

    $output['title'] = explode(" Order ", $item['description'])[0];

    $output = array_merge($output, $this->detailizer->detailizeItem($item));

    return $output;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {
    $this->transaction->normalizeFilters($data);
    $this->transaction->setSimplePaginate(true);

    $transactions = [];

    foreach ($this->transaction->gets() as $transaction) {
      $transactions[] = $transaction;
    }

    if (sizeof($transactions) > 0)
      return $listener->response(200, array_merge(
        ["transactions" => $this->_extends($transactions, $this->transaction)],
        $this->transaction->getPagination()
      ));
    return $listener->response(204, ["transactions" => []]);
  }

  public function find(PipelineListener $listener, $data) {
    $this->transaction->normalizeFilters($data);

    $transaction = $this->transaction->findTransaction($data['cash_transactions_id']);

    if ($transaction) {
      $transaction = $this->recentSelector->_transforms($transaction, $this->transaction);
      $transaction = $this->_transforms($transaction, $this->transaction);

      return $listener->response(200, $this->_extends($transaction, $this->transaction));
    }

    return $listener->response(204, []);
  }
}
