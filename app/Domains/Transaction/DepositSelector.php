<?php

namespace Odeo\Domains\Transaction;

use Odeo\Domains\Constant\StoreDeposit;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class DepositSelector implements SelectorListener {

  private $storeDeposits, $currency;

  public function __construct() {
    $this->storeDeposits = app()->make(\Odeo\Domains\Transaction\Repository\StoreDepositRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function _transforms($item, Repository $repository) {

    $data['store_deposit_id'] = $item->id;
    $data['store_id'] = $item->store_id;
    $data['amount'] = $this->currency->formatPrice($item->amount);

    if ($item->status) {
      if ($item->status == StoreDeposit::PENDING) $status = "PENDING";
      else if ($item->status == StoreDeposit::COMPLETED) $status = "COMPLETED";
      else if ($item->status == StoreDeposit::CANCELLED) $status = "CANCELLED";
      else $status = "UNKNOWN";
      $data["status"] = $status;
    }

    if ($item->created_at) $data["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    if ($item->updated_at) $data["updated_at"] = $item->updated_at->format("Y-m-d H:i:s");

    return $data;
  }

  public function _extends($data, Repository $repository) {
    if ($storeDepositIds = $repository->beginExtend($data, "store_deposit_id")) {
      if ($repository->hasExpand('order')) {
        $order = app()->make(\Odeo\Domains\Order\Helper\OrderManager::class);
        $result = $order->getByStoreDepositId($storeDepositIds);
        $repository->addExtend('order', $result);
      }
    }
    return $repository->finalizeExtend($data);
  }


  public function getDetail(PipelineListener $listener, $data) {

    $this->storeDeposits->normalizeFilters($data);

    if (isset($data["has_pending_deposit"])) {
      if ($storeDeposit = $this->storeDeposits->findLastStoreDeposit($data["store_id"])) {
        return $listener->response(200, $this->_extends($this->_transforms($storeDeposit, $this->storeDeposits), $this->storeDeposits));
      }
    }

    return $listener->response(204);
  }
}
