<?php

namespace Odeo\Domains\Transaction\Helper;

use DB;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;

class CashInserter {

  private $rows = [];

  public function __construct() {
    $this->cash = app()->make(\Odeo\Domains\Transaction\Repository\UserCashRepository::class);
    $this->cashTransaction = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
    $this->userTempCash = app()->make(\Odeo\Domains\Transaction\Repository\UserTempCashRepository::class);
    $this->storeTempDeposit = app()->make(\Odeo\Domains\Transaction\Repository\StoreTempDepositRepository::class);
  }

  public function add($row) {
    $this->rows[] = $row;
  }

  public function clear() {
    $this->rows = [];
  }

  public function removeRow($key, $value) {
    $newRow = [];
    foreach ($this->rows as $row) {
      if (!(isset($row[$key]) && $row[$key] == $value)) {
        $newRow[] = $row;
      }
    }
    $this->rows = $newRow;
  }

  public function run() {
    $cashTransactionIds = [];
    if (!count($this->rows)) return;
    DB::beginTransaction();
    $this->cash->lock();
    try {
      foreach ($this->rows as $row) {
        if ($row['amount'] == 0) continue;
        if (isset($row['store_id']) && (isset($row['cash_type']) && $row['cash_type'] == CashType::OCASH)) {
          if ($data = json_decode($row['data'], true)) {
            $data['store_id'] = $row['store_id'];
            $row['data'] = json_encode($data);
          } else {
            $row['data'] = json_encode([
              'store_id' => $row['store_id']
            ]);
          }
          unset($row['store_id']);
        }
        if (isset($row['store_id'])) {
          $balance = $this->cash->findByStoreId($row['user_id'], $row['store_id'], $row['cash_type']);
        } else {
          $balance = $this->cash->findByUserId($row['user_id'], $row['cash_type']);
        }

        $cashTransaction = $this->cashTransaction->getNew();
        $cashTransaction->fill($row);
        if (isset($row['data']) && $data = json_decode($row['data'])) {
          if (isset($data->order_id)) {
            $cashTransaction->order_id = $data->order_id;
          }
          if (isset($data->settlement_at)) {
            $cashTransaction->settlement_at = $data->settlement_at;
          }
        }
        $cashTransaction->balance_before = $balance ? $balance->amount : 0;
        $cashTransaction->balance_after = $cashTransaction->balance_before + $row['amount'];
        $this->cashTransaction->save($cashTransaction);

        if (isset($row['settlement_at'])) {
          $userTempCash = $this->userTempCash->getNew();
          $userTempCash->fill($row);
          if (isset($data) && isset($data->order_id)) $userTempCash->order_id = $data->order_id;
          $userTempCash->cash_transaction_id = $cashTransaction->id;
          $this->userTempCash->save($userTempCash);
        }

        if ($balance) $balance->amount += $row['amount'];
        else {
          $balance = $this->cash->getNew();
          $balance->user_id = $row['user_id'];
          if (isset($row['store_id'])) {
            $balance->store_id = $row['store_id'];
          }
          $balance->cash_type = $row['cash_type'];
          $balance->amount = $row['amount'];
        }

        if ($balance->amount < 0) {
          throw new InsufficientFundException(isset($row['flag']) ? $row['flag'] : '');
        }
        $this->cash->save($balance);

        $cashTransactionIds[] = $cashTransaction->id;
      }
    } catch (\Exception $e) {
      DB::rollback();
      throw $e;
    }
    DB::commit();

    return $cashTransactionIds;
  }
}
