<?php

namespace Odeo\Domains\Transaction\Helper;

use Odeo\Domains\Account\Repository\UserGroupRepository;
use Odeo\Domains\Account\UserKtpValidator;
use Odeo\Domains\Constant\CashConfig;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;

class CashLimiter {

  const THROTTLED_USER = [
    //100139 => 200000000 //AMC
    112342 => 4000000000, //easycash
    132238 => 2000000000, //kuredi
    545 => 10000000000, //vincent
    81915 => 1000000000, // rio
    88764 => 1000000000, //ezeelink
    127372 => 1000000000 // andystrip singapore
  ];

  private $users, $userStores, $jabberUsers, $userKtps, $cashManager, $h2hManager, $userGroupRepo,
    $cashTransactions, $currency, $affiliateRepo, $disbursementApiUserRepo, $userKtpValidator;

  public function __construct() {
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->userStores = app()->make(\Odeo\Domains\Subscription\Repository\UserStoreRepository::class);
    $this->jabberUsers = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberUserRepository::class);
    $this->userKtps = app()->make(\Odeo\Domains\Account\Repository\UserKtpRepository::class);
    $this->cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $this->cashTransactions = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
    $this->withdraw = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->affiliateRepo = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateRepository::class);
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->h2hManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper\PulsaH2HManager::class);
    $this->userKtpValidator = app()->make(UserKtpValidator::class);
    $this->userGroupRepo = app()->make(UserGroupRepository::class);
  }


  private function throttledUser($userId) {
    return in_array($userId, array_keys(self::THROTTLED_USER));
  }

  public function check($userId, $amount, $subject = 'Anda') {
    if (!$this->userGroupRepo->findUserWithinGroup($userId, UserType::GROUP_BYPASS_CASH_LIMIT)) {
      $maxCash = $this->getMaximumUserCashAmount($userId);

      $cash = $this->cashManager->getCashBalance($userId, Currency::IDR);

      if ($cash[$userId]['amount'] + $amount > $maxCash) {
        $errorMessage = $subject . ' tidak dapat mempunyai oCash melebihi ' . $this->currency->formatPrice($maxCash)['formatted_amount'] . '.';
        // have to refactor below
        if ($maxCash == CashConfig::MAXIMUM_CASH_PER_ACCOUNT) {
          $errorMessage .= ' ' . $subject . ' dapat meng-upload KTP-nya untuk menambahkan limit oCash ke ' .
            $this->currency->formatPrice(CashConfig::MAXIMUM_CASH_PER_ACCOUNT_PLUS)['formatted_amount'] . '.';
        }
        return [false, $errorMessage];
      }

      list($toppedAmount, $maximumCashInParticular, $time) = $this
        ->getAttributeForMaxTopUpTime($userId, $maxCash);

      if ($toppedAmount->total_amount + $amount > $maximumCashInParticular) {
        return [false, trans('errors.cash.maximum_cash_in_particular_time', [
          'subject' => $subject,
          'amount' => $this->currency->formatPrice($maximumCashInParticular)['formatted_amount'],
          'time' => $time
        ])];

      }
    }

    return [true, ''];
  }

  public function isWithdrawOverlimit($userId, $withdrawAmount, $currency) {
    $amount = $this->formatAmountOnly($withdrawAmount, $currency);
    $withdrawDayLimit = $this->getWithdrawDayLimit($userId);
    if ($withdrawDayLimit > 0) {
      $amountWithdrew = $this->currency->getAmountOnly($this->withdraw->getTodayLimit($userId), $currency);
      if ($amountWithdrew + $amount > $withdrawDayLimit) {
        return [true, "Withdraw overlimit. You only can withdraw " . $this->currency->getFormattedOnly($withdrawDayLimit - $amountWithdrew, $currency) . " today."];
      }
    }
    return [false, ''];
  }

  private function getAttributeForMaxTopUpTime($userId, $maxCash) {
    if ($this->throttledUser($userId)) {
      return [
        $this->cashTransactions->getCurrentDayTopUp($userId),
        $maxCash,
        'hari'
      ];
    } else {
      return [
        $this->cashTransactions->getCurrentMonthTopup($userId),
        CashConfig::MAXIMUM_CASH_IN_PER_MONTH,
        'bulan'
      ];
    }
  }

  public function getCashLimit($userId) {
    $maxOcash = $this->getMaximumUserCashAmount($userId);
    $ocash = $this->cashManager->getCashBalance($userId, Currency::IDR);
    $remainingOcashFromMax = max($maxOcash - $ocash[$userId]['amount'], 0);

    $limitAmount = $this->getCashLimitDetailed($userId);
    $topupDayRemaining = max($limitAmount['topup']['day_limit'] - $limitAmount['topup']['day_amount'], 0);
    $topupMonthRemaining = max($limitAmount['topup']['month_limit'] - $limitAmount['topup']['month_amount'], 0);
    $withdrawDayRemaining = $limitAmount['withdraw']['day_limit'] - $limitAmount['withdraw']['day_amount'];

    /// TOP UP ///
    // for normal user, has no master
    $topUpLimit = min($remainingOcashFromMax, $topupDayRemaining, $topupMonthRemaining);

    // for normal user, has master
    if (!$this->userHasNotConfigureAgent($userId)) {
      $topUpLimit = null;
    } else if ($this->throttledUser($userId)) {
      // for throttledUser
      $topUpLimit = min($remainingOcashFromMax, $topupDayRemaining);
    }
    /// END TOP UP ///

    /// WITHDRAW ///
    // all users only have day limit. If the day limit = 0, means it is bypassed
    $withdrawLimit = null;
    if ($limitAmount['withdraw']['day_limit'] > 0) {
      $withdrawLimit = max(0, $withdrawDayRemaining);
    }
    /// END WITHDRAW ///

    $result = [
      'is_verified' => $this->userKtpValidator->isVerified($userId),
    ];

    if ($topUpLimit !== null) {
      $result['topup_limit'] = $this->currency->formatPrice($topUpLimit);
    }

    if ($withdrawLimit !== null) {
      $result['withdraw_limit'] = $this->currency->formatPrice($withdrawLimit);
    }

    return $result;
  }

  private function formatAmountOnly($amount, $currency = Currency::IDR) {
    return $this->currency->getAmountOnly($amount, $currency);
  }

  private function getWithdrawDayLimit($userId, $currency = Currency::IDR) {
    // if is bypassed, no limit. Otherwise, maximum = maximum cash out per day
    if ($this->userGroupRepo->findUserWithinGroup($userId, UserType::GROUP_BYPASS_WITHDRAW_LIMIT)
      || $this->userHasSupplyStore($userId)
    ) {
      return 0;
    }

    return $this->formatAmountOnly(CashConfig::MAXIMUM_CASH_OUT_PER_DAY, $currency);
  }

  private function getTopUpDayLimit($userId) {
    // if not bypassed and is not an agent, maximum = maximum ocash allowed
    if ($this->userHasNotConfigureAgent($userId)) {
      return $this->formatAmountOnly($this->getMaximumUserCashAmount($userId));
    }

    return 0;
  }

  private function getTopUpMonthLimit($userId) {
    // if throttled user, no limit. Otherwise, maximum = maximum cash in per month allowed
    if ($this->throttledUser($userId) || !$this->userHasNotConfigureAgent($userId)) {
      return 0;
    }

    return $this->formatAmountOnly(CashConfig::MAXIMUM_CASH_IN_PER_MONTH);
  }

  public function getCashLimitDetailed($userId) {
    // Limits
    $topUpDayLimit = $this->getTopUpDayLimit($userId);
    $topUpMonthLimit = $this->getTopUpMonthLimit($userId);
    $withdrawDayLimit = $this->getWithdrawDayLimit($userId);
    $withdrawMonthLimit = 0; // no limit

    // Amount withdrew and top-up-ed
    $amountWithdrewDay = $this->formatAmountOnly($this->withdraw->getTodayLimit($userId));
    $amountWithdrewMonth = $this->formatAmountOnly($this->withdraw->getCurrentMonthTotalWithdraw($userId));
    $amountTopUpedDay = $this->formatAmountOnly($this->cashTransactions->getCurrentDayTopUp($userId)->total_amount);
    $amountTopUpedMonth = $this->formatAmountOnly($this->cashTransactions->getCurrentMonthTopup($userId)->total_amount);

    return [
      "topup" => [
        "day_limit" => $topUpDayLimit,
        "month_limit" => $topUpMonthLimit,
        "day_amount" => $amountTopUpedDay,
        "month_amount" => $amountTopUpedMonth
      ],
      "withdraw" => [
        "day_limit" => $withdrawDayLimit,
        "month_limit" => $withdrawMonthLimit,
        "day_amount" => $amountWithdrewDay,
        "month_amount" => $amountWithdrewMonth
      ]
    ];
  }

  public function getMaximumUserCashAmount($userId) {
    if ($this->throttledUser($userId)) {
      return self::THROTTLED_USER[$userId];
    }

    return $this->userKtpValidator->isVerified($userId) ? CashConfig::MAXIMUM_CASH_PER_ACCOUNT_PLUS : CashConfig::MAXIMUM_CASH_PER_ACCOUNT;
  }

  public function userHasNotConfigureAgent($userId) {
    $user = $this->users->findById($userId);

    if ($this->throttledUser($userId)) return true;

    return $user->purchase_preferred_store_id == null
      && !$this->userStores->hasPayedStore($userId)
      && !$this->disbursementApiUserRepo->findByUserId($userId)
      && !$this->h2hManager->isUserH2H($userId)
      && !$this->userGroupRepo->findUserWithinGroup($userId, UserType::GROUP_BYPASS_CASH_LIMIT);
  }

  public function userHasSupplyStore($userId) {
    return $this->userStores->findSupplyStore($userId);
  }
}
