<?php

namespace Odeo\Domains\Transaction\Helper;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Jobs\SendOCashWarning;
use Odeo\Domains\Constant\CashConfig;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;

class CashManager {

  public function __construct() {
    $this->cash = app()->make(\Odeo\Domains\Transaction\Repository\UserCashRepository::class);
    $this->depositTemp = app()->make(\Odeo\Domains\Transaction\Repository\StoreTempDepositRepository::class);
    $this->cashTemp = app()->make(\Odeo\Domains\Transaction\Repository\UserTempCashRepository::class);
    $this->revenue = app()->make(\Odeo\Domains\Transaction\Repository\CashRevenueRepository::class);
    $this->cashTransaction = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function getStoreDepositBalance($storeIds, $symbol = "", &$temp_deposits = []) {
    $deposits = [];

    if (!is_array($storeIds)) $storeIds = [$storeIds];

    if ($data = $this->cash->findStoreBalance($storeIds, CashType::ODEPOSIT)) {
      $store_ids_temp = [];
      foreach ($data as $store_id => $amount) {
        if (!in_array($store_id, $store_ids_temp)) $store_ids_temp[] = $store_id;
      }

      $temp_deposits = [];
      if (sizeof($store_ids_temp) > 0) {
        $temp_deposits = $this->depositTemp->sumAgentServerAmountGroupByStoreId($store_ids_temp);
      }

      foreach ($data as $store_id => $amount) {
        if (isset($temp_deposits[$store_id])) $amount -= $temp_deposits[$store_id];
        else $temp_deposits[$store_id] = 0;
        $deposits[$store_id] = ($symbol != "") ? $this->currency->formatPrice($amount, $symbol) : $amount;
        $temp_deposits[$store_id] = ($symbol != "") ? $this->currency->formatPrice($temp_deposits[$store_id], $symbol) : $temp_deposits[$store_id];
      }
    }

    foreach ($storeIds as $item) {
      if (!isset($deposits[$item])) {
        $deposits[$item] = ($symbol != "") ? $this->currency->formatPrice(0, $symbol) : 0;
      }
      if (!isset($temp_deposits[$item])) {
        $temp_deposits[$item] = ($symbol != "") ? $this->currency->formatPrice(0, $symbol) : 0;
      }
    }
    return $deposits;
  }

  public function getUserDepositBalance($userIds, $symbol = "", &$temp_user_deposits = []) {
    $data = $this->cash->findUserDeposit($userIds);
    $deposits = [];
    if (sizeof($data) > 0) {

      $store_temp = [];
      $store_ids_temp = [];
      foreach ($data as $item) {
        if (!isset($store_temp[$item->user_id])) $store_temp[$item->user_id] = [];
        $store_temp[$item->user_id][$item->store_id] = $item->total_amount;
        $store_ids_temp[] = $item->store_id;
      }

      $temp_deposits = [];
      if (sizeof($store_ids_temp) > 0) {
        $temp_deposits = $this->depositTemp->sumGroupByStoreId($store_ids_temp);
      }

      foreach ($store_temp as $userId => $store_ids) {
        $deposits[$userId] = 0;
        $temp_user_deposits[$userId] = 0;
        foreach ($store_ids as $store_id => $amount) {
          if (isset($temp_deposits[$store_id])) {
            $amount -= $temp_deposits[$store_id];
            $temp_user_deposits[$userId] += $temp_deposits[$store_id];
          }
          $deposits[$userId] += $amount;
        }
        $deposits[$userId] = ($symbol != "") ? $this->currency->formatPrice($deposits[$userId], $symbol) : $deposits[$userId];
        $temp_user_deposits[$userId] = ($symbol != "") ? $this->currency->formatPrice($temp_user_deposits[$userId], $symbol) : $temp_user_deposits[$userId];
      }
      return $deposits;
    }

    foreach ($userIds as $item) {
      $deposits[$item] = ($symbol != "") ? $this->currency->formatPrice(0, $symbol) : 0;
      $temp_user_deposits[$item] = ($symbol != "") ? $this->currency->formatPrice(0, $symbol) : 0;
    }

    return $deposits;
  }

  public function getStoreBalance($storeIds, $cashType, $symbol = "") {
    $data = $this->cash->findStoreBalance($storeIds, $cashType);

    if (!is_array($storeIds)) $storeIds = [$storeIds];
    foreach ($storeIds as $item) {
      if (!isset($data[$item])) $data[$item] = 0;
      $data[$item] = ($symbol != "") ? $this->currency->formatPrice($data[$item], $symbol) : $data[$item];
    }

    return $data;
  }

  public function getCashBalance($userIds, $symbol = "", &$userTempCashes = []) {
    $cashes = [];

    if (!is_array($userIds)) $userIds = [$userIds];

    $data = $this->cash->findCash($userIds);
    if ($data) {
      $tempUserIds = [];
      foreach ($data as $userId => $amount) {
        if (!in_array($userId, $tempUserIds)) $tempUserIds[] = $userId;
      }

      $userTempCashes = [];
      if (sizeof($tempUserIds) > 0) {
        $userTempCashes = $this->cashTemp->getSumByUsers($tempUserIds);
      }

      foreach ($data as $userId => $amount) {
        if (isset($userTempCashes[$userId])) $amount -= $userTempCashes[$userId];
        else $userTempCashes[$userId] = 0;
        $cashes[$userId] = ($symbol != "") ? $this->currency->formatPrice($amount, $symbol) : $amount;
        $userTempCashes[$userId] = ($symbol != "") ? $this->currency->formatPrice($userTempCashes[$userId], $symbol) : $userTempCashes[$userId];
      }
    }

    foreach ($userIds as $item) {
      if (!isset($cashes[$item])) {
        $cashes[$item] = ($symbol != "") ? $this->currency->formatPrice(0, $symbol) : 0;
      }
      if (!isset($userTempCashes[$item])) {
        $userTempCashes[$item] = ($symbol != "") ? $this->currency->formatPrice(0, $symbol) : 0;
      }
    }
    return $cashes;
  }

  public function getStoreCash($storeIds, $symbol = "", $cashType) {
    if (!is_array($storeIds)) $storeIds = [$storeIds];

    $data = $this->cash->findByStoreIds($storeIds, $cashType);
    foreach ($storeIds as $item) {
      if (!isset($data[$item])) $data[$item] = 0;
      $data[$item] = ($symbol != "") ? $this->currency->formatPrice($data[$item], $symbol) : $data[$item];
    }

    return $data;
  }

  public function getTodayRevenue($userIds, $symbol = "") {
    $data = $this->revenue->getUserToday($userIds);

    if (!is_array($userIds)) $userIds = [$userIds];
    foreach ($userIds as $item) {
      if (!isset($data[$item])) $data[$item] = 0;
      $data[$item] = ($symbol != "") ? $this->currency->formatPrice($data[$item], $symbol) : $data[$item];
    }

    return $data;
  }

  public function getStoreTodayRevenue($storeIds, $symbol = "") {
    $data = $this->revenue->getStoreToday($storeIds);

    if (!is_array($storeIds)) $storeIds = [$storeIds];
    foreach ($storeIds as $item) {
      if (!isset($data[$item])) $data[$item] = 0;
      $data[$item] = ($symbol != "") ? $this->currency->formatPrice($data[$item], $symbol) : $data[$item];
    }

    return $data;
  }

  public function getStoreTodayProfit($storeIds, $type = "", $trx_types = [], $detailed = false) {
    $data = $this->cashTransaction->getStoreProfit($storeIds, $type, $trx_types, $detailed);

    if ($detailed) {
      $todays = [];
      foreach ($data as $item) {
        if (!isset($todays[$item->store_id])) {
          $todays[$item->store_id] = [];
        }
        $todays[$item->store_id][$item->trx_type] = $item->total_amount;
      }
      return $todays;
    }

    return $data;
  }

  public function checkNotifyOCash($userId, $cashAmount = 0) {
    $notificationBalanceRepo = app()->make(\Odeo\Domains\Notification\Repository\NotificationBalanceRepository::class);

    if ($setting = $notificationBalanceRepo->findByUserId($userId)) {
      if ($cashAmount == 0) {
        if ($cash = $this->cash->findByUserId($userId, CashType::OCASH)) {
          $cashAmount = $cash->amount;
        }
      }
      if ($setting->amount > $cashAmount) {
        $redis = Redis::connection();
        if ($redis->setnx('cash_notify_' . $userId, 0)) {
          $redis->expire('cash_notify_' . $userId, 1800);

          dispatch(new SendOCashWarning($userId, $cashAmount));
        }
      }
    }
  }

  public function findOrderHistory($order, $type, $isNotify = false) {
    if ($transaction = $this->cashTransaction->findByOrderId($order->id, $type)) {
      $cashAmount = $transaction->balance_after;
      $userId = $transaction->user_id;
      $result = [
        'balance_before' => $transaction->balance_before,
        'amount' => $transaction->amount,
        'balance_after' => $transaction->balance_after,
        'message' => number_format($transaction->balance_before, 0) .
          ($transaction->amount < 0 ? ' - ' : ' + ') . number_format(abs($transaction->amount), 0) . ' = ' .
          $this->currency->formatPrice($transaction->balance_after)['formatted_amount']
      ];
    }
    else {
      $cash = $this->getCashBalance($order->user_id, Currency::IDR);
      $cashAmount = $cash[$order->user_id]['amount'];
      $userId = $order->user_id;
      if ($type == TransactionType::REFUND) $cashAmount+= $order->total;
      $result = [
        'balance_before' => null,
        'amount' => null,
        'balance_after' => null,
        'message' => $this->currency->formatPrice($cashAmount)['formatted_amount']
      ];
    }

    if ($isNotify && $userId) {
      $this->checkNotifyOCash($userId, $cashAmount);
    }

    return $result;
  }
}
