<?php

namespace Odeo\Domains\Transaction\Helper;

use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Order\Helper\OrderParser;
use Odeo\Domains\Order\Helper\StatusParser;
use Odeo\Domains\Subscription\Repository\StoreRepository;

class CashTransactionDetailizer {

  private $orders, $orderSelector, $withdraws, $withdrawSelector, $orderParser, $statusParser, $storeRepo;

  public function __construct() {
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->storeRepo = app()->make(StoreRepository::class);
    $this->orderSelector = app()->make(\Odeo\Domains\Order\OrderSelector::class);
    $this->withdraws = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);
    $this->withdrawSelector = app()->make(\Odeo\Domains\Transaction\WithdrawSelector::class);
    $this->orderParser = app()->make(OrderParser::class);
    $this->statusParser = app()->make(StatusParser::class);
  }

  public function detailizeItem($item) {
    $output = [];
    $output['detail'] = [];

    switch ($item['trx_type']) {
      case TransactionType::ORDER_CONVERT:
        $output['detail'] = [
          [
            'label' => trans('transaction.mutation_id'),
            'value' => $item['id']
          ],
        ];

        if (isset($item['data']) && isset($item['data']->store_id)) {
          $store = $this->storeRepo->findById($item['data']->store_id);

          $output['detail'][] = [
            'label' => trans('transaction.store_name'),
            'value' => $store['name']
          ];
        }

        if (isset($item['tags']) && count($item['tags']) >= 0) {
          $tag = $item['tags'][0];
          $output['detail'][] = [
            'label' => trans('transaction.via'),
            'value' => $tag['text']
          ];
        }

        break;

      case TransactionType::PAYMENT:
      case TransactionType::ORDER_REFUND:
      case TransactionType::REFUND:
      case TransactionType::TOPUP:
      case TransactionType::UNIQUE_CODE_REFUND:
        $this->orders->normalizeFilters([
          'expand' => 'seller'
        ]);
        $order = $this->orders->findById($item['order_id']);
        $order = $this->orderSelector->_transforms($order, $this->orders);
        $order = array_merge($order, $this->statusParser->parse($order));

        $output['order'] = $order;
        $output['detail'][] = [
          'label' => trans('transaction.product_name'),
          'value' => $order['cart_data']['items'][0]['name']
        ];
        if (isset($order['cart_data']['items'][0]['item_detail']['number'])) {
          $output['detail'][] = [
            'label' => trans('transaction.customer_number'),
            'value' => $order['cart_data']['items'][0]['item_detail']['number']
          ];
        }
        break;
      case TransactionType::BANK_TRANSFER:
      case TransactionType::BANK_TRANSFER_FEE:
      case TransactionType::WITHDRAW:
      case TransactionType::WITHDRAW_REFUND:

        $withdraw = $this->withdraws->findById($item['data']->id);
        $withdraw = $this->withdrawSelector->_transforms($withdraw, $this->withdraws);

        $output['withdraw'] = $withdraw;
        $output['detail'][] = [
          'label' => trans('transaction.bank_name'),
          'value' => $withdraw['account_bank']
        ];
        $output['detail'][] = [
          'label' => trans('transaction.bank_account_number'),
          'value' => $withdraw['account_number']
        ];
        $output['detail'][] = [
          'label' => trans('transaction.bank_account_name'),
          'value' => $withdraw['account_name']
        ];
        if (isset($withdraw['description'])) {
          $output['detail'][] = [
            'label' => trans('transaction.notes'),
            'value' => $withdraw['description']
          ];
        }
        break;
      case TransactionType::TRANSFER:

        if (isset($item['data']->from)) {
          $output['detail'][] = [
            'label' => trans('transaction.from'),
            'value' => $item['data']->from
          ];
        }
        if (isset($item['data']->to)) {
          $output['detail'][] = [
            'label' => trans('transaction.to'),
            'value' => $item['data']->to
          ];
        }
        $output['detail'][] = [
          'label' => trans('transaction.notes'),
          'value' => $item['data']->notes
        ];
        break;
    }

    if (isset($output['mutation']['via'])) {
      $output['detail'][] = [
        'label' => trans('transaction.via'),
        'value' => $output['mutation']['via']
      ];
    }

    return $output;

  }

}
