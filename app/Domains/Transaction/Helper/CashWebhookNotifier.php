<?php


namespace Odeo\Domains\Transaction\Helper;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Odeo\Domains\Account\Repository\SubUserRepository;
use Odeo\Domains\Constant\CashWebhookType;
use Odeo\Domains\Transaction\Repository\CashNotifyLogRepository;
use Odeo\Domains\Vendor\OdeoV2\Helper\ApiManager;

class CashWebhookNotifier {

  private $cashNotifyLogRepo, $subUserRepo;

  public function __construct() {
    $this->subUserRepo = app()->make(SubUserRepository::class);
    $this->cashNotifyLogRepo = app()->make(CashNotifyLogRepository::class);
  }

  public function notify($userId, $url, $signingKey, $type, $referenceId, $body) {
    $log = $this->cashNotifyLogRepo->getNew();
    $log->user_id = $userId;
    $log->reference_type = CashWebhookType::USER_WITHDRAW;
    $log->reference_id = $referenceId;
    $log->url = $url;

    try {
      $request = $this->createRequest($url, $signingKey, $body);
      $client = new Client();
      $response = $client->send($request, ['timeout' => 10]);
      $statusCode = $response->getStatusCode();
      $content = $response->getBody()->getContents();

      $log->status_code = $statusCode;
      $log->response_dump = $content;

      $log->is_success = $this->isSuccess($statusCode, $content);
    } catch (\Exception $e) {
      \Log::error($e);
      $log->is_success = false;
      $log->status_code = 0;
      $log->error_log = $e->getMessage();
    }

    $this->cashNotifyLogRepo->save($log);

    return [
      'success' => $log->is_success,
      'retry' => !$log->is_success && $this->shouldRetry($type, $referenceId)
    ];
  }

  private function createRequest($url, $signingKey, $body) {
    $apiManager = app()->make(ApiManager::class);
    $timestamp = time();

    $path = parse_url($url, PHP_URL_PATH) ?? '/';
    $query = parse_url($url, PHP_URL_QUERY) ?? '';

    if (!starts_with($path, '/')) {
      $path = '/' . $path;
    }

    $json = json_encode($body);
    $signature = $apiManager->getSignature($signingKey, 'POST', $path, $query, '', $timestamp, $json);

    return new Request('POST', $url, [
      'Content-Type' => 'application/json',
      ApiManager::TIMESTAMP_HEADER => $timestamp,
      Apimanager::SIGNATURE_HEADER => $signature,
    ], $json);
  }

  private function isSuccess($statusCode, $content) {
    if ($statusCode != 200) {
      return false;
    }

    $json = json_decode($content, true);
    if (!$json) {
      return false;
    }

    if (!isset($json['success'])) {
      return false;
    }

    return $json['success'];
  }

  private function shouldRetry($type, $referenceId) {
    $failed = $this->cashNotifyLogRepo->countFailedByReferenceTypeAndReferenceId($type, $referenceId);
    return $failed <= 3;
  }
}