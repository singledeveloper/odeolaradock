<?php

namespace Odeo\Domains\Transaction\Helper;

class Currency {

  const USD = "USD";
  const IDR = "IDR";

  public function __construct() {
    $this->data = [
      self::USD => ["symbol" => "$", "symbol_place" => "front", "decimal" => 2],
      self::IDR => ["symbol" => trans('misc.rp_label'), "symbol_place" => "front", "decimal" => 0],
    ];
  }

  public function isExist($symbol) {
    return array_key_exists($symbol, $this->data);
  }

  public function getCurrency($symbol = self::IDR) {
    if ($this->isExist($symbol)) {
      return json_decode(json_encode($this->data[$symbol]));
    }
    return false;
  }

  public function formatPrice($amount, $symbol = self::IDR) {
    if ($currency = $this->getCurrency($symbol)) {
      $formattedAmount = "";
      if ($amount < 0) $formattedAmount .= "(";
      if ($currency->symbol_place == "front") $formattedAmount .= $currency->symbol;

      $formattedAmount .= number_format(floor(abs($amount)), $currency->decimal);

      if ($currency->symbol_place == "back") $formattedAmount .= " " . $currency->symbol;

      if ($amount < 0) $formattedAmount .= ")";

      return [
        "amount" => (double)$amount,
        "currency" => $symbol,
        "formatted_amount" => $formattedAmount
      ];
    }
    return [];
  }

  public function getFormattedOnly($amount, $symbol = self::IDR) {
    $price = $this->formatPrice($amount, $symbol);
    return isset($price["formatted_amount"]) ? $price["formatted_amount"] : "";
  }

  public function getAmountOnly($amount, $symbol = self::IDR) {
    $price = $this->formatPrice($amount, $symbol);
    return isset($price["amount"]) ? $price["amount"] : 0;
  }
}
