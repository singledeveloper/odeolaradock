<?php

namespace Odeo\Domains\Transaction\Helper;

use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\CashType;

class DepositConverter {

  public function __construct() {
    $this->treeParentManager = app()->make(\Odeo\Domains\Network\Helper\ParentManager::class);
    $this->revenue = app()->make(\Odeo\Domains\Transaction\Repository\CashRevenueRepository::class);
    $this->cashInserter = app()->make(CashInserter::class);
    $this->cash = app()->make(CashManager::class);
    $this->orderStores = app()->make(\Odeo\Domains\Order\Repository\OrderStoreRepository::class);
  }

  //$userId, $storeId, $paidAmount, $depositAmount, $data, $orderId, $revenueType, $skipBalanceCheck = false, $withMarketingBudget = false
  public function convert($data, $additional = []) {

    if (isset($data['deposit_amount']) && $data['deposit_amount'] == 0) return 0;

    $this->cashInserter->clear();

    switch ($data['convert_type'] ?? null) {

      case 'agent_server':
        $this->addOcash($data, $additional);
        break;

      case 'agent':

        if ($data['paid_amount'] < $data['deposit_amount']) {
          clog('price_minus_issue', \json_encode($data));
        }

        $this->addOcash($data, $additional);
        $this->minusOcash($data, $additional);
        break;

      default:
        isset($data['with_marketing_budget']) && $this->addRush($data, $additional);
        $this->minusDeposit($data, $additional);
        $this->addOcash($data, $additional);
    }

    $this->cashInserter->run();

    $this->saveRevenue($data, $additional);

  }

  public function default($paidAmount, $depositAmount, $data) {
    if (isset($data['settlement_at'])) unset($data['settlement_at']);
    $this->cashInserter->clear();
    $this->cashInserter->add([
      'user_id' => NULL,
      'store_id' => NULL,
      'trx_type' => TransactionType::ORDER_CONVERT,
      'cash_type' => CashType::OCASH,
      'amount' => $paidAmount,
      'data' => json_encode($data),
    ]);
    $this->cashInserter->add([
      'user_id' => NULL,
      'store_id' => NULL,
      'trx_type' => TransactionType::ORDER_CONVERT,
      'cash_type' => CashType::OCASH,
      'amount' => -$depositAmount,
      'data' => json_encode($data),
    ]);
    $this->cashInserter->run();
  }

  public function minusDeposit($data, $additional = []) {
    if (isset($additional['settlement_at'])) unset($additional['settlement_at']);
    $this->cashInserter->add([
      'user_id' => $data['user_id'],
      'store_id' => $data['store_id'],
      'trx_type' => TransactionType::ORDER_CONVERT,
      'cash_type' => CashType::ODEPOSIT,
      'amount' => -$data['deposit_amount'],
      'data' => json_encode($additional),
    ]);
  }

  public function minusOcash($data, $additional = []) {
    $this->cashInserter->add([
      'user_id' => $data['user_id'],
      'store_id' => $data['store_id'],
      'trx_type' => TransactionType::ORDER_CONVERT,
      'cash_type' => CashType::OCASH,
      'amount' => -$data['deposit_amount'],
      'data' => json_encode($additional),
      'settlement_at' => isset($additional['settlement_at']) ? $additional['settlement_at'] : NULL
    ]);
  }

  public function addOcash($data, $additional = []) {
    $this->cashInserter->add([
      'user_id' => $data['user_id'],
      'store_id' => $data['store_id'],
      'trx_type' => TransactionType::ORDER_CONVERT,
      'cash_type' => CashType::OCASH,
      'amount' => $data['paid_amount'],
      'data' => json_encode($additional),
      'settlement_at' => isset($additional['settlement_at']) ? $additional['settlement_at'] : NULL
    ]);
  }

  public function addRush($data, $additional) {
    if (isset($additional['settlement_at'])) unset($additional['settlement_at']);
    $this->cashInserter->add([
      'user_id' => $data['user_id'],
      'store_id' => $data['store_id'],
      'trx_type' => TransactionType::ORDER_CONVERT,
      'cash_type' => CashType::ORUSH,
      'amount' => -($data['paid_amount'] - $data['deposit_amount']),
      'data' => json_encode($additional),
    ]);
  }


  public function saveRevenue($data, $additional = []) {

    $orderStore = $this->orderStores->getNew();
    $orderStore->order_id = $data['order_id'];
    $orderStore->store_id = $data['store_id'];
    $orderStore->role = empty($data['revenue_type']) ? 'hustler' : $data['revenue_type'];
    $orderStore->is_community = $additional['is_community'] ?? false;
    $this->orderStores->save($orderStore);

    if ($data['revenue_type'] == 'rush') $data['revenue_type'] = '';

    $trxType = strtoupper($data['revenue_type']) . (!empty($data['revenue_type']) ? '_' : '') . 'ORDER_REVENUE';
    $this->revenue->save([
      'user_id' => $data['user_id'],
      'store_id' => $data['store_id'],
      'order_id' => $data['order_id'],
      'amount' => $data['paid_amount'],
      'trx_type' => constant('\Odeo\Domains\Constant\TransactionType::' . $trxType),
      'data' => json_encode($additional),
    ]);

    $trxType = strtoupper($data['revenue_type']) . (!empty($data['revenue_type']) ? '_' : '') . 'ORDER_BONUS';
    $this->revenue->save([
      'user_id' => $data['user_id'],
      'store_id' => $data['store_id'],
      'order_id' => $data['order_id'],
      'amount' => $data['paid_amount'] - $data['deposit_amount'],
      'trx_type' => constant('\Odeo\Domains\Constant\TransactionType::' . $trxType),
      'data' => json_encode($additional),
    ]);
  }


}
