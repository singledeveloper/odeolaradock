<?php

namespace Odeo\Domains\Transaction\Helper;

use Illuminate\Support\Facades\Storage;
use Odeo\Domains\Constant\AwsConfig;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\PipelineListener;

class ExportDownloader {

  private $exportQueueRepo;

  public function __construct() {
    $this->exportQueueRepo = app()->make(\Odeo\Domains\Transaction\Repository\ExportQueueRepository::class);
  }

  public function create($data) {
    if (isset($data['platform_id']) && $data['platform_id'] == Platform::SELLER_CENTER) {
      $queue = $this->exportQueueRepo->getNew();
      $queue->user_id = $data['auth']['user_id'];
      $this->exportQueueRepo->save($queue);

      return $queue->id;
    }
    return 0;
  }

  public function check(PipelineListener $listener, $data) {
    if ($queue = $this->exportQueueRepo->findById($data['export_id'])) {
      if ($queue->user_id != $data['auth']['user_id']) {
        return $listener->response(400, 'No access');
      }
      if (!$queue->is_active && !$queue->error_message) {
        return $listener->response(400);
      }
      return $listener->response(200, [
        'export_id' => $queue->id,
        'download_link' => $queue->download_link ? (AwsConfig::S3_BASE_URL . '/' . $queue->download_link) : '',
        'error_message' => $queue->error_message ? $queue->error_message : ''
      ]);
    }
    return $listener->response(400);
  }

  public function update($id, $file, $fileName = '') {
    if ($id != '0' && $queue = $this->exportQueueRepo->findById($id)) {
      $random = base_convert($id, 10, 36) . '_' . time();
      $fileNameUrl = ($fileName != '' ? ($random . '_' . $fileName) : ($random . '.xls'));
      $url = 'export/' . $fileNameUrl;
      if (!Storage::disk('s3')->put($url, $file, [
        'visibility' => 'public',
        'ContentDisposition' => 'attachment; filename=' . ($fileName != '' ? $fileName : $fileNameUrl),
      ])) {
        $queue->is_active = false;
        $queue->error_message = 'Fail to generate file';
        $this->exportQueueRepo->save($queue);
      }
      else {
        $queue->download_link = $url;
        $this->exportQueueRepo->save($queue);
      }
    }
  }

  public function error($id, $errorMessage) {
    if ($id != '0' && $queue = $this->exportQueueRepo->findById($id)) {
      $queue->error_message = $errorMessage;
      $this->exportQueueRepo->save($queue);
    }
  }
}
