<?php

namespace Odeo\Domains\Transaction\Helper;

use Odeo\Domains\Constant\TransactionType;

class RevenueInserter {

  public function __construct() {
    $this->treeParentManager = app()->make(\Odeo\Domains\Network\Helper\ParentManager::class);
    $this->revenue = app()->make(\Odeo\Domains\Transaction\Repository\CashRevenueRepository::class);
  }

  public function insertRevenue($storeId, $orderId, $amount, $meGet = true, $isBonus = false, $isSubscription = false) {
    $exact = $this->treeParentManager->get_exact_parents($storeId);

    if ($exact->me && $meGet) {
      $this->revenue->save([
        'user_id' => $exact->me->user_id,
        'store_id' => $exact->me->store_id,
        'order_id' => $orderId,
        'amount' => is_array($amount) ? $amount['me'] : $amount,
        'trx_type' => $isSubscription ? TransactionType::SUBSCRIPTION_REVENUE : ($isBonus ? TransactionType::ORDER_BONUS : TransactionType::ORDER_REVENUE),
      ]);
    }

    if ($exact->parent) {
      $this->revenue->save([
        'user_id' => $exact->parent->user_id,
        'store_id' => $exact->parent->store_id,
        'order_id' => $orderId,
        'amount' => is_array($amount) ? $amount['parent'] : $amount,
        'data' => json_encode(["from_store_id" => $exact->me->store_id]),
        'trx_type' => $isSubscription ? TransactionType::SUBSCRIPTION_REVENUE : ($isBonus ? TransactionType::MENTOR_ORDER_BONUS : TransactionType::MENTOR_ORDER_REVENUE),
      ]);
    }

    if ($exact->grandparent && !$isSubscription) {
      $this->revenue->save([
        'user_id' => $exact->grandparent->user_id,
        'store_id' => $exact->grandparent->store_id,
        'order_id' => $orderId,
        'amount' => is_array($amount) ? $amount['grandparent'] : $amount,
        'data' => json_encode(["from_store_id" => $exact->me->store_id]),
        'trx_type' => $isSubscription ? TransactionType::SUBSCRIPTION_REVENUE : ($isBonus ? TransactionType::LEADER_ORDER_BONUS : TransactionType::LEADER_ORDER_REVENUE),
      ]);
    }
  }
}
