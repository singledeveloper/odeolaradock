<?php

namespace Odeo\Domains\Transaction\Jobs;

use Odeo\Domains\Account\Repository\SubUserRepository;
use Odeo\Domains\Constant\CashWebhookType;
use Odeo\Domains\OAuth2\Repository\OAuth2ClientRepository;
use Odeo\Domains\Transaction\Helper\CashManager;
use Odeo\Domains\Transaction\Helper\CashWebhookNotifier;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Transaction\Repository\CashWebhookRepository;
use Odeo\Domains\Transaction\Repository\UserTopupRepository;
use Odeo\Jobs\Job;

class NotifyTopupSuccess extends Job {

  private $topupId;

  public function __construct($withdrawId) {
    parent::__construct();
    $this->topupId = $withdrawId;
  }

  public function handle() {
    $subUserRepo = app()->make(SubUserRepository::class);
    $cashWebhookRepo = app()->make(CashWebhookRepository::class);
    $oauth2ClientRepo = app()->make(OAuth2ClientRepository::class);
    $userTopupRepo = app()->make(UserTopupRepository::class);
    $cashManager = app()->make(CashManager::class);
    $cashWebhookNotifier = app()->make(CashWebhookNotifier::class);

    $topup = $userTopupRepo->findById($this->topupId);
    $userId = $topup->user_id;
    $subUser = $subUserRepo->getParentUserId($userId);
    if ($subUser) {
      $userId = $subUser->parent_user_id;
    }

    $webhook = $cashWebhookRepo->findByUserIdAndType($userId, CashWebhookType::USER_TOPUP);
    if (!$webhook || !$webhook->url) {
      return;
    }

    $oauth2Client = $oauth2ClientRepo->findByUserId($userId);
    if (!$oauth2Client) {
      return;
    }

    $lockedCash = [];
    $cash = $cashManager->getCashBalance($topup->user_id, Currency::IDR, $lockedCash);
    $userCash = $cash[$topup->user_id];
    $userLockedCash = $lockedCash[$topup->user_id];

    $body = [
      'event_type' => 'topup_success',
      'user_id' => $topup->user_id . '',
      'topup_id' => $topup->id . '',
      'amount' => +$topup->amount,
      'current_cash' => +$userCash['amount'],
      'current_locked_cash' => +$userLockedCash['amount'],
    ];

    $res = $cashWebhookNotifier
      ->notify($userId, $webhook->url, $oauth2Client->signing_key, CashWebhookType::USER_TOPUP, $topup->id, $body);

    if ($res['retry']) {
      dispatch((new NotifyTopupSuccess($this->topupId))->delay(30));
    }
  }
}
