<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 5:23 PM
 */

namespace Odeo\Domains\Transaction\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendWithdrawAlert extends Job  {

  use SerializesModels;

  private $data, $users, $currency;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function handle() {

    if (app()->environment() != 'production') {
      return;
    }

    $user = $this->users->findById($this->data['user_id']);

    Mail::send('emails.withdraw_alert', [
      'data' => [
        'user' => $user->toArray(),
        'withdraw_amount' => $this->currency->formatPrice($this->data['amount']),
        'account_bank' => $this->data['account_bank'],
        'account_number' => $this->data['account_number'],
        'additional_message' => isset($this->data['additional_message']) ? $this->data['additional_message'] : ''
      ]
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('fraud@odeo.co.id')->subject('Withdraw Request - ' . time());
    });

  }

}
