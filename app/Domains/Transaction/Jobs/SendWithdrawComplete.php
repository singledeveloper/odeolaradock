<?php
namespace Odeo\Domains\Transaction\Jobs;

use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendWithdrawComplete extends Job {

  use SerializesModels;

  private $withdraw, $user, $currencyHelper;

  public function __construct($withdraw, $user) {
    parent::__construct();
    $this->withdraw = $withdraw;
    $this->user = $user;
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function handle() {
    $data = [];
    $data['name'] = $this->user['name'];
    $data['withdraw_id'] = $this->withdraw['id'];
    $data['verified_at'] = $this->withdraw['verified_at'];
    $data['amount'] = $this->currencyHelper->formatPrice($this->withdraw['amount'])['formatted_amount'];
    $data['fee'] = $this->currencyHelper->formatPrice($this->withdraw['fee'])['formatted_amount'];
    $data['account_name'] = $this->withdraw['account_name'];
    $data['account_number'] = $this->withdraw['account_number'];
    $data['account_bank'] = $this->withdraw['account_bank'];
    $data['is_bank_transfer'] = $this->withdraw['is_bank_transfer'];

    $email = $this->user->email;
    $withdrawId = $this->withdraw['id'];
    $isBankTransfer = $this->withdraw['is_bank_transfer'];

    Mail::send('emails.withdraw_complete', ['data' => $data], function ($m) use ($email, $withdrawId, $isBankTransfer) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to($email)->subject('[ODEO] ' . $isBankTransfer ? 'Bank Transfer' : 'Withdraw' . ' Complete - ' . $withdrawId);
    });
  }

}
