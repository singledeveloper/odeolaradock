<?php

namespace Odeo\Domains\Transaction\Model;

use Odeo\Domains\Account\Model\Bank;
use Odeo\Domains\Core\Entity;

class CashRecurring extends Entity
{
  public function bank() {
    return $this->belongsTo(Bank::class);
  }
}
