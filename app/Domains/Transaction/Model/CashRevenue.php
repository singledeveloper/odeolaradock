<?php

namespace Odeo\Domains\Transaction\Model;

use Odeo\Domains\Core\Entity;

class CashRevenue extends Entity
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at'];

    protected $fillable = ['user_id', 'store_id', 'order_id', 'trx_type', 'amount', 'data'];

    public $timestamps = false;

    public static function boot() {
      parent::boot();

      static::creating(function($model) {
        $timestamp = $model->freshTimestamp();
        $model->setCreatedAt($timestamp);
      });
    }
}
