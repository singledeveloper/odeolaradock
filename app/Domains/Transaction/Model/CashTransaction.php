<?php

namespace Odeo\Domains\Transaction\Model;

use Odeo\Domains\Core\Entity;

class CashTransaction extends Entity {

  protected $dates = ['created_at'];

  protected $fillable = ['user_id', 'store_id', 'trx_type', 'cash_type', 'amount', 'data', 'transaction_key', 'reference_type', 'reference_id'];

  public $timestamps = false;

  public static function boot() {
    parent::boot();

    static::creating(function($model) {
      $timestamp = $model->freshTimestamp();
      $model->setCreatedAt($timestamp);
      $model->trx_month = $timestamp->month;
      $model->trx_year = $timestamp->year;
    });
  }

}
