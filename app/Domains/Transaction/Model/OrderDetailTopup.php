<?php

namespace Odeo\Domains\Transaction\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Order\Model\OrderDetail;

class OrderDetailTopup extends Entity {

  protected $dates = ['created_at', 'updated_at'];
  
  public $timestamps = true;
  
  public function topup() {
    return $this->belongsTo(UserTopup::class);
  }

  public function orderDetail() {
    return $this->belongsTo(OrderDetail::class);
  }
}
