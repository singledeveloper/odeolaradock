<?php

namespace Odeo\Domains\Transaction\Model;

use Odeo\Domains\Account\Model\Bank;
use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class UserWithdraw extends Entity {

  protected $dates = ['created_at', 'updated_at'];
  
  public $timestamps = true;
  
  public function user() {
    return $this->belongsTo(User::class);
  }

  public function bank() {
    return $this->belongsTo(Bank::class);
  }
}
