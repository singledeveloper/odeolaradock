<?php

namespace Odeo\Domains\Transaction;

use Odeo\Domains\Accounting\CompanyTransactionInformation\Repository\CompanyTransactionAdditionalInformationRepository;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Transaction\Jobs\SendPendingTransactionReport;
use Odeo\Domains\Transaction\Jobs\SendRecentTransactionReport;

class RecentSelector implements SelectorListener {

  private $transaction, $currency, $orderTypeParser, $users, $tempCash, $currentUserName;

  public function __construct() {
    $this->transaction = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
    $this->tempCash = app()->make(\Odeo\Domains\Transaction\Repository\UserTempCashRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->orderTypeParser = app()->make(\Odeo\Domains\Order\Helper\OrderTypeParser::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    $output = [];

    $output["id"] = $item->id;
    $output["order_id"] = $item->order_id;
    $output["trx_type"] = $item->trx_type;
    $output["data"] = json_decode($item->data);

    if (isset($item->settlement_at)) {
      $output['settlement_at'] = $item->settlement_at;
    }

    $output["description"] = ucwords(str_replace("_", " ", $item->trx_type));
    if ($item->cash_type == CashType::ODEPOSIT) {
      $output["description"] = "Deposit " . $output["description"];
    }

    $type = "";
    if ($item->data != "" && $etc = json_decode($item->data)) {
      if (isset($etc->recent_desc)) {
        $type = $etc->recent_desc;
        if ($item->trx_type == TransactionType::PAYMENT) {
          $output["to"] = $etc->recent_desc;
        } else if ($item->trx_type == TransactionType::MDR) {
          $output["description"] = strtoupper($output["description"]) . " - " . $etc->recent_desc;
        }
      }
      if (isset($etc->order_id)) {
        $output['description'] .= ($item->trx_type == TransactionType::ORDER_CONVERT ? '' : ' Order') . ' #' . $etc->order_id;
      }
      if ($item->trx_type == TransactionType::ORDER_CONVERT) {
        if (!isset($etc->role)) {
          $etc->role = 'hustler';
          $output['via'] = $etc->recent_desc ?? '';
        }
        if (!isset($etc->is_community)) {
          $etc->is_agent = true;
        }
      } else if ($item->trx_type == TransactionType::RUSH_BONUS ||
        $item->trx_type == TransactionType::MENTOR_RUSH_BONUS ||
        $item->trx_type == TransactionType::LEADER_RUSH_BONUS) {
        $etc->role = explode('_', $item->trx_type)[0];
        if (!isset($etc->is_community)) {
          $etc->is_agent = true;
        }
      } else if ($item->trx_type == TransactionType::API_DISBURSEMENT) {
        $output["description"] .= ' #' . $etc->id;
        if (isset($etc->to)) {
          $output['to'] = $etc->to;
        }
      } else if ($item->trx_type == TransactionType::API_DISBURSEMENT_INQUIRY) {
        $output["description"] .= ' #' . $etc->id;
      }

      $output = array_merge($output, $this->orderTypeParser->parse($etc));
      if ($item->trx_type == TransactionType::TRANSFER) {
        $output['from'] = isset($etc->from) ? $etc->from : $this->currentUserName;
        $output['to'] = isset($etc->to) ? $etc->to : $this->currentUserName;
      }
      else if ($item->trx_type == TransactionType::DISBURSEMENT_OCASH) {
        if (isset($etc->from_user)) {
          $output['from'] = $etc->from_user;
        }
        if ($this->currentUserName) {
          $output['to'] = $this->currentUserName;
        }
      }
      else if (in_array($item->trx_type, [
        TransactionType::BATCH_DISBURSEMENT,
        TransactionType::BANK_TRANSFER
      ])) {
        if (isset($etc->to_account_name)) {
          $output['to'] = $etc->to_account_name;
        }
        if ($this->currentUserName) {
          $output['from'] = $this->currentUserName;
        }
      }
      else {
        if (isset($etc->from)) {
          if ($item->cash_type == CashType::ODEPOSIT) {
            $output['via'] = $etc->from;
          } else if ($item->cash_type == CashType::OCASH) {
            $output['via'] = $etc->recent_desc;
          }
        } else if (isset($etc->recent_desc)) {
          $output['via'] = $etc->recent_desc;
        } else if (!isset($etc->notes)) {
          $output['via'] = 'odeo';
        }
      }
      if (isset($etc->settlement_at)) {
        $output["settlement_at"] = $etc->settlement_at;
      }
      if (isset($etc->notes)) {
        $output['notes'] = $etc->notes;
      }
      if (isset($etc->description)) {
        $output['notes'] = $etc->description;
      }
    }
    if (isset($item->order_id) && isset($item->gateway_id)) {
      $gateway = Payment::getGatewayName($item->gateway_id);
      if (!isset($output['via'])) $output['via'] = 'odeo';
      $output['via'] .= isset($gateway) ? ' / ' . $gateway : '';
      $userId = getUserId();
      if (($userId && $userId == $item->order_user_id) || ($etc && isset($etc->role) && isset($etc->is_agent) && $etc->role == 'hustler' && $etc->is_agent)) {
        $output['from'] = $item->email;
      } else {
        if ($length = strlen($item->email)) {
          $output['from'] = substr($item->email, 0, 5) . str_repeat('*', max(0, $length - 5));
        }
      }
      if (isset($item->payment_name)) {
        $output['payment'] = $item->payment_name;
      }
    }
    if ($item->cash_type == CashType::OCASH) {
      $type = "oCash";
    } else if ($item->cash_type == CashType::OCOD) {
      $type = "Cash on Delivery";
    } /*else {
      $type = $type;
    }*/
    $output["type"] = $type;

    if ($item->created_at && method_exists($item->created_at, 'format')) $output["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    if ($item->amount) $output["transaction_amount"] = $this->currency->formatPrice($item->amount);
    if ($item->balance_before) $output["balance_before"] = $this->currency->formatPrice($item->balance_before);
    if ($item->balance_after) $output["balance_after"] = $this->currency->formatPrice($item->balance_after);
    if (isset($item->reference_id)) $output['reference_id'] = $item->reference_id;
    return $output;
  }

  public function _extends($data, Repository $repository) {
    if ($cashTransactionIds = $repository->beginExtend($data, "id")) {
      if ($repository->hasExpand('information_count')) {
        $result = [];
        $companyAdditionalRepo = app()->make(CompanyTransactionAdditionalInformationRepository::class);
        foreach ($companyAdditionalRepo->getInformationCountByTransactionIds($cashTransactionIds) as $key => $item) {
          $result[$key] = $item->total_count;
        }
        $repository->addExtend('information_count', $result);
      }
    }
    return $repository->finalizeExtend($data);
  }

  public function setUserName($name) {
    $this->currentUserName = $name;
  }

  public function get(PipelineListener $listener, $data) {
    $this->transaction->setSimplePaginate(true);
    $this->transaction->normalizeFilters($data);

    $transactions = [];

    foreach ($this->transaction->getRecent() as $item) {
      $transaction = $this->_transforms($item, $this->transaction);
      $transactions[] = $transaction;
    }

    if (sizeof($transactions) > 0)
      return $listener->response(200, array_merge(
        ["recent_transactions" => $this->_extends($transactions, $this->transaction)],
        $this->transaction->getPagination()
      ));

    return $listener->response(204, ["recent_transactions" => []]);
  }

  public function exportRecent(PipelineListener $listener, $data) {

    $this->transaction->normalizeFilters($data);

    $userName = $this->users->findById(getUserId())->name;

    $filters = [
      'start_date' => $data['start_date'],
      'end_date' => $data['end_date'],
      'type' => $data['type'],
      'file_type' => $data['file_type'] ?? 'csv',
      'order_id' => $data['order_id'] ?? '',
      'payment_id' => $data['payment_id'] ?? '',
      'trx_type' => $data['trx_type'] ?? '',
      'category' => $data['category'] ?? '',
      'user_id' => getUserId(),
      'user_name' => $userName
    ];

//    !$excludeValidation && Redis::setex($data["type"] . "_transaction_export_" . getUserId(), 3600, 1);

    $exportId = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class)->create($data);

    $listener->pushQueue(new SendRecentTransactionReport($filters, $data['email'], $exportId));
    return $listener->response(200, [
      'export_id' => $exportId
    ]);
  }

  public function getPending(PipelineListener $listener, $data) {
    $this->tempCash->setSimplePaginate(true);
    $this->tempCash->normalizeFilters($data);

    $transactions = [];
    foreach ($this->tempCash->getPending() as $item) {
      $output = [];
      $output['amount'] = $this->currency->formatPrice($item->amount);
      $output['cash_transaction_id'] = $item->cash_transaction_id;
      $output['order_id'] = $item->order_id;
      $output['created_at'] = $item->created_at;
      $output['settlement_at'] = $item->settlement_at;
      $output['cash_transaction'] = $this->_transforms($item, $this->transaction);
      $transactions[] = $output;
    }

    if (sizeof($transactions) > 0)
      return $listener->response(200, array_merge(
        ["recent_transactions" => $this->_extends($transactions, $this->tempCash)],
        $this->tempCash->getPagination()
      ));

    return $listener->response(204, ["recent_transactions" => []]);
  }

  public function exportPendingTransactions(PipelineListener $listener, $data) {

    $this->transaction->normalizeFilters($data);

    $userName = $this->users->findById(getUserId())->name;

    $filters = [
      'start_date' => $data['start_date'],
      'end_date' => $data['end_date'],
      'type' => $data['type'],
      'file_type' => $data['file_type'] ?? 'csv',
      'order_id' => $data['order_id'] ?? '',
      'payment_id' => $data['payment_id'] ?? '',
      'trx_type' => $data['trx_type'] ?? '',
      'user_id' => getUserId(),
      'user_name' => $userName
    ];

//    !$excludeValidation && Redis::setex($data["type"] . "_transaction_export_" . getUserId(), 3600, 1);
    $listener->pushQueue(new SendPendingTransactionReport($filters, $data['email']));
    return $listener->response(200);
  }

  public function getPendingSummary(PipelineListener $listener, $data) {
    $this->tempCash->normalizeFilters($data);

    $summary = $this->tempCash->getPendingSummary();
    $total = $this->currency->formatPrice($summary->total);
    $count = $summary->count;

    return $listener->response(200, [
      "total" => $total,
      "count" => $count
    ]);
  }

  private function maskTransferData($trxType, $data) {
    if ($trxType != TransactionType::TRANSFER) {
      return $data;
    }

    if (isset($data->from)) {
      $data->from = maskMessage($data->from);
    } else if (isset($data->to)) {
      $data->to = maskMessage($data->to);
    }

    return $data;
  }
}
