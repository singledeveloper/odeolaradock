<?php


namespace Odeo\Domains\Transaction\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\CashApiUser;

class CashApiUserRepository extends Repository {

  public function __construct(CashApiUser $model) {
    $this->setModel($model);
  }

  public function findByUserId($userId) {
    return $this->model
      ->where('user_id', $userId)
      ->first();
  }
}