<?php

namespace Odeo\Domains\Transaction\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\CashNotifyLog;

class CashNotifyLogRepository extends Repository
{

  public function __construct(CashNotifyLog $cashNotifyLog)
  {
    $this->model = $cashNotifyLog;
  }

  public function countFailedByReferenceTypeAndReferenceId($referenceType, $referenceId)
  {
    $model = $this->getCloneModel();
    return $model
      ->where('reference_type', $referenceType)
      ->where('reference_id', $referenceId)
      ->where('is_success', '=', false)
      ->count();
  }
}
