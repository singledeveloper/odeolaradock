<?php

namespace Odeo\Domains\Transaction\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\CashRecurring;
use Odeo\Domains\Transaction\Model\CashRecurringHistory;

class CashRecurringHistoryRepository extends Repository {

  public function __construct(CashRecurringHistory $cashRecurringHistory) {
    $this->model = $cashRecurringHistory;
  }

  public function gets($userId) {
    return $this->getResult($this->getCloneModel()
      ->join('cash_recurrings', 'cash_recurrings.id', '=', 'cash_recurring_histories.cash_recurring_id')
      ->join('banks', 'banks.id', '=', 'cash_recurrings.bank_id')
      ->where('cash_recurrings.user_id', $userId)
      ->orderBy('cash_recurring_histories.id', 'desc')
      ->select(
        'cash_recurring_histories.id',
        'banks.name as bank_name',
        'cash_recurrings.destination_number',
        'cash_recurrings.amount',
        'cash_recurrings.template_notes',
        'cash_recurring_histories.status',
        'cash_recurring_histories.error_message',
        'cash_recurring_histories.created_at',
        'cash_recurring_histories.retried_at'
      )
    );
  }

  public function findByReferenceId($type, $referenceId) {
    return $this->getCloneModel()->where('reference_type', $type)
      ->where('reference_id', $referenceId)->first();
  }

}
