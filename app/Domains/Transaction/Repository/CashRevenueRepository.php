<?php

namespace Odeo\Domains\Transaction\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\CashRevenue;

use Odeo\Domains\Constant\TransactionType;

class CashRevenueRepository extends Repository {

  public function __construct(CashRevenue $cashRevenue) {
    $this->model = $cashRevenue;
  }

  public function getUserToday($userIds) {
    $query = $this->model->groupBy('user_id')
      ->selectRaw('sum(amount) as total_amount, user_id')
      ->whereIn("trx_type", [
        TransactionType::SUBSCRIPTION_REVENUE,
      ])
      ->whereDate('created_at', '=', date('Y-m-d'));

    if (is_array($userIds)) $query = $query->whereIn('user_id', $userIds);
    else $query = $query->where('user_id', $userIds);

    return $query->pluck('total_amount', 'user_id');
  }

  public function getStoreLifetimeTotal($storeId) {
    return $this->model
      ->selectRaw(
        'sum(amount) as total_amount, count(id) as total_order'
      )
      ->where('store_id', $storeId)
      ->whereIn("trx_type", [
        TransactionType::PRESET_CASHBACK,
        TransactionType::SPONSOR_BONUS
      ])
      ->first();

  }

  public function getUserDetailedToday() {
    $filters = $this->getFilters();
    $query = $this->model->groupBy('store_id', 'trx_type')->selectRaw('sum(amount) as total_amount, store_id, trx_type')
      ->where("user_id", $filters["auth"]["user_id"])->where('created_at', '>=', date('Y-m-d') . ' 00:00:00');

    return $this->getResult($query);
  }

  public function getStoreToday($storeIds) {
    $query = $this->model->groupBy('store_id')->selectRaw('sum(amount) as total_amount, store_id')
      ->whereIn("trx_type", [
        TransactionType::SUBSCRIPTION_REVENUE,
        TransactionType::PRESET_CASHBACK
      ])
      ->whereDate('created_at', '=', date('Y-m-d'));

    if (is_array($storeIds)) $query = $query->whereIn('store_id', $storeIds);
    else $query = $query->where('store_id', $storeIds);

    return $query->pluck('total_amount', 'store_id');
  }
}
