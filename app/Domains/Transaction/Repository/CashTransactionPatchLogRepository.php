<?php

namespace Odeo\Domains\Transaction\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\CashTransactionPatchLog;

class CashTransactionPatchLogRepository extends Repository
{

  public function __construct(CashTransactionPatchLog $cashTransactionPatchLog)
  {
    $this->model = $cashTransactionPatchLog;
  }
  
}
