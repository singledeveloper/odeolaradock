<?php

namespace Odeo\Domains\Transaction\Repository;

use Carbon\Carbon;
use Odeo\Domains\Constant\CashConfig;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\CashTransaction;

use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\CashType;

class CashTransactionRepository extends Repository {

  public function __construct(CashTransaction $cashTransaction) {
    $this->model = $cashTransaction;
  }

  public function gets() {

    $filters = $this->getFilters();

    $query = $this->getCloneModel();

    if (isset($filters['search']['id'])) {
      $query = $query->where('id', $filters['search']['id']);
    }
    if (isset($filters['search']['order_id'])) {
      $query = $query->where('order_id', $filters['search']['order_id']);
    }

    return $this->getResult($query->orderBy($filters['sort_by'], $filters['sort_type']));
  }

  public function findFreeCash($userId) {
    return $this->model->where('user_id', $userId)->where('trx_type', TransactionType::FREE_REGISTER_CASH)->first();
  }

  public function findNewUserCashback($userId, $channelId) {
    return $this->model->where("trx_type", TransactionType::NEW_CASHBACK)
      ->where("user_id", $userId)
      ->where("cash_type", CashType::OCASH)
      ->where("data", "LIKE", '%"channel_id":' . $channelId . '%')->first();
  }

  public function findTransaction($cashTransactionId) {
    $filters = $this->getFilters();

    $query = $this->model->select('cash_transactions.id', 'trx_type', 'cash_type',
      'cash_transactions.amount', 'data', 'cash_transactions.created_at', 'cash_transactions.order_id', 'orders.user_id as order_user_id',
      'orders.email', 'orders.gateway_id', 'payment_odeo_payment_channel_informations.name as payment_name',
      'cash_transactions.balance_before as balance_before', 'cash_transactions.balance_after as balance_after', 'orders.id as order_id',
      'user_temp_cashes.settlement_at')
      ->where("trx_type", "<>", TransactionType::MARKETING_BUDGET)
      ->where('cash_transactions.id', '=', $cashTransactionId)
      ->leftJoin('orders', 'orders.id', '=', 'cash_transactions.order_id')
      ->leftJoin('payments', 'payments.order_id', '=', 'orders.id')
      ->leftJoin('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payments.info_id')
      ->leftJoin('user_temp_cashes', 'user_temp_cashes.cash_transaction_id', '=', 'cash_transactions.id');

    return $query->first();
  }

  public function getRecent() {
    $filters = $this->getFilters();

    $query = $this->model->select('cash_transactions.id', 'trx_type', 'cash_type', 'cash_transactions.reference_id',
      'cash_transactions.amount', 'data', 'cash_transactions.created_at', 'cash_transactions.order_id', 'orders.user_id as order_user_id',
      'orders.email', 'orders.gateway_id', 'payment_odeo_payment_channel_informations.name as payment_name',
      'cash_transactions.balance_before as balance_before', 'cash_transactions.balance_after as balance_after')
      ->where("trx_type", "<>", TransactionType::MARKETING_BUDGET)
      ->leftJoin('orders', 'orders.id', '=', 'cash_transactions.order_id')
      ->leftJoin('payments', 'payments.order_id', '=', 'orders.id')
      ->leftJoin('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payments.info_id');

    if (isNewDesign()) {
      if (isset($filters["category"]) && $filters["category"] === 'history') {
        $query = $query->where(function ($q) {
          $q->where('cash_transactions.settlement_at', '<', Carbon::now()->toDateTimeString())
            ->orWhereNull('cash_transactions.settlement_at');
        });
      }
    }

    if (!isAdmin($filters)) $query = $query->where("cash_transactions.user_id", $filters["auth"]["user_id"]);
    else {
      if (isset($filters["user_id"])) $query = $query->where("cash_transactions.user_id", $filters["user_id"]);
    }
    if (isset($filters["type"])) $query = $query->where("cash_type", $filters["type"]);
    else {
      $query = $query->whereNotIn("cash_type", [
        CashType::ORUSH, CashType::OADS, CashType::OCASHBACK
      ]);
    }

    if (isset($filters["trx_type"]) && $filters['trx_type'] != '')
      $query = $query->where("trx_type", $filters["trx_type"]);
    if (isset($filters["payment_id"]) && $filters["payment_id"] != '')
      $query = $query->where('payment_odeo_payment_channel_informations.id', '=', $filters["payment_id"]);
    if (isset($filters["order_id"]) && $filters["order_id"] != '')
      $query = $query->where("orders.id", "=", $filters["order_id"]);
    if (isset($filters["start_date"]) && $filters["start_date"] != '' && isset($filters["end_date"]) && $filters["end_date"] != '')
      $query = $query->whereBetween('cash_transactions.created_at', [date($filters['start_date'] . ' 00:00:00', time()), date($filters['end_date'] . ' 23:59:59', time())]);

    return $this->getResult($query->orderBy("cash_transactions.id", "DESC"));
  }

  public function getTransactionReport() {
    $filters = $this->getFilters();
    $query = $this->model->select('cash_transactions.id', 'trx_type', 'cash_type', 'stores.name as store_name',
      'cash_transactions.amount', 'data', 'cash_transactions.created_at', 'cash_transactions.order_id', 'orders.user_id as order_user_id',
      'orders.email', 'orders.gateway_id', 'payment_odeo_payment_channel_informations.name as payment_name',
      'cash_transactions.balance_before as balance_before', 'cash_transactions.balance_after as balance_after')
      ->from('cash_transactions')
      ->where("trx_type", "<>", TransactionType::MARKETING_BUDGET)
      ->leftJoin('orders', 'orders.id', '=', 'cash_transactions.order_id')
      ->leftJoin('stores', 'stores.id', '=', 'cash_transactions.store_id')
      ->leftJoin('payments', 'payments.order_id', '=', 'orders.id')
      ->leftJoin('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payments.info_id');

    if (isset($filters["category"]) && $filters["category"] === 'history') {
      $query = $query->leftJoin('user_temp_cashes', 'user_temp_cashes.cash_transaction_id', '=', 'cash_transactions.id')
        ->where('user_temp_cashes.id', null);

      if (isset($filters["trx_type"]) && $filters['trx_type'] != '') $query = $query->where("trx_type", $filters["trx_type"]);
      if (isset($filters["payment_id"]) && $filters["payment_id"] != '') $query = $query->where('payment_odeo_payment_channel_informations.id', '=', $filters["payment_id"]);
      if (isset($filters["order_id"]) && $filters["order_id"] != '') $query = $query->where("orders.id", "=", $filters["order_id"]);
    }

    if (!isAdmin($filters)) $query->where("cash_transactions.user_id", $filters['user_id']);

    if (isset($filters["type"])) $query->where("cash_type", $filters["type"]);
    else {
      $query->where("cash_type", "<>", CashType::ORUSH)->where("cash_type", "<>", CashType::OADS)->where("cash_type", "<>", CashType::OCASHBACK);
    }

    if (isset($filters["start_date"]) && $filters["start_date"] != '' && isset($filters["end_date"]) && $filters["end_date"] != '') {
      $query->whereDate('cash_transactions.created_at', '>=', $filters['start_date']);
      $query->whereDate('cash_transactions.created_at', '<=', $filters['end_date']);
    }

    $query->orderBy("cash_transactions.id", "asc");

    $db = \DB::connection()->getPdo();

    $result = $db->prepare($query->toSql());
    $result->execute($query->getBindings());

    return $result;
  }

  public function getStoreProfit($storeIds, $type = "", $trxTypes = [], $detailed = false) {
    if ($detailed) $query = $this->model->groupBy('store_id', 'trx_type')->selectRaw('sum(amount) as total_amount, store_id, trx_type');
    else $query = $this->model->groupBy('store_id')->selectRaw('sum(amount) as total_amount, store_id');

    $query = $query->whereIn("store_id", $storeIds);

    if ($type != "") $query = $query->where("cash_type", $type);
    if (sizeof($trxTypes) > 0) $query = $query->whereIn("trx_type", $trxTypes);
    else $query = $query->whereNotIn("trx_type", [TransactionType::TOPUP, TransactionType::WITHDRAW, TransactionType::REFUND, TransactionType::API_DISBURSEMENT]);

    $query = $query->where('created_at', '>=', Carbon::now()->startOfDay());

    if ($detailed) return $query->get();
    return $query->pluck('total_amount', 'store_id');
  }

  public function todayCashTransaction($userId, $type = "", $trxTypes = [], $detailed = false) {
    if ($detailed) $query = $this->model->groupBy('user_id', 'trx_type')->selectRaw('sum(amount) as total_amount, user_id, trx_type');
    else $query = $this->model->groupBy('user_id')->selectRaw('sum(amount) as total_amount, user_id');

    $query = $query->where("user_id", $userId);

    if ($type != "") $query = $query->where("cash_type", $type);
    if (sizeof($trxTypes) > 0) $query = $query->whereIn("trx_type", $trxTypes);

    $query = $query->where('created_at', '>=', date('Y-m-d') . ' 00:00:00');

    if ($detailed) return $query->get();
    return $query->pluck('total_amount', 'user_id');
  }

  public function getTodayCashout($userId) {
    $query = $this->model->groupBy('user_id')->selectRaw('sum(abs(amount)) as total_amount, user_id')
      ->where("user_id", $userId)
      ->where("cash_type", CashType::OCASH)
      ->whereIn("trx_type", [TransactionType::TRANSFER, TransactionType::WITHDRAW])
      ->where("amount", '<', 0)
      ->where('created_at', '>=', Carbon::now()->startOfDay());

    return $query->pluck('total_amount', 'user_id');
  }

  public function getTransactionBy($transactionType, $cashType, $storeId) {

    $query = $this->getCloneModel();

    $query = $query->where('trx_type', $transactionType)
      ->where('cash_type', $cashType)
      ->where('store_id', $storeId)
      ->orderBy('id', 'desc');

    return $this->getResult($query);
  }

  public function getTransactionData($trxType, $cashType, $day) {

    $date = Carbon::now()->subDays($day);

    return $this->model->where('trx_type', $trxType)
      ->where('cash_type', $cashType)
      ->where('created_at', '>=', $date->startOfDay())
      ->where('created_at', '<=', $date->endOfDay())
      ->select(\DB::raw('sum(amount) as total'), \DB::raw('count(id) as count'))->first();
  }

  public function findByOrderId($orderId, $trxType) {
    return $this->model->where('order_id', $orderId)->where('trx_type', $trxType)->first();
  }

  public function getCurrentMonthTopup($userId) {
    return $this->model->where('user_id', $userId)
      ->whereIn('trx_type', [
        TransactionType::TOPUP,
        TransactionType::TRANSFER
      ])
      ->where('amount', '>', 0)
      ->where('cash_type', CashType::OCASH)
      ->where('created_at', '>=', Carbon::now()->firstOfMonth())
      ->select(\DB::raw('sum(amount) as total_amount'))->first();
  }

  public function getCurrentDayTopUp($userId) {
    return $this->model
      ->where('user_id', $userId)
      ->whereIn('trx_type', [
        TransactionType::TOPUP,
        TransactionType::TRANSFER
      ])
      ->where('amount', '>', 0)
      ->where('cash_type', CashType::OCASH)
      ->where('created_at', '>=', Carbon::today())
      ->select(\DB::raw('sum(amount) as total_amount'))
      ->first();
  }

  public function findLastOrderRefund($userId) {
    return $this->model->where('user_id', $userId)
      ->where('trx_type', TransactionType::ORDER_REFUND)
      ->where('cash_type', CashType::OCASH)
      ->orderBy('id', 'desc')->first();
  }

  public function depositInterestSummary($startDate = null, $endDate = null) {

    $query = $this->model->selectRaw("
      created_at :: DATE as date, 
      sum(amount) as total
    ");

    if ($startDate) $query->whereDate('created_at', '>=', $startDate);
    if ($endDate) $query->whereDate('created_at', '<=', $endDate);

    return $query
      ->where('trx_type', '=', 'compound_interest')
      ->where('cash_type', '=', 'cashback')
      ->groupBy(\DB::raw('created_at::date'))
      ->orderBy('date')
      ->get();
  }

  public function getCompanyCashMutation() {
    $query = $this->model
      ->where('user_id', '=', CashConfig::COMPANY_USER_ID)
      ->select('id', 'trx_type',
        \DB::raw('CASE WHEN amount > 0 THEN amount ELSE 0 END AS credit,'),
        \DB::raw('CASE WHEN amount < 0 THEN abs(amount) ELSE 0 END AS debit'),
        'data',
        \DB::raw('created_at::date'))
      ->orderBy('id', 'desc');

    return $query->get();
  }

  public function findWithdraw($withdrawId) {
    return $query = $this->model
      ->where('reference_type', TransactionType::WITHDRAW)
      ->where('reference_id', $withdrawId)
      ->first();
  }

  public function getByUserId($userId) {
    $filters = $this->getFilters();

    $query = $this->getCloneModel()->where('user_id', $userId);

    if (isset($filters['start_date'])) $query->whereDate('created_at', '>=', $filters['start_date']);
    if (isset($filters['end_date'])) $query->whereDate('created_at', '<=', $filters['end_date']);

    if (isset($filters['search'])) {
      if (isset($filters['search']['id'])) {
        $query->where('id', $filters['search']['id']);
      }

      if (isset($filters['search']['reference_type'])) {
        $query->where('reference_type', $filters['search']['reference_type']);
      }

      if (isset($filters['search']['reference'])) {
        $query->where('reference_id', $filters['search']['reference']);
      }

      if (isset($filters['search']['data'])) {
        $query->where('data', 'ilike', '%' . $filters['search']['data'] . '%');
      }

      if (isset($filters['search']['trx_type'])) {
        $query->where('trx_type', $filters['search']['trx_type']);
      }

    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }
}
