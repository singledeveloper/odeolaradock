<?php

namespace Odeo\Domains\Transaction\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\CashWebhook;

class CashWebhookRepository extends Repository
{

  public function __construct(CashWebhook $cashWebhook)
  {
    $this->model = $cashWebhook;
  }

  public function findByUserIdAndType($userId, $type)
  {
    return $this->model->where("user_id", $userId)->where('type', $type)->first();
  }
}
