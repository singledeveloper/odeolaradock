<?php

namespace Odeo\Domains\Transaction\Repository;

use Odeo\Domains\Constant\StoreDeposit;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\OrderDetailStoreDeposit;
use Odeo\Domains\Transaction\Model\UserTopup;

class OrderDetailStoreDepositRepository extends Repository {

  public function __construct(OrderDetailStoreDeposit $orderDetailStoreDeposit) {
    $this->model = $orderDetailStoreDeposit;
  }

  public function findByOrderDetailId($order_detail_id) {
    return $this->model
      ->where('order_detail_id', $order_detail_id)
      ->first();
  }

}
