<?php

namespace Odeo\Domains\Transaction\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\TempCashDeposit;

class TempCashDepositRepository extends Repository {

  public function __construct(TempCashDeposit $tempCash) {
    $this->model = $tempCash;
  }
  
  public function get() {
    $filters = $this->getFilters();
    $query = $this->model;
    
    if (isset($filters["store_id"])) $query = $query->where('store_id', $filters["store_id"]);
    if (isset($filters["created_at"])) $query = $query->where('created_at', '<', $filters["created_at"]);
    
    return $this->getResult($query);
  }
  
  public function findByStoreId($storeId) {
    return $this->model->where("store_id", $storeId)->first();
  }
  
}
