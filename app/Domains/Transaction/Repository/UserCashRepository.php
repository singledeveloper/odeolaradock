<?php

namespace Odeo\Domains\Transaction\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\UserCash;
use Odeo\Domains\Constant\CashType;

class UserCashRepository extends Repository {

  private $lock = false;

  public function __construct(UserCash $cash) {
    $this->model = $cash;
  }

  public function lock() {
    $this->lock = true;
  }

  public function findByStoreId($userId = null, $store_id, $cashType) {
    $model = $this->model;
    if ($this->lock) $model = $model->lockForUpdate();

    $query = $model->where('store_id', $store_id)->where('cash_type', $cashType);

    $userId && $query->where('user_id', $userId);

    return $query->first();

  }

  public function findByUserId($userId, $cash_type) {
    $model = $this->model;
    if ($this->lock) $model = $model->lockForUpdate();
    return $model->where('user_id', $userId)->where('cash_type', $cash_type)->first();
  }

  public function findByStoreIds($storeIds, $cashType) {
    $model = $this->model;
    if ($this->lock) $model = $model->lockForUpdate();
    return $model->whereIn("store_id", $storeIds)->where("cash_type", $cashType)->pluck('amount', 'store_id');
  }

  public function findCash($userId) {
    $query = $this->model->groupBy('user_id')->selectRaw('sum(amount) as total_amount, user_id')->where("cash_type", CashType::OCASH);
    $query = is_array($userId) ? $query->whereIn("user_id", $userId) : $query->where("user_id", $userId);

    return $query->pluck('total_amount', 'user_id');
  }

  public function findStoreBalance($store_id = "", $cashType) {
    $query = $this->model->groupBy('store_id')->selectRaw('sum(amount) as total_amount, store_id')->where("cash_type", $cashType);
    if ($store_id != "")
      $query = is_array($store_id) ? $query->whereIn("store_id", $store_id) : $query->where("store_id", $store_id);
    else $query = $query->whereNotNull("store_id");

    return $query->pluck('total_amount', 'store_id');
  }

  public function findUserDeposit($userId = "") {
    $query = $this->model->groupBy('user_id', 'store_id')->selectRaw('sum(amount) as total_amount, user_id, store_id')
      ->where("cash_type", CashType::ODEPOSIT)->whereNotNull("store_id");
    $query = is_array($userId) ? $query->whereIn("user_id", $userId) : $query->where("user_id", $userId);

    return $query->get();
  }

  public function findMatch($store_id, $type) {
    return $this->model->where('store_id', $store_id)->where('cash_type', $type)->first();
  }

  public function getUserCashActivity() {
    return $this->runCache('activity', 'activity.user_cash_activity.', 1440, function () {
      return [
        'total_cash' => $this->model->where('cash_type', CashType::OCASH)->sum('amount'),
      ];
    });
  }

  public function getUserCashByUserIds($userIds) {
    return $this->model->join('users', 'users.id', '=', 'user_cashes.user_id')
      ->where('cash_type', CashType::OCASH)->whereIn('user_id', $userIds)
      ->orderBy('user_cashes.amount', 'desc')
      ->select('user_cashes.user_id', 'users.name', 'user_cashes.amount')->get();
  }

  public function getSumByCashType() {
    return $this->getCloneModel()->select(\DB::raw('cash_type, sum(amount) as total'))->groupBy('cash_type')->get();
  }

  public function getBusinessPrivateCash() {
    return $this->getCloneModel()
      ->join('users', 'users.id', '=', 'user_cashes.user_id')
      ->where('user_cashes.cash_type', CashType::OCASH)
      ->where('users.telephone', 'like', '62805%')
      ->where('users.name', 'like', '%Private%')
      ->select('users.id', 'users.name', 'user_cashes.amount')
      ->get();
  }
}
