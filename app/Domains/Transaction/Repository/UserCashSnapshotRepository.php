<?php

namespace Odeo\Domains\Transaction\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\UserCashSnapshot;

class UserCashSnapshotRepository extends Repository {

  public function __construct(UserCashSnapshot $userCashSnapshot) {
    $this->model = $userCashSnapshot;
  }

}
