<?php

namespace Odeo\Domains\Transaction\Repository;

use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\UserTempCash;
use Carbon\Carbon;

class UserTempCashRepository extends Repository {

  public function __construct() {
    $this->model = app()->make(\Odeo\Domains\Transaction\Model\UserTempCash::class);
  }

  public function get() {
    $filters = $this->getFilters();
    $query = $this->model;

    if (isset($filters["user_id"])) $query = $query->where('user_id', $filters["user_id"]);
    if (isset($filters["created_at"])) $query = $query->where('created_at', '<', $filters["created_at"]);

    return $this->getResult($query);
  }

  public function getSumByUsers($user_ids) {
    if (!is_array($user_ids)) $user_ids = explode(",", $user_ids);
    return $this->model->groupBy('user_id')->selectRaw('sum(amount) as total_amount, user_id')
      ->whereIn("user_id", $user_ids)->pluck('total_amount', 'user_id');
  }

  public function getSettled() {
    return $this->model->where('settlement_at', '<=', Carbon::now()->toDateTimeString())->get();
  }

  public function getPending() {
    $filters = $this->getFilters();
    $query = $this->model
    ->select(\DB::raw('*, user_temp_cashes.created_at'))
    ->leftJoin('cash_transactions', 'cash_transactions.id', '=', 'user_temp_cashes.cash_transaction_id')
    ->leftJoin('payments', 'payments.order_id', '=', 'user_temp_cashes.order_id')
    ->leftJoin('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payments.info_id')
    ->where('user_temp_cashes.user_id', $filters["auth"]["user_id"]);

    if (isset($filters["trx_type"]) && $filters['trx_type'] != '') $query = $query->where("trx_type", $filters["trx_type"]);
    if (isset($filters["payment_id"]) && $filters["payment_id"] != '') $query = $query->where('payment_odeo_payment_channel_informations.id', '=', $filters["payment_id"]);
    if (isset($filters["order_id"]) && $filters["order_id"] != '') $query = $query->where("user_temp_cashes.order_id", "=", $filters["order_id"]);
    if (isset($filters["start_date"]) && $filters["start_date"] != '' && isset($filters["end_date"]) && $filters["end_date"] != '') $query = $query->whereBetween('user_temp_cashes.created_at', [date($filters['start_date'] . ' 00:00:00', time()), date($filters['end_date'] . ' 23:59:59', time())]);

    return $this->getResult($query->orderBy('cash_transactions.id', 'DESC'));
  }

  public function getPendingTransactionReport() {
    $filters = $this->getFilters();
    $query = $this->model
    ->select(\DB::raw('*, user_temp_cashes.created_at, stores.name as store_name'))
    ->leftJoin('cash_transactions', 'cash_transactions.id', '=', 'user_temp_cashes.cash_transaction_id')
    ->leftJoin('payments', 'payments.order_id', '=', 'user_temp_cashes.order_id')
    ->leftJoin('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payments.info_id')
    ->leftJoin('stores', 'stores.id', '=', 'cash_transactions.store_id')
    ->where('user_temp_cashes.user_id', $filters["user_id"])
    ->whereBetween('user_temp_cashes.created_at', [date($filters['start_date'] . ' 00:00:00', time()), date($filters['end_date'] . ' 23:59:59', time())]);

    if (isset($filters["trx_type"]) && $filters['trx_type'] != '') $query = $query->where("trx_type", $filters["trx_type"]);
    if (isset($filters["payment_id"]) && $filters["payment_id"] != '') $query = $query->where('payment_odeo_payment_channel_informations.id', '=', $filters["payment_id"]);
    if (isset($filters["order_id"]) && $filters["order_id"] != '') $query = $query->where("user_temp_cashes.order_id", "=", $filters["order_id"]);

    $query->orderBy("user_temp_cashes.created_at", "asc");

    $db = \DB::connection()->getPdo();

    $result = $db->prepare($query->toSql());
    $result->execute($query->getBindings());

    return $result;
  }

  public function getPendingSummary() {
    $filters = $this->getFilters();
    $query = $this->model
      ->select(\DB::raw('sum(user_temp_cashes.amount) as total, count(user_temp_cashes.id) as count'))
      ->leftJoin('cash_transactions', 'cash_transactions.id', '=', 'user_temp_cashes.cash_transaction_id')
      ->leftJoin('payments', 'payments.order_id', '=', 'user_temp_cashes.order_id')
      ->leftJoin('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payments.info_id')
      ->where('user_temp_cashes.user_id', $filters["auth"]["user_id"]);

    if (isset($filters["trx_type"]) && $filters['trx_type'] != '') $query = $query->where("trx_type", $filters["trx_type"]);
    if (isset($filters["payment_id"]) && $filters["payment_id"] != '') $query = $query->where('payment_odeo_payment_channel_informations.id', '=', $filters["payment_id"]);
    if (isset($filters["order_id"]) && $filters["order_id"] != '') $query = $query->where("user_temp_cashes.order_id", "=", $filters["order_id"]);
    if (isset($filters["start_date"]) && $filters["start_date"] != '' && isset($filters["end_date"]) && $filters["end_date"] != '') $query = $query->whereBetween('user_temp_cashes.created_at', [date($filters['start_date'] . ' 00:00:00', time()), date($filters['end_date'] . ' 23:59:59', time())]);

    return $query->first();
  }
}
