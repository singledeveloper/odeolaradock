<?php

namespace Odeo\Domains\Transaction\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\UserTopup;
use Odeo\Domains\Constant\TopupStatus;

class UserTopupRepository extends Repository {

  public function __construct(UserTopup $userTopup) {
    $this->model = $userTopup;
  }

  public function get() {
    $filters = $this->getFilters();
    $query = $this->model;

    if ($this->hasExpand('user')) $query = $query->with('user');

    if (isAdmin($filters)) {
      $query = $query->where('user_id', $filters["user_id"]);
    }
    else {
      $query = $query->where('user_id', $filters["auth"]["user_id"]);
    }


    if (isset($filters['status'])) {
      $query = $query->where('status', $filters['status']);
    }
    else if (!isset($filters['all']) || !$filters['all']) {
      $query = $query->where('status', TopupStatus::PENDING);
    }

    if ($filters['sort_by']) {
      $query = $query->orderBy($filters['sort_by'], $filters['sort_type'] ? $filters['sort_type'] : 'asc');
    }

    return $this->getResult($query);
  }

  public function findLastTopup($userId) {
    return $this->model->where("user_id", $userId)->where('status', TopupStatus::PENDING)->orderBy("id", "DESC")->first();
  }
}
