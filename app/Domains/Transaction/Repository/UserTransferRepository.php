<?php

namespace Odeo\Domains\Transaction\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\UserTransfer;

class UserTransferRepository extends Repository {

  public function __construct(UserTransfer $userTransfer) {
    $this->model = $userTransfer;
  }

  public function sumTodayTransfer($userId) {
    $query =  $this->model
      ->selectRaw('sum(amount) as total_amount')
      ->where('sender_user_id', $userId)
      ->whereDate('created_at', '=', Carbon::now())
      ->first();

    return $query->total_amount ?? 0;
  }

}
