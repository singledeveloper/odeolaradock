<?php

namespace Odeo\Domains\Transaction\Scheduler;

use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BriDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementRequester;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bni\Repository\BankBniInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Repository\BankBriInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Repository\BankCimbInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Repository\BankPermataInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Odeo\Domains\Transaction\Jobs\SendAutoCompletedApiDisbursementAlert;
use Odeo\Domains\Transaction\Jobs\SuspectedApiDisbursementAlert;
use Odeo\Domains\Vendor\Mandiri\Repository\DisbursementMandiriDisbursementRepository;
use Odeo\Domains\Vendor\Permata\Repository\DisbursementPermataDisbursementRepository;

class BankMutationApiDisbursementReconScheduler {

  private $apiDisbursementRepo, $bankAccountRepo, $bcaInquiryRepo, $bniInquiryRepo, $cimbInquiryRepo, $permataInquiryRepo, $mandiriInquiryRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->bankAccountRepo = app()->make(BankScrapeAccountNumberRepository::class);
    $this->bcaInquiryRepo = app()->make(BankBcaInquiryRepository::class);
    $this->bniInquiryRepo = app()->make(BankBniInquiryRepository::class);
    $this->briInquiryRepo = app()->make(BankBriInquiryRepository::class);
    $this->cimbInquiryRepo = app()->make(BankCimbInquiryRepository::class);
    $this->permataInquiryRepo = app()->make(BankPermataInquiryRepository::class);
    $this->mandiriInquiryRepo = app()->make(BankMandiriGiroInquiryRepository::class);
  }

  public function run() {
    $inquiries = $this->bankAccountRepo->getApiDisbursementUnreconciledInquiries();

    list($completeList, $suspects) = $this->mergeTuple(
      $this->checkBca($inquiries['bca']),
      $this->checkBni($inquiries['bni']),
      $this->checkCimb($inquiries['cimb']),
      $this->checkPermata($inquiries['permata']),
      $this->checkBri($inquiries['bri']),
      $this->checkMandiri($inquiries['mandiri_giro'])
    );

    if (count($suspects) > 0) {
      dispatch(new SuspectedApiDisbursementAlert([
        'suspects' => $suspects
      ]));
    }

    if (count($completeList) > 0) {
      dispatch(new SendAutoCompletedApiDisbursementAlert([
        'id_list' => $completeList,
      ]));

      foreach ($completeList as $disbursementId) {
        $pipeline = new Pipeline();
        $pipeline->add(new Task(ApiDisbursementRequester::class, 'complete'));
        $pipeline->execute([
          'disbursement_id' => $disbursementId,
        ]);
      }
    }
  }

  private function mergeTuple() {
    $result = [[], []];
    $args = func_get_args();
    for ($i = 0; $i < count($args); $i++) {
      $result = [
        array_merge($result[0], $args[$i][0]),
        array_merge($result[1], $args[$i][1]),
      ];
    }
    return [$result[0], $result[1]];
  }

  private function checkBca($inquiries) {
    return $this->check(ApiDisbursement::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT, $this->bcaInquiryRepo,
      '/.*(?:DISB#|WDOCASH 2)(\d*).*/', $inquiries);
  }

  private function checkBni($inquiries) {
    return $this->check(ApiDisbursement::DISBURSEMENT_VENDOR_BNI_API_DISBURSEMENT, $this->bniInquiryRepo,
      '/.*(?:WDOCASH 2)(\d*).*/', $inquiries);
  }

  private function checkCimb($inquiries) {
    return $this->check(ApiDisbursement::DISBURSEMENT_VENDOR_CIMB_API_DISBURSEMENT, $this->cimbInquiryRepo,
      '/.*ADIS(\d+).*/', $inquiries);
  }

  private function checkPermata($inquiries) {
    return $this->check(ApiDisbursement::DISBURSEMENT_VENDOR_PERMATA_API_DISBURSEMENT, $this->permataInquiryRepo,
      '', $inquiries);
  }

  private function checkBri($inquiries) {
    return $this->check(ApiDisbursement::DISBURSEMENT_VENDOR_BRI_API_DISBURSEMENT, $this->briInquiryRepo,
      '/WDOCASH 2(\d*).*/', $inquiries);
  }

  private function checkMandiri($inquiries) {
    return $this->check(ApiDisbursement::DISBURSEMENT_VENDOR_MANDIRI_API_DISBURSEMENT, $this->mandiriInquiryRepo,
      '/WDOCASH 2(\d*).*/', $inquiries);
  }

  private function reconInquiryByCustomerReffNo($bankDisbursementRepo, $inquiries, $disbursementReffCol, $inquiryReffCol) {
    $disbursements = $bankDisbursementRepo->getDisbursementIdByCustRefs($inquiries->pluck($inquiryReffCol));
    $apiDisbursements = $this->apiDisbursementRepo->getAllByIdList($disbursements->pluck('disbursement_reference_id'));
    $disbursementByReff = $disbursements->keyBy($disbursementReffCol);

    $inquiryByDisbursementKey = [];

    foreach ($inquiries->keyBy($inquiryReffCol) as $key => $inquiry) {
      if (isset($disbursementByReff[$key])) {
        $apiDisbursementId = $disbursementByReff[$key]->disbursement_reference_id;
        $inquiryByDisbursementKey[$apiDisbursementId] = $inquiry;
      }
    }

    return [$inquiryByDisbursementKey, $apiDisbursements];
  }

  private function check($vendor, $bankInquiryRepo, $descRE, $inquiries) {

    switch ($vendor) {
      case ApiDisbursement::DISBURSEMENT_VENDOR_PERMATA_API_DISBURSEMENT:
        $permataDisbursements = app()->make(DisbursementPermataDisbursementRepository::class);
        list($inquiryByDisbursementKeys, $apiDisbursements) = $this->reconInquiryByCustomerReffNo(
          $permataDisbursements,
          $inquiries,
          'cust_reff_id',
          'cust_reff_no'
        );
        break;
      default:
        $inquiryByDisbursementKeys = $inquiries
          ->map(function ($inquiry) use ($descRE) {
            preg_match($descRE, $inquiry->description, $matches);
            return [$matches[1] ?? null, $inquiry];
          })
          ->filter(function($pair) {
            return $pair[0] !== null;
          })
          ->pluck(1, 0); // return inquiry keyed by disbursement id

          $apiDisbursements = $this->apiDisbursementRepo
            ->getAllByIdList($inquiryByDisbursementKeys->keys());
        break;
    }

    $suspects = [];
    $completeList = [];
    $matchedInquiryIdMap = [];

    foreach ($apiDisbursements as $disbursement) {
      $inquiry = $inquiryByDisbursementKeys[$disbursement->id];
      $matchedInquiryIdMap[$inquiry->id] = true;

      if ($this->canAutoSuccess($completeList, $vendor, $disbursement, $inquiry)) {
        $completeList[] = $disbursement->id;
        continue;
      }

      if ($disbursement->status != ApiDisbursement::COMPLETED || $this->isDifferentAmount($disbursement, $inquiry->debit)) {
        $suspects[] = [
          'id' => $disbursement->id,
          'inquiry_id' => $inquiry->id,
          'vendor' => $vendor,
          'disbursement_status' => $disbursement->status,
          'disbursement_amount' => $disbursement->amount,
          'inquiry_amount' => $inquiry->debit,
          'inquiry_description' => $inquiry->description,
        ];
        continue;
      }

      $inquiry->reference = \json_encode(["" . $disbursement->id]);
      $inquiry->reference_type = 'api_disbursement_id';
      $bankInquiryRepo->save($inquiry);
    }

    foreach ($inquiryByDisbursementKeys as $disbursementId => $inquiry) {
      if (isset($matchedInquiryIdMap[$inquiry->id])) {
        continue;
      }

      $suspects[] = [
        'id' => 'not found',
        'inquiry_id' => $inquiry->id,
        'vendor' => $vendor,
        'disbursement_status' => 'not found',
        'disbursement_amount' => 'not found',
        'inquiry_amount' => $inquiry->debit,
        'inquiry_description' => $inquiry->description,
      ];
    }

    return [$completeList, $suspects];
  }

  private function canAutoSuccess($completeList, $vendor, $disbursement, $inquiry) {
    return $disbursement->auto_disbursement_vendor == $vendor
      && in_array($disbursement->status, [ApiDisbursement::PENDING, ApiDisbursement::ON_PROGRESS, ApiDisbursement::SUSPECT])
      && $disbursement->amount == $inquiry->debit
      && !in_array($disbursement->id, $completeList);
  }

  private function isDifferentAmount($disbursement, $debitAmount) {
    switch ($disbursement->auto_disbursement_vendor) {
      case ApiDisbursement::DISBURSEMENT_VENDOR_BRI_API_DISBURSEMENT:
        return $disbursement->amount != $debitAmount - BriDisbursement::COST;
      default:
        return $disbursement->amount != $debitAmount;
    }
  }

}