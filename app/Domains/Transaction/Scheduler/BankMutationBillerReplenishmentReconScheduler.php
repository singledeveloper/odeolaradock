<?php

namespace Odeo\Domains\Transaction\Scheduler;

use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Disbursement\BillerReplenishment\Model\VendorSwitcherReplenishment;
use Odeo\Domains\Transaction\Jobs\SuspectedBillerReplenishmentAlert;

class BankMutationBillerReplenishmentReconScheduler {

  private $bankAccounts, $bcaInquiries;

  public function __construct() {
    $this->bankAccounts = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository::class);
    $this->bcaInquiries = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository::class);
  }

  public function run() {

    $unReconciledBillerReplenishmentInquiries = $this->bankAccounts->getBillerReplenishmentUnreconciledInquiries();

    $this->_bca($unReconciledBillerReplenishmentInquiries);

  }


  public function _bca($unReconciledBillerReplenishmentInquiries) {

    $replenishmentList = [];

    foreach ($unReconciledBillerReplenishmentInquiries['bca'] as $inquiry) {

      $temp = explode('.00VSREPL ', $inquiry->description);
      $temp = explode(' ', trim($temp[1]));

      $replenishmentList[intval(trim($temp[0]))] = $inquiry;

    }

    $billerReplenishmentModel = app()->make(VendorSwitcherReplenishment::class);
    $suspectedReplenishment = [];

    foreach ($billerReplenishmentModel->whereIn('id', array_keys($replenishmentList))->get() as $replenishment) {

      $inquiry = $replenishmentList[$replenishment->id];

      if ($replenishment->status == BillerReplenishment::FAILED || $replenishment->amount != $inquiry->debit) {
        $suspectedReplenishment[] = [
          'id' => $replenishment->id,
          'bank' => 'bca',
          'inquiry_id' => $inquiry->id
        ];
        continue;
      }

      $inquiry->reference = \json_encode(["" . $replenishment->id]);
      $inquiry->reference_type = 'biller_replenishment_id';

      $this->bcaInquiries->save($inquiry);
    }

    count($suspectedReplenishment) && dispatch(new SuspectedBillerReplenishmentAlert([
      'suspected_replenishment' => $suspectedReplenishment
    ]));


  }


}