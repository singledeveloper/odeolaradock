<?php

namespace Odeo\Domains\Transaction\Scheduler;

use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BriDisbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Repository\BankBriInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Odeo\Domains\Transaction\Jobs\SuspectedWithdrawAlert;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;

class BankMutationTransferReconScheduler {

  private $bankAccountRepo, $withdrawRepo;

  public function __construct() {
    $this->bankAccountRepo = app()->make(BankScrapeAccountNumberRepository::class);
    $this->withdrawRepo = app()->make(UserWithdrawRepository::class);
  }

  public function run() {

    $unreconciledTransferInquiries = $this->bankAccountRepo->getTransferUnreconciledInquiries();
    $reconciledIds = [];
    $reconciledTransfers = [];

    if (!empty($unreconciledTransferInquiries['bca'] ?? [])) {
      $reconciledIds = array_merge($reconciledIds, $this->checkBca($unreconciledTransferInquiries['bca']));
    }

    if (!empty($unreconciledTransferInquiries['bri'] ?? [])) {
      $reconciledIds = array_merge($reconciledIds, $this->checkBri($unreconciledTransferInquiries['bri']));
    }

    foreach($reconciledIds as $id) {
      $reconciledTransfers[] = [
        'id' => $id,
        'is_reconciled' => true
      ];
    }
  
    !empty($reconciledTransfers) && $this->withdrawRepo->updateBulk($reconciledTransfers);
  }

  private function checkBri($unreconciledTransferInquiries) {
    $briInquiryRepo = app()->make(BankBriInquiryRepository::class);
    $unreconciledBriTransfer = $this->withdrawRepo->getUnreconciledTransferByVendor(ApiDisbursement::DISBURSEMENT_VENDOR_BRI_API_DISBURSEMENT)->keyBy('id');
    $sortedInquiries = $unreconciledTransferInquiries->keyBy(function($inquiry) {
      preg_match('/WDOCASH 3(\d*).*/', $inquiry->description, $matches);
      return $matches[1];
    });
    $detectedInquiry = [];

    foreach ($sortedInquiries as $trfId => $inquiry) {
      if (isset($unreconciledBriTransfer[$trfId])) {
        $detectedInquiry[$trfId] = $inquiry;
      }
    }

    !empty($detectedInquiry) && $updatedInquiry = $this->process($detectedInquiry, 'bri');
    !empty($updatedInquiry) && $briInquiryRepo->updateBulk($updatedInquiry);

    return array_keys($detectedInquiry);
  }

  private function checkBca($unreconciledTransferInquiries) {
    $bcaInquiryRepo = app()->make(BankBcaInquiryRepository::class);
    $unreconciledBcaTransfers = $this->withdrawRepo->getUnreconciledTransferByVendor(ApiDisbursement::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT);
    $detectedInquiry = [];

    foreach ($unreconciledBcaTransfers as $transfer) {
      $desc = \Carbon\Carbon::parse($transfer->verified_at)->format('dm')
        . '/FTSCY/WS95051 '
        . ($transfer->amount - $transfer->fee)
        . '.00'
        . (isset($transfer->description)? substr($transfer->description . ' ', 0, 36) : '') .
        trim(substr($transfer->account_name, 0, 18));

      if ($inquiry = $unreconciledTransferInquiries->where('description', $desc)->first()) {
        $detectedInquiry[$transfer->id] = $inquiry;
      }
    }

    !empty($detectedInquiry) && $updatedInquiry = $this->process($detectedInquiry, 'bca');
    !empty($updatedInquiry) && $bcaInquiryRepo->updateBulk($updatedInquiry);

    return array_keys($detectedInquiry);
  }

  private function isNotExistedWithdraw($withdrawList, $detectedWithdrawId) {
    return !isset($withdrawList[$detectedWithdrawId]);
  }

  private function transformSuspectedData($withdrawId, $bank, $inquiry) {
    return [
      'id' => $withdrawId,
      'bank' => $bank,
      'inquiry_id' => $inquiry->id
    ];
  }

  private function transformDataToUpdate($withdrawId, $inquiry) {
    return [
      'id' => $inquiry->id,
      'reference' => \json_encode(["" . $withdrawId]),
      'reference_type' => 'withdraw_id'
    ];
  }

  private function isNotCompleted($withdraw, $inquiry, $bank) {
    $sendAmount = $withdraw->amount - $withdraw->fee;
    return $withdraw->status != Withdraw::COMPLETED || $this->isDifferentAmount($bank, $sendAmount, $inquiry->debit);
  }

  private function process($detectedInquiry, $bank) {
    $suspectedWithdrawal = [];
    $withdrawToBeUpdate = [];

    $withdrawList = $this->withdrawRepo->getWithdrawById(array_keys($detectedInquiry))
      ->keyBy('id')
      ->all();

    clog('recon_wd_bank', \json_encode($withdrawList));

    foreach ($detectedInquiry as $detectedWithdrawId => $di) {

      $notExisted = $this->isNotExistedWithdraw($withdrawList, $detectedWithdrawId);

      if ($notExisted) {
        $suspectedWithdrawal[] = $this
          ->transformSuspectedData($detectedWithdrawId, $bank, $di);
        continue;
      }

      $wd = $withdrawList[$detectedWithdrawId];

      if ($this->isNotCompleted($wd, $di, $bank)) {
        $suspectedWithdrawal[] = $this
          ->transformSuspectedData($detectedWithdrawId, $bank, $di);
        continue;
      }

      $withdrawToBeUpdate[] = $this->transformDataToUpdate($detectedWithdrawId, $di);
    }

    !empty($suspectedWithdrawal) && dispatch(new SuspectedWithdrawAlert([
      'suspected_withdraw' => $suspectedWithdrawal
    ]));

    return $withdrawToBeUpdate;
  }

  private function isDifferentAmount($bank, $sendAmount, $debitAmount) {
    switch ($bank) {
      case 'bri':
        return $sendAmount != $debitAmount - BriDisbursement::COST;
      default:
        return $sendAmount != $debitAmount;
    }

  }

}
