<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/13/17
 * Time: 10:57 PM
 */

namespace Odeo\Domains\Transaction\Scheduler;

use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bni\Repository\BankBniInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Repository\BankBriInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Repository\BankCimbInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Repository\BankPermataInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Odeo\Domains\Transaction\Jobs\SuspectedWithdrawAlert;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Vendor\Mandiri\Repository\DisbursementMandiriDisbursementRepository;
use Odeo\Domains\Vendor\Permata\Repository\DisbursementPermataDisbursementRepository;

class BankMutationWithdrawReconScheduler {

  private $bankAccountRepo, $bcaInquiryRepo, $cimbInquiryRepo, $permataInquiryRepo, $withdrawRepo;

  public function __construct() {
    $this->bankAccountRepo = app()->make(BankScrapeAccountNumberRepository::class);
    $this->bcaInquiryRepo = app()->make(BankBcaInquiryRepository::class);
    $this->bniInquiryRepo = app()->make(BankBniInquiryRepository::class);
    $this->briInquiryRepo = app()->make(BankBriInquiryRepository::class);
    $this->cimbInquiryRepo = app()->make(BankCimbInquiryRepository::class);
    $this->permataInquiryRepo = app()->make(BankPermataInquiryRepository::class);
    $this->mandiriInquiryRepo = app()->make(BankMandiriGiroInquiryRepository::class);
    $this->withdrawRepo = app()->make(UserWithdrawRepository::class);
  }

  public function run() {

    $unReconciledWithdrawInquiries = $this->bankAccountRepo->getWithdrawUnreconciledInquiries();

    if (!empty($unReconciledWithdrawInquiries['bca'] ?? [])) {
      $this->checkBca($unReconciledWithdrawInquiries['bca']);
    }
    if (!empty($unReconciledWithdrawInquiries['cimb'] ?? [])) {
      $this->checkCimb($unReconciledWithdrawInquiries['cimb']);
    }
    if (!empty($unReconciledWithdrawInquiries['bni'] ?? [])) {
      $this->checkBni($unReconciledWithdrawInquiries['bni']);
    }
    if (!empty($unReconciledWithdrawInquiries['permata'] ?? [])) {
      $this->checkPermata($unReconciledWithdrawInquiries['permata']);
    }
    if (!empty($unReconciledWithdrawInquiries['bri'] ?? [])) {
      $this->checkBri($unReconciledWithdrawInquiries['bri']);
    }
    if (!empty($unReconciledWithdrawInquiries['mandiri_giro'] ?? [])) {
      $this->checkMandiri($unReconciledWithdrawInquiries['mandiri_giro']);
    }
  }

  private function checkCimb($unReconciledWithdrawInquiries) {
    $detectedInquiry = $this
      ->getWithdrawIdFromInquiries($unReconciledWithdrawInquiries, '.WDOCASH ');

    !empty($detectedInquiry) &&
    $withdrawToBeUpdate = $this->process($detectedInquiry, 'cimb');

    !empty($withdrawToBeUpdate) &&
    $this->cimbInquiryRepo->updateBulk($withdrawToBeUpdate);
  }

  private function checkBca($unReconciledWithdrawInquiries) {
    $detectedInquiry = $this
      ->getWithdrawIdFromInquiries($unReconciledWithdrawInquiries, '.00WDOCASH 1');

    !empty($detectedInquiry) &&
    $withdrawToBeUpdate = $this->process($detectedInquiry, 'bca');

    !empty($withdrawToBeUpdate) &&
    $this->bcaInquiryRepo->updateBulk($withdrawToBeUpdate);

  }

  private function checkBni($unReconciledWithdrawInquiries) {
    $detectedInquiry = $this
      ->getWithdrawIdFromInquiries($unReconciledWithdrawInquiries, 'WDOCASH 1');

    !empty($detectedInquiry) &&
    $withdrawToBeUpdate = $this->process($detectedInquiry, 'bni');

    !empty($withdrawToBeUpdate) &&
    $this->bniInquiryRepo->updateBulk($withdrawToBeUpdate);

  }

  private function checkBri($unReconciledWithdrawInquiries) {

    $detectedInquiry = $this->getWithdrawIdFromInquiries($unReconciledWithdrawInquiries, 'WDOCASH 1');

    !empty($detectedInquiry) &&
    $withdrawTobeUpdate = $this->process($detectedInquiry, 'bri');
    !empty($withdrawTobeUpdate) &&
    $this->briInquiryRepo->updateBulk($withdrawTobeUpdate);
  }

  private function checkPermata($unReconciledWithdrawInquiries) {
    $inquiries = $unReconciledWithdrawInquiries->keyBy('cust_reff_no');
    $permataDisbursement = app()->make(DisbursementPermataDisbursementRepository::class);
    $unreconciledDisbursement = $permataDisbursement->getUnreconciledWithdraws()->keyBy('cust_reff_id');
    $detectedInquiry = [];
    $updateDisbursements = [];

    foreach ($unreconciledDisbursement as $reff => $disbursement) {
      if (isset($inquiries[$reff])) {
        $withdrawId = $disbursement->disbursement_reference_id;
        $detectedInquiry[$withdrawId] = $inquiries[$reff];
        $updateDisbursements[] = [
          'id' => $disbursement->id,
          'is_reconciled' => true
        ];
      }
    }

    !empty($detectedInquiry) &&
    $withdrawToUpdate = $this->process($detectedInquiry, 'permata');

    !empty($withdrawToUpdate) &&
    $this->permataInquiryRepo->updateBulk($withdrawToUpdate);

    !empty($updateDisbursements) &&
    $permataDisbursement->updateBulk($updateDisbursements);

  }

  private function checkMandiri($unreconciledWithdrawInquiries) {
    $mandiriDisbursement = app()->make(DisbursementMandiriDisbursementRepository::class);
    $inquiries = $unreconciledWithdrawInquiries->keyBy('reference_number');
    $unreconciledDisbursement = $mandiriDisbursement->getUnreconciledWithdraws()->keyBy('cust_reff_no');
    $detectedInquiry = [];
    $updateDisbursements = [];

    foreach ($unreconciledDisbursement as $reff => $disbursement) {
      if (isset($inquiries[$reff])) {
        $withdrawId = $disbursement->disbursement_reference_id;
        $detectedInquiry[$withdrawId] = $inquiries[$reff];
        $updateDisbursements[] = [
          'id' => $disbursement->id,
          'is_reconciled' => true
        ];
      }
    }

    !empty($detectedInquiry) &&
    $withdrawToUpdate = $this->process($detectedInquiry, 'mandiri_giro');

    !empty($withdrawToUpdate) &&
    $this->mandiriInquiryRepo->updateBulk($withdrawToUpdate);

    !empty($updateDisbursements) &&
    $mandiriDisbursement->updateBulk($updateDisbursements);
  }

  private function getWithdrawIdFromInquiries($unReconciledWithdrawInquiries, $startString) {

    clog('recon_wd_bank', json_encode($unReconciledWithdrawInquiries));

    return $unReconciledWithdrawInquiries->reduce(function ($prev, $curr) use ($startString) {
      $desc = $curr->description;

      clog('recon_wd_bank', $desc);

      $leftPos = strpos($desc, $startString);
      if ($leftPos === false) return $prev;

      clog('recon_wd_bank', $leftPos);

      $leftPos += strlen($startString);

      $rightPos = strpos($desc, ' ', $leftPos);

      $rightPos = $rightPos > 0 ? $rightPos : 999999;

      $length = $rightPos - $leftPos;

      clog('recon_wd_bank', $length);

      $withdrawId = trim(substr($curr->description, $leftPos, $length));

      clog('recon_wd_bank', $withdrawId);

      $prev[$withdrawId] = $curr;

      clog('recon_wd_bank', \json_encode($prev));

      return $prev;

    }, []);
  }

  private function isNotExistedWithdraw($withdrawList, $detectedWithdrawId) {
    return !isset($withdrawList[$detectedWithdrawId]);
  }

  private function transformSuspectedData($withdrawId, $bank, $inquiry) {
    return [
      'id' => $withdrawId,
      'bank' => $bank,
      'inquiry_id' => $inquiry->id
    ];
  }

  private function transformDataToUpdate($withdrawId, $inquiry) {
    return [
      'id' => $inquiry->id,
      'reference' => \json_encode(["" . $withdrawId]),
      'reference_type' => 'withdraw_id'
    ];
  }

  private function isNotCompleted($withdraw, $inquiry) {
    $sendAmount = $withdraw->amount - $withdraw->fee;
    return $withdraw->status != Withdraw::COMPLETED || $sendAmount != $inquiry->debit;
  }

  private function process($detectedInquiry, $bank) {
    $suspectedWithdrawal = [];
    $withdrawToBeUpdate = [];

    $withdrawList = $this->withdrawRepo->getWithdrawById(array_keys($detectedInquiry))
      ->keyBy('id')
      ->all();

    clog('recon_wd_bank', \json_encode($withdrawList));

    foreach ($detectedInquiry as $detectedWithdrawId => $di) {

      $notExisted = $this->isNotExistedWithdraw($withdrawList, $detectedWithdrawId);

      if ($notExisted) {
        $suspectedWithdrawal[] = $this
          ->transformSuspectedData($detectedWithdrawId, $bank, $di);
        continue;
      }

      $wd = $withdrawList[$detectedWithdrawId];

      if ($this->isNotCompleted($wd, $di)) {
        $suspectedWithdrawal[] = $this
          ->transformSuspectedData($detectedWithdrawId, $bank, $di);
        continue;
      }

      $withdrawToBeUpdate[] = $this->transformDataToUpdate($detectedWithdrawId, $di);
    }

    !empty($suspectedWithdrawal) && dispatch(new SuspectedWithdrawAlert([
      'suspected_withdraw' => $suspectedWithdrawal
    ]));

    return $withdrawToBeUpdate;
  }

}
