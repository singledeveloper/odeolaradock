<?php

namespace Odeo\Domains\Transaction\Scheduler;

use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;

class CashSettler {

  public function __construct() {
    $this->tempCash = app()->make(\Odeo\Domains\Transaction\Repository\UserTempCashRepository::class);
  }

  public function run() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(__CLASS__, 'settleCash'));
    $pipeline->execute([]);
  }

  public function settleCash(PipelineListener $listener, $data) {
    $tempCashes = $this->tempCash->getSettled();

    foreach($tempCashes as $tempCash) {
      $this->tempCash->delete($tempCash);
    }

    return $listener->response(200);
  }
}
