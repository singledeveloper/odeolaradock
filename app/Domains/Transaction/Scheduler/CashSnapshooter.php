<?php

namespace Odeo\Domains\Transaction\Scheduler;

use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;

class CashSnapshooter {

  public function __construct() {
    $this->userCash = app()->make(\Odeo\Domains\Transaction\Repository\UserCashRepository::class);
    $this->userCashSnapshot = app()->make(\Odeo\Domains\Transaction\Repository\UserCashSnapshotRepository::class);
  }

  public function run() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(__CLASS__, 'snapshotCash'));
    $pipeline->enableTransaction();
    $pipeline->execute([]);
  }

  public function snapshotCash(PipelineListener $listener, $data) {
    $userCashSums = $this->userCash->getSumByCashType();

    $cashSnapshot = [];
    foreach($userCashSums as $sum) {
      $cashSnapshot[] = [
        'date' => \Carbon\Carbon::yesterday()->format('Y-m-d'),
        'cash_type' => $sum->cash_type,
        'sum' => $sum->total,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
      ];
    }
    $this->userCashSnapshot->saveBulk($cashSnapshot);

    return $listener->response(200);
  }
}
