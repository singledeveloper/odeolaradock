<?php

namespace Odeo\Domains\Transaction\Scheduler;

use Illuminate\Support\Facades\Storage;

class ExportCleaner {

  private $exportQueueRepo;

  public function __construct() {
    $this->exportQueueRepo = app()->make(\Odeo\Domains\Transaction\Repository\ExportQueueRepository::class);
  }

  public function run() {
    foreach ($this->exportQueueRepo->getExpired() as $item) {
      if ($item->download_link) {
        Storage::disk('s3')->delete($item->download_link);
      }
      $item->is_active = false;
      $this->exportQueueRepo->save($item);
    }
  }

}
