<?php

namespace Odeo\Domains\Transaction;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;

class TempCashSelector implements SelectorListener {

  public function __construct() {
    $this->tempCash = app()->make(\Odeo\Domains\Transaction\Repository\TempCashDepositRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }
  
  public function _transforms($item, Repository $repository) {
    $temp = [];
    if ($item->id) $temp["id"] = $item->id;
    if ($item->store_id) $temp["store_id"] = $item->store_id;
    if ($item->user_id) $temp["user_id"] = $item->user_id;
    if ($item->amount) $temp["amount"] = $this->currency->formatPrice($item->amount);
    if ($item->created_at) $user["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    if ($item->updated_at) $user["updated_at"] = $item->updated_at->format("Y-m-d H:i:s");
    
    return $temp;
  }
  
  public function _extends($data, Repository $repository) {
    return $data;
  }
  
  public function get(PipelineListener $listener, $data) {
    if (isset($data["store_id"])) {
      $temp = $this->tempCash->findByStoreId($data["store_id"]);
      if ($temp) {
        return $listener->response(200,
          ["deposit_requests" => $this->_transforms($temp, $this->tempCash)]
        );
      }
    }
    else {
      $this->tempCash->normalizeFilters($data);
    
      $temps = [];
      foreach($this->tempCash->get() as $item) {
        $temps[] = $this->_transforms($item, $this->tempCash);
      }
      
      if (sizeof($temps) > 0)
        return $listener->response(200, array_merge(
          ["deposit_requests" => $this->_extends($temps, $this->tempCash)],
          $this->tempCash->getPagination()
        ));
    }
    
    return $listener->response(204, ["deposit_requests" => []]);
  }
}
