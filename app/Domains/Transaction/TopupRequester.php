<?php

namespace Odeo\Domains\Transaction;

use Odeo\Domains\Account\Jobs\SendUserBannedAlert;
use Odeo\Domains\Account\UserHistoryCreator;
use Odeo\Domains\Constant\Gojek;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\TopupStatus;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Transaction\Jobs\NotifyTopupSuccess;
use Odeo\Domains\Transaction\Jobs\SendTopupOverlimitAlert;

class TopupRequester {

  private $topup, $currency, $inserter, $selector, $notification, $orderDetail,
    $gatewayCallback, $inquiries, $orders, $userTopupBankTransfers, $users, $userHistoryCreator;

  public function __construct() {
    $this->topup = app()->make(\Odeo\Domains\Transaction\Repository\UserTopupRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->selector = app()->make(\Odeo\Domains\Transaction\TopupSelector::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->orderDetail = app()->make(\Odeo\Domains\Transaction\Repository\OrderDetailTopupRepository::class);
    $this->gatewayCallback = app()->make(\Odeo\Domains\Order\Helper\GatewayCallbacker::class);
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->userTopupBankTransfers = app()->make(\Odeo\Domains\Transaction\Repository\UserTopupBankTransferRepository::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->userHistoryCreator = app()->make(UserHistoryCreator::class);
  }

  public function request(PipelineListener $listener, $data) {

    $userId = $data["auth"]["user_id"];
    if ($this->topup->findLastTopup($userId) && !isset($data['skip_check_topup'])) {
      return $listener->response(400, trans('errors.cash.pending_topup_exists'));
    }

    $amount = $this->currency->getAmountOnly($data['cash']['amount'], $data['cash']['currency']);

    $cashLimiter = app()->make(\Odeo\Domains\Transaction\Helper\CashLimiter::class);
    if ($cashLimiter->userHasNotConfigureAgent($userId) && $data['auth']['platform_id'] != Platform::ADMIN) {
      list($isValid, $message) = $cashLimiter->check($userId, $amount);
      if (!$isValid) return $listener->response(400, $message);
    }

    $topup = $this->topup->getNew();
    $topup->user_id = $userId;
    $topup->amount = $amount;
    $topup->status = TopupStatus::PENDING;

    if ($this->topup->save($topup))
      return $listener->response(201, $this->selector->_transforms($topup, $this->topup));

    return $listener->response(400, trans('errors.database'));
  }

  public function complete(PipelineListener $listener, $data) {
    if ($topup = $this->topup->findById($data["topup_id"])) {
      if (!isAdmin($data) && $data["auth"]["user_id"] != $topup->user_id)
        return $listener->response(400, "Can't continue to verify topup.");

      $orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
      $orderDetail = $orderDetails->findById($data['order_detail_id']);

      $additionalData = [];

      $payment = $orderDetail->order->payment;
      $this->inquiries = Payment::getBankInquiryRepository($payment->info_id);

      if (app()->environment() == 'production' && $this->inquiries) {
        $inquiry = $this->inquiries->findByOrderId($orderDetail->order_id);

        if (!$inquiry && $payment->info_id == Payment::OPC_GROUP_BANK_TRANSFER_MANDIRI) {
          $bankInquiries = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository::class);
          $inquiry = $bankInquiries->findByOrderId($orderDetail->order_id);
        }

        if ($inquiry) {
          $order = $orderDetail->order;

          $limiter = app()->make(\Odeo\Domains\Transaction\Helper\CashLimiter::class);
          if ($limiter->userHasNotConfigureAgent($topup->user_id)) {
            $bankParser = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Helper\DescriptionParser::class);
            $bankUserName = $bankParser->getUserName($inquiry->description, $payment->info_id);
            $bankId = Payment::getBankId($payment->info_id);

            if (!$userTopupBankTransfer = $this->userTopupBankTransfers->findSame($topup->user_id, $bankId, $bankUserName)) {
              $userTopupBankTransfer = $this->userTopupBankTransfers->getNew();
              $userTopupBankTransfer->user_id = $topup->user_id;
              $userTopupBankTransfer->from_bank_id = $bankId;
              $userTopupBankTransfer->from_bank_account_name = $bankUserName;
              $userTopupBankTransfer->is_suspected = $bankUserName != '' && strtolower(trim($order->user->name)) != strtolower($bankUserName);
              $userTopupBankTransfer->route = 'complete_topup_requester';
            }

            $this->userTopupBankTransfers->save($userTopupBankTransfer);

            if ($this->userTopupBankTransfers->getUserIdSuspectCount($topup->user_id) >= TopupStatus::TOPUP_SUSPECT_LIMIT) {
              $user = $this->users->findById($topup->user_id);

              $user->status = UserStatus::BANNED_TOPUP_FROM_DIFFERENT_BANK_ACCOUNT;
              $this->users->save($user);

              $this->userHistoryCreator->create($user);

              $listener->pushQueue(new SendUserBannedAlert([
                'user_id' => $topup->user_id,
                'banned_reason' => 'User (' . $user->email . ') tried to topup from ' . TopupStatus::TOPUP_SUSPECT_LIMIT . ' bank accounts or more within 5 days. 
                  This guy also didn\'t register Agent and connect H2H yet.'
              ]));
            }
          }

          if (isAdmin($data)) {
            $inquiryCredit = $inquiry->credit;
            if ($order->total != $inquiryCredit) {
              $topup->amount = $inquiryCredit;
              $additionalData['skip_unique_code_refund'] = true;

              $orderDetail->sale_price = $topup->amount;
              $orderDetail->base_price = $topup->amount;
              $orderDetail->verified_at = date('Y-m-d H:i:s');
              $orderDetails->save($orderDetail);

              $order->subtotal = $topup->amount;

              $orderCharges = app()->make(\Odeo\Domains\Order\Repository\OrderChargeRepository::class);
              $charge = $orderCharges->getExisting([
                'type' => OrderCharge::UNIQUE_CODE,
                'group_type' => OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER,
                'order_id' => $order->id
              ])->first();

              $order->total = $order->subtotal + $charge->amount;
              $this->orders->save($order);
            }
          }
        } else if (isAdmin($data)) {
          $order = $orderDetail->order;
          $order->status = OrderStatus::OPENED;
          $this->orders->save($order);

          $push = app()->make(\Odeo\Domains\Notification\Helper\PushSender::class);
          $push->toUser([4350, 5193, 6096, 25551, 90051], [
            'messages' => ["Can't verify order #" . $order->id . '. No bank mutation.']
          ]);

          return $listener->response(200, [
            "skip_unique_code_refund" => true
          ]);
        }
      }

      if ($orderDetail->verified_at == null) {
        $orderDetail->verified_at = date('Y-m-d H:i:s');
        $orderDetails->save($orderDetail);
      }

      $topup->status = TopupStatus::COMPLETED;
      $this->topup->save($topup);

      $this->inserter->add([
        'user_id' => $topup->user_id,
        'trx_type' => TransactionType::TOPUP,
        'cash_type' => CashType::OCASH,
        'data' => json_encode([
          'id' => $topup->id,
          'order_id' => $orderDetail->order_id
        ]),
        'amount' => $topup->amount
      ]);

      $this->inserter->run();

      $this->notification->setup($topup->user_id, NotificationType::CHECK, NotificationGroup::TRANSACTION);
      $this->notification->topup($orderDetail->order_id, $topup->amount);
      $listener->pushQueue($this->notification->queue());

      $cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
      $cash = $cashManager->getCashBalance($topup->user_id, Currency::IDR);
      $this->gatewayCallback->callback($listener, $orderDetail->order, trans('pulsa.inline.topup_success', [
        'sisa_saldo' => $cash[$topup->user_id]['formatted_amount']
      ]));

      if ($topup->amount >= TopupStatus::TOPUP_OVERLIMIT_MIN_AMOUNT) {
        // $listener->pushQueue(new SendTopupOverlimitAlert([
        //   'user_id' => $topup->user_id,
        //   'amount' => $topup->amount,
        //   'payment_method' => $payment->information->name
        // ]));
        if ($topup->user_id == 131313) { // MIP TOPUP WARNING TO GOJEK SUPPLIER
          foreach (Gojek::GOJEK_SUPPS as $item) {
            $this->notification->setup($item, NotificationType::WARNING, NotificationGroup::TRANSACTION);
            $this->notification->gojekSupplierWarning();
          }
          $listener->pushQueue($this->notification->queue());
        }

        $user = $this->users->findById($topup->user_id);
        $internalNoticer = app()->make(\Odeo\Domains\Notification\Helper\InternalNoticer::class);
        $internalNoticer->saveMessage('oCash ' . $user->name . ' bertambah ke ' . $cash[$topup->user_id]['formatted_amount']);
      }

      $listener->pushQueue(new NotifyTopupSuccess($topup->id));

      return $listener->response(201, array_merge($this->selector->_transforms($topup, $this->topup), $additionalData));
    }

    return $listener->response(400, trans('errors.database'));
  }

  public function cancel(PipelineListener $listener, $data) {
    if ($topup = $this->topup->findLastTopup($data["auth"]["user_id"])) {
      $topup->status = TopupStatus::CANCELLED;
      if ($this->topup->save($topup)) {
        return $listener->response(200, $this->selector->_transforms($topup, $this->topup));
      }
    }

    return $listener->response(400, trans('errors.database'));
  }

  public function checkout(PipelineListener $listener, $data) {
    if ($topup = $this->topup->findLastTopup($data["auth"]["user_id"])) {
      $orderDetail = $this->orderDetail->getNew();
      $orderDetail->topup_id = $topup->id;
      $orderDetail->order_detail_id = $data['order_detail']['id'];
      $topup->order_id = $data['order']['id'];

      $this->orderDetail->save($orderDetail);
      $this->topup->save($topup);
      return $listener->response(201);
    }
    return $listener->response(400, "Unable to checkout topup.");
  }

}