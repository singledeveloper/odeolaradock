<?php

namespace Odeo\Domains\Transaction\Vendor\SMS;

use Odeo\Domains\Vendor\SMS\CenterManager;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Helper\Currency;

class CashSelector extends CenterManager {

  private $cashManager;

  public function __construct() {
    parent::__construct();
    $this->cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $this->saveHistory($data);

    $cash = $this->cashManager->getCashBalance($data['auth']['user_id'], Currency::IDR);

    $this->reply('Sisa oCash Anda ' . $cash[$data['auth']['user_id']]['formatted_amount'], $data['sms_to']);

    return $listener->response(200);
  }

}