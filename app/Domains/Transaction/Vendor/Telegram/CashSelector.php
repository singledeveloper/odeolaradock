<?php

namespace Odeo\Domains\Transaction\Vendor\Telegram;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Vendor\Telegram\TelegramManager;

class CashSelector extends TelegramManager {

  private $cashManager;

  public function __construct() {
    parent::__construct();
    $this->cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $this->initialize($data['chat_id'], $data['token']);

    $cash = $this->cashManager->getCashBalance($data['auth']['user_id'], Currency::IDR);

    $this->reply('Sisa oCash Anda ' . $cash[$data['auth']['user_id']]['formatted_amount']);

    return $listener->response(200);
  }

}