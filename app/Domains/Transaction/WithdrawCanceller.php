<?php

namespace Odeo\Domains\Transaction;

use Carbon\Carbon;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\ArtajasaMutationDisbursement;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Jobs\NotifyWithdrawStatusChanged;
use Odeo\Domains\Transaction\Repository\CashRecurringHistoryRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\ArtajasaBalanceUpdater;

class WithdrawCanceller {

  const WITHDRAW_CANCEL_REASON = [
    Withdraw::CANCELLED_FOR_WRONG_ACCOUNT_NUMBER => 'Please recheck your bank account number and resubmit',
    Withdraw::CANCELLED_FOR_INVALID_ACCOUNT_INFORMATION => 'Please recheck your bank account information and resubmit',
    Withdraw::CANCELLED_FOR_CLOSED_BANK_ACCOUNT => 'Your account cannot be used for disbursement',
    Withdraw::CANCELLED_FOR_BANK_REJECTION => 'Your disbursement is rejected by BANK'
  ];

  public function __construct() {
    $this->withdraw = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);
    $this->cash = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $this->inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->balanceUpdater = app()->make(ArtajasaBalanceUpdater::class);
  }

  public function cancel(PipelineListener $listener, $data) {

    if (isset($data['withdraw_id']) && ($withdraw = $this->withdraw->findById($data['withdraw_id']))) ;
    else {
      $withdraw = $this->withdraw->findLastWithdraw(isset($data["user_id"]) ? $data["user_id"] : $data["auth"]["user_id"]);
    }

    if (!$withdraw) return $listener->response(400, trans('errors.database'));

    $withdraw->status = $data['withdraw_status'] ?? Withdraw::CANCELLED;
    $withdraw->cancelled_at = Carbon::now()->toDateTimeString();

    if (!$this->withdraw->save($withdraw)) {
      return $listener->response(400, trans('errors.database'));
    }

    $this->inserter->add([
      'user_id' => $withdraw->user_id,
      'trx_type' => TransactionType::WITHDRAW_REFUND,
      'cash_type' => CashType::OCASH,
      'data' => json_encode(['type' => TransactionType::WITHDRAW, 'id' => $withdraw->id]),
      'amount' => $withdraw->amount
    ]);

    $this->inserter->run();

    if ($withdraw->auto_disbursement_vendor === Withdraw::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT) {
      $this->balanceUpdater->updateBalance([
        'amount' => $withdraw->amount - $withdraw->fee,
        'fee' => ArtajasaDisbursement::COST,
        'type' => ArtajasaMutationDisbursement::TYPE_REFUND,
        'disbursement_artajasa_disbursement_id' => $withdraw->auto_disbursement_reference_id,
      ]);
    }

    $this->notification->setup($withdraw->user_id, NotificationType::CHECK, NotificationGroup::TRANSACTION);
    $this->notification->withdraw_cancel(self::WITHDRAW_CANCEL_REASON[$withdraw->status] ?? '', $withdraw->is_bank_transfer);
    $listener->pushQueue($this->notification->queue());
    $listener->pushQueue(new NotifyWithdrawStatusChanged($withdraw->id));

    $cashRecurringHistoryRepo = app()->make(CashRecurringHistoryRepository::class);
    if ($recurringHistory = $cashRecurringHistoryRepo->findByReferenceId($withdraw->is_bank_transfer ? TransactionType::BANK_TRANSFER : TransactionType::WITHDRAW, $withdraw->id)) {
      $recurringHistory->status = Recurring::FAILED;
      $cashRecurringHistoryRepo->save($recurringHistory);
    }

    return $listener->response(200);

  }
}
