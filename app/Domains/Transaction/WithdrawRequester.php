<?php

namespace Odeo\Domains\Transaction;

use Carbon\Carbon;
use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Account\Repository\UserGroupRepository;
use Odeo\Domains\Account\UserHistoryCreator;
use Odeo\Domains\Account\UserKtpValidator;
use Odeo\Domains\Account\Helper\CustomerEmailManager;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\FlipDisbursement;
use Odeo\Domains\Constant\Gojek;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\PermataDisbursement;
use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Transaction\Helper\CashLimiter;
use Odeo\Domains\Transaction\Helper\CashRecurringManager;
use Odeo\Domains\Transaction\Jobs\NotifyWithdrawStatusChanged;
use Odeo\Domains\Transaction\Jobs\SendWithdrawAlert;
use Odeo\Domains\Transaction\Repository\CashRecurringHistoryRepository;
use Odeo\Domains\Transaction\Jobs\SendWithdrawComplete;
use Odeo\Domains\Transaction\Repository\UserCashRepository;
use Odeo\Domains\Vendor\Bca\Helper\CostCalculator;

class WithdrawRequester {

  private $bankRepo;

  public function __construct() {
    $this->bankRepo = app()->make(BankRepository::class);
    $this->withdraw = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);
    $this->bankAccount = app()->make(\Odeo\Domains\Account\Repository\UserBankAccountRepository::class);
    $this->cash = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->selector = app()->make(\Odeo\Domains\Transaction\WithdrawSelector::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->withdrawTnc = app()->make(\Odeo\Domains\Transaction\Helper\WithdrawTnc::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->userTopupBankTransfers = app()->make(\Odeo\Domains\Transaction\Repository\UserTopupBankTransferRepository::class);
    $this->userBannedBankAccounts = app()->make(\Odeo\Domains\Account\Repository\UserBannedBankAccountRepository::class);
    $this->cashTransactions = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
    $this->cashLimiter = app()->make(CashLimiter::class);
    $this->userHistoryCreator = app()->make(UserHistoryCreator::class);
    $this->orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
    $this->costCalculator = app()->make(CostCalculator::class);
    $this->userGroupRepo = app()->make(UserGroupRepository::class);
    $this->userCashRepo = app()->make(UserCashRepository::class);
  }

  public function request(PipelineListener $listener, $data) {
    $userId = $data["auth"]["user_id"];

    $userKtpValidator = app()->make(UserKtpValidator::class);
    if (!$userKtpValidator->isVerified($userId)) return $listener->response(400, 'Anda tidak dapat melakukan penarikan dana sebelum melakukan verifikasi KTP.');

    if (isset($data['bank_account_id'])) {
      $account = $this->bankAccount->findById($data['bank_account_id']);
      if (!$account) {
        return $listener->response(400, trans('errors.data_not_exists'));
      }
      if ($account->user_id != $userId) {
        return $listener->response(400, trans('errors.account_mismatch'));
      }
      $bank = $account->bank;
      $data['bank_id'] = $bank->id;
      $accountNumber = $data['account_number'] = $account->account_number;
      $accountName = $data['account_name'] = $account->account_name;
    } else {
      $bank = $this->bankRepo->findById($data['bank_id']);
      $accountNumber = $data['account_number'];
      $accountName = $data['account_name'];
    }

    $amount = $this->currency->getAmountOnly($data['cash']['amount'], $data['cash']['currency']);
    $isBankTransfer = isset($data['is_bank_transfer']) && $data['is_bank_transfer'] == 'true';

    if ($isBankTransfer) {
      try {
        $continue = $this->validateRecurringData($data);
      } catch (CashTransferException $e) {
        return $listener->response(400, $e->getMessage());
      }
      if (!$continue) return $listener->response(200, ['recurring_created' => true]);
    }

    $fee = $this->getWithdrawFee($userId, $bank->id, $isBankTransfer);

    $amountToProcess = $isBankTransfer ? $amount + $fee : $amount;

    list($isOverLimit, $errorMessage) = $this->isOverLimit($userId, $data['cash']);
    if ($isOverLimit) {
      return $listener->response(400, $errorMessage);
    }

    $balance = $this->cash->getCashBalance($userId);

    if (!isset($balance[$userId])) {
      return $listener->response(400, trans('errors.data_not_exists'));
    }

    if ($amountToProcess > $balance[$userId]) {
      return $listener->response(400, trans('errors.cash.insufficient_cash', [
        'cash' => 'oCash'
      ]));
    }

    $withdraw = $this->withdraw->getNew();
    $withdraw->user_id = $userId;
    $withdraw->amount = $amountToProcess;
    $withdraw->fee = $fee;

    $withdraw->account_name = $accountName;
    $withdraw->account_number = $accountNumber;
    $withdraw->account_bank = $bank->name;
    $withdraw->account_bank_logo = $bank->logo;
    $withdraw->bank_id = $bank->id;
    $withdraw->is_bank_transfer = $isBankTransfer;
    $withdraw->description = $data['description'] ?? null;

    $bankMap = $this->bankRepo->getATMBersamaBankList()->keyBy('id');
    $isAutoTransfer = Disbursement::isAutoDisbursement($bank->id) || isset($bankMap[$bank->id]);

    $whitelisted = $this->isWhitelisted($userId);

    if ($userId != UserStatus::INTERNAL_FRAUD_USER_ID && !$whitelisted) {
      if ($this->userBannedBankAccounts->findByAccountNumber($accountNumber)) {
        $withdraw->status = Withdraw::SUSPECT_CON;
        $accounts = $this->bankAccount->getByAccountNumber($accountNumber);
        foreach ($accounts as $item) {
          if ($item->user_id != UserStatus::INTERNAL_FRAUD_USER_ID) {
            $user = $this->users->findById($item->user_id);
            $user->status = UserStatus::BANNED_WITHDRAW_RELATED_TO_BANNED_BANK_ACCOUNT;

            $this->users->save($user);

            $this->userHistoryCreator->create($user);
          }
        }
        $listener->pushQueue(new SendWithdrawAlert([
          'user_id' => $withdraw->user_id,
          'amount' => $withdraw->amount,
          'account_bank' => $withdraw->account_bank,
          'account_number' => $withdraw->account_number,
          'additional_message' => 'But I have suspended this request and banned all related 
          user accounts to this bank account number. Please check and proceed carefully.'
        ]));
      } 
    }

    /*if ($isBankTransfer && $amountToProcess > 250000 && !$whitelisted
      && $cash = $this->userCashRepo->findByUserId($userId, CashType::OCASH)) {
      if ($cash->amount * 0.9 < $amountToProcess && !$this->bankAccount->findAccountNumberInUserId(UserStatus::INTERNAL_FRAUD_USER_ID, $withdraw->account_number)) {
        $withdraw->status = Withdraw::SUSPECT_CON;
        $listener->pushQueue(new SendWithdrawAlert([
          'user_id' => $withdraw->user_id,
          'amount' => $withdraw->amount,
          'account_bank' => $withdraw->account_bank,
          'account_number' => $withdraw->account_number,
          'additional_message' => 'This account want to transfer almost all of cash. Please check and proceed carefully.'
        ]));
      }
    }*/

    if ($withdraw->status != Withdraw::SUSPECT_CON) {

      $testingAutoTransfer = in_array($bank->id, [Bank::BRI, Bank::BNI]);

      if ((isProduction() || $testingAutoTransfer) || $isAutoTransfer)
        $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER;
      else {
        $withdraw->auto_disbursement_vendor = Withdraw::DISBURSEMENT_VENDOR_BANK_CLEARING;
        $withdraw->status = Withdraw::PENDING;
      }

      if ($withdraw->amount >= CashType::AMOUNT_WARNING && !$whitelisted)
        $listener->pushQueue(new SendWithdrawAlert([
          'user_id' => $withdraw->user_id,
          'amount' => $withdraw->amount,
          'account_bank' => $withdraw->account_bank,
          'account_number' => $withdraw->account_number
        ]));
    }

    $this->withdraw->save($withdraw);

    if ($isBankTransfer) {
      $this->inserter->add([
        'user_id' => $userId,
        'trx_type' => TransactionType::BANK_TRANSFER,
        'cash_type' => CashType::OCASH,
        'amount' => -$amount,
        'reference_type' => TransactionType::WITHDRAW,
        'reference_id' => $withdraw->id,
        'data' => json_encode([
          'id' => $withdraw->id,
          'to_account_name' => $withdraw->account_name,
          'description' => isset($withdraw->description) ? $withdraw->description : '',
        ])
      ]);
      $this->inserter->add([
        'user_id' => $userId,
        'trx_type' => TransactionType::BANK_TRANSFER_FEE,
        'cash_type' => CashType::OCASH,
        'amount' => -$fee,
        'reference_type' => TransactionType::WITHDRAW,
        'reference_id' => $withdraw->id,
        'data' => json_encode([
          'id' => $withdraw->id,
          'description' => isset($withdraw->description) ? $withdraw->description : ''
        ])
      ]);
    } else {
      $this->inserter->add([
        'user_id' => $userId,
        'trx_type' => TransactionType::WITHDRAW,
        'cash_type' => CashType::OCASH,
        'amount' => -$amountToProcess,
        'reference_type' => TransactionType::WITHDRAW,
        'reference_id' => $withdraw->id,
        'data' => json_encode([
          'id' => $withdraw->id
        ])
      ]);
    }

    if ($withdraw->status == Withdraw::SUSPECT_CON) {
      $this->inserter->add([
        'user_id' => app()->environment() == 'production' ? UserStatus::INTERNAL_FRAUD_USER_ID : 1,
        'trx_type' => $isBankTransfer ? TransactionType::BANK_TRANSFER : TransactionType::WITHDRAW,
        'cash_type' => CashType::OCASH,
        'amount' => $amountToProcess,
        'reference_type' => TransactionType::WITHDRAW,
        'reference_id' => $withdraw->id,
        'data' => json_encode([
          'id' => $withdraw->id,
          'to_account_name' => $withdraw->account_name
        ]),
      ]);
    }

    $withdrawTransaction = $this->inserter->run();

    $response = $this->selector->_transforms($withdraw, $this->withdraw);
    $response['tnc'] = $this->withdrawTnc->getTnc();
    $response['transaction_id'] = $withdrawTransaction[0];

    return $listener->response(201, $response);
  }

  private function isOverLimit($userId, $cash) {
    return $this->cashLimiter->isWithdrawOverlimit($userId, $cash['amount'], $cash['currency']);
  }


  public function validateWithdraw(PipelineListener $listener, $data) {
    $amount = $this->currency->getAmountOnly($data['cash']['amount'], $data['cash']['currency']);
    list($isOverLimit, $errorMessage) = $this->isOverLimit(getUserId(), $data['cash']);
    if ($isOverLimit) {
      return $listener->response(400, $errorMessage);
    }

    $balance = $this->cash->getCashBalance($data['auth']['user_id']);

    if (!isset($balance[$data['auth']['user_id']])) {
      return $listener->response(400, trans('errors.data_not_exists'));
    }

    if ($amount > $balance[$data['auth']['user_id']]) {
      return $listener->response(400, trans('errors.cash.insufficient_cash', [
        'cash' => 'oCash'
      ]));
    }

    return $listener->response(200, [
      'display_kredivo_tnc_page' => false,
    ]);

  }

  public function getConfirmationData(PipelineListener $listener, $data) {
    if (isset($data['bank_account_id'])) {
      $account = $this->bankAccount->findById($data['bank_account_id']);
      $bankId = $account->bank_id;
    } else {
      $bankId = $data['bank_id'];
    }

    $isBankTransfer = isset($data['is_bank_transfer']) && $data['is_bank_transfer'] == 'true';
    $fee = $this->getWithdrawFee($data['auth']['user_id'], $bankId, $isBankTransfer);
    $grandTotal = $isBankTransfer
      ? $data['cash_amount'] + $fee
      : $data['cash_amount'] - $fee;

    return $listener->response(200, [
      'withdraw_fee' => $this->currency->formatPrice($fee),
      'total' => $this->currency->formatPrice($data['cash_amount']),
      'grand_total' => $this->currency->formatPrice($grandTotal),
      'tnc' => $this->withdrawTnc->getTnc($isBankTransfer)
    ]);

  }

  private function getWithdrawFee($userId, $bankId, $isBankTransfer) {
    if ($this->userGroupRepo->findUserWithinGroup($userId, UserType::GROUP_BYPASS_WITHDRAW_FEE))
      return 0;
    else if ($bankId == Bank::GOPAY)
      return 4500;
    else if ($bankId == Bank::BCA && $this->cashLimiter->userHasSupplyStore($userId) && !$isBankTransfer)
      return 2500;
    return Withdraw::REAL_TIME_FEE;
  }

  private function isWhitelisted($userId) {
    return $this->userGroupRepo->findUserWithinGroup($userId, [
      UserType::GROUP_BYPASS_KYC,
      UserType::GROUP_BYPASS_CASH_LIMIT,
      UserType::GROUP_BYPASS_WITHDRAW_LIMIT,
      UserType::GROUP_BYPASS_WITHDRAW_FEE
    ]);
  }

  public function complete(PipelineListener $listener, $data) {
    if (isset($data['withdraw_id'])) {
      $withdraw = $this->withdraw->findById($data['withdraw_id']);
    } else {
      $withdraw = $this->withdraw->findLastWithdraw($data['user_id']);
    }

    if ($withdraw) {
      $cashTransaction = $this->cashTransactions->findWithdraw($withdraw->id);
      $withdraw->cost = $this->getWithdrawCost($withdraw->auto_disbursement_vendor, $withdraw->auto_disbursement_reference_id);
      $withdraw->status = Withdraw::COMPLETED;
      $withdraw->verified_at = Carbon::now()->toDateTimeString();

      $withdraw->process_by = (function () use ($data) {
        if (isset($data['auth']) && isset($data['auth']['user_id'])) {
          return $data['auth']['user_id'];
        }
        return null;
      })();;

      $this->withdraw->save($withdraw);

      $this->notification->setup($withdraw->user_id, NotificationType::CHECK, NotificationGroup::TRANSACTION);
      $this->notification->withdraw($cashTransaction->id, $withdraw->amount, $withdraw->account_bank . " - " . $withdraw->account_name, $withdraw->is_bank_transfer);
      $listener->pushQueue($this->notification->queue());
      $listener->pushQueue(new NotifyWithdrawStatusChanged($withdraw->id));

      $user = $this->users->findById($withdraw->user_id);
      $listener->addNext(new Task(CustomerEmailManager::class, 'pushJobIfEmailVerified', [
        'email' => $user->email,
        'job' => new SendWithdrawComplete($withdraw, $user)
      ]));

      if ($withdraw->bank_id == Bank::GOPAY) {
        $gojekUsers = app()->make(\Odeo\Domains\Biller\Gojek\Repository\GojekUserRepository::class);
        if ($gojekUser = $gojekUsers->findByTelephone(preg_replace('/^' . Gojek::WITHDRAW_PREFIX . '/', '', $withdraw->account_number))) {
          $gojekUser->current_balance += intval($withdraw->amount - $withdraw->fee);
          $gojekUsers->save($gojekUser);
        }
      }

      $cashRecurringHistoryRepo = app()->make(CashRecurringHistoryRepository::class);
      if ($recurringHistory = $cashRecurringHistoryRepo->findByReferenceId($withdraw->is_bank_transfer ? TransactionType::BANK_TRANSFER : TransactionType::WITHDRAW, $withdraw->id)) {
        $recurringHistory->status = Recurring::COMPLETED;
        $cashRecurringHistoryRepo->save($recurringHistory);
      }

      return $listener->response(200, $this->selector->_transforms($withdraw, $this->withdraw));
    }

    return $listener->response(400, trans('errors.database'));
  }

  private function getWithdrawCost($vendor, $referenceId) {
    switch ($vendor) {
      case Withdraw::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT:
        return BcaDisbursement::TRANSFER_BASE_COST;

      case Withdraw::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT:
        return ArtajasaDisbursement::COST;

      case Withdraw::DISBURSEMENT_VENDOR_PERMATA_API_DISBURSEMENT:
        return PermataDisbursement::DISBURSEMENT_COST;

      case Withdraw::DISBURSEMENT_VENDOR_BANK_CLEARING:
        return Withdraw::CLEARING_COST;

      case Withdraw::DISBURSEMENT_VENDOR_FLIP_API_DISBURSEMENT:
        return FlipDisbursement::FEE_LOCAL;

      default:
        return 0;
    }
  }


  private function validateRecurringData($data) {
    $cashRecurringManager = app()->make(CashRecurringManager::class);
    list($isError, $isContinue, $recData) = $cashRecurringManager->check($data);
    if ($isError) throw new CashTransferException($recData);
    if (!$isContinue) {
      $cashRecurringRepo = app()->make(\Odeo\Domains\Transaction\Repository\CashRecurringRepository::class);
      $recurring = $cashRecurringRepo->getNew();
      $recurring->user_id = $data['auth']['user_id'];
      $recurring->transaction_type = TransactionType::BANK_TRANSFER;
      $recurring->bank_id = $data['bank_id'];
      $recurring->destination_number = $data['account_number'] . '::' . $data['account_name'];
      $recurring->type = $recData;
      $recurring->amount = $data['cash']['amount'];
      $recurring->template_notes = $data['description'] ?? '';
      $recurring->is_enabled = true;
      $cashRecurringRepo->save($recurring);
      return false;
    }
    return true;
  }

}
