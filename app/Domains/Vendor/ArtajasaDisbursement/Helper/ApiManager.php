<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Helper;

use DateTime;
use DateTimeZone;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use phpseclib\Crypt\RSA;
use phpseclib\File\X509;

/**
 * Generate key:
 * https://stackoverflow.com/questions/16480846/x-509-private-public-key
 * openssl genrsa -out private.key 1024
 * openssl req -new -x509 -key private.key -out publickey.cer -days 365
 * openssl pkcs12 -export -out public_privatekey.pfx -inkey private.key -in publickey.cer
 */
class ApiManager {

  static $CONFIG = [
    'production' => [
      'host' => '10.14.29.80',
      'url' => 'http://172.31.10.96:8080/10.14.29.80:13001/Disbursement/servlet/DisburseOdeo',
      'inst_id' => '000102',
      'account_id_prefix' => '102',
      'private_key_path' => 'artajasa-disbursement/prod-private.key',
      'public_key_path' => 'artajasa-disbursement/prod-publickey.cer',
      'aj_public_key_path' => 'artajasa-disbursement/AJRemittanceDev.cer',
    ],
    'staging' => [
      'host' => '10.17.77.93',
      'url' => 'http://10.17.77.93:17990/Disbursement/servlet/DisburseOdeo',
      'inst_id' => '000002',
      'account_id_prefix' => '102',
      'private_key_path' => 'artajasa-disbursement/private.key',
      'public_key_path' => 'artajasa-disbursement/publickey.cer',
      'aj_public_key_path' => 'artajasa-disbursement/AJRemittanceDev.cer',
    ],
    'dev' => [
      'host' => '180.250.102.116',
      'url' => 'http://180.250.102.116:17990/Disbursement/servlet/DisburseOdeo',
      'inst_id' => '000002',
      'account_id_prefix' => '102',
      'private_key_path' => 'artajasa-disbursement/private.key',
      'public_key_path' => 'artajasa-disbursement/publickey.cer',
      'aj_public_key_path' => 'artajasa-disbursement/AJRemittanceDev.cer',
    ],
  ];

  static $PRIVATE_KEY_PATH;
  static $PUBLIC_KEY_PATH;
  static $AJ_PUBLIC_KEY_PATH;

  static $URL;
  static $INST_ID;
  static $ACCOUNT_ID_PREFIX;
  static $NAME;

  const TIMEOUT_SEC = 50;

  private $client, $redis;

  public function __construct() {
    $this->client = new Client();
    $this->redis = Redis::connection();
  }

  public static function init() {
    $env = app()->environment();

    if ($env != 'production' && $env != 'staging') {
      $env = 'dev';
    }

    $config = self::$CONFIG[$env];

    self::$URL = $config['url'];
    self::$INST_ID = $config['inst_id'];
    self::$ACCOUNT_ID_PREFIX = $config['account_id_prefix'];

    self::$PRIVATE_KEY_PATH = $config['private_key_path'];
    self::$PUBLIC_KEY_PATH = $config['public_key_path'];
    self::$AJ_PUBLIC_KEY_PATH = $config['aj_public_key_path'];
  }

  public function sign($components) {
    $message = implode('', $components);

    $rsa = new RSA();
    $rsa->loadKey(Storage::disk('local')->get(self::$PRIVATE_KEY_PATH));
    $rsa->setSignatureMode(RSA::SIGNATURE_PKCS1);
    $rsa->setHash('md5');

    return bin2hex($rsa->sign($message));
  }

  public function verify($components, $signature) {
    try {
      $message = implode('', $components);

      $cert = new X509();
      $cert->loadX509(Storage::disk('local')->get(self::$AJ_PUBLIC_KEY_PATH));

      $rsa = new RSA();
      $rsa->loadKey($cert->getPublicKey());
      $rsa->setSignatureMode(RSA::SIGNATURE_PKCS1);
      $rsa->setHash('md5');

      if (!$rsa->verify($message, hex2bin($signature))) {
        \Log::info('Invalid signature: ' . json_encode([$signature, $components]));
        throw new Exception('Invalid signature (verified)');
      }
    } catch (Exception $e) {
      \Log::error($e->getMessage() . PHP_EOL . $e->getTraceAsString() . PHP_EOL . json_encode($components));
      throw new Exception('Invalid signature (exception)', 0, $e);
    }
  }

  public function request($payload, $timeout = self::TIMEOUT_SEC) {
    $payloadXml = arrayToXml($payload, 'MethodCall');
    $body = $payloadXml->asXML();

    $response = $this->client->request('POST', self::$URL, [
      'headers' => [
        'Content-Type' => 'application/xml',
      ],
      'timeout' => $timeout,
      'body' => $body
    ]);
    $content = $response->getBody()->getContents();

    if (empty($content)) {
      throw new Exception('Empty response, request: ' . PHP_EOL . $body);
    }

    try {
      $result = $this->xmlStringToArray($content);

      if (isset($result['Response']['Code']) && !in_array($result['Response']['Code'], ['00', '76', '57'])) {
        clog('aj-disbursement', 'aj failed request: ' . $body);
        clog('aj-disbursement', 'aj failed response: ' . $content);
      }

      return $result;
    } catch (\Exception $e) {
      \Log::info(json_encode([$payload, $content]));
      throw $e;
    }
  }

  private function xmlStringToArray($xmlString) {
    $xml = simplexml_load_string($xmlString);
    return xmlToArray($xml);
  }

  public function transferInquiry($stan, $date, $refNumber, $bankCode, $regencyCode, $accountId, $amount, $purposeDesc,
                                  $channelType, $userId, $userName, $terminalId, $timeout, $senderPrefix) {
    $gmtDate = clone $date;
    $gmtDate->setTimezone(new DateTimeZone('UTC'));

    $formatedAmount = intval($amount);

    $payload = [
      'MethodID' => [
        'Name' => 'Inquiry.Artajasa.ATMBTransfer'
      ],
      'TransactionID' => [
        'STAN' => str_pad($stan, 6, '0', STR_PAD_LEFT),
        'TransDateTime' => $gmtDate->format('YmdHis'),
        'InstID' => self::$INST_ID,
      ],
      'TransactionInfo' => [
        'ProcCode' => '390000',
        'ChannelType' => $channelType,
        'RefNumber' => str_pad($refNumber, 12, '0', STR_PAD_LEFT),
        'TerminalID' => $terminalId,
        'CountryCode' => 'ID',
        'LocalDateTime' => $date->format('YmdHis')
      ],
      'SenderData' => [
        'AccountID' => substr(self::$ACCOUNT_ID_PREFIX . '000' . $userId, 0, 18),
        'Name' => substr(strtoupper($senderPrefix . '/' . $userName), 0, 30),
        'CurrCode' => '360',
        'Amount' => $formatedAmount,
        'Rate' => '1.0000',
        'AreaCode' => '393',
      ],
      'BeneficiaryData' => [
        'InstID' => $bankCode,
        'AccountID' => $accountId,
        'CurrCode' => '360',
        'Amount' => $formatedAmount,
        'CustRefNumber' => str_pad($refNumber, 16, '0', STR_PAD_LEFT),
        'RegencyCode' => $regencyCode,
        'PurposeCode' => '1',
        'PurposeDesc' => empty($purposeDesc) ? '-' : $purposeDesc
      ],
      'Signature' => [
        'Data' => NULL
      ]
    ];

    $payload['Signature']['Data'] = $this->sign([
      $payload['TransactionID']['STAN'],
      $payload['TransactionID']['TransDateTime'],
      $payload['TransactionID']['InstID'],
      $payload['TransactionInfo']['RefNumber'],
      $payload['TransactionInfo']['TerminalID'],
      $payload['TransactionInfo']['LocalDateTime'],
      $payload['SenderData']['AccountID'],
      $payload['SenderData']['Amount'],
      $payload['BeneficiaryData']['InstID'],
      $payload['BeneficiaryData']['AccountID'],
      $payload['BeneficiaryData']['Amount'],
      $payload['BeneficiaryData']['CustRefNumber'],
      $payload['TransactionInfo']['CountryCode'],
    ]);

    $result = $this->request($payload, $timeout > 0 ? $timeout : self::TIMEOUT_SEC);

    if (is_array($result['BeneficiaryData']['Name'])) {
      $result['BeneficiaryData']['Name'] = $result['BeneficiaryData']['Name'][0];
    }

    $this->verify([
      $result['TransactionID']['STAN'],
      $result['TransactionID']['TransDateTime'],
      $result['TransactionID']['InstID'],
      $result['SenderData']['AccountID'],
      $result['SenderData']['Amount'],
      $result['BeneficiaryData']['InstID'],
      $result['BeneficiaryData']['AccountID'],
      $result['BeneficiaryData']['Amount'],
      $result['BeneficiaryData']['CustRefNumber'],
      $result['BeneficiaryData']['Name'],
      $result['Response']['Code'],
    ], $result['Signature']['Data']);

    return $result;
  }

  public function transfer($stan, $date, $refNumber, $bankCode, $regencyCode, $accountId, $name, $amount, $purposeDesc,
                           $channelType, $userId, $userName, $terminalId, $senderPrefix) {
    $gmtDate = clone $date;
    $gmtDate->setTimezone(new DateTimeZone('UTC'));

    $formatedAmount = intval($amount);

    $payload = [
      'MethodID' => [
        'Name' => 'Transfer.Artajasa.ATMBTransfer'
      ],
      'TransactionID' => [
        'STAN' => str_pad($stan, 6, '0', STR_PAD_LEFT),
        'TransDateTime' => $gmtDate->format('YmdHis'),
        'InstID' => self::$INST_ID,
      ],
      'TransactionInfo' => [
        'ProcCode' => '400000',
        'ChannelType' => $channelType,
        'RefNumber' => str_pad($refNumber, 12, '0', STR_PAD_LEFT),
        'TerminalID' => $terminalId,
        'CountryCode' => 'ID',
        'LocalDateTime' => $date->format('YmdHis')
      ],
      'SenderData' => [
        'AccountID' => substr(self::$ACCOUNT_ID_PREFIX . '000' . $userId, 0, 18),
        'Name' => substr(strtoupper($senderPrefix . '/' . $userName), 0, 30),
        'CurrCode' => '360',
        'Amount' => $formatedAmount,
        'Rate' => '1.0000',
        'AreaCode' => '393',
      ],
      'BeneficiaryData' => [
        'InstID' => $bankCode,
        'AccountID' => $accountId,
        'CurrCode' => '360',
        'Amount' => $formatedAmount,
        'CustRefNumber' => str_pad($refNumber, 16, '0', STR_PAD_LEFT),
        'Name' => $name,
        'RegencyCode' => $regencyCode,
        'PurposeCode' => '1',
        'PurposeDesc' => empty($purposeDesc) ? '-' : $purposeDesc
      ],
      'Signature' => [
        'Data' => NULL
      ]
    ];

    $payload['Signature']['Data'] = $this->sign([
      $payload['TransactionID']['STAN'],
      $payload['TransactionID']['TransDateTime'],
      $payload['TransactionID']['InstID'],
      $payload['TransactionInfo']['RefNumber'],
      $payload['TransactionInfo']['TerminalID'],
      $payload['TransactionInfo']['LocalDateTime'],
      $payload['SenderData']['AccountID'],
      $payload['SenderData']['Amount'],
      $payload['BeneficiaryData']['InstID'],
      $payload['BeneficiaryData']['AccountID'],
      $payload['BeneficiaryData']['Amount'],
      $payload['BeneficiaryData']['CustRefNumber'],
      $payload['TransactionInfo']['CountryCode'],
    ]);

    $result = $this->request($payload);

    $this->verify([
      $result['TransactionID']['STAN'],
      $result['TransactionID']['TransDateTime'],
      $result['TransactionID']['InstID'],
      $result['SenderData']['AccountID'],
      $result['SenderData']['Amount'],
      $result['BeneficiaryData']['InstID'],
      $result['BeneficiaryData']['AccountID'],
      $result['BeneficiaryData']['Amount'],
      $result['BeneficiaryData']['CustRefNumber'],
      $result['Response']['Code'],
    ], $result['Signature']['Data']);

    return $result;
  }

  public function statusInquiry($stan, DateTime $date, $targetStan, DateTime $targetDate) {
    $gmtDate = clone $date;
    $gmtDate->setTimezone(new DateTimeZone('UTC'));

    $targetGmtDate = clone $targetDate;
    $targetGmtDate->setTimezone(new DateTimeZone('UTC'));

    $payload = [
      'MethodID' => [
        'Name' => 'Status.Artajasa.ATMBTransfer'
      ],
      'TransactionID' => [
        'STAN' => str_pad($stan, 6, '0', STR_PAD_LEFT),
        'TransDateTime' => $gmtDate->format('YmdHis'),
        'InstID' => self::$INST_ID,
      ],
      'TransactionInfo' => [
        'CountryCode' => 'ID',
        'LocalDateTime' => $date->format('YmdHis')
      ],
      'QueryTransactionID' => [
        'STAN' => str_pad($targetStan, 6, '0', STR_PAD_LEFT),
        'TransDateTime' => $targetGmtDate->format('YmdHis'),
      ],
      'Signature' => [
        'Data' => NULL
      ]
    ];

    $payload['Signature']['Data'] = $this->sign([
      $payload['TransactionID']['STAN'],
      $payload['TransactionID']['TransDateTime'],
      $payload['TransactionID']['InstID'],
      $payload['TransactionInfo']['LocalDateTime'],
      $payload['QueryTransactionID']['STAN'],
      $payload['QueryTransactionID']['TransDateTime'],
    ]);

    $result = $this->request($payload);

    $this->verify([
      $result['TransactionID']['STAN'],
      $result['TransactionID']['TransDateTime'],
      $result['TransactionID']['InstID'],
      $result['QueryTransactionID']['STAN'],
      $result['QueryTransactionID']['TransDateTime'],
      $result['Response']['Code'],
    ], $result['Signature']['Data']);

    return $result;
  }
}