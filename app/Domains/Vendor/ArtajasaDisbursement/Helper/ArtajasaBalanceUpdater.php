<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Helper;

use Odeo\Domains\Constant\ArtajasaMutationDisbursement;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaMutationRepository;

class ArtajasaBalanceUpdater {

  private $vendorDisbursements, $artajasaMutations;

  public function __construct() {
    $this->vendorDisbursements = app()->make(VendorDisbursementRepository::class);
    $this->artajasaMutations = app()->make(DisbursementArtajasaMutationRepository::class);
  }

  public function updateBalance($data) {
    \DB::transaction(function () use ($data) {
      $amount = abs($data['amount']);
      $fee = abs($data['fee']);
      $type = $data['type'];

      $this->vendorDisbursements->lock();

      $vendorDisbursement = $this->vendorDisbursements->findById(VendorDisbursement::ARTAJASA);

      $mutation = $this->artajasaMutations->getNew();
      $mutation->amount = $amount;
      $mutation->fee = $fee;
      $mutation->balance_before = $vendorDisbursement->balance;
      $mutation->disbursement_artajasa_disbursement_id = $data['disbursement_artajasa_disbursement_id'] ?? null;
      $mutation->type = $type;

      switch ($type) {
        case ArtajasaMutationDisbursement::TYPE_TRANSFER:
          $vendorDisbursement->balance -= $amount + $fee;
          break;
        case ArtajasaMutationDisbursement::TYPE_REFUND:
        case ArtajasaMutationDisbursement::TYPE_TOPUP:
          $vendorDisbursement->balance += $amount + $fee;
          break;
        default:
          break;
      }

      $mutation->balance_after = $vendorDisbursement->balance;

      $this->vendorDisbursements->save($vendorDisbursement);
      $this->artajasaMutations->save($mutation);
    });
  }


}