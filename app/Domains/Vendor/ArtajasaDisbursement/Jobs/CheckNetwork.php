<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Jobs;


use GuzzleHttp\Client;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\ApiManager;
use Odeo\Jobs\Job;

class CheckNetwork extends Job implements ShouldQueue {

  public function __construct() {
    parent::__construct();
  }

  public function handle() {
    $key = 'odeo_core:artajasa_disbursement_network_alerted';
    $url = ApiManager::$CONFIG['production']['url'];
    $redis = Redis::connection();
    $exitCode = 1;
    $message = "";

    $client = new Client();

    try {
      $response = $client->get($url, [
        'timeout' => 10,
      ]);

      if ($response->getStatusCode() == 200) {
        $exitCode = 0;
      }
      else {
        $message = 'Status code is ' . $response->getStatusCode();
      }
    } catch (\Exception $e) {
      $exitCode = 1;
      $message = $e->getMessage();
    }

    if ($exitCode == 0 || $redis->get($key)) {
      return;
    }

    $redis->setex($key, 60 * 60, 1);

    Mail::send('emails.artajasa_disbursement_network_down_alert', [
      'data' => [
        'url' => $url,
        'message' => $message,
      ]
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('disbursement@odeo.co.id')->subject('Artajasa Disbursement Network Down Alert');
    });
  }

}
