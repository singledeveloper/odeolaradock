<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Model\DisbursementArtajasaStatusInquiry;

class DisbursementArtajasaStatusInquiryRepository extends Repository {
  
  private $lock = false;
  
  public function __construct(DisbursementArtajasaStatusInquiry $disbursementArtajasaStatusInquiry) {
    $this->model = $disbursementArtajasaStatusInquiry;
  }

  public function lock() {
    $this->lock = true;
  }

  public function findLastByDate($date) {
    $model = $this->getCloneModel();
    
    if ($this->lock) $model = $this->model->lockForUpdate();

    return $model
      ->whereDate('transaction_datetime', '>=', $date)
      ->orderBy('transaction_id', 'desc')
      ->first();
  }

  public function findLastByDisbursementIds($ids) {
    $model = $this->getCloneModel();

    if ($this->lock) $model = $this->model->lockForUpdate();

    return $model
      ->select(\DB::raw('distinct on (disbursement_artajasa_disbursement_id) *'))
      ->whereIn('disbursement_artajasa_disbursement_id', $ids)
      ->orderBy('disbursement_artajasa_disbursement_id')
      ->orderBy('id', 'desc');
  }

}