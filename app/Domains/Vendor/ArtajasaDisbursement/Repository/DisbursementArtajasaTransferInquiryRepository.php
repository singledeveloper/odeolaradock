<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Repository;


use Odeo\Domains\Account\Model\Bank;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Model\DisbursementArtajasaTransferInquiry;

class DisbursementArtajasaTransferInquiryRepository extends Repository {

  private $lock = false;

  public function __construct(DisbursementArtajasaTransferInquiry $model) {
    $this->model = $model;
  }

  public function lock() {
    $this->lock = true;
  }

  /**
   * @param $bankCode Bank.aj_bank_code
   * @param $accountNumber
   * @return mixed
   */
  public function findByBankCodeAndAccountNumber($bankCode, $accountNumber) {
    $model = $this->getCloneModel();
    if ($this->lock) $model = $this->model->lockForUpdate();

    return $model
      ->where('bank_code', $bankCode)
      ->where('transfer_to', $accountNumber)
      ->whereIn('response_code', ['00', '78'])
      ->oldest()
      ->first();
  }

  public function findById($id, $fields = ['*']) {
    $model = $this->getCloneModel();
    if ($this->lock) $model = $this->model->lockForUpdate();
    return $model->find($id, $fields);
  }

  public function findLastByDate($date) {
    $model = $this->getCloneModel();

    if ($this->lock) $model = $this->model->lockForUpdate();

    return $model
      ->whereDate('transaction_datetime', '>=', $date)
      ->orderBy('transaction_id', 'desc')
      ->first();
  }

}