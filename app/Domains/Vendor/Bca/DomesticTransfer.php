<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/04/19
 * Time: 17.12
 */

namespace Odeo\Domains\Vendor\Bca;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Bca\Helper\ApiManager;
use Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository;

class DomesticTransfer {

  private $bcaDisbursements;
  private $redis;
  private $apiManager;

  public function init() {
    $this->bcaDisbursements = app()->make(DisbursementBcaDisbursementRepository::class);
    $this->apiManager = app()->make(ApiManager::class);
    $this->redis = Redis::connection();
  }

  public function exec(PipelineListener $listener, $data) {
    $this->init();

    if (!$this->redis->hsetnx('odeo_core:bca_disbursement_lock', 'id_' . $data['bca_disbursement_id'], 1)) {
      return $listener->response(400, 'duplicate transaction');
    }

    \DB::beginTransaction();

    try {

      $this->bcaDisbursements->lock();

      $bcaDisbursement = $this->bcaDisbursements->findById($data['bca_disbursement_id']);

      if ($this->isDuplicate($bcaDisbursement)) {
        throw new \Exception('duplicate transaction');
      }

      $beforeSend = Carbon::now();

      $lastUsedTransactionId = $this->redis->hincrby('odeo_core:bca_disbursement_domestic_last_used_transaction_id', 'id', 1);
      $lastUsedTransactionIdDate = $this->redis->hget('odeo_core:bca_disbursement_domestic_last_used_transaction_id', 'date');
      if (Carbon::parse($lastUsedTransactionIdDate)->diffInDays($beforeSend) > 180) {
        $lastUsedTransactionId = 1;
        $this->redis->hmset('odeo_core:bca_disbursement_domestic_last_used_transaction_id', [
          'id' => 1,
          'date' => $beforeSend->toDateString()
        ]);
      }

      $bcaDisbursement->transaction_id = sprintf('1%07s', $lastUsedTransactionId);
      $bcaDisbursement->transaction_key = $data['transaction_key'];
      $bcaDisbursement->corporate_id = $data['corporate_id'];
      $bcaDisbursement->transfer_datetime = $beforeSend;
      $bcaDisbursement->transaction_date = $beforeSend->toDateString();
      $bcaDisbursement->remark_1 = $data['remark_1'];
      $bcaDisbursement->remark_2 = $data['remark_2'];
      $bcaDisbursement->transfer_from = $data['transfer_from'];
      $bcaDisbursement->transfer_to = $data['transfer_to'];
      $bcaDisbursement->amount = $data['amount'];
      $bcaDisbursement->bank_code = $data['bank_code'];
      $bcaDisbursement->transfer_to_name = $data['transfer_to_name'];
      $this->bcaDisbursements->save($bcaDisbursement);

      \DB::commit();
    } catch (\Exception $e) {
      \DB::rollback();
      \Log::info($e->getMessage());
      \Log::info($e->getTraceAsString());
      return $listener->response(400);
    }

    try {

      $requestData = [
        'transaction_id' => str_pad($bcaDisbursement->transaction_id, 8, '0', STR_PAD_LEFT),
        'transaction_date' => $bcaDisbursement->transaction_date,
        'reference_id' => $bcaDisbursement->id,
        'source_account_number' => $bcaDisbursement->transfer_from,
        'beneficiary_account_number' => $bcaDisbursement->transfer_to,
        'benificiary_bank_code' => $bcaDisbursement->bank_code,
        'beneficiary_name' => $bcaDisbursement->transfer_to_name,
        'amount' => $bcaDisbursement->amount,
        'remark_1' => $bcaDisbursement->remark_1,
        'remark_2' => $bcaDisbursement->remark_2
      ];

      $response = $this->apiManager->domesticTransfer($requestData);

      $resultData = $response['data'];

      if (isset($resultData['ErrorCode'])) {
        $bcaDisbursement->error_code = $resultData['ErrorCode'];
        $bcaDisbursement->error_reason = $resultData['ErrorMessage']['English'];
        throw new \Exception(json_encode($resultData), $response['status_code']);
      }

      $resultData['TransactionID'] = ltrim($resultData['TransactionID'], '0');

      if ($resultData['TransactionID'] != $bcaDisbursement->transaction_id ||
        $resultData['TransactionDate'] != $bcaDisbursement->transaction_date ||
        $resultData['ReferenceID'] != $bcaDisbursement->id) {
        throw new \Exception(json_encode($resultData), $response['status_code']);
      }

      $bcaDisbursement->status = BcaDisbursement::SUCCESS;
      $bcaDisbursement->bca_status = 'Success';
      $bcaDisbursement->ppu_number = $resultData['PPUNumber'];

      $bcaDisbursement->error_exception_message = null;
      $bcaDisbursement->error_exception_code = null;

    } catch (\Exception $e) {
      if (strpos($e->getMessage(), 'cURL error 28') !== false) {
        \Log::info("bca disbursement {$bcaDisbursement->id} timeout");
        $bcaDisbursement->status = BcaDisbursement::TIMEOUT;
        $bcaDisbursement->error_code = 'TIMEOUT';
      }
      else {
        $bcaDisbursement->status = BcaDisbursement::FAIL;
      }

      $bcaDisbursement->error_exception_message = $e->getMessage();
      $bcaDisbursement->error_exception_code = $e->getCode();
    }

    $bcaDisbursement->response_datetime = Carbon::now()->toDateTimeString();

    $this->bcaDisbursements->save($bcaDisbursement);

    $this->redis->hdel('odeo_core:bca_disbursement_lock', 'id_' . $data['bca_disbursement_id']);

    if ($bcaDisbursement->status != BcaDisbursement::SUCCESS) {
      return $listener->response(400, [
        'error_code' => $bcaDisbursement->error_code && strlen($bcaDisbursement->error_code) ? $bcaDisbursement->error_code : '0'
      ]);
    }
  }

  private function isDuplicate($disbursement) {
    return $disbursement->bca_status != null
      || strlen($disbursement->bca_status) > 0
      || $disbursement->response_datetime;
  }

}