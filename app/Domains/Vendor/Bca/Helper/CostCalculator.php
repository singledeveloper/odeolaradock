<?php
/**
 * Created by PhpStorm.
 * User: hs
 * Date: 9/1/18
 * Time: 4:22 PM
 */

namespace Odeo\Domains\Vendor\Bca\Helper;


use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository;

class CostCalculator {

  private $bcaDisbursementRepo;

  public function __construct() {
    $this->bcaDisbursementRepo = app()->make(DisbursementBcaDisbursementRepository::class);
  }

  public function getCostByDate($date) {
    $n = $this->bcaDisbursementRepo->countSuccessByPeriod($date->format('Y-m'));
    return $this->getCost($n);
  }

  public function getCostById($id) {
    $m = $this->bcaDisbursementRepo->getRowNumberOfMonth($id);
    return $this->getCost($m->rn);
  }

  private function getCost($rn) {
    if ($rn < BcaDisbursement::TRANSFER_API_LIMIT) {
      return BcaDisbursement::TRANSFER_BASE_COST;
    }
    return BcaDisbursement::TRANSFER_EXCEEDED_COST;
  }
}