<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/04/19
 * Time: 15.43
 */

namespace Odeo\Domains\Vendor\Bca;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\InternalRefund;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\Bca\Helper\BcaResponseMapper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Odeo\Domains\Vendor\Bca\Helper\ApiManager;
use Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository;

class InternalRefundRequester {

  private $bankScrapeAccountNumberRepo, $redis, $bankRepo,
    $bcaDisbursementRepo, $bcaApiManager, $bcaResponseMapper;

  public function init() {
    $this->bankRepo = app()->make(BankRepository::class);
    $this->bcaDisbursementRepo = app()->make(DisbursementBcaDisbursementRepository::class);
    $this->bankScrapeAccountNumberRepo = app()->make(BankScrapeAccountNumberRepository::class);
    $this->bcaResponseMapper = app()->make(BcaResponseMapper::class);
    $this->bcaApiManager = app()->make(ApiManager::class);
    $this->redis = Redis::connection();
  }

  public function request(PipelineListener $listener, $data) {
    $this->init();

    $bankScrapeAccountNumber = $this->bankScrapeAccountNumberRepo->findByID($data['account_number_id']);

    if (!$bankScrapeAccountNumber || !$bankScrapeAccountNumber->number) {
      return $listener->response(400, 'Unknown bank account');
    }

    if (!$bankId = InternalRefund::getBankId($bankScrapeAccountNumber)) {
      return $listener->response(400, 'Unknown bank account');
    }

    if ($data['password'] != env('INTERNAL_REFUND_REQUEST_PASSWORD')) {
      return $listener->response(400, 'Invalid password');
    }

    if ($this->isDuplicate($bankId)) {
      return $listener->response(400, 'Dalam waktu 5 menit terakhir, sudah ada yang transfer ke bank ini');
    }

    if (!$this->lock($bankId)) {
      return $listener->response(400, 'Sedang ada transfer yang terjadi');
    }

    $key = 'odeo_core:internal_refund_id';
    $internalRefundId = $this->redis->incr($key);
    $bank = $this->bankRepo->findById($bankId);

    $bca = $this->bcaDisbursementRepo->getNew();
    $bca->disbursement_type = Disbursement::INTERNAL_REFUND;
    $bca->disbursement_reference_id = 0;
    $bca->status = BcaDisbursement::PENDING;
    $this->bcaDisbursementRepo->save($bca);

    $pipeline = new Pipeline();

    $pipeline->add(new Task(DomesticTransfer::class, 'exec', [
      'bca_disbursement_id' => $bca->id,
      'corporate_id' => ApiManager::$CORPORATEID,
      'remark_1' => 'Internal Refund',
      'remark_2' => '#' . $internalRefundId,
      'transfer_from' => ApiManager::$ACCOUNTNUMBER[0],
      'transfer_to' => $bankScrapeAccountNumber->number,
      'transaction_key' => 'INTERNALREFUND#' . $internalRefundId,
      'amount' => $data['amount'],
      'bank_code' => $bank->swift_code,
      'transfer_to_name' => InternalRefund::INTERNAL_REFUND_USER_NAME
    ]));

    $pipeline->execute();

    if ($pipeline->success()) {
      $this->redis->hset('odeo_core:internal_refund', $bankId, time());
      $this->unlock($bankId);
      return $listener->response(200);
    } else {
      $responseCode = $pipeline->errorMessage['error_code'] ?? null;
      $statusCode = $this->bcaResponseMapper->map($responseCode);
      $this->unlock($bankId);

      switch ($statusCode) {
        case ApiDisbursement::SUSPECT:
          return $listener->response(400, 'Transaksi suspect. Silakan cek transaksi ini terlebih dahulu');

        default:
          $statusMessage = ApiDisbursement::getStatusMessage($statusCode);
          return $listener->response(400, "Internal refund #$internalRefundId failed: $statusMessage");
      }
    }
  }

  private function lock($bankId) {
    return $this->redis->hsetnx('odeo_core:internal_refund_lock', $bankId, 1);
  }

  private function unlock($bankId) {
    $this->redis->hdel('odeo_core:internal_refund_lock', $bankId);
  }

  private function isDuplicate($bankId) {
    $lastTime = $this->redis->hget('odeo_core:internal_refund', $bankId);

    if ($lastTime == null) {
      return false;
    }

    return time() - $lastTime < 5 * 60;
  }

}