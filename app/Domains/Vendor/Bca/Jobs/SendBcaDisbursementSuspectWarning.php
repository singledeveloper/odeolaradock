<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/8/17
 * Time: 10:47 PM
 */

namespace Odeo\Domains\Vendor\Bca\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendBcaDisbursementSuspectWarning extends Job  {

  private $bcaDisbursementSuspects, $data;

  public function __construct($bcaDisbursementSuspects, $data) {
    parent::__construct();
    $this->bcaDisbursementSuspects = $bcaDisbursementSuspects;
    $this->data = $data;

  }

  public function handle() {

    Mail::send('emails.bca_disbursement_suspect_warning', ['data' => [
      'bca_disbursement_suspect' => $this->bcaDisbursementSuspects->toArray(),
      'additional' => $this->data
    ]], function ($m) {

      $m->from('noreply@odeo.co.id', 'odeo');

      $m->to('vincent.wu.vt@gmail.com')->subject('Bca Disbursement Suspect - ' . time());

    });
  }

}
