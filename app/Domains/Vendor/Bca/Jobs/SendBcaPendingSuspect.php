<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/8/17
 * Time: 10:47 PM
 */

namespace Odeo\Domains\Vendor\Bca\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendBcaPendingSuspect extends Job  {

  public function __construct() {
    parent::__construct();
  }

  public function handle() {
    Mail::send('emails.bca_fetch_mutation_pending_suspect', [], function ($m) {
      $m->from('noreply@odeo.co.id', 'ODEO');
      $m->to('pg@odeo.co.id')->subject('Bca fetch mutation pending suspect - ' . time());
    });
  }

}
