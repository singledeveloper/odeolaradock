<?php

namespace Odeo\Domains\Vendor\Bca\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository;
use Odeo\Domains\Vendor\Bca\Helper\ApiManager;

class CheckBalanceScheduler {

  private $vendorDisbursementRepo, $apiManager;

  private function init() {
    $this->vendorDisbursementRepo = app()->make(VendorDisbursementRepository::class);
    $this->apiManager = app()->make(ApiManager::class);
  }

  public function run() {
    $this->init();
    $vendor = $this->vendorDisbursementRepo->findById(VendorDisbursement::BCA);

    $result = $this->apiManager->getBalance();
    
    if ($result['status_code'] == 200 && isset($result['data']['AccountDetailDataSuccess']) 
    && isset($result['data']['AccountDetailDataSuccess'][0])
    && isset($result['data']['AccountDetailDataSuccess'][0]['AvailableBalance'])) {
      $vendor->balance = $result['data']['AccountDetailDataSuccess'][0]['AvailableBalance'];
      $this->vendorDisbursementRepo->save($vendor);
    }
    return $vendor->balance;
  }

}