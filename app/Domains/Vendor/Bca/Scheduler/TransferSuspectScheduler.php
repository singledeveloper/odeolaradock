<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/8/17
 * Time: 9:51 PM
 */

namespace Odeo\Domains\Vendor\Bca\Scheduler;


use Odeo\Domains\Vendor\Bca\Jobs\SendBcaDisbursementSuspectWarning;

class TransferSuspectScheduler {

  private $bcaDisbursements;

  public function __construct() {
    $this->bcaDisbursements = app()->make(\Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository::class);
  }

  public function run() {

    if (!($suspected = $this->bcaDisbursements->getSuspectNotRunning()) || $suspected->isEmpty()) return;

    dispatch(new SendBcaDisbursementSuspectWarning($suspected, [
      'reason' => 'Suspect not running'
    ]));
  }
}