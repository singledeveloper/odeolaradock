<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/25/17
 * Time: 9:25 PM
 */

namespace Odeo\Domains\Vendor\Bca;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Core\PipelineListener;

class Transfer {

  private $bcaDisbursements;
  private $redis;
  private $apiManager;

  public function __construct() {
    $this->bcaDisbursements = app()->make(\Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository::class);
    $this->apiManager = app()->make(\Odeo\Domains\Vendor\Bca\Helper\ApiManager::class);
    $this->redis = Redis::connection();
  }

  public function exec(PipelineListener $listener, $data) {
    if (!$this->redis->hsetnx('odeo_core:bca_disbursement_lock', 'id_' . $data['bca_disbursement_id'], 1)) {
      return $listener->response(400, 'duplicate transaction');
    }

    $beforeSend = Carbon::now();

    \DB::beginTransaction();

    try {

      $this->bcaDisbursements->lock();

      $bcaDisbursement = $this->bcaDisbursements->findById($data['bca_disbursement_id']);

      if ($bcaDisbursement->bca_status != null || strlen($bcaDisbursement->bca_status) > 0) {
       throw new \Exception('duplicate transaction');
      }

      if ($bcaDisbursement->response_datetime) {
        throw new \Exception('duplicate transaction');
      }

      if (isset($data['amount']) && $bcaDisbursement->amount == 0) {
        $bcaDisbursement->amount = $data['amount'];
      }

      $bcaDisbursement->corporate_id = $data['corporate_id'];
      $bcaDisbursement->remark_1 = $data['remark_1'];
      $bcaDisbursement->remark_2 = $data['remark_2'];
      $bcaDisbursement->transfer_from = $data['transfer_from'];
      $bcaDisbursement->transfer_to = $data['transfer_to'];
      $bcaDisbursement->transfer_datetime = $beforeSend->toDateTimeString();
      $bcaDisbursement->transaction_date = $beforeSend->toDateString();
      $bcaDisbursement->transaction_key = $data['transaction_key'];

      $lastUsedTransactionId = $this->redis->hincrby('odeo_core:bca_disbursement_last_used_transaction_id', 'id', 1);
      $lastUsedTransactionIdDate = $this->redis->hget('odeo_core:bca_disbursement_last_used_transaction_id', 'date');

      if ($lastUsedTransactionIdDate != $beforeSend->toDateString()) {
        $lastUsedTransactionId = 1;
        $this->redis->hmset('odeo_core:bca_disbursement_last_used_transaction_id', [
          'id' => 1,
          'date' => $beforeSend->toDateString()
        ]);
      }

      $bcaDisbursement->transaction_id = '1' . $lastUsedTransactionId;

      $this->bcaDisbursements->save($bcaDisbursement);

      \DB::commit();

    } catch (\Exception $exception) {
      \DB::rollback();
      return $listener->response(400);
    }

    $afterSend = null;

    try {

      $response = $this->apiManager->transfer([
        'transaction_id' => str_pad($bcaDisbursement->transaction_id, 8, '0', STR_PAD_LEFT),
        'reference_id' => $bcaDisbursement->id,
        'remark_1' => $bcaDisbursement->remark_1,
        'remark_2' => $bcaDisbursement->remark_2,
        'source_account_number' => $bcaDisbursement->transfer_from,
        'beneficiary_account_number' => $bcaDisbursement->transfer_to,
        'amount' => $bcaDisbursement->amount,
        'corporate_id' => $bcaDisbursement->corporate_id,
        'transaction_date' => $beforeSend->toDateString()
      ]);

      $resultData = $response['data'];

      if (isset($resultData['ErrorCode'])) {
        $bcaDisbursement->error_code = $resultData['ErrorCode'];
        $bcaDisbursement->error_reason = $resultData['ErrorMessage']['English'];
        throw new \Exception(json_encode($resultData), $response['status_code']);
      }

      $resultData['TransactionID'] = ltrim($resultData['TransactionID'], '0');

      if ($resultData['TransactionID'] != $bcaDisbursement->transaction_id ||
        $resultData['TransactionDate'] != $bcaDisbursement->transaction_date ||
        $resultData['ReferenceID'] != $bcaDisbursement->id ||
        strtolower($resultData['Status']) != 'success'
      ) {
        throw new \Exception(json_encode($resultData), $response['status_code']);
      }

      $bcaDisbursement->status = BcaDisbursement::SUCCESS;
      $bcaDisbursement->bca_status = $resultData['Status'];

      $bcaDisbursement->error_exception_message = null;
      $bcaDisbursement->error_exception_code = null;

    } catch (\Exception $exception) {
      if (strpos($exception->getMessage(), 'cURL error 28') !== false) {
        \Log::info("bca disbursement {$bcaDisbursement->id} timeout");
        $bcaDisbursement->status = BcaDisbursement::TIMEOUT;
        $bcaDisbursement->error_code = 'TIMEOUT';
      }
      else {
        $bcaDisbursement->status = BcaDisbursement::FAIL;
      }

      $bcaDisbursement->error_exception_message = $exception->getMessage();
      $bcaDisbursement->error_exception_code = $exception->getCode();
    }

    $bcaDisbursement->response_datetime = Carbon::now()->toDateTimeString();

    $this->bcaDisbursements->save($bcaDisbursement);

    $this->redis->hdel('odeo_core:bca_disbursement_lock', 'id_' . $data['bca_disbursement_id']);

    if ($bcaDisbursement->status != BcaDisbursement::SUCCESS) {
      return $listener->response(400, [
        'error_code' => $bcaDisbursement->error_code && strlen($bcaDisbursement->error_code) ? $bcaDisbursement->error_code : '0'
      ]);
    }

    return $listener->response(200);

  }

}