<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/29/17
 * Time: 9:26 PM
 */

namespace Odeo\Domains\Vendor\Bca;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class TransferRecordSelector implements SelectorListener {

  private $bcaDisbursements;

  public function __construct() {
    $this->bcaDisbursements = app()->make(\Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {

    $response['bca_disbursement_id'] = $item->id;
    $response['transfer_from'] = [
      'corporate_id' => $item->corporate_id,
      'account_number' => $item->transfer_from
    ];
    $response['transfer_to'] = $item->transfer_to;
    $response['remark'] = [
      'remark_1' => $item->remark_1,
      'remark_2' => $item->remark_2
    ];
    $response['transfer_datetime'] = $item->transfer_datetime;
    $response['response_datetime'] = $item->response_datetime;
    $response['amount'] = $item->amount;
    $response['disbursement_type'] = $item->disbursement_type;
    $response['disbursement_reference_id'] = $item->disbursement_reference_id;
    $response['status'] = $item->status;
    $response['bca_status'] = $item->bca_status;
    $response['error_code'] = $item->error_code;
    $response['error_reason'] = $item->error_reason;
    $response['error_exception_message'] = $item->error_exception_message;
    $response['error_exception_code'] = $item->error_exception_code;

    return $response;
  }

  public function get(PipelineListener $listener, $data) {

    $this->bcaDisbursements->normalizeFilters($data);
    $this->bcaDisbursements->setSimplePaginate(true);

    $bcaDisbursementRecords = [];

    foreach ($this->bcaDisbursements->get() as $bcaDisbursement) {
      $bcaDisbursementRecords[] = $this->_transforms($bcaDisbursement, $this->bcaDisbursements);
    }

    if (sizeof($bcaDisbursementRecords) > 0) {
      return $listener->response(200, array_merge(
        ["bca_disbursement_record" => $this->_extends($bcaDisbursementRecords, $this->bcaDisbursements)],
        $this->bcaDisbursements->getPagination()
      ));
    }
    return $listener->response(204, ["bca_disbursement_record" => []]);
  }

  public function getSuspectDisbursements(PipelineListener $listener, $data) {
    if ($vendorDisbursements = $this->bcaDisbursements->getSuspectDisbursements()) {
      return $listener->response(200, ['suspect_disbursement_record' => $vendorDisbursements]);
    }

    return $listener->response(204, ['suspect_disbursement_record' => []]);
  }

}
