<?php
/**
 * Created by PhpStorm.
 * User: odeo
 * Date: 12/9/17
 * Time: 5:10 PM
 */

namespace Odeo\Domains\Vendor\Bni\Scheduler;


use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository;
use Odeo\Domains\Vendor\Bni\Helper\ApiManager;

class AutoRecordBalanceScheduler {

  private $vendorDisRepo, $apiManager;

  public function __construct() {
    $this->vendorDisRepo = app()->make(VendorDisbursementRepository::class);
    $this->apiManager = app()->make(ApiManager::class);
  }


  public function run() {
    $accountNumber = ApiManager::getConfig('account_number');

    $account = $this->apiManager->getBalance($accountNumber);
    clog('bni-balance', json_encode($account));

    if ($account && $account->responseCode == '0001' && $account->accountBalance) {
      $this->vendorDisRepo->getModel()
        ->where('id', VendorDisbursement::BNI)
        ->update(['balance' => $account->accountBalance]);
    }

  }
}
