<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 12/07/19
 * Time: 14.12
 */

namespace Odeo\Domains\Vendor\Bri\Helper;


class BriResponse {
  const VA_NOT_FOUND = 'VA number not found';
  const INSUFFICIENT_AMOUNT = 'Saldo Tidak Cukup';
  const REFERENCE_USED = 'Reference number is already used';
  const DUPLICATE_JOURNAL = 'Duplicate journal seq';
}