<?php

namespace Odeo\Domains\Vendor\Bri\Scheduler;


use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository;
use Odeo\Domains\Vendor\Bri\Helper\ApiManager;

class AutoRecordBalanceScheduler {

  private $vendorDisRepo, $apiManager;

  public function __construct() {
    $this->vendorDisRepo = app()->make(VendorDisbursementRepository::class);
    $this->apiManager = app()->make(ApiManager::class);
  }


  public function run() {

    $accountNumber = ApiManager::$accountNumber;
    $account = $this->apiManager->getBalance($accountNumber);

    if (isset($account->Data->sourceAccountBalace)) {
      $this->vendorDisRepo->getModel()
        ->where('id', VendorDisbursement::BRI)
        ->update(['balance' => $account->Data->sourceAccountBalace]);
    }

  }
}