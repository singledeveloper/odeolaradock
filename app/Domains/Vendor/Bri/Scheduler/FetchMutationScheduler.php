<?php

namespace Odeo\Domains\Vendor\Bri\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Repository\BankBriInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Odeo\Domains\Vendor\Bri\Helper\ApiManager;

class FetchMutationScheduler {

  private $api, $accountNumbers, $briInquiries;

  public function __construct() {
    $this->api = app()->make(ApiManager::class);
    $this->accountNumbers = app()->make(BankScrapeAccountNumberRepository::class);
    $this->briInquiries = app()->make(BankBriInquiryRepository::class);
  }

  public function run() {

    if (isProduction()) return;

    $bankAccountNumber = $this->accountNumbers->findByName('bri');

    $maxScrapedDate = $this->getMaxScrapedDate();
    $lastScrappedDate = $this->getLastScraperDate($bankAccountNumber->last_scrapped_date);
    $accountNumber = ApiManager::$accountNumber;

    $response = $this->api->getMutation($accountNumber,
      $lastScrappedDate->format('d-m-Y'), $maxScrapedDate->format('d-m-Y')
    );

    if (!$response->status ||  !$response->data) {
      return;
    }

    $inquiryList = $this->briInquiries->getListByAccountNumberIdBetweenDate(
      $bankAccountNumber->id,
      $lastScrappedDate->toDateString(),
      $maxScrapedDate->addDay()->toDateString()
    )->map(function($inquiry) { return [$this->getInquiryKey($inquiry), $inquiry]; })
    ->pluck(1, 0);

    foreach ($response->data as $result) {
      $key = $this->getInquiryKey([
        'date' => Carbon::parse($result->tanggal_tran),
        'description' => $result->ket_tran,
        'debit' => $result->mutasi_debet,
        'credit' => $result->mutasi_kredit
      ]);

      if($inquiryList->has($key)){
        continue;
      }

      $inquiry = $this->briInquiries->getNew();

      $inquiry->bank_scrape_account_number_id = $bankAccountNumber->id;
      $inquiry->date = $result->tanggal_tran;
      $inquiry->description = $result->ket_tran;
      $inquiry->debit = $result->mutasi_debet;
      $inquiry->credit = $result->mutasi_kredit;
      $inquiry->balance = $result->saldo_akhir_mutasi;
      $inquiry->bank_reff = $result->nomor_reff;
      $inquiry->teller_id = $result->channel_id;
      $inquiry->transaction_type = $result->jenis_tran;
      $inquiry->transaction_code = $result->kode_tran;
      $inquiry->balance_before = $result->saldo_awal_mutasi;
      $inquiry->account_number = $result->nomor_rekening;
      $inquiry->scale_position = $result->posisi_neraca;
      $inquiry->currency = $result->mata_uang;

      $this->briInquiries->save($inquiry);

    }

    $accountNumber->last_scrapped_date = Carbon::now();
    $accountNumber->last_time_scrapped = Carbon::now();
    $this->accountNumbers->save($accountNumber);
  }

  private function getLastScraperDate($lastScrappedDate) {

    if ($lastScrappedDate) {
      return Carbon::parse($lastScrappedDate);
    }

    return Carbon::today()->subDay(30);
  }

  private function getMaxScrapedDate() {
    return Carbon::now();
  }

  private function getInquiryKey($inquiry) {
    return $inquiry['date']->format('YmdHis') . trim($inquiry['description']) .
      number_format($inquiry['debit']) . number_format($inquiry['credit']);
  }

}
