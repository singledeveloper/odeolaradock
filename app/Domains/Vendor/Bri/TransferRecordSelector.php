<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 28/06/19
 * Time: 11.52
 */

namespace Odeo\Domains\Vendor\Bri;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Vendor\Bri\Repository\DisbursementBriDisbursementRepository;

class TransferRecordSelector implements SelectorListener {

  private $briDisbursementRepo;

  function __construct() {
    $this->briDisbursementRepo = app()->make(DisbursementBriDisbursementRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $response = [
      'bri_disbursement_id' => $item->id,
      'remark' => $item->remark,
      'transfer_to' => $item->transfer_to,
      'transfer_datetime' => $item->transfer_datetime,
      'response_datetime' => $item->response_datetime,
      'amount' =>  $item->amount,
      'status' => $item->status,
      'disbursement_type' => $item->disbursement_type,
      'disbursement_reference_id' => $item->disbursement_reference_id,
      'referral_number' => $item->referral_number,
      'response_code' => $item->response_code,
      'response_desc' => $item->response_description,
      'error_desc' => $item->error_description,
      'response_log' => $item->response_log
    ];
    return $response;
  }

  public function get(PipelineListener $listener, $data) {
    $this->briDisbursementRepo->normalizeFilters($data);

    $briDisbursementRecords = [];

    foreach ($this->briDisbursementRepo->get() as $briDisbursement) {
      $briDisbursementRecords[] = $this->_transforms($briDisbursement, $this->briDisbursementRepo);
    }

    if(count($briDisbursementRecords)) {
      return $listener->response(200, [
        'bri_disbursement_record' => $briDisbursementRecords
      ],
        $this->briDisbursementRepo->getPagination()
      );
    }

    return $listener->response(204, ['bri_disbursement_record' => []]);
  }

  public function getSuspectDisbursements(PipelineListener $listener, $data) {
    if ($vendorDisbursements = $this->briDisbursementRepo->getSuspectDisbursements()) {
      return $listener->response(200, ['suspect_disbursement_record' => $vendorDisbursements]);
    }

    return $listener->response(204, ['suspect_disbursement_record' => []]);
  }

}