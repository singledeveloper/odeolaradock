<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/29/17
 * Time: 7:23 PM
 */

namespace Odeo\Domains\Vendor\Cimb\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Repository\BankCimbInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Odeo\Domains\Vendor\Cimb\Helper\ApiManager;
use \Odeo\Domains\Constant\CimbApi;

class FetchMutationScheduler {

  private $accountNumberRepo, $cimbInquiryRepo;

  public function __construct() {
    $this->accountNumberRepo = app()->make(BankScrapeAccountNumberRepository::class);
    $this->cimbInquiryRepo = app()->make(BankCimbInquiryRepository::class);
  }


  public function run() {
    try {
      $this->process();
    } catch (\Exception $e) {
    }
  }


  private function process() {
    $accountNumber = $this->accountNumberRepo->findById(VendorDisbursement::CIMB);

    $maxDateTime = Carbon::now();

    $lastScrappedDate = $this->getLastScrappedDate($accountNumber, $maxDateTime);

    $scrapedData = $this->cimbInquiryRepo->getInquiryBetweenDate($lastScrappedDate, $maxDateTime);
    $scrapedDataKey = $this->transformInquriesDataToKey($scrapedData);

    $mutationRows = [];

    foreach (['A'] as $direction) {
      $mutationRows = $this->fetchMutation(
        $accountNumber->id, $direction,
        $lastScrappedDate->format('Ymd'), $maxDateTime->format('Ymd'),
        $mutationRows, $scrapedDataKey
      );
    }

    if (count($mutationRows)) {
      $this->cimbInquiryRepo->saveBulk($mutationRows);
    }

    $accountNumber->last_scrapped_date = $maxDateTime;
    $accountNumber->last_time_scrapped = $maxDateTime;

    $this->accountNumberRepo->save($accountNumber);
  }

  public function fetchMutation($accountNumberId, $direction, $dateFrom, $dateTo, $mutationRows, $scrapedDataKey) {
    $dom = new \DOMDocument();
    $dateTime = Carbon::now();
    $client = ApiManager::getClient();

    list($copId) = ApiManager::getConfig(['cop_id']);

    $txnRequestDateTime = $dateTime->format('YmdHis');
    $requestId = CimbApi::generateSeqUniqTrxId(CimbApi::GET_MUTATION_PREFIX);

    $data = [
      'tokenAuth' => ApiManager::generateAuthToken($txnRequestDateTime, $requestId, ApiManager::SERVICE_TRX_INQUIRY),
      'txnData' => ApiManager::generateTransactionInquiryData($direction, $dateFrom, $dateTo),
      'serviceCode' => ApiManager::SERVICE_TRX_INQUIRY,
      'corpID' => $copId,
      'requestID' => $requestId,
      'txnRequestDateTime' => $txnRequestDateTime
    ];

    $response = $client->call(ApiManager::$SOAP, [$data]);

    $dom->loadXML($response['txnData']);

    foreach ($dom->getElementsByTagName('data') as $node) {
      $temp = [];

      foreach ($node->childNodes as $childNode) {
        if ($childNode->nodeName == '#text') continue;
        $temp[$childNode->nodeName] = $childNode->nodeValue;
      }

      $formattedRow['bank_scrape_account_number_id'] = $accountNumberId;
      $formattedRow['description'] = $temp['remarks'];
      $formattedRow['debit'] = $this->getAmount($temp['debitCredit'], 'D', $temp['amount']);
      $formattedRow['credit'] = $this->getAmount($temp['debitCredit'], 'C', $temp['amount']);
      $formattedRow['bank_reff_no'] = $temp['bankReffNo'];
      $formattedRow['date'] = Carbon::createFromFormat('Ymd H:i:s', $temp['trxDate']);
      $formattedRow['created_at'] = Carbon::now();
      $formattedRow['updated_at'] = Carbon::now();

      if ($this->checkDuplicateInquriesInSameDate($formattedRow, $scrapedDataKey)) {
        continue;
      }

      $formattedRow['date'] = $this->parseDate($temp['trxDate']);
      $mutationRows[] = $formattedRow;
    }

    return $mutationRows;

  }

  private function generateKey($data) {
    return $data['description'] . '#' . floor($data['debit']) . '#' . floor($data['credit']) . '#' . $data['bank_reff_no'] . '#' . $data['date'];
  }


  private function getAmount($value, $comparator, $amount) {
    return $value == $comparator ? $amount : 0;
  }

  private function getLastScrappedDate($cimbAccount, $maxDate) {
    return Carbon::parse($cimbAccount->last_scrapped_date) ?? $maxDate;
  }

  private function transformInquriesDataToKey($inquiry) {
    return $inquiry
      ->keyBy(function ($iq) {
        $iq->date = $this->deparseDate($iq->date, $iq->created_at);
        return $this->generateKey($iq);
      })
      ->keys()
      ->all();
  }

  private function checkDuplicateInquriesInSameDate($formattedRow, $scrapedDataKey) {
    return in_array($this->generateKey($formattedRow), $scrapedDataKey);
  }


  private function deparseDate($date, $createdAt) {
    $d = Carbon::parse($date);
    $c = Carbon::parse($createdAt);
    if ($d->hour == 12 && in_array($c->hour, [12, 13])) {
      return $d->subHour(12);
    }
    return $d;
  }

  private function parseDate($rawDate) {
    $r = Carbon::parse($rawDate);
    $c = Carbon::now();
    if ($r->hour == 0 && in_array($c->hour, [12, 13])) {
      return $r->addHour(12);
    }
    return $r;
  }

}
