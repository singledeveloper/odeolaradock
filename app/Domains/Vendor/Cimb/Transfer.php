<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/20/17
 * Time: 2:47 PM
 */

namespace Odeo\Domains\Vendor\Cimb;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\CimbDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Cimb\Helper\ApiManager;
use Odeo\Domains\Vendor\Cimb\Repository\DisbursementCimbDisbursementRepository;
use Odeo\Exceptions\FailException;

class Transfer {

  private $redis, $cimbDisbursementRepo;

  public function __construct() {
    $this->redis = Redis::connection();
    $this->cimbDisbursementRepo = app()->make(DisbursementCimbDisbursementRepository::class);
  }

  public function exec(PipelineListener $listener, $data) {

    if (!$this->redis->hsetnx('odeo_core:cimb_disbursement_lock', 'id_' . $data['cimb_disbursement_id'], 1)) {
      clog('cimb', 'lock_duplicate');
      return $listener->response(400, 'duplicate transaction');
    }

    $dateTime = new \DateTime();

    $this->cimbDisbursementRepo->lock();

    \DB::beginTransaction();

    list($url, $debitAccountNumber) = ApiManager::getConfig(['url', 'debit_account_number']);

    $fundTransferXml = null;

    try {

      if (!$cimbDisbursement = $this->cimbDisbursementRepo->findById($data['cimb_disbursement_id'])) {
        throw new \Exception('cimbDisbursement not found', 400);
      }

      if ($cimbDisbursement->status_code != null || $cimbDisbursement->status_code != '') {
        throw new \Exception('status code is not empty', 400);
      }

      if ($cimbDisbursement->response_datetime) {
        throw new \Exception("response_datetime is not empty, but got {$cimbDisbursement->response_datetime}", 400);
      }

      if ($cimbDisbursement->amount == 0 && isset($data['amount'])) {
        $cimbDisbursement->amount = $data['amount'];
      }

      $tokenAuth = ApiManager::generateAuthToken(
        $dateTime->format('YmdHis'),
        $data['request_id'],
        ApiManager::SERVICE_TRANSFER_FUND
      );

      $fundTransferXml = ApiManager::generateFundTransferXml([
        'tokenAuth' => $tokenAuth,
        'requestID' => $data['request_id'],
        'txnRequestDateTime' => $dateTime->format('YmdHis'),
        'txnDate' => $dateTime->format('Ymd'),
        'debitAcctNo' => $debitAccountNumber,
        'benAcctNo' => $data['account_number'],
        'benName' => $data['account_name'],
        'amount' => $cimbDisbursement->amount,
        'memo' => $data['memo']
      ]);

      $cimbDisbursement->transfer_from = $debitAccountNumber;
      $cimbDisbursement->transfer_to = $data['account_number'];
      $cimbDisbursement->transaction_date = $dateTime->format('YmdHis');
      $cimbDisbursement->request_id = $data['request_id'];
      $cimbDisbursement->memo =  $data['memo'];

      $this->cimbDisbursementRepo->save($cimbDisbursement);

      \DB::commit();

    } catch (\Exception $e) {
      \Log::error($e);
      \DB::rollback();
      return $listener->response(400);
    }

    $result = null;

    try {
      $client = ApiManager::getClient();

      $client->operation = ApiManager::$SOAP;

      $response = $client->send($fundTransferXml, $url);
      $result = $response['output'];

      $cimbDisbursement->status_code = $result['statusCode'];
      $cimbDisbursement->status_message = $result['statusMsg'];
      $cimbDisbursement->response_datetime = $result['txnResponseDateTime'];
      $cimbDisbursement->bank_reff_no = $result['bankReffNo'];

      if ($result['statusCode'] == '00') {
        $cimbDisbursement->status = CimbDisbursement::STATUS_SUCCESS;
      }
      else {
        $cimbDisbursement->status = CimbDisbursement::STATUS_FAIL;
        $cimbDisbursement->error_log_response = \json_encode($result);
      }
    } catch (\Exception $e) {
      \Log::error($e);

      $cimbDisbursement->status = CimbDisbursement::STATUS_FAIL;
      $cimbDisbursement->error_code = $e->getCode();
      $cimbDisbursement->error_message = $e->getMessage();
      $cimbDisbursement->error_log_response = \json_encode($result);
      $cimbDisbursement->response_datetime = Carbon::now()->toDateTimeString();
    }

    $this->cimbDisbursementRepo->save($cimbDisbursement);

    $this->redis->hdel('odeo_core:cimb_disbursement_lock', 'id_' . $data['cimb_disbursement_id']);

    if ($cimbDisbursement->status != CimbDisbursement::STATUS_SUCCESS) {
      return $listener->response(400, [
        'error_code' => $cimbDisbursement->error_code && strlen($cimbDisbursement->error_code)
          ? $cimbDisbursement->error_code
          : false
      ]);
    }

    return $listener->response(200);
  }
}
