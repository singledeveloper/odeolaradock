<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/29/17
 * Time: 6:12 PM
 */

namespace Odeo\Domains\Vendor\Cimb;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Vendor\Cimb\Repository\DisbursementCimbDisbursementRepository;

class TransferRecordSelector implements SelectorListener {

  private $cimbDisbursementRepo;

  public function __construct() {
    $this->cimbDisbursementRepo = app()->make(DisbursementCimbDisbursementRepository::class);
  }

  public function get(PipelineListener $listener, $data) {

    $this->cimbDisbursementRepo->normalizeFilters($data);

    $cimbDisbursementRecords = [];

    foreach ($this->cimbDisbursementRepo->get() as $cimbDisbursement) {
      $cimbDisbursementRecords[] = $this->_transforms($cimbDisbursement, $this->cimbDisbursementRepo);
    }

    if(count($cimbDisbursementRecords)) {
      return $listener->response(200, [
          'cimb_disbursement_record' => $cimbDisbursementRecords
        ],
        $this->cimbDisbursementRepo->getPagination()
      );
    }

    return $listener->response(204, ['cimb_disbursement_record' => []]);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {

    $response['cimb_disbursement_id'] = $item->id;
    $response['transfer_to'] = $item->transfer_to;
    $response['amount'] = $item->amount;
    $response['status'] = $item->status;
    $response['disbursement_type'] = $item->disbursement_type;
    $response['transaction_key'] = $item->transaction_key;
    $response['cimb_status'] = [
      'code' => $item->status_code,
      'message' => $item->status_message
    ];
    $response['bank_reff_no'] = $item->bank_reff_no;
    $response['memo'] = $item->memo;
    $response['transaction_datetime'] = $item->transaction_datetime;
    $response['response_datetime'] = $item->response_datetime;
    $response['error'] = [
      'code' => $item->error_code,
      'message' => $item->error_message
    ];
    $response['disbursement_reference_id'] = $item->disbursement_reference_id;

    return $response;
  }

  public function getSuspectDisbursements(PipelineListener $listener, $data) {
    if ($vendorDisbursements = $this->cimbDisbursementRepo->getSuspectDisbursements()) {
      return $listener->response(200, ['suspect_disbursement_record' => $vendorDisbursements]);
    }

    return $listener->response(204, ['suspect_disbursement_record' => []]);
  }
}
