<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/6/17
 * Time: 7:32 PM
 */

namespace Odeo\Domains\Vendor\Flip;

use Odeo\Domains\Constant\FlipMutationDisbursement;
use Odeo\Domains\Core\PipelineListener;

class BalanceUpdater {

  private $flipBalanceUpdater;

  public function __construct() {
    $this->flipBalanceUpdater = app()->make(\Odeo\Domains\Vendor\Flip\Helper\FlipBalanceUpdater::class);
  }

  public function updateBalance(PipelineListener $listener, $data) {

    $this->flipBalanceUpdater->updateBalance([
      'amount' => $data['amount'],
      'fee' => 0,
      'type' => FlipMutationDisbursement::TYPE_TOPUP
    ]);

    return $listener->response(200);
  }

}