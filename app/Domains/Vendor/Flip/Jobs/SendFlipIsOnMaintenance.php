<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 5:23 PM
 */

namespace Odeo\Domains\Vendor\Flip\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendFlipIsOnMaintenance extends Job  {

  use SerializesModels;

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    if (app()->environment() != 'production') {
      return;
    }

    Mail::send('emails.flip_is_down_for_maintenance', [
      'data' => [
        'user_id' => $this->data['user_id'],
        'reference_id' => $this->data['reference_id'],
        'reference_type' => $this->data['reference_type'],
        'requested_amount' => $this->currencyHelper->formatPrice($this->data['requested_amount']),
      ]
    ], function ($m) {

      $m->from('noreply@odeo.co.id', 'odeo');

      $m->to('pg@odeo.co.id')->subject('Flip is currently down for maintenance - ' . time());

    });


  }

}
