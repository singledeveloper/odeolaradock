<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/9/17
 * Time: 5:05 PM
 */

namespace Odeo\Domains\Vendor\Flip\Model;


use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\Entity;

class DisbursementFlipMutationDisbursement extends Entity {

  public function disbursementFlipDisbursement() {
    return $this->belongsTo(DisbursementFlipDisbursement::class);
  }

  public function vendorDisbursement() {
    return $this->belongsTo(VendorDisbursement::class);
  }
}