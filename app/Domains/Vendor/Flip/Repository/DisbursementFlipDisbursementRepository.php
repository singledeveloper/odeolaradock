<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/2/17
 * Time: 11:47 PM
 */

namespace Odeo\Domains\Vendor\Flip\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Flip\Model\DisbursementFlipDisbursement;

class DisbursementFlipDisbursementRepository extends Repository {

  private $lock = false;

  public function __construct(DisbursementFlipDisbursement $disbursementFlipDisbursement) {
    $this->model = $disbursementFlipDisbursement;
  }

  public function lock() {
    $this->lock = true;
  }

  public function get() {

    $filters = $this->getFilters();

    $query = $this->getCloneModel();

    if (isset($filters['search'])) {
      if (isset($filters['search']['flip_disbursement_id'])) {
        $query = $query->where('id', $filters['search']['flip_disbursement_id']);
      }
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function findById($id, $fields = ['*']) {
    $model = $this->getCloneModel();
    if ($this->lock) $model = $this->model->lockForUpdate();
    return $model->find($id, $fields);
  }

  public function findByFlipId($id) {
    $model = $this->getCloneModel();
    if ($this->lock) $model = $this->model->lockForUpdate();
    return $model->where('flip_id', $id)->first();
  }

  public function findByReferenceId($referenceType, $referenceId) {
    return $this->model
      ->where('reference_type', $referenceType)
      ->where('reference_id', $referenceId)
      ->get();
  }

}