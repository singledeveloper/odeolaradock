<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/2/17
 * Time: 11:47 PM
 */

namespace Odeo\Domains\Vendor\Flip\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Flip\Model\DisbursementFlipMutationDisbursement;

class DisbursementFlipMutationDisbursementRepository extends Repository {
  
  public function __construct(DisbursementFlipMutationDisbursement $disbursementFlipMutationDisbursement) {
    $this->model = $disbursementFlipMutationDisbursement;
  }

}