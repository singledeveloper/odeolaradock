<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/3/17
 * Time: 10:15 PM
 */

namespace Odeo\Domains\Vendor\Flip\Scheduler;

use Odeo\Domains\Constant\VendorDisbursement;

class CheckBalanceScheduler {

  private $vendorDisbursements, $flipApi, $redis;

  public function __construct() {
    $this->vendorDisbursements = app()->make(\Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository::class);
    $this->flipApi = app()->make(\Odeo\Domains\Vendor\Flip\Helper\FlipApi::class);
    $this->redis = \Illuminate\Support\Facades\Redis::connection();
  }

  public function run() {
    $result = $this->flipApi->getBalance();

    if ($result['status'] != 200) return;

    $vendorDisbursement = $this->vendorDisbursements->findById(VendorDisbursement::FLIP);

    if ($vendorDisbursement->balance != $result['data']['balance']) {

      if (!($countUp = $this->redis->hget('odeo_core:flip', 'balance_imbalance_count_up'))) {
        $countUp = 0;
      }

      if ($countUp == 3) {

        dispatch(new \Odeo\Domains\Vendor\Flip\Jobs\SendFlipImbalanceBalanceAlert([
          'flip_balance' => $result['data']['balance'],
          'recorded_balance' => $vendorDisbursement->balance,
          'datetime' => \Carbon\Carbon::now()->toDateTimeString()
        ]));

      }

      $this->redis->hset('odeo_core:flip', 'balance_imbalance_count_up', $countUp % 3 + 1);

    }

  }

}
