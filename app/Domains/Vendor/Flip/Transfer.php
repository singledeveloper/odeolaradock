<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/11/17
 * Time: 2:59 PM
 */

namespace Odeo\Domains\Vendor\Flip;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\FlipDisbursement;
use Odeo\Domains\Constant\FlipMutationDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Flip\Helper\FlipApi;

class Transfer {

  private $api, $flipDisbursements, $redis;

  public function __construct() {

    $this->redis = Redis::connection();

    $this->api = app()->make(\Odeo\Domains\Vendor\Flip\Helper\FlipApi::class);
    $this->flipDisbursements = app()->make(\Odeo\Domains\Vendor\Flip\Repository\DisbursementFlipDisbursementRepository::class);

  }

  public function exec(PipelineListener $listener, $data) {

    if ($this->redis->hget('odeo_core:flip_disbursement_lock', 'id_' . $data['flip_disbursement_id'])) {
      return $listener->response(400, 'duplicate transaction');
    }

    $this->redis->hset('odeo_core:flip_disbursement_lock', 'id_' . $data['flip_disbursement_id'], 1);

    $flipDisbursement = null;
    $errorCode = null;

    \DB::beginTransaction();

    try {

      $this->flipDisbursements->lock();

      $flipDisbursement = $this->flipDisbursements->findById($data['flip_disbursement_id']);

      if ($flipDisbursement->response_datetime) {
        \DB::rollback();
        return $listener->response(400);
      }

      $flipDisbursement->transfer_datetime = Carbon::now()->toDateTimeString();
      $flipDisbursement->transaction_key = $data['transaction_key'];
      $flipDisbursement->flip_receiver_bank = $data['account_bank'];
      $flipDisbursement->flip_receiver_account_number = $data['account_number'];
      $flipDisbursement->flip_note = $data['note'];

      $this->flipDisbursements->save($flipDisbursement);

    } catch (\Exception $exception) {
      \DB::rollback();
      clog('flip_error', $exception->getMessage());
      clog('flip_error', $exception->getTraceAsString());
      return $listener->response(400);

    }

    $response = null;

    try {

      $response = $this->api->post(FlipApi::$CREATE_DISBURSEMENT_URL, [
        'no_rek_tujuan' => $data['account_number'],
        'bank_penerima' => $data['account_bank'],
        'nominal' => (int)$flipDisbursement->amount_to_be_transfer,
        'berita' => $data['note'],
        'kota' => 393,
      ]);

      $flipDisbursement->response_datetime = Carbon::now()->toDateTimeString();

      $result = $response['data'];

      if (isset($result['code']) && $result['code'] == 'VALIDATION_ERROR') $errorCode = $result['code'];

      else if ($response['status'] == 401) $errorCode = 'FLIP_ON_MAINTENANCE';

      if ($response['status'] != 200) throw new \Exception(\json_encode($response));

      $flipDisbursement->flip_id = $result['id'];
      $flipDisbursement->flip_user_id = $result['id_user'];
      $flipDisbursement->flip_amount = $result['nominal'];
      $flipDisbursement->flip_status = $result['status'];
      $flipDisbursement->flip_timestamp = $result['timestamp'];
      $flipDisbursement->flip_receiver_bank = $result['bank_penerima'];
      $flipDisbursement->flip_receiver_account_number = $result['no_rek_tujuan'];
      $flipDisbursement->flip_receiver_name = $result['nama_tujuan'];
      $flipDisbursement->flip_sender_bank = $result['bank_pengirim'];
      $flipDisbursement->flip_note = $result['berita'];
      $flipDisbursement->flip_evidence = $result['bukti'];
      $flipDisbursement->flip_time_served = $result['time_served'];
      $flipDisbursement->flip_bundle_id = $result['id_bundle'];
      $flipDisbursement->flip_company_id = $result['id_perusahaan'];
      $flipDisbursement->flip_city = $result['kota'];
      $flipDisbursement->status = FlipDisbursement::WAIT_FOR_NOTIFY;
      $flipDisbursement->response = null;
      $flipDisbursement->error_exception_message = null;
      $flipDisbursement->error_exception_code = null;

    } catch (\Exception $exception) {

      $flipDisbursement->response = json_encode($response);
      $flipDisbursement->status = FlipDisbursement::FAIL;
      $flipDisbursement->error_exception_message = $exception->getMessage();
      $flipDisbursement->error_exception_code = $exception->getCode();

    }

    $this->flipDisbursements->save($flipDisbursement);

    \DB::commit();

    $this->redis->hdel('odeo_core:flip_disbursement_lock', 'id_' . $data['flip_disbursement_id']);

    if ($flipDisbursement->status != FlipDisbursement::WAIT_FOR_NOTIFY) {
      return $listener->response(400, [
        'error_code' => $errorCode ? $errorCode : '0'
      ]);
    }

    app()->make(\Odeo\Domains\Vendor\Flip\Helper\FlipBalanceUpdater::class)
      ->updateBalance([
        'amount' => -$flipDisbursement->amount_to_be_transfer,
        'fee' => -$data['fee'],
        'type' => FlipMutationDisbursement::TYPE_TRANSFER,
        'disbursement_flip_disbursement_id' => $flipDisbursement->id,
      ]);

    return $listener->response(200);

  }

}