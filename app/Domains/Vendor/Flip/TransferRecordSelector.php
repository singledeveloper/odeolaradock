<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/29/17
 * Time: 9:26 PM
 */

namespace Odeo\Domains\Vendor\Flip;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class TransferRecordSelector implements SelectorListener {

  private $flipDisbursements;

  public function __construct() {
    $this->flipDisbursements = app()->make(\Odeo\Domains\Vendor\Flip\Repository\DisbursementFlipDisbursementRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {

    $response['flip_disbursement_id'] = $item->id;
    $response['transfer_from'] = [
      'bank' => $item->flip_sender_bank
    ];
    $response['transfer_to'] = [
      'account_number' => $item->flip_receiver_account_number,
      'bank' => $item->flip_receiver_bank
    ];
    $response['amount'] = $item->amount_to_be_transfer;
    $response['remark'] = $item->flip_note;
    $response['transfer_datetime'] = $item->transfer_datetime;
    $response['response_datetime'] = $item->response_datetime;
    $response['notify_datetime'] = $item->notify_datetime;
    $response['disbursement_type'] = $item->disbursement_type;
    $response['disbursement_reference_id'] = $item->disbursement_reference_id;
    $response['status'] = $item->status;
    $response['flip_status'] = $item->bca_status;
    $response['error_code'] = $item->error_code;
    $response['error_reason'] = $item->error_reason;
    $response['error_exception_message'] = $item->error_exception_message;
    $response['error_exception_code'] = $item->error_exception_code;

    return $response;
  }

  public function get(PipelineListener $listener, $data) {

    $this->flipDisbursements->normalizeFilters($data);
    $this->flipDisbursements->setSimplePaginate(true);

    $flipDisbursementRecords = [];

    foreach ($this->flipDisbursements->get() as $bcaDisbursement) {
      $flipDisbursementRecords[] = $this->_transforms($bcaDisbursement, $this->flipDisbursements);
    }

    if (sizeof($flipDisbursementRecords) > 0) {
      return $listener->response(200, array_merge(
        ["flip_disbursement_record" => $this->_extends($flipDisbursementRecords, $this->flipDisbursements)],
        $this->flipDisbursements->getPagination()
      ));
    }
    return $listener->response(204, ["flip_disbursement_record" => []]);

  }

}
