<?php

namespace Odeo\Domains\Vendor\Jabber\Commands;

use Illuminate\Console\Command;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Vendor\Jabber\Connector;

class StartConnection extends Command
{

  protected $signature = 'jabber:restart';

  protected $description = 'Start Jabber Bot';

  public function __construct()
  {
    parent::__construct();
  }

  public function handle()
  {
    (new Connector())->run();
  }
}
