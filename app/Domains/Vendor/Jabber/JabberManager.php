<?php

namespace Odeo\Domains\Vendor\Jabber;

use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Constant\UserStatus;

class JabberManager {

  private $pin = '', $command = '';
  protected $jabberUser, $jabberHistories, $jabberStores, $channelLogs;

  public function __construct() {

    app('translator')->setLocale('id');

    $this->jabberUser = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberUserRepository::class);
    $this->jabberHistories = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberHistoryRepository::class);
    $this->jabberStores = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberStoreRepository::class);
    $this->channelLogs = app()->make(\Odeo\Domains\Affiliate\Repository\ChannelLogRepository::class);
  }

  public function setClient($pid, $email, $pass, $dir = '') {
    $params = [
      'pid' => $pid,
      'jid' => $email,
      'pass' => $pass,
      'strict' => false,
      'resource' => 'odeo',
      'multi_client' => true
    ];
    if ($dir != '') $params['priv_dir'] = $dir;
    $params['log_level'] = 5;
    return new \JAXL($params);
  }

  protected function format($data) {
    $temp = explode('.', $data);
    $command = strtolower($temp[0]);

    $data = Inline::COMMAND_FORMATS;

    if (isset($data[$command]) && count($temp) != $data[$command]) return false;

    $this->command = strtolower($temp[0]);

    $this->pin = $temp[count($temp) - 1];

    if (!isset($data[$command]) && strtolower($this->command) == strtolower($this->pin)) return false;

    return $temp;
  }

  public function reply($socketPath, $function, $args = []) {

    if (isset($args['message'])) {
      if (strlen($args['message']) > 500) {
        foreach (str_split($args['message'], 500) as $item) {
          $args['message'] = $item;
          $this->reply($socketPath, $function, $args);
        }
        return '';
      }
    }

    $errMessage = '';
    try {
      if (!$fp = stream_socket_client("unix://" . $socketPath, $errno, $errstr)) {
        $errMessage = "ERROR: $errno - $errstr";
        clog('jabber_socket', $errMessage);
      }
      else {
        foreach($args as &$item) $item = '"' . str_replace('"', '\"', htmlspecialchars(strip_tags($item))) . '"';
        $write = '$this->' . $function . '(' . implode(', ', $args) . ');';
        fwrite($fp, $write);
        fread($fp,  4096);
        fclose($fp);
      }
    }
    catch(\Exception $e) {
      $executor = posix_getpwuid(posix_geteuid())['name'];
      $errMessage = $e->getMessage();
      $hostname = $_ENV['HOSTNAME'] ?? 'UNDEFINED';
      clog('jabber_socket', "$errMessage error in reply with user $executor when execute $function on host $hostname");
    }

    return $errMessage;
  }

  protected function getCommand() {
    return $this->command;
  }

  protected function auth($jabberUser) {
    if (Inline::commandNoAuth($this->command)) return [true, null];

    $user = $jabberUser->user;
    if ($jabberUser->pin_try_counts == UserStatus::LOGIN_BLOCKED_COUNT ||
      $user->status != UserStatus::OK || $user->login_counts == UserStatus::LOGIN_BLOCKED_COUNT)
      return [false, trans('errors.account_blocked')];
    else if (!Hash::check($this->pin, $jabberUser->pin)) {
      $jabberUser->pin_try_counts = $jabberUser->pin_try_counts + 1;
      $this->jabberUser->save($jabberUser);
      if ($jabberUser->pin_try_counts == UserStatus::LOGIN_BLOCKED_COUNT)
        return [false, trans('errors.account_blocked')];
      return [false, trans('errors.invalid_password', ["count" => 3 - $jabberUser->pin_try_counts])];
    }
    else {
      if ($jabberUser->pin_try_counts != 0) {
        $jabberUser->pin_try_counts = 0;
        $this->jabberUser->save($jabberUser);
      }
      return [true, $jabberUser->id];
    }
  }

  public function createLog($from, $type, $description) {
    if (strpos($description, ' ping ') !== false) return false;

    if ($channelLog = $this->channelLogs->findSame($from, $type, $description)) {
      $channelLog->duplication_counts = $channelLog->duplication_counts + 1;
      $return = false;
    }
    else {
      $channelLog = $this->channelLogs->getNew();
      $channelLog->type = $type;
      $channelLog->from = $from;
      $channelLog->description = $description;
      $return = true;
    }
    $this->channelLogs->save($channelLog);
    return $return;
  }

}
