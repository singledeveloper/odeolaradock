<?php

namespace Odeo\Domains\Vendor\Jabber\Jobs;

use Odeo\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;

class AutoCommandRestarter extends Job {

  use InteractsWithQueue, SerializesModels;

  /**
   * Create a new job instance.
   *
   * @return void
   */

  public function __construct() {
    parent::__construct();
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle() {
    exec("nohup php /var/www/odeo-core/artisan jabber:restart > /dev/null 2>&1 &");
  }

}
