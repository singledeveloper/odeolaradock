<?php

namespace Odeo\Domains\Vendor\Jabber\Jobs;

use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\ServerConfig;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;

class BridgeNotify extends Job {
  use InteractsWithQueue, SerializesModels;

  protected $from, $message;

  public function __construct($from, $message) {
    parent::__construct();
    $this->runOnlyOnServer('jabber');
    $this->from = $from;
    $this->message = $message;
  }

  public function handle() {
    $pipeline = new Pipeline();
    $pipeline->add(new Task(InlineJabber::getNotifyPath($this->from), 'notify'));
    $pipeline->execute([
      'jabber_message' => $this->message,
      'jabber_from' => $this->from
    ]);
  }

}
