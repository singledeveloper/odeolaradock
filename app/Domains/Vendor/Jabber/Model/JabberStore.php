<?php

namespace Odeo\Domains\Vendor\Jabber\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Store;

class JabberStore extends Entity
{
  public function store() {
    return $this->belongsTo(Store::class);
  }
}
