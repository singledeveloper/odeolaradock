<?php

namespace Odeo\Domains\Vendor\Jabber\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class JabberUser extends Entity
{
  public function user() {
    return $this->belongsTo(User::class);
  }

  public function jabberStore() {
    return $this->belongsTo(JabberStore::class);
  }
}
