<?php

namespace Odeo\Domains\Vendor\Jabber\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Jabber\Model\JabberHistory;

class JabberHistoryRepository extends Repository {

  public function __construct(JabberHistory $jabberHistory) {
    $this->model = $jabberHistory;
  }

  public function findSame($updateId) {
    return $this->model->where('update_id', $updateId)->first();
  }

  public function findSameMessage($updateId, $message) {
    return $this->model->where('message', $message)->where('update_id', $updateId)
      ->where('created_at', '>', Carbon::now()->subHours(12)->toDateTimeString())->first();
  }

}
