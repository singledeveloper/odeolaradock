<?php

namespace Odeo\Domains\Vendor\Jabber\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Jabber\Model\JabberOrder;

class JabberOrderRepository extends Repository {

  public function __construct(JabberOrder $jabberOrder) {
    $this->model = $jabberOrder;
  }

  public function findByOrderId($orderId) {
    return $this->model->where('order_id', $orderId)->first();
  }

  public function findByTrxId($trxId, $userId) {
    return $this->model->where('trx_id', $trxId)->where('jabber_user_id', $userId)->first();
  }

}
