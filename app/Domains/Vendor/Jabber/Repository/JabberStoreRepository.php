<?php

namespace Odeo\Domains\Vendor\Jabber\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Jabber\Model\JabberStore;

class JabberStoreRepository extends Repository {

  public function __construct(JabberStore $jabberStore) {
    $this->model = $jabberStore;
  }

  public function findByEmail($email) {
    return $this->model->where('email', $email)->first();
  }

  public function getAllActive() {
    return $this->model->where('is_active', true)->get();
  }

  public function getAllActiveStore() {
    return $this->model->where('is_active', true)
      ->whereNotNull('store_id')->get();
  }

  public function findByStoreId($storeId) {
    return $this->model->where('store_id', $storeId)->first();
  }

}
