<?php

namespace Odeo\Domains\Vendor\Jabber\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Jabber\Model\JabberUser;

class JabberUserRepository extends Repository {

  public function __construct(JabberUser $jabberUser) {
    $this->model = $jabberUser;
  }

  public function findByEmail($email, $ownerStoreId = null) {
    $query = $this->model->where('email', $email)->where('is_active', true);

    if ($ownerStoreId == null) $query = $query->whereNull('owner_store_id');
    else $query = $query->where('owner_store_id', $ownerStoreId);

    return $query->first();
  }

  public function findByUserId($userId) {
    return $this->model->where('user_id', $userId)->first();
  }

  public function getByUserId($userIds) {
    return $this->model->whereIn('user_id', $userIds)->get();
  }

}
