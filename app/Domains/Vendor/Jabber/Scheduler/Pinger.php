<?php

namespace Odeo\Domains\Vendor\Jabber\Scheduler;

use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Vendor\Jabber\Jobs\ReplySocket;

class Pinger {

  public function run() {

    dispatch(new ReplySocket(InlineJabber::JAXL_CHAT, [
      'to' => 'odeo@jabber.id',
      'message' => 'ping ' . time()
    ]));

    $jabberStores = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberStoreRepository::class);
    foreach ($jabberStores->getAllActiveStore() as $item) {
      dispatch(new ReplySocket(InlineJabber::JAXL_CHAT, [
        'to' => $item->email,
        'message' => 'ping ' . time()
      ]));
    }

  }

}