<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 14/03/19
 * Time: 13.12
 */

namespace Odeo\Domains\Vendor\Mandiri\Helper;


use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class ApiManager {

  static $serviceTransactionStatus = '';
  static $servicePaymentTransfer = '';
  static $serviceAccountInquiryName = '';
  static $serviceBalanceInquiry = '';
  static $odeoAccountNo = '';
  static $clientId = '';
  static $certificatePassword = '';
  static $privateKeyPath = '';
  static $certificateFilePath = '';
  static $loginUsername = '';
  static $loginPassword = '';

  const DO_PAYMENT = 'doPayment';
  const DO_BALANCE = 'doBalanceInquiry';
  const DO_ACC_INQUIRY = 'doAccountInquiryName';
  const DO_TRX_STATUS = 'doTransactionStatusInquiry';

  public static function init() {
   self::$serviceTransactionStatus = env('API_MANDIRI_SERVICE_TRANSACTION_STATUS');
   self::$servicePaymentTransfer = env('API_MANDIRI_SERVICE_PAYMENT_TRANSFER');
   self::$serviceAccountInquiryName = env('API_MANDIRI_SERVICE_ACCOUNT_INQUIRY_NAME');
   self::$serviceBalanceInquiry = env('API_MANDIRI_SERVICE_BALANCE_INQUIRY');
   self::$odeoAccountNo = env('API_MANDIRI_ODEO_ACCOUNT_NO');
   self::$clientId = env('API_MANDIRI_CLIENT_ID');
   self::$certificatePassword = env('API_MANDIRI_CERTIFICATE_PASSWORD');
   self::$loginUsername = env('API_MANDIRI_LOGIN_USERNAME');
   self::$loginPassword = env('API_MANDIRI_LOGIN_PASSWORD');
   self::$privateKeyPath = 'mandirih2h/private-key.pem';
   self::$certificateFilePath = 'mandirih2h/certificate.cer';
  }

  public static function getClient($server) {
    $options = [
      'cache' => WSDL_CACHE_NONE,
      'login' => self::$loginUsername,
      'password' => self::$loginPassword,
      'location' => $server,
      'trace' => true
    ];
    return new \SoapClient($server . '?WSDL', $options);
  }

  public function verifySign($message, $sign) {
    // $key = openssl_pkey_get_public(Storage::disk('local')->get(self::$certificateFilePath));
    // $isVerified = openssl_verify($message, base64_decode($sign), $key, 'sha1WithRSAEncryption');
    return 1;
  }

  public function testPaymentTransfer($data) {
    $currency = $data['currency'];
    $externalId = time();
    $time = Carbon::now();
    $requestHeader = self::createRequestHeader($externalId, $time);
    $signatureString = self::generateSign($data['debitAcc'] . $data['creditAcc'] . $currency . $data['amount'] . $externalId) ;
    $securityHeader = self::createSecurityHeader($signatureString);
    $requestBody = self::createRequest([
      'debitAccountNo' => $data['debitAcc'],
      'creditAccountNo' => $data['creditAcc'],
      'valueDate' => $time->toDateString(),
      'valueCurrency' => $currency,
      'valueAmount' => $data['amount'],
      'preferredTransferMethodId' => $data['trfMethodId'],
      'destinationBankCode' => $data['bankCode'],
      'beneficiaryName' => $data['benefName'],
      'beneficiaryEmailAddress' => 'febrianjdev@gmail.com',
      'chargingModelId' => $data['chargeModelId'],
      'remark1' => $data['remark1'],
      'customerReferenceNumber' => $data['reffNo'],
      'reservedField1' => $data['reserve1'] ?? '',
      'reservedField2' => $data['reserve2'] ?? ''
    ]);

    $paymentRequest = new \stdClass;
    $paymentRequest->requestHeader = $requestHeader;
    $paymentRequest->securityHeader = $securityHeader;
    $paymentRequest->body = $requestBody;

    $request = new \stdClass;
    $request->PaymentRequest = $this->wrapRequest($paymentRequest);
    $result = $this->execute(self::$servicePaymentTransfer, self::DO_PAYMENT, $request);

    $mandiriMessage = self::$odeoAccountNo . $data['creditAcc'] . $currency . $data['amount'] . $result['responseHeader']['internalId'];

    return [$result, $this->verifySign($mandiriMessage, $result['securityHeader']['signature'])];
  }

  public function createPaymentTransfer($creditAcc, $amount, $remark, $reffNo, $benefName = '', $bankCode = '') {
    $currency = 'IDR';

    list($usec, $sec) = explode(" ", microtime());
    $externalId = $sec . ($usec * 1000000);
    
    $time = Carbon::now();
    $requestHeader = self::createRequestHeader($externalId, $time);
    $signatureString = self::generateSign(self::$odeoAccountNo . $creditAcc . $currency . $amount . $externalId);
    $securityHeader = self::createSecurityHeader($signatureString);
    $requestBody = self::createRequest([
      'debitAccountNo' => self::$odeoAccountNo,
      'creditAccountNo' => $creditAcc,
      'valueDate' => $time->toDateString(),
      'valueCurrency' => $currency,
      'valueAmount' => $amount,
      'preferredTransferMethodId' => $bankCode ? 2 : 0, // 1 rtgs 2 skn 3 tt
      'destinationBankCode' => $bankCode,
      'beneficiaryName' => $bankCode ? $benefName : '',
      'chargingModelId' => 'OUR',
      'remark1' => $remark,
      'customerReferenceNumber' => $reffNo,
      'extendedPaymentDetail' => $remark
    ]);

    $paymentRequest = new \stdClass;
    $paymentRequest->requestHeader = $requestHeader;
    $paymentRequest->securityHeader = $securityHeader;
    $paymentRequest->body = $requestBody;

    $request = new \stdClass;
    $request->PaymentRequest = $this->wrapRequest($paymentRequest);
    $result = $this->execute(self::$servicePaymentTransfer, self::DO_PAYMENT, $request);

    $mandiriMessage = self::$odeoAccountNo . $creditAcc . $currency . $amount . $result['responseHeader']['internalId'];

    clog('mandiri_api', json_encode($result));

    return [$result, $this->verifySign($mandiriMessage, $result['securityHeader']['signature'])];
  }

  public function getTransactionStatus($reffNo) {
    list($usec, $sec) = explode(" ", microtime());
    $externalId = $sec . ($usec * 1000000);

    $time = Carbon::now();
    $requestHeader = self::createRequestHeader($externalId, $time);
    $signatureString = self::generateSign($externalId . $reffNo);
    $securityHeader = self::createSecurityHeader($signatureString);
    $requestBody = self::createRequest([
      'originalCustomerReferenceNumber' => $reffNo
    ]);

    $trxStatusRequest = new \stdClass;
    $trxStatusRequest->requestHeader = $requestHeader;
    $trxStatusRequest->securityHeader = $securityHeader;
    $trxStatusRequest->body = $requestBody;

    $request = new \stdClass;
    $request->TransactionStatusInquiryRequest = $this->wrapRequest($trxStatusRequest);

    $result = $this->execute(self::$serviceTransactionStatus, self::DO_TRX_STATUS, $request);

    $mandiriMessage = $result['responseHeader']['internalId'] . $reffNo;

    return [$result, $this->verifySign($mandiriMessage, $result['securityHeader']['signature'])];
  }

  public function getAccountInquiry($accountNo) {
    $requestHeader = self::createRequestHeader(time(), Carbon::now());
    $requestBody = self::createRequest([
      'accountNumber' => $accountNo
    ]);

    $accInquiryRequest = new \stdClass;
    $accInquiryRequest->requestHeader = $requestHeader;
    $accInquiryRequest->body = $requestBody;

    $request = new \stdClass;
    $request->AccountInquiryNameRequest = $this->wrapRequest($accInquiryRequest);

    $result = $this->execute(self::$serviceAccountInquiryName, self::DO_ACC_INQUIRY, $request);

    return $result;
  }

  public function getBalanceInquiry() {
    list($usec, $sec) = explode(" ", microtime());
    $externalId = $sec . ($usec * 1000000);

    $time = Carbon::now();
    $requestHeader = self::createRequestHeader($externalId, $time);
    $requestBody = self::createRequest([
      'accountNumber' => self::$odeoAccountNo
    ]);

    $balInquiryRequest = new \stdClass;
    $balInquiryRequest->requestHeader = $requestHeader;
    $balInquiryRequest->body = $requestBody;

    $request = new \stdClass;
    $request->BalanceInquiryRequest = $this->wrapRequest($balInquiryRequest);

    $result = $this->execute(self::$serviceBalanceInquiry, self::DO_BALANCE, $request);
    return $result;
  }

  private function execute($server, $callable, $request) {
    $client = self::getClient($server);
    $result = json_decode(json_encode($client->$callable($request)), true);
    return reset($result);
  }

  private function createRequestHeader($externalId, Carbon $time) {
    return self::createRequest([
      'clientId' => self::$clientId,
      'externalId' => $externalId,
      'channelId' => '1',
      'timestamp' => $time->toIso8601ZuluString()
    ]);
  }

  private function createSecurityHeader($signature) {
    return self::createRequest([
      'signature' => $signature
    ]);
  }

  private function createRequest($args=[]) {
    $body = new \stdClass;
    foreach ($args as $key => $val) {
      $body->$key = $val;
    }
    return $body;
  }

  private function wrapRequest($request) {
    return new \SoapVar(
      $request,
      SOAP_ENC_OBJECT,
      null,
      null,
      null,
      "urn:mandiri:autopay:types:v090212"
    );
  }

  public function generateSign($string) {
    $key = openssl_pkey_get_private(Storage::disk('local')->get(self::$privateKeyPath), 'password');
    openssl_sign($string, $sign, $key, 'sha1WithRSAEncryption');
    return base64_encode($sign);
  }

}