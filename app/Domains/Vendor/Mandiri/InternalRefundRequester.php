<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 19/07/19
 * Time: 14.03
 */

namespace Odeo\Domains\Vendor\Mandiri;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\InternalRefund;
use Odeo\Domains\Constant\MandiriDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Odeo\Domains\Vendor\Mandiri\Repository\DisbursementMandiriDisbursementRepository;

class InternalRefundRequester {

  private $bankScrapeAccountNumberRepo, $redis, $bankRepo,
    $mandiriDisbursementRepo;

  private function init() {
    $this->bankRepo = app()->make(BankRepository::class);
    $this->mandiriDisbursementRepo = app()->make(DisbursementMandiriDisbursementRepository::class);
    $this->bankScrapeAccountNumberRepo = app()->make(BankScrapeAccountNumberRepository::class);
    $this->redis = Redis::connection();
  }

  public function request(PipelineListener $listener, $data) {
    $this->init();

    $bankScrapeAccountNo = $this->bankScrapeAccountNumberRepo->findById($data['account_number_id']);

    if (!$bankScrapeAccountNo || !$bankScrapeAccountNo->number) {
      return $listener->response(400, 'Unknown bank account');
    }

    if (!$bankId = InternalRefund::getBankId($bankScrapeAccountNo)) {
      return $listener->response(400, 'Unknown bank account');
    }

    if ($data['password'] != env('INTERNAL_REFUND_REQUEST_PASSWORD')) {
      return $listener->response(400, 'Invalid password');
    }

    if ($this->isDuplicate($bankId, 5)) {
      return $listener->response(400, 'Dalam waktu 5 menit terakhir, sudah ada yang transfer ke bank ini');
    }

    if (!$this->lock($bankId)) {
      return $listener->response(400, 'Sedang ada transfer yang terjadi');
    }

    $key = 'odeo_core:internal_refund_id';
    $internalRefundId = $this->redis->incr($key);
    $bank = $this->bankRepo->findById($bankId);

    $mandiri = $this->mandiriDisbursementRepo->getNew();
    $mandiri->disbursement_type = Disbursement::INTERNAL_REFUND;
    $mandiri->disbursement_reference_id = 0;
    $mandiri->status = MandiriDisbursement::PENDING;
    $mandiri->amount = $data['amount'];
    $this->mandiriDisbursementRepo->save($mandiri);

    $pipeline = new Pipeline();

    $pipeline->add(new Task(Transfer::class, 'exec', [
      'amount' => $mandiri->amount,
      'mandiri_disbursement_id' => $mandiri->id,
      'account_number' => $bankScrapeAccountNo->number,
      'remark' => 'Internal Refund ' . $internalRefundId,
      'account_name' => InternalRefund::INTERNAL_REFUND_USER_NAME,
      'transaction_key' => 'INTERNALREFUND#' . $internalRefundId,
      'bank_code' => $bankId != Bank::MANDIRI ? $bank->swift_code : ''
    ]));

    $pipeline->execute();

    if ($pipeline->success()) {
      $this->redis->hset('odeo_core:internal_refund', $bankId, time());
      $this->unlock($bankId);
      return $listener->response(200);
    } else {
      $responseCode = $pipeline->errorMessage['response_code'] ?? '';
      $this->unlock($bankId);

      switch ($responseCode) {
        case '2':
          return $listener->response(400, "Internal refund #$internalRefundId failed");
        case '3':
        case '8':
        default:
          return $listener->response(400, 'Transaksi suspect. Silakan cek transaksi ini terlebih dahulu');
      }
    }
  }

  private function lock($bankId) {
    return $this->redis->hsetnx('odeo_core:internal_refund_lock', $bankId, 1);
  }

  private function unlock($bankId) {
    $this->redis->hdel('odeo_core:internal_refund_lock', $bankId);
  }

  private function isDuplicate($bankId, $minutes = 10) {
    $lastTime = $this->redis->hget('odeo_core:internal_refund', $bankId);

    if ($lastTime == null) {
      return false;
    }

    return time() - $lastTime < $minutes * 60;
  }

}