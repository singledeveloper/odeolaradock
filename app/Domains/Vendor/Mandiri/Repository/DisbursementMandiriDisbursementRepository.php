<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 25/03/19
 * Time: 17.33
 */

namespace Odeo\Domains\Vendor\Mandiri\Repository;


use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\MandiriDisbursement;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Mandiri\Model\DisbursementMandiriDisbursement;

class DisbursementMandiriDisbursementRepository extends Repository {

  private $lock = false;

  function __construct(DisbursementMandiriDisbursement $mandiriDisbursement) {
    $this->model = $mandiriDisbursement;
  }

  public function lock() {
    $this->lock = true;
  }

  public function get() {
    $filters = $this->getFilters();
    $q = $this->getCloneModel();

    if (isset($filters['search']) && isset($filters['search']['mandiri_disbursement_id'])) {
      $q = $q->where('id', $filters['search']['mandiri_disbursement_id']);
    }

    return $this->getResult($q->orderBy('id', 'desc'));
  }

  public function getDisbursementIdByCustRefs($reffNums, $type = Disbursement::API_DISBURSEMENT) {
    return $this->model->whereIn('cust_reff_no', $reffNums)
      ->where('disbursement_type', '=', $type)
      ->where(function ($query) {
        $query->whereNull('response_error_message')
          ->orWhere('response_error_message', '=', '');
      })
      ->select('id', 'disbursement_reference_id', 'cust_reff_no')
      ->get();
  }

  public function getUnreconciledWithdraws() {
    return $this->model
      ->where('disbursement_type', '=', Disbursement::DISBURSEMENT_FOR_WITHDRAW_OCASH)
      ->where('status', MandiriDisbursement::SUCCESS)
      ->where(function ($query) {
        $query->where('is_reconciled', '=', false)
          ->orWhereNull('is_reconciled');
      })
      ->get();
  }

  public function getSuspectDisbursements() {
    return $this->model
      ->join('api_disbursements', 'disbursement_mandiri_disbursements.disbursement_reference_id', '=', 'api_disbursements.id')
      ->where('api_disbursements.status', ApiDisbursement::SUSPECT)
      ->select('disbursement_mandiri_disbursements.*')
      ->get();
  }
}