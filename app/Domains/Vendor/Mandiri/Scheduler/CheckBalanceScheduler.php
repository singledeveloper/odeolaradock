<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 25/03/19
 * Time: 17.34
 */

namespace Odeo\Domains\Vendor\Mandiri\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository;
use Odeo\Domains\Vendor\Mandiri\Helper\ApiManager;

class CheckBalanceScheduler {

  /**
   * @var VendorDisbursementRepository
   */
  private $vendorDisbursementRepo;

  /**
   * @var ApiManager
   */
  private $apiManager;

  private function init() {
    $this->vendorDisbursementRepo = app()->make(VendorDisbursementRepository::class);
    $this->apiManager = app()->make(ApiManager::class);
  }

  public function run() {
    $this->init();
    $vendor = $this->vendorDisbursementRepo->findById(VendorDisbursement::MANDIRI);

    $result = $this->apiManager->getBalanceInquiry();
    if (isset($result['body']['availableBalance'])) {
      $vendor->balance = $result['body']['availableBalance'];
      $this->vendorDisbursementRepo->save($vendor);
    } else {
      clog('mandiri_api', json_encode($result));
    }
    return $vendor->balance;
  }

}