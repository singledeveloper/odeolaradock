<?php

namespace Odeo\Domains\Vendor\OdeoV2\Helper;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class ApiManager
{

  static $BASE_URL_MAP = [
    'production' => 'https://api.v2.odeo.co.id',
    'staging' => 'https://api.v2.staging.odeo.co.id',
    'local' => 'http://api.v2.odeo.test',
  ];
  static $BASE_URL;
  static $CLIENT_ID;
  static $CLIENT_SECRET;
  static $SIGNING_KEY;

  /**
   * @var Client
   */
  private $client;
  private $redis;

  const SIGNATURE_HEADER = 'X-Odeo-Signature';
  const TIMESTAMP_HEADER = 'X-Odeo-Timestamp';

  public function __construct()
  {
    $this->redis = Redis::connection();
    $this->client = new Client();
  }

  public static function init()
  {
    self::$BASE_URL = env('ODEO_V2_BASE_URL');
    self::$CLIENT_ID = env('ODEO_V2_CLIENT_ID');
    self::$CLIENT_SECRET = env('ODEO_V2_CLIENT_SECRET');
    self::$SIGNING_KEY = env('ODEO_V2_SIGNING_KEY');

    if (self::$BASE_URL == '' && isset(self::$BASE_URL_MAP[app()->environment()])) {
      self::$BASE_URL = self::$BASE_URL_MAP[app()->environment()];
    }
  }

  private function getAccessToken()
  {
    $namespace = 'odeo_core:odeo_v2_client_credential';

    if ($accessToken = $this->redis->hget($namespace, 'access_token')) {
      return $accessToken;
    }

    $response = $this->client->request('POST', self::$BASE_URL . '/oauth2/token', [
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
      ],
      'form_params' => [
        'grant_type' => 'client_credentials',
        'client_id' => self::$CLIENT_ID,
        'client_secret' => self::$CLIENT_SECRET,
      ],
    ]);

    if ($response->getStatusCode() != 200) {
      throw new \Exception("get access token failed, status code should be 200, but got {$response->getStatusCode()} instead");
    }

    $result = json_decode($response->getBody()->getContents());
    $this->redis->hset($namespace, 'access_token', $result->access_token);
    $this->redis->hset($namespace, 'token_type', $result->token_type);
    $this->redis->hset($namespace, 'scope', $result->scope);
    $this->redis->expireat($namespace, time() + $result->expires_in - 10);

    return $result->access_token;
  }

  public function getSignature($signingKey, $method, $path, $queryString, $accessToken, $timestamp, $body)
  {
    if (!starts_with($path, '/')) {
      $path = '/' . $path;
    }
    $bodyHash = base64_encode(hash('sha256', empty($body) ? '' : $body, true));
    $stringToSign = implode(':', [$method, $path, $queryString, $accessToken, $timestamp, $bodyHash]);
    return base64_encode(hash_hmac('sha256', $stringToSign, $signingKey, true));
  }

  public function request($method, $path, $data = [])
  {
    $token = $this->getAccessToken();
    $timestamp = time();

    if ($method == 'GET') {
      $queryString = http_build_query($data);
      $url = $path . '?' . $queryString;
      $body = '';
    } else {
      $queryString = '';
      $url = $path;
      $body = json_encode($data);
    }

    $signature = $this->getSignature(self::$SIGNING_KEY, $method, $path, $queryString, $token, $timestamp, $body);
    $response = $this->client->request($method, self::$BASE_URL . $url, [
      'headers' => [
        'Authorization' => 'Bearer ' . $token,
        'Content-Type' => 'application/json',
        'X-Odeo-Timestamp' => $timestamp,
        'X-Odeo-Signature' => $signature,
        'Accept-Language' => 'en',
      ],
      'timeout' => 100,
      'http_errors' => false,
      'body' => $body,
    ]);

    return [
      'status_code' => $response->getStatusCode(),
      'data' => json_decode($response->getBody()->getContents(), true),
    ];
  }

}
