<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/12/18
 * Time: 16.22
 */

namespace Odeo\Domains\Vendor\Permata;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\PermataDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Permata\Helper\ApiManager;
use Odeo\Domains\Vendor\Permata\Repository\DisbursementPermataDisbursementRepository;

class Transfer {

  /**
   * @var Redis
   */
  private $redis;

  /**
   * @var DisbursementPermataDisbursementRepository
   */
  private $permataDisbursementRepo;

  /**
   * @var ApiManager
   */
  private $apiManager;

  private function initialize() {
    $this->redis = Redis::connection();
    $this->permataDisbursementRepo = app()->make(DisbursementPermataDisbursementRepository::class);
    $this->apiManager = app()->make(ApiManager::class);
  }

  public function exec(PipelineListener $listener, $data) {
    $this->initialize();

    if (!$this->redis->hsetnx('odeo_core:permata_disbursement_lock', 'id_' . $data['permata_disbursement_id'], 1)) {
      return $listener->response(400, 'duplicate transaction');
    }

    \DB::beginTransaction();

    try {
      $this->permataDisbursementRepo->lock();
      $disbursement = $this->permataDisbursementRepo->findById($data['permata_disbursement_id']);
      if ($disbursement->status_code != null || $disbursement->status_code != '') {
        throw new \Exception('duplicate transaction');
      }

      if ($disbursement->response_datetime) {
        throw new \Exception('duplicate transaction');
      }

      if (strlen($data['desc']) > 20) {
        list($desc1, $desc2) = str_split($data['desc'], 20);
      }

      $disbursement->request_timestamp = $this->apiManager->getTimestamp();
      $disbursement->to_account_number = $data['account_number'];
      $disbursement->to_account_name = $data['account_name'];
      $disbursement->cust_reff_id = time();
      $disbursement->trx_desc = $desc1 ?? $data['desc'];
      $disbursement->trx_desc2 = $desc2 ?? '';
      $disbursement->transaction_key = $data['transaction_key'];
      $this->permataDisbursementRepo->save($disbursement);

      \DB::commit();
    } catch (\Exception $e) {
      \Log::info($e->getMessage());
      \DB::rollback();
      return $listener->response(400);
    }

    try {
      $result = $this->apiManager->overbooking([
          'cust_ref_id' => $disbursement->cust_reff_id,
          'request_timestamp' => $disbursement->request_timestamp,
          'to_account_no' => $data['account_number'],
          'amount' => $disbursement->amount,
          'desc' => $disbursement->trx_desc,
          'desc2' => $disbursement->trx_desc2,
          'to_account_name' => $data['account_name']
        ]
      );

      $header = $result['XferAddRs']['MsgRsHdr'];

      $disbursement->status_code = $header['StatusCode'];
      $disbursement->response_timestamp = $header['ResponseTimestamp'];
      $disbursement->status_desc = $header['StatusDesc'];
      $disbursement->trx_reff_no = $result['XferAddRs']['TrxReffNo'];
      $disbursement->response_datetime = Carbon::now();

      switch ($disbursement->status_code) {
        case '00':
          $disbursement->status = PermataDisbursement::STATUS_SUCCESS;
          break;
        case '68':
        case '02':
          $disbursement->status = PermataDisbursement::STATUS_TIMEOUT;
          $disbursement->error_log_response = json_encode($result);
          break;
        default:
          $disbursement->status = PermataDisbursement::STATUS_FAIL;
          $disbursement->error_log_response = json_encode($result);
      }
    } catch (\Exception $e) {
      $disbursement->status = PermataDisbursement::STATUS_FAIL;
      $disbursement->ex_error_message = $e->getMessage();
    }

    $this->permataDisbursementRepo->save($disbursement);

    $this->redis->hdel('odeo_core:permata_disbursement_lock', 'id_' . $data['permata_disbursement_id']);

    if ($disbursement->status != PermataDisbursement::STATUS_SUCCESS) {
      return $listener->response(400, [
        'error_code' => $disbursement->status_code,
      ]);
    }

    return $listener->response(200);
  }

}
