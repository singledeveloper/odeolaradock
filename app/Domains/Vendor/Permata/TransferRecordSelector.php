<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 22/02/19
 * Time: 13.02
 */

namespace Odeo\Domains\Vendor\Permata;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Vendor\Permata\Repository\DisbursementPermataDisbursementRepository;

class TransferRecordSelector implements SelectorListener {

  /**
   * @var DisbursementPermataDisbursementRepository
   */
  private $permataDisbursements;

  public function __construct() {
    $this->permataDisbursements = app()->make(DisbursementPermataDisbursementRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $response['permata_disbursement_id'] = $item->id;
    $response['to_account_number'] = $item->to_account_number;
    $response['to_account_name'] = $item->to_account_name;
    $response['trx_desc'] = [
      'trx_desc_1' => $item->trx_desc,
      'trx_desc_2' => $item->trx_desc2
    ];
    $response['cust_reff_id'] = $item->cust_reff_id;
    $response['response_datetime'] = $item->response_datetime;
    $response['amount'] = $item->amount;
    $response['disbursement_type'] = $item->disbursement_type;
    $response['disbursement_reference_id'] = $item->disbursement_reference_id;
    $response['status'] = $item->status;
    $response['permata_status'] = $item->status_code;
    $response['permata_status_desc'] = $item->status_desc;
    $response['error_log'] = $item->error_log_response;
    return $response;
  }

  public function get(PipelineListener $listener, $data) {
    $this->permataDisbursements->normalizeFilters($data);
    $this->permataDisbursements->setSimplePaginate(true);

    $permataDisbursementRecords = [];
    foreach ($this->permataDisbursements->get() as $permataDisbursement) {
      $permataDisbursementRecords[] = $this->_transforms($permataDisbursement, $this->permataDisbursements);
    }

    if (count($permataDisbursementRecords)) {
      return $listener->response(200, array_merge(
        ['permata_disbursement_record' => $this->_extends($permataDisbursementRecords, $this->permataDisbursements)],
        $this->permataDisbursements->getPagination()
      ));
    }
    return $listener->response(204, ['permata_disbursement_record' => []]);
  }

  public function getSuspectDisbursements(PipelineListener $listener, $data) {
    if ($vendorDisbursements = $this->permataDisbursements->getSuspectDisbursements()) {
      return $listener->response(200, ['suspect_disbursement_record' => $vendorDisbursements]);
    }

    return $listener->response(204, ['suspect_disbursement_record' => []]);
  }
}