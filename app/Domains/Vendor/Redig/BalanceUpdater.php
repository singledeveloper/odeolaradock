<?php


namespace Odeo\Domains\Vendor\Redig;


use Odeo\Domains\Constant\RedigDisbursementMutation;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Redig\Helper\RedigBalanceUpdater;

class BalanceUpdater {

  private $redigBalanceUpdater;

  public function __construct() {
    $this->redigBalanceUpdater = app()->make(RedigBalanceUpdater::class);
  }

  public function deductDisbursement(PipelineListener $listener, $data) {
    $this->redigBalanceUpdater->updateBalance($listener, [
      [
        'reference_type' => RedigDisbursementMutation::REFERENCE_TYPE_REDIG_DISBURSEMENT,
        'reference_id' => $data['redig_disbursement_id'],
        'amount' => -$data['amount'],
        'type' => RedigDisbursementMutation::TYPE_DISBURSEMENT,
      ],
      [
        'reference_type' => RedigDisbursementMutation::REFERENCE_TYPE_REDIG_DISBURSEMENT,
        'reference_id' => $data['redig_disbursement_id'],
        'amount' => -$data['cost'],
        'type' => RedigDisbursementMutation::TYPE_FEE,
      ],
    ]);

    return $listener->response(200);
  }

  public function refundDisbursement(PipelineListener $listener, $data) {
    $this->redigBalanceUpdater->updateBalance($listener, [
      [
        'reference_type' => RedigDisbursementMutation::REFERENCE_TYPE_REDIG_DISBURSEMENT,
        'reference_id' => $data['redig_disbursement_id'],
        'amount' => $data['amount'],
        'type' => RedigDisbursementMutation::TYPE_REFUND_DISBURSEMENT,
      ],
      [
        'reference_type' => RedigDisbursementMutation::REFERENCE_TYPE_REDIG_DISBURSEMENT,
        'reference_id' => $data['redig_disbursement_id'],
        'amount' => $data['cost'],
        'type' => RedigDisbursementMutation::TYPE_REFUND_FEE,
      ],
    ]);

    return $listener->response(200);
  }

  public function topup(PipelineListener $listener, $data) {
    $this->redigBalanceUpdater->updateBalance($listener, [
      [
        'amount' => $data['amount'],
        'type' => RedigDisbursementMutation::TYPE_TOPUP,
      ],
    ]);
    return $listener->response(200);
  }

}
