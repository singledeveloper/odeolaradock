<?php

namespace Odeo\Domains\Vendor\Redig\Helper;


use GuzzleHttp\Client;
use Odeo\Domains\Vendor\Redig\Repository\RedigApiRequestRepository;

class ApiManager {
  const TIMEOUT_SEC = 50;

  private $primaApiRequestRepo;
  private $client;
  private $url;
  private $username;
  private $password;

  public function __construct() {
    $this->primaApiRequestRepo = app()->make(RedigApiRequestRepository::class);
    $this->client = new Client();
    $this->url = env('REDIG_API_URL');
    $this->username = env('REDIG_API_USERNAME');
    $this->password = env('REDIG_API_PASSWORD');
  }

  public function advice($checkTrxId, $refType = null, $refId = null) {
    return $this->request("advice.$checkTrxId", $refType, $refId);
  }

  public function deposit($refType = null, $refId = null) {
    $hash = md5($this->password);
    return $this->request("deposit.{$this->username}.$hash", $refType, $refId);
  }

  public function inquiry($bankCode, $accountNo, $amount, $description, $refType = null, $refId = null) {
    return $this->request("inq_trf.bank_transfer.$bankCode.$accountNo.$amount", $refType, $refId);
  }

  public function transfer($bankCode, $accountNo, $amount, $description, $refType = null, $refId = null) {
    return $this->request("pay_trf.bank_transfer.$bankCode.$accountNo.$amount", $refType, $refId);
  }

  private function new($cmd, $refType, $refId) {
    $req = $this->primaApiRequestRepo->getNew();
    $req->command = $cmd;
    $req->reference_type = $refType;
    $req->reference_id = $refId;
    $req->save();
    return $req;
  }

  private function request($cmd, $refType, $refId, $timeout = self::TIMEOUT_SEC) {
    $apiReq = $this->new($cmd, $refType, $refId);

    $request = arrayToXml([
      'cmd' => $cmd,
      'trxid' => $apiReq->id,
      'user' => $this->username,
      'password' => md5($this->password),
    ], 'api')->asXML();
    $request = str_replace("<?xml version=\"1.0\"?>\n", '', $request);
    $httpStatusCode = null;
    $data = null;
    $error = null;
    $content = null;

    try {
      $response = $this->client->post($this->url, [
        'timeout' => $timeout,
        'form_params' => [
          'req' => $request,
        ],
      ]);
      $httpStatusCode = $response->getStatusCode();
      $content = $response->getBody()->getContents();

      if (!empty($content)) {
        $data = $this->xmlStringToArray($content);
      }
    } catch (\Exception $e) {
      $error = $e->getMessage();
      \Log::error($e);
    }

    $apiReq->error = $error;
    $apiReq->http_status_code = $httpStatusCode;
    $apiReq->response = $content;
    $apiReq->save();

    return [
      'api_request' => $apiReq,
      'api_request_id' => $apiReq->id,
      'http_status_code' => $httpStatusCode,
      'data' => $data,
      'error' => $error,
    ];
  }

  private function xmlStringToArray($xmlString) {
    $xml = simplexml_load_string($xmlString);
    return xmlToArray($xml);
  }
}