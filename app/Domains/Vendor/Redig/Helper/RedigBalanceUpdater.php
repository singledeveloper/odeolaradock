<?php


namespace Odeo\Domains\Vendor\Redig\Helper;


use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Odeo\Domains\Constant\RedigDisbursement;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\BillerReplenishment\Jobs\BeginReplenishment;
use Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository;
use Odeo\Domains\Inventory\Repository\VendorSwitcherRepository;
use Odeo\Domains\Vendor\Redig\Repository\DisbursementRedigMutationRepository;

class RedigBalanceUpdater {

  private $vendorDisbursementRepo, $vendorSwitcherRepo, $disbursementRedigMutationRepo;

  const MINIMUM_TOPUP = 20000000;

  public function __construct() {
    $this->vendorDisbursementRepo = app()->make(VendorDisbursementRepository::class);
    $this->vendorSwitcherRepo = app()->make(VendorSwitcherRepository::class);
    $this->disbursementRedigMutationRepo = app()->make(DisbursementRedigMutationRepository::class);
  }

  public function updateBalance(PipelineListener $listener, $rows) {
    DB::transaction(function () use ($listener, $rows) {
      $this->vendorDisbursementRepo->lock();
      $vendor = $this->vendorDisbursementRepo->findById(VendorDisbursement::REDIG);
      $balance = $vendor->balance;

      foreach ($rows as $row) {
        $mutation = $this->disbursementRedigMutationRepo->getNew();
        $mutation->reference_type = Arr::get($row, 'reference_type');
        $mutation->reference_id = Arr::get($row, 'reference_id');
        $mutation->amount = $row['amount'];
        $mutation->balance_before = $balance;
        $mutation->balance_after = $balance = $balance + $row['amount'];
        $mutation->type = $row['type'];
        $mutation->save();
      }

      $biller = $this->vendorSwitcherRepo->findById(RedigDisbursement::VENDOR_SWITCHER_ID);
      $biller->current_balance = $balance;
      $biller->save();

      $vendor->balance = $balance;
      $vendor->save();

      if ($biller->current_balance <= self::MINIMUM_TOPUP && $biller->is_auto_replenish) {
        $preferredAmount = $this->getPreferredAmount($biller);
        clog('redig-disbursement', 'auto replenish started, preferred amount:' . $preferredAmount);
        /*$listener->pushQueue(new BeginReplenishment([
          'vendor_switcher_id' => $biller->id,
          'preferred_amount' => $preferredAmount,
        ]));*/ //DISABLED
      }
    });
  }

  private function getPreferredAmount($biller) {
    $result = $this->disbursementRedigMutationRepo
      ->getTotalDisbursementAmountAfter(Carbon::now()->subDay(4));
    $amount = ceil(-$result->sum / 2000) * 1000;
    return min(max(self::MINIMUM_TOPUP, $amount), $biller->maximum_topup);
  }


}