<?php


namespace Odeo\Domains\Vendor\Redig\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Redig\Model\DisbursementRedigMutation;

class DisbursementRedigMutationRepository extends Repository {

  public function __construct(DisbursementRedigMutation $model) {
    $this->setModel($model);
  }

  public function getTotalDisbursementAmountAfter($date) {
    return $this->model
      ->where('created_at', '>=', $date)
      ->where('type', 'disbursement')
      ->selectRaw('sum(amount) as sum')
      ->first();
  }
}