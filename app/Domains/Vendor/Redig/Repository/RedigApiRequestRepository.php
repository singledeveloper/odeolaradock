<?php


namespace Odeo\Domains\Vendor\Redig\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Redig\Model\RedigApiRequest;

class RedigApiRequestRepository extends Repository {

  public function __construct(RedigApiRequest $model) {
    $this->setModel($model);
  }
}