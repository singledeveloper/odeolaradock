<?php

namespace Odeo\Domains\Vendor\Redig;


use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Constant\RedigApiRequestReference;
use Odeo\Domains\Constant\RedigDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Redig\Helper\ApiManager;
use Odeo\Domains\Vendor\Redig\Repository\DisbursementRedigDisbursementRepository;
use Odeo\Domains\Vendor\Redig\Repository\RedigApiRequestRepository;

class Transfer {

  private $apiManager, $redigDisbursementRepo, $primaApiRequestRepo, $bankRepo, $redis;

  public function __construct() {
    $this->apiManager = app()->make(ApiManager::class);
    $this->bankRepo = app()->make(BankRepository::class);
    $this->redis = Redis::connection();
    $this->primaApiRequestRepo = app()->make(RedigApiRequestRepository::class);
    $this->redigDisbursementRepo = app()->make(DisbursementRedigDisbursementRepository::class);
  }

  public function exec(PipelineListener $listener, $data) {
    $redigDisb = $data['redig_disbursement'];

    if ($redigDisb->status != RedigDisbursement::PENDING) {
      return $listener->response(400);
    }

    if (!$this->lockDisbursement($redigDisb->id)) {
      return $listener->repsonse(400);
    }

    $bank = $this->bankRepo->findById($redigDisb->bank_id);
    if (!$bank || !$bank->aj_bank_code) {
      return $listener->response(400);
    }

    $success = $this->inquiry($redigDisb, explode('-', $bank->aj_bank_code)[0]);
    if (!$success) {
      return $listener->response(400, [
        'status' => RedigDisbursement::FAIL,
        'transferred' => false,
      ]);
    }

    $success = $this->transfer($redigDisb, explode('-', $bank->aj_bank_code)[0]);
    $this->unlockDisbursement($redigDisb->id);
    return $listener->response($success ? 200 : 400, [
      'status' => $redigDisb->status,
      'transferred' => true,
    ]);
  }

  private function lockDisbursement($id) {
    return $this->redis->hsetnx('odeo_core:redig_disbursement_lock', 'id_' . $id, 1);
  }

  private function unlockDisbursement($id) {
    return $this->redis->hdel('odeo_core:redig_disbursement_lock', 'id_' . $id);
  }

  private function inquiry($redigDisb, $bankCode) {
    $res = $this->apiManager->inquiry($bankCode, $redigDisb->account_no, intval($redigDisb->amount),
      $redigDisb->description, RedigApiRequestReference::PRE_DISBURSEMENT_INQUIRY, $redigDisb->id);
    $apiStatus = Arr::get($res, 'data.status');

    if (Arr::get($res, 'error')) {
      $status = RedigDisbursement::SUSPECT;
      $statusCode = RedigDisbursement::STATUS_CODE_EXCEPTION;
    } else if ($apiStatus === '1' || $apiStatus === 1) {
      $status = RedigDisbursement::FAIL;
      $statusCode = $apiStatus;
    } else if ($apiStatus === '0' || $apiStatus === 0) {
      $status = RedigDisbursement::SUCCESS;
      $statusCode = $apiStatus;
    } else {
      $status = RedigDisbursement::SUSPECT;
      $statusCode = RedigDisbursement::STATUS_CODE_UNKNOWN;
    }

    if ($status == RedigDisbursement::SUCCESS) {
      $redigDisb->account_name = Arr::get($res, 'data.data1');
    } else {
      $redigDisb->status = RedigDisbursement::FAIL;
    }

    $redigDisb->inquiry_status = $status;
    $redigDisb->inquiry_status_code = $statusCode;
    $redigDisb->inquiry_request_id = Arr::get($res, 'api_request_id');
    $redigDisb->save();

    return $status == RedigDisbursement::SUCCESS;
  }

  private function transfer($redigDisb, $bankCode) {
    $res = $this->apiManager->transfer($bankCode, $redigDisb->account_no, intval($redigDisb->amount),
      $redigDisb->description, RedigApiRequestReference::DISBURSEMENT, $redigDisb->id);
    $apiStatus = Arr::get($res, 'data.status');

    if (Arr::get($res, 'error')) {
      $status = RedigDisbursement::SUSPECT;
      $statusCode = RedigDisbursement::STATUS_CODE_EXCEPTION;
    } else if ($apiStatus === '1' || $apiStatus === 1) {
      $status = RedigDisbursement::FAIL;
      $statusCode = $apiStatus;
    } else if ($apiStatus === '0' || $apiStatus === 0) {
      $status = RedigDisbursement::SUCCESS;
      $statusCode = $apiStatus;
    } else {
      $status = RedigDisbursement::SUSPECT;
      $statusCode = RedigDisbursement::STATUS_CODE_UNKNOWN;
    }

    $redigDisb->status = $status;
    $redigDisb->transfer_status_code = $statusCode;
    $redigDisb->transfer_request_id = Arr::get($res, 'api_request_id');
    $redigDisb->save();

    return $status == RedigDisbursement::SUCCESS;
  }
}