<?php

namespace Odeo\Domains\Vendor\SMS\Center\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;
use Odeo\Domains\Order\Model\Order;

class SmsCenterHistory extends Entity
{

  public function centerUser() {
    return $this->belongsTo(SmsCenterUser::class, 'sms_center_user_id');
  }

  public function order() {
    return $this->belongsTo(Order::class);
  }
}
