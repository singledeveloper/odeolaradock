<?php

namespace  Odeo\Domains\Vendor\SMS\Center;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Vendor\SMS\CenterManager;
use Odeo\Domains\Vendor\SMS\Registrar;

class RegisterParser extends CenterManager {

  public function __construct() {
    parent::__construct();
  }

  public function parse(PipelineListener $listener, $data) {

    $this->saveHistory($data);

    list($command, $telephone, $pin) = explode('.', $data['sms_message']);

    $pipeline = new Pipeline();
    $pipeline->add(new Task(Registrar::class, 'register'));
    $pipeline->enableTransaction();
    $pipeline->execute([
      'telephone' => $telephone,
      'user_id' => $data['auth']['user_id']
    ]);

    if ($pipeline->fail()) $this->reply($pipeline->errorMessage, $data['sms_to']);

    return $listener->response(200);
  }

}