<?php

namespace Odeo\Domains\Vendor\SMS\Center\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\SMS\Center\Model\SmsCenterUser;

class SmsCenterUserRepository extends Repository {

  public function __construct(SmsCenterUser $smsCenterUser) {
    $this->model = $smsCenterUser;
  }

  public function findByTelephone($telephone) {
    return $this->model->where('telephone', $telephone)->where('is_active', true)->first();
  }

  public function findByUserId($userId) {
    return $this->model->where('user_id', $userId)->where('is_active', true)->first();
  }

  public function gets($userId) {
    return $this->getResult($this->model->where('user_id', $userId)
      ->where('is_active', true)->orderBy('id', 'desc'));
  }

}
