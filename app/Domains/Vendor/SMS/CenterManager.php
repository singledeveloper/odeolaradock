<?php

namespace Odeo\Domains\Vendor\SMS;

use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Vendor\SMS\Job\SendSms;

class CenterManager {

  private $pin = '', $command = '';
  protected $centerUsers, $centerHistories;

  public function __construct() {

    app('translator')->setLocale('id');

    $this->centerUsers = app()->make(\Odeo\Domains\Vendor\SMS\Center\Repository\SmsCenterUserRepository::class);
    $this->centerHistories = app()->make(\Odeo\Domains\Vendor\SMS\Center\Repository\SmsCenterHistoryRepository::class);
  }

  protected function format($data) {
    $temp = explode('.', $data);
    $command = strtolower($temp[0]);

    $data = Inline::COMMAND_FORMATS;

    if (isset($data[$command]) && count($temp) != $data[$command]) return false;

    $this->command = strtolower($temp[0]);
    $this->pin = $temp[count($temp) - 1];

    if (!isset($data[$command]) && strtolower($this->command) == strtolower($this->pin)) return false;

    return $temp;
  }

  protected function getCommand() {
    return $this->command;
  }

  public function reply($message, $to) {
    dispatch(new SendSms([
      'sms_destination_number' => $to,
      'sms_text' => $message
    ]));
  }

  protected function auth($centerUser) {
    if (Inline::commandNoAuth($this->command)) return ['user_id' => null];

    $user = $centerUser->user;
    if ($centerUser->pin_try_counts == UserStatus::LOGIN_BLOCKED_COUNT ||
      $user->status != UserStatus::OK || $user->login_counts == UserStatus::LOGIN_BLOCKED_COUNT) {
      $this->reply(trans('errors.account_blocked'), $centerUser->telephone);
      return false;
    }
    else if (!Hash::check($this->pin, $centerUser->pin)) {
      $centerUser->pin_try_counts = $centerUser->pin_try_counts + 1;
      $this->centerUsers->save($centerUser);
      if ($centerUser->pin_try_counts == UserStatus::LOGIN_BLOCKED_COUNT) {
        $this->reply(trans('errors.account_blocked'), $centerUser->telephone);
      }
      $this->reply(trans('errors.invalid_password', ["count" => 3 - $centerUser->pin_try_counts]), $centerUser->telephone);
      return false;
    }
    else {
      if ($centerUser->pin_try_counts != 0) {
        $centerUser->pin_try_counts = 0;
        $this->centerUsers->save($centerUser);
      }
      return ['user_id' => $centerUser->id];
    }
  }

  protected function saveHistory($data) {
    $history = $this->centerHistories->getNew();
    $history->message = strtoupper($data['sms_message']);
    $history->sms_center_user_id = isset($data['sms_user_id']) ? $data['sms_user_id'] : null;
    $history->sms_center_id = isset($data['sms_center_id']) ? $data['sms_center_id'] : null;
    $this->centerHistories->save($history);

    return $history;
  }

}
