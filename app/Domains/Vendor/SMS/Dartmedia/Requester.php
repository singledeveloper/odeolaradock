<?php

namespace Odeo\Domains\Vendor\SMS\Dartmedia;

use GuzzleHttp\Client;
use Odeo\Domains\Constant\Affiliate;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\SMS;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\BillerReplenishment\Jobs\BeginReplenishment;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;
use Odeo\Domains\Vendor\SMS\Job\ChangeVendor;

class Requester {

  private $dartmediaSmsLogs, $vendorSwitchers, $cashInserter, $inquiryFees;

  const BASE_URL = 'https://203.166.197.162/ApiDM/';

  function __construct() {
    $this->dartmediaSmsLogs = app()->make(\Odeo\Domains\Vendor\SMS\Dartmedia\Repository\SmsDartmediaLogRepository::class);
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->inquiryFees = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateInquiryFeeRepository::class);
  }

  public function request(PipelineListener $listener, $data) {

    $isAffiliate = isset($data['is_affiliate']);

    if ($isAffiliate && !SMS::findDartmediaReseller($data['auth']['user_id'])) {
      return $listener->response(400, 'You don\'t have access to this endpoint.');
    }

    $log = $this->dartmediaSmsLogs->getNew();
    $log->to = purifyTelephone($data['sms_destination_number']);
    $log->sms_text = $data['sms_text'];
    $log->sms_switcher_id = $isAffiliate ? null : $data['sms_switcher_id'];
    $log->trans_id = time() . purifyTelephone($data['sms_destination_number']);
    $log->user_id = $isAffiliate ? $data['auth']['user_id'] : null;
    $this->dartmediaSmsLogs->save($log);

    $fee = 0;
    if ($isAffiliate) {
      $fee = $this->inquiryFees->findByUserId($data['auth']['user_id']);
      $fee = $fee && $fee->sms_fee != null ? $fee->sms_fee : Affiliate::DEFAULT_PRODUCT_SMS_FEE;
      if ($fee > 0) {
        try {
          $this->cashInserter->add([
            'user_id' => $data['auth']['user_id'],
            'trx_type' => TransactionType::AFFILIATE_INQUIRY_FEE,
            'cash_type' => CashType::OCASH,
            'amount' => -$fee,
            'data' => json_encode([
              'number' => purifyTelephone($data['sms_destination_number']),
              'type' => 'sms'
            ]),
            'reference_type' => TransactionType::AFFILIATE_SMS_DARTMEDIA,
            'reference_id' => $log->id
          ]);
          $this->cashInserter->run();
        }
        catch (InsufficientFundException $e) {
          $log->status = SwitcherConfig::BILLER_FAIL;
          $log->error_message = 'Your oCash is not enough';
          $this->dartmediaSmsLogs->save($log);
          return $listener->response(200, [
            'status' => 'FAILED',
            'error_message' => $log->error_message
          ]);
        }
      }
    }

    if (app()->environment() == 'production') {
      $client = new Client([
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ],
        'timeout' => 30
      ]);

      try {
        $clientResponse = $client->get(self::BASE_URL . 'SMSmt.php', [
          'query' => [
            'uid' => env('DARTMEDIA_UID'),
            'pwd' => env('DARTMEDIA_PWD'),
            'serviceid' => 1000,
            'msisdn' => purifyTelephone($data['sms_destination_number']),
            'sms' => $data['sms_text'],
            'transid' => $log->trans_id,
            'smstype' => 0,
            'sc' => 'ODEO'
          ]
        ]);

        $response = $clientResponse->getBody()->getContents();
        $result = simplexml_load_string($response);

        $array = json_decode(json_encode($result), true);

        if ($array['STATUS'] != '0') {
          if (!$isAffiliate) {
            $listener->pushQueue(new ChangeVendor([
              'sms_destination_number' => $data['sms_destination_number'],
              'sms_text' => $data['sms_text'],
              'sms_switcher_id' => $data['sms_switcher_id']
            ]));
          }
          $log->status = SwitcherConfig::BILLER_FAIL;
        }
        else $log->status = SwitcherConfig::SWITCHER_COMPLETED;

        try {
          $clientResponse = $client->get(self::BASE_URL . 'checkbalance.php', [
            'query' => [
              'uid' => env('DARTMEDIA_UID'),
              'pwd' => env('DARTMEDIA_PWD')
            ]
          ]);

          $balanceResponse = $clientResponse->getBody()->getContents();
          $result = simplexml_load_string($balanceResponse);

          $array = json_decode(json_encode($result), true);

          $biller = $this->vendorSwitchers->findById(SwitcherConfig::DARTMEDIA);
          $biller->current_balance = $array[0];
          $this->vendorSwitchers->save($biller);

          if (intval($biller->current_balance) <= 2000) {
            $listener->pushQueue(new BeginReplenishment([
              'vendor_switcher_id' => SwitcherConfig::DARTMEDIA,
              'preferred_amount' => 3960000
            ]));

            if (intval($biller->current_balance) <= 500) {
              $internalNoticer = app()->make(\Odeo\Domains\Notification\Helper\InternalNoticer::class);
              $internalNoticer->saveMessage('SMS Dartmedia akan segera habis. Mohon cek ulang ke grup (WARN: ' . NotificationType::PARSE_MULTIPLIER  . ')',
                NotificationType::NOTICE_REMINDER, null, true);
            }
          }
        }
        catch (\Exception $e) {}

        $log->xml_response = $response;

      } catch (\Exception $e) {
        $log->xml_response = $e->getMessage();
        if (isset($response)) $log->xml_response .= ',RES: ' . $response;
        $log->status = SwitcherConfig::BILLER_FAIL;
        $log->error_message = 'Operator timeout';

        if (!$isAffiliate) {
          $listener->pushQueue(new ChangeVendor([
            'sms_destination_number' => $data['sms_destination_number'],
            'sms_text' => $data['sms_text'],
            'sms_switcher_id' => $data['sms_switcher_id']
          ]));
        }
      }
    }
    else {
      $log->status = (substr($data['sms_destination_number'], -2) == '14' ?
        SwitcherConfig::BILLER_FAIL : SwitcherConfig::SWITCHER_COMPLETED);
    }

    $this->dartmediaSmsLogs->save($log);

    $result = ['status' => $log->status == SwitcherConfig::BILLER_FAIL ? 'FAILED' : 'SUCCESS'];
    if ($log->status == SwitcherConfig::BILLER_FAIL) {
      $result['error_message'] = $log->error_message ? $log->error_message : '';
      if ($isAffiliate && $fee > 0) {
        $this->cashInserter->clear();
        $this->cashInserter->add([
          'user_id' => $data['auth']['user_id'],
          'trx_type' => TransactionType::REFUND,
          'cash_type' => CashType::OCASH,
          'amount' => $fee,
          'data' => json_encode([
            'number' => purifyTelephone($data['sms_destination_number']),
            'type' => 'sms'
          ]),
          'reference_type' => TransactionType::AFFILIATE_SMS_DARTMEDIA,
          'reference_id' => $log->id
        ]);
        $this->cashInserter->run();
      }
    }

    return $listener->response(200, $result);
  }

}