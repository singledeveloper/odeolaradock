<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/8/17
 * Time: 4:36 PM
 */

namespace Odeo\Domains\Vendor\SMS\Job;


use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Vendor\SMS\Switcher;
use Odeo\Jobs\Job;

class SendSms extends Job  {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    $pipeline = new Pipeline;
    $pipeline->add(new Task(Switcher::class, 'request'));
    $pipeline->execute($this->data);

  }

}
