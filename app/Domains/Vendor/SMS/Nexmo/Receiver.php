<?php

namespace Odeo\Domains\Vendor\SMS\Nexmo;

use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Constant\Obox;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\SMS\CenterManager;

class Receiver extends CenterManager {

  private $currentRoute, $smsCenters;

  public function __construct() {

    parent::__construct();

    $this->smsCenters = app()->make(\Odeo\Domains\Vendor\SMS\Center\Repository\SmsCenterRepository::class);
    $this->currentRoute = 'sms_center_nexmo';
  }

  public function receive(PipelineListener $listener, $data) {
    
    if (!$temp = $this->format(trim($data['text']))) clog($this->currentRoute, 'Format not valid: ' . $data['text']);
    else if (time() - (strtotime($data['message-timestamp']) + (7 * 3600)) > 600) {
      if (!Inline::commandNoAuth($this->getCommand())) $temp[count($temp) - 1] = 'XXXXXX';
      clog($this->currentRoute, "Your message [" . implode('.', $temp) . "] has been expired.");
    }
    else if ($centerUser = $this->centerUsers->findByTelephone($data['msisdn'])) {
      if ($this->auth($centerUser)) {
        if (!Inline::commandNoAuth($this->getCommand())) $temp[count($temp) - 1] = 'XXXXXX';

        $smsCenter = $this->smsCenters->findByPhoneNumber(revertTelephone($data['to']));
    
        app()->make(Obox::getParserPath($this->getCommand()))->parse($listener, [
          'auth' => [
            'user_id' => $centerUser->user_id,
            'type' => UserType::SELLER,
            'platform_id' => Platform::SMS_CENTER
          ],
          'sms_message' => implode('.', $temp),
          'sms_user_id' => $centerUser->id,
          'sms_to' => $centerUser->telephone,
          'sms_center_id' => $smsCenter ? $smsCenter->id : null,
        ]);
      }
      else clog($this->currentRoute, 'Auth not valid: ' . $data['msisdn'] . ', message: ' . trim($data['text']));
    }
    else if ($this->getCommand() == Inline::CMD_AGENT_REGISTRATION) {
      $smsCenter = $this->smsCenters->findByPhoneNumber(revertTelephone($data['to']));
    
      app()->make(Obox::getParserPath($this->getCommand()))->parse($listener, [
        'sms_message' => implode('.', $temp),
        'sms_to' => $data['msisdn'],
        'sms_center_id' => $smsCenter ? $smsCenter->id : null,
      ]);
    }
    else clog($this->currentRoute, 'Telephone not registered: ' . $data['msisdn'] . ', message: ' . trim($data['text']));

    return $listener->response(200);
  }

}