<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/7/17
 * Time: 10:51 PM
 */

namespace Odeo\Domains\Vendor\SMS\Nexmo\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\SMS\Nexmo\Model\SmsNexmoLog;

class SmsNexmoLogRepository extends Repository {

  public function __construct(SmsNexmoLog $smsNexmoLog) {
    $this->model = $smsNexmoLog;
  }

  public function findByMessageId($messageId) {
    return $this->model
      ->where('message_id', $messageId)
      ->first();
  }

  public function getBySwitcherIds($switcherIds) {
    return $this->model->whereIn('sms_switcher_id', $switcherIds)->get();
  }

}