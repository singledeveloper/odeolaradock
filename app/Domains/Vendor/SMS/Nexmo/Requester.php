<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/7/17
 * Time: 3:58 PM
 */

namespace Odeo\Domains\Vendor\SMS\Nexmo;

use GuzzleHttp\Client;
use Odeo\Domains\Constant\NexmoConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\SMS\Job\ChangeVendor;
use Odeo\Domains\Vendor\SMS\Nexmo\Job\SendNexmoFailAlert;

class Requester {

  private $nexmoSmsLogs;

  public function __construct() {
    $this->nexmoSmsLogs = app()->make(\Odeo\Domains\Vendor\SMS\Nexmo\Repository\SmsNexmoLogRepository::class);
  }

  public function request(PipelineListener $listener, $data) {

    if (app()->environment() != 'production') {
      return $listener->response(200);
    }

    $client = new Client([
      'base_uri' => NexmoConfig::BASE_URI,
      'verify' => false
    ]);

    $data['sms_sender_number'] = $data['sms_sender_number'] ?? 'ODEO';

    try {
      $clientResponse = $client->get('json', [
        'query' => [
          'api_key' => NexmoConfig::API_KEY,
          'api_secret' => NexmoConfig::API_SECRET,
          'from' => $data['sms_sender_number'],
          'to' => $data['sms_destination_number'],
          'text' => $data['sms_text'],
        ]
      ]);
    }
    catch(\Exception $e) {
      $listener->pushQueue(new ChangeVendor([
        'sms_destination_number' => $data['sms_destination_number'],
        'sms_text' => $data['sms_text'],
        'sms_switcher_id' => $data['sms_switcher_id']
      ]));

      return $listener->response(200);
    }

    $result = json_decode($clientResponse->getBody(), true);

    if ($result['messages'][0]['status'] != '0') {
      dispatch(new SendNexmoFailAlert([
        'sms_text' => $data['sms_text'],
        'sms_destination_number' => $data['sms_destination_number'],
        'status' => $result['messages'][0]['status'] ?? '-',
        'sms_sender_number' => $data['sms_sender_number'],
        'message_id' => $result['messages'][0]['message-id'] ?? '-',
      ]));

      $listener->pushQueue(new ChangeVendor([
        'sms_destination_number' => $data['sms_destination_number'],
        'sms_text' => $data['sms_text'],
        'sms_switcher_id' => $data['sms_switcher_id']
      ]));
    }

    $log = $this->nexmoSmsLogs->getNew();
    $log->message_count = $result['message-count'] ?? 0;
    $log->from = $data['sms_sender_number'];
    $log->to = $result['messages'][0]['to'];
    $log->message_id = $result['messages'][0]['message-id'] ?? null;
    $log->request_status = $result['messages'][0]['status'] ?? null;
    $log->remaining_balance = $result['messages'][0]['remaining-balance'] ?? 0;
    $log->message_price = $result['messages'][0]['message-price'] ?? 0;
    $log->network_code = $result['messages'][0]['network'] ?? null;
    $log->sms_text = $data['sms_text'];
    $log->sms_switcher_id = $data['sms_switcher_id'];

    $this->nexmoSmsLogs->save($log);

    return $listener->response(200);

  }

}