<?php

namespace Odeo\Domains\Vendor\SMS\Obox\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Vendor\SMS\Center\Model\SmsCenter;

class SmsOutboundLog extends Entity
{
  public function smsCenter() {
    return $this->belongsTo(SmsCenter::class);
  }
}
