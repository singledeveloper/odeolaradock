<?php

namespace Odeo\Domains\Vendor\SMS\Obox\Repository;

use Odeo\Domains\Vendor\SMS\Obox\Model\SmsOutboundLog;
use Odeo\Domains\Core\Repository;

class SmsOutboundLogRepository extends Repository {

  public function __construct(SmsOutboundLog $smsOutboundLog) {
    $this->model = $smsOutboundLog;
  }

  public function findByNumber($number) {
    return $this->model->where('number', $number)->orderBy('id', 'desc')->first();
  }

  public function findLastId() {
    return $this->model->orderBy('id', 'desc')->first();
  }

  public function getBySwitcherIds($switcherIds) {
    return $this->model->whereIn('sms_switcher_id', $switcherIds)->get();
  }

}
