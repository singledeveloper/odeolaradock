<?php

namespace Odeo\Domains\Vendor\SMS\Obox;

use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\Obox;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\PurchasePulsaRecurring;
use Odeo\Domains\Vendor\Jabber\Jobs\SendErrorReport;
use Odeo\Domains\Vendor\SMS\CenterManager;
use Odeo\Domains\Vendor\SMS\Job\ChangeVendor;

class SMSAcceptor extends CenterManager {

  private $jabberManager, $currentRoute, $smsCenters;

  public function __construct() {

    parent::__construct();

    $this->jabberManager = app()->make(\Odeo\Domains\Vendor\Jabber\JabberManager::class);
    $this->smsCenters = app()->make(\Odeo\Domains\Vendor\SMS\Center\Repository\SmsCenterRepository::class);
    $this->currentRoute = 'sms_center';
  }

  public function notify(PipelineListener $listener, $data) {
    try {
      $smsCenter = $this->smsCenters->findByJabberAccount($data['jabber_from']);
      
      preg_match("/\[(.*)\]\[(.*)\]\[(.*)\]/", $data['jabber_message'], $matches);
      list(, $from, $datetime, $message) = $matches;

      if (!$temp = $this->format(trim($message))) clog($this->currentRoute, 'Format not valid: ' . $message);
      else if (time() - strtotime($datetime) > 600) {
        if (!Inline::commandNoAuth($this->getCommand())) $temp[count($temp) - 1] = 'XXXXXX';
        dispatch(new SendErrorReport("Your message [" . implode('.', $temp) . "] has been expired."));
      }
      else if ($centerUser = $this->centerUsers->findByTelephone(ltrim($from, '+'))) {
        if ($this->auth($centerUser)) {
          if (!Inline::commandNoAuth($this->getCommand())) $temp[count($temp) - 1] = 'XXXXXX';

          app()->make(Obox::getParserPath($this->getCommand()))->parse($listener, [
            'auth' => [
              'user_id' => $centerUser->user_id,
              'type' => UserType::SELLER,
              'platform_id' => Platform::SMS_CENTER
            ],
            'sms_message' => implode('.', $temp),
            'sms_user_id' => $centerUser->id,
            'sms_to' => $centerUser->telephone,
            'sms_center_id' => $smsCenter ? $smsCenter->id : null,
          ]);
        }
        else clog($this->currentRoute, 'Auth not valid: ' . $from . ', message: ' . trim($message));
      }
      else if ($this->getCommand() == Inline::CMD_AGENT_REGISTRATION) {
        app()->make(Obox::getParserPath($this->getCommand()))->parse($listener, [
          'sms_message' => implode('.', $temp),
          'sms_to' => ltrim($from, '+'),
          'sms_center_id' => $smsCenter ? $smsCenter->id : null,
        ]);
      }
      else clog($this->currentRoute, 'Telephone not registered: ' . $from . ', message: ' . trim($message));
    }
    catch (\Exception $e) {
      try {
        preg_match("/SMS to (.*) (.*)/", $data['jabber_message'], $matches);
        list(, $to, $info) = $matches;
        
        $to = explode(' ', $to)[0];

        $outboundLogs = app()->make(\Odeo\Domains\Vendor\SMS\Obox\Repository\SmsOutboundLogRepository::class);
        $outbound = $outboundLogs->findByNumber($to);
        $outbound->log_response = $data['jabber_message'];
        $outboundLogs->save($outbound);

        if (strpos($info, "Success") === false) {
          $listener->pushQueue(new ChangeVendor([
            'sms_destination_number' => $to,
            'sms_text' => $outbound->sms_text,
            'sms_switcher_id' => $outbound->sms_switcher_id
          ]));
        }
        else {
          $pulsaPrefixes = app()->make(\Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaPrefixRepository::class);
          $pulsaPrefix = $pulsaPrefixes->getNominal(['prefix' => substr($outbound->number, 0, 5)]);
          $smsCenter = $outbound->smsCenter;
          if ($smsCenter->is_active && $smsCenter->additional_data != null) {
            $json = json_decode($smsCenter->additional_data, true);
            if (isset($json['operator'])) {
              $isRecurring = false;
              $smsCenters = app()->make(\Odeo\Domains\Vendor\SMS\Center\Repository\SmsCenterRepository::class);
              $operatorName = strtolower(str_replace(' ', '_', $pulsaPrefix->operator->name));
              if (isset($json['operator'][$operatorName])) {
                $json['operator'][$operatorName] -= 1;
                if ($json['operator'][$operatorName] <= 0) {
                  $json['operator'][$operatorName] = 0;
                  $isRecurring = true;
                }
              }
              else if (isset($json['operator']['others'])) {
                $json['operator']['others'] -= 1;
                if ($json['operator']['others'] <= 0) {
                  $json['operator']['others'] = 0;
                  $isRecurring = true;
                }
              }
              $smsCenter->additional_data = json_encode($json);
              if ($isRecurring) {
                $smsCenter->is_active = false;
                $listener->pushQueue(new PurchasePulsaRecurring($smsCenter->pulsa_recurring_id));
              }
              $smsCenters->save($smsCenter);
            }
          }
        }
      }
      catch (\Exception $e) {
        try {
          preg_match("/Error:(.*)/", $data['jabber_message'], $matches);
          list(, $info) = $matches;

          if ($smsCenter = $this->smsCenters->findByJabberAccount($data['jabber_from'])) {
            if ($smsCenter->last_error_reported_at == null || ($smsCenter->last_error_reported_at != null && time() - strtotime($smsCenter->last_error_reported_at) > 60))
              dispatch(new SendErrorReport($data['jabber_message'], true));
            $smsCenter->last_error_reported_at = date("Y-m-d H:i:s");
            $this->smsCenters->save($smsCenter);
          }
        }
        catch (\Exception $e) {
          $ping = false;
          try {
            preg_match("/Signal:(.*)\. IP:(.*)/", $data['jabber_message'], $matches);
            list(, $signalStatus, $ip) = $matches;
            $ping = true;
          }
          catch (\Exception $e) {
            if (trim(strtolower($data['jabber_message'])) == 'ping') $ping = true;
            else if (strpos(strtolower($data['jabber_message']), 'ping') === false)
              $this->jabberManager->createLog($this->currentRoute, InlineJabber::TYPE_UNMATCH_PATTERN, $data['jabber_message']);
          }

          $smsCenter = $this->smsCenters->findByJabberAccount($data['jabber_from']);
          if ($ping) {
            $pingData = isset($signalStatus) ? [
              'signal_status' => $signalStatus,
              'ip' => $ip
            ] : [];
            $smsCenter->last_pinged_at = date("Y-m-d H:i:s");
            $smsCenter->is_active = true;
            $smsCenter->additional_data = json_encode(isset($smsCenter->additional_data) ? 
              array_merge(json_decode($smsCenter->additional_data, true), $pingData) : $pingData);
            $this->smsCenters->save($smsCenter);
          }
          else if ($smsCenter->additional_data != null) {
            $json = json_decode($smsCenter->additional_data, true);
            if ($json && isset($json['on_success_recurring']) && isset($json['on_success_recurring']['check_saldo_command_reply_pattern'])) {
              $basePattern = $json['on_success_recurring']['check_saldo_command_reply_pattern'];

              try {
                preg_match_all("/\[[^\]]*\]/", $basePattern, $matchParams);
                $newPattern = str_replace('...', '.*', str_replace($matchParams[0], '(.*)', $basePattern));
                preg_match('/' . $newPattern . '/', $data['jabber_message'], $matches);

                $operators = [];
                $flag = 0;
                foreach($matchParams[0] as $item) {
                  $operators[str_replace(['[', ']'], '', $item)] = trim($matches[++$flag]);
                }

                $json['operator'] = $operators;

                $smsCenter->additional_data = json_encode($json);
                $smsCenter->is_active = true;

                $this->smsCenters->save($smsCenter);
              }
              catch(\Exception $e) {}
            }
          }
        }
      }
    }

    return $listener->response(200);
  }
}