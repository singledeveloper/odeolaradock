<?php

namespace Odeo\Domains\Vendor\SMS;

use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\SMS\Job\SendSms;

class Registrar {

  private $smsUser;

  public function __construct() {
    $this->smsUser = app()->make(\Odeo\Domains\Vendor\SMS\Center\Repository\SmsCenterUserRepository::class);
  }

  public function register(PipelineListener $listener, $data) {

    if ($oldSmsUser = $this->smsUser->findByTelephone(purifyTelephone($data['telephone']))) {
      return $listener->response(400, 'Nomor ini sudah pernah digunakan untuk SMS Center.');
    }

    if (!isset($data['transaction_pin'])) $data['transaction_pin'] = rand('000000', '999999');
    else {
      list($isValid, $message) = checkPinSecure($data['transaction_pin']);
      if (!$isValid) return $listener->response(400, $message);
    }

    $smsUser = $this->smsUser->getNew();
    $smsUser->user_id = isset($data['user_id']) ? $data['user_id'] : getUserId();
    $smsUser->telephone = purifyTelephone($data['telephone']);
    $smsUser->pin = Hash::make($data['transaction_pin']);
    $this->smsUser->save($smsUser);

    if (isset($data['master_code'])) {
      $stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
      if ($store = $stores->findBySubdomain($data['master_code'])) $storeName = $store->name;
      else $storeName = 'ODEO';
    }
    else $storeName = 'ODEO';

    $listener->pushQueue(new SendSms([
      'sms_destination_number' => $data['telephone'],
      'sms_text' => 'Nomor berhasil terdaftar untuk transaksi SMS ' . $storeName . '. ' .
        'Silakan gunakan pin ' . $data['transaction_pin'] .
        '. Ketik ' . Inline::CMD_CHANGE_PIN_SHORT . '.pin_baru.pin_lama untuk ganti pin.'
    ]));

    return $listener->response(200);
  }

  public function remove(PipelineListener $listener, $data) {

    if ($smsUser = $this->smsUser->findById($data['sms_center_id'])) {
      if ($smsUser->user_id != getUserId()) {
        return $listener->response(400, 'No access to this data.');
      }
      $smsUser->is_active = false;
      $this->smsUser->save($smsUser);

      return $listener->response(200);
    }
    return $listener->response(400, 'No access to this data.');
  }

  public function changeTransactionPin(PipelineListener $listener, $data) {

    if ($smsUser = $this->smsUser->findById($data['sms_center_id'])) {
      if ($smsUser->user_id != getUserId())
        return $listener->response(400, 'Tidak ada akses ke data ini.');

      list($isValid, $message) = checkPinSecure($data['new_transaction_pin']);
      if (!$isValid) return $listener->response(400, $message);

      $smsUser->pin = Hash::make($data['new_transaction_pin']);
      $smsUser->pin_try_counts = 0;
      $this->smsUser->save($smsUser);

      return $listener->response(200);
    }
    return $listener->response(400, 'Tidak ada akses ke data ini.');

  }

  public function changeTransactionPinByTelephone(PipelineListener $listener, $data) {

    return $listener->response(400, 'Sedang dalam perbaikan');

    if ($smsUser = $this->smsUser->findByTelephone(purifyTelephone($data['telephone']))) {

      if (!isset($data['bypass_check_old'])) {
        if (!Hash::check($data['old_transaction_pin'], $smsUser->pin))
          return $listener->response(400, 'Pin transaksi lama salah.');
        else if ($data['old_transaction_pin'] == $data['new_transaction_pin'])
          return $listener->response(400, 'Anda tidak dapat menggunakan pin lama.');
      }

      $smsUser->pin = Hash::make($data['new_transaction_pin']);
      $this->smsUser->save($smsUser);

      return $listener->response(200);
    }

    return $listener->response(400, 'Nomor tidak terdaftar.');

  }

}
