<?php

namespace Odeo\Domains\Vendor\SMS;

use Odeo\Domains\Constant\Obox;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class SMSSelector implements SelectorListener {

  private $smsUser, $smsCenters;

  public function __construct() {
    $this->smsUser = app()->make(\Odeo\Domains\Vendor\SMS\Center\Repository\SmsCenterUserRepository::class);
    $this->smsCenters = app()->make(\Odeo\Domains\Vendor\SMS\Center\Repository\SmsCenterRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function getDetail(PipelineListener $listener, $data) {
    if ($smsUser = $this->smsUser->findById($data['sms_center_id'])) {
      if ($smsUser->user_id != getUserId()) return $listener->response(204);
      return $listener->response(200, [
        'sms_registrant' => revertTelephone($smsUser->telephone),
        'is_active' => true
      ]);
    }
    return $listener->response(204);
  }

  public function getList(PipelineListener $listener, $data) {
    $smsCenters = [];
    foreach ($this->smsCenters->getPublished() as $item) {
      $smsCenters[] = [
        'name' => strtoupper($item->operator_name),
        'phone_number' => $item->number
      ];
    }
    return $listener->response(200, $smsCenters);
  }

  public function get(PipelineListener $listener, $data) {
    $smsUsers = [];

    $this->smsUser->normalizeFilters($data);

    foreach ($this->smsUser->gets(getUserId()) as $item) {
      $smsUsers[] = [
        'sms_center_id' => $item->id,
        'sms_registrant' => revertTelephone($item->telephone)
      ];
    }

    if (sizeof($smsUsers) > 0)
      return $listener->response(200, array_merge(
        ["sms_users" => $this->_extends($smsUsers, $this->smsUser)],
        $this->smsUser->getPagination()
      ));

    return $listener->response(204, ["sms_users" => []]);
  }

  public function getCommandExamples(PipelineListener $listener) {
    return $listener->response(200, Obox::getCommandDescriptions());
  }

  public function getStatus(PipelineListener $listener, $data) {
    if ($smsCenter = $this->smsCenters->findById($data['sms_center_id'])) {
      if (isset($smsCenter->additional_data)) {
        try {
          $data = json_decode($smsCenter->additional_data);

          return $listener->response(200, [
            'signal_status' => $data->signal_status,
            'ip' => $data->ip
          ]);
        } catch(\Exception $e) {
          return $listener->response(400, 'Invalid sms center id');
        }
      }
    }
    return $listener->response(400, 'Invalid sms center id');
  }
}
