<?php

namespace Odeo\Domains\Vendor\SMS;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class SMSSwitcherSelector implements SelectorListener {

  private $smsSwitchers, $smsNexmo, $smsObox, $smsZenziva;

  public function __construct() {
    $this->smsSwitchers = app()->make(\Odeo\Domains\Vendor\SMS\Repository\SmsSwitcherRepository::class);
    $this->smsNexmo = app()->make(\Odeo\Domains\Vendor\SMS\Nexmo\Repository\SmsNexmoLogRepository::class);
    $this->smsObox = app()->make(\Odeo\Domains\Vendor\SMS\Obox\Repository\SmsOutboundLogRepository::class);
    $this->smsZenziva = app()->make(\Odeo\Domains\Vendor\SMS\Zenziva\Repository\SmsZenzivaLogRepository::class);
  }

  public function _extends($data, Repository $repository) {
    if ($switcherIds = $repository->beginExtend($data, 'id')) {
      if ($repository->hasExpand('zenziva')) {
        $repository->addExtend('log_zenziva', $this->getZenzivaLogs($switcherIds));
      }
      if ($repository->hasExpand('obox')) {
        $repository->addExtend('log_obox', $this->getOboxLogs($switcherIds));
      }
      if ($repository->hasExpand('nexmo')) {
        $repository->addExtend('log_nexmo', $this->getNexmoLogs($switcherIds));
      }
    }

    return $repository->finalizeExtend($data);
  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function get(PipelineListener $listener, $data) {
    $smsSwitchers = [];

    $this->smsSwitchers->normalizeFilters($data);
    $this->smsSwitchers->setSimplePaginate(true);

    foreach ($this->smsSwitchers->get() as $item) {
      $smsSwitchers[] = [
        'id' => $item->id,
        'number' => revertTelephone($item->number),
        'sms_text' => $item->sms_text,
        'created_at' => $item->created_at->format('Y-m-d H:i:s')
      ];
    }

    if (sizeof($smsSwitchers) > 0)
      return $listener->response(200, array_merge(
        ["sms_switchers" => $this->_extends($smsSwitchers, $this->smsSwitchers)],
        $this->smsSwitchers->getPagination()
      ));

    return $listener->response(204, ["sms_switchers" => []]);
  }

  private function getNexmoLogs($switcherIds) {
    $nexmoLogs = [];
    foreach ($this->smsNexmo->getBySwitcherIds($switcherIds) as $item) {
      $nexmoLogs[$item->sms_switcher_id] = [
        'status' => $item->response_status . ' / Saldo: $' . number_format($item->remaining_balance, 2)
      ];
    }

    foreach ($switcherIds as $item) {
      if (!isset($nexmoLogs[$item])) $nexmoLogs[$item] = [];
    }

    return $nexmoLogs;
  }


  private function getOboxLogs($switcherIds) {
    $oboxLogs = [];
    foreach ($this->smsObox->getBySwitcherIds($switcherIds) as $item) {
      $oboxLogs[$item->sms_switcher_id] = [
        'status' => $item->log_response
      ];
    }

    foreach ($switcherIds as $item) {
      if (!isset($oboxLogs[$item])) $oboxLogs[$item] = [];
    }

    return $oboxLogs;
  }


  private function getZenzivaLogs($switcherIds) {
    $zenzivaLogs = [];
    foreach ($this->smsZenziva->getBySwitcherIds($switcherIds) as $item) {
      try {
        $result = simplexml_load_string($item->xml_response);
        $array = json_decode(json_encode($result), true);
        $zenzivaLogs[$item->sms_switcher_id] = [
          'status' => $array['message']['text'] . ' / Saldo: ' .  $array['message']['balance']
        ];
      }
      catch(\Exception $e) {}
    }

    foreach ($switcherIds as $item) {
      if (!isset($zenzivaLogs[$item])) $zenzivaLogs[$item] = [];
    }

    return $zenzivaLogs;
  }

}
