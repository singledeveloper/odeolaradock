<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/8/17
 * Time: 3:31 PM
 */

namespace Odeo\Domains\Vendor\SMS\Zenziva\Job;


use Odeo\Jobs\Job;

class SendZenzivaFailAlert extends Job  {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    /*Mail::send('emails.zenziva_fail_send_sms', [
      'data' => $this->data
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('server@odeo.co.id', 'odeo')->subject('Zenziva failed to send SMS - ' . time());
    });*/
  }
}
