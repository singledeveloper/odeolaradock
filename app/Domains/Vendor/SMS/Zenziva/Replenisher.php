<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/8/17
 * Time: 1:06 PM
 */

namespace Odeo\Domains\Vendor\SMS\Zenziva;

use GuzzleHttp\Client;
use Odeo\Domains\Biller\Contract\ReplenishmentContract;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\ZenzivaConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Supply\Jobs\SaveLog;

class Replenisher implements ReplenishmentContract {

  private $vendorSwitchers, $paymentInformations;

  function __construct() {
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->paymentInformations = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelInformationRepository::class);
  }

  public function request(PipelineListener $listener, $data) {
    try {
      $client = new \Goutte\Client();
      $client->setClient(new Client(['cookies' => true]));
      $form = $client->request('GET', ZenzivaConfig::BASE_URI)->selectButton('Login')->form();
      $form['username'] = ZenzivaConfig::USERNAME;
      $form['password'] = ZenzivaConfig::PASSWORD;
      $client->submit($form);

      $biller = $this->vendorSwitchers->findById(SwitcherConfig::ZENZIVA);

      $client->request('POST', ZenzivaConfig::BASE_URI . 'apps/save_buycredit.php', [
        'jmlcredit' => json_decode($biller->additional_data)->current_credit_type,
        'bank' => 'BCA'
      ]);

      $response = $client->request('GET', ZenzivaConfig::BASE_URI . 'apps/credit_buyget.php');
      $json = json_decode(strip_tags($response->html()), true);
      $amount = str_replace(',', '', $json["rows"][0]["jmltransfer"]);
      return ['amount' => $amount];
    }
    catch (\Exception $e) {
      dispatch(new SaveLog(SwitcherConfig::ZENZIVA, Supplier::LOG_UNKNOWN_REPORT, $e->getMessage()));
      if (isset($data['in_pipeline'])) return $listener->response(400, $e->getMessage());
      return ['is_error' => true];
    }
  }

  public function complete(PipelineListener $listener, $data) {
    try {
      $client = new \Goutte\Client();
      $client->setClient(new Client(['cookies' => true]));
      $form = $client->request('GET', ZenzivaConfig::BASE_URI)->selectButton('Login')->form();
      $form['username'] = ZenzivaConfig::USERNAME;
      $form['password'] = ZenzivaConfig::PASSWORD;
      $client->submit($form);

      $response = $client->request('GET', ZenzivaConfig::BASE_URI . 'apps/credit_buyget.php');
      $json = json_decode(strip_tags($response->html()), true);

      $paymentDetail = json_decode($this->paymentInformations->findById(Payment::OPC_GROUP_BANK_TRANSFER_BCA)->detail);

      $client->request('POST', ZenzivaConfig::BASE_URI . 'apps/confirm_post.php', [
        'tgl' => date('d/m/Y'),
        'kode' => $json["rows"][0]["kode_trx"],
        'namabank' => $paymentDetail->acc_bank,
        'nama' => $paymentDetail->acc_name,
        'norek' => str_replace('-', '', $paymentDetail->acc_number),
        'jml' => $data['amount'],
        'catatan' => ''
      ]);
    }
    catch (\Exception $e) {
      dispatch(new SaveLog(SwitcherConfig::ZENZIVA, Supplier::LOG_UNKNOWN_REPORT, $e->getMessage()));
    }
    return [];
  }
}