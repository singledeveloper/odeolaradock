<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/8/17
 * Time: 3:48 PM
 */

namespace Odeo\Domains\Vendor\SMS\Zenziva\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\SMS\Zenziva\Model\SmsZenzivaLog;

class SmsZenzivaLogRepository extends Repository {

  public function __construct(SmsZenzivaLog $smsZenzivaLog) {
    $this->model = $smsZenzivaLog;
  }

  public function getBySwitcherIds($switcherIds) {
    return $this->model->whereIn('sms_switcher_id', $switcherIds)->get();
  }

}