<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/8/17
 * Time: 1:06 PM
 */

namespace Odeo\Domains\Vendor\SMS\Zenziva;

use GuzzleHttp\Client;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\ZenzivaConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\BillerReplenishment\Jobs\BeginReplenishment;
use Odeo\Domains\Vendor\SMS\Job\ChangeVendor;
use Odeo\Domains\Vendor\SMS\Zenziva\Job\SendZenzivaFailAlert;

class Requester {
  private $zenzivaSmsLogs, $vendorSwitchers, $paymentInformations, $pulsaPrefix;

  function __construct() {
    $this->pulsaPrefix = app()->make(\Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaPrefixRepository::class);
    $this->zenzivaSmsLogs = app()->make(\Odeo\Domains\Vendor\SMS\Zenziva\Repository\SmsZenzivaLogRepository::class);
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->paymentInformations = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelInformationRepository::class);
  }

  public function request(PipelineListener $listener, $data) {

    $pulsaPrefix = $this->pulsaPrefix->getNominal(['prefix' => substr(purifyTelephone($data['sms_destination_number']), 0, 5)]);
    if (!$pulsaPrefix) return $listener->response(400, "You can't request from this number");

    if ($pulsaPrefix->operator_id != Pulsa::OPERATOR_TELKOMSEL) {
      $client = new Client([
        'base_uri' => ZenzivaConfig::BASE_URI . 'apps/smsapi.php',
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ]
      ]);

      $log = $this->zenzivaSmsLogs->getNew();

      try {
        foreach(explode(';', trans('user.sms_random_pattern')) as $item) {
          list($pattern, $replacementString) = explode('-', $item);
          $replacements = explode('|', $replacementString);
          $data['sms_text'] = str_replace($pattern, $replacements[rand('0', count($replacements) - 1)], $data['sms_text']);
        }
      }
      catch (\Exception $e) {}

      $log->to = $data['sms_destination_number'];
      $log->sms_text = $data['sms_text'];
      $log->sms_switcher_id = $data['sms_switcher_id'];

      try {
        $clientResponse = $client->get('', [
          'query' => [
            'userkey' => ZenzivaConfig::USER_KEY,
            'passkey' => ZenzivaConfig::PASS_KEY,
            'nohp' => $data['sms_destination_number'],
            'pesan' => $data['sms_text']
          ]
        ]);

        $response = $clientResponse->getBody()->getContents();
        $result = simplexml_load_string($response);

        $array = json_decode(json_encode($result), true);

        if ($array['message']['status'] != '0') {
          $listener->pushQueue(new ChangeVendor([
            'sms_destination_number' => $data['sms_destination_number'],
            'sms_text' => $data['sms_text'],
            'sms_switcher_id' => $data['sms_switcher_id']
          ]));

          dispatch(new SendZenzivaFailAlert([
            'text' => $array['message']['text'] ?? '-',
            'to' => $data['sms_destination_number'],
            'status' => $array['message']['status'] ?? '-'
          ]));
        }

        try {
          if (isset($array['message']['balance'])) {
            $biller = $this->vendorSwitchers->findById(SwitcherConfig::ZENZIVA);
            $biller->current_balance = $array['message']['balance'];
            $this->vendorSwitchers->save($biller);

            if (intval($biller->current_balance) <= 20) {
              $listener->pushQueue(new BeginReplenishment([
                'vendor_switcher_id' => SwitcherConfig::ZENZIVA
              ]));
            }
          }
        }
        catch (\Exception $e) {}

        $log->xml_response = $response;

      } catch (\Exception $e) {
        $log->xml_response = $e->getMessage();

        $listener->pushQueue(new ChangeVendor([
          'sms_destination_number' => $data['sms_destination_number'],
          'sms_text' => $data['sms_text'],
          'sms_switcher_id' => $data['sms_switcher_id']
        ]));
      }

      $this->zenzivaSmsLogs->save($log);
    }
    else {
      $listener->pushQueue(new ChangeVendor([
        'sms_destination_number' => $data['sms_destination_number'],
        'sms_text' => $data['sms_text'],
        'sms_switcher_id' => $data['sms_switcher_id']
      ]));
    }

    return $listener->response(200);
  }

}