<?php

namespace Odeo\Domains\Vendor\Telegram\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Order\Model\Order;

class TelegramOrder extends Entity
{
  public function order() {
    return $this->belongsTo(Order::class);
  }

  public function telegramStore() {
    return $this->belongsTo(TelegramStore::class);
  }
}
