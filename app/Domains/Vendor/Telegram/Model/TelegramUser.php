<?php

namespace Odeo\Domains\Vendor\Telegram\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class TelegramUser extends Entity
{
  public function user() {
    return $this->belongsTo(User::class);
  }
}
