<?php

namespace Odeo\Domains\Vendor\Telegram\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Telegram\Model\TelegramHistory;

class TelegramHistoryRepository extends Repository {

  public function __construct(TelegramHistory $telegramHistory) {
    $this->model = $telegramHistory;
  }

  public function findSame($updateId, $timestamp) {
    return $this->model->where('update_id', $updateId)->where('timestamp', $timestamp)->first();
  }

}
