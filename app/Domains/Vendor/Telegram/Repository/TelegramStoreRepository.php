<?php

namespace Odeo\Domains\Vendor\Telegram\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Telegram\Model\TelegramStore;

class TelegramStoreRepository extends Repository {

  public function __construct(TelegramStore $telegramStore) {
    $this->model = $telegramStore;
  }

  public function findByToken($token) {
    return $this->model->where('token', $token)->first();
  }

}
