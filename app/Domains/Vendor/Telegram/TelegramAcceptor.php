<?php

namespace Odeo\Domains\Vendor\Telegram;

use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Constant\InlineTelegram;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;

class TelegramAcceptor extends TelegramManager {

  private $settings;

  public function __construct() {
    parent::__construct();
    $this->settings = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Repository\VendorSwitcherReplenishmentSettingRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {

    if ((isset($data['token']) && isset($data['message'])) && $telegramStore = $this->telegramStores->findByToken($data['token'])) {
      $this->initialize($data['message']['chat']['id'], $data['token']);

      if (!isset($data['message']['text']) || (isset($data['message']['text']) && !$temp = $this->format(trim($data['message']['text'])))) {
        $this->reply('Format salah. Silakan coba lagi. Ketik hi/help');
      }
      else {
        $telegramUser = $this->telegramUser->findByChatId($this->getChatId());

        if ($this->getCommand() == Inline::CMD_SETHP) {
          $user = $this->user->findByTelephone($temp[1]);

          if (!$user) {
            $this->reply('Akun ODEO dengan nomor ' . $temp[1] . ' tidak ditemukan. Silahkan daftar melalui aplikasi ODEO.');
            return $listener->response(200);
          }
          if (!$telegramUser) $telegramUser = $this->telegramUser->findByUserId($user->id);
          if (!$telegramUser) {
            $this->reply('Layanan Telegram belum diaktifkan pada akun ODEO Anda. Untuk mengaktifkan layanan Telegram silahkan buat PIN transaksi baru ' .
              'melalui aplikasi ODEO pada menu Pengaturan > Host to Host > Telegram > Buat PIN Transaksi.');
          } else if ($this->authFromId($telegramUser)) {
            $telegramUser->chat_id = $this->getChatId();
            
            $telegramUser->name = isset($data['message']['from']['username']) ? 
              $data['message']['from']['username'] : 
              $data['message']['from']['first_name'] . (isset($data['message']['from']['last_name']) ? (' ' . $data['message']['from']['last_name']) : '');
            $telegramUser->user_id = $user->id;

            $this->telegramUser->save($telegramUser);

            $this->reply('Akun Telegram Anda telah terhubung ke akun ' . ($telegramStore->store ? $telegramStore->store->name : 'ODEO') .
              ' dengan nomor ' . $temp[1] . '. Anda dapat memulai transaksi. ' .
              'Demi keamanan akun Anda, jagalah selalu kerahasiaan PIN transaksi Telegram Anda dan lakukan penggantian ' .
              'PIN transaksi secara berkala.', InlineTelegram::KEYBOARD_DEFAULT);

            $notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
            $notification->setup($user->id, NotificationType::WARNING, NotificationGroup::ACCOUNT);
            $notification->report("Akun Anda baru saja berhasil melakukan login melalui sistem Telegram. 
              Jika ini bukan Anda, secepatnya lakukan perubahan PIN transaksi Telegram di Pengaturan > Host to Host > Telegram > Ubah pin transaksi");
            $listener->pushQueue($notification->queue());
          }
        }
        else if (!$telegramUser || ($telegramUser && $telegramUser->user_id == null)) {
          if (!in_array($this->getChatId(), $this->settings->getTelegramIgnoredChatId())) {
            $this->reply('Akun Telegram Anda belum terhubung ke Akun ' . ($telegramStore->store ? $telegramStore->store->name : 'ODEO') .
            '. Silahkan masukkan nomor telepon Anda dan PIN transaksi Telegram yang terdaftar dengan format ' .
            strtoupper(Inline::CMD_SETHP) . '.NOMOR_TELEPON.PIN_TRANSAKSI_TELEGRAM');
          }

          $channelLog = $this->channelLogs->getNew();
          $channelLog->type = 'unknown_chat';
          $channelLog->from = 'telegram';
          $channelLog->description = json_encode($data);
          $this->channelLogs->save($channelLog);
        }
        else if ($this->getCommand() == Inline::CMD_LOGOUT) {
          $telegramUser->user_id = null;
          $this->telegramUser->save($telegramUser);

          $this->reply('Anda telah melakukan logout.', false);
        }
        else if ($this->authFromId($telegramUser)) {
          if (Inline::commandHistory($this->getCommand())) {
            if (!$this->telegramHistories->findSame($data['update_id'], $data['message']['date'])) {
              $history = $this->telegramHistories->getNew();
              $history->chat_id = $this->getChatId();
              $history->update_id = $data['update_id'];
              $history->timestamp = $data['message']['date'];

              if (!Inline::commandNoAuth($this->getCommand())) $temp[count($temp) - 1] = 'XXXXXX';
              $history->message = implode('.', $temp);
              $this->telegramHistories->save($history);
            }
            else return $listener->response(200);
          }

          $parser = InlineTelegram::getParserPath($this->getCommand());
          if ($parser == '') $this->reply('Format salah. Silakan coba lagi. Ketik hi');
          else app()->make($parser)->parse($listener, [
            'chat_id' => $this->getChatId(),
            'store_id' => $telegramStore->store_id,
            'token' => $data['token'],
            'auth' => [
              'user_id' => $telegramUser->user_id,
              'type' => UserType::SELLER,
              'platform_id' => Platform::TELEGRAM
            ],
            'telegram_message' => implode('.', $temp)
          ]);
        }
      }
    }
    else clog('telegram_token_invalid', 'token: ' . (isset($data['token']) ? $data['token'] : 'UNKNOWN') . ', data: ' . json_encode($data));

    return $listener->response(200);
  }

}