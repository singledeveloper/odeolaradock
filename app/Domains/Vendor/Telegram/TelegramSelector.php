<?php

namespace Odeo\Domains\Vendor\Telegram;

use Odeo\Domains\Constant\InlineTelegram;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class TelegramSelector implements SelectorListener {

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function getCommandExamples(PipelineListener $listener) {
    return $listener->response(200, InlineTelegram::getCommandDescriptions());
  }

}
