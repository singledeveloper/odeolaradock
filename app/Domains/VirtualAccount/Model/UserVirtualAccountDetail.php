<?php
namespace Odeo\Domains\VirtualAccount\Model;

use Odeo\Domains\Core\Entity;

class UserVirtualAccountDetail extends Entity {

  protected $dates = ['created_at', 'updated_at'];

  public $timestamps = true;

  public function virtualAccount() {
    return $this->belongsTo(UserVirtualAccount::class, 'user_virtual_account_id');
  }
}
