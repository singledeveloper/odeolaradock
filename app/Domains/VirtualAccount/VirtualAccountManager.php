<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 12/12/19
 * Time: 11.49
 */

namespace Odeo\Domains\VirtualAccount;


use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Invoice\Repository\BusinessInvoiceUserSettingRepository;
use Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountDetailRepository;
use Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountRepository;

class VirtualAccountManager {

  public function __construct() {
    $this->userVirtualAccounts = app()->make(UserVirtualAccountRepository::class);
    $this->userVaDetails = app()->make(UserVirtualAccountDetailRepository::class);
    $this->invoiceUserSettings = app()->make(BusinessInvoiceUserSettingRepository::class);
  }

  protected function getUserVirtualAccounts($userVaId, $userId) {
    $vaDetails = [];
    $virtualAccounts = [];
    $vendors = $this->invoiceUserSettings->getActiveVendorChannel($userId);

    foreach ($vendors as $vendor) {
      if ($vendor->max_length == null || !$vendor->support_affiliate_payment) continue;

      $vaNumber = VirtualAccount::generateVirtualAccountNo($vendor->prefix, $vendor->max_length, $userVaId);
      $vaDetails[] = [
        'user_virtual_account_id' => $userVaId,
        'virtual_account_number' => $vaNumber,
        'vendor' => $vendor->code,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
      ];
      $virtualAccounts[] = [
        'name' => $vendor->name,
        'code' => implode('.', str_split($vaNumber, 4))
      ];
    }

    if (count($vaDetails)) {
      $existingVaNumbers = $this->userVaDetails->getVirtualAccountInfoByAccountNumber(array_column($vaDetails, 'virtual_account_number'))->pluck('virtual_account_number')->toArray();
      foreach ($vaDetails as $key => $val) {
        if (in_array($val['virtual_account_number'], $existingVaNumbers)) {
          unset($vaDetails[$key]);
        }
      }
      $this->userVaDetails->saveBulk($vaDetails);
    }

    return $virtualAccounts;
  }

  protected function createVaUser($billedUserId, $userId) {
    $userVa = $this->userVirtualAccounts->getNew();
    $newId = VirtualAccount::getBillerNewVaSequence(UserInvoice::BILLER_ODEO);
    $userVa->id = $this->generateVaId(UserInvoice::BILLER_ODEO, $newId);
    $userVa->user_id = $billedUserId;
    $userVa->biller = UserInvoice::BILLER_ODEO;
    $userVa->service_detail_id = ServiceDetail::USER_INVOICE_ODEO;
    $userVa->service_reference_id = '';
    $userVa->display_name = '';
    $userVa->created_by = $userId;
    $this->userVirtualAccounts->save($userVa);
    VirtualAccount::setBillerLastVaSequence(UserInvoice::BILLER_ODEO, $newId);
  }

  protected function getNewVaUser($billedUserId, $userId, $invoiceNo, $invoiceName, $date) {
    $newId = VirtualAccount::getBillerNewVaSequence(UserInvoice::BILLER_ODEO);
    VirtualAccount::setBillerLastVaSequence(UserInvoice::BILLER_ODEO, $newId);
    return [
      'id' => $this->generateVaId(UserInvoice::BILLER_ODEO, $newId),
      'user_id' => $billedUserId,
      'biller' => UserInvoice::BILLER_ODEO,
      'service_detail_id' => ServiceDetail::USER_INVOICE_ODEO,
      'service_reference_id' => $invoiceNo,
      'display_name' => $invoiceName,
      'created_by' => $userId,
      'created_at' => $date,
      'updated_at' => $date
    ];
  }

  protected function updateVaUser($billedUserId, $userId, $invoiceNo, $invoiceName, $date) {
    $userVa = $this->userVirtualAccounts->findByUserIdAndCreatedBy($billedUserId, $userId);
    return [
      'id' => $userVa->id,
      'service_reference_id' => $invoiceNo,
      'display_name' => $invoiceName,
      'updated_at' => $date
    ];
  }

  protected function generateVaId($billerId, $sequence) {
    return $billerId . sprintf('%0' . 8 . "s", $sequence);
  }

}