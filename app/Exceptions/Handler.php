<?php

namespace Odeo\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Odeo\Domains\Account\TokenManager;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\Platform;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler {

  use \Odeo\Domains\Core\IO;

  protected $dontReport = [
    AuthorizationException::class,
    HttpException::class,
    ModelNotFoundException::class,
    ValidationException::class,
    CommandNotFoundException::class,
  ];

  public function report(Exception $e) {
    if ($this->shouldReport($e)) {
      if (isProduction()) {
        app('sentry')->captureException($e, [
          'extra' => [
            'request_payload' => serialize(file_get_contents('php://input')),
          ]
        ]);
      }
      else {
        \Log::info($e->getMessage());
      }
    }
    parent::report($e);
  }


  public function render($request, Exception $e) {
    $env = app()->environment();

    if ($env == 'production' || $env == 'staging') {
      $userData = TokenManager::getUserData();

      if ($userData && $userData['platform_id'] == Platform::DISBURSEMENT) {
        return response()->json(['error_code' => ApiDisbursement::ERROR_GENERAL_ERROR], 500);
      }
    }

    if ($e instanceof NotFoundHttpException || $e instanceof MethodNotAllowedHttpException) {
      return $this->buildErrorsResponse(null, $e->getStatusCode());
    }

    if ($env == 'production') {
      return $this->buildErrorsResponse($e->getMessage(), 500);
    }

    return parent::render($request, $e);
  }
}
