<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Accounting\AccountingReportSelector;

class AccountingController extends Controller {
    
  public function __construct() {
    parent::__construct();
  }

  public function report() {

    list($isValid, $data) = $this->validateData([
      'period' => 'required',
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountingReportSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

}