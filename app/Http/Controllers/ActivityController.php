<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/7/16
 * Time: 11:41 PM
 */
namespace Odeo\Http\Controllers;

use Odeo\Domains\Activity\ActivitySelector;
use Odeo\Domains\Core\Task;

class ActivityController extends Controller {
    
  public function __construct() {
    parent::__construct();
  }

  public function activity() {

    $data = $this->getRequestData();

    $this->pipeline->add(new Task(ActivitySelector::class, 'get'));

    return $this->executeAndResponse($data);

  }

}