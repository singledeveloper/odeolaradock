<?php
namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Inventory\UserInvoice\UserInvoiceBillerValidator;
use Odeo\Domains\Inventory\UserInvoice\UserInvoiceManager;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\UserInvoiceSelector;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\UserInvoiceUpdater;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\UserInvoiceUserSelector;
use Odeo\Domains\Payment\PaymentVaDirectSelector;

class AffiliateInvoiceController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function list() {
    list($isValid, $data) = $this->validateData([
      'biller_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserInvoiceBillerValidator::class, 'guard'));
    $this->pipeline->add(new Task(UserInvoiceSelector::class, 'get'));

    $this->pipeline->execute($data);

    if($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    $result = $this->pipeline->data;

    unset($result['user_id']);

    return $this->buildResponse($this->pipeline->statusCode, $result);
  }

  public function create() {
    list($isValid, $data) = $this->validateData([
      'biller_id' => 'required',
      'due_date' => 'required|date',
      'billed_user_id' => 'required',
      'billed_user_name' => 'required',
      'period' => 'date_format:Y-m',
      'items' => 'required|array',
      'items.*.name' => 'required',
      'items.*.price' => 'required|integer'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserInvoiceBillerValidator::class, 'guard'));
    $this->pipeline->add(new Task(UserInvoiceUpdater::class, 'create'));
    $this->pipeline->enableTransaction();

    $this->pipeline->execute($data);

    if($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }
    
    $result = [
      'invoice' => [
        'name' => $this->pipeline->data['invoice_name'],
        'total' => $this->pipeline->data['amount']
      ]
    ];

    return $this->buildResponse(201, $result);
  }

  public function batchUpload() {
    list($isValid, $data) = $this->validateData([
      'invoice' => 'required|file|mimes:xlsx,csv',
      'biller_id' => 'required',
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserInvoiceBillerValidator::class, 'guard'));
    $this->pipeline->add(new Task(UserInvoiceUpdater::class, 'batchCreate'));

    $this->pipeline->execute($data);

    if($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }
    
    $result = [
      'invoices' => array_map(function($item) {
        return [
          'name' => $item['name'],
          'total' => $item['total']
        ];
      }, $this->pipeline->data['invoices'])
    ];

    return $this->buildResponse(201, $result);
  }

  public function void($invoiceNumber) {
    $data = $this->getRequestData();
    $data['invoice_number'] = $invoiceNumber;

    $this->pipeline->add(new Task(UserInvoiceBillerValidator::class, 'guard'));
    $this->pipeline->add(new Task(UserInvoiceUpdater::class, 'void'));
    $this->pipeline->enableTransaction();

    $this->pipeline->execute($data);

    if($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    $result = $this->pipeline->data;
    
    unset($result['user_id']);
    
    return $this->buildResponse(200, $result);
  }

  public function initUser($billerId) {
    $data = $this->getRequestData();
    $data['biller_id'] = $billerId;
    $data['service_detail_id'] = ServiceDetail::USER_INVOICE_ODEO;

    $this->pipeline->add(new Task(UserInvoiceManager::class, 'initializeInvoiceUsers'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function userList() {
    list($isValid, $data) = $this->validateData([
      'biller_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserInvoiceBillerValidator::class, 'guard'));
    $this->pipeline->add(new Task(UserInvoiceUserSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function inquiry() {
    list($isValid, $data) = $this->validateData([
      'biller_id' => 'required',
      'user_id' => 'required_without:biller_invoice_number',
      'biller_invoice_number' => 'required_without:user_id'
    ]);

    if (!$isValid) return $data;

    $data['service_detail_id'] = ServiceDetail::USER_INVOICE_ODEO;

    $this->pipeline->add(new Task(UserInvoiceBillerValidator::class, 'guard'));
    $this->pipeline->add(new Task(UserInvoiceManager::class, 'inquiry'));

    return $this->executeAndResponse($data);
  }

  public function pay() {
    list($isValid, $data) = $this->validateData([
      'biller_id' => 'required',
      'biller_invoice_number' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['service_detail_id'] = ServiceDetail::USER_INVOICE_ODEO;

    $this->pipeline->add(new Task(UserInvoiceBillerValidator::class, 'guard'));
    $this->pipeline->add(new Task(UserInvoiceManager::class, 'pay'));

    $this->pipeline->execute($data);

    if($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    $result = $this->pipeline->data['invoices'][0];
    $result['virtual_accounts'] = $this->pipeline->data['virtual_accounts'];

    return $this->buildResponse(200, $result);
  }

  public function cancelPay() {
    list($isValid, $data) = $this->validateData([
      'biller_id' => 'required',
      'user_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['service_detail_id'] = ServiceDetail::USER_INVOICE_ODEO;

    $this->pipeline->add(new Task(UserInvoiceBillerValidator::class, 'guard'));
    $this->pipeline->add(new Task(UserInvoiceManager::class, 'cancelPay'));

    return $this->executeAndResponse($data);
  }

  public function resendNotification() {
    list($isValid, $data) = $this->validateData([
      'biller_id' => 'required',
      'invoice_number' => 'required'
    ]);
    if (!$isValid) return $data;
    
    $data['service_detail_id'] = ServiceDetail::USER_INVOICE_ODEO;

    $this->pipeline->add(new Task(UserInvoiceManager::class, 'resendNotification'));

    return $this->executeAndResponse($data);
  }
}
