<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/8/17
 * Time: 10:44 PM
 */
namespace Odeo\Http\Controllers;

use Odeo\Domains\Agent\AgentAcceptor;
use Odeo\Domains\Agent\AgentRemover;
use Odeo\Domains\Agent\AgentRequester;
use Odeo\Domains\Agent\AgentSelector;
use Odeo\Domains\Agent\AgentPrioritySelector;
use Odeo\Domains\Agent\AgentPriorityUpdater;
use Odeo\Domains\Agent\MasterBonusSetter;
use Odeo\Domains\Agent\MasterDashboardSelector;
use Odeo\Domains\Agent\MasterSetting;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Subscription\ManageInventory\StoreInventoryGroupPriceSelector;
use Odeo\Domains\Subscription\ManageInventory\StoreInventoryGroupPriceUserSelector;
use Odeo\Domains\Subscription\ManageInventory\StoreInventoryGroupPriceUserUpdater;
use Odeo\Domains\Subscription\ManageInventory\StoreInventoryVendorSelector;
use Odeo\Domains\Subscription\StoreVerificator;

class AgentController extends Controller {
    
  public function __construct() {
    parent::__construct();
  }

  public function masterDashboard($storeId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;


    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'allow_mentor' => true,
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(MasterDashboardSelector::class, 'get'));
    $this->pipeline->add(new Task(AgentSelector::class, 'get', [
      'limit' => 3,
      'offset' => 0,
    ]));
    $this->pipeline->add(new Task(AgentSelector::class, 'getPendingAgent', [
      'limit' => 3,
      'offset' => 0,
    ]));

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    }

    $response = $this->pipeline->data;
    $statusCode = 200;

    if (!isset($response['pending_agents']) && !isset($response['agents'])) {
      $statusCode = 204;
    }

    return $this->buildSuccessResponse($response, $statusCode);

  }

  public function getAgent($storeId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'allow_mentor' => true,
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(AgentSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function getPendingAgent($storeId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'allow_mentor' => true,
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(AgentSelector::class, 'getPendingAgent'));

    return $this->executeAndResponse($data);
  }


  public function masterGetBonusSetting($storeId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(MasterBonusSetter::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function masterSetBonusSetting($storeId) {
    $data = $this->getRequestData();

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(MasterBonusSetter::class, 'set'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function masterInformation() {
    $this->pipeline->add(new Task(AgentRequester::class, 'masterInformation'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function masterList() {
    $this->pipeline->add(new Task(AgentRequester::class, 'masterList'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function addMaster() {
    $this->pipeline->add(new Task(AgentRequester::class, 'addMaster'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData());
  }

  public function removeMaster() {
    $this->pipeline->add(new Task(AgentRemover::class, 'agentRemoveMaster'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData());
  }

  public function deleteMaster() {
    $this->pipeline->add(new Task(AgentRemover::class, 'agentDeleteMaster'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getPrioritiesByServiceId($serviceId) {
    $data = $this->getRequestData();
    $data['service_id'] = $serviceId;

    $this->pipeline->add(new Task(AgentPrioritySelector::class, 'getPriorities'));
    return $this->executeAndResponse($data);
  }

  public function updatePrioritiesByServiceId() {

    list($isValid, $data) = $this->validateData([
      'service_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AgentPriorityUpdater::class, 'updatePrioritiesByServiceId'));
    return $this->executeAndResponse($data);
  }

  public function acceptAgent($storeId) {

    list($isValid, $data) = $this->validateData([
      'agent_id' => 'required',
    ]);

    if (!$isValid) return $data;

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(AgentAcceptor::class, 'accept'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function removeAgent($storeId) {

    list($isValid, $data) = $this->validateData([
      'agent_id' => 'required',
    ]);

    if (!$isValid) return $data;

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(AgentRemover::class, 'remove'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function rejectAgent($storeId) {

    list($isValid, $data) = $this->validateData([
      'agent_id' => 'required',
    ]);

    if (!$isValid) return $data;

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(AgentAcceptor::class, 'reject'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }


  public function getAgentGroupPrice($storeId) {

    list($isValid, $data) = $this->validateData([
      'agent_user_id' => 'required',
    ]);

    if (!$isValid) return $data;

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true,
      'response_with_store_data' => true
    ]));

    $this->pipeline->add(new Task(StoreInventoryGroupPriceUserSelector::class, 'getByUser'));

    return $this->executeAndResponse($data, null, [
      'store'
    ]);

  }

  public function getGroupPriceByUserService($storeId, $serviceDetailId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;
    $data['service_detail_id'] = $serviceDetailId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true,
      'response_with_store_data' => true
    ]));

    $this->pipeline->add(new Task(StoreInventoryGroupPriceSelector::class, 'getByServiceDetailId'));

    return $this->executeAndResponse($data, null, [
      'store'
    ]);
  }

  public function updateAgentGroupPrice($storeId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true,
    ]));
    $this->pipeline->add(new Task(StoreInventoryGroupPriceUserUpdater::class, 'update'));

    return $this->executeAndResponse($data);
  }

  public function masterToggleAutoApproveAgent($storeId) {

    $data = $this->getRequestData();
    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(MasterSetting::class, 'toggleAutoApproveAgent'));
    return $this->executeAndResponse($data);

  }
  
}