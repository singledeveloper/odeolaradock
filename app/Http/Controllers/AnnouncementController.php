<?php
namespace Odeo\Http\Controllers;

use Odeo\Domains\Notification\AnnouncementSelector;
use Odeo\Domains\Notification\AnnouncementRequester;
use Odeo\Domains\Core\Task;

class AnnouncementController extends Controller {
    
  public function __construct() {
    parent::__construct();
  }

  public function getActive() {
    $this->pipeline->add(new Task(AnnouncementSelector::class, 'getActive'));

    return $this->executeAndResponse($this->getRequestData());
  }

  public function get() {
    $data = $this->getRequestData();
    $data['sort_by'] = $data['sort_by'] ?? 'id';
    $data['sort_type'] = $data['sort_type'] ?? 'desc';

    $this->pipeline->add(new Task(AnnouncementSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function create() {
    list($isValid, $data) = $this->validateData([
      'content' => 'required',
      'start_date' => 'required',
      'end_date' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AnnouncementRequester::class, 'create'));

    return $this->executeAndResponse($data);
  }

  public function delete() {
    list($isValid, $data) = $this->validateData([
      'announcement_id' => 'required',
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AnnouncementRequester::class, 'delete'));

    return $this->executeAndResponse($data);
  }

}