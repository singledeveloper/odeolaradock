<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementInquiryRequester;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementInquiryValidator;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementRequester;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementSelector;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementSignatureValidator;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementValidator;
use Odeo\Domains\Disbursement\ApiDisbursement\UserBalanceSelector;
use Odeo\Domains\Transaction\Helper\Currency;

class ApiDisbursementController extends Controller {

  public function createDisbursement() {
    $userId = $this->getRequestData()['auth']['user_id'];
    list($isValid, $data, $errors) = $this->validateData([
      'bank_id' => 'required',
      'account_number' => 'required|max:18',
      'amount' => 'required|integer',
      'customer_name' => 'required',
      'customer_email' => 'email',
      'reference_id' => 'required|max:32|regex:/^[a-zA-Z0-9-_]*$/|unique:api_disbursements,reference_id,null,id,user_id,' . $userId,
    ]);

    if (!$isValid) {
      return $this->invalidRequest($errors);
    }

    if (!isset($data['customer_email'])) {
      $data['customer_email'] = '';
    }

    if (!isset($data['description'])) {
      $data['description'] = '';
    }

    $this->pipeline->add(new Task(ApiDisbursementSignatureValidator::class, 'validate', [
      'signature_components' => [
        'CreateDisbursement',
        $data['bank_id'],
        $data['account_number'],
        $data['amount'],
        $data['customer_name'],
        $data['customer_email'],
        $data['description'],
        $data['reference_id'],
      ]
    ]));

    $this->pipeline->add(new Task(ApiDisbursementValidator::class, 'validate'));
    $this->pipeline->add(new Task(ApiDisbursementRequester::class, 'request'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponseUnwrapped($data);
  }

  public function getDisbursementById($id) {
    list($isValid, $data, $errors) = $this->validateData([
      'id' => 'required|integer',
    ], [
      'id' => $id,
    ]);

    if (!$isValid) {
      return $this->invalidRequest($errors);
    }

    $this->pipeline->add(new Task(ApiDisbursementSignatureValidator::class, 'validate', [
      'signature_components' => [
        'GetDisbursement',
        $id,
      ]
    ]));
    $this->pipeline->add(new Task(ApiDisbursementSelector::class, 'findById'));
    return $this->executeAndResponseUnwrapped($this->getRequestData(['disbursement_id' => $id]));
  }

  public function getDisbursementByReferenceId($referenceId) {
    list($isValid, $data, $errors) = $this->validateData([
      'reference_id' => 'required|max:32|regex:/^[a-zA-Z0-9-_]*$/',
    ], [
      'reference_id' => $referenceId,
    ]);

    if (!$isValid) {
      return $this->invalidRequest($errors);
    }

    $this->pipeline->add(new Task(ApiDisbursementSignatureValidator::class, 'validate', [
      'signature_components' => [
        'GetDisbursementByReferenceId',
        $referenceId,
      ]
    ]));
    $this->pipeline->add(new Task(ApiDisbursementSelector::class, 'findByReferenceId'));
    return $this->executeAndResponseUnwrapped($data);
  }

  public function createDisbursementInquiry() {
    list($isValid, $data, $errors) = $this->validateData([
      'bank_id' => 'required',
      'account_number' => 'required|max:20',
      'customer_name' => 'required',
    ]);

    if (!$isValid) {
      return $this->invalidRequest($errors);
    }

    $this->pipeline->add(new Task(ApiDisbursementSignatureValidator::class, 'validate', [
      'signature_components' => [
        'InquireDisbursement',
        $data['bank_id'],
        $data['account_number'],
        $data['customer_name'],
      ]
    ]));
    $this->pipeline->add(new Task(ApiDisbursementInquiryValidator::class, 'validate'));
    $this->pipeline->add(new Task(ApiDisbursementInquiryRequester::class, 'request'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponseUnwrapped($data);
  }

  public function getBalance() {
    /*list($isValid, $data, $errors) = $this->validateData([
      'currency' => 'required|in:IDR',
    ]);

    if (!$isValid) {
      return $this->invalidRequest($errors);
    }*/

    $data = $this->getRequestData();
    $this->pipeline->add(new Task(ApiDisbursementSignatureValidator::class, 'validate', [
      'signature_components' => [
        'GetBalance',
        $data['currency'] ?? Currency::IDR
      ]
    ]));
    $this->pipeline->add(new Task(UserBalanceSelector::class, 'get'));
    return $this->executeAndResponseUnwrapped($data);
  }

  private function invalidRequest($errors) {
    $errorCode = ApiDisbursement::ERROR_INVALID_REQUEST;
    return response()->json([
      'error_code' => $errorCode,
      'message' => ApiDisbursement::getErrorMessage($errorCode) . ': ' . $errors[0],
    ], 400);
  }
}
