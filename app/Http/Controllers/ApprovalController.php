<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/11/19
 * Time: 15.37
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Approval\ApprovalInserter;
use Odeo\Domains\Approval\ApprovalSelector;
use Odeo\Domains\Approval\ApprovalUpdater;
use Odeo\Domains\Core\Task;

class ApprovalController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function submitData() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(ApprovalInserter::class, 'createRequest'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse(['message' => 'Error submitting data']);
    }
    return $this->buildSuccessResponse($this->pipeline->data);
  }

  public function get() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(ApprovalSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function findRawData($id) {
    $data = $this->getRequestData();
    $data['approval_id'] = $id;;
    $this->pipeline->add(new Task(ApprovalSelector::class, 'getRawDataById'));
    return $this->executeAndResponse($data);
  }

  public function approveData($id) {
    $data = $this->getRequestData();
    $data['approval_id'] = $id;
    $this->pipeline->add(new Task(ApprovalUpdater::class, 'approve'));
    return $this->executeAndResponse($data);
  }

  public function cancelData($id) {
    $data = $this->getRequestData();
    $data['approval_id'] = $id;
    $this->pipeline->add(new Task(ApprovalUpdater::class, 'cancel'));
    return $this->executeAndResponse($data);
  }

}