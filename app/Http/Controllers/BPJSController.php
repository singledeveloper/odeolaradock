<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\BPJS\BPJSManager;

class BPJSController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getBpjsKesTnC() {
    $this->pipeline->add(new Task(BPJSManager::class, 'getTermAndCon'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getBpjsTkTnC() {
    $this->pipeline->add(new Task(BPJSManager::class, 'getTermAndCon'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getInquiryKes() {

    list($isValid, $data) = $this->validateData([
      'service_detail_id' => 'required',
      'store_id' => 'required|exists:stores,id',
      'number' => 'required',
      'month_counts' => 'required_without:item_id|integer|min:1|max:12', // TO DO BPJS DELETE LATER
      'item_id' => 'required_without:month_counts'
    ]);

    if (!$isValid) return $data;
    $this->pipeline->add(new Task(BPJSManager::class, 'validatePostpaidInventory', ["for_bill" => true]));
    return $this->executeAndResponse($data);
  }

  public function getInquiryKtg() {

    list($isValid, $data) = $this->validateData([
      'service_detail_id' => 'required',
      'store_id' => 'required|exists:stores,id',
      'nik' => 'required'
    ]);

    if (!$isValid) return $data;
    $this->pipeline->add(new Task(BPJSManager::class, 'validatePostpaidInventory', ["for_bill" => true]));
    return $this->executeAndResponse($data);
  }

}
