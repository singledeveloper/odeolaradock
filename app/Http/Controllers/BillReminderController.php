<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Reminder\BillReminderCreator;
use Odeo\Domains\Reminder\BillReminderRemover;
use Odeo\Domains\Reminder\BillReminderSelector;

class BillReminderController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getBillReminderByUserId() {
    $this->pipeline->add(new Task(BillReminderSelector::class, 'getByUserId'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function create() {
    list($isValid, $data) = $this->validateData([
      'name' => 'required',
      'postpaid_type' => 'required|exists:pulsa_odeos,id',
      'customer_number' => 'required',
      'reminder_date' => 'required|integer|between:1,28',
      'expired_date' => 'date|after:today'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BillReminderCreator::class, 'create'));
    return $this->executeAndResponse($data);
  }

  public function remove() {
    list($isValid, $data) = $this->validateData([
      'bill_reminder_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BillReminderRemover::class, 'remove'));
    return $this->executeAndResponse($data);
  }

}
