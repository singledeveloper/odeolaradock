<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Constant\BillerBakoel;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Biller\Bakoel\Notifier;

class BillerBakoelController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function notifyRefund() {
    $this->pipeline->add(new Task(Notifier::class, 'notify'));
    $this->pipeline->disableResponse();
    return $this->executeAndResponse($this->getRequestData());
  }

  public function checkStatus() {
    list($isValid, $data) = $this->validateData([
      'number' => 'required',
      'trx_id' => 'required'
    ]);

    if (!$isValid) return $data;

    return $this->buildResponse(200, BillerBakoel::setClient(BillerBakoel::PULSA_SERVER)->call(BillerBakoel::CMD_CHECK_STATUS, [
      'msisdn' => $data['number'],
      'trxID' => $data['trx_id']
    ]));
  }

}
