<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Biller\Datacell\Notifier;
use Odeo\Domains\Constant\BillerDatacell;

class BillerDatacellController extends Controller {

  public function __construct() {
    parent::__construct();
  }
  
  public function notify() {
    $this->pipeline->add(new Task(Notifier::class, 'notify'));
    return $this->executeAndResponse();
  }
  
  public function notifyRefund() {
    $this->pipeline->add(new Task(Notifier::class, 'notify'));
    return $this->executeAndResponse($this->getRequestData());
  }
  
  public function test() {
    if (app()->environment() == 'production') return $this->buildErrorsResponse(null, 404);
    
    list($isValid, $data) = $this->validateData([
      'msisdn' => 'required',
      'code' => 'required'
    ]);

    if (!$isValid) return $data;
    
    $time = date("His");
    $xml = '<?xml version="1.0"?><datacell><perintah>charge</perintah>
        <oprcode>' . $data['code'] . '</oprcode>
        <userid>' . BillerDatacell::USER_ID . '</userid>
        <time>' . $time . '</time>
        <msisdn>' . $data['msisdn'] . '</msisdn>
        <ref_trxid>123</ref_trxid>
        <sgn>' . base64_encode((substr($data['msisdn'], -4) . $time) ^ (substr(BillerDatacell::USER_ID, 4) . BillerDatacell::PASSWORD)) . '</sgn>
      </datacell>';
    
    return $this->buildResponse(200, ["text" => "curl --data '" . trim(preg_replace('/\s\s+/', '', $xml)) . "' " . BillerDatacell::PROD_SERVER]);
  }
}
