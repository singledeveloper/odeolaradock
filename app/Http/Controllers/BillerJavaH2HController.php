<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/15/17
 * Time: 6:34 PM
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Biller\JavaH2H\Notifier;
use Odeo\Domains\Core\Task;

class BillerJavaH2HController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function notify() {
    $this->pipeline->add(new Task(Notifier::class, 'notify'));
    return $this->executeAndResponse($this->getRequestData());
  }

}