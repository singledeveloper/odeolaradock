<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Bolt\BoltManager;

class BoltController extends Controller {


  public function __construct() {

    parent::__construct();
    
  }

  public function get() {

    $this->pipeline->add(new Task(BoltManager::class, 'get'));

    return $this->executeAndResponse($this->getRequestData());

  }

}
