<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Constant\ForBusiness;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Marketing\BusinessResellerGroupUpdater;
use Odeo\Domains\Marketing\BusinessResellerSelector;

class BusinessResellerController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getOptions() {
    $data = $this->getRequestData();
    if (isset($data['type'])) {
      if ($data['type'] == 'fee')
        return $this->buildResponse(200, ForBusiness::getFeeTypeList());
      else if ($data['type'] == 'conditional')
        return $this->buildResponse(200, ForBusiness::getConditionalTypeList());
      else if ($data['type'] == 'transaction')
        return $this->buildResponse(200, ForBusiness::getTransactionTypeList());
      else if ($data['type'] == 'reseller') {
        $this->pipeline->add(new Task(BusinessResellerSelector::class, 'getOptions'));
        return $this->executeAndResponse($data);
      }
    }
    return $this->buildResponse(204);
  }

  public function getList() {
    $this->pipeline->add(new Task(BusinessResellerSelector::class, 'getList'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getTemplates() {
    $this->pipeline->add(new Task(BusinessResellerSelector::class, 'getTemplates'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getAgents() {
    $this->pipeline->add(new Task(BusinessResellerSelector::class, 'getAgentList'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getAgentDetails($resellerId) {
    $data = $this->getRequestData();
    $data['business_reseller_id'] = $resellerId;
    $this->pipeline->add(new Task(BusinessResellerSelector::class, 'getAgentDetail'));
    return $this->executeAndResponse($data);
  }

  public function getDetail($resellerId) {
    $data = $this->getRequestData();
    $data['business_reseller_id'] = $resellerId;
    $this->pipeline->add(new Task(BusinessResellerSelector::class, 'getDetail'));
    return $this->executeAndResponse($data);
  }

  public function getDetailTemplate($templateId) {
    $data = $this->getRequestData();
    $data['template_id'] = $templateId;
    $this->pipeline->add(new Task(BusinessResellerSelector::class, 'getDetailTemplate'));
    return $this->executeAndResponse($data);
  }

  public function changeResellerTemplate() {
    list($isValid, $data) = $this->validateData([
      'business_reseller_id' => 'required',
      'template_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BusinessResellerGroupUpdater::class, 'changeTemplate'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function addGroup() {
    list($isValid, $data) = $this->validateData([
      'business_reseller_id' => 'required_without:template_id',
      'template_id' => 'required_without:business_reseller_id',
      'type' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BusinessResellerGroupUpdater::class, 'addGroup'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function removeGroup($groupId) {
    $data = $this->getRequestData();
    $data['group_id'] = $groupId;
    $this->pipeline->add(new Task(BusinessResellerGroupUpdater::class, 'removeGroup'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function addGroupDetail() {
    list($isValid, $data) = $this->validateData([
      'group_id' => 'required',
      'fee_type' => 'required',
      'fee_value' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BusinessResellerGroupUpdater::class, 'addGroupDetail'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function removeGroupDetail($detailId) {
    $data = $this->getRequestData();
    $data['detail_id'] = $detailId;
    $this->pipeline->add(new Task(BusinessResellerGroupUpdater::class, 'removeGroupDetail'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function approveBonus() {
    $this->pipeline->add(new Task(BusinessResellerGroupUpdater::class, 'approveBonus'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getPgTransactions() {
    list($isValid, $data) = $this->validateData([
      'payment_group_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BusinessResellerSelector::class, 'getPgTransactions'));
    return $this->executeAndResponse($data);
  }
}
