<?php
/**
 * Created by PhpStorm.
 * User: mrp130
 * Date: 9/15/2017
 * Time: 11:54 AM
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Campaign\DepositCashback\DepositInterestSelector;
use Odeo\Domains\Campaign\DepositCashback\PresetSelector;
use Odeo\Domains\Campaign\WeeklyTarget\WeeklyTargetSelector;
use Odeo\Domains\Core\Task;

class CampaignController extends Controller {

  public function weeklyStoreCampaign($storeId) {

    $data = $this->getRequestData();
    $data['storeId'] = $storeId;

    $this->pipeline->add(new Task(WeeklyTargetSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function getDepositCashbackPreset($storeId) {

    $data = $this->getRequestData();
    $data['storeId'] = $storeId;

    $this->pipeline->add(new Task(PresetSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function weekdayRestockReport() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(PresetSelector::class, 'summary'));

    return $this->executeAndResponse($data);
  }

  public function interestReport() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(DepositInterestSelector::class, 'summary'));

    return $this->executeAndResponse($data);
  }
}
