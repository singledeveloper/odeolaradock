<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 5:03 PM
 */

namespace Odeo\Http\Controllers;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Loginner;
use Odeo\Domains\Constant\CashConfig;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Order\CartSelector;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\PaymentOpenRequester;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Domains\Subscription\StoreVerificator;
use Odeo\Domains\Transaction\TempDepositRequester;

class CartController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getCart() {
    $this->pipeline->add(new Task(CartSelector::class, 'get'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function addCart() {
    list($isValid, $data) = $this->validateData([
      'service_detail_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guardService'));
    $this->pipeline->add(new Task(CartInserter::class, 'addToCart'));
    $this->pipeline->enableTransaction();

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    }

    $response = $this->pipeline->data;

    unset($response['sale_price'], $response['base_price'], $response['margin'], $response['name']);

    return $this->buildSuccessResponse($response, $this->pipeline->statusCode);
  }

  public function removeCart() {
    list($isValid, $data) = $this->validateData([
      'uid' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(CartRemover::class, 'removeItem'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function clearAll() {
    $this->pipeline->add(new Task(CartRemover::class, 'clear'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($this->getRequestData());
  }

  public function checkout() {
    list($isValid, $data) = $this->validateData([
      'gateway_id' => 'required',
    ]);

    if (!$isValid) return $data;

    $data["platform_id"] = $data["auth"]["platform_id"];


    $this->pipeline->add(new Task(CartCheckouter::class, 'checkout'));
    $this->pipeline->enableTransaction();

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    return $this->buildResponse(201, $this->pipeline->data);

  }

  public function instantCheckout() {
    list($isValid, $data) = $this->validateData([
      'item_id' => 'required',
      'store_id' => 'required',
      'gateway_id' => 'required',
      'service_detail_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $data["platform_id"] = $data["auth"]["platform_id"];
    $data["check_double_purchase"] = $data["check_double_purchase"] ?? true;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guardService'));
    $this->pipeline->add(new Task(CartInserter::class, 'addToCart'));
    $this->pipeline->add(new Task(CartCheckouter::class, 'checkout'));
    $this->pipeline->enableTransaction();

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, 400, '', $this->pipeline->errorStatus);
    }

    return $this->buildResponse(200, [
      'order_id' => $this->pipeline->data['order_id']
    ]);
  }

  public function cashCheckout() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required',
      'gateway_id' => 'required',
      'direct_payment_id' => 'integer',
      'info_id' => 'required_with:direct_payment_id',
      'channel_id' => 'integer',
      'channel_name' => 'required_with:channel_id',
      'item_id' => 'required',
      'cash_amount' => 'integer',
      'service_detail_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $data["platform_id"] = $data["auth"]["platform_id"];
    $data["check_double_purchase"] = $data["check_double_purchase"] ?? true;

    $redis = Redis::connection();

    $namespace = 'odeo_core:checkout_lock';
    $key = 'checkout_item_' . $data['item_id'] . '_' . $data['auth']['user_id'] .
      (isset($data["item_detail"]) && isset($data["item_detail"]["number"]) ?
        ("_" . $data["item_detail"]["number"]) : "");

    if ($data['platform_id'] == Platform::ANDROID && !$redis->hsetnx($namespace, $key, 1)) {
      return $this->buildResponse(200);
    }

    $requirePin = (isset($data['cash_amount']) && $data['cash_amount'] > CashConfig::MIN_PAYMENT_REQUIRE_PIN) || isset($data['pin']);

    $firstPipelineData = [];
    if ($requirePin) {
      $this->pipeline->add(new Task(Loginner::class, 'checkPin'));
      $this->pipeline->execute($data);

      if ($this->pipeline->fail()) {
        $data['platform_id'] == Platform::ANDROID && $redis->hdel($namespace, $key);
        return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
      }

      $firstPipelineData = $this->pipeline->data;
      $this->pipeline = $this->pipeline->clear();
    }

    $info_id = isset($data['info_id']) && !empty($data['info_id']) ? $data['info_id'] : Payment::OPC_GROUP_OCASH;
    $this->pipeline->add(new Task(CartRemover::class, 'clear'));
    $this->pipeline->add(new Task(CartInserter::class, 'addToCart'));
    $this->pipeline->add(new Task(CartCheckouter::class, 'generateSignature', [
      'with_opc' => true,
      'info_id' => $info_id
    ]));
    $this->pipeline->add(new Task(CartCheckouter::class, 'checkout'));
    $this->pipeline->add(new Task(TempDepositRequester::class, 'tempDeposit'));
    $this->pipeline->add(new Task(PaymentRequester::class, 'request'));

    if ($info_id == Payment::OPC_GROUP_OCASH) {
      $this->pipeline->add(new Task(PaymentOpenRequester::class, 'open', [
        'with_password' => true
      ]));
    }
    $this->pipeline->enableTransaction();
    $this->pipeline->execute(array_merge($data, $firstPipelineData));

    $data['platform_id'] == Platform::ANDROID && $redis->hdel($namespace, $key);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    } else {
      return $this->buildResponse(200, []);
    }
  }

  public function depositCheckout() {

    return $this->buildResponse(400, 'Pembelian melalui deposit telah ditutup. Silahkan melakukan pembelian melalui web atau gunakan kode master toko Anda.');

    list($isValid, $data) = $this->validateData([
      'store_id' => 'required',
      'gateway_id' => 'required',
      'item_id' => 'required',
      'service_detail_id' => 'required',
      'paid_amount' => 'integer',
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', ['store_must_active' => true]));
    $this->pipeline->add(new Task(CartRemover::class, 'clear'));
    $this->pipeline->add(new Task(CartInserter::class, 'addToCart'));
    $this->pipeline->add(new Task(CartCheckouter::class, 'generateSignature', [
      'with_opc' => true,
      'info_id' => Payment::OPC_GROUP_CASH_ON_DELIVERY
    ]));
    $this->pipeline->add(new Task(CartCheckouter::class, 'checkout'));
    $this->pipeline->add(new Task(TempDepositRequester::class, 'tempDeposit'));
    $this->pipeline->add(new Task(PaymentRequester::class, 'request'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    } else {
      dispatch(new VerifyOrder($this->pipeline->data['order_id']));
      return $this->buildResponse(200, []);
    }
  }

  public function terminalCheckout() {
    list($isValid, $data) = $this->validateData([
      'item_id' => 'required',
      'gateway_id' => 'required',
      'service_detail_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $data["platform_id"] = $data["auth"]["platform_id"];
    unset($data['skip_signature']);

    $this->pipeline->add(new Task(StoreVerificator::class, 'guardService'));
    $this->pipeline->add(new Task(CartInserter::class, 'addToCart'));
    $this->pipeline->add(new Task(CartCheckouter::class, 'checkout'));
    $this->pipeline->enableTransaction();

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, 400, '', $this->pipeline->errorStatus);
    }

    return $this->buildResponse(200, [
      'order_id' => $this->pipeline->data['order_id']
    ]);
  }
}
