<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Transaction\CashRecurringSelector;
use Odeo\Domains\Transaction\CashRecurringUpdater;

class CashRecurringController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getLatestSchedules() {
    $this->pipeline->add(new Task(CashRecurringSelector::class, 'getLatest'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getSchedules() {
    $this->pipeline->add(new Task(CashRecurringSelector::class, 'gets'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getHistories() {
    $this->pipeline->add(new Task(CashRecurringSelector::class, 'getHistories'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function toggleSchedule($scheduleId) {
    $data = $this->getRequestData();
    $data['schedule_id'] = $scheduleId;
    $this->pipeline->add(new Task(CashRecurringUpdater::class, 'toggle'));
    return $this->executeAndResponse($data);
  }

  public function retryHistory($historyId) {
    $data = $this->getRequestData();
    $data['history_id'] = $historyId;
    $this->pipeline->add(new Task(CashRecurringUpdater::class, 'retry'));
    return $this->executeAndResponse($data);
  }

}
