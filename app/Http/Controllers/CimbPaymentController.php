<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 16/07/19
 * Time: 12.37
 */

namespace Odeo\Http\Controllers;


use Illuminate\Http\Request;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Cimb\Helper\CimbHelper;
use Odeo\Domains\Payment\Cimb\Inquirer;
use Odeo\Domains\Payment\Cimb\Notifier;
use Odeo\Domains\Payment\Cimb\Repository\PaymentCimbCallbackLogRepository;
use Odeo\Domains\Payment\Cimb\Validator;
use Odeo\Domains\Payment\Helper\PaymentInvoiceHelper;
use Odeo\Domains\Payment\PaymentVaDirectInquirer;
use Odeo\Domains\PaymentGateway\PaymentGatewayChannelValidator;
use Odeo\Domains\PaymentGateway\PaymentGatewayInvoiceNotifier;
use Odeo\Domains\PaymentGateway\PaymentGatewayInvoiceSelector;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;

class CimbPaymentController extends PaymentGatewayInvoiceController {

  function __construct() {
    parent::__construct();
    $this->cimbCallbackLogRepo = app()->make(PaymentCimbCallbackLogRepository::class);
  }

  private function logCallback($type, $body) {
    $log = $this->cimbCallbackLogRepo->getNew();
    $log->type = $type;
    $log->body = $body;
    $this->cimbCallbackLogRepo->save($log);
  }

  public function cimbEcho() {
    return response(CimbHelper::generateResponseXml(['EchoResponse' => 'RESPONSE'], CimbHelper::SERVICE_ECHO))->header('Content-Type', 'application/xml');
  }

  public function cimbInquiry(Request $request) {
    $xmlData = $request->getContent();

    $xml = str_replace(['<m:', '</m:'], ['<', '</'], $xmlData);
    $dom = new \DOMDocument();
    $dom->loadXML($xml);

    $data = domElementsToArray($dom, 'InquiryRq');
    $data['xml_request'] = $xml;

    $data['virtual_account_number'] = $data['CompanyCode'] . $data['CustomerKey1'];

    if ($this->isInvoiceRequest($data['virtual_account_number'])) {
      return $this->invoiceInquiry($data);
    }

    $this->logCallback('va_inquiry', $xmlData);

    $data['xml_request'] = $xml;
    $data['vendor_code'] = VirtualAccount::VENDOR_DIRECT_CIMB;

    $this->pipeline->add(new Task(PaymentGatewayChannelValidator::class, 'validatePaymentVendor'));
    $this->pipeline->add(new Task(Inquirer::class, 'vaInquiry'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAInquiry'));
    $this->pipeline->add(new Task(Inquirer::class, 'createInquiry'));
    $this->pipeline->execute($data);

    $result = $this->pipeline->data;
    if ($this->pipeline->fail()) {
      clog('cimb_va', 'inquiry error');
      clog('cimb_va', json_encode($this->pipeline->errorMessage));
      $errorCode = $this->pipeline->errorMessage['cimb_error_code'] ?? '40';
      $errorData = $this->buildInquiryErrorResponse($data, $errorCode);

      if (isset($this->pipeline->errorMessage['error_code']) && in_array($this->pipeline->errorMessage['error_code'], [9999, 3002])) {
        $errorData['ResponseCode'] = '16';
        $errorData['ResponseDescription'] = 'Customer Not Found';
      }

      $errorXml = CimbHelper::generateResponseXml($errorData, CimbHelper::SERVICE_INQUIRY);
      clog('cimb_va', 'error_inquiry_response');
      clog('cimb_va', $errorXml);

      return response($errorXml)
        ->header('Content-type', 'application/xml');
    }

    clog('cimb_va', 'inquiry_response');
    clog('cimb_va', $result['xml_response']);
    return response($result['xml_response'])
      ->header('Content-type', 'application/xml');

  }

  private function invoiceInquiry($data) {
    $invoiceLog = $this->logInvoiceRequest('invoice_inquiry', VirtualAccount::VENDOR_DIRECT_CIMB, json_encode($data));
    $data['vendor_code'] = VirtualAccount::VENDOR_DIRECT_CIMB;

    $this->pipeline->add(new Task(PaymentVaDirectInquirer::class, 'inquiry'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $errorCode = $this->pipeline->errorMessage['error_code'] ?? 9999;
      switch ($errorCode) {
        case PaymentInvoiceHelper::INVOICE_INVALID:
          $errorCode = '16';
          break;
        case PaymentInvoiceHelper::INVOICE_NOT_FOUND:
          $errorCode = '40';
          break;
        default:
          $errorCode = '99';
      }

      $errorXml = CimbHelper::buildInquiryErrorXml($data, $errorCode);
      clog('cimb_invoice_va', 'error_inquiry_response');
      clog('cimb_invoice_va', $errorXml);

      return response($errorXml)
        ->header('Content-type', 'application/xml');
    }

    $result = $this->pipeline->data;
    $this->updateInvoiceLog($invoiceLog, $result['order_id']);
    $responseXml = CimbHelper::buildInvoiceInquiryXml($data, $result);

    clog('cimb_invoice_va', 'inquiry_response');
    clog('cimb_invoice_va', $responseXml);

    return response($responseXml)
      ->header('Content-type', 'application/xml');
  }

  public function cimbNotify(Request $request) {
    $xmlData = $request->getContent();

    $xml = str_replace(['<m:', '</m:'], ['<', '</'], $xmlData);
    $dom = new \DOMDocument();
    $dom->loadXML($xml);
    $data = domElementsToArray($dom, 'PaymentRq');

    $data['virtual_account_number'] = $data['CompanyCode'] . $data['CustomerKey1'];
    if ($this->isInvoiceRequest($data['virtual_account_number'])) {
      return $this->invoicePayment($data);
    }
    $this->logCallback('va_payment', $xmlData);

    $data['xml_request'] = $xml;

    $data['virtual_account_number'] = $data['CompanyCode'] . $data['CustomerKey1'];
    $data['vendor_code'] = VirtualAccount::VENDOR_DIRECT_CIMB;

    $this->pipeline->add(new Task(PaymentGatewayChannelValidator::class, 'validatePaymentVendor'));
    $this->pipeline->add(new Task(Notifier::class, 'vaPayment'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAPayment'));
    $this->pipeline->add(new Task(Notifier::class, 'updatePayment'));

    $this->pipeline->execute($data);
    $result = $this->pipeline->data;
    if ($this->pipeline->fail()) {
      clog('cimb_va', 'payment error');
      clog('cimb_va', json_encode($this->pipeline->errorMessage));

      $errorCode = $this->pipeline->errorMessage['cimb_error_code'] ?? '32';
      $errorXml = CimbHelper::buildPaymentErrorXml($data, $errorCode);

      clog('cimb_va', 'error_response');
      clog('cimb_va', $errorXml);

      return response($errorXml)->header('Content-type', 'application/xml');
    }

    clog('cimb_va', 'payment response');
    clog('cimb_va', $result['xml_response']);

    return response($result['xml_response'])->header('Content-type', 'application/xml');

  }

  private function invoicePayment($data) {
    $invoiceLog = $this->logInvoiceRequest('invoice_payment', VirtualAccount::VENDOR_DIRECT_CIMB, json_encode($data));

    $data['vendor_code'] = VirtualAccount::VENDOR_DIRECT_CIMB;
    $data['amount'] = $data['Amount'];

    $this->pipeline->add(new Task(Validator::class, 'validatePayment'));
    $this->pipeline->add(new Task(PaymentGatewayInvoiceSelector::class, 'getDetailByVaNo'));
    $this->pipeline->add(new Task(PaymentGatewayInvoiceNotifier::class, 'notify'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $errorCode = $this->pipeline->errorMessage['cimb_error_message'] ?? '32';

      switch ($errorCode) {
        case PaymentInvoiceHelper::INVOICE_NOT_FOUND:
        case PaymentInvoiceHelper::INVOICE_INVALID:
          $errorCode = '32';
          break;
        case PaymentInvoiceHelper::INVOICE_INVALID_AMOUNT:
          $errorCode = '38';
          break;
        default:
          $errorCode = '99';
      }

      $errorXml = CimbHelper::buildPaymentErrorXml($data, $errorCode);

      clog('cimb_invoice_va', 'error payment');
      clog('cimb_invoice_va', $errorXml);

      return response($errorXml)->header('Content-type', 'application/xml');
    }

    $orderId = $this->pipeline->data['order_id'];
    $this->updateInvoiceLog($invoiceLog, $orderId);

    $responseXml = CimbHelper::buildInvoicePaymentXml($data);

    clog('cimb_invoice_va', 'payment response');
    clog('cimb_invoice_va', $responseXml);

    return response($responseXml)->header('Content-type', 'application/xml');
  }

  private function buildInquiryErrorResponse($data, $errorCode) {
    return [
      'TransactionID' => $data['TransactionID'],
      'TransactionDate' => $data['TransactionDate'],
      'CompanyCode' => $data['CompanyCode'],
      'CustomerKey1' => $data['CustomerKey1'],
      'BillDetailList' =>
        '<BillDetail>' . PHP_EOL .
        '<BillCurrency>IDR</BillCurrency>' . PHP_EOL .
        '<BillCode/>' . PHP_EOL .
        '<BillAmount>0</BillAmount>' . PHP_EOL .
        '<BillReference/>' . PHP_EOL .
        '</BillDetail>' . PHP_EOL,
      'Amount' => '',
      'Fee' => '',
      'PaidAmount' => '',
      'CustomerName' => '',
      'FlagPayment' => '',
      'ResponseCode' => $errorCode,
      'ResponseDescription' => CimbHelper::getResponseMessage($errorCode)
    ];
  }

}
