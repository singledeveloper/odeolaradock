<?php

namespace Odeo\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\IOListener;

class Controller extends BaseController implements IOListener {

  use \Odeo\Domains\Core\IO;

  protected $pipeline;

  public function __construct() {
    $this->pipeline = app()->make(\Odeo\Domains\Core\Pipeline::class);
  }

  protected function executeAndResponse($data = [], $pipeline = null, $forget = []) {
    if (!$pipeline) $pipeline = $this->pipeline;

    $pipeline->execute($data);

    $response = $pipeline->fail() ? $pipeline->errorMessage : $pipeline->data;

    !empty($forget) && is_array($response) && $response = array_filter($response, function ($key) use ($forget) {
      return !in_array($key, $forget, true);
    }, ARRAY_FILTER_USE_KEY);

    if ($pipeline->fail()) {
      return $this->buildErrorsResponse($response, $pipeline->statusCode, "", $pipeline->errorStatus ?? null);
    } else if ($pipeline->isNeedResponse) {
      return $this->buildSuccessResponse($response, $pipeline->statusCode);
    }
  }

  protected function executeAndResponseUnwrapped($data = [], $pipeline = null) {
    if (!$pipeline) $pipeline = $this->pipeline;
    $pipeline->execute($data);

    $sc = $pipeline->statusCode;

    if ($pipeline->fail()) {
      $response = $pipeline->errorMessage;
      if (!$response || !isset($response['error_code'])) {
        clog('disbursement-controller', 'Unknown error message format: ' . json_encode($response));
        $response = ['error_code' => ApiDisbursement::ERROR_GENERAL_ERROR];
      }
      return response()->json($response, $sc);
    } else if ($sc == 204) {
      return response(null, $sc);
    } else if ($pipeline->isNeedResponse) {
      return response()->json($pipeline->data, $sc);
    }
  }

  protected function enableOnlyOnPlatform($platformId, $platform = []) {
    if (!in_array($platformId, $platform)) {
      return $this->buildErrorsResponse(null, 400);
    }
  }
}
