<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Creditbill\CreditBillManager;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;

class CreditBillController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function createCreditBill() {
    list($isValid, $data) = $this->validateData([
      'amount.amount' => 'required|numeric',
      'amount.currency' => 'required',
      'biller' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(CartRemover::class, 'clear'));
    $this->pipeline->add(new Task(CreditBillManager::class, 'request', [
      'service_detail_id' => ServiceDetail::CREDIT_BILL_ODEO
    ]));

    $this->pipeline->add(new Task(CartInserter::class, 'addToCart', ['service_detail_id' => ServiceDetail::CREDIT_BILL_ODEO]));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    $pipeline2 = new Pipeline;

    $data = array_merge($this->pipeline->data, $data);

    $pipeline2->add(new Task(CartCheckouter::class, 'checkout', [
      'signature' => Platform::generateCartSignature($data)
    ]));

    $pipeline2->enableTransaction();
    $pipeline2->execute($data);

    if ($pipeline2->fail()) {
      return $this->buildErrorsResponse($pipeline2->errorMessage);
    }

    return $this->buildSuccessResponse([
      'order_id' => $pipeline2->data['order_id']
    ]);

  }


}