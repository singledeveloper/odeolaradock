<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Account\TokenManager;

class DeviceController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function registerNewDeviceToken() {
    list($isValid, $data) = $this->validateData([
      'device_token' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(TokenManager::class, 'registerDevice'));
    return $this->executeAndResponse($data);
  }

  public function removeDeviceToken() {
    list($isValid, $data) = $this->validateData([
      'device_token' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(TokenManager::class, 'removeDevice'));
    return $this->executeAndResponse($data);
  }

  public function devices() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(TokenManager::class, 'getDevices'));
    return $this->executeAndResponse($data);
  }

  public function remove() {
    $data = $this->getRequestData();
    
    $this->pipeline->add(new Task(TokenManager::class, 'removeDeviceById'));
    return $this->executeAndResponse($data);
  }

  public function removeAll() {
    $data = $this->getRequestData();
    $data = array_merge($data, getAuthorizationBearer());

    $this->pipeline->add(new Task(TokenManager::class, 'removeAllDevice'));
    return $this->executeAndResponse($data);
  }

}
