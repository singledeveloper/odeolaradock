<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Doku\Creditcard\AddCardSubmitter;
use Odeo\Domains\Payment\PaymentUserDirectCreator;
use Odeo\Domains\Payment\PaymentUserDirectSelector;
use Odeo\Domains\Payment\PaymentUserDirectUpdater;
use Odeo\Domains\Payment\PaymentInformationSelector;

class DirectPaymentController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getDirectPaymentList() {

    $data = $this->getRequestData();

    $this->pipeline->add(new Task(PaymentUserDirectSelector::class, 'get'));

    return $this->executeAndResponse($data);

  }

  public function getDirectPayments() {

    $data = $this->getRequestData();

    $this->pipeline->add(new Task(PaymentInformationSelector::class, 'getDirect'));

    return $this->executeAndResponse($data);

  }

  public function addNewDirectPayment() {

    $data = $this->getRequestData();

    $this->pipeline->add(new Task(PaymentUserDirectCreator::class, 'create'));
    $this->pipeline->enableTransaction();

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    if (isset($this->pipeline->data['view_page'])) {
      return view($this->pipeline->data['view_page'], $this->pipeline->data['view_data']);
    }

    return $this->buildResponse(200, $this->pipeline->data);
  }

  public function deleteDirectPayment() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(PaymentUserDirectUpdater::class, 'delete'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    return $this->buildResponse(200, $this->pipeline->data);
  }

  public function paymentDirectDokuSubmit() {

    $data = $this->getRequestData();

    $data['opc_group'] = $data['opc'];
    $data['reference_id'] = substr($data['doku_invoice_no'], strrpos($data['doku_invoice_no'], '_') + 1);
    unset($data['opc']);

    $this->pipeline->add(new Task(AddCardSubmitter::class, 'submit'));

    $this->pipeline->enableTransaction();

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return response()->json($this->pipeline->errorMessage['additional'] ?? [], 200);
    }
    return response()->json($this->pipeline->data['additional'], $this->pipeline->statusCode);
  }

  public function updateUserDirectPaymentPreferences() {

    list($isValid, $data) = $this->validateData([
      'id' => 'required',
      'opc_group' => 'required',
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PaymentUserDirectUpdater::class, 'updateUserDirectPaymentPreferences'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }


}
