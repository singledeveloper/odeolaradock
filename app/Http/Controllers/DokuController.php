<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 10/29/16
 * Time: 8:54 PM
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Doku\Creditcard\Selector as CreditcardSelector;
use Odeo\Domains\Payment\Doku\Dokuwallet\Selector as DokuwalletSelector;
use Odeo\Domains\Payment\Doku\Vagroup\Selector as AlfagroupSelector;

class DokuController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getAllCreditCard() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(CreditcardSelector::class, 'get'));

    return $this->executeAndResponse($data);

  }

  public function getAllAlfaGroup() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(AlfagroupSelector::class, 'get'));

    return $this->executeAndResponse($data);

  }

  public function getAllDokuWallet() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(DokuwalletSelector::class, 'get'));

    return $this->executeAndResponse($data);

  }

}
