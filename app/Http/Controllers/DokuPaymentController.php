<?php

namespace Odeo\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Core\Task;
use Odeo\Domains\OAuth2\Helper\ResponseHelper;
use Odeo\Domains\Payment\Doku\Helper\DokuLibrary;
use Odeo\Domains\Payment\Doku\VaDirect\Inquirer;
use Odeo\Domains\Payment\Doku\VaDirect\InquirySelector;
use Odeo\Domains\Payment\Doku\VaDirect\Payer;
use Odeo\Domains\Payment\Doku\VaDirect\PaymentSelector;
use Odeo\Domains\Payment\Doku\VaDirect\Repository\PaymentDokuCallbackLogRepository;
use Odeo\Domains\Payment\Doku\VaDirect\Validator;
use Odeo\Domains\Payment\PaymentVaDirectInquirer;
use Odeo\Domains\PaymentGateway\PaymentGatewayChannelValidator;
use Odeo\Domains\PaymentGateway\PaymentGatewayInvoiceNotifier;
use Odeo\Domains\PaymentGateway\PaymentGatewayInvoiceSelector;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;

class DokuPaymentController extends PaymentGatewayInvoiceController {

  public function __construct() {
    parent::__construct();
    $this->dokuCallbackLogRepo = app()->make(PaymentDokuCallbackLogRepository::class);
    $this->library = app()->make(DokuLibrary::class);
  }

  private function logCallback($type, $body) {
    $log = $this->dokuCallbackLogRepo->getNew();
    $log->type = $type;
    $log->body = $body;
    $this->dokuCallbackLogRepo->save($log);
  }

  public function vaInquiry(Request $request) {
    $data = $request->all();
    
    if ($this->isInvoiceRequest($data['PAYMENTCODE'])) {
      return $this->invoiceInquiry($data);
    }
    
    $this->logCallback('va_inquiry', $request->getContent());

    $data['virtual_account_number'] = $data['PAYMENTCODE'];

    $this->pipeline->add(new Task(PaymentGatewayChannelValidator::class, 'validateDokuVendor'));
    $this->pipeline->add(new Task(Inquirer::class, 'vaInquiry'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAInquiry'));
    $this->pipeline->add(new Task(Inquirer::class, 'createInquiry'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      clog('doku_va', $this->pipeline->errorMessage);
      $response = [
        'PAYMENTCODE' => $data['va_code'] ?? '',
        'AMOUNT' => $data['amount'] ?? '',
        'PURCHASEAMOUNT' => $data['amount'] ?? '',
        'TRANSIDMERCHANT' => $data['trans_id_merchant'] ?? '',
        'WORDS' => $data['words'] ?? '',
        'REQUESTDATETIME' => $data['request_date_time'] ?? '',
        'CURRENCY' => 360,
        'PURCHASECURRENCY' => 360,
        'SESSIONID' => $data['session_id'] ?? '',
        'NAME' => $data['customer_name'] ?? '',
        'EMAIL' => 'pg@odeo.co.id',
        'BASKET' => $data['basket'] ?? '',
        'ADDITIONALDATA' => '',
        'RESPONSECODE' => '9999'
      ];
    } else {
      $data = $this->pipeline->data;
      $response = [
        'PAYMENTCODE' => $data['va_code'],
        'AMOUNT' => $data['amount'],
        'PURCHASEAMOUNT' => $data['amount'],
        'TRANSIDMERCHANT' => $data['trans_id_merchant'],
        'WORDS' => $data['words'],
        'REQUESTDATETIME' => $data['request_date_time'],
        'CURRENCY' => 360,
        'PURCHASECURRENCY' => 360,
        'SESSIONID' => $data['session_id'],
        'NAME' => $data['customer_name'],
        'EMAIL' => 'pg@odeo.co.id',
        'BASKET' => $data['basket'],
        'ADDITIONALDATA' => $data['display_text'],
        'RESPONSECODE' => '0000'
      ];
    }

    return response(ResponseHelper::parseResponseToXml($response, 'INQUIRY_RESPONSE'))->header('Content-Type', 'application/xml');
  }

  private function invoiceInquiry($data) {
    $invoiceLog = $this->logInvoiceRequest('invoice_inquiry', VirtualAccount::VENDOR_DOKU_ALFA, json_encode($data));

    $data['virtual_account_number'] = $data['PAYMENTCODE'];
    $data['vendor_code'] = VirtualAccount::VENDOR_DOKU_ALFA;

    $this->pipeline->add(new Task(Validator::class, 'validate'));
    $this->pipeline->add(new Task(PaymentVaDirectInquirer::class, 'inquiry'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      clog('doku_va', json_encode($this->pipeline->errorMessage));
      $response = [
        'PAYMENTCODE' => $data['va_code'] ?? '',
        'AMOUNT' => $data['amount'] ?? '',
        'PURCHASEAMOUNT' => $data['amount'] ?? '',
        'TRANSIDMERCHANT' => $data['trans_id_merchant'] ?? '',
        'WORDS' => $data['words'] ?? '',
        'REQUESTDATETIME' => $data['request_date_time'] ?? '',
        'CURRENCY' => 360,
        'PURCHASECURRENCY' => 360,
        'SESSIONID' => $data['session_id'] ?? '',
        'NAME' => $data['customer_name'] ?? '',
        'EMAIL' => 'pg@odeo.co.id',
        'BASKET' => $data['basket'] ?? '',
        'ADDITIONALDATA' => 'Tagihan tidak ditemukan atau sudah dibayarkan',
        'RESPONSECODE' => '9999'
      ];
    } else {
      $result = $this->pipeline->data;
      $this->updateInvoiceLog($invoiceLog, $result['order_id']);

      $response = [
        'PAYMENTCODE' => $data['PAYMENTCODE'],
        'AMOUNT' => $result['amount'],
        'PURCHASEAMOUNT' => $result['amount'],
        'TRANSIDMERCHANT' => 'OID' . $result['order_id'],
        'WORDS' => $this->library->doCreateWords([
          'amount' => $result['amount'],
          'invoice' => $result['order_id']
        ]),
        'REQUESTDATETIME' => Carbon::now()->format('YmdHis'),
        'CURRENCY' => 360,
        'PURCHASECURRENCY' => 360,
        'SESSIONID' => $result['reference'],
        'NAME' => $result['customer_name'],
        'EMAIL' => 'pg@odeo.co.id',
        'BASKET' => $result['item_name'] . ',' . $result['amount'] . ',1,' . $result['amount'],
        'ADDITIONALDATA' => $result['description'],
        'RESPONSECODE' => '0000'
      ];
    }

    return response(ResponseHelper::parseResponseToXml($response, 'INQUIRY_RESPONSE'))->header('Content-Type', 'application/xml');
  }

  public function vaPayment(Request $request) {
    $data = $request->all();
    
    if ($this->isInvoiceRequest($data['PAYMENTCODE'])) {
      return $this->invoicePayment($data);
    }
    
    $this->logCallback('va_payment', $request->getContent());
    $data['virtual_account_number'] = $data['PAYMENTCODE'];

    $this->pipeline->add(new Task(PaymentGatewayChannelValidator::class, 'validateDokuVendor'));
    $this->pipeline->add(new Task(Payer::class, 'vaPayment'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAPayment'));
    $this->pipeline->add(new Task(Payer::class, 'updatePayment'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      clog('doku_va', serialize($this->pipeline->data));
      clog('doku_va', $this->pipeline->errorMessage);
      return response('ERROR', 400);
    }

    return response('CONTINUE', 200);
  }

  private function invoicePayment($data) {
    $invoiceLog = $this->logInvoiceRequest('invoice_payment', VirtualAccount::VENDOR_DOKU_ALFA, json_encode($data));
    $data['virtual_account_number'] = $data['PAYMENTCODE'];
    $data['amount'] = $data['AMOUNT'];
    $data['vendor_code'] = VirtualAccount::VENDOR_DOKU_ALFA;

    $this->pipeline->add(new Task(Validator::class, 'validate'));
    $this->pipeline->add(new Task(PaymentGatewayInvoiceSelector::class, 'getDetailByVaNo'));
    $this->pipeline->add(new Task(PaymentGatewayInvoiceNotifier::class, 'notify'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      clog('doku_va', json_encode($this->pipeline->errorMessage));
      return response('ERROR', 400);
    }

    $result = $this->pipeline->data;

    $this->updateInvoiceLog($invoiceLog, $result['order_id']);
    return response('CONTINUE', 200);
  }

  public function checkStatusTest(Request $request) {
    if(isProduction()) {
      return response('', 404);
    }

    $response = [
      'RESULTMSG' => 'SUCCESS',
      'RESPONSECODE' => '0000'
    ];

    return response(ResponseHelper::parseResponseToXml($response, 'INQUIRY_RESPONSE'))->header('Content-Type', 'application/xml');
  }

  public function getAllVaPayment() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(PaymentSelector::class, 'getAll'));
    return $this->executeAndResponse($data);
  }

  public function getAllVaInquiry() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(InquirySelector::class, 'getAll'));
    return $this->executeAndResponse($data);
  }

}
