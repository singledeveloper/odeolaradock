<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\EMoney\EMoneyManager;

class EMoneyController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function searchNominal() {
    list($isValid, $data) = $this->validateData([
      'default_price' => 'required_without_all:service_detail_id,store_id',
      'service_detail_id' => 'required_unless:default_price,true',
      'store_id' => 'required_unless:default_price,true|exists:stores,id'
    ]);

    if (!$isValid) return $data;

    if (isset($data['default_price']) && $data['default_price']
      || (isset($data['service_detail_id']) && $data['service_detail_id'] == 'undefined')) {
      $data['service_detail_id'] = ServiceDetail::EMONEY_ODEO;
    }

    $this->pipeline->add(new Task(EMoneyManager::class, 'searchNominal'));

    return $this->executeAndResponse($data);

  }

  public function getCategories() {
    $data = $this->getRequestData();
    $data['service_detail_id'] = ServiceDetail::EMONEY_ODEO;
    $this->pipeline->add(new Task(EMoneyManager::class, 'getAllCategoryByServiceDetailId'));
    return $this->executeAndResponse($data);
  }

  public function getAmounts() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required|exists:stores,id',
      'category' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['service_detail_id'] = ServiceDetail::EMONEY_ODEO;

    $this->pipeline->add(new Task(EMoneyManager::class, 'get'));

    return $this->executeAndResponse($data);
  }

}
