<?php

namespace Odeo\Http\Controllers;


use Odeo\Domains\Account\Helper\AdminChecker;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Faq\FaqInserter;
use Odeo\Domains\Faq\FaqRemover;
use Odeo\Domains\Faq\FaqSelector;
use Odeo\Domains\Faq\FaqUpdater;

class FaqController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function get() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(FaqSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function insert() {
    list($isValid, $data) = $this->validateData([
      'question' => 'required',
      'answer' => 'required',
      'slug' => 'required|max:60|alpha_dash',
      'priority' => 'unique:faqs,priority'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(FaqInserter::class, 'insert'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function remove() {

    $data = $this->getRequestData();

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(FaqRemover::class, 'remove'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function update($faqId) {
    list($isValid, $data) = $this->validateData([
      'question' => 'required',
      'answer' => 'required',
      'slug' => 'required|max:60|alpha_dash',
    ]);
    if (!$isValid) return $data;

    $data['faq_id'] = $faqId;
    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(FaqUpdater::class, 'update'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function updatePriority($faqId) {
    $data = $this->getRequestData();
    $data['faq_id'] = $faqId;
    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(FaqUpdater::class, 'updatePriority'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

}