<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 26-Jan-18
 * Time: 6:32 PM
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\FavoriteNumberCreator;
use Odeo\Domains\Account\FavoriteNumberRemover;
use Odeo\Domains\Account\FavoriteNumberSelector;
use Odeo\Domains\Core\Task;

class FavoriteNumberController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getAll() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(FavoriteNumberSelector::class, 'getAll'));

    return $this->executeAndResponse($data);
  }

  public function getByServiceId($serviceId) {
    $data = $this->getRequestData();
    $data['service_id'] = $serviceId;
    
    $this->pipeline->add(new Task(FavoriteNumberSelector::class, 'getByServiceId'));

    return $this->executeAndResponse($data);
  }

  public function create() {

    list($isValid, $data) = $this->validateData([
      'name' => 'required',
      'number' => 'required',
      'service_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(FavoriteNumberCreator::class, 'create'));

    return $this->executeAndResponse($data);
  }

  public function remove() {

    list($isValid, $data) = $this->validateData([
      'ids' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(FavoriteNumberRemover::class, 'remove'));

    return $this->executeAndResponse($data);
  }
}
