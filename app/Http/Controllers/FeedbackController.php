<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\FeedbackSelector;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Account\FeedbackRequester;

class FeedbackController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function createFeedback() {
    list($isValid, $data) = $this->validateData([
      'message' => 'required',
      'email' => 'email',
      'phone_number' => 'regex:/^[1-9][0-9]*$/|min:7|max:15'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(FeedbackRequester::class, 'create'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function getAll() {

    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(FeedbackSelector::class, 'getAll'));

    return $this->executeAndResponse($data);
  }

}
