<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Flight\FlightManager;

class FlightController extends Controller {


  private $flightManager;

  public function __construct() {

    parent::__construct();

    $this->flightManager = app()->make(\Odeo\Domains\Inventory\Flight\FlightManager::class);

  }

  public function index() {
    list($isValid, $data) = $this->validateData([
      'service_detail_id' => 'required'
    ]);

    if (!$isValid) return $data;
    
    $this->pipeline->add(new Task(FlightManager::class, 'getAirport'));

    return $this->executeAndResponse($data);
    
  }


  public function searchFlight() {
    
    list($isValid, $data) = $this->validateData([
      'service_detail_id' => 'required',
      'store_id' => 'required|exists:stores,id',
      "adult" => 'required',
      "child" => 'required',
      "infant" => 'required',
      "date" => "required",
      "destination" => "required",
      "origin" => "required"
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(FlightManager::class, 'searchFlight'));

    return $this->executeAndResponse($data);

  }


  public function getBookingForm() {
    
    list($isValid, $data) = $this->validateData([
      'service_detail_id' => 'required',
      'store_id' => 'required|exists:stores,id',
      "date" => "required",
      "flight_id" => "required"
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(FlightManager::class, 'getBookingForm'));

    return $this->executeAndResponse($data);

  }

}
