<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaH2HBroadcaster;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaH2HEditor;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaH2HSelector;

class H2HController extends Controller {

  public function getGroupList() {
    $this->pipeline->add(new Task(PulsaH2HSelector::class, 'getGroupList'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getGroupDetails($groupId) {
    $data = $this->getRequestData();
    $data['h2h_group_id'] = $groupId;
    $this->pipeline->add(new Task(PulsaH2HSelector::class, 'getGroupDetails'));
    return $this->executeAndResponse($data);
  }

  public function addGroup() {
    list($isValid, $data) = $this->validateData([
      'h2h_group_name' => 'required',
      'h2h_group_description' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PulsaH2HEditor::class, 'addGroup'));
    return $this->executeAndResponse($data);
  }

  public function editGroup($groupId) {
    list($isValid, $data) = $this->validateData([
      'h2h_group_name' => 'required',
      'h2h_group_description' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['h2h_group_id'] = $groupId;

    $this->pipeline->add(new Task(PulsaH2HEditor::class, 'editGroup'));
    return $this->executeAndResponse($data);
  }

  public function addUser($groupId) {
    list($isValid, $data) = $this->validateData([
      'user_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $data['h2h_group_id'] = $groupId;

    $this->pipeline->add(new Task(PulsaH2HEditor::class, 'addUser'));
    return $this->executeAndResponse($data);
  }

  public function removeUser($groupId) {
    list($isValid, $data) = $this->validateData([
      'h2h_user_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PulsaH2HEditor::class, 'removeUser'));
    return $this->executeAndResponse($data);
  }

  public function addGroupDetail($groupId) {
    list($isValid, $data) = $this->validateData([
      'pulsa_odeo_id' => 'required',
      'h2h_price' => 'required'
    ]);

    if (!$isValid) return $data;
    $data['h2h_group_id'] = $groupId;

    $this->pipeline->add(new Task(PulsaH2HEditor::class, 'addGroupDetail'));
    return $this->executeAndResponse($data);
  }

  public function removeGroupDetail($groupId) {
    list($isValid, $data) = $this->validateData([
      'h2h_group_detail_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PulsaH2HEditor::class, 'removeGroupDetail'));
    return $this->executeAndResponse($data);
  }

  public function broadcastFromGroup($groupId) {
    $data = $this->getRequestData();
    $data['h2h_group_id'] = $groupId;

    $this->pipeline->add(new Task(PulsaH2HBroadcaster::class, 'groupBroadcast'));
    return $this->executeAndResponse($data);
  }

}
