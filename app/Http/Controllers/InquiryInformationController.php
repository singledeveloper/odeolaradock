<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/12/17
 * Time: 12.28
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Accounting\InquiryInformation\InquiryInformationFileImageSelector;
use Odeo\Domains\Accounting\InquiryInformation\InquiryInformationFileImageRemover;
use Odeo\Domains\Accounting\InquiryInformation\InquiryInformationSelector;
use Odeo\Domains\Core\Task;

class InquiryInformationController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getInquiryInformations() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(InquiryInformationSelector::class, 'getInquiryInformations'));
    return $this->executeAndResponse($data);
  }

  public function getInquiryInformationFileImages() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(InquiryInformationFileImageSelector::class, 'getInquiryInformationFileImages'));
    return $this->executeAndResponse($data);
  }

  public function getInquiryInformationDetails($bank, $reffId) {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $data['bank'] = $bank;
    $data['reference_id'] = $reffId;
    $this->pipeline->add(new Task(InquiryInformationFileImageSelector::class, 'getInquiryInformationDetail'));
    return $this->executeAndResponse($data);
  }

  public function removeDetail($id) {
    $data  = $this->getRequestData();
    $this->parseQuerySearch($data);
    $data['id'] = $id;
    $this->pipeline->add(new Task(InquiryInformationFileImageRemover::class, 'remove'));
    return $this->executeAndResponse($data);
  }

}