<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/7/16
 * Time: 11:41 PM
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Adapter\Jobs\ExecuteDisbursement;
use Odeo\Domains\Disbursement\Adapter\Reserve;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementInquiryRequester;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementInquiryValidator;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementRequester;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementValidator;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Domains\Inventory\UserInvoice\UserInvoiceManager;
use Odeo\Domains\Order\OrderGuard;
use Odeo\Domains\Order\OrderCanceller;
use Odeo\Domains\Payment\Jobs\DevAutoSuccessVAPayment;
use Odeo\Domains\Payment\PaymentVaDirectSelector;

class InternalController extends Controller {

  private $disbursementApiUserRepo;

  public function __construct() {
    parent::__construct();
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
  }

  public function enqueue() {
    $data = $this->getRequestData();
    $class = new \ReflectionClass($data['class_name']);
    $instance = $class->newInstanceArgs($data['args']);
    dispatch($instance);
    return $this->buildSuccessResponse([], 201);
  }

  public function createDisbursement()
  {
    list($isValid, $data, $errors) = $this->validateData([
      'user_id' => 'required',
      'bank_id' => 'required',
      'amount' => 'required',
      'account_number' => 'required|numeric',
      'customer_name' => 'required',
      'reference_id' => 'required',
    ]);
    if (!$isValid) {
      return $this->buildResponse(400, [
        'errors' => [
          'error_code' => '10001',
          'message' => $errors[0],
        ]
      ]);
    }

    if (!isset($data['customer_email'])) {
      $data['customer_email'] = '';
    }

    if (!isset($data['description'])) {
      $data['description'] = '';
    }

    $apiUser = $this->disbursementApiUserRepo->findByUserId($data['user_id']);
    $data['auth'] = [
      'user_id' => $data['user_id'],
      'api_user' => $apiUser,
    ];

    $this->pipeline->add(new Task(ApiDisbursementValidator::class, 'validate'));
    $this->pipeline->add(new Task(ApiDisbursementRequester::class, 'request'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function reserveDisbursement()
  {
    list($isValid, $data) = $this->validateData([
      'disbursement_id' => 'required',
    ]);
    if (!$isValid) {
      return $data;
    }

    $this->pipeline->add(new Task(Reserve::class, 'reserve'));
    return $this->executeAndResponse($data);
  }

  public function executeReservedDisbursement()
  {
    list($isValid, $data) = $this->validateData([
      'disbursement_id' => 'required',
    ]);
    if (!$isValid) {
      return $data;
    }

    dispatch(new ExecuteDisbursement($data['disbursement_id']));
    return $this->buildResponse(200);
  }

  public function createBankAccountInquiry()
  {
    list($isValid, $data, $errors) = $this->validateData([
      'user_id' => 'required',
      'bank_id' => 'required',
      'account_number' => 'required|numeric',
      'customer_name' => 'required',
    ]);
    if (!$isValid) {
      return $this->buildResponse(400, [
        'errors' => [
          'error_code' => '10001',
          'message' => $errors[0],
        ]
      ]);
    }

    $data['with_validation'] = $data['with_validation'] ?? false;
    
    if ($data['with_validation']) {
      $data['expand'] = 'validation';
    }

    $apiUser = $this->disbursementApiUserRepo->findByUserId($data['user_id']);
    $data['auth'] = [
      'user_id' => $data['user_id'],
      'api_user' => $apiUser,
    ];

    $this->pipeline->add(new Task(ApiDisbursementInquiryValidator::class, 'validate'));
    $this->pipeline->add(new Task(ApiDisbursementInquiryRequester::class, 'request'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function getOrCreateVA()
  {
    list($isValid, $data) = $this->validateData([
      'auth_user_id' => 'required',
      'user_name' => 'required',
      'user_id' => 'required',
      'biller_id' => 'nullable|integer',
      'service_detail_id' => 'required',
      'reference_id' => 'required',
      'amount' => 'required|integer',
    ]);
    if (!$isValid) {
      return $data;
    }

    $this->pipeline->add(new Task(PaymentVaDirectSelector::class, 'get', [
      'expand' => 'va_info',
    ]));

    return $this->executeAndResponse($data);
  }

  public function testCompleteVAPayment() {
    if (isProduction()) {
      return $this->buildResponse(400);
    }

    list($isValid, $data) = $this->validateData([
      'service_detail_id' => 'required',
      'reference_id' => 'required',
      'user_id' => 'required',
      'auth_user_id' => 'required',
    ]);

    if (!$isValid) {
      return $data;
    }

    dispatch(new DevAutoSuccessVAPayment([
      'service_detail_id' => $data['service_detail_id'],
      'reference_id' => $data['reference_id'],
      'user_id' => $data['user_id'],
      'auth_user_id' => $data['auth_user_id'],
    ]));

    return $this->buildResponse(200);
  }

  public function affiliateInvoiceInquiry() {
    list($isValid, $data) = $this->validateData([
      'phone_number' => 'required',
      'biller_id' => 'required',
      'user_id' => 'required',
      'terminal_id' => 'required',
    ]);
    if (!$isValid) return $data;

    $data['service_detail_id'] = ServiceDetail::USER_INVOICE_ODEO;
    $data['auth']['user_id'] = $data['user_id'];
    $this->pipeline->add(new Task(UserInvoiceManager::class, 'terminalInquiry'));

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    $result = $this->pipeline->data;

    unset($result['user_id']);

    return $this->buildResponse($this->pipeline->statusCode, $result);
  }

  public function cancelOrder() {
    list($isValid, $data) = $this->validateData([
      'order_id' => 'required',
      'auth_user_id' => 'required'
    ]);
    $data['auth']['user_id'] = $data['auth_user_id'];

    $this->pipeline->add(new Task(OrderGuard::class, 'validateUser'));
    $this->pipeline->add(new Task(OrderCanceller::class, 'cancel'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }
}