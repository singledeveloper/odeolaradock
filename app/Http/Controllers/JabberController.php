<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Supply\JabberStoreDetailizer;
use Odeo\Domains\Supply\JabberStoreEditor;
use Odeo\Domains\Vendor\Jabber\JabberSelector;
use Odeo\Domains\Vendor\Jabber\Registrar;

class JabberController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function register() {
    list($isValid, $data) = $this->validateData([
      'password' => 'required',
      'confirm_password' => 'required|same:password',
      'transaction_pin' => 'required',
      'confirm_transaction_pin' => 'required|same:transaction_pin'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Registrar::class, 'register'));
    return $this->executeAndResponse($data);
  }

  public function remove() {
    list($isValid, $data) = $this->validateData([
      'password' => 'required',
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Registrar::class, 'remove'));
    return $this->executeAndResponse($data);
  }

  public function changePassword() {
    list($isValid, $data) = $this->validateData([
      //'old_password' => 'required',
      'new_password' => 'required',
      'confirm_password' => 'required|same:new_password',
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Registrar::class, 'changePassword'));
    return $this->executeAndResponse($data);
  }

  public function changeTransactionPin() {
    list($isValid, $data) = $this->validateData([
      'new_transaction_pin' => 'required',
      'confirm_transaction_pin' => 'required|same:new_transaction_pin'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Registrar::class, 'changeTransactionPin'));
    return $this->executeAndResponse($data);
  }

  public function get() {
    $this->pipeline->add(new Task(JabberSelector::class, 'getDetail'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getList() {
    $this->pipeline->add(new Task(JabberSelector::class, 'getList'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getReplyExample() {
    $this->pipeline->add(new Task(JabberSelector::class, 'getReplyExamples'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getCommandExample() {
    $this->pipeline->add(new Task(JabberSelector::class, 'getCommandExamples'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getJabberStore($storeId) {
    $data = $this->getRequestData();
    $data['store_id'] = $storeId;
    $this->pipeline->add(new Task(JabberStoreDetailizer::class, 'getDetail'));
    return $this->executeAndResponse($data);
  }

  public function addJabberStore() {
    list($isValid, $data) = $this->validateData([
      'email' => 'required',
      'password' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(JabberStoreEditor::class, 'add'));
    return $this->executeAndResponse($data);
  }

  public function changeJabberStorePassword($storeId) {
    list($isValid, $data) = $this->validateData([
      'new_password' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(JabberStoreEditor::class, 'editPassword'));
    return $this->executeAndResponse($data);
  }

  public function editJabberStoreStatus($storeId) {
    list($isValid, $data) = $this->validateData([
      'status_message' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(JabberStoreEditor::class, 'editStatusMessage'));
    return $this->executeAndResponse($data);
  }

}