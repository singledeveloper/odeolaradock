<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 11/4/16
 * Time: 8:25 PM
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Kredivo\Selector as KredivoSelector;
use Odeo\Domains\Payment\Kredivo\VoidRequester;

class KredivoController extends Controller {

  public function __construct() {

    parent::__construct();

  }

  public function getAllKredivo() {

    $data = $this->getRequestData();

    $this->pipeline->add(new Task(KredivoSelector::class, 'get'));

    return $this->executeAndResponse($data);

  }

  public function voidLockedOcash() {
    $this->pipeline->add(new Task(VoidRequester::class, 'withdrawLockedRequestVoid'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData());
  }


}