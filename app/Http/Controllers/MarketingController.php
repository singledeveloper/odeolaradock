<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Marketing\BudgetConverter;
use Odeo\Domains\Marketing\QueueSelector;
use Odeo\Domains\Marketing\Scheduler\StoreRevenueRefresher;
use Odeo\Domains\Subscription\StoreSelector;

class MarketingController extends Controller {

  public function adsClick() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required|exists:stores,id',
      'ads_id' => 'required|exists:ads,id'
    ]);

    if (!$isValid) return $data;

    $request = app()->make(\Illuminate\Http\Request::class);

    $this->pipeline->add(new Task(BudgetConverter::class, 'ads', [
      'ip' => $request->server('REMOTE_ADDR'),
      'user_agent' => $request->server('HTTP_USER_AGENT')
    ]));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function getQueue() {
    list($isValid, $data) = $this->validateData([
      'service_id' => 'required|exists:services,id'
    ]);

    if (!$isValid) return $data;

    $data['use_preferred_store'] = !isNewDesign();

    $this->pipeline->add(new Task(QueueSelector::class, 'get'));
    $this->pipeline->add(new Task(StoreSelector::class, 'getStoreDetail', [
      'fields' => 'id,plan_id,current_data,name,logo_path,subdomain_name,domain_name,can_supply',
      'expand' => 'current_data',
      'current_data_set_default' => true
    ]));

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    $response = $this->pipeline->data;
    $response['service_detail_id'] = ServiceDetail::getIdFromCurrentData(array_merge(
      $response, $data
    ));

    return $this->buildSuccessResponse($response);

  }

  public function recalculateRevenue() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(\Odeo\Domains\Marketing\RevenueUpdater::class, 'recalculateRevenue'));
    return $this->executeAndResponse($data);
  }
}
