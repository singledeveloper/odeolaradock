<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/10/19
 * Time: 10.47
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Maybank\Va\Inquirer;
use Odeo\Domains\Payment\Maybank\Va\Payer;
use Odeo\Domains\Payment\Maybank\Va\Repository\PaymentMaybankCallbackLogRepository;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;

class MaybankPaymentController extends Controller {

  private $callbackRepo;

  function __construct() {
    parent::__construct();
    $this->callbackRepo = app()->make(PaymentMaybankCallbackLogRepository::class);
  }

  public function vaInquiry() {
    $data = $this->getRequestData();
    $this->logCallback('va_inquiry', json_encode($data));

    $this->pipeline->add(new Task(Inquirer::class, 'vaInquiry'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVaInquiry'));
    $this->pipeline->add(new Task(Inquirer::class, 'createInquiry'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $error = $this->pipeline->errorMessage;
      clog('maybank_va', serialize($error));
      return $this->responseError('inquiry', $error['code'], $error['message']);
    }

    $data = $this->pipeline->data;
    return response([
      'inquiryVAResponse' => [
        'responseCode' => '00',
        'responseMessage' => 'Success',
        'detailResponse' => [
          'vaName' => $data['customer_name'],
          'billingAmount' => $data['amount'],
          'currency' => '360',
          'reference' => $data['vendor_reference_id']
        ]
      ]
    ]);
  }

  public function vaPayment() {
    $data = $this->getRequestData();
    $this->logCallback('va_payment', json_encode($data));

    $this->pipeline->add(new Task(Payer::class, 'vaPayment'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAPayment'));
    $this->pipeline->add(new Task(Payer::class, 'updatePayment'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      clog('maybank_va', serialize($this->pipeline->errorMessage));
      $error = $this->pipeline->errorMessage;
//      return $this->responseError('payment', $error['code'], $error['message']);
    }

    return response([
      'paymentVAResponse' => [
        'responseCode' => '00',
        'responseMessage' => 'Success'
      ]
    ]);
  }

  private function logCallback($type, $body) {
    $log = $this->callbackRepo->getNew();
    $log->type = $type;
    $log->body = $body;
    $this->callbackRepo->save($log);
  }

  private function responseError($type, $errCode = '14', $message = 'Invalid Transaction') {
    return response([
      "{$type}VAResponse" => [
        'responseCode' => $errCode,
        'responseMessage' => $message
      ]
    ], 422);
  }

}