<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Subscription\MdPlus\MdPlusPlanSelector;
use Odeo\Domains\Subscription\MdPlus\MdPlusRequester;
use Odeo\Domains\Subscription\MdPlus\MdPlusSelector;
use Odeo\Domains\Subscription\MdPlus\ReferredUserSelector;
use Odeo\Domains\Subscription\StoreVerificator;

class MdPlusController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function index($storeId) {
    $data = $this->getRequestData();
    
    $data['store_id'] = $storeId;
    $data['expand'] = 'order';
    
    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'allow_mentor' => true,
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(MdPlusSelector::class, 'getDashboard'));
    
    return $this->executeAndResponse($data);
  }

  public function subscribe() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required|exists:stores,id',
      'md_plus_plan_id' => 'required|exists:md_plus_plans,id',
      'is_auto_renew' => 'required|boolean'
    ]);

    if (!$isValid) return $data;

    $data['store_must_active'] = true;
    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->add(new Task(MdPlusRequester::class, 'create'));
    $this->pipeline->add(new Task(CartRemover::class, 'clear'));
    $this->pipeline->add(new Task(CartInserter::class, 'addToCart', ['service_detail_id' => ServiceDetail::MD_PLUS_ODEO]));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function plans() {
    $this->pipeline->add(new Task(MdPlusPlanSelector::class, 'get'));

    return $this->executeAndResponse([]);
  }

  public function information() {
    $this->pipeline->add(new Task(MdPlusPlanSelector::class, 'getInformation'));

    return $this->executeAndResponse([]);
  }

  public function toggleAutoRenew($storeId) {
    $data = $this->getRequestData();
    
    $data['store_id'] = $storeId;
    
    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(MdPlusRequester::class, 'toggleAutoRenew'));
    
    return $this->executeAndResponse($data);
  }

  public function referredUsers($storeId) {
    $data = $this->getRequestData();
    
    $data['store_id'] = $storeId;
    $data['expand'] = 'user,referral_cashbacks';

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(ReferredUserSelector::class, 'get'));
    
    return $this->executeAndResponse($data);
  }
}
