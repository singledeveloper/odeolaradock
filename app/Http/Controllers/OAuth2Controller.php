<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\OAuth2\Helper\ResponseHelper;
use Odeo\Domains\OAuth2\OAuth2TokenRequestParser;
use Odeo\Domains\OAuth2\OAuth2TokenGenerator;

class OAuth2Controller extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function requestToken() {
    list($isValid, $data) = $this->validateData([
      'grant_type' => 'required'
    ]);

    if (!$isValid) {
      return response()->json([
          'message' => trans('oauth2.require_grant'),
          'status_code' => ResponseHelper::STATUS_ERROR,
          'error_code' => 10001
        ], 400);
    }

    $this->pipeline->add(new Task(OAuth2TokenRequestParser::class, 'parseClientIdAndClientSecret'));
    $this->pipeline->add(new Task(OAuth2TokenRequestParser::class, 'parseScope'));
    $this->pipeline->execute($data);
    if($this->pipeline->fail()) {
      return response()->json([
        'message' => $this->pipeline->errorMessage,
        'status_code' => ResponseHelper::STATUS_ERROR,
        'error_code' => 10000
      ], 400);
    }
    $data = array_merge($data, $this->pipeline->data);

    $this->pipeline = new Pipeline;
    $this->pipeline->add(new Task(OAuth2TokenGenerator::class, 'generate'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);
    return response()->json($this->pipeline->data, 200);
  }

}
