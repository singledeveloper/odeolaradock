<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\Reconciliation\OrderReconciliationSelector;
use Odeo\Domains\Order\Reconciliation\OrderReconciler;

class OrderReconciliationController extends Controller {

  public function reconciliation() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $data['sort_by'] = $data['sort_by'] ?? 'id';
    $data['sort_type'] = $data['sort_type'] ?? 'desc';

    $this->pipeline->add(new Task(OrderReconciliationSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function reconciliationSummary() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(OrderReconciliationSelector::class, 'getSummary'));

    return $this->executeAndResponse($data);
  }

  public function reconcileOrder() {
    list($isValid, $data) = $this->validateData([
      'order_id' => 'required',
      'diff_reason' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(OrderReconciler::class, 'manualRecon'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function reconcileOrderBatch() {
    $data = $this->getRequestData();

    $this->pipeline->add(new \Odeo\Domains\Core\Task(OrderReconciler::class, 'dispatchRecon'));

    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function recalculateReconciliation() {
    $data = $this->getRequestData();

    $this->pipeline->add(new \Odeo\Domains\Core\Task(OrderReconciler::class, 'recalculateData'));

    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }
}
