<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 21/10/19
 * Time: 15.23
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayInvoicePaymentLogRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayInvoicePaymentRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;

class PaymentGatewayInvoiceController extends Controller {

  protected $pgUserChannelRepo, $pgInvoicePaymentRepo, $invoicePaymentLogRepo;

  function __construct() {
    parent::__construct();
    $this->pgUserChannelRepo = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
    $this->pgInvoicePaymentRepo = app()->make(PaymentGatewayInvoicePaymentRepository::class);
    $this->invoicePaymentLogRepo = app()->make(PaymentGatewayInvoicePaymentLogRepository::class);
  }

  function isInvoiceRequest($vaCode) {
    if ($this->pgUserChannelRepo->findByActivePrefix($vaCode)) {
      return false;
    }
    return true;
  }

  function findInvoiceByReferenceAndAmount($vendor, $reference, $amount) {
    return $this->pgInvoicePaymentRepo->findByReferenceAndAmount($vendor, $reference, $amount);
  }

  function logInvoiceRequest($type, $vendor, $body) {
    $invoice = $this->invoicePaymentLogRepo->getNew();
    $invoice->type = $type;
    $invoice->vendor = $vendor;
    $invoice->log_request = $body;
    $this->invoicePaymentLogRepo->save($invoice);

    return $invoice;
  }

  function updateInvoiceLog($invoiceLog, $orderId) {
    $invoiceLog->order_id = $orderId;
    $this->invoicePaymentLogRepo->save($invoiceLog);
  }

}