<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 25/07/19
 * Time: 18.02
 */

namespace Odeo\Http\Controllers;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Core\Task;
use Odeo\Domains\OAuth2\Helper\ResponseHelper;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;
use Odeo\Domains\PaymentGateway\PaymentGatewayTester;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserRepository;

class PaymentGatewayTestController extends Controller {

  private $redis, $paymentGatewayUserRepo;

  public function __construct() {
    parent::__construct();
    $this->paymentGatewayUserRepo = app()->make(PaymentGatewayUserRepository::class);
    $this->redis = Redis::connection();
  }

  public function getTestPage($encodedClientId) {
    if (isProduction()) {
      return $this->buildErrorsResponse('', 404);
    }

    $clientId = base64_decode($encodedClientId);
    if ($payments = $this->paymentGatewayUserRepo->getActivePrefixesByClientId($clientId)) {
      return view('pg-test', [
        'payments' => $payments
      ]);
    }
    return '';
  }

  public function notifyInquiry() {
    if (isProduction()) {
      return;
    }

    list($isValid, $data) = $this->validateData([
      'virtual_account_number' => 'required|numeric',
    ]);

    if (!$isValid) {
      return $data;
    }

    $this->pipeline->add(new Task(PaymentGatewayTester::class, 'validateVirtualAccount'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAInquiry'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $response = [
        'vaName' => '',
        'responseCode' => '01',
        'responseMessage' => $this->pipeline->errorMessage,
        'billingAmount' => 0
      ];
    } else {
      $data = $this->pipeline->data;
      $response = [
        'vaName' => $data['customer_name'],
        'vaDesc' => $data['display_text'],
        'vaItemName' => $data['item_name'],
        'responseCode' => '00',
        'responseMessage' => 'Success',
        'billingAmount' => $data['amount'],
        'traceNo' => $data['vendor_reference_id']
      ];
    }

    return response()->json($response, ResponseHelper::parseResponseStatus($response));
  }

  public function notifyPayment() {

    list($isValid, $data) = $this->validateData([
      'virtual_account_number' => 'required|numeric',
      'amount' => 'required|numeric',
      'trace_no' => 'required'
    ]);

    if (!$isValid) {
      return $data;
    }

    $data['billed_amount'] = $data['amount'];

    $this->pipeline->add(new Task(PaymentGatewayTester::class, 'validateVirtualAccount'));
    $this->pipeline->add(new Task(PaymentGatewayTester::class, 'createPayment'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAPayment'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $response = [
        'responseCode' => '01',
        'responseMessage' => $this->pipeline->errorMessage,
      ];
    } else {
      $data = $this->pipeline->data;
      $response = [
        'responseCode' => '00',
        'responseMessage' => 'Success',
        'pgPaymentId' => $data['pg_payment_id']
      ];
    }

    return response()->json($response, ResponseHelper::parseResponseStatus($response));
  }

  public function notifyStatus() {
    list($isValid, $data) = $this->validateData([
      'pg_payment_id' => 'required',
      'pg_payment_status' => 'required',
      'trace_no' => 'required'
    ]);
    
    $data['receive_notify_time'] = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
    $data['vendor_reference_id'] = $data['trace_no'];
    
    if (!$isValid) {
      return $data;
    }

    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyPaymentStatus'));

    return $this->executeAndResponse($data);
  }

}