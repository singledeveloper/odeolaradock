<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 21/08/19
 * Time: 10.17
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Helper\PaymentInvoiceHelper;
use Odeo\Domains\Payment\PaymentVaDirectInquirer;
use Odeo\Domains\Payment\Permata\Va\Inquirer;
use Odeo\Domains\Payment\Permata\Va\InquirySelector;
use Odeo\Domains\Payment\Permata\Va\Payer;
use Odeo\Domains\Payment\Permata\Va\PaymentSelector;
use Odeo\Domains\Payment\Permata\Va\Repository\PaymentPermataCallbackLogRepository;
use Odeo\Domains\Payment\Permata\Va\Reverser;
use Odeo\Domains\PaymentGateway\PaymentGatewayChannelValidator;
use Odeo\Domains\PaymentGateway\PaymentGatewayInvoiceNotifier;
use Odeo\Domains\PaymentGateway\PaymentGatewayInvoiceSelector;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;

class PermataPaymentController extends PaymentGatewayInvoiceController {

  private $permataLogs;

  public function __construct() {
    parent::__construct();
    $this->permataLogs = app()->make(PaymentPermataCallbackLogRepository::class);
  }

  public function vaInquiry() {
    $data = $this->getRequestData();
    
    if (!$this->isIpWhitelist()) {
      return response([
        'GetBillRq' => [
          'CUSTNAME' => '',
          'BILL_AMOUNT' => 0,
          'VI_CCY' => 360,
          'STATUS' => '01'
        ]
      ]);
    }
        
    if ($this->isInvoiceRequest($data['GetBillRq']['VI_VANUMBER'])) {
      return $this->invoiceInquiry($data['GetBillRq']);
    }

    $this->logCallback(json_encode($data), 'va_inquiry');
    
    $data['virtual_account_number'] = $data['GetBillRq']['VI_VANUMBER'];
    $data['vendor_code'] = VirtualAccount::VENDOR_DIRECT_PERMATA;

    $this->pipeline->add(new Task(PaymentGatewayChannelValidator::class, 'validatePaymentVendor'));
    $this->pipeline->add(new Task(Inquirer::class, 'vaInquiry'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAInquiry'));
    $this->pipeline->add(new Task(Inquirer::class, 'createInquiry'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      clog('permata_va', 'error inquiry');
      clog('permata_va', json_encode($this->pipeline->errorMessage));
      $errCode = isset($this->pipeline->errorMessage['code']) ? $this->pipeline->errorMessage['code'] : '66';
      return response([
        'GetBillRq' => [
          'CUSTNAME' => '',
          'BILL_AMOUNT' => 0,
          'VI_CCY' => 360,
          'STATUS' => $errCode
        ]
      ]);
    }

    $data = $this->pipeline->data;
    return response(['GetBillRq' => [
      'CUSTNAME' => strtoupper($data['customer_name']),
      'BILL_AMOUNT' => $data['amount'],
      'VI_CCY' => '360',
      'STATUS' => '00'
    ]]);
  }

  private function invoiceInquiry($data) {
    $invoiceLog = $this->logInvoiceRequest('invoice_inquiry', VirtualAccount::VENDOR_DIRECT_PERMATA, json_encode($data));

    $data['virtual_account_number'] = $data['VI_VANUMBER'];
    $data['vendor_code'] = VirtualAccount::VENDOR_DIRECT_PERMATA;

    $this->pipeline->add(new Task(PaymentVaDirectInquirer::class, 'inquiry'));

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      clog('permata_invoice_va', 'error invoice inquiry');
      clog('permata_invoice_va', json_encode($this->pipeline->errorMessage));
      $errCode = $this->pipeline->errorMessage['error_code'] ?? 9999;
      switch ($errCode) {
        case PaymentInvoiceHelper::INVOICE_NOT_FOUND:
        case PaymentInvoiceHelper::INVOICE_INVALID:
          $errCode = '14';
          break;
        default:
          $errCode = '66';
          break;
      }

      return response([
        'GetBillRq' => [
          'CUSTNAME' => '',
          'BILL_AMOUNT' => 0,
          'VI_CCY' => 360,
          'STATUS' => $errCode
        ]
      ]);
    }

    $result = $this->pipeline->data;

    $this->updateInvoiceLog($invoiceLog, $result['order_id']);
    return response([
      'GetBillRq' => [
        'CUSTNAME' => strtoupper($result['customer_name']),
        'BILL_AMOUNT' => $result['amount'],
        'VI_CCY' => '360',
        'STATUS' => '00'
      ]
    ]);
  }

  public function vaPayment() {
    $data = $this->getRequestData();

    if (!$this->isIpWhitelist()) {
      return response([
        'PayBillRs' => [
          'STATUS' => '01'
        ]
      ]);
    }

    if ($this->isInvoiceRequest($data['PayBillRq']['VI_VANUMBER'])) {
      return $this->invoicePayment($data['PayBillRq']);
    }

    $this->logCallback(json_encode($data), 'va_payment');
    
    $data['virtual_account_number'] = $data['PayBillRq']['VI_VANUMBER'];
    $data['vendor_code'] = VirtualAccount::VENDOR_DIRECT_PERMATA;

    $this->pipeline->add(new Task(PaymentGatewayChannelValidator::class, 'validatePaymentVendor'));
    $this->pipeline->add(new Task(Payer::class, 'vaPayment'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAPayment'));
    $this->pipeline->add(new Task(Payer::class, 'updatePayment'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $error = $this->pipeline->errorMessage;
      clog('permata_va', 'error va payment');
      clog('permata_va', json_encode($error));
      $errCode = $error['error_code'] ?? '0';

      switch ($errCode) {
        case 3002:
        case 9999:
          $errCode = '14';
          break;
        default:
          $errCode = '66';
          break;
      }

      return response([
        'PayBillRs' => [
          'STATUS' => $errCode
        ]
      ]);
    }

    return response([
      'PayBillRs' => [
        'STATUS' => '00'
    ]]);
  }

  private function invoicePayment($data) {
    $invoiceLog = $this->logInvoiceRequest('invoice_payment', VirtualAccount::VENDOR_DIRECT_PERMATA, json_encode($data));
    $data['virtual_account_number'] = $data['VI_VANUMBER'];
    $data['vendor_code'] = VirtualAccount::VENDOR_DIRECT_PERMATA;
    $data['amount'] = $data['BILL_AMOUNT'];

    $this->pipeline->add(new Task(PaymentGatewayInvoiceSelector::class, 'getDetailByVaNo'));
    $this->pipeline->add(new Task(PaymentGatewayInvoiceNotifier::class, 'notify'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $error = $this->pipeline->errorMessage;
      clog('permata_invoice_va', 'error invoice payment');
      clog('permata_invoice_va', json_encode($error));
      $errCode = $error['error_code'] ?? '0';

      switch ($errCode) {
        case PaymentInvoiceHelper::INVOICE_NOT_FOUND:
        case PaymentInvoiceHelper::INVOICE_INVALID:
          $errCode = '14';
          break;
        case PaymentInvoiceHelper::INVOICE_INVALID_AMOUNT:
          $errCode = '13';
          break;
        default:
          $errCode = '66';
          break;
      }

      return response([
        'PayBillRs' => [
          'STATUS' => $errCode
        ]
      ]);
    }

    $orderId = $this->pipeline->data['order_id'];

    $this->updateInvoiceLog($invoiceLog, $orderId);
    return response([
      'PayBillRs' => [
        'STATUS' => '00'
      ]
    ]);
  }

  public function vaReversal() {
    $data = $this->getRequestData();
    $this->logCallback(json_encode($data), 'va_reverse');

    if (!$this->isIpWhitelist()) {
      return response([
        'RevBillRs' => [
         'STATUS' => '01'
        ]
      ]);
    }

    $this->pipeline->add(new Task(Reverser::class, 'vaReverse'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $errCode = $this->getErrorCode($this->pipeline->errorMessage);
      return response([
        'RevBillRs' => [
          'STATUS' => $errCode
        ]
      ]);
    }

    return response([
      'RevBillRs' => [
        'STATUS' => '00'
      ]
    ]);
  }

  public function getAllVaPayment() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(PaymentSelector::class, 'getAll'));
    return $this->executeAndResponse($data);
  }

  public function getAllVaInquiry() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(InquirySelector::class, 'getAll'));
    return $this->executeAndResponse($data);
  }

  private function isIpWhitelist() {
    if (isProduction()) return empty(env('PERMATA_WHITELIST_IP')) || in_array(getClientIP(), explode(',', env('PERMATA_WHITELIST_IP')));
    return true;
  }

  private function logCallback($body, $type) {
    $log = $this->permataLogs->getNew();
    $log->body = $body;
    $log->type = $type;
    $this->permataLogs->save($log);
  }

  private function getErrorCode($error) {
    return isset($error['error_code']) ? $error['error_code'] : '66';
  }

}
