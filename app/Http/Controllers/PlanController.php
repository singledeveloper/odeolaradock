<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Subscription\PlanSelector;

class PlanController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getPlans() {
    $this->pipeline->add(new Task(PlanSelector::class, 'getPlan'));

    return $this->executeAndResponse($this->getRequestData());
  }

  public function getMarketingPlans() {
    $data = $this->getRequestData();

    $pages = [];
    // $disabled_pages = ["015", "017"];
    // $marketing_plan_pages = ["051", "052", "053", "054", "055", "056", "057", "058", "059"];

    for ($x = 1; $x <= 30; $x++) {
      $page_id = sprintf("%03d", $x);

      // if (in_array($page_id, $disabled_pages)) {
      //   continue;
      // }

      // if (!in_array($page_id, $marketing_plan_pages) || (isset($data["network_enable"]) && $data["network_enable"])) {
      $pages[] = baseURL() . "images/marketing_plan/telemarketing v1.0-page-" . $page_id . ".jpg";
      // }
    }

    return $this->buildSuccessResponse($pages)
      ->header('Cache-control', 'max-age=43200, public');

  }
}
