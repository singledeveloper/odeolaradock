<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 4/27/17
 * Time: 7:20 PM
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\Helper\CustomerEmailManager;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\PriceListRequester;

class PriceListController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function sendPriceList($storeId = null) {
    $data = $this->getRequestData();
    $data['store_id'] = $storeId;
    $this->pipeline->add(new Task(CustomerEmailManager::class, 'checkIsEmailVerified'));
    $this->pipeline->add(new Task(PriceListRequester::class, 'sendPriceList'));
    return $this->executeAndResponse($data);
  }

  public function getPriceList($storeId = null) {
    $data = $this->getRequestData();
    $data['store_id'] = $storeId;
    $this->pipeline->add(new Task(PriceListRequester::class, 'getPriceList'));
    return $this->executeAndResponse($data);
  }
}
