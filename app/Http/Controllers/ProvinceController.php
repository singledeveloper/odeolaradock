<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/28/17
 * Time: 5:30 PM
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Account\CitySelector;
use Odeo\Domains\Account\ProvinceSelector;
use Odeo\Domains\Core\Task;

class ProvinceController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function get() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(ProvinceSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function getCities($provinceId) {
    $data = $this->getRequestData();
    $data['province_id'] = $provinceId;
    $this->pipeline->add(new Task(CitySelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

}