<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Pulsa\PulsaManager;

class PulsaController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getTnC() {
    $this->pipeline->add(new Task(PulsaManager::class, 'getTermAndCon'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getList() {
    $this->pipeline->add(new Task(PulsaManager::class, 'getList'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getPostpaidList() {
    $this->pipeline->add(new Task(PulsaManager::class, 'getPostpaidList'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function searchNominal() {
    list($isValid, $data) = $this->validateData([
      'default_price' => 'required_without_all:service_detail_id,store_id',
      'service_detail_id' => 'required_unless:default_price,true',
      'store_id' => 'required_unless:default_price,true|integer|exists:stores,id',
      'operator_id' => 'required_without_all:operator_code,prefix',
      'operator_code' => 'required_without_all:operator_id,prefix',
      'prefix' => 'required_without_all:operator_id,operator_code'
    ]);

    if (!$isValid) return $data;

    if (isset($data['default_price']) && $data['default_price']) {
      $data['service_detail_id'] = ServiceDetail::PULSA_ODEO;
    }

    $data['category'] = PulsaCode::groupPulsa();
    $this->pipeline->add(new Task(PulsaManager::class, 'searchNominal'));

    return $this->executeAndResponse($data);
  }


  public function searchNominalData() {
    list($isValid, $data) = $this->validateData([
      'default_price' => 'required_without_all:service_detail_id,store_id',
      'service_detail_id' => 'required_unless:default_price,true',
      'store_id' => 'required_unless:default_price,true|exists:stores,id',
      'operator_id' => 'required_without_all:operator_code,prefix',
      'operator_code' => 'required_without_all:operator_id,prefix',
      'prefix' => 'required_without_all:operator_id,operator_code'
    ]);

    if (!$isValid) return $data;

    if (isset($data['default_price']) && $data['default_price']) {
      $data['service_detail_id'] = ServiceDetail::PAKET_DATA_ODEO;
    }

    $data['category'] = PulsaCode::groupData();
    $this->pipeline->add(new Task(PulsaManager::class, 'searchNominal'));

    return $this->executeAndResponse($data);
  }

  public function searchNominalSmsTelpPackages() {
    list($isValid, $data) = $this->validateData([
      'default_price' => 'required_without_all:service_detail_id,store_id',
      'service_detail_id' => 'required_unless:default_price,true',
      'store_id' => 'required_unless:default_price,true|exists:stores,id',
      'operator_id' => 'required_without_all:operator_code,prefix',
      'operator_code' => 'required_without_all:operator_id,prefix',
      'prefix' => 'required_without_all:operator_id,operator_code'
    ]);

    if (!$isValid) return $data;

    if (isset($data['default_price']) && $data['default_price']) {
      $data['service_detail_id'] = ServiceDetail::PULSA_ODEO;
    }

    $data['category'] = PulsaCode::groupSmsTelephonePackages();
    $this->pipeline->add(new Task(PulsaManager::class, 'searchNominal'));

    return $this->executeAndResponse($data);
  }

  public function getBalance() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(PulsaManager::class, 'getBalance'));
    return $this->executeAndResponse($data);
  }

  public function getPostpaid() {

    list($isValid, $data) = $this->validateData([
      'default_price' => 'required_without_all:service_detail_id,store_id',
      'service_detail_id' => 'required_unless:default_price,true',
      'store_id' => 'required_unless:default_price,true|exists:stores,id',
      'number' => 'required',
      'postpaid_type' => 'required'
    ]);

    if (!$isValid) return $data;

    if (isset($data['default_price']) && $data['default_price']) {
      $data['service_detail_id'] = ServiceDetail::PULSA_POSTPAID_ODEO;
    }

    $this->pipeline->add(new Task(PulsaManager::class, 'validatePostpaidInventory', ["for_bill" => true]));
    return $this->executeAndResponse($data);
  }

}
