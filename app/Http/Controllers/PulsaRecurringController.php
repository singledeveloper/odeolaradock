<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\PulsaRecurring\Configurator;
use Odeo\Domains\Order\PulsaRecurring\PulsaRecurringHistorySelector;
use Odeo\Domains\Order\PulsaRecurring\PulsaRecurringSelector;
use Odeo\Domains\Order\PulsaRecurring\PulsaRecurringScheduledSelector;

class PulsaRecurringController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function get() {
    $this->pipeline->add(new Task(PulsaRecurringSelector::class, 'get'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getScheduled() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(PulsaRecurringScheduledSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function findById($recurringId) {
    $this->pipeline->add(new Task(PulsaRecurringSelector::class, 'findById', [
      'id' => $recurringId
    ]));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getHistories($recurringId) {
    $data = $this->getRequestData();
    $data['pulsa_recurring_id'] = $recurringId;
    $this->pipeline->add(new Task(PulsaRecurringHistorySelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function getTypes() {
    $this->pipeline->add(new Task(Configurator::class, 'getTypes'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function create() {

    list($isValid, $data) = $this->validateData([
      'item_id' => 'required',
      'recurring_type' => 'required',
      'recurring_date' => 'required_if:recurring_type,' . Recurring::TYPE_MONTH . ',' . Recurring::TYPE_ONCE
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Configurator::class, 'create'));
    return $this->executeAndResponse($data);

  }

  public function toggle() {

    list($isValid, $data) = $this->validateData([
      'pulsa_recurring_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Configurator::class, 'toggle'));
    return $this->executeAndResponse($data);

  }

  public function remove() {

    list($isValid, $data) = $this->validateData([
      'pulsa_recurring_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Configurator::class, 'remove'));
    return $this->executeAndResponse($data);

  }

  public function updatePrice() {
    list($isValid, $data) = $this->validateData([
      'pulsa_recurring_id' => 'required',
      'inventory_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Configurator::class, 'updatePrice'));
    return $this->executeAndResponse($data);

  }

}
