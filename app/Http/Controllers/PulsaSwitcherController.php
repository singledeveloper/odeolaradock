<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\Registrar;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaInventoryEditor;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaMutationEditor;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaOrderSelector;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaEditor;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaSearcher;
use Odeo\Domains\Biller\BillerSelector;

class PulsaSwitcherController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getInventories() {
    $data = $this->getRequestData();
    if (isAdmin()) {
      if (isset($data['vendor_switcher_id']) && $data['vendor_switcher_id'] == '0')
        $data['expand'] = 'cheapest_inventory,price_change';
      else $data['expand'] = 'other_inventory';
    }
    $data['is_paginate'] = false;
    $this->pipeline->add(new Task(PulsaSearcher::class, 'searchAll'));
    return $this->executeAndResponse($data);
  }

  public function getSupplierInventories() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(PulsaSearcher::class, 'getSupplierInventories'));
    return $this->executeAndResponse($data);
  }

  public function addPulsa() {
    list($isValid, $data) = $this->validateData([
      'price' => 'required',
      'name' => 'required',
      'category' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PulsaInventoryEditor::class, 'addPulsa'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function replicatePulsa() {
    list($isValid, $data) = $this->validateData([
      'pulsa_odeo_id' => 'required',
      'name' => 'required',
      'inventory_code' => 'required',
      'price' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PulsaInventoryEditor::class, 'replicate'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function addInventories() {
    list($isValid, $data) = $this->validateData([
      'pulsa_odeo_id' => 'required',
      'inventory_code' => 'required',
      'vendor_switcher_id' => 'required',
      'base_price' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PulsaInventoryEditor::class, 'add'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function editInventories() {
    $this->pipeline->add(new Task(PulsaInventoryEditor::class, 'edit'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getOrder() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(PulsaOrderSelector::class, 'getOrder'));
    return $this->executeAndResponse($data);
  }

  public function getOrderLog($switcherId) {
    $data = $this->getRequestData();
    $data['order_detail_pulsa_switcher_id'] = $switcherId;

    $this->pipeline->add(new Task(PulsaOrderSelector::class, 'getLog'));
    return $this->executeAndResponse($data);
  }

  public function refund() {
    list($isValid, $data) = $this->validateData([
      'order_detail_pulsa_switcher_id' => 'required|exists:order_detail_pulsa_switchers,id'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Registrar::class, 'autoSign'));
    return $this->executeAndResponse($data);
  }

  public function editPulsaOrder() {
    list($isValid, $data) = $this->validateData([
      'order_detail_pulsa_switcher_id' => 'required|exists:order_detail_pulsa_switchers,id'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PulsaEditor::class, 'editSwitcher'));
    return $this->executeAndResponse($data);
  }

  public function getOperators() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(PulsaSearcher::class, 'getAllOperator'));
    return $this->executeAndResponse($data);
  }

  public function getCategories() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(PulsaSearcher::class, 'getAllCategory'));
    return $this->executeAndResponse($data);
  }

  public function get() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(PulsaSearcher::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function getReport() {
    $this->pipeline->add(new Task(BillerSelector::class, 'getReport'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getSLA() {

    setQueryTimeout(5);

    $data['is_paginate'] = false;
    $data['fields'] = 'id,name';
    $data['expand'] = 'sla';
    $data['is_active'] = true;
    $this->pipeline->add(new Task(BillerSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function getLeadTime() {

    setQueryTimeout(5);

    $data['is_paginate'] = false;
    $data['fields'] = 'id,name';
    $data['expand'] = 'lead_time';
    $data['is_active'] = true;
    $this->pipeline->add(new Task(BillerSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function checkRepeat() {
    list($isValid, $data) = $this->validateData([
      'number' => 'required',
      'item_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PulsaOrderSelector::class, 'checkRepeat'));
    return $this->executeAndResponse($data);
  }

  public function getRecent() {
    $this->pipeline->add(new Task(PulsaOrderSelector::class, 'getRecent'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function insertManualMutation() {
    list($isValid, $data) = $this->validateData([
      'type' => 'required',
      'vendor_switcher_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PulsaMutationEditor::class, 'insertManualMutation'));
    return $this->executeAndResponse($data);
  }

  public function insertBalanceMutationAll() {
    list($isValid, $data) = $this->validateData([
      'date' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PulsaMutationEditor::class, 'insertBalanceMutationAll'));
    return $this->executeAndResponse($data);
  }

}
