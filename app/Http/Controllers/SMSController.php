<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Vendor\SMS\Obox\Requester;
use Odeo\Domains\Vendor\SMS\Registrar;
use Odeo\Domains\Vendor\SMS\SMSSelector;
use Odeo\Domains\Vendor\SMS\SMSSwitcherSelector;

class SMSController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function register() {
    list($isValid, $data) = $this->validateData([
      'telephone' => 'required',
      'transaction_pin' => 'required',
      'confirm_transaction_pin' => 'required|same:transaction_pin'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Registrar::class, 'register'));
    return $this->executeAndResponse($data);
  }

  public function remove() {
    $this->pipeline->add(new Task(Registrar::class, 'remove'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function changeTransactionPin() {
    list($isValid, $data) = $this->validateData([
      'new_transaction_pin' => 'required',
      'confirm_transaction_pin' => 'required|same:new_transaction_pin'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Registrar::class, 'changeTransactionPin'));
    return $this->executeAndResponse($data);
  }

  public function get($id) {
    $data = $this->getRequestData();
    $data['sms_center_id'] = $id;
    $this->pipeline->add(new Task(SMSSelector::class, 'getDetail'));
    return $this->executeAndResponse($data);
  }

  public function getAll() {
    $this->pipeline->add(new Task(SMSSelector::class, 'get'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getList() {
    $this->pipeline->add(new Task(SMSSelector::class, 'getList'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getCommandExample() {
    $this->pipeline->add(new Task(SMSSelector::class, 'getCommandExamples'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function bypassOutbound() {
    $this->pipeline->add(new Task(Requester::class, 'bypassOutbound'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getStatus($id) {
    $data = $this->getRequestData();
    $data['sms_center_id'] = $id;
    
    $this->pipeline->add(new Task(SMSSelector::class, 'getStatus'));

    return $this->executeAndResponse($data);
  }

  public function getLogs() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(SMSSwitcherSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function affiliateSendSMS() {
    list($isValid, $data) = $this->validateData([
      'number' => 'required',
      'message' => 'required|max:160'
    ]);

    if (!$isValid) return $data;

    $data['sms_destination_number'] = $data['number'];
    $data['sms_text'] = $data['message'];
    $data['is_affiliate'] = true;

    unset($data['number']);
    unset($data['message']);

    $this->pipeline->add(new Task(\Odeo\Domains\Vendor\SMS\Dartmedia\Requester::class, 'request'));
    return $this->executeAndResponse($data);
  }

}