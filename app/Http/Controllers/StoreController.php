<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\Helper\AdminChecker;
use Odeo\Domains\Channel\ChannelSelector;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Marketing\QueueSelector;
use Odeo\Domains\Network\ReferralRequester;
use Odeo\Domains\Network\SponsorRequester;
use Odeo\Domains\Network\TreeRequester;
use Odeo\Domains\Subscription\DomainVerificator;
use Odeo\Domains\Subscription\StoreOwnerSelector;
use Odeo\Domains\Subscription\StoreSelector;
use Odeo\Domains\Subscription\StoreUpdater;
use Odeo\Domains\Subscription\StoreVerificator;
use Odeo\Domains\Transaction\TempDepositSelector;

class StoreController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function userStore() {
    $data = $this->getRequestData();
    $data["expand"] = "plan,deposit,order,network_status,revenue,profit,order_count";
    $data["fields"] = "user_stores.user_id,stores.id,plan_id,name,domain_name,subdomain_name,status,renewal_at,stores.created_at";
    $data["is_active"] = true;
    $this->pipeline->add(new Task(StoreSelector::class, 'getUserStore'));
    return $this->executeAndResponse($data);
  }

  public function hustlerStore() {
    $data = $this->getRequestData();
    $data["expand"] = "plan,deposit,order,network_status,revenue,profit,order_count";
    $data["fields"] = "user_stores.user_id,stores.id,plan_id,name,domain_name,subdomain_name,status,renewal_at,stores.created_at";
    $data["is_active"] = true;
    $this->pipeline->add(new Task(StoreSelector::class, 'getHustlerStore'));
    return $this->executeAndResponse($data);
  }

  public function teamStore() {
    $data = $this->getRequestData();
    $data["expand"] = "plan,deposit,order,network_status,revenue,profit,order_count";
    $data["fields"] = "user_stores.user_id,stores.id,plan_id,name,domain_name,subdomain_name,status,renewal_at,stores.created_at";
    $data["is_active"] = true;
    $this->pipeline->add(new Task(StoreSelector::class, 'getTeamStore'));
    return $this->executeAndResponse($data);
  }

  public function index() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $data["expand"] = "plan,owner,deposit,invitator,orush,oads,network_status,md_plus";
    $this->pipeline->add(new Task(StoreSelector::class, 'getStore'));
    return $this->executeAndResponse($data);
  }

  public function getUsersHaveNoStore() {
    $data = $this->getRequestData();
    $data["expand"] = "plan,owner,invitator";
    $data['is_paginate'] = false;
    $this->pipeline->add(new Task(StoreSelector::class, 'getUsersHaveNoStore'));
    return $this->executeAndResponse($data);
  }

  public function getStoreOwnerInformation($storeId) {

    $data = $this->getRequestData();
    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true,
      'allow_mentor' => true
    ]));
    $this->pipeline->add(new Task(StoreOwnerSelector::class, 'getStoreOwnerInformation'));
    return $this->executeAndResponse($data);
  }

  public function getStoreDetail($storeId) {
    $data = $this->getRequestData();
    $data["expand"] = "plan,deposit,revenue,profit,locked_deposit,orush,order,current_data";
    $data["store_id"] = $storeId;
    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'allow_mentor' => true
    ]));
    $this->pipeline->add(new Task(StoreSelector::class, 'getStoreDetail'));
    return $this->executeAndResponse($data);
  }


  public function getFirstActiveStore() {
    $data = $this->getRequestData();

    $data["expand"] = "plan";

    if (isset($data['store_id'])) {
      $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    } else {
      $this->pipeline->add(new Task(StoreSelector::class, 'getFirstActiveStore'));
    }

    $this->pipeline->add(new Task(StoreSelector::class, 'getStoreDetail'));

    return $this->executeAndResponse($data);
  }

  public function getPubSettings($domain) {
    $data = $this->getRequestData();
    $data["domain"] = $domain;

    if ($domain == 'www.odeo.co.id' || $domain == 'www.staging.odeo.co.id') {
      $this->pipeline->add(new Task(\Odeo\Domains\Subscription\DomainLauncher::class, 'launchDomainSwitcher'));
    } else {
      $data["expand"] = "banner,account,deposit,current_data";
      $data["fields"] = "id,plan_id,current_data,name,logo_path,favicon_path";
      $this->pipeline->add(new Task(\Odeo\Domains\Subscription\DomainLauncher::class, 'launch'));
    }

    return $this->executeAndResponse($data);
  }

  public function getDomainDescription($domain) {
    $data = $this->getRequestData();
    $data["domain"] = $domain;
    $data["fields"] = "id,plan_id,name,favicon_path,logo_path,subdomain_name,domain_name";
    $this->pipeline->add(new Task(\Odeo\Domains\Subscription\DomainLauncher::class, 'getDomainDescription'));
    return $this->executeAndResponse($data);
  }

  public function cash($storeId) {
    $data = $this->getRequestData();
    $data["expand"] = "deposit,locked_deposit,rush";
    $data["fields"] = "id";
    $data["store_id"] = $storeId;
    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'allow_mentor' => true
    ]));
    $this->pipeline->add(new Task(StoreSelector::class, 'getStoreDetail'));
    return $this->executeAndResponse($data);
  }

  public function getInventories($storeId) {

    return $this->buildErrorsResponse('Manage inventories is on maintenance, use https://m.odeo.co.id instead');

  }

  public function getAccounts($storeId) {
    $data = $this->getRequestData();
    $data["expand"] = "account";
    $data["fields"] = "id";
    $data["store_id"] = $storeId;

    if (app()->environment() == 'production' && $data['auth']['platform_id'] == Platform::ANDROID) {
      return $this->buildErrorsResponse('This feature is currently on maintenance', 400);
    }

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'allow_mentor' => true
    ]));
    $this->pipeline->add(new Task(StoreSelector::class, 'getStoreDetail'));
    return $this->executeAndResponse($data);
  }

  public function updateReferral() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true,
      'not_available_for_free_store' => true
    ]));
    $this->pipeline->add(new Task(DomainVerificator::class, 'checkInvitationCode'));
    $this->pipeline->add(new Task(TreeRequester::class, 'update'));
    $this->pipeline->add(new Task(ReferralRequester::class, 'create'));
    $this->pipeline->add(new Task(SponsorRequester::class, 'create'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function domainSetup() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required',
      'domain_name' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->add(new Task(DomainVerificator::class, 'checkDomain'));
    $this->pipeline->add(new Task(StoreUpdater::class, 'updateDomain'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function getChannels($storeId) {
    $data = $this->getRequestData();
    $data["store_id"] = $storeId;
    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'allow_mentor' => true
    ]));
    $this->pipeline->add(new Task(ChannelSelector::class, 'get'));
    $this->pipeline->execute($data);

    $response = $this->pipeline->data;
    if (count($response['channels']) > 0) return $this->buildSuccessResponse($response);
    else return $this->buildSuccessResponse([], 204);
  }

  public function markup() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required',
      'service_detail_id' => 'exists:service_details,id',
      'active' => 'integer|in:0,1',
      'discount' => 'numeric|min:0',
      'phone_number' => 'regex:/^[1-9][0-9]*$/|min:8|max:16',
      'banner' => 'json'
    ]);
    if (!$isValid) return $data;

    if (isset($data["disable"]) && $data["disable"] == "1") $data["active"] = 0;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', ['store_must_active' => true]));
    $this->pipeline->add(new Task(StoreUpdater::class, 'update'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function getLockedDeposit($storeId) {
    $data = $this->getRequestData();
    $data['expand'] = 'total_deposit,order,user';
    $data['store_id'] = $storeId;
    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'allow_mentor' => true
    ]));
    $this->pipeline->add(new Task(TempDepositSelector::class, 'getLockedDeposit'));
    $this->pipeline->execute($data);

    $response = $this->pipeline->data;
    if (count($response)) return $this->buildSuccessResponse($response);
    else return $this->buildSuccessResponse([], 204);

  }

  public function getAvailableStores($serviceId) {
    $data = $this->getRequestData();
    $data['service_id'] = $serviceId;

    $this->pipeline->add(new Task(QueueSelector::class, 'get'));
    $this->pipeline->add(new Task(StoreSelector::class, 'getAvailableStores'));

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    return $this->buildSuccessResponse($this->pipeline->data);
  }

  public function getAllMasterStores() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(StoreSelector::class, 'getAllMasterStores'));
    return $this->executeAndResponse($data);
  }

  public function getSupplierStores() {
    $this->pipeline->add(new Task(StoreSelector::class, 'getSupplierStores'));
    return $this->executeAndResponse($this->getRequestData());
  }

}
