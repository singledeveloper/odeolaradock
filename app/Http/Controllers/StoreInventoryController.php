<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/14/17
 * Time: 9:53 PM
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Core\Task;
use Odeo\Domains\Subscription\ManageInventory\StoreInventoryDisabler;
use Odeo\Domains\Subscription\ManageInventory\StoreInventorySelector;
use Odeo\Domains\Subscription\ManageInventory\StoreInventoryVendorRequester;
use Odeo\Domains\Subscription\ManageInventory\StoreInventoryVendorSelectionUpdater;
use Odeo\Domains\Subscription\StoreVerificator;

class StoreInventoryController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getStoreInventories($storeId, $serviceDetailid) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;
    $data['service_detail_id'] = $serviceDetailid;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(StoreInventorySelector::class, 'get'));

    return $this->executeAndResponse($data);

  }

  public function getVendorPricing($storeId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(StoreInventoryVendorRequester::class, 'getVendorPricing'));

    return $this->executeAndResponse($data);

  }

  public function addVendor($storeId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true,
      'response_with_store_data' => true
    ]));

    $this->pipeline->add(new Task(StoreInventoryVendorRequester::class, 'addVendor'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data, null, [
      'store'
    ]);

  }

  public function saveVendorSelectedPrice($storeId, $serviceDetailid) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;
    $data['service_detail_id'] = $serviceDetailid;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true,
      'response_with_store_data' => true
    ]));
    $this->pipeline->add(new Task(StoreInventoryVendorSelectionUpdater::class, 'saveSelectedVendorPrice'));

    return $this->executeAndResponse($data, null, [
      'store'
    ]);
  }

  public function toggleService($storeId, $serviceDetailId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;
    $data['service_detail_id'] = $serviceDetailId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(StoreInventoryDisabler::class, 'toggle'));

    return $this->executeAndResponse($data);

  }

  public function removeVendor($storeId, $serviceDetailId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;
    $data['service_detail_id'] = $serviceDetailId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(StoreInventoryVendorRequester::class, 'removeVendor'));

    return $this->executeAndResponse($data);

  }

  public function replaceVendor($storeId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(StoreInventoryVendorRequester::class, 'replaceVendor'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }

}