<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Vendor\Telegram\TelegramAcceptor;
use Odeo\Domains\Vendor\Telegram\TelegramSelector;
use Odeo\Domains\Vendor\Telegram\TelegramUserConfigurator;

class TelegramController extends Controller {
  
  public function __construct() {
    parent::__construct();
  }

  public function parse($token) {
    $data = $this->getRequestData();
    $data['token'] = $token;
    $this->pipeline->add(new Task(TelegramAcceptor::class, 'parse'));
    return $this->executeAndResponse($data);
  }

  public function getUserConfig() {
    $this->pipeline->add(new Task(TelegramUserConfigurator::class, 'getConfig'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function createUserTransactionPin() {

    list($isValid, $data) = $this->validateData([
      'transaction_pin' => 'required',
      'confirm_transaction_pin' => 'required|same:transaction_pin'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(TelegramUserConfigurator::class, 'createTransactionPin'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function changeUserTransactionPin() {

    list($isValid, $data) = $this->validateData([
      'new_transaction_pin' => 'required',
      'confirm_transaction_pin' => 'required|same:new_transaction_pin'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(TelegramUserConfigurator::class, 'changeTransactionPin'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getCommandExample() {
    $this->pipeline->add(new Task(TelegramSelector::class, 'getCommandExamples'));
    return $this->executeAndResponse($this->getRequestData());
  }

}
