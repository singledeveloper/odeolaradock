<?php

namespace Odeo\Http\Controllers;

use Illuminate\Http\Request;
use Odeo\Domains\Payment\Pax\Repository\TerminalActivityLogRepository;

class TerminalActivityController extends Controller {

  public function __construct() {
    parent::__construct();

    $this->terminalActivityLogRepo = app()->make(TerminalActivityLogRepository::class);
  }

  public function logPaxAppCallResult(Request $request) {
    list($isValid, $data) = $this->validateData([
      'command' => 'required',
      'status' => 'required',
      'app_response' => 'required',
    ]);

    if (!$isValid) return $data;

    $terminalActivity = $this->terminalActivityLogRepo->getNew();
    $terminalActivity->order_id = $data['order_id'] ?? NULL;
    $terminalActivity->command = $data['command'];
    $terminalActivity->terminal_id = $data['terminal_id'];
    $terminalActivity->status = $data['status'];
    $terminalActivity->app_response = $data['app_response'];

    $terminalActivity->save();

    return $this->buildResponse(200);
  }
}
