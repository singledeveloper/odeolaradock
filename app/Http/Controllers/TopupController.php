<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Transaction\TopupBankTransferRequester;
use Odeo\Domains\Transaction\TopupBankTransferSelector;
use Odeo\Domains\Transaction\TopupRequester;
use Odeo\Domains\Transaction\TopupSelector;

class TopupController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function topup($topupId = null) {

    $data = $this->getRequestData();
    $data["expand"] = "order";

    if ($topupId) {
      $data["topup_id"] = $topupId;
      $task = new Task(TopupSelector::class, 'getDetail');
    } else if (isset($data["last_topup"])) {
      $task = new Task(TopupSelector::class, 'getDetail');
    } else $task = new Task(TopupSelector::class, 'get');

    $this->pipeline->add($task);
    return $this->executeAndResponse($data);
  }

  public function createTopup() {

    list($isValid, $data) = $this->validateData([
      'cash.amount' => 'required|numeric|min:10000|max:100000000000',
      'cash.currency' => 'required'
    ]);


    if (!$isValid) return $data;

    $this->enableOnlyOnPlatform($data['auth']['platform_id'], [
      Platform::ANDROID, Platform::WEB_APP, Platform::IOS
    ]);

    $isCheckout = isset($data['checkout']) && $data['checkout'];

    $this->pipeline->add(new Task(CartRemover::class, 'clear'));
    $this->pipeline->add(new Task(TopupRequester::class, 'request'));
    $this->pipeline->add(new Task(CartInserter::class, 'addToCart', ['service_detail_id' => ServiceDetail::TOPUP_ODEO]));

    $isCheckout && $this->pipeline->add(new Task(CartCheckouter::class, 'checkout'));

    $this->pipeline->enableTransaction();

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    }

    $response = $this->pipeline->data;

    return $this->buildSuccessResponse($isCheckout ? [
      'order_id' => $response['order_id']
    ] : $response, $this->pipeline->statusCode);
  }

  public function getTopupBankTransfers() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(TopupBankTransferSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function toggleTopupBankTransfers() {
    list($isValid, $data) = $this->validateData([
      'topup_bank_transfer_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(TopupBankTransferRequester::class, 'toggle'));
    return $this->executeAndResponse($data);
  }

}
