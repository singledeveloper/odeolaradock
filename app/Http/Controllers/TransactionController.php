<?php

namespace Odeo\Http\Controllers;

use Carbon\Carbon;
use Odeo\Domains\Account\Helper\AdminChecker;
use Odeo\Domains\Account\Helper\CustomerEmailManager;
use Odeo\Domains\Account\UserSelector;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Subscription\StoreSelector;
use Odeo\Domains\Transaction\CashLimitSelector;
use Odeo\Domains\Transaction\CashTransactionSelector;
use Odeo\Domains\Transaction\Helper\ExportDownloader;
use Odeo\Domains\Transaction\RecentSelector;
use Odeo\Domains\Transaction\RevenueSelector;
use Odeo\Domains\Transaction\TransactionRequester;

class TransactionController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function dashboard() {

    setQueryTimeout(10);

    $data = $this->getRequestData();

    $data["expand"] = "cash,locked_cash";
    $data["limit"] = 5;
    $data["offset"] = 0;
    $data['type'] = 'cash';

    $this->pipeline->add(new Task(RecentSelector::class, 'get'));
    $this->pipeline->add(new Task(UserSelector::class, 'getUserDetail', ["fields" => "id"]));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);

    $response = $this->pipeline->data;
    $response['last_transaction'] = "" . Carbon::now('Asia/Jakarta');
    if (isset($response['metadata'])) unset($response['metadata']);
    return $this->buildSuccessResponse($response);
  }

  public function dashboardStore() {

    setQueryTimeout(10);

    $data = $this->getRequestData();
    $data["expand"] = "deposit,locked_deposit,revenue";
    $data["limit"] = 5;
    $data["offset"] = 0;
    $data['type'] = 'deposit';

    $this->pipeline->add(new Task(RecentSelector::class, 'get'));
    $this->pipeline->add(new Task(UserSelector::class, 'getUserDetail', ["fields" => "id"]));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);

    $response = $this->pipeline->data;
    $response['last_transaction'] = "" . Carbon::now('Asia/Jakarta');
    if (isset($response['metadata'])) unset($response['metadata']);
    return $this->buildSuccessResponse($response);
  }

  public function revenue() {
    $data = $this->getRequestData();
    if (isset($data["store_ids"])) {
      $data["expand"] = "revenue,deposit,locked_deposit";
      $this->pipeline->add(new Task(StoreSelector::class, 'getStore'));
    } else {
      $this->pipeline->add(new Task(RevenueSelector::class, 'get'));
    }
    return $this->executeAndResponse($data);
  }

  public function recent() {
    setQueryTimeout(10);

    $data = $this->getRequestData();

    $data['type'] = 'cash';
    $this->pipeline->add(new Task(RecentSelector::class, 'get'));
    $this->pipeline->execute($data);
    $res = $this->pipeline->data;

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    }
    return $this->buildSuccessResponse($res);
  }

  public function recentStore() {

    setQueryTimeout(10);

    $data = $this->getRequestData();
    $data['type'] = 'deposit';

    $this->pipeline->add(new Task(RecentSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function export() {
    list($isValid, $data) = $this->validateData([
      'start_date' => 'required|date',
      'end_date' => 'required|date'
    ]);

    if (!$isValid) return $data;

    $data['type'] = 'cash';

    $this->pipeline->add(new Task(CustomerEmailManager::class, 'checkIsEmailVerified'));
    $this->pipeline->add(new Task(RecentSelector::class, 'exportRecent'));
    return $this->executeAndResponse($data);
  }

  public function exportStore() {
    list($isValid, $data) = $this->validateData([
      'start_date' => 'required|date',
      'end_date' => 'required|date'
    ]);

    if (!$isValid) return $data;

    $data['type'] = 'deposit';

    $this->pipeline->add(new Task(CustomerEmailManager::class, 'checkIsEmailVerified'));
    $this->pipeline->add(new Task(RecentSelector::class, 'exportRecent'));
    return $this->executeAndResponse($data);
  }

  public function pendingTransactions() {

    setQueryTimeout(10);

    $data = $this->getRequestData();

    $data['type'] = 'cash';
    $this->pipeline->add(new Task(RecentSelector::class, 'getPending'));
    $this->pipeline->execute($data);
    $res = $this->pipeline->data;

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    }
    return $this->buildSuccessResponse($res);
  }

  public function pendingTransactionsExport() {
    list($isValid, $data) = $this->validateData([
      'start_date' => 'required|date',
      'end_date' => 'required|date'
    ]);

    if (!$isValid) return $data;

    $data['type'] = 'cash';

    $this->pipeline->add(new Task(CustomerEmailManager::class, 'checkIsEmailVerified'));
    $this->pipeline->add(new Task(RecentSelector::class, 'exportPendingTransactions'));
    return $this->executeAndResponse($data);
  }

  public function getPendingTransactionSummary() {
    $data['type'] = 'cash';
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(RecentSelector::class, 'getPendingSummary'));
    $this->pipeline->execute($data);
    $res = $this->pipeline->data;

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    }
    return $this->buildSuccessResponse($res);
  }

  public function getTopUpWithdrawLimitAndAmount() {
    setQueryTimeout(10);

    $data = $this->getRequestData();

    $this->pipeline->add(new Task(CashLimitSelector::class, 'getTopUpWithdrawLimitAndAmount'));
    return $this->executeAndResponse($data);
  }

  public function findCashTransaction($cashTransactionsId) {
    setQueryTimeout(10);

    $data = $this->getRequestData();

    $data['cash_transactions_id'] = $cashTransactionsId;
    $data['type'] = 'cash';
    $this->pipeline->add(new Task(CashTransactionSelector::class, 'find'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    }

    return $this->buildSuccessResponse($this->pipeline->data);
  }

  public function get() {
    setQueryTimeout(10);

    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    if (!isset($data['sort_by'])) {
      $data['sort_by'] = 'id';
    }
    if (!isset($data['sort_type'])) {
      $data['sort_type'] = 'desc';
    }

    $this->pipeline->add(new Task(CashTransactionSelector::class, 'get'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    }

    return $this->buildSuccessResponse($this->pipeline->data);
  }

  public function patch() {
    list($isValid, $data) = $this->validateData([
      'id' => 'required',
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AdminChecker::class, 'checkHasPermission', [
      'task' => UserType::GROUP_HAS_PATCH_TRANSACTION_PERMISSION
    ]));
    $this->pipeline->add(new Task(TransactionRequester::class, 'patch'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function fetchExportStatus($exportId) {
    $data = $this->getRequestData();
    $data['export_id'] = $exportId;
    $this->pipeline->add(new Task(ExportDownloader::class, 'check'));
    return $this->executeAndResponse($data);
  }
}
