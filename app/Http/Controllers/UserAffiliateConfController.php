<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/16/17
 * Time: 11:28 PM
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Affiliate\UserAffiliateConfigurator;
use Odeo\Domains\Core\Task;

class UserAffiliateConfController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getWhiteList() {
    $this->pipeline->add(new Task(UserAffiliateConfigurator::class, 'getWhiteList'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function addWhiteList() {
    $this->pipeline->add(new Task(UserAffiliateConfigurator::class, 'addWhiteList'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function removeWhiteList() {
    $this->pipeline->add(new Task(UserAffiliateConfigurator::class, 'removeWhiteList'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function setApiType() {
    $this->pipeline->add(new Task(UserAffiliateConfigurator::class, 'setApiType'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getSecretKey() {
    $this->pipeline->add(new Task(UserAffiliateConfigurator::class, 'getSecretKey'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getNotifyUrl() {
    $this->pipeline->add(new Task(UserAffiliateConfigurator::class, 'getNotifyUrl'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function setNotifyUrl() {
    $this->pipeline->add(new Task(UserAffiliateConfigurator::class, 'setNotifyUrl'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function testSuccessNotify() {
    $this->pipeline->add(new Task(UserAffiliateConfigurator::class, 'testSuccessNotify'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function testFailNotify() {
    $this->pipeline->add(new Task(UserAffiliateConfigurator::class, 'testFailNotify'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function testNewPriceNotify() {
    $this->pipeline->add(new Task(UserAffiliateConfigurator::class, 'testNewPriceNotify'));
    return $this->executeAndResponse($this->getRequestData());
  }

}