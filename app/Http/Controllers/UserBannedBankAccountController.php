<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Account\UserBannedBankAccountSelector;
use Odeo\Domains\Account\UserBannedBankAccountUpdater;

class UserBannedBankAccountController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function get() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(UserBannedBankAccountSelector::class, 'getAll'));

    return $this->executeAndResponse($data);
  }

  public function update($bannedBankAcountId) {
    list($isValid, $data) = $this->validateData([]);
    if (!$isValid) return $data;

    $data['banned_bank_account_id'] = $bannedBankAcountId;
    $this->pipeline->add(new Task(UserBannedBankAccountUpdater::class, 'update'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

}