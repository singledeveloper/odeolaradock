<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\UserKycSelector;
use Odeo\Domains\Account\UserKycUpdater;
use Odeo\Domains\Constant\UserKycStatus;
use Odeo\Domains\Core\Task;

class UserBusinessKycController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function check() {
    $this->pipeline->add(new Task(UserKycSelector::class, 'check'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function checkById($kycId) {
    $data = $this->getRequestData();
    $data['kyc_id'] = $kycId;
    $this->pipeline->add(new Task(UserKycSelector::class, 'checkById'));
    return $this->executeAndResponse($data);
  }

  public function get() {
    list($isValid, $data) = $this->validateData([
      'business_type' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserKycSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function upload() {
    list($isValid, $data) = $this->validateData([
      'option_id' => 'required',
      'file' => 'max:102400'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserKycUpdater::class, 'upload'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function submit() {
    $this->pipeline->add(new Task(UserKycUpdater::class, 'requestVerify'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($this->getRequestData());
  }

  public function cancel() {
    $this->pipeline->add(new Task(UserKycUpdater::class, 'cancel'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($this->getRequestData());
  }

  public function getUsers() {
    $this->pipeline->add(new Task(UserKycSelector::class, 'getUsers'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getById($kycId) {
    $data = $this->getRequestData();
    $data['kyc_id'] = $kycId;
    $this->pipeline->add(new Task(UserKycSelector::class, 'getById'));
    return $this->executeAndResponse($data);
  }

  public function manageApproval() {
    list($isValid, $data) = $this->validateData([
      'kyc_detail_id' => 'required',
      'status' => 'required|in:' . UserKycStatus::REJECTED . ',' . UserKycStatus::VERIFIED
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserKycUpdater::class, 'manageApproval'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($this->getRequestData());
  }

}
