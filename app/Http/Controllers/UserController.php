<?php

namespace Odeo\Http\Controllers;

use Illuminate\Http\Request;
use Odeo\Domains\Affiliate\UserAffiliateConfigurator;
use Odeo\Domains\Affiliate\UserAffiliateSelector;
use Odeo\Domains\Agent\AgentRequester;
use Odeo\Domains\Account\UserMailOtpVerificator;
use Odeo\Domains\Account\Helper\AdminChecker;
use Odeo\Domains\Account\Helper\UserChecker;
use Odeo\Domains\Account\Loginner;
use Odeo\Domains\Account\Logout;
use Odeo\Domains\Account\OtpSender;
use Odeo\Domains\Account\Registrar;
use Odeo\Domains\Account\Resetter;
use Odeo\Domains\Account\TokenManager;
use Odeo\Domains\Account\UserMailVerificator;
use Odeo\Domains\Account\UserSelector;
use Odeo\Domains\Account\UserUpdater;
use Odeo\Domains\Account\UserValidator;
use Odeo\Domains\Account\UserVerificator;
use Odeo\Domains\Approval\ApprovalSelector;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Faq\FaqSelector;
use Odeo\Domains\Notification\NotificationSelector;

class UserController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getAll() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $data['expand'] = 'cash';
    $this->pipeline->add(new Task(UserSelector::class, 'getAll'));

    return $this->executeAndResponse($data);
  }

  public function getAllAffiliates() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(UserAffiliateSelector::class, 'gets'));

    return $this->executeAndResponse($data);
  }

  public function editAffiliate() {
    list($isValid, $data) = $this->validateData([
      'affiliate_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserAffiliateConfigurator::class, 'editFromAdminPanel'));

    return $this->executeAndResponse($data);
  }

  public function summariesUser($user_id) {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $data['user_id'] = $user_id;

    $data['expand'] = 'cash,deposit,locked_deposit,revenue,total_withdrawal,current_month_topup,ktp_verification,maximum_cash';

    $this->pipeline->add(new Task(UserSelector::class, 'summariesUser'));

    return $this->executeAndResponse($data);
  }

  public function toggleUserBlock($user_id) {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $data['user_id'] = $user_id;

    $this->pipeline->add(new Task(UserUpdater::class, 'toggleBlock'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function updateStatus($user_id) {
    $data = $this->getRequestData();
    $data['user_id'] = $user_id;

    $this->pipeline->add(new Task(UserUpdater::class, 'updateStatus'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function sendVerification() {
    $data = $this->getRequestData();
    $rules = [
      'scenario' => 'required|in:sign_up,reset_password',
      'phone_number' => 'required|regex:/^[1-9][0-9]*$/|min:8|max:16'
    ];

    if (isset($data['scenario']) && $data['scenario'] == 'sign_up') {
      $rules['phone_number'] .= '|unique:users,telephone';
    }

    list($isValid, $data) = $this->validateData($rules);
    if (!$isValid) return $data;

    $taskFunction = $data['scenario'] == 'sign_up' ? 'sendOtp' : 'sendForgotPasswordOtp';
    $task = new Task(OtpSender::class, $taskFunction);
    $this->pipeline->add($task);
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function sendVerificationSignUp() {
    $rules = [
      'phone_number' => 'required|regex:/^[1-9][0-9]*$/|min:8|max:16|unique:users,telephone'
    ];

    list($isValid, $data) = $this->validateData($rules);
    if (!$isValid) return $data;

    $data['scenario'] = 'sign_up';

    $task = new Task(OtpSender::class, 'sendOtp');
    $this->pipeline->add($task);
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function sendVerificationForgotPassword() {
    $rules = [
      'phone_number' => 'required|regex:/^[1-9][0-9]*$/|min:8|max:16'
    ];

    list($isValid, $data) = $this->validateData($rules);
    if (!$isValid) return $data;

    $data['scenario'] = 'reset_password';

    $task = new Task(OtpSender::class, 'sendForgotPasswordOtp');
    $this->pipeline->add($task);
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function sendVerificationUpdatePin() {
    $this->pipeline->add(new Task(OtpSender::class, 'sendUpdatePinOtp'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData());
  }

  public function beginChangeNumber() {
    list($isValid, $data) = $this->validateData([
      'phone_number' => 'required',
    ]);
    if (!$isValid) return $data;

    $data['scenario'] = 'change_number';

    $this->pipeline->add(new Task(UserVerificator::class, 'guardActiveTelephone'));
    $this->pipeline->add(new Task(OtpSender::class, 'sendOtp'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function verify() {
    list($isValid, $data) = $this->validateData([
      'email' => 'required_without:phone_number',
      'phone_number' => 'required_without:email|regex:/^[1-9][0-9]*$/|min:8|max:16',
      'verification_code' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserVerificator::class, 'verify'));
    return $this->executeAndResponse($data);
  }

  public function verifyAfterLogin() {
    list($isValid, $data) = $this->validateData([
      'verification_code' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserVerificator::class, 'verifyAfterLogin'));
    return $this->executeAndResponse($data);
  }

  public function verifyChangeNumber() {
    list($isValid, $data) = $this->validateData([
      'phone_number' => 'required|regex:/^[1-9][0-9]*$/|min:8|max:16',
      'verification_code' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserVerificator::class, 'guardActiveTelephone'));
    $this->pipeline->add(new Task(UserVerificator::class, 'verifyChangeNumber'));
    return $this->executeAndResponse($data);
  }

  public function register() {
    list($isValid, $data) = $this->validateData([
      'phone_number' => 'required_without:email|unique:users,telephone',
      'email' => 'required_without:phone_number',
      'password' => 'required|same:confirm_password',
      'confirm_password' => 'required',
      'type' => 'in:user,seller',
      'platform_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $signUpData = [];

    if (isset($data['upgrade']) && $data['upgrade']) {

      $request = app()->make(Request::class);
      $request->header('Authorization') && ($auth = explode(' ', $request->header('Authorization'))) && (count($auth) == 2) && ($signUpData['type'] = 'seller') &&
      $this->pipeline->add(new Task(TokenManager::class, 'check', [
        'bearer' => $auth[1],
        "roles" => "guest",
      ]));
    }

    $this->pipeline->add(new Task(Registrar::class, 'signUp', $signUpData));

    if (isset($data['referral_code']) && $data['referral_code']) {
      $data['master_code'] = $data['referral_code'];
      $this->pipeline->add(new Task(AgentRequester::class, 'addMaster'));
    }

    $this->pipeline->add(new Task(Loginner::class, 'login'));
    $this->pipeline->add(new Task(TokenManager::class, 'create'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    if (isset($this->pipeline->data['type'])) unset($this->pipeline->data['type']);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    } else if ($this->pipeline->isNeedResponse) {
      return $this->buildSuccessResponse(array_merge($this->pipeline->data, [
        'free_cash' => 0
      ]), $this->pipeline->statusCode);
    }
  }

  public function createUser() {
    list($isValid, $data) = $this->validateData([
      'name' => 'required',
      'phone_number' => 'required_without:email|unique:users,telephone',
      'email' => 'required_without:phone_number',
      'password' => 'required',
      'type' => 'in:user,seller'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(Registrar::class, 'signUp'));
    return $this->executeAndResponse($data);
  }

  public function createBusinessUser() {
    list($isValid, $data) = $this->validateData([
      'name' => 'required',
      'email' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Registrar::class, 'signUpBusiness'));
    return $this->executeAndResponse($data);
  }

  public function checkPhoneNumber() {
    list($isValid, $data) = $this->validateData([
      'phone_number' => 'required|regex:/^[1-9][0-9]*$/|min:8|max:16'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserChecker::class, 'checkPhoneNumber'));
    return $this->executeAndResponse($data);
  }

  public function login() {
    list($isValid, $data) = $this->validateData([
      'email' => 'required_without:phone_number|email',
      'phone_number' => 'required_without:email|regex:/^[1-9][0-9]*$/|min:8|max:16',
      'password' => 'required',
      'platform_id' => 'required'
    ]);

    if (!$isValid) return $data;

    if ($data['platform_id'] == Platform::ANDROID || $data['platform_id'] == Platform::WEB_APP) {
      $data['profile_check'] = true;
    }

    $this->pipeline->add(new Task(Loginner::class, 'login'));
    $this->pipeline->execute($data);
    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode, '', $this->pipeline->errorStatus);
    }
    $firstPipelineData = $this->pipeline->data;

    $this->pipeline = $this->pipeline->clear();

    $this->pipeline->add(new Task(TokenManager::class, 'create'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse(array_merge($data, $firstPipelineData));
  }

  public function guestLogin() {
    list($isValid, $data) = $this->validateData([
      'platform_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Loginner::class, 'guest'));
    $this->pipeline->add(new Task(TokenManager::class, 'create'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function refreshToken() {
    list($isValid, $data) = $this->validateData([
      'user_id' => 'required|exists:user_tokens,user_id',
      'refresh_token' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(TokenManager::class, 'refresh'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function logout() {
    $data = array_merge($this->getRequestData(), getAuthorizationBearer());
    $this->pipeline->add(new Task(Logout::class, 'logout'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function resetPassword() {
    list($isValid, $data) = $this->validateData([
      'email' => 'required_without:telephone|email|exists:users,email',
      'telephone' => 'required_without:email|regex:/^[1-9][0-9]*$/|min:8|max:16|exists:users,telephone',
      'type' => 'required|in:email,sms',
      'token' => 'required',
      'password' => 'required|same:confirm_password',
      'confirm_password' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Resetter::class, 'resetPassword'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function resetPin() {
    list($isValid, $data) = $this->validateData([
      'token' => 'required',
      'pin' => 'required|same:pin',
      'confirm_pin' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Resetter::class, 'resetPin'));
    return $this->executeAndResponse($data);
  }

  public function checkPin() {
    $this->pipeline->add(new Task(UserChecker::class, 'checkPinExist'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function contactCheck() {
    list($isValid, $data) = $this->validateData([
      'telephone_list' => 'required'
    ]);
    if (!$isValid) return $data;

    $data["fields"] = "id,telephone,name";
    $data["is_paginate"] = "false";

    $this->pipeline->add(new Task(UserSelector::class, 'getUser'));
    return $this->executeAndResponse($data);
  }

  public function account() {
    $this->pipeline->add(new Task(UserSelector::class, 'getUserDetail'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function updateAccount() {
    list($isValid, $data) = $this->validateData([
      'email' => 'email',
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserUpdater::class, 'changeData'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function changePassword() {
    list($isValid, $data) = $this->validateData([
      'old_password' => 'required_without:pin',
      'pin' => 'required_without:old_password',
      'new_password' => 'required'
    ]);
    if (!$isValid) return $data;
    //temporary
    if (isset($data['old_password']) && !isAdmin()) {
      $data['old_pin'] = $data['old_password'];
      $data['new_pin'] = $data['new_password'];
      $this->pipeline->add(new Task(UserUpdater::class, 'changePin'));
    } else {
      $this->pipeline->add(new Task(UserUpdater::class, 'changePassword'));
    }

    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function changePin() {
    list($isValid, $data) = $this->validateData([
      'old_pin' => 'required',
      'new_pin' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserUpdater::class, 'changePin'));
    return $this->executeAndResponse($data);
  }

  public function faq() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(FaqSelector::class, 'get'));
    $this->pipeline->execute($data);
    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    return $this->buildSuccessResponse($this->pipeline->data, 200)
      ->header('Cache-control', 'max-age=43200, public');
  }

  public function notification() {
    setQueryTimeout(10);
    $this->pipeline->add(new Task(NotificationSelector::class, 'get'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function notificationStatus() {
    list($isValid, $data) = $this->validateData([
      'last_read_timestamp' => 'required',
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(NotificationSelector::class, 'getStatus'));
    return $this->executeAndResponse($data);
  }

  public function checkPassword() {
    list($isValid, $data) = $this->validateData([
      'password' => 'required_without:pin',
      'pin' => 'required_without:password',
    ]);

    if (!$isValid) return $data;

    if (isset($data['password'])) {
      $data['pin'] = $data['password'];
    }

    $this->pipeline->add(new Task(Loginner::class, 'checkPassword'));
    return $this->executeAndResponse($data);
  }

  public function validateEmail() {
    list($isValid, $data) = $this->validateData([
      'email' => 'required',
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserValidator::class, 'validateEmail'));
    return $this->executeAndResponse($data);
  }

  public function sendVerificationEmail() {
    $data = $this->getRequestData();
    $task = new Task(UserMailVerificator::class, 'sendVerification');
    $this->pipeline->add($task);
    return $this->executeAndResponse($data);
  }

  public function sendVerificationEmailOtp() {
    list($isValid, $data) = $this->validateData([
      'email' => 'required',
    ]);
    if (!$isValid) return $data;

    $data['email'] = strtolower($data['email']);
    $this->pipeline->add(new Task(UserMailOtpVerificator::class, 'sendVerifyEmailOtp'));

    return $this->executeAndResponse($data);
  }

  public function verifyEmailOtp() {
    list($isValid, $data) = $this->validateData([
      'email' => 'required',
      'verification_code' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['email'] = strtolower($data['email']);
    $this->pipeline->add(new Task(UserMailOtpVerificator::class, 'verifyEmailOtp'));
    return $this->executeAndResponse($data);
  }

  public function verifyEmail($token) {
    $data = $this->getRequestData();
    $data['token'] = urldecode($token);
    $this->pipeline->add(new Task(UserMailVerificator::class, 'verifyEmail'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);
    $response = $this->pipeline->data;
    return redirect('https://www.odeo.co.id/mcf/' . $response['status']);
  }

  public function sendOtpEmail() {
    $data = $this->getRequestData();

    $data['scenario'] = 'by_admin';

    $this->pipeline->add(new Task(OtpSender::class, 'sendForgotByEmail'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function sendOtpEmailSc() {
    $data = $this->getRequestData();

    $data['scenario'] = 'otp_from_sc';
    $data['user_id'] = $data['auth']['user_id'];
    $data['security_type'] = $data['security_type'] ?? 'pin';

    $this->pipeline->add(new Task(OtpSender::class, 'sendForgotByEmail'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function requestOtpEmail() {
    list($isValid, $data) = $this->validateData([
      'phone_number' => 'required|regex:/^[1-9][0-9]*$/|min:8|max:16',
    ]);
    if (!$isValid) {
      return $data;
    }

    $data['scenario'] = 'from_sc';

    $this->pipeline->add(new Task(OtpSender::class, 'requestOtpEmail'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function resetPasswordByOtp() {
    list($isValid, $data) = $this->validateData([
      'phone_number' => 'required|regex:/^[1-9][0-9]*$/|min:8|max:16',
      'verification_code' => 'required',
      'password' => 'required|same:confirm_password',
      'confirm_password' => 'required'
    ]);

    if (!$isValid) return $data;
    $data['token'] = $data['verification_code'];
    //$this->pipeline->add(new Task(UserVerificator::class, 'verify'));
    $this->pipeline->add(new Task(Resetter::class, 'resetPassword'));
    return $this->executeAndResponse($data);
  }

  public function checkMaker() {
    $this->pipeline->add(new Task(ApprovalSelector::class, 'checkMaker'));
    return $this->executeAndResponse($this->getRequestData());
  }

}
