<?php
namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\UserInvoiceDashboardSelector;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\UserInvoiceSelector;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\UserInvoiceUpdater;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\InvoiceCategorySelector;
use Odeo\Domains\Payment\PaymentVaDirectSelector;
use Odeo\Domains\Subscription\StoreVerificator;
use Odeo\Domains\Subscription\StoreSelector;

class UserInvoiceController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function invoiceDashboard($storeId) {
    $data = $this->getRequestData();
    $data['store_id'] = $storeId;
    $data['expand'] = 'config,payment';
    $data['limit'] = 10;
    $data['offset'] = 0;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(UserInvoiceDashboardSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function invoiceList($storeId) {
    $data = $this->getRequestData();
    $data['store_id'] = $storeId;
    $data['expand'] = 'config,payment,virtual_accounts';
    $data['biller_id'] = UserInvoice::BILLER_ODEO;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(UserInvoiceSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function userInvoiceList() {
    $data = $this->getRequestData();
    $data['billed_user_id'] = $data['auth']['user_id'];
    $data['expand'] = 'virtual_accounts';
    $data['biller_id'] = UserInvoice::BILLER_ODEO;

    $this->pipeline->add(new Task(UserInvoiceSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function invoiceInquiry() {
    list($isValid, $data) = $this->validateData([
      'invoice_number' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserInvoiceSelector::class, 'getDetail', [
      'expand' => 'config,payment,virtual_accounts'
    ]));
    $this->pipeline->add(new Task(StoreSelector::class, 'getStoreDetail', [
      'fields' => 'id,name,logo_path,subdomain_name,domain_name'
    ]));

    return $this->executeAndResponse($data);
  }

  public function invoiceCategories() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(InvoiceCategorySelector::class, 'get'));

    return $this->executeAndResponse($data)
      ->header('Cache-control', 'max-age=43200, public');
  }

  public function create($storeId) {
    list($isValid, $data) = $this->validateData([
      'name' => 'required',
      'due_date' => 'required|date|after:today',
      'telephone' => 'required|regex:/^[1-9][0-9]*$/|min:8|max:16',
      'user_name' => 'required',
      'user_id' => 'required',
      'category_id' => 'required|exists:invoice_categories,id',
      'items' => 'required|array',
      'items.*.name' => 'required',
      'items.*.amount' => 'required|integer',
      'items.*.price' => 'required|integer'
    ]);

    if (!$isValid) return $data;

    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(UserInvoiceUpdater::class, 'create'));
    $this->pipeline->add(new Task(PaymentVaDirectSelector::class, 'get', [
      'service_detail_id' => ServiceDetail::USER_INVOICE_ODEO,
      'biller' => [
        'id' => UserInvoice::BILLER_ODEO,
        'name' => 'odeo'
      ]
    ]));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function void() {
    list($isValid, $data) = $this->validateData([
      'invoice_number' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserInvoiceUpdater::class, 'void'));

    return $this->executeAndResponse($data);
  }

}
