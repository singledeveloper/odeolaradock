<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\Helper\UserChecker;
use Odeo\Domains\Account\UserKtpSelector;
use Odeo\Domains\Account\UserKtpUpdater;
use Odeo\Domains\Account\UserKtpHelper;
use Odeo\Domains\Core\Task;

class UserKtpController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getAll() {

    $data = $this->getRequestData();

    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(UserKtpSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }


  public function updateKtp() {
    list($isValid, $data) = $this->validateData([
      'ktp_nik' => 'size:16',
      'ktp_image' => 'image',
      'ktp_selfie_image' => 'image'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserKtpHelper::class, 'fillEmptyData'));
    $this->pipeline->add(new Task(UserKtpUpdater::class, 'updateKtp'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function reuploadKtp() {

    list($isValid, $data) = $this->validateData([
      'ktp_nik' => 'size:16',
      'ktp_image' => 'image',
      'ktp_selfie_image' => 'image',
      'user_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(UserChecker::class, 'checkUserBanned'));
    $this->pipeline->add(new Task(UserKtpHelper::class, 'fillEmptyData'));
    $this->pipeline->add(new Task(UserKtpUpdater::class, 'updateKtp'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function updateStatus($ktpId) {
    $data = $this->getRequestData();
    $data['ktp_id'] = $ktpId;

    $this->pipeline->add(new Task(UserKtpUpdater::class, 'updateStatus'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function getKtpByUser() {
    $this->pipeline->add(new Task(UserKtpSelector::class, 'getByUser'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getLastKtpStatusByUser() {
    $this->pipeline->add(new Task(UserKtpSelector::class, 'getLastStatusByUser'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function isUserKtpVerified() {
    $this->pipeline->add(new Task(UserKtpSelector::class, 'isUserKtpVerified'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getUserKtpHistory($userId) {
    $data = $this->getRequestData();
    $data['user_id'] = $userId;

    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(UserKtpSelector::class, 'getUserKtpHistory'));
    return $this->executeAndResponse($data);
  }

}
