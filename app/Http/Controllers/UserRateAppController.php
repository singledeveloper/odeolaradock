<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/7/16
 * Time: 11:41 PM
 */
namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Marketing\Rateapp\RateRequester;

class UserRateAppController extends Controller {
    
  public function __construct() {
    parent::__construct();
  }

  public function rate() {

    list($isValid, $data) = $this->validateData([
      'confirmation_name' => 'required',
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(RateRequester::class, 'request'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }

}