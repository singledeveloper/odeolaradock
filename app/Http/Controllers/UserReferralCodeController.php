<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/3/17
 * Time: 4:26 PM
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Core\Task;
use Odeo\Domains\Marketing\Userreferral\UserReferralRequester;

class UserReferralCodeController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getReferralCode() {

    $data = $this->getRequestData();
    $this->pipeline->add(new Task(UserReferralRequester::class, 'getReferralCode'));

    return $this->executeAndResponse($data);

  }

  public function redeemReferralCode() {

    $data = $this->getRequestData();
    $this->pipeline->add(new Task(UserReferralRequester::class, 'redeemReferralCode'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }
}