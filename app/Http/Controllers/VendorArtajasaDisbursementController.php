<?php

namespace Odeo\Http\Controllers;


use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\BalanceSelector;
use Odeo\Domains\Vendor\ArtajasaDisbursement\BalanceUpdater;
use Odeo\Domains\Vendor\ArtajasaDisbursement\InternalRefundRequester;
use Odeo\Domains\Vendor\ArtajasaDisbursement\TransferRecordSelector;

class VendorArtajasaDisbursementController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getArtajasaRecord() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(TransferRecordSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function getInformation() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(BalanceSelector::class, 'getBalance', [
      'id' => VendorDisbursement::ARTAJASA
    ]));
    return $this->executeAndResponse($data);
  }

  public function updateBalance() {

    $data = $this->getRequestData();

    $this->pipeline->add(new Task(BalanceUpdater::class, 'updateBalance'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }

  public function internalRefund() {
    list ($isValid, $data) = $this->validateData([
      'account_number_id' => 'required',
      'amount' => 'required|numeric|min:50000|max:50000000',
      'password' => 'required',
    ]);

    if (!$isValid) {
      return $data;
    }

    $this->pipeline->add(new Task(InternalRefundRequester::class, 'request'));
    return $this->executeAndResponse($data);
  }

  public function getSuspectDisbursements() {
    $this->pipeline->add(new Task(TransferRecordSelector::class, 'getSuspectDisbursements'));
    return $this->executeAndResponse();
  }

}