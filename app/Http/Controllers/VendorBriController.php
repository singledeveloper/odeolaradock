<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 28/06/19
 * Time: 11.35
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\BalanceSelector;
use Odeo\Domains\Vendor\Bri\TransferRecordSelector;

class VendorBriController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getInformation() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(BalanceSelector::class, 'getBalance', [
      'id' => VendorDisbursement::BRI
    ]));
    return $this->executeAndResponse($data);
  }

  public function getBriRecord() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(TransferRecordSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function getSuspectDisbursements() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(TransferRecordSelector::class, 'getSuspectDisbursements'));
    return $this->executeAndResponse($data);
  }

}