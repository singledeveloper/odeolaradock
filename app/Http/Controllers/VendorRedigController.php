<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/8/17
 * Time: 5:35 PM
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\BalanceSelector;
use Odeo\Domains\Vendor\Redig\RedigDisbursementSelector;

class VendorRedigController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getRedigRecord() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(RedigDisbursementSelector::class, 'get', [
      'expand' => 'bank,inquiry_request,transfer_request',
    ]));
    return $this->executeAndResponse($data);
  }

  public function getInformation() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(BalanceSelector::class, 'getBalance', [
      'id' => VendorDisbursement::REDIG
    ]));
    return $this->executeAndResponse($data);
  }

}