<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Subscription\WarrantyRequester;
use Odeo\Domains\Subscription\WarrantySelector;
use Odeo\Domains\Subscription\StoreVerificator;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;

class WarrantyController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function index($storeId) {
    $data = $this->getRequestData();
    $data["store_id"] = $storeId;
    $data["currency"] = Currency::IDR;
    $data['expand'] = "order";
    $data['store_must_active'] = true;
    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'allow_mentor' => true
    ]));
    $this->pipeline->add(new Task(WarrantySelector::class, 'getDetail'));
    return $this->executeAndResponse($data);
  }

  public function create() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required|exists:stores,id',
      'info.name' => 'required',
      'info.phone_number' => 'required',
      'info.address' => 'required',
      'info.province' => 'required',
      'info.city' => 'required',
      'info.zip_code' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['store_must_active'] = true;
    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->add(new Task(WarrantyRequester::class, 'create'));
    $this->pipeline->add(new Task(CartRemover::class, 'clear'));
    $this->pipeline->add(new Task(CartInserter::class, 'addToCart', ['service_detail_id' => ServiceDetail::WARRANTY_ODEO]));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }
}
