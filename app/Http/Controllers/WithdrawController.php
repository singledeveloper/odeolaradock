<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\Helper\AdminChecker;
use Odeo\Domains\Approval\ApprovalSelector;
use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Withdrawal\MarkAsCancelledRequester;
use Odeo\Domains\Disbursement\Withdrawal\MarkAsSuccessRequester;
use Odeo\Domains\Disbursement\Withdrawal\RetryRequester;
use Odeo\Domains\Transaction\WithdrawCanceller;
use Odeo\Domains\Transaction\WithdrawRequester;
use Odeo\Domains\Transaction\WithdrawSelector;

class WithdrawController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function detail() {
    $this->pipeline->add(new Task(WithdrawSelector::class, 'getDetail'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function findWithdraw($withdrawId) {
    $data = $this->getRequestData();
    $data['withdraw_id'] = $withdrawId;
    $this->pipeline->add(new Task(WithdrawSelector::class, 'find'));
    return $this->executeAndResponse($data);
  }

  public function withdraw() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $data["expand"] = "user,cash,clearing";
    $data["all"] = true;
    $this->pipeline->add(new Task(WithdrawSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function validateWithdraw() {
    $this->pipeline->add(new Task(WithdrawRequester::class, 'validateWithdraw'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getConfirmationData() {
    list($isValid, $data) = $this->validateData([
      'cash_amount' => 'required|numeric|min:10000|max:50000000',
      'bank_account_id' => 'required_without:bank_id|exists:user_bank_accounts,id',
      'bank_id' => 'required_without:bank_account_id|exists:banks,id'
    ]);
    
    if (!$isValid) return $data;
    $this->pipeline->add(new Task(WithdrawRequester::class, 'getConfirmationData'));
    return $this->executeAndResponse($data);
  }

  public function createWithdraw() {
    list($isValid, $data) = $this->validateData([
      'bank_account_id' => 'required_without_all:bank_id,account_number,account_name|exists:user_bank_accounts,id',
      'bank_id' => 'required_without:bank_account_id|exists:banks,id',
      'account_number' => 'required_without:bank_account_id',
      'account_name' => 'required_without:bank_account_id',
      'cash.amount' => 'required|numeric|min:10000|max:50000000',
      'cash.currency' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(ApprovalSelector::class, 'guardNonMakerEndpoint', ['path' => Approval::PATH_WITHDRAW]));
    $this->pipeline->add(new Task(WithdrawRequester::class, 'request'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function createWithdrawMaker() {
    list($isValid, $data) = $this->validateData([
      'bank_account_id' => 'required_without_all:bank_id,account_number,account_name|exists:user_bank_accounts,id',
      'bank_id' => 'required_without:bank_account_id|exists:banks,id',
      'account_number' => 'required_without:bank_account_id',
      'account_name' => 'required_without:bank_account_id',
      'cash.amount' => 'required|numeric|min:10000|max:50000000',
      'cash.currency' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(WithdrawRequester::class, 'request'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function completeWithdraw() {
    list($isValid, $data) = $this->validateData([
      'user_id' => 'required',
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(WithdrawRequester::class, 'complete'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData());
  }

  public function retryAutoWithdraw() {
    $this->pipeline->add(new Task(RetryRequester::class, 'retry'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData());
  }

  public function cancelWithdraw() {
    $this->pipeline->add(new Task(WithdrawCanceller::class, 'cancel'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData());
  }

  public function markAsCancel() {
    $this->pipeline->add(new Task(MarkAsCancelledRequester::class, 'markAsCancel'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData());
  }

  public function markAsSuccess() {
    $this->pipeline->add(new Task(MarkAsSuccessRequester::class, 'markAsSuccess'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData());
  }

  public function userWithdrawReport() {
    $data = $this->getRequestData();
    $tempPhones = explode(';', $data['phone_no']);

    $data['phone_no'] = [];
    foreach ($tempPhones as $phone) {
      $data['phone_no'][] = purifyTelephone(trim($phone));
    }

    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse();
    }

    return (new WithdrawSelector())->exportUserWithdrawReport($data);
  }

}
