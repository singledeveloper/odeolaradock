<?php

namespace Odeo\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Storage;
use Odeo\Domains\Account\Jobs\SaveAccessLogs;
use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Approval\Repository\ApprovalPendingRequestRepository;
use Odeo\Domains\Approval\Repository\ApprovalUserRepository;

class AuthenticateApproval {

  use \Odeo\Domains\Core\IO;

  public function handle($request, Closure $next) {

    $route = $request->route()[1];
    if (isset($route['as'])) {
      $approvalType = $route['as'];
      if (!Approval::isValidPath($approvalType)) {
        return $this->buildErrorsResponse('Approval type not valid', 400);
      }

      $approvalUserRepo = app()->make(ApprovalUserRepository::class);
      list($isValid, $data) = $this->validateData([
        (isset($route[Approval::PARAM_FILE]) ? $route[Approval::PARAM_FILE] : 'file') => 'max:102400'
      ]);

      if (!$isValid) return $data;

      $user = $approvalUserRepo->findMakerPath($data['auth']['user_id'], $approvalType);
      if (Approval::isPathNeedApprover($approvalType) && !$user) {
        return $this->buildErrorsResponse('Route need approver', 400);
      }

      if ($user) {
        \DB::beginTransaction();

        try {
          $file = isset($route[Approval::PARAM_FILE]) ? $route[Approval::PARAM_FILE] : 'file';
          $fileType = isset($route[Approval::PARAM_FILE_TYPE]) ? $route[Approval::PARAM_FILE_TYPE] : 'file_type';

          if (isset($data[$file])) {
            $temp = [];
            foreach ($data as $key => $item) {
              if (is_string($item) && $encodedItem = json_decode($item, true)) {
                $temp[$key] = $encodedItem;
              }
              else $temp[$key] = $item;
            }
            $data = $temp;
          }

          $approvalPendingRequestRepo = app()->make(ApprovalPendingRequestRepository::class);
          $request = $approvalPendingRequestRepo->getNew();
          $request->raw_data = json_encode($data);
          $request->path = $approvalType;
          $request->requested_by = $data['auth']['user_id'];
          $request->need_approver_from = $user->approver_user_id;
          $request->status = Approval::CREATED;
          $approvalPendingRequestRepo->save($request);

          if (isset($data[$file])) {
            $dataFileType = isset($data[$fileType]) ? $data[$fileType] : 'unknown';
            $cashTransactionInformationRepo = app()->make(\Odeo\Domains\Accounting\CompanyTransactionInformation\Repository\CompanyTransactionAdditionalInformationRepository::class);
            $url = (isProduction() ? 'transaction-info' : 'temp_transaction') . '/' . $data['auth']['user_id'] . '_' . $dataFileType . '_' . time() . '.' . $data[$file]->getClientOriginalExtension();
            if (!Storage::disk('s3')->put($url, file_get_contents($data[$file]), 'private')) {
              return $this->buildErrorsResponse('Fail to upload the file. Please try again');
            }
            else {
              $information = $cashTransactionInformationRepo->getNew();
              $information->image_url = $url;
              $information->type = $dataFileType;
              $information->reference_type = 'approval';
              $information->reference_id = $request->id;
              $cashTransactionInformationRepo->save($information);
            }
          }
        }
        catch (\Exception $e) {
          \DB::rollback();
          return $this->buildErrorsResponse($e->getMessage());
        }
        \DB::commit();

        return $this->buildResponse(200);
      }
      return $next($request);
    }
    return $this->buildErrorsResponse('Route not set in approval');
  }
}
