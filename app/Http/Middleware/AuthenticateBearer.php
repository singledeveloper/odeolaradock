<?php

namespace Odeo\Http\Middleware;

use Closure;
use Odeo\Domains\Account\Logout;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Account\TokenManager;

class AuthenticateBearer {

  use \Odeo\Domains\Core\IO;

  public function handle($request, Closure $next, $roles = '') {
    $data = getAuthorizationBearer();
    $data["roles"] = $roles;

    $pipeline = new Pipeline;
    $pipeline->add(new Task(TokenManager::class, 'check'));
    $pipeline->add(new Task(Logout::class, 'validateHasLogout'));
    $pipeline->execute($data);

    if ($pipeline->fail()) return $this->buildErrorsResponse($pipeline->errorMessage, $pipeline->statusCode);

    $request->merge(["auth" => $pipeline->data]);
    return $next($request);
  }
}
