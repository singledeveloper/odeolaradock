<?php

namespace Odeo\Http\Middleware;

use Closure;

class AuthenticateInternal {

  use \Odeo\Domains\Core\IO;

  public function handle($request, Closure $next) {
    $authorization = $request->header('Authorization');
    if (!$authorization) {
      return $this->buildErrorsResponse("Authorization header is empty");
    }

    $signingKey = env('INTERNAL_API_SIGNING_KEY');
    if (!$signingKey) {
      return $this->buildErrorsResponse('Unauthorized', 401);
    }

    $body = $request->getContent();
    $signature = hash_hmac('sha256', $body, $signingKey);

    if ($signature != $authorization) {
      return $this->buildErrorsResponse('Unauthorized', 401);
    }

    return $next($request);
  }
}
