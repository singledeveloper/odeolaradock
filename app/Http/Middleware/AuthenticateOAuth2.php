<?php

namespace Odeo\Http\Middleware;

use Closure;
use Odeo\Domains\Account\Logout;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\OAuth2\OAuth2TokenChecker;

class AuthenticateOAuth2 {

  use \Odeo\Domains\Core\IO;

  public function handle($request, Closure $next, $allowedScope) {
    $data = getAuthorizationBearer();
    $data['allowed_scope'] = $allowedScope;
    $data['headers'] = $request->headers->all();
    $data['body'] = $request->all();
    $data['method'] = $request->getMethod();
    $data['path'] = $request->getPathInfo();

    $pipeline = new Pipeline;
    $pipeline->add(new Task(OAuth2TokenChecker::class, 'check'));
    $pipeline->execute($data);
    if ($pipeline->fail()) {
      return $this->buildErrorsResponse($pipeline->errorMessage, 401);
    }

    $request->merge(["oauth2" => $pipeline->data]);
    return $next($request);
  }
}
