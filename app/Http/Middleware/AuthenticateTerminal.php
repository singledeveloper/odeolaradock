<?php

namespace Odeo\Http\Middleware;

use Closure;
use Odeo\Domains\Account\TokenManager;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Odeo\Terminal\TerminalGuard;

class AuthenticateTerminal {

  use \Odeo\Domains\Core\IO;

  public function handle($request, Closure $next) {
    list($isValid, $data) = $this->validateData([
      'terminal_id' => 'required',
    ]);
    if (!$isValid) return $data;

    $pipeline = new Pipeline;
    $pipeline->add(new Task(TerminalGuard::class, 'guard'));
    $pipeline->add(new Task(TokenManager::class, 'setUserFromTerminal'));
    $pipeline->execute($data);

    if ($pipeline->fail()) return $this->buildErrorsResponse($pipeline->errorMessage, $pipeline->statusCode);

    $request->merge(["auth" => $pipeline->data]);
    return $next($request);
  }
}
