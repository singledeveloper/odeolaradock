<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/announcements', 'AnnouncementController@getActive');

$app->get('/plans', 'PlanController@getPlans');
$app->get('/plans/marketing', 'PlanController@getMarketingPlans');

$app->post('/user/request-otp-email', 'UserController@requestOtpEmail');
$app->post('/user/sendVerification', 'UserController@sendVerification');
$app->post('/user/verify', 'UserController@verify');
$app->post('/user/otp/reset-password', 'UserController@resetPasswordByOtp');
$app->post('/user/otp/send-verification/sign-up', 'UserController@sendVerificationSignUp');
$app->post('/user/otp/send-verification/forgot-password', 'UserController@sendVerificationForgotPassword');
$app->post('/user/otp/verify/before-login', 'UserController@verify');
$app->post('/user/register', 'UserController@register');
$app->post('/user/checkPhoneNumber', 'UserController@checkPhoneNumber');
$app->post('/user/login', 'UserController@login');
$app->post('/user/guestLogin', 'UserController@guestLogin');
$app->post('/user/ktp/reupload', 'UserKtpController@reuploadKtp');

$app->post('/user/resetPassword', 'UserController@resetPassword');
$app->post('/user/resetPin', 'UserController@resetPin');
$app->post('/user/refreshToken', 'UserController@refreshToken');
$app->post('/user/validate/email', 'UserController@validateEmail');
$app->post('/user/sendVerification/email', 'UserController@sendVerificationEmail');
$app->get('/user/verify/email/{token}', 'UserController@verifyEmail');

$app->post('/oauth2/token', "OAuth2Controller@requestToken");

$app->get('/provinces', 'ProvinceController@get');
$app->get('/provinces/{province_id}/cities', 'ProvinceController@getCities');
$app->get('store-inventory-pricing/get-update-duration', 'StoreInventoryPricingController@getUpdateDuration');
$app->get('/banks', 'BankController@index');
$app->get('/faq', 'UserController@faq');

$app->get('/store/{domain_name}/informations', 'StoreController@getPubSettings');
$app->get('/store/{domain_name}/domain-description', 'StoreController@getDomainDescription');
$app->post('/store/adsClick', 'MarketingController@adsClick');
$app->get('/queues', 'MarketingController@getQueue');

$app->get('/pulsa/list', 'PulsaController@getList');
$app->get('/pulsa/postpaid-list', 'PulsaController@getPostpaidList');

$app->get('/pulsa/tnc', 'PulsaController@getTnC');
$app->get('/pulsa-data/tnc', 'PulsaController@getTnC');
$app->get('/pln/tnc', 'PlnController@getTnC');
$app->get('/bpjs-kes/tnc', 'BPJSController@getBpjsKesTnC');
$app->get('/bpjs-tk/tnc', 'BPJSController@getBpjsTkTnC');
$app->get('/transportation/tnc', 'TransportationController@getTnC');
$app->get('/game-voucher/tnc', 'GameVoucherController@getTnC');
$app->get('/google-play/tnc', 'GameVoucherController@getGooglePlayTnC');
$app->get('/google-play/redeem-instructions', 'GameVoucherController@getGooglePlayRedeemInstructions');

$app->post('/pulsa/datacell/test', 'BillerDatacellController@test');
$app->post('/pulsa/datacell/verify', 'BillerDatacellController@notify');
$app->get('/pulsa/datacell/refund', 'BillerDatacellController@notifyRefund');

$app->get('/pulsa/bakoel/refund', 'BillerBakoelController@notifyRefund');

$app->post('/pulsa/servindo/notify', 'BillerServindoController@notify');

$app->post('/pulsa/javah2h/notify', 'BillerJavaH2HController@notify');

$app->get('/pulsa/{slug}/notify', 'SupplyController@notify');
$app->post('/pulsa/{slug}/notify', 'SupplyController@notify');

$app->post('/payment/doku/notify', 'PaymentController@dokuNotify');
$app->post('/payment/doku/inquiry', 'PaymentController@dokuInquiry');
$app->post('/payment/doku/checkout', 'PaymentController@dokuBackToMerchant');
$app->post('/payment/doku/va/inquiry', 'DokuPaymentController@vaInquiry');
$app->post('/payment/doku/va/payment', 'DokuPaymentController@vaPayment');

$app->post('/payment/prismalink/notify', 'PrismalinkPaymentController@prismalinkNotify');
$app->post('/payment/prismalink/inquiry', 'PrismalinkPaymentController@prismalinkInquiry');
$app->post('/payment/prismalink/reversal', 'PaymentController@prismalinkReversal');
$app->post('/payment/prismalink/va/inquiry', 'PrismalinkPaymentController@vaInquiry');
$app->post('/payment/prismalink/va/payment', 'PrismalinkPaymentController@vaPayment');

$app->post('/payment/permata/va/inquiry', 'PermataPaymentController@vaInquiry');
$app->post('/payment/permata/va/payment', 'PermataPaymentController@vaPayment');
$app->post('/payment/permata/va/reversal', 'PermataPaymentController@vaReversal');

$app->post('/payment/maybank/va/inquiry', 'MaybankPaymentController@vaInquiry');
$app->post('/payment/maybank/va/payment', 'MaybankPaymentController@vaPayment');

$app->post('/payment/kredivo/notify', 'PaymentController@kredivoNotify');
$app->get('/payment/pay', 'PaymentController@requestPayment');
$app->get('/payment/direct/pay', 'DirectPaymentController@addNewDirectPayment');

$app->get('/payment/dam/mandiri/ecash/return-url', 'PaymentController@damRedirectUrl');
$app->post('/payment/dam/mandiri/ecash/return-url', 'PaymentController@damNotify');

$app->post('/payment/akulaku/notify', 'PaymentController@akulakuNotify');
$app->get('/payment/akulaku/checkout', 'PaymentController@akulakuRedirect');

$app->post('/payment/cimb/echo', 'CimbPaymentController@cimbEcho');
$app->post('/payment/cimb/inquiry', 'CimbPaymentController@cimbInquiry');
$app->post('/payment/cimb/notify', 'CimbPaymentController@cimbNotify');

$app->get('/payment/dam/mandiri/mpt/return-url', 'PaymentController@damMandiriMptNotify');
$app->get('/payment/redirect', 'PaymentController@showPaymentStatus');

$app->post('/payment/artajasa/va/inquiry', 'ArtajasaPaymentController@vaInquiry');
$app->post('/payment/artajasa/va/payment', 'ArtajasaPaymentController@vaPayment');

$app->post('/vendor/flip/notify', 'VendorFlipController@notify');
$app->post('/vendor/flip/update-balance', 'VendorFlipController@UpdateBalance');

$app->post('/vendor/artajasa-disbursement/update-balance', 'VendorArtajasaDisbursementController@UpdateBalance');

$app->get('/report/pln-postpaid', 'PostpaidSwitcherController@plnReport');

$app->get('/report/bpjs', 'PostpaidSwitcherController@bpjsReport');

$app->post('/zendesk-jwt-auth', 'ZendeskController@ZendeskJwtAuthentication');

$app->get('/ping', function () {
  $clientIP = app()->make(\Illuminate\Http\Request::class)->ip() ?? 'kosong';
  echo env('SERVER_NICKNAME') . ' is on healthy condition with IP' . $clientIP;
});

$app->get('/http-header', function (\Illuminate\Http\Request $request) {
  //Health Check ELB
  return response()->json([
    'data' => $request->headers->all(),
    'header' => $request->header('CF-IPCountry')
  ]);
});

$app->get('/test-notification', function () {
  return app()->make(\Odeo\Domains\Notification\Helper\PushSender::class)->test();
});

$app->post('/nexmo/notify', 'NexmoController@notify');
$app->post('/nexmo/inbound', 'NexmoController@inbound');

$app->post('/bot/telegram/{token}', 'TelegramController@parse');

$app->get('/sms-center/{sms_center_id}/status', 'SMSController@getStatus');
$app->get('/sms-center/bypass-outbound', 'SMSController@bypassOutbound');

$app->post('/affiliate/test-notify', function () {
  \Log::notice('NOTIFY: ' . app()->make(\Illuminate\Http\Request::class)->getContent());
});

$app->get('/deposit-cashback/preset/{store_id}', 'CampaignController@getDepositCashbackPreset');

$app->get('/invoice-temp-ff/{order_id}', function ($orderId) {
  $response = [];
  $order = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class)->findById($orderId);
  $this->orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);
  $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  $this->paymentChannels = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);
  $paymentData = $this->paymentChannels->findById($order->payment->opc);
  $response['cart_data']['items'] = $this->orderDetailizer->detailizeItem($order)[0];
  $response['payment_data'] = $paymentData->information->toArray();

  return view('emails.temporary_invoice', ['data' => array_merge([
    'created_at' => $order->created_at,
    'paid_at' => $order->paid_at,
    'closed_at' => $order->closed_at,
    'order_id' => $order->id,
    'total' => $currencyHelper->formatPrice($order->total)
  ], $response)]);
});

$app->post('/test-supply-purchase', function () {
  $request = app()->make(\Illuminate\Http\Request::class)->getContent();
  \Log::notice('SUPPLY DATA: ' . $request);
  $data = json_decode($request);
  //echo 'OK';
  echo \Odeo\Domains\Constant\BillerVhancell::getXMLResponse([
    'NOHP' => $data->msisdn,
    'REQUESTID' => $data->requestid,
    'NOM' => $data->code
  ]);
});

$app->post('/test-capture-pulsa-notify', function () {
  \Log::notice('NOTIFY DATA: ' . app()->make(\Illuminate\Http\Request::class)->getContent());
});

$app->get('/test-supply-inquiry', function (\Illuminate\Http\Request $request) {
  $data = $request->all();
  echo 'XL 5 10 TURUN HARGA R#3685768 HTHD3.0895414344042 akan diproses @12062110. 
  Saldo Awal Rp.2.721.414 - Harga Rp.47.800 = Saldo Akhir Rp.2.673.614 
  ID Center Tlegram chat cs Total hari ini 102 Trx';

  dispatch((new \Odeo\Domains\Supply\Jobs\TestInquiryNotify($data['refID']))->delay(5));
});

$app->post('/test/pg/capture-notify', function (\Illuminate\Http\Request $request) {
  $data = $request->all();
  switch ($data['notify_type']) {
    case 'va_inquiry':
      if (isProduction() && substr($data['va_code'], -6) != '000999')
        return response('wrong va number', 400);

      return response()->json([
        "customer_name" => "gerard edwin",
        "item_name" => "test",
        "amount" => 10000,
        "display_text" => "test payment"
      ]);
    case 'va_payment':
      if (isProduction() && substr($data['va_code'], -6) != '000999')
        return response('wrong va number', 400);

      return response()->json([
        "reference_id" => "test" . time(),
      ]);
    case 'payment_status':
      return response()->json([
        "success" => true,
      ]);
  }
});

$app->post('prismalink/test/notify', function (\Illuminate\Http\Request $request) {
  if (isProduction()) {
    return null;
  }
  clog('pg_test', 'from notify headers: ' . json_encode($request->headers->all()));
  clog('pg_test', 'from notify: ' . json_encode($request->all()));

  $data = $request->all();
  $header = $request->headers->all();

  $hash = base64_encode(hash('sha256', json_encode($data), true));
  $rawSign = "POST:/v1/prismalink/test/notify:::{$header['x-odeo-timestamp'][0]}:{$hash}";
  $signature = base64_encode(hash_hmac('sha256', $rawSign, env("OAUTH2_SIGNING_KEY_FOR_PG_TESTING"), true));

  clog('pg_test', "raw data: " . json_encode($data));
  clog('pg_test', "raw sign: {$rawSign}");
  clog('pg_test', "hashed sign: {$signature}");

  if ($signature != $header['x-odeo-signature'][0]) {
    return response('', 400);
  }

  switch ($data['notify_type']) {
    case 'va_inquiry':
      return response()->json([
        "customer_name" => "ODEO DEVELOPMENT",
        "item_name" => "ITEM TESTING",
        "amount" => "1000000",
        "display_text" => "DISPLAY TESTING"
      ]);
    case 'va_payment':
      return response()->json([
        "reference_id" => "yiha135" . time(),
      ]);
    case 'payment_status':
      return response()->json([
        "success" => true,
      ]);
  }
});

$app->post('prismalink/test/checkstatus', 'PrismalinkPaymentTestController@checkStatus');
$app->post('doku/test/checkstatus', 'DokuPaymentController@checkStatusTest');

$app->get('/test/pg/{encoded_client_id}', 'PaymentGatewayTestController@getTestPage');
$app->post('/test/pg/notify-inquiry', 'PaymentGatewayTestController@notifyInquiry');
$app->post('/test/pg/notify-payment', 'PaymentGatewayTestController@notifyPayment');
$app->post('/test/pg/notify-status', 'PaymentGatewayTestController@notifyStatus');

$app->get('test-lala/{date}', function ($date) {
  $notfoundMul = [];
  $updatesMul = 0;
  if ($date != 'none') {
    $date = Carbon::createFromFormat('Y-m-d', $date);
    list($notfound, $count) = (new \Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\BriUpdateRecon($date, $date))->check();
    $updatesMul += $count;
    $notfoundMul = array_merge($notfoundMul, $notfound);
  } else {
    $startDate = Carbon::createFromFormat('Y-m-d', '2018-05-01');
    $endDate = $startDate->copy()->endOfMonth();
    $date = $startDate->copy();
    while ($date->lessThan($endDate)) {
      list($notfound, $count) = (new \Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\BriUpdateRecon($date, $date))->check();
      $date->addDay(1);
      $updatesMul += $count;
      $notfoundMul = array_merge($notfoundMul, $notfound);
    }
  }

  if (!empty(($notfoundMul))) {
    clog('update_bri', 'not found: ' . json_encode($notfoundMul));
  }

  if (!empty($updatesMul)) {
    clog('update_bri', 'updated: ' . $updatesMul);
  }
});

$app->post('mandiri-va', 'MandiriVADirectController@getInstance');

/*

$app->get('inq', function(){
  if (app()->environment() == 'production') return false;

  $tempDepositTobeInsert = [];
  $cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
  try {
    $storeId = 7;
    $cashInserter->add([
      'user_id' => 16237,
      'store_id' => $storeId,
      'trx_type' => \Odeo\Domains\Constant\TransactionType::ORDER_CONVERT,
      'cash_type' => \Odeo\Domains\Constant\CashType::ODEPOSIT,
      'amount' => -10000,
      'flag' => 1
    ]);
    $tempDepositDesc['flag'] = 1;

    $tempDepositTobeInsert[] = [
      'reference_id' => 1,
      'store_id' => $storeId,
      'type' => \Odeo\Domains\Constant\StoreTempDeposit::AGENT_SERVER_ORDER,
      'amount' => 10000,
      'sale_amount' => 10050,
      'description' => \json_encode($tempDepositDesc),
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s')
    ];
    $cashInserter->run();
  }
  catch(\Odeo\Domains\Transaction\Exception\InsufficientFundException $e) {
    $reasonFlag = $e->getMessage();
    $error = true;
    do {
      $cashInserter->removeRow('flag', $reasonFlag);
      $newTempDeposit = [];
      foreach ($tempDepositTobeInsert as $item) {
        $tempDepositDesc = json_decode($item['description'], true);
        if (isset($tempDepositDesc['flag']) && $tempDepositDesc['flag'] == $reasonFlag)
          continue;
        $newTempDeposit[] = $item;
      }
      $tempDepositTobeInsert = $newTempDeposit;
      try {
        $cashInserter->run();
        $error = false;
      }
      catch (\Odeo\Domains\Transaction\Exception\InsufficientFundException $e) {
        $reasonFlag = $e->getMessage();
      }
    } while ($error);
  }
  return [
    'temp_deposit' => $tempDepositTobeInsert
  ];
});*/

$app->get('send-asg-notif', function() {
  $orderIds = [6079373,6079355,6079349,6079291,6079258,6079206,6079074,6078846,6078810,6078748,6078711,6078650,6078627,6078621,6078612,6078513,6078499,6078443,6078419,6077999,6077912,6077876,6077838,6077822,6077793,6077409,6077390,6077320,6077318,6077293,6077271,6077255,6077216,6077162,6077160,6077144,6077142,6077137,6077116,6077113,6077056,6077011,6076979,6076880,6076869,6076759,6076652,6076575,6046821];
  $failedIds = [];

  foreach($orderIds as $orderId) {
  $pipeline = app()->make(\Odeo\Domains\Core\Pipeline::class);

  $pipeline->add(new \Odeo\Domains\Core\Task(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Requester::class, 'sendNotification'));

  $pipeline->enableTransaction();
  $pipeline->execute([
      'order_id' => $orderId
  ]);

  if ($pipeline->fail()) {
      $failedIds[] = $orderId;
    }
  }
  return response()->json(['failed_ids' => $failedIds], 200);

  return response()->json($pipeline->data, 200);
});

$app->get('/pushnotif', function(){
  if (app()->environment() != 'production') {
    $push = app()->make(\Odeo\Domains\Notification\Helper\PushSender::class);
    return $push->toUser([16903], [
      'message' => "Test messages " . time(),
      'click_action' => "localhost:3000"
    ]);
  }
});
