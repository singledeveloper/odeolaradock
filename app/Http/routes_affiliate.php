<?php

$app->get('affiliate/product/categories', 'AffiliateController@getProductCategories');
$app->get('affiliate/product/list', 'AffiliateController@getProductList');
$app->get('affiliate/product/price', 'AffiliateController@getProductPrice');
$app->get('affiliate/product/description', 'AffiliateController@getProductDescription');
$app->post('affiliate/prepaid/purchase', 'AffiliateController@purchasePrepaid');
$app->get('affiliate/prepaid/pln/inquiry', 'AffiliateController@inquiryPrepaidPln');
$app->get('affiliate/prepaid/inquiry', 'AffiliateController@inquiryPrepaid');
$app->get('affiliate/postpaid/inquiry', 'AffiliateController@inquiryPostpaid');
$app->post('affiliate/postpaid/purchase', 'AffiliateController@purchasePrepaid');
$app->get('affiliate/order/{order_id}', 'AffiliateController@checkOrderStatus');
$app->post('affiliate/sms', 'SMSController@affiliateSendSMS');

$app->get('affiliate/prepaid/purchase-{api_type}', 'AffiliateController@purchasePrepaid');
$app->post('affiliate/prepaid/purchase-{api_type}', 'AffiliateController@purchasePrepaid');

$app->post('affiliate/invoice/inquiry', 'AffiliateInvoiceController@inquiry');
$app->post('affiliate/invoice/pay', 'AffiliateInvoiceController@pay');
$app->post('affiliate/invoice/cancel-pay', 'AffiliateInvoiceController@cancelPay');
$app->get('affiliate/invoice/users', 'AffiliateInvoiceController@userList');
$app->get('affiliate/invoice', 'AffiliateInvoiceController@list');
$app->post('affiliate/invoice', 'AffiliateInvoiceController@create');
$app->post('affiliate/invoice/batch', 'AffiliateInvoiceController@batchUpload');
$app->post('affiliate/invoice/{invoice_number}/void', 'AffiliateInvoiceController@void');