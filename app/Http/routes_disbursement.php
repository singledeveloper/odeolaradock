<?php

$app->get('disbursement/balance', 'ApiDisbursementController@getBalance');
$app->get('disbursement/{id}', 'ApiDisbursementController@getDisbursementById');
$app->get('disbursement/reference-id/{referenceId}', 'ApiDisbursementController@getDisbursementByReferenceId');
$app->post('disbursement', 'ApiDisbursementController@createDisbursement');
$app->post('disbursement/inquiry', 'ApiDisbursementController@createDisbursementInquiry');
