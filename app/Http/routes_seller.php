<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/user/contactCheck', 'UserController@contactCheck');
$app->get('/user/stores', 'StoreController@userStore');
$app->get('/user/stores/hustler', 'StoreController@hustlerStore');
$app->get('/user/stores/team', 'StoreController@teamStore');
$app->get('/user/stores/masters/', 'StoreController@getAllMasterStores');
$app->get('/user/stores/available/{serviceId}', 'StoreController@getAvailableStores');
$app->get('/user/cash', 'CashController@cash');
$app->get('/user/cash/deposit', 'CashController@cashDepositRequest');
$app->post('/user/cash/transfer', 'CashController@transferOCash');
$app->post('/user/cash/pay', 'CashController@payOCash');
$app->get('/user/cash/latest-schedules', 'CashRecurringController@getLatestSchedules');
$app->get('/user/cash/schedules', 'CashRecurringController@getSchedules');
$app->get('/user/cash/schedule-histories', 'CashRecurringController@getHistories');
$app->post('/user/change-number/verify', 'UserController@verifyChangeNumber');
$app->post('/user/otp/send-verification', 'UserController@sendVerificationUpdatePin');
$app->post('/user/otp/send-email', 'UserController@sendOtpEmailSc');
$app->post('/user/otp/verify', 'UserController@verifyAfterLogin');
$app->post('/user/otp/send-verification/update-pin', 'UserController@sendVerificationUpdatePin');
$app->post('/user/otp/verify/after-login', 'UserController@verifyAfterLogin');
$app->post('/user/resetPin', 'UserController@resetPin');
$app->get('/user/checkPin', 'UserController@checkPin');
$app->get('/user/checkMaker', 'UserController@checkMaker');

$app->post('/pulsa/check-repeat', 'PulsaSwitcherController@checkRepeat');
$app->get('/pulsa/recurring/types', 'PulsaRecurringController@getTypes');
$app->get('/pulsa/recurring/scheduled', 'PulsaRecurringController@getScheduled');
$app->get('/pulsa/recurring/{recurring_id}/histories', 'PulsaRecurringController@getHistories');
$app->get('/pulsa/recurring/{recurring_id}', 'PulsaRecurringController@findById');
$app->get('/pulsa/recurring', 'PulsaRecurringController@get');
$app->post('/pulsa/recurring/toggle', 'PulsaRecurringController@toggle');
$app->post('/pulsa/recurring/remove', 'PulsaRecurringController@remove');
$app->post('/pulsa/recurring/update-price', 'PulsaRecurringController@updatePrice');
$app->get('/pulsa/recent-orders', 'PulsaSwitcherController@getRecent');
$app->get('/pulsa/operators', 'PulsaSwitcherController@getOperators');
$app->get('/pulsa/categories', 'PulsaSwitcherController@getCategories');

$app->get('/user/topup', 'TopupController@topup');
$app->get('/user/topup/{topup_id}', 'TopupController@topup');
$app->post('/user/topup/create', 'TopupController@createTopup');

$app->get('/user/withdraw', 'WithdrawController@detail');
$app->post('/user/withdraw/validate', 'WithdrawController@validateWithdraw');
$app->get('/user/withdraw/confirm', 'WithdrawController@getConfirmationData');
$app->post('/user/withdraw/cancel', 'WithdrawController@cancelWithdraw');
$app->get('/user/withdraw/{withdraw_id}', 'WithdrawController@findWithdraw');

$app->get('/user/bankAccount', 'BankController@bankAccounts');
$app->post('/user/bankAccount/create', 'BankController@createBankAccount');
$app->post('/user/bankAccount/delete', 'BankController@deleteBankAccount');
$app->post('/user/bankAccount/check', 'BankController@checkBankAccount');

$app->get('/user/account', 'UserController@account');
$app->post('/user/account', 'UserController@updateAccount');
$app->post('/user/account/ktp', 'UserController@updateKtp');
$app->post('/user/account/password', 'UserController@changePassword');
$app->post('/user/account/pin', 'UserController@changePin');
$app->post('/user/account/transaction-pin', 'UserController@changeTransactionPin');
$app->post('/user/checkPassword', 'UserController@checkPassword');

$app->get('/user/dashboard', 'TransactionController@dashboard');
$app->post('/user/feedback', 'FeedbackController@createFeedback');
$app->post('/user/fcm/token', 'DeviceController@registerNewDeviceToken');
$app->delete('/user/fcm/token', 'DeviceController@removeDeviceToken');

$app->get('/user/notifications', 'UserController@notification');
$app->post('/user/notifications/status', 'UserController@notificationStatus');
$app->get('/user/notifications/balance', 'NotificationController@getBalanceNotification');
$app->post('/user/notifications/balance', 'NotificationController@saveBalanceNotification');
$app->get('/user/revenues', 'TransactionController@revenue');
$app->get('/user/transactions/{cash_transactions_id}', 'TransactionController@findCashTransaction');
$app->get('/user/recentTransactions', 'TransactionController@recent');
$app->get('/user/recentTransactions/export', 'TransactionController@export');
$app->get('/user/pending-transactions', 'TransactionController@pendingTransactions');
$app->get('/user/pending-transactions/export', 'TransactionController@pendingTransactionsExport');
$app->get('/user/pending-transactions-summary', 'TransactionController@getPendingTransactionSummary');

$app->get('/user/business-kyc/check', 'UserBusinessKycController@check');
$app->get('/user/business-kyc/details', 'UserBusinessKycController@get');
$app->post('/user/business-kyc/upload', 'UserBusinessKycController@upload');
$app->post('/user/business-kyc/submit', 'UserBusinessKycController@submit');
$app->post('/user/business-kyc/cancel', 'UserBusinessKycController@cancel');

$app->post('/cart/cashCheckout', 'CartController@cashCheckout');

$app->get('/store/availability/{subdomain}', 'SubscriptionController@checkSubdomain');
$app->post('/store/create', 'SubscriptionController@create');

$app->get('/store/dashboard', 'TransactionController@dashboardStore');
$app->get('/store/recentTransactions', 'TransactionController@recentStore');
$app->get('/store/recentTransactions/export', 'TransactionController@exportStore');
$app->post('/store/changePlan', 'SubscriptionController@change');
$app->post('/store/renewal', 'SubscriptionController@renewal');

$app->post('/store/markup', 'StoreController@markup');
$app->post('/store/updateSettings', 'SubscriptionController@updateSettings');
$app->post('/store/updateReferral', 'StoreController@updateReferral');
$app->get('/store/banners', 'SubscriptionController@getBanners');

$app->get('/store/active-store', 'StoreController@getFirstActiveStore');
$app->get('/store/{store_id}/owner/info', 'StoreController@getStoreOwnerInformation');
$app->get('/store/{store_id}', 'StoreController@getStoreDetail');
$app->get('/store/{store_id}/cash', 'StoreController@cash');
$app->get('/store/{store_id}/settings', 'SubscriptionController@getSettings');
$app->get('/store/{store_id}/inventories', 'StoreController@getInventories');
$app->get('/store/{store_id}/accounts', 'StoreController@getAccounts');
$app->get('/store/{store_id}/invitationCheck', 'NetworkController@checkInvitation');
$app->get('/store/{store_id}/channels', 'StoreController@getChannels');
$app->get('/store/{store_id}/lockedDeposits', 'StoreController@getLockedDeposit');
$app->post('/store/{store_id}/deposit/create', 'DepositController@createDeposit');
$app->get('/store/{store_id}/deposit', 'DepositController@index');

$app->get('/store/{store_id}/inventory/get-vendor-pricing', 'StoreInventoryController@getVendorPricing');
$app->get('/store/{store_id}/inventory/{service_detail_id}', 'StoreInventoryController@getStoreInventories');
$app->post('/store/{store_id}/inventory/{service_detail_id}/toggle', 'StoreInventoryController@toggleService');

$app->get('/store/{store_id}/inventory-pricing/{service_detail_id}', 'StoreInventoryPricingController@getPricing');
$app->get('/store/{store_id}/inventory-group-price/{service_detail_id}/compare', 'StoreInventoryPricingController@compareGroupPrice');
$app->post('/store/{store_id}/inventory-group-price/{service_detail_id}/toggle-auto-update', 'StoreInventoryPricingController@toggleGroupPriceAutoUpdateProfit');

$app->get('/store/{store_id}/md-plus', 'MdPlusController@index');
$app->get('/store/{store_id}/md-plus/referred', 'MdPlusController@referredUsers');
$app->post('/store/{store_id}/md-plus/toggle-auto-renew', 'MdPlusController@toggleAutoRenew');
$app->post('/store/md-plus/subscribe', 'MdPlusController@subscribe');

$app->post('/network/claimReferral', 'NetworkController@claimReferral');
$app->post('/network/assign',  'NetworkController@assign');
$app->get('/network/{store_id}/referred', 'NetworkController@referred');
$app->get('/network/{store_id}', 'NetworkController@getBranch');
$app->get('/network/{store_id}/info', 'NetworkController@getInfo');

$app->post('/warranty/create', 'WarrantyController@create');
$app->get('/warranty/{store_id}', 'WarrantyController@index');

$app->get('/md-plus/plans', 'MdPlusController@plans');
$app->get('/md-plus/information', 'MdPlusController@information');

$app->get('/channel', 'ChannelController@index');
$app->get('/channel/type', 'ChannelController@type');
$app->post('/channel/pay', 'ChannelController@pay');

$app->get('/payment/direct', 'DirectPaymentController@getDirectPaymentList');
$app->get('/directPayments', 'DirectPaymentController@getDirectPayments');
$app->post('/payment/direct/doku/submit', 'DirectPaymentController@paymentDirectDokuSubmit');
$app->post('/payment/direct/preference', 'DirectPaymentController@updateUserDirectPaymentPreferences');
$app->get('/order/store/{store_id}', 'OrderController@getByStore');
$app->get('/order/store/{store_id}/export', 'OrderController@exportByStore');
$app->get('/order/user', 'OrderController@getByUser');
$app->get('/order/user/export', 'OrderController@exportOrder');
$app->get('/order/filter', 'OrderController@getFilterContents');
$app->post('/order/confirm-receipt', 'OrderController@confirmReceipt');
$app->post('/order/update-rating', 'OrderController@updateRating');

$app->post('/user/rate/app', 'UserRateAppController@rate');

$app->get('user/referral/code', 'UserReferralCodeController@getReferralCode');
$app->post('user/referral/code', 'UserReferralCodeController@redeemReferralCode');

$app->get('/user/{user_id}/ktp', 'UserKtpController@getKtpByUser');
$app->get('/user/{user_id}/ktp-status', 'UserKtpController@getLastKtpStatusByUser');
$app->get('/user/{user_id}/ktp-status/verified', 'UserKtpController@isUserKtpVerified');

$app->get('/user/devices', 'DeviceController@devices');

$app->get('mobile/banner', 'MobileBannerController@get');

$app->post('app/identifier/register', 'AppIdentifierController@register');

$app->get('agent/master-information', 'AgentController@masterInformation');
$app->get('agent/master-list', 'AgentController@masterList');
$app->get('agent/priorities/{service_id}', 'AgentController@getPrioritiesByServiceId');
$app->get('store/{store_id}/master/information', 'AgentController@masterDashboard');
$app->get('store/{store_id}/master/agent', 'AgentController@getAgent');
$app->get('store/{store_id}/master/agent-pending', 'AgentController@getPendingAgent');
$app->get('store/{store_id}/master/bonus-setting', 'AgentController@masterGetBonusSetting');
$app->post('store/{store_id}/master/toggle-auto-approve-agent', 'AgentController@masterToggleAutoApproveAgent');
$app->post('store/{store_id}/master/agent-reject', 'AgentController@rejectAgent');
$app->get('/store/{store_id}/master/agent-group-price', 'AgentController@getAgentGroupPrice');
$app->get('/store/{store_id}/master/agent-group-price/{service_detail_id}', 'AgentController@getGroupPriceByUserService');
$app->post('/store/{store_id}/master/agent-group-price', 'AgentController@updateAgentGroupPrice');

$app->get('user/invoices', 'UserInvoiceController@userInvoiceList');
$app->get('invoice/categories', 'UserInvoiceController@invoiceCategories');
$app->get('store/{store_id}/invoice/information', 'UserInvoiceController@invoiceDashboard');
$app->get('store/{store_id}/invoice/list', 'UserInvoiceController@invoiceList');
$app->post('store/{store_id}/invoice/create', 'UserInvoiceController@create');
$app->post('store/{store_id}/invoice/void', 'UserInvoiceController@void');
$app->get('store/{store_id}/dist','DistributionChannelController@get');

$app->get('/user/receipt-config', 'UserReceiptConfigController@getConfig');
$app->post('/user/receipt-config', 'UserReceiptConfigController@updateConfig');
$app->get('/order/{order_id}/receipt/sheet', 'OrderController@getSheetReceipt');

$app->post('affiliate/config/set-api-type', 'UserAffiliateConfController@setApiType');
$app->get('affiliate/config/notify-url', 'UserAffiliateConfController@getNotifyUrl');
$app->get('affiliate/config/test-success-notify', 'UserAffiliateConfController@testSuccessNotify');
$app->get('affiliate/config/test-fail-notify', 'UserAffiliateConfController@testFailNotify');
$app->get('affiliate/config/test-new-price-notify', 'UserAffiliateConfController@testNewPriceNotify');
$app->get('affiliate/config/white-list', 'UserAffiliateConfController@getWhiteList');

$app->get('/jabber', 'JabberController@get');
$app->get('/jabber/list', 'JabberController@getList');
$app->post('/jabber/register', 'JabberController@register');
$app->post('/jabber/remove', 'JabberController@remove');
$app->post('/jabber/change-password', 'JabberController@changePassword');
$app->get('/jabber/reply-example', 'JabberController@getReplyExample');
$app->get('/jabber/command-example', 'JabberController@getCommandExample');
$app->post('/jabber/change-transaction-pin', 'JabberController@changeTransactionPin');

$app->post('/telegram/user/config/change-transaction-pin', 'TelegramController@changeUserTransactionPin');
$app->post('/telegram/user/config/create-transaction-pin', 'TelegramController@createUserTransactionPin');
$app->get('/telegram/user/config', 'TelegramController@getUserConfig');
$app->get('/telegram/command-example', 'TelegramController@getCommandExample');

$app->get('/sms-center/list', 'SMSController@getList');
$app->get('/sms-center/config', 'SMSController@getAll');
$app->get('/sms-center/config/{sms_center_id}', 'SMSController@get');
$app->post('/sms-center/register', 'SMSController@register');
$app->post('/sms-center/remove', 'SMSController@remove');
$app->post('/sms-center/change-transaction-pin', 'SMSController@changeTransactionPin');
$app->get('/sms-center/command-example', 'SMSController@getCommandExample');

$app->get('/supply/connection/list', 'SupplyController@getConnectionList');
$app->get('/supply/billers', 'SupplyController@getBillerList');
$app->get('/supply/options', 'SupplyController@getOptions');
$app->post('/supply/connection/new', 'SupplyController@registerConnection');
$app->get('/supply/connection/{biller_id}', 'SupplyController@getDetail');
$app->post('/supply/connection/{biller_id}/edit', 'SupplyController@editConnection');
$app->post('/supply/connection/{biller_id}/edit-password', 'SupplyController@editConnectionPassword');
$app->post('/supply/connection/{biller_id}/edit-pin', 'SupplyController@editConnectionPin');
$app->post('/supply/connection/{biller_id}/edit-token', 'SupplyController@editConnectionToken');
$app->post('/supply/connection/{biller_id}/toggle', 'SupplyController@toggleBiller');
$app->post('/supply/connection-test', 'SupplyController@testConnection');

$app->get('/supply/templates', 'SupplyTemplateController@getTemplates');
$app->post('/supply/template/preview', 'SupplyTemplateController@getTemplatePreviewExample');
$app->get('/supply/template/{template_id}/responses', 'SupplyTemplateController@getTemplateResponses');
$app->get('/supply/template/{template_id}/basic-responses', 'SupplyTemplateController@getTemplateBasicResponses');
$app->get('/supply/template/{template_id}', 'SupplyTemplateController@getTemplateDetail');
$app->post('/supply/template/new', 'SupplyTemplateController@createTemplate');
$app->post('/supply/template/{template_id}/edit', 'SupplyTemplateController@editTemplate');
$app->post('/supply/template/response-group/new', 'SupplyTemplateController@setResponseGroup');
$app->post('/supply/template/response-group/{template_response_id}/remove', 'SupplyTemplateController@removeResponseGroup');
$app->post('/supply/template/response/new', 'SupplyTemplateController@registerResponse');
$app->post('/supply/template/response/{template_response_detail_id}/remove', 'SupplyTemplateController@removeResponse');

$app->post('/supply/inventory/new', 'SupplyController@addInventory');
$app->get('/supply/marketplace/inventories', 'SupplyController@getMarketplaceInventories');
$app->post('/supply/marketplace/inventory/{inventory_id}/edit', 'SupplyController@editMarketplace');
$app->post('/supply/marketplace/inventory/{inventory_id}/remove', 'SupplyController@removeMarketplace');
$app->post('/supply/marketplace/add', 'SupplyController@sellToMarketPlace');
$app->post('/supply/inventory/{inventory_id}/edit', 'SupplyController@editInventory');
$app->post('/supply/inventory/{inventory_id}/toggle', 'SupplyController@toggleInventory');
$app->get('/supply/inventories', 'SupplyController@getBaseInventories');
$app->get('/supply/unpopulate-inventories', 'SupplyController@getUnpopulateInventories');

$app->get('/supply/orders', 'SupplyController@reconSupply');
$app->post('/supply/order/edit', 'SupplyController@editSupplyOrder');
$app->post('/supply/order/export', 'SupplyController@exportOrder');

$app->get('/supply/inventory-groups', 'SupplyAgentController@getInventoryGroups');
$app->post('/supply/inventory-group/new', 'SupplyAgentController@addInventoryGroup');
$app->post('/supply/inventory-group/edit', 'SupplyAgentController@editInventoryGroup');
$app->post('/supply/inventory-group/edit-agent', 'SupplyAgentController@editAgentGroup');
$app->get('/supply/inventory-group/{group_id}/details', 'SupplyAgentController@getInventoryGroupDetails');
$app->post('/supply/inventory-group/{group_id}/export', 'SupplyAgentController@exportPriceList');
$app->post('/supply/inventory-group/{group_id}/add-price-details', 'SupplyAgentController@addInventoryGroupPriceDetails');
$app->post('/supply/inventory-group/{group_id}/set-as-default', 'SupplyAgentController@setInventoryGroupAsDefault');
$app->post('/supply/inventory-group-detail/{group_detail_id}/edit', 'SupplyAgentController@editInventoryGroupPriceDetail');
$app->post('/supply/inventory-group-detail/{group_detail_id}/remove', 'SupplyAgentController@removeInventoryGroupPriceDetail');

$app->get('/supply/jabber/center/{store_id}', 'JabberController@getJabberStore');
$app->post('/supply/jabber/center/add', 'JabberController@addJabberStore');
$app->post('/supply/jabber/center/{store_id}/change-password', 'JabberController@changeJabberStorePassword');
$app->post('/supply/jabber/center/{store_id}/edit-status', 'JabberController@editJabberStoreStatus');

$app->post('/supply/gojek/otp', 'GojekController@requestOtp');
$app->post('/supply/gojek/token', 'GojekController@generateToken');
$app->post('/supply/gojek/pin', 'GojekController@setupPin');
$app->post('/supply/gojek/reset-uid', 'GojekController@resetUniqueId');
$app->post('/supply/gojek/balance', 'GojekController@checkBalance');
$app->post('/supply/gojek/telephone/change', 'GojekController@changeTelephone');
$app->post('/supply/gojek/telephone/verify-change', 'GojekController@verifyChangeTelephone');
$app->post('/supply/gojek/update-pin', 'GojekController@updatePin');
$app->post('/supply/gojek/remove', 'GojekController@remove');
$app->post('/supply/gojek/test', 'GojekController@test');
$app->get('/supply/gojek/histories', 'GojekController@getHistories');
$app->get('/supply/gojek/accounts', 'GojekController@getAccounts');
$app->get('/supply/gojek/summary', 'GojekController@getSummaries');

$app->post('/supply/tcash/otp', 'TCashController@requestOtp');
$app->post('/supply/tcash/otp/verify', 'TCashController@verifyOtp');
$app->post('/supply/tcash/login', 'TCashController@login');
$app->post('/supply/tcash/distribute', 'TCashController@distributeDeposit');
$app->post('/supply/tcash/test', 'TCashController@test');
$app->get('/supply/tcash/histories', 'TCashController@getHistories');
$app->get('/supply/tcash', 'TCashController@get');

$app->get('/biller/connection/logs', 'VendorSwitcherConnectionLogsController@get');

$app->get('/disbursement', 'DisbursementController@getDisbursements');
$app->get('/disbursement/{id}/notify-logs', 'DisbursementController@getDisbursementNotifyLogs');
$app->get('/disbursement/receipt/{id}', 'DisbursementController@getDisbursementById');
$app->get('/disbursement/export', 'DisbursementController@exportDisbursement');
$app->get('/disbursement/invoice-export', 'DisbursementController@exportInvoice');
$app->get('/disbursement/filter', 'DisbursementController@getFilterContents');
$app->get('/disbursement/inquiry', 'DisbursementController@getDisbursementInquiries');
$app->get('/disbursement/inquiry/export', 'DisbursementController@exportDisbursementInquiry');

$app->get('/disbursement/dashboard', 'DisbursementController@getDashboard');
$app->get('/disbursement/summary', 'DisbursementController@getSummary');
$app->get('/disbursement/summary/{period}', 'DisbursementController@getSummaryByPeriod');
$app->get('/disbursement/inquiry-summary', 'DisbursementController@getInquirySummary');
$app->get('/disbursement/settings', 'DisbursementController@getSettings');
$app->post('/disbursement/test-callback', 'DisbursementController@testCallback');

$app->get('/payment-gateway/settings', 'PaymentGatewayController@getPaymentGatewayUser');

$app->post('/batch-disbursement/{id}/cancel', 'BatchDisbursementController@cancel');
$app->post('/batch-disbursement/{id}/export', 'BatchDisbursementController@export');
$app->post('/batch-disbursement/{id}/export-failed', 'BatchDisbursementController@exportFailed');
$app->get('/batch-disbursement/{id}', 'BatchDisbursementController@getBatchDisbursement');
$app->get('/batch-disbursement/{id}/detail', 'BatchDisbursementController@getBatchDisbursementDetail');
$app->get('/batch-disbursement', 'BatchDisbursementController@getBatchDisbursements');
$app->post('/batch-disbursement', 'BatchDisbursementController@createBatchDisbursement');
$app->post('/batch-disbursement/{batch-id:\d+}/{disbursement-id:\d+}', 'BatchDisbursementController@updateDetail');
$app->post('/batch-disbursement/{batch-id:\d+}/{disbursement-id:\d+}/revalidate', 'BatchDisbursementController@revalidateDetail');
$app->post('/batch-disbursement/{batch-id:\d+}/{disbursement-id:\d+}/mark-as-success', 'BatchDisbursementController@markDetailAsValidationSuccess');
$app->delete('/batch-disbursement/{batch-id:\d+}/{disbursement-id:\d+}', 'BatchDisbursementController@deleteDetail');

$app->get('/user/favorite-number', 'FavoriteNumberController@getAll');
$app->get('/user/favorite-number/{service_id}', 'FavoriteNumberController@getByServiceId');
$app->post('/user/favorite-number/create', 'FavoriteNumberController@create');
$app->post('/user/favorite-number/remove', 'FavoriteNumberController@remove');

$app->get('/approvals', 'ApprovalController@get');

// to support older m-v3 version
$app->get('/user/purchase-favorite', 'FavoriteNumberController@getAll');
$app->get('/user/purchase-favorite/{service_id}', 'FavoriteNumberController@getByServiceId');
$app->post('/user/purchase-favorite/create', 'FavoriteNumberController@create');
$app->post('/user/purchase-favorite/remove', 'FavoriteNumberController@remove');

$app->post('/user/otp/send-verification/email', 'UserController@sendVerificationEmailOtp');
$app->post('/user/otp/verify/email', 'UserController@verifyEmailOtp');

$app->get('/user/transaction/limit-amount', 'TransactionController@getTopUpWithdrawLimitAndAmount');

$app->get('/pg/user-payment', 'PaymentGatewayController@getPaymentByUser');
$app->get('/pg/user-payment/filter', 'PaymentGatewayController@getFilterPaymentByUser');
$app->get('/pg/user-payment/export', 'PaymentGatewayController@exportPaymentByUser');
$app->get('/pg/user-payment/invoice-export', 'PaymentGatewayController@exportInvoiceByUser');
$app->get('/pg/payment/log/{payment_id}', 'PaymentGatewayController@getPaymentLog');
$app->post('/pg/payment/renotify', 'PaymentGatewayController@renotifyPayment');

$app->get('/user-invoice/settings', 'BusinessInvoiceController@getInvoiceUser');

$app->get('/user-invoice/users', 'BusinessInvoiceController@getUserInvoiceUsers');
$app->post('/user-invoice/users', 'BusinessInvoiceController@addInvoiceUser');
$app->get('/user-invoice/users/export', 'BusinessInvoiceController@exportInvoiceUsers');
$app->get('/user-invoice/user/{id:\d+}', 'BusinessInvoiceController@findInvoiceUser');
$app->post('/user-invoice/user/{id:\d+}', 'BusinessInvoiceController@updateInvoiceUser');
$app->get('/user-invoice/user/{id:\d+}/invoices', 'BusinessInvoiceController@getInvoiceByBilledUserId');
$app->get('/user-invoice/user/{id:\d+}/va', 'BusinessInvoiceController@getBilledUserVaCode');

$app->get('/user-invoice/invoices', 'BusinessInvoiceController@getUserInvoices');
$app->post('/user-invoice/invoices', 'BusinessInvoiceController@addInvoiceItems');
$app->post('/user-invoice/invoices/bulk', 'BusinessInvoiceController@insertBulkInvoice');
$app->get('/user-invoice/invoice/{id:\d+}', 'BusinessInvoiceController@findInvoice');
$app->get('/user-invoice/invoice/{id:\d+}/va', 'BusinessInvoiceController@getInvoiceVaCode');

$app->get('/business-reseller/agent-list', 'BusinessResellerController@getAgents');
$app->get('/business-reseller/agent/{reseller_id}', 'BusinessResellerController@getAgentDetails');

$app->post('/qiscus/jwt-auth', 'QiscusController@qiscusJwtAuthentication');
$app->post('/qiscus/record-qiscus-account', 'QiscusController@recordQiscusAccount');
$app->get('/qiscus/cs-odeo', 'QiscusController@getCsOdeoUserIds');
$app->get('/qiscus/chat-notification', 'QiscusController@getChatNotification');

$app->get('/bill-reminder/list', 'BillReminderController@getBillReminderByUserId');
$app->post('/bill-reminder/remove', 'BillReminderController@remove');

$app->get('/oauth2-client/check', 'OAuth2ClientController@checkOAuth2Client');

$app->get('/export-status/{export_id}', 'TransactionController@fetchExportStatus');

$app->get('/financial/cash-transaction-information/{cash_transaction_id}/details', 'CompanyCashController@getTransactionInformation');
$app->post('/financial/company-cash-account/{id}/create-transaction-information', 'CompanyCashController@createTransactionInformation');

$app->get('/pulsa/bulk-purchase/group/{pulsa_bulk_id}/summary', 'PulsaBulkPurchaseController@getGroupSummary');
$app->get('/pulsa/bulk-purchase/group/{pulsa_bulk_id}/details', 'PulsaBulkPurchaseController@getGroupDetails');
$app->get('/pulsa/bulk-purchase/group/{pulsa_bulk_id}/export', 'PulsaBulkPurchaseController@export');
