<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->post('/user/withdraw/create-maker', [
  'as' => \Odeo\Domains\Approval\Helper\Approval::PATH_WITHDRAW,
  'uses' => 'WithdrawController@createWithdrawMaker'
]);

$app->post('/user/cash/transfer-maker', [
  'as' => \Odeo\Domains\Approval\Helper\Approval::PATH_OCASH_TRANSFER,
  'uses' => 'CashController@transferOCashMaker'
]);