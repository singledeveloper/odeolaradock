<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->post('/user/cash/convert', 'CashController@convert');
$app->post('/user/withdraw/create', 'WithdrawController@createWithdraw');
$app->post('/user/bankAccount/update', 'BankController@updateBankAccount');
$app->post('/payment/direct/create', 'DirectPaymentController@addNewDirectPayment');
$app->post('/payment/direct/delete', 'DirectPaymentController@deleteDirectPayment');
$app->post('/user/{user_id}/ktp', 'UserKtpController@updateKtp');
$app->post('/user/change-number/begin', 'UserController@beginChangeNumber');

$app->post('/user/cash/schedule/{schedule_id}/toggle', 'CashRecurringController@toggleSchedule');
$app->post('/user/cash/schedule-history/{history_id}/retry', 'CashRecurringController@retryHistory');

$app->post('/user/devices/remove', 'DeviceController@remove');
$app->post('/user/devices/remove-all', 'DeviceController@removeAll');

$app->post('/store/close', 'SubscriptionController@terminate');

$app->post('/cart/depositCheckout', 'CartController@depositCheckout');

$app->post('/channel/create', 'ChannelController@create');
$app->post('/channel/updateSettings', 'ChannelController@updateSettings');

$app->post('agent/request', 'AgentController@addMaster');
$app->post('agent/update-priorities', 'AgentController@updatePrioritiesByServiceId');
$app->post('agent/add-master', 'AgentController@addMaster');
$app->post('agent/remove-master', 'AgentController@removeMaster');
$app->post('agent/delete-master', 'AgentController@deleteMaster');
$app->post('store/{store_id}/master/agent-accept', 'AgentController@acceptAgent');
$app->post('store/{store_id}/master/agent-remove', 'AgentController@removeAgent');
$app->post('store/{store_id}/master/bonus-setting', 'AgentController@masterSetBonusSetting');

$app->post('affiliate/config/notify-url', 'UserAffiliateConfController@setNotifyUrl');
$app->post('affiliate/config/secret-key', 'UserAffiliateConfController@getSecretKey');
$app->post('affiliate/config/white-list/add', 'UserAffiliateConfController@addWhiteList');
$app->post('affiliate/config/white-list/remove', 'UserAffiliateConfController@removeWhiteList');

$app->post('payment/kredivo/locked-ocash/void', 'KredivoController@voidLockedOcash');

$app->post('/store/{store_id}/inventory-pricing/{service_detail_id}/update-pricing', 'StoreInventoryPricingController@updatePricing');
$app->post('/store/{store_id}/inventory-pricing/{service_detail_id}/add-group-price', 'StoreInventoryPricingController@addGroupPrice');
$app->post('/store/{store_id}/inventory-pricing/{service_detail_id}/remove-group-price', 'StoreInventoryPricingController@removeGroupPrice');

$app->post('/store/{store_id}/inventory/{service_detail_id}/save-selected-vendor-price', 'StoreInventoryController@saveVendorSelectedPrice');
$app->post('/store/{store_id}/inventory/{service_detail_id}/remove-vendor', 'StoreInventoryController@removeVendor');
$app->post('/store/{store_id}/inventory/add-vendor', 'StoreInventoryController@addVendor');
$app->post('/store/{store_id}/inventory/replace-vendor', 'StoreInventoryController@replaceVendor');

$app->post('/pulsa/recurring/create', 'PulsaRecurringController@create');

$app->post('/bill-reminder/create', 'BillReminderController@create');

$app->post('store/{store_id}/dist/toggle','DistributionChannelController@toggle');

$app->post('/payment/requestOpen', 'PaymentController@requestOpenPayment');

$app->post('/payment/requestOpen', 'PaymentController@requestOpenPayment');

$app->post('/oauth2-client/settings', 'OAuth2ClientController@getOAuth2Client');
$app->post('/oauth2-client/settings/create', 'OAuth2ClientController@createOAuth2Client');
$app->post('/oauth2-client/settings/pg/create', 'OAuth2ClientController@createPaymentGatewayUser');
$app->post('/oauth2-client/settings/dg/create', 'OAuth2ClientController@createDisbursementApiUser');
$app->post('/oauth2-client/settings/user-invoice/create', 'OAuth2ClientController@createUserInvoiceUser');
$app->post('/oauth2-client/settings/secret/generate', 'OAuth2ClientController@generateSecret');
$app->post('/oauth2-client/settings/signing-key/generate', 'OAuth2ClientController@generateSigningKey');

$app->post('/payment-gateway/settings/callback', 'PaymentGatewayController@updateCallback');
$app->post('/payment-gateway/settings/email', 'PaymentGatewayController@updateEmailReport');
$app->post('/payment-gateway/settings/toggle-payment', 'PaymentGatewayController@togglePaymentChannel');
$app->post('/payment-gateway/settings/settlement', 'PaymentGatewayController@updateSettlement');

//$app->get('/disbursement/settings', 'DisbursementController@getV2DisbursementUser');
$app->post('/disbursement/settings/secret-key/generate', 'DisbursementController@generateSecretKey');
$app->post('/disbursement/settings/signing-key/generate', 'DisbursementController@generateSigningKey');
$app->post('/disbursement/settings/callback', 'DisbursementController@updateCallback');
$app->post('/disbursement/settings/whitelist', 'DisbursementController@updateWhitelist');
$app->post('/disbursement/settings/email', 'DisbursementController@updateEmail');

$app->post('/batch-disbursement/{id}/approve', 'BatchDisbursementController@approve');

$app->post('/user-invoice/settings/request', 'BusinessInvoiceController@requestInvoiceUser');
$app->post('/user-invoice/settings/toggle-charge', 'BusinessInvoiceController@toggleChargeToCustomer');
$app->post('/user-invoice/settings/toggle-channel', 'BusinessInvoiceController@toggleInvoicePaymentChannel');
$app->post('/user-invoice/settings/settlement', 'BusinessInvoiceController@updateSettlement');

$app->post('/approval/{id}/ofb-approve', 'ApprovalController@approveData');
$app->post('/approval/{id}/ofb-cancel', 'ApprovalController@cancelData');