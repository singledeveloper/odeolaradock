<?php

function appendZero($string) {
  if (strpos($string, '0') === 0 || strpos($string, '62') === 0) {
    return $string;
  }
  return  '0' . $string;
}

function purifyTelephone($number) {
  $number = trim(str_replace([".", " ", "+", "-"], "", $number));
  if (substr($number, 0, 1) === '0') $number = substr_replace($number, '62', 0, 1);
  return trim($number);
}

function baseUrl($path = "") {
  if (app()->environment() == 'production') $url = "https://api.odeo.co.id/";
  else if (app()->environment() == 'staging') $url = "https://api." . env('APP_STAGING_BASE_URL') . '/';

  else $url = url() . "/";

  starts_with($path, '/') && ($path = substr($path, 1));

  return $url . $path;
}

function qsUrl($path = null, $qs = array(), $secure = null) {
  $url = app('url')->to($path, $secure);
  if (count($qs)) {
    foreach ($qs as $key => $value) {
      $qs[$key] = sprintf('%s=%s', $key, urlencode($value));
    }
    $url = sprintf('%s?%s', $url, implode('&', $qs));
  }

  return $url;
}

function isAdmin($data = null) {
  if (!$data) $data['auth'] = auth();
  return isset($data["auth"]) && (isset($data["auth"]["type"]) && $data["auth"]["type"] == "admin");

}

function auth() {
  return \Odeo\Domains\Account\TokenManager::getUserData();
}

function revertTelephone($number) {
  if (isset($number[0]) && isset($number[1]))
    return $number[0] == "6" && $number[1] == "2" ? '0' . substr($number, 2) : $number;
  return '-';
}

function getUserId($data = null) {
  $auth = $data != null && isset($data['auth']) ? $data['auth'] : auth();
  if (isset($auth["user_id"])) return $auth["user_id"];
  return "";
}

function getPlatformId() {
  return auth()['platform_id'];
}

function parseFilePath($key, $category, $type = ['hires', 'thumb', 'normal']) {
  $data = [];
  foreach ($type as $item) {
    $data[$category . "_image_" . $item] = ($key == null ? "" : ("https://odeo.s3-ap-southeast-1.amazonaws.com" . $key));
  }
  return $data;
}

function normalizeMonthID($string) {
  $data = [
    "MEI" => "MAY",
    "AGU" => "AUG",
    "OKT" => "OCT",
    "DES" => "DEC"
  ];

  if (isset($data[$string])) return $data[$string];
  return $string;
}

function public_path($path = null) {
  return rtrim(app()->basePath('public/' . $path), '/');
}

function config_path($path = '') {
  return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
}

function ksort_recursive(&$array) {
  foreach ($array as &$value) {
    if (is_array($value)) ksort_recursive($value);
  }
  return ksort($array);
}

function formatPoint($string) {
  return number_format($string) . " p";
}

function checkPatterns($string, $patterns = []) {
  if (!is_string($string)) return false;

  foreach ($patterns as $item) {
    if (strpos($string, $item) !== false) return true;
  }
  return false;
}

function versioning($request, $headerValue, $comparation) {
  $updated = "same";
  if ($android_request_ver = $request->header($headerValue)) {
    $temp_req = explode(".", $android_request_ver);
    $temp_const = explode(".", $comparation);
    for ($x = 0; $x < 3; $x++) {
      if (!isset($temp_req[$x])) $temp_req[$x] = 0;
      if (!isset($temp_const[$x])) $temp_const[$x] = 0;
      if ($temp_req < $temp_const) {
        $updated = "not-same";
        break;
      }
    }
  }

  return $updated;
}

function clog($name, $message) {

  $viewLog = new Monolog\Logger($name . ' Logs');

  $viewLog->pushHandler(
    (new Monolog\Handler\StreamHandler(storage_path('/logs/' . $name . '-' . php_sapi_name() . '-' . date('Y-m-d', time()) . '.log'), Monolog\Logger::DEBUG, true, 0664))
      ->setFormatter(new \Monolog\Formatter\LineFormatter(null, null, true, true))
  );

  $viewLog->addInfo($message);

}

function datenow() {
  return date('Y-m-d H:i:s');
}

function authDebug($message, $uid = '') {
  //if (app()->environment() == 'production') clog('affiliate_auth_log', $message . ($uid != '' ? (' - ' . $uid) : ''));
  return app()->environment() == 'production' ? 'Unauthorized.' : ('Auth error: ' . $message);
}

function randomStr($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
  $str = '';
  $max = mb_strlen($keyspace, '8bit') - 1;
  for ($i = 0; $i < $length; ++$i) {
    $str .= $keyspace[random_int(0, $max)];
  }
  return $str;
}

function arrayToXml($data, $root = 'data') {
  $xml_data = new SimpleXMLElement("<?xml version=\"1.0\"?><$root></$root>");

  $walkFn = function ($data, &$xml_data) use (&$walkFn) {
    foreach ($data as $key => $value) {
      if (is_numeric($key)) {
        $key = 'item' . $key; //dealing with <0/>..<n/> issues
      }
      if (is_array($value)) {
        $subnode = $xml_data->addChild($key);
        $walkFn($value, $subnode);
      } else {
        $xml_data->addChild($key, htmlspecialchars($value));
      }
    }
  };

  $walkFn($data, $xml_data);

  return $xml_data;
}

function xmlToArray(SimpleXMLElement $xml) {
  $array = (array)$xml;

  foreach (array_slice($array, 0) as $key => $value) {
    if ($value instanceof SimpleXMLElement) {
      $array[$key] = empty($value) ? NULL : xmlToArray($value);
    }
  }

  return $array;
}

function domElementsToArray(DOMDocument $dom, $nodeName) {
  $data = [];
  foreach ($dom->getElementsByTagName($nodeName) as $rootNode) {
    foreach ($rootNode->childNodes as $node) {
      if ($node->nodeName == '#text') continue;
      $data[$node->nodeName] = $node->nodeValue;
    }
  }
  return $data;
}

function maskPhoneNumber($phoneNumber) {
  $length = strlen($phoneNumber);
  return str_repeat('*', max(0, $length - 4)) . substr($phoneNumber, $length - 4);
}

/**
 * @param $platformId
 * @param $version string must be semantic version
 * @return bool
 */
function isVersionSatisfy($platformId, $version) {
  if (!$header = \Odeo\Domains\Constant\Header::getHeaderByPlatform($platformId)) {
    return false;
  }

  if (!$clientVersion = app()->make(\Illuminate\Http\Request::class)
    ->header($header)) {
    return false;
  }

  try {
    list ($major, $minor, $revision) = getVersion($version);
    list ($clientMajor, $clientMinor, $clientRevision) = getVersion($clientVersion);
  } catch (\Odeo\Exceptions\FailException $e) {
    return false;
  }

  return
    $clientMajor > $major ||
    ($clientMajor == $major && $clientMinor > $minor) ||
    ($clientMajor == $major && $clientMinor == $minor && $clientRevision >= $revision);
}

function getVersion($version) {
  $intval = function ($val) {
    return (int)$val;
  };

  $versionParts = array_map($intval, explode('.', $version));
  if (count($versionParts) !== 3) {
    throw new \Odeo\Exceptions\FailException('version part array not contains 3 component');
  }

  return $versionParts;
}

function isOcashApp() {
  //clog('debug_is_ocash_app', json_encode(app()->make(\Illuminate\Http\Request::class)->headers->all()));
  if (!$header = \Odeo\Domains\Constant\Header::getHeaderByPlatform(\Odeo\Domains\Constant\Platform::ANDROID_OCASH)) {
    return false;
  }


  if (!$clientVersion = app()->make(\Illuminate\Http\Request::class)
    ->header($header)) {
    return false;
  }

  //clog('debug_is_ocash_app', $clientVersion);

  return in_array($clientVersion, [
    '1.0.5',
    '1.0.6',
  ]);
}

function logMicroTime($prefix = '', $fileName = null) {
  $backtrace = debug_backtrace();
  $last = $backtrace[0];
  $s = "{$last['file']} line {$last['line']}";
  $s = $prefix . ':' . $s . ':' . microtime(true);
  $fileName ? clog($fileName, $s) : \Log::info($s);
}

function isProduction() {
  return app()->environment() == 'production';
}

function isStaging() {
  return app()->environment() == 'staging';
}

function combineArrayByMatchedValue($arr1, $arr2) {
  return array_map(function ($item1, $item2) {
    return $item1 + $item2;
  }, $arr1, $arr2);
}

function sanitizeNumber($number) {
  return preg_replace("/\\D/", "", $number);
}

function arrayContain($s, $arr) {
  $filtered = array_filter($arr, function ($item) use ($s) {
    return str_contains($s, $item);
  });
  return !empty($filtered);
}

function setQueryTimeout($s) {
  \DB::statement('set statement_timeout TO ' . $s * 1000 . ';');
}

function getClientIP() {
  $request = app()->make(\Illuminate\Http\Request::class);
  $originalIP = $request->header('x-forwarded-for');
  // clog('user_ip', $originalIP);
  if(strpos($originalIP, ',') !== false) {
    $originalIP = explode(',', $originalIP);
    $originalIP = trim($originalIP[0]);
  }
  return $originalIP ?? $request->ip();
}

function dateRange($from, $to) {
  $dateFrom = \Carbon\Carbon::parse($from);
  $dateTo = \Carbon\Carbon::parse($to)->addDay();

  return array_map(function ($date) {
    return $date->format('Y-m-d');
  }, iterator_to_array(new DatePeriod($dateFrom, new DateInterval('P1D'), $dateTo)));
}

function logIsInTransaction($name, $message) {
  $txId1 = \DB::select(\DB::raw('select txid_current()'))[0];
  $txId2 = \DB::select(\DB::raw('select txid_current()'))[0];
  if ($txId1 == $txId2) {
    clog($name, "$message is in tx");
  } else {
    clog($name, "$message is not in tx");
  }

  return $txId1 == $txId2;
}

function isNewDesign() {
  if (isVersionSatisfy(\Odeo\Domains\Constant\Platform::ANDROID, '3.0.0')) {
    return true;
  }

  $auth = auth();
  return $auth && $auth['platform_id'] == \Odeo\Domains\Constant\Platform::TERMINAL;
}

function getAuthorizationBearer() {
  $request = app()->make(\Illuminate\Http\Request::class);
  $data = $headers = [];

  if ($request->header('Authorization')) {
    $headers = explode(", ", $request->header('Authorization'));
  }

  foreach ($headers as $header) {
    $auth = explode(" ", $header);
    if (count($auth) == 2) {
      $data[strtolower($auth[0])] = $auth[1];
    }
  }

  return $data;
}

function maskMessage($str) {
  if (strlen(trim($str)) == 0) { // check whitespace
    return $str;
  }

  $messages = explode(' ', $str);
  for($i = 0; $i < count($messages); $i++) {
    if (strlen($messages[$i]) <= 1) {
      continue;
    }
    $length = strlen($messages[$i]) - 1;
    $messages[$i] = $messages[$i][0] . str_repeat('*', $length);
  }

  return join(' ', $messages);
}

function maskEmail($str) {
  if (strlen(trim($str)) == 0) { // check whitespace
    return $str;
  }
  $messages = explode('@', $str);
  for ($i = 0; $i < count($messages); $i++) {
    if (strlen($messages[$i]) <= 1) {
      continue;
    }

    $temps = explode('.', $messages[$i]);
    for ($j = 0; $j < count($temps); $j++) {
      $length = strlen($temps[$j]) - 1;
      $temps[$j] = $temps[$j][0] . str_repeat('*', $length);
    }

    $messages[$i] = join('.', $temps);
  }
  return join('@', $messages);
}

function checkPinSecure($string) {
  if (in_array(trim($string), ['1234', '12345', '123456']))
    return [false, 'PIN Anda tidak aman. Mohon gunakan kombinasi PIN lain.'];
  return [true, ''];
}

function ipInRange($ip, $range) {
  if (strpos($range, '/') == false) $range .= '/32';
  list($range, $netmask) = explode('/', $range, 2);
  $rangeDecimal = ip2long($range);
  $ipDecimal = ip2long($ip);
  $netmaskDecimal = ~ (pow(2, (32 - $netmask)) - 1);
  return (($ipDecimal & $netmaskDecimal) == ($rangeDecimal & $netmaskDecimal));
}

function checkHeaderLanguage($acceptableLang = []) {
  $request = app()->make(\Illuminate\Http\Request::class);
  if ($lang = $request->header("Accept-Language")) {
    $lang = explode(",", $lang);
    $lang = trim($lang[0]);
    if ($lang == "in") {
      $lang = "id";
    }
    if (count($acceptableLang) > 0 && !in_array($lang, $acceptableLang)) {
      $lang = "en";
    }
    return $lang;
  }
  return "en";
}