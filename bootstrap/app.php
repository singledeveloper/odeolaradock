<?php

require_once __DIR__ . '/../vendor/autoload.php';

Illuminate\Http\Request::setTrustedProxies(['127.0.0.1']);

try {
  (new Dotenv\Dotenv(__DIR__ . '/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
  //
}

if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] === 'OPTIONS') {

  $allowedPattern = ['odeo.co.id'];
  $allowedOrigin = '';

  if (isset($_ENV['APP_ENV']) && isset($_SERVER['HTTP_ORIGIN']) && $_ENV['APP_ENV'] == 'production') {

    if (file_exists("../domain_active.txt")) {
      $activeDomains = explode("\n", file_get_contents("../domain_active.txt"));
      $activeDomains = array_filter($activeDomains);
      $allowedPattern = array_merge($allowedPattern, $activeDomains);
    }

    foreach ($allowedPattern as $item) {
      if (strrpos($_SERVER['HTTP_ORIGIN'], $item) !== false) {
        $allowedOrigin = $_SERVER['HTTP_ORIGIN'];
        break;
      }
    }

  } else {
    $allowedOrigin = '*';
  }

  $allowedPlatform = implode(',', \Odeo\Domains\Constant\Header::getAllHeaderPlatform());

  header('Access-Control-Allow-Origin:*');
  // header('Access-Control-Allow-Origin:' . $allowedOrigin);
  header('Access-Control-Allow-Credentials:true');
  header('Access-Control-Allow-Method:GET, POST, PUT, DELETE');
  header('Access-Control-Allow-Headers:Origin, Content-Type, X-Requested-With, Authorization, Accept-Language, ' . $allowedPlatform);

  exit();

}


/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
  realpath(__DIR__ . '/../')
);

$app->withFacades();
$app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
  Illuminate\Contracts\Debug\ExceptionHandler::class,
  Odeo\Exceptions\Handler::class
);

$app->singleton(
  Illuminate\Contracts\Console\Kernel::class,
  Odeo\Console\Kernel::class
);


/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/


$app->middleware([
  Odeo\Http\Middleware\Cors::class
]);

$app->routeMiddleware([
  'bearer' => Odeo\Http\Middleware\AuthenticateBearer::class,
  'affiliate' => Odeo\Http\Middleware\AuthenticateAffiliate::class,
  'disbursement' => Odeo\Http\Middleware\AuthenticateDisbursement::class,
  'internal' => Odeo\Http\Middleware\AuthenticateInternal::class,
  'auth_password' => Odeo\Http\Middleware\AuthenticatePassword::class,
  'terminal' => \Odeo\Http\Middleware\AuthenticateTerminal::class,
  'admin_logger' => Odeo\Http\Middleware\AdminLogger::class,
  'oauth2' => Odeo\Http\Middleware\AuthenticateOAuth2::class,
  'approval' => \Odeo\Http\Middleware\AuthenticateApproval::class
]);

$app->configure('constants');


/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

$app->register(Odeo\Providers\AppServiceProvider::class);
$app->register(Odeo\Providers\ModelServiceProvider::class);
// $app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);

$app->register(Artisaninweb\SoapWrapper\ServiceProvider::class);

$app->register(\Illuminate\Redis\RedisServiceProvider::class);

$app->register(Sentry\SentryLaravel\SentryLumenServiceProvider::class);

class_alias('Illuminate\Support\Facades\Config', 'Config');
$app->register(Elibyy\TCPDF\ServiceProvider::class);

$app->register(Illuminate\Mail\MailServiceProvider::class);
$app->configure('mail');

class_alias('Illuminate\Support\Facades\Response', 'Response');

$app->singleton('filesystem', function ($app) {
  return $app->loadComponent('filesystems', 'Illuminate\Filesystem\FilesystemServiceProvider', 'filesystem');
});
$app->configure('filesystems');
/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$route_groups = [
  "", 
  "admin", 
  "seller", 
  "guest|seller", 
  "affiliate", 
  "disbursement", 
  "internal", 
  "terminal", 
  "oauth2_pg_get_payment:read"
];

foreach ($route_groups as $item) {
  $groups = ['prefix' => 'v1', 'namespace' => '\Odeo\Http\Controllers'];
  switch ($item) {
    case \Odeo\Domains\Constant\UserType::AFFILIATE:
      $groups['middleware'][] = \Odeo\Domains\Constant\UserType::AFFILIATE;
      break;
    case \Odeo\Domains\Constant\UserType::DISBURSEMENT:
      $groups['middleware'][] = \Odeo\Domains\Constant\UserType::DISBURSEMENT;
      break;
    case \Odeo\Domains\Constant\UserType::INTERNAL:
      $groups['middleware'][] = \Odeo\Domains\Constant\UserType::INTERNAL;
      break;
    case \Odeo\Domains\Constant\UserType::TERMINAL:
      $groups['middleware'][] = \Odeo\Domains\Constant\UserType::TERMINAL;
      break;
    case '';
      break;
    default:
      if (strpos($item, 'oauth2_') === 0) {
        $groups['middleware'][] = 'oauth2:' . substr($item, strlen('oauth2_'));
      } else {
        $groups['middleware'][] = 'bearer:' . $item;
        if ($item == \Odeo\Domains\Constant\UserType::ADMIN) {
          $groups['middleware'][] = 'admin_logger';
        }
      }
      break;
  }

  $link = ($item != "" ? ('_' . str_replace("|", "_", $item)) : '');
  $link = __DIR__ . '/../app/Http/routes' . $link;

  $app->group($groups, function ($app) use ($link) {
    if (file_exists($link . '.php')) require $link . '.php';
  });

  if ($item != "") {
    $groupWithAuthPassword = $groups;
    $groupWithAuthPassword['middleware'][] = 'auth_password';
    $app->group($groupWithAuthPassword, function ($app) use ($link) {
      if (file_exists($link . '_with_password.php')) require $link . '_with_password.php';
    });

    $groupWithApproval = $groups;
    $groupWithApproval['middleware'][] = 'approval';
    $app->group($groupWithApproval, function ($app) use ($link) {
      if (file_exists($link . '_with_approval.php')) require $link . '_with_approval.php';
    });
  }
}

$app->get('/test-ip', function () {
  echo getClientIP();
});

$app->configureMonologUsing(function (Monolog\Logger $monolog) {
  $handler = (new \Monolog\Handler\StreamHandler(storage_path('/logs/lumen-' . php_sapi_name() . '-' . date('Y-m-d', time()) . '.log'), Monolog\Logger::DEBUG, true, 0666))
    ->setFormatter(new \Monolog\Formatter\LineFormatter(null, null, true, true));
  return $monolog->pushHandler($handler);
});

return $app;
