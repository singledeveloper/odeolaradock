<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentOdeoPaymentChannelInformationsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('payment_odeo_payment_channel_informations', function (Blueprint $table) {
      $table->unsignedInteger('id');
      $table->primary('id');

      $table->string('name');
      $table->string('actual_name');

      $table->decimal('min_transaction', 17, 2);
      $table->decimal('max_transaction', 17, 2);

      $table->text('detail')->nullable();
      $table->string('logo')->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('payment_odeo_payment_channel_informations');
  }
}
