<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserHistoriesTable extends Migration {

  public function up() {
    Schema::create('user_histories', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->string('name', 50)->nullable();
      $table->string('email', 50)->nullable();
      $table->string('telephone', 20)->nullable();
      $table->timestamps();
      $table->softDeletes();
    });
  }

  public function down() {
    Schema::drop('user_histories');
  }
}
