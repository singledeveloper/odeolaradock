<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 20);
            $table->string('color', 7);
            $table->integer('cost_per_month');
            $table->integer('minimal_months');
            $table->boolean('is_free_domain')->default(0);
            $table->boolean('can_order_bolt')->default(0);
            $table->boolean('can_order_pln')->default(0);
            $table->boolean('can_order_flight')->default(0);
            $table->boolean('can_order_hotel')->default(0);
            $table->integer('sales_volume');
            $table->integer('max_assign_sponsor_bonus');
            $table->integer('max_upgrade_sponsor_bonus');
            $table->integer('max_pairing');
            $table->integer('max_pairing_potential');
            $table->integer('warranty');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plans');
    }
}
