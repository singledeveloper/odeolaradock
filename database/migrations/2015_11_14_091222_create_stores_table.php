<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('plan_id')->unsigned();
            $table->foreign('plan_id')->references('id')->on('plans')->onDelete('set null');
            $table->text('current_data');
            $table->string('name', 50);
            $table->string('logo_path')->nullable();
            $table->string('favicon_path')->nullable();
            $table->string('logo_no_odeo_path')->nullable();
            $table->string('opay_logo_path')->nullable();
            $table->string('subdomain_name', 50);
            $table->string('domain_name', 50)->nullable();
            $table->dateTime('renewal_at')->nullable();
            $table->char('status', 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stores');
    }
}
