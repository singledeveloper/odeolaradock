<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('order_details', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('order_id')->unsigned();
      $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
      $table->integer('service_detail_id')->unsigned()->nullable();
      $table->foreign('service_detail_id')->references('id')->on('service_details')->onDelete('set null');
      $table->string('name');
      $table->decimal('sale_price', 17, 2);
      $table->decimal('base_price', 17, 2)->nullable();
      $table->integer('quantity')->default(1);
      $table->decimal('margin', 17, 2)->nullable();
      $table->timestamp('verified_at')->nullable();
      $table->timestamp('refunded_at')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('order_details');
  }
}
