<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePulsaOdeosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('pulsa_odeos', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('operator_id')->unsigned();
        $table->foreign('operator_id')->references('id')->on('pulsa_operators')->onDelete('cascade');
        $table->string("category", 50)->default('');
        $table->integer('nominal');
        $table->integer('price')->default(0);
        $table->boolean("is_active")->default(true);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('pulsa_odeos');
    }
}
