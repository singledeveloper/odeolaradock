<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePulsaPrefixesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pulsa_prefixes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('prefix', 5);
            $table->integer('operator_id')->unsigned();
            $table->foreign('operator_id')->references('id')->on('pulsa_operators')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pulsa_prefixes');
    }
}
