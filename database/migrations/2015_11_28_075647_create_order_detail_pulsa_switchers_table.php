<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailPulsaSwitchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail_pulsa_switchers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_detail_id');
            $table->foreign('order_detail_id')->references('id')->on('order_details')->onDelete('cascade');
            $table->integer('first_inventory_id')->unsigned();
            $table->foreign('first_inventory_id')->references('id')->on('pulsa_odeo_inventories')->onDelete('cascade');
            $table->integer('current_inventory_id')->unsigned();
            $table->foreign('current_inventory_id')->references('id')->on('pulsa_odeo_inventories')->onDelete('cascade');
            $table->integer('current_base_price');
            $table->string('operator');
            $table->string('number', 20);
            $table->integer("tries_count")->unsigned()->default(0);
            $table->boolean("tries_is_able_repurchase")->default(false);
            $table->text('inventory_query_dumps')->nullable();
            $table->string('status', 5)->default('10000');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_detail_pulsa_switchers');
    }
}
