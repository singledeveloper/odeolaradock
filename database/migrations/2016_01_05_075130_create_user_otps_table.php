<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserOtpsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('user_otps', function (Blueprint $table) {
      $table->increments('id');
      $table->string('telephone', 20);
      $table->char('otp', 4);
      $table->enum('is_used', [0, 1]);
      $table->timestamp('created_at');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('user_otps');
  }
}
