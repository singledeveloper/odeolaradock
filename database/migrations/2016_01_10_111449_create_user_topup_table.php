<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTopupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('user_topups', function(Blueprint $table)
      {
        $table->increments('id');
        $table->bigInteger('user_id');
        $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        $table->decimal('amount', 17, 2);
        $table->string('status');
        $table->timestamps();
      });
      
      Schema::create('order_detail_topups', function(Blueprint $table)
      {
        $table->increments('id');
        $table->bigInteger('order_detail_id');
        $table->foreign('order_detail_id')->references('id')->on('order_details')->onDelete('cascade');
        $table->integer('topup_id');
        $table->foreign('topup_id')->references('id')->on('user_topups')->onDelete('set null');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('order_detail_topups');
      Schema::drop('user_topups');
    }
}
