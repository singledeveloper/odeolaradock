<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cash_transactions', function(Blueprint $table)
      {
        $table->bigIncrements('id');
        $table->bigInteger('user_id')->unsigned()->nullable();
        $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        $table->bigInteger('store_id')->unsigned()->nullable();
        $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
        $table->string('trx_type');
        $table->string('cash_type');
        $table->decimal('amount', 17, 2);
        $table->text('data')->default('');
        $table->decimal('balance_before', 17, 2);
        $table->decimal('balance_after', 17, 2);
        $table->tinyInteger('trx_month');
        $table->smallInteger('trx_year');
        $table->timestamp('created_at');
        $table->index(['store_id','trx_month', 'trx_year'], 'trx_index');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('cash_transactions');
    }
}
