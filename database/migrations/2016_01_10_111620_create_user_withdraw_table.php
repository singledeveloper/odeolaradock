<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWithdrawTable extends Migration {

  public function up() {
    Schema::create('user_withdraws', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id');
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->integer('user_bank_account_id')->nullable();
      $table->foreign('user_bank_account_id')->references('id')->on('user_bank_accounts')->onDelete('set null');
      $table->decimal('amount', 17, 2);
      $table->decimal('fee', 8, 2)->default(0);
      $table->string('account_name');
      $table->string('account_number');
      $table->string('account_bank');
      $table->string('status');
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('user_withdraws');
  }
}
