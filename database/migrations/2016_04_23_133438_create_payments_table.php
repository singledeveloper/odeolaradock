<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('payments', function (Blueprint $table) {

      $table->bigIncrements('id');

      $table->bigInteger('order_id')->unsigned()->nullable();
      $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');

      $table->tinyInteger('opc')->unsigned()->nullable();
      $table->foreign('opc')->references('code')->on('payment_odeo_payment_channels')->onDelete('set null');

      $table->tinyInteger('info_id')->unsigned()->nullable();
      $table->foreign('info_id')->references('id')->on('payment_odeo_payment_channel_informations')->onDelete('set null');

      $table->integer('reference_id')->unsigned()->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('payments');
  }
}
