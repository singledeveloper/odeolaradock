<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNetworkTree extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('network_trees', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('store_id')->unsigned();
      $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
      $table->bigInteger('referred_store_id')->unsigned()->nullable();
      $table->foreign('referred_store_id')->references('id')->on('stores')->onDelete('set null');
      $table->bigInteger('parent_store_id')->unsigned()->nullable();
      $table->foreign('parent_store_id')->references('id')->on('stores')->onDelete('set null');
      $table->enum('position', ['0', '1'])->nullable();
      $table->enum('is_network_enable', ['0', '1'])->default('0');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('network_trees');
  }
}
