<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentVeritransPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_veritrans_payments', function (Blueprint $table) {

            $table->increments('id');

            $table->string('transaction_id')->nullable();
            $table->string('url')->nullable();
            $table->datetime('transaction_time')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_veritrans_payments');
    }


}
