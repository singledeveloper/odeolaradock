<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNetworkSponsorBonus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('network_sponsor_bonuses', function(Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('store_id')->unsigned();
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
            $table->integer('sponsor_store_id')->unsigned();
            $table->foreign('sponsor_store_id')->references('id')->on('stores')->onDelete('set null');
            $table->integer('minimal_required_plan_id')->unsigned()->nullable()->comment = "NULL if amount = claimed_amount";
            $table->foreign('minimal_required_plan_id')->references('id')->on('plans')->onDelete('set null');
            $table->integer('amount')->unsigned();
            $table->integer('claimed_amount')->unsigned();
            $table->enum("created_cause", array("assign", "upgrade"));
            $table->char('day_limit', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('network_sponsor_bonuses');
    }
}
