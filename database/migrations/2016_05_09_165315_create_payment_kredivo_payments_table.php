<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentKredivoPaymentsTable extends Migration {

  public function up() {
    Schema::create('payment_kredivo_payments', function (Blueprint $table) {
      $table->increments('id');

      $table->string('transaction_id')->nullable();
      $table->string('signature_key')->nullable();
      $table->string('fraud_status')->nullable();
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('payment_kredivo_payments');
  }
}
