<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentKredivoPaymentDetailsTable extends Migration {

  public function up() {
    Schema::create('payment_kredivo_payment_details', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('kredivo_payment_id')->unsigned();
      $table->foreign('kredivo_payment_id')->references('id')->on('payment_kredivo_payments');
      $table->string('process_step')->nullable();
      $table->string('status')->nullable();
      $table->string('message')->nullable();
      $table->string('transaction_status')->nullable();
      $table->timestamp('transaction_time')->nullable();
    });
  }

  public function down() {
    Schema::drop('payment_kredivo_payment_details');
  }
}
