<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentVeritransPaymentDetailsTable extends Migration
{

    public function up()
    {
        Schema::create('payment_veritrans_payment_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('veritrans_payment_id')->unsigned();
            $table->foreign('veritrans_payment_id')->references('id')->on('payment_veritrans_payments')->onDelete('set null');

            $table->string('status_code')->nullable();
            $table->string('status_message')->nullable();
            $table->string('transaction_status')->nullable();
            $table->string('signature_key')->nullable();
            $table->decimal('amount', 17, 2)->nullable();

            $table->text('additional')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_veritrans_payment_details');
    }
}
