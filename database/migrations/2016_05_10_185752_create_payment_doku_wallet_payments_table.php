<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDokuWalletPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_doku_wallet_payments', function (Blueprint $table) {
            $table->increments('id');

            $table->string('approval_code')->nullable();
            $table->string('bank')->nullable();
            $table->string('dp_mall_id')->nullable();
            $table->string('payment_channel_code')->nullable();
            $table->string('response_code')->nullable();
            $table->string('response_msg')->nullable();
            $table->string('status')->nullable();
            $table->string('tracking_id')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_doku_wallet_payments');
    }
}
