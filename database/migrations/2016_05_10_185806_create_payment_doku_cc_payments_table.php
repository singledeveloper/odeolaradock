<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDokuCcPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_doku_cc_payments', function (Blueprint $table) {
            $table->increments('id');

            $table->decimal('amount', 17, 2)->nullable();
            $table->string('response_code')->nullable();
            $table->string('response_msg')->nullable();
            $table->string('approval_code')->nullable();
            $table->string('bank')->nullable();
            $table->string('mcn')->nullable();
            $table->string('mid')->nullable();
            $table->string('payment_channel')->nullable();
            $table->string('payment_date_time')->nullable();
            $table->string('session_id')->nullable();
            $table->string('verify_id')->nullable();
            $table->string('verify_score')->nullable();
            $table->string('verify_status')->nullable();
            $table->string('words')->nullable();
            $table->string('token_payment')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_doku_cc_payments');
    }
}
