<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDokuMandiriClickpayPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_doku_mandiri_clickpay_payments', function (Blueprint $table) {
            $table->increments('id');

            $table->string('response_msg')->nullable();
            $table->string('transaction_code')->nullable();
            $table->string('mcn')->nullable();
            $table->string('approval_code')->nullable();
            $table->timestamp('payment_date')->nullable();
            $table->string('request_code')->nullable();
            $table->string('bank')->nullable();
            $table->string('response_code')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_doku_mandiri_clickpay_payments');
    }
}
