<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDokuBcaClickpayPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_doku_bca_clickpay_payments', function (Blueprint $table) {
            $table->increments('id');

            $table->string('chain_merchant')->nullable();
            $table->decimal('amount', 17, 2)->nullable();
            $table->decimal('purchase_amount', 17, 2)->nullable();
            $table->string('words')->nullable();
            $table->timestamp('request_datetime')->nullable();
            $table->string('currency')->nullable();
            $table->string('purchase_currency')->nullable();
            $table->string('session_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_doku_bca_clickpay_payments');
    }
}
