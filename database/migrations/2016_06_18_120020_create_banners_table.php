<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration {

  public function up() {
    Schema::create('banners', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
      $table->string('description');
      $table->string('url');
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('banners');
  }
}
