<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketingBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketing_budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            for($i = 1; $i <= 12; $i++) {
              $table->decimal('variant_'.$i,5,2);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('marketing_budgets');
    }
}
