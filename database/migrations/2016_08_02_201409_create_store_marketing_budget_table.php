<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreMarketingBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_marketing_budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('store_id')->unsigned();
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
            $table->integer('variant')->unsigned();
            $table->decimal('additional_budget', 5, 2);
            $table->integer('plan_price_per_day')->unsigned();
            $table->integer('min_ads')->unsigned();
            $table->integer('max_ads')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('store_marketing_budgets');
    }
}
