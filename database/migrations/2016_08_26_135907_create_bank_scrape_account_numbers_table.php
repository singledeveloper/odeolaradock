<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankScrapeAccountNumbersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('bank_scrape_account_numbers', function (Blueprint $table) {

      $table->integer('id')->unsigned();
      $table->primary('id');

      $table->string('number');

      $table->text('detail')->nullable();

      $table->timestamp('last_scrapped_date');

      $table->timestamp('last_time_scrapped')->nullable();

      $table->boolean('active');

      $table->integer('bank_scrape_account_id')->unsigned();
      $table->foreign('bank_scrape_account_id')
        ->references('id')
        ->on('bank_scrape_accounts');

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('bank_scrape_account_numbers');
  }
}
