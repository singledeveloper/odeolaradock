<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentOdeoBankTransferCodeConfigsTable extends Migration {

  public function up() {
    Schema::create('payment_odeo_bank_transfer_code_configs', function (Blueprint $table) {

      $table->bigIncrements('id');
      $table->decimal('amount', 17, 2);
      $table->string('bank');
      $table->integer('increment');
      $table->integer('increment_amplifier');
      $table->index(['amount', 'bank']);
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('payment_odeo_bank_transfer_code_configs');
  }
}
