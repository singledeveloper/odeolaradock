<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('channel_codes', function (Blueprint $table) {
        $table->increments('id');
        $table->string('code')->unique();
        $table->string('hash', 40)->unique();
        $table->text('qr_url')->nullable();
        $table->timestamp('created_at');
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('channel_codes');
    }
}
