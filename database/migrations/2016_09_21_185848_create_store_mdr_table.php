<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreMdrTable extends Migration {

  public function up() {
    Schema::create('store_mdrs', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('store_id')->unsigned()->unique();
      $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
      $table->decimal('base_mdr', 7, 4);
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('store_mdrs');
  }
}
