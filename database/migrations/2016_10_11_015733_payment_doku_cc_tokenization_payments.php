<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentDokuCcTokenizationPayments extends Migration {

  public function up() {
    Schema::create('payment_doku_cc_tokenization_payments', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->decimal('amount', 17, 2)->nullable();
      $table->string('response_code')->nullable();
      $table->string('response_msg')->nullable();
      $table->string('approval_code')->nullable();
      $table->string('bank')->nullable();
      $table->string('mcn')->nullable();
      $table->string('mid')->nullable();
      $table->string('payment_channel')->nullable();
      $table->string('payment_date_time')->nullable();
      $table->string('session_id')->nullable();
      $table->string('verify_id')->nullable();
      $table->string('verify_score')->nullable();
      $table->string('verify_status')->nullable();
      $table->string('words')->nullable();
      $table->string('mall_id')->nullable();
      $table->string('currency')->nullable();
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('payment_doku_cc_tokenization_payments');
  }
}
