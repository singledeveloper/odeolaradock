<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreDepositsTable extends Migration {

  public function up() {
    Schema::create('store_deposits', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('store_id')->unsigned();
      $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
      $table->decimal('amount', 17, 2);
      $table->char('status', 5);
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('store_deposits');
  }
}
