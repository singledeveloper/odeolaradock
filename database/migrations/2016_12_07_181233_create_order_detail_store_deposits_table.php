<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailStoreDepositsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('order_detail_store_deposits', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('order_detail_id')->unsigned();
      $table->foreign('order_detail_id')->references('id')->on('order_details')->onDelete('cascade');
      $table->integer('store_deposit_id');
      $table->foreign('store_deposit_id')->references('id')->on('store_deposits')->onDelete('set null');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('order_detail_store_deposits');
  }
}
