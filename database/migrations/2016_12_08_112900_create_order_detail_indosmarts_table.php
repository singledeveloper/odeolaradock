<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailIndosmartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail_indosmarts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_detail_pulsa_switcher_id');
            $table->foreign('order_detail_pulsa_switcher_id')->references('id')->on('order_detail_pulsa_switchers')->onDelete('cascade');
            $table->char('transaction_id', 6);
            $table->char('status', 5)->default('10000');
            $table->text('log_request')->nullable();
            $table->text('log_response')->nullable();
            $table->text('log_advice')->nullable();
            $table->timestamps();

        });
    
        DB::update("ALTER SEQUENCE order_detail_indosmarts_id_seq RESTART WITH 1000000000");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_detail_indosmarts');
    }
}
