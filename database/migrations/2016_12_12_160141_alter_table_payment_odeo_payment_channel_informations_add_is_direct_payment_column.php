<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentOdeoPaymentChannelInformationsAddIsDirectPaymentColumn extends Migration {

  public function up() {
    Schema::table('payment_odeo_payment_channel_informations', function (Blueprint $table) {
      $table->tinyInteger('is_direct_payment')->nullable();
    });
  }

  public function down() {
    Schema::table('payment_odeo_payment_channel_informations', function (Blueprint $table) {
      $table->dropColumn('is_direct_payment');
    });
  }
}
