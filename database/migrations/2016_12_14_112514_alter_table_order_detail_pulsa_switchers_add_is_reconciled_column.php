<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailPulsaSwitchersAddIsReconciledColumn extends Migration {

  public function up() {
    Schema::table('order_detail_pulsa_switchers', function (Blueprint $table) {
      $table->boolean("is_reconciled")->default(false)->after('status');
    });
  }

  public function down() {
    Schema::table('order_detail_pulsa_switchers', function (Blueprint $table) {
      //$table->dropColumn("is_reconciled");
    });
  }
}
