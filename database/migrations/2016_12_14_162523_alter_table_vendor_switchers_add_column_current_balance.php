<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableVendorSwitchersAddColumnCurrentBalance extends Migration {

  public function up() {
    Schema::table('vendor_switchers', function (Blueprint $table) {
      $table->decimal('current_balance', 17, 2)->default(0);
    });
  }

  public function down() {
    Schema::table('vendor_switchers', function (Blueprint $table) {
      $table->dropColumn('current_balance');
    });
  }

}
