<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsersAddDirectPaymentReferenceColumns extends Migration {

  public function up() {
    Schema::table('users', function (Blueprint $table) {
      $table->unsignedInteger('direct_payment_reference_type')->default(\Odeo\Domains\Constant\Payment::OPC_GROUP_OCASH);
      $table->foreign('direct_payment_reference_type')->references('id')->on('payment_odeo_payment_channel_informations')->onDelete('set null');
      $table->unsignedInteger('direct_payment_reference_id')->nullable();
    });
  }


  public function down() {
    Schema::table('users', function (Blueprint $table) {
      $table->dropColumn('direct_payment_reference_type');
      $table->dropColumn('direct_payment_reference_id');
    });
  }
}
