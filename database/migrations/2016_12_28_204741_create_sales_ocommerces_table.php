<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOcommercesTable extends Migration {

  public function up() {
    Schema::create('sales_ocommerce_reports', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('service_id')->unsigned()->default(0);
      $table->integer('sales')->unsigned()->default(0);
      $table->integer('sales_target')->unsigned()->default(0);
      $table->boolean('is_locked')->default(0);
      $table->date('date');
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('sales_ocommerce_reports');
  }
}

