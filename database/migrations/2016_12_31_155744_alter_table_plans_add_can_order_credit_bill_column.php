<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePlansAddCanOrderCreditBillColumn extends Migration {

  public function up() {
    Schema::table('plans', function (Blueprint $table) {
      $table->boolean('can_order_credit_bill')->default(0);
    });
  }


  public function down() {
    Schema::table('plans', function (Blueprint $table) {
      $table->dropColumn('can_order_credit_bill');
    });
  }
}
