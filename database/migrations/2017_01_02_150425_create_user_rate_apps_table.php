<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRateAppsTable extends Migration {

  public function up() {
    Schema::create('user_rate_apps', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
      $table->tinyInteger('platform_id');
      $table->timestamp('applied_at');
      $table->timestamp('closed_at')->nullable();
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('user_rate_apps');
  }
}
