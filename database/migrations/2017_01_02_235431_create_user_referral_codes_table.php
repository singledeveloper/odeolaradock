<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserReferralCodesTable extends Migration {

  public function up() {
    Schema::create('user_referral_codes', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
      $table->bigInteger('user_referred_id')->unsigned();
      $table->foreign('user_referred_id')->references('id')->on('users')->onDelete('cascade');
      $table->string('referral_code');
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('user_referral_codes');
  }
}
