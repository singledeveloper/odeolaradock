<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderDetailPulsaSwitchersAddRequestedAtColumn extends Migration {

  public function up() {
    Schema::table('order_detail_pulsa_switchers', function (Blueprint $table) {
      $table->timestamp("requested_at")->nullable()->after('created_at');
    });
  }

  public function down() {
    Schema::table('order_detail_pulsa_switchers', function (Blueprint $table) {
      $table->dropColumn("requested_at");
    });
  }
}
