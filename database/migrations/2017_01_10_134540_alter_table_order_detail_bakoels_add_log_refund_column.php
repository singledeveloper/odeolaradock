<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailBakoelsAddLogRefundColumn extends Migration {

  public function up() {
    Schema::table('order_detail_bakoels', function (Blueprint $table) {
      $table->text('log_refund')->nullable()->after('log_response');
    });
  }


  public function down() {
    Schema::table('order_detail_bakoels', function (Blueprint $table) {
      $table->dropColumn('log_refund');
    });
  }
}
