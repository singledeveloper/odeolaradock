<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsersAddKtpColumns extends Migration {

  public function up() {
    Schema::table('users', function (Blueprint $table) {
      $table->string('ktp_nik')->nullable();
      $table->string('ktp_image_url')->nullable();
    });
  }


  public function down() {
    Schema::table('users', function (Blueprint $table) {
      $table->dropColumn('ktp_nik');
      $table->dropColumn('ktp_image_url');
    });
  }
}
