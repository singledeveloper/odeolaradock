<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserKtpsTable extends Migration {

  public function up() {
    Schema::create('user_ktps', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->string('ktp_nik')->nullable();
      $table->string('ktp_image_url')->nullable();
      $table->timestamp('created_at');
      $table->timestamp('verified_at')->nullable();
    });
  }


  public function down() {
    Schema::drop('user_ktps');
  }
}
