<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserKtpsAddRejectedAtColumn extends Migration {

  public function up() {
    Schema::table('user_ktps', function (Blueprint $table) {
      $table->timestamp('rejected_at')->nullable();
    });
  }


  public function down() {
    Schema::table('user_ktps', function (Blueprint $table) {
      $table->dropColumn('rejected_at');
    });
  }
}
