<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePlansAddCanOrderPaketData extends Migration {

  public function up() {
    Schema::table('plans', function (Blueprint $table) {
      $table->boolean('can_order_paket_data')->default(0)->after('is_free_domain');
    });
  }

  public function down() {
    Schema::table('plans', function (Blueprint $table) {
      $table->dropColumn('can_order_paket_data');
    });
  }
}
