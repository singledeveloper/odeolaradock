<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisteredUserReportsTable extends Migration {

  public function up() {
    Schema::create('registered_user_reports', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('registered_user')->unsigned()->default(0);
      $table->integer('registration_target')->unsigned()->default(0);
      $table->boolean('is_locked')->default(0);
      $table->date('date');
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('registered_user_reports');
  }
}
