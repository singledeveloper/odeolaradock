<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentUserDirectPaymentsAddDeletedAt extends Migration {

  public function up() {
    Schema::table('payment_user_direct_payments', function (Blueprint $table) {
      $table->softDeletes();
    });
  }

  public function down() {
    Schema::table('payment_user_direct_payments', function (Blueprint $table) {
      $table->dropColumn('deleted_at');
    });
  }
}
