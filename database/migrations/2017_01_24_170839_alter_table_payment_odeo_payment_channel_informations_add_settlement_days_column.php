<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentOdeoPaymentChannelInformationsAddSettlementDaysColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('payment_odeo_payment_channel_informations', function (Blueprint $table) {
        $table->tinyInteger("settlement_days")->unsigned()->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('payment_odeo_payment_channel_informations', function (Blueprint $table) {
        $table->dropColumn("settlement_days");
      });
    }
}
