<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderChargesAddGroupTypeColumn extends Migration {

  public function up() {
    Schema::table('order_charges', function (Blueprint $table) {
      $table->string('group_type')->nullable();
    });
  }

  public function down() {
    Schema::table('order_charges', function (Blueprint $table) {
      $table->dropColumn('group_type');
    });
  }
}
