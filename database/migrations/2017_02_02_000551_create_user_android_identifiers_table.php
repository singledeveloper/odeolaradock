<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAndroidIdentifiersTable extends Migration {

  public function up() {
    Schema::create('user_android_identifiers', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->string('instance_id');
      $table->string('ssaid');
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('user_android_identifiers');
  }
}
