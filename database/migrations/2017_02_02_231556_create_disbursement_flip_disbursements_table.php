<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisbursementFlipDisbursementsTable extends Migration {

  public function up() {
    Schema::create('disbursement_flip_disbursements', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->decimal('amount_to_be_transfer', 17, 2)->nullable();
      $table->string('disbursement_type')->nullable();
      $table->unsignedBigInteger('disbursement_reference_id')->nullable();

      $table->bigInteger('flip_id')->nullable();
      $table->bigInteger('flip_user_id')->nullable();
      $table->string('flip_timestamp')->nullable();
      $table->string('flip_receiver_bank')->nullable();
      $table->string('flip_receiver_account_number')->nullable();
      $table->string('flip_receiver_name')->nullable();
      $table->string('flip_sender_bank')->nullable();
      $table->string('flip_note')->nullable();
      $table->string('flip_evidence')->nullable();
      $table->string('flip_time_served')->nullable();
      $table->string('flip_bundle_id')->nullable();
      $table->string('flip_company_id')->nullable();
      $table->string('flip_city')->nullable();
      $table->string('transaction_key')->unique()->nullable();
      $table->string('flip_amount')->nullable();

      $table->string('flip_status')->nullable();
      $table->string('status')->nullable();

      $table->text('response')->nullable();
      $table->dateTime('transfer_datetime')->nullable();
      $table->dateTime('response_datetime')->nullable();
      $table->dateTime('notify_datetime')->nullable();

      $table->string('error_code')->nullable();
      $table->text('error_reason')->nullable();
      $table->text('error_exception_message')->nullable();
      $table->integer('error_exception_code')->nullable();

      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('disbursement_flip_disbursements');
  }
}
