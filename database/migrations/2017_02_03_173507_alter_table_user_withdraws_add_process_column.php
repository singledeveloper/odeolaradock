<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserWithdrawsAddProcessColumn extends Migration {

  public function up() {
    Schema::table('user_withdraws', function (Blueprint $table) {

      $table->bigInteger('process_by')->unsigned()->nullable();
      $table->foreign('process_by')->references('id')->on('users')->onDelete('set null');

      $table->bigInteger('auto_disbursement_reference_id')->unsigned()->nullable();
      $table->string('auto_disbursement_vendor')->nullable();

    });
  }


  public function down() {
    Schema::table('user_withdraws', function (Blueprint $table) {

      $table->dropColumn('process_by');
      $table->dropColumn('auto_disbursement_reference_id');
      $table->dropColumn('auto_disbursement_vendor');

    });
  }
}
