<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailPulsasAddRouteColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('order_detail_artajasas', function (Blueprint $table) {
        $table->string('route', 50)->nullable()->after('order_detail_pulsa_switcher_id');
      });

      Schema::table('order_detail_bakoels', function (Blueprint $table) {
        $table->string('route', 50)->nullable()->after('order_detail_pulsa_switcher_id');
      });

      Schema::table('order_detail_datacells', function (Blueprint $table) {
        $table->string('route', 50)->nullable()->after('order_detail_pulsa_switcher_id');
      });

      Schema::table('order_detail_indosmarts', function (Blueprint $table) {
        $table->string('route', 50)->nullable()->after('order_detail_pulsa_switcher_id');
      });

      Schema::table('order_detail_mkms', function (Blueprint $table) {
        $table->string('route', 50)->nullable()->after('order_detail_pulsa_switcher_id');
      });

      Schema::table('order_detail_raja_billers', function (Blueprint $table) {
        $table->string('route', 50)->nullable()->after('order_detail_pulsa_switcher_id');
      });

      Schema::table('order_detail_spektrums', function (Blueprint $table) {
        $table->string('route', 50)->nullable()->after('order_detail_pulsa_switcher_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('order_detail_artajasas', function (Blueprint $table) {
        $table->dropColumn('route');
      });

      Schema::table('order_detail_bakoels', function (Blueprint $table) {
        $table->dropColumn('route');
      });

      Schema::table('order_detail_datacells', function (Blueprint $table) {
        $table->dropColumn('route');
      });

      Schema::table('order_detail_indosmarts', function (Blueprint $table) {
        $table->dropColumn('route');
      });

      Schema::table('order_detail_mkms', function (Blueprint $table) {
        $table->dropColumn('route');
      });

      Schema::table('order_detail_raja_billers', function (Blueprint $table) {
        $table->dropColumn('route');
      });

      Schema::table('order_detail_spektrums', function (Blueprint $table) {
        $table->dropColumn('route');
      });
    }
}
