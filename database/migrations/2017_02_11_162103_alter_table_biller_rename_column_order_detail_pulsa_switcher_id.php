<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Odeo\Domains\Constant\SwitcherConfig;

class AlterTableBillerRenameColumnOrderDetailPulsaSwitcherId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('order_detail_artajasas', function (Blueprint $table) {
        $table->renameColumn('order_detail_pulsa_switcher_id', 'switcher_reference_id');
        $table->string('reference_type', 20)->nullable()->after('switcher_reference_id');
      });

      Schema::table('order_detail_bakoels', function (Blueprint $table) {
        $table->renameColumn('order_detail_pulsa_switcher_id', 'switcher_reference_id');
        $table->string('reference_type', 20)->nullable()->after('switcher_reference_id');
      });

      Schema::table('order_detail_datacells', function (Blueprint $table) {
        $table->renameColumn('order_detail_pulsa_switcher_id', 'switcher_reference_id');
        $table->string('reference_type', 20)->nullable()->after('switcher_reference_id');
      });

      Schema::table('order_detail_indosmarts', function (Blueprint $table) {
        $table->renameColumn('order_detail_pulsa_switcher_id', 'switcher_reference_id');
        $table->string('reference_type', 20)->nullable()->after('switcher_reference_id');
      });

      Schema::table('order_detail_mkms', function (Blueprint $table) {
        $table->renameColumn('order_detail_pulsa_switcher_id', 'switcher_reference_id');
        $table->string('reference_type', 20)->nullable()->after('switcher_reference_id');
      });

      Schema::table('order_detail_jatelindos', function (Blueprint $table) {
        $table->renameColumn('order_detail_pulsa_switcher_id', 'switcher_reference_id');
        $table->string('reference_type', 20)->nullable()->after('switcher_reference_id');
      });

      Schema::table('order_detail_raja_billers', function (Blueprint $table) {
        $table->renameColumn('order_detail_pulsa_switcher_id', 'switcher_reference_id');
        $table->string('reference_type', 20)->nullable()->after('switcher_reference_id');
      });

      Schema::table('order_detail_spektrums', function (Blueprint $table) {
        $table->renameColumn('order_detail_pulsa_switcher_id', 'switcher_reference_id');
        $table->string('reference_type', 20)->nullable()->after('switcher_reference_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('order_detail_artajasas', function (Blueprint $table) {
        $table->renameColumn('switcher_reference_id', 'order_detail_pulsa_switcher_id');
        $table->dropColumn('reference_type');
      });

      Schema::table('order_detail_bakoels', function (Blueprint $table) {
        $table->renameColumn('switcher_reference_id', 'order_detail_pulsa_switcher_id');
        $table->dropColumn('reference_type');
      });

      Schema::table('order_detail_datacells', function (Blueprint $table) {
        $table->renameColumn('switcher_reference_id', 'order_detail_pulsa_switcher_id');
        $table->dropColumn('reference_type');
      });

      Schema::table('order_detail_indosmarts', function (Blueprint $table) {
        $table->renameColumn('switcher_reference_id', 'order_detail_pulsa_switcher_id');
        $table->dropColumn('reference_type');
      });

      Schema::table('order_detail_mkms', function (Blueprint $table) {
        $table->renameColumn('switcher_reference_id', 'order_detail_pulsa_switcher_id');
        $table->dropColumn('reference_type');
      });

      Schema::table('order_detail_jatelindos', function (Blueprint $table) {
        $table->renameColumn('switcher_reference_id', 'order_detail_pulsa_switcher_id');
        $table->dropColumn('reference_type');
      });

      Schema::table('order_detail_raja_billers', function (Blueprint $table) {
        $table->renameColumn('switcher_reference_id', 'order_detail_pulsa_switcher_id');
        $table->dropColumn('reference_type');
      });

      Schema::table('order_detail_spektrums', function (Blueprint $table) {
        $table->renameColumn('switcher_reference_id', 'order_detail_pulsa_switcher_id');
        $table->dropColumn('reference_type');
      });
    }
}
