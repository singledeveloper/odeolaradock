<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoReportVendorsTable extends Migration {

  public function up() {
    Schema::create('auto_report_vendors', function (Blueprint $table) {
      $table->increments('id');
      $table->string('from')->nullable();
      $table->string('to')->nullable();
      $table->string('vendor')->nullable();
      $table->string('type')->nullable();
      $table->timestamp('sent_at')->nullable();
      $table->bigInteger('reference_id')->nullable();
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('auto_report_vendors');
  }
}
