<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentOdeoBankTransferDetailsAddConfirmationDescriptionColumn extends Migration {

  public function up() {
    Schema::table('payment_odeo_bank_transfer_details', function (Blueprint $table) {
      $table->text('confirmation_description')->nullable();
    });
  }


  public function down() {
    Schema::table('payment_odeo_bank_transfer_details', function (Blueprint $table) {
      $table->dropColumn('confirmation_description');
    });
  }
}
