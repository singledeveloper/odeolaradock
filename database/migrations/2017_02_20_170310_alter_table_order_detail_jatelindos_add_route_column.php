<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailJatelindosAddRouteColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('order_detail_jatelindos', function (Blueprint $table) {
        $table->string('route', 50)->nullable()->after('order_detail_pulsa_switcher_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('order_detail_jatelindos', function (Blueprint $table) {
        $table->dropColumn('route');
      });
    }
}
