<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBillerDropForeignSwitcherReferenceId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('order_detail_artajasas', function (Blueprint $table) {
        $table->dropForeign('order_detail_artajasas_order_detail_pulsa_switcher_id_foreign');
      });

      Schema::table('order_detail_bakoels', function (Blueprint $table) {
        $table->dropForeign('order_detail_bakoels_order_detail_pulsa_switcher_id_foreign');
      });

      Schema::table('order_detail_datacells', function (Blueprint $table) {
        $table->dropForeign('order_detail_datacells_order_detail_pulsa_switcher_id_foreign');
      });

      Schema::table('order_detail_indosmarts', function (Blueprint $table) {
        $table->dropForeign('order_detail_indosmarts_order_detail_pulsa_switcher_id_foreign');
      });

      Schema::table('order_detail_mkms', function (Blueprint $table) {
        $table->dropForeign('order_detail_mkms_order_detail_pulsa_switcher_id_foreign');
      });

      Schema::table('order_detail_jatelindos', function (Blueprint $table) {
        $table->dropForeign('order_detail_jatelindos_order_detail_pulsa_switcher_id_foreign');
      });

      Schema::table('order_detail_raja_billers', function (Blueprint $table) {
        $table->dropForeign('order_detail_raja_billers_order_detail_pulsa_switcher_id_foreign');
      });

      Schema::table('order_detail_spektrums', function (Blueprint $table) {
        $table->dropForeign('order_detail_spektrums_order_detail_pulsa_switcher_id_foreign');
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
