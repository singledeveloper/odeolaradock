<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePostpaidRemoveAdditionalDataAddMeterChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('order_detail_postpaid_switchers', function (Blueprint $table) {
        $table->dropColumn('additional_data');
      });

      Schema::table('order_detail_postpaid_details', function (Blueprint $table) {
        $table->string('meter_changes', 50)->nullable()->before('created_at');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('order_detail_postpaid_switchers', function (Blueprint $table) {
        $table->text('additional_data');
      });

      Schema::table('order_detail_postpaid_details', function (Blueprint $table) {
        $table->dropColumn('meter_changes');
      });
    }
}
