<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePulsaOdeoInventoryFailuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('pulsa_odeo_inventory_failures', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('pulsa_odeo_inventory_id');
        $table->foreign('pulsa_odeo_inventory_id')->references('id')->on('pulsa_odeo_inventories')->onDelete('cascade');
        $table->integer('flag_count');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('pulsa_odeo_inventory_failures');
    }
}
