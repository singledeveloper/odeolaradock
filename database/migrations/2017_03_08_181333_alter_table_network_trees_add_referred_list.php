<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableNetworkTreesAddReferredList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('network_trees', function (Blueprint $table) {
        $table->text('referred_store_ids')->default('');
        $table->text('hustler_store_ids')->default('');
        $table->text('grand_hustler_store_ids')->default('');
        $table->text('team_store_ids')->default('');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('network_trees', function (Blueprint $table) {
        $table->dropColumn('referred_store_ids');
        $table->dropColumn('hustler_store_ids');
        $table->dropColumn('grand_hustler_store_ids');
        $table->dropColumn('team_store_ids');
      });
    }
}
