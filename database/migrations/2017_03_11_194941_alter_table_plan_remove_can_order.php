<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePlanRemoveCanOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('plans', function (Blueprint $table) {
        $table->dropColumn('can_order_bolt');
        $table->dropColumn('can_order_pln');
        $table->dropColumn('can_order_flight');
        $table->dropColumn('can_order_hotel');
        $table->dropColumn('can_order_credit_bill');
        $table->dropColumn('can_order_paket_data');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
