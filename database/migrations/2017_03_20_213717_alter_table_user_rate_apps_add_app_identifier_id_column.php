<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserRateAppsAddAppIdentifierIdColumn extends Migration {

  public function up() {
    Schema::table('user_rate_apps', function (Blueprint $table) {
      $table->bigInteger('app_identifier_id')->nullable();
    });
  }

  public function down() {
    Schema::table('user_rate_apps', function (Blueprint $table) {
      $table->dropColumn('app_identifier_id');
    });
  }
}
