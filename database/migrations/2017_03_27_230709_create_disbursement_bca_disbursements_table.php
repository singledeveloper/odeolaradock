<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisbursementBcaDisbursementsTable extends Migration {

  public function up() {
    Schema::create('disbursement_bca_disbursements', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('corporate_id')->nullable();
      $table->string('remark_1')->nullable();
      $table->string('remark_2')->nullable();
      $table->string('transfer_from')->nullable();
      $table->string('transfer_to')->nullable();
      $table->dateTime('transfer_datetime')->nullable();
      $table->date('transaction_date')->nullable();
      $table->integer('transaction_id')->nullable();
      $table->string('transaction_key')->unique()->nullable();
      $table->dateTime('response_datetime')->nullable();
      $table->decimal('amount', 17, 2)->nullable();
      $table->string('disbursement_type')->nullable();
      $table->string('status')->nullable();
      $table->string('bca_status')->nullable();
      $table->string('error_code')->nullable();
      $table->text('error_reason')->nullable();
      $table->text('error_exception_message')->nullable();
      $table->integer('error_exception_code')->nullable();
      $table->unsignedBigInteger('disbursement_reference_id')->nullable();
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('disbursement_bca_disbursements');
  }
}
