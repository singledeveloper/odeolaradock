<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTelegramOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegram_orders', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->bigInteger('order_id');
          $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
          $table->integer('chat_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('telegram_orders');
    }
}
