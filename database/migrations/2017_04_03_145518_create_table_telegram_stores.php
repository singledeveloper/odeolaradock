<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTelegramStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('telegram_stores', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('store_id')->nullable();
        $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
        $table->string('token', 255);
        $table->timestamps();
      });

      Schema::table('telegram_users', function(Blueprint $table){
        $table->bigInteger('telegram_store_id')->nullable();
        $table->foreign('telegram_store_id')->references('id')->on('telegram_stores')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('telegram_stores');
    }
}
