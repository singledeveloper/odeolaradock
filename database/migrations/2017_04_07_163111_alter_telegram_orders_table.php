<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTelegramOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('telegram_users', function(Blueprint $table){
        $table->dropForeign('telegram_users_telegram_store_id_foreign');
        $table->dropColumn('telegram_store_id');
      });

      Schema::table('telegram_orders', function(Blueprint $table){
        $table->bigInteger('telegram_store_id')->nullable();
        $table->foreign('telegram_store_id')->references('id')->on('telegram_stores')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
