<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterServiceDetailsTableAddMarginColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('service_details', function (Blueprint $table) {
        $table->decimal('default_hustler_margin', 10, 2)->nullable();
        $table->decimal('mentor_margin', 10, 2)->nullable();
        $table->decimal('leader_margin', 10, 2)->nullable();
        $table->decimal('max_margin', 10, 2)->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('service_details', function (Blueprint $table) {
        $table->dropColumn('default_hustler_margin');
        $table->dropColumn('mentor_margin');
        $table->dropColumn('leader_margin');
        $table->dropColumn('max_margin');
      });
    }
}
