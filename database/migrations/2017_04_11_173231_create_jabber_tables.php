<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJabberTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('jabber_stores', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('store_id')->nullable();
        $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
        $table->string('email', 50);
        $table->string('password', 50);
        $table->timestamps();
      });

      Schema::create('jabber_users', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('jabber_store_id');
        $table->foreign('jabber_store_id')->references('id')->on('jabber_stores')->onDelete('cascade');
        $table->bigInteger('user_id')->nullable();
        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        $table->string('email', 50)->nullable();
        $table->timestamps();
      });

      Schema::create('jabber_histories', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->text('update_id');
        $table->text('message');
        $table->timestamps();
      });

      Schema::create('jabber_orders', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('order_id');
        $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        $table->bigInteger('jabber_user_id');
        $table->foreign('jabber_user_id')->references('id')->on('jabber_users')->onDelete('cascade');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
