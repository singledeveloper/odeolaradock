<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentKredivoPaymentsAddColumnsKredivoV2 extends Migration {

  public function up() {
    Schema::table('payment_kredivo_payments', function (Blueprint $table) {
      $table->timestamp('transaction_time')->nullable();
      $table->decimal('amount', 17, 2)->nullable();
      $table->string('payment_type')->nullable();
      $table->string('message')->nullable();
    });
  }

  public function down() {
    Schema::table('payment_kredivo_payments', function (Blueprint $table) {
      $table->dropColumn('transaction_time');
      $table->dropColumn('amount');
      $table->dropColumn('payment_type');
      $table->dropColumn('message');
    });
  }
}
