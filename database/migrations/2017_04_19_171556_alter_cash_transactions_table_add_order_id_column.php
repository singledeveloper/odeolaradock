<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCashTransactionsTableAddOrderIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('cash_transactions', function (Blueprint $table) {
        $table->bigInteger('order_id')->unsigned()->nullable();
        $table->index('order_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('cash_transactions', function (Blueprint $table) {
        $table->dropIndex('cash_transactions_order_id_index');
        $table->dropColumn('order_id');
      });
    }
}
