<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableJabberUsersRemoveJabberStoreIdAddStoreId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('jabber_users', function(Blueprint $table){
        $table->dropForeign('jabber_users_jabber_store_id_foreign');
        $table->dropColumn('jabber_store_id');
        $table->integer('store_id')->nullable();
        $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
