<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserReceiptConfigsTable extends Migration {

  public function up() {
    Schema::create('user_receipt_configs', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->string('title')->nullable();
      $table->string('slogan')->nullable();
      $table->string('address')->nullable();
      $table->string('footer')->nullable();

      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('user_receipt_configs');
  }
}
