<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePostpaidTableRemoveAllUnrelatedTableWithCurrentSystem extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::drop('order_detail_postpaid_switchers');
    Schema::drop('postpaid_odeos');

    Schema::table('order_detail_spektrums', function (Blueprint $table) {
      $table->dropColumn('reference_type');
    });

    Schema::table('order_detail_datacells', function (Blueprint $table) {
      $table->dropColumn('reference_type');
    });

    Schema::table('order_detail_bakoels', function (Blueprint $table) {
      $table->dropColumn('reference_type');
    });

    Schema::table('order_detail_artajasas', function (Blueprint $table) {
      $table->dropColumn('reference_type');
    });

    Schema::table('double_purchase_suspects', function (Blueprint $table) {
      $table->dropColumn('reference_type');
    });

    Schema::table('order_detail_indosmarts', function (Blueprint $table) {
      $table->dropColumn('reference_type');
    });

    Schema::table('order_detail_jatelindos', function (Blueprint $table) {
      $table->dropColumn('reference_type');
    });

    Schema::table('order_detail_mkms', function (Blueprint $table) {
      $table->dropColumn('reference_type');
    });

    Schema::table('order_detail_pps', function (Blueprint $table) {
      $table->dropColumn('reference_type');
    });

    Schema::table('order_detail_raja_billers', function (Blueprint $table) {
      $table->dropColumn('reference_type');
    });

    Schema::table('order_detail_servindos', function (Blueprint $table) {
      $table->dropColumn('reference_type');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }
}
