<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserWithdrawsAddColumnVerifiedAt extends Migration {

  public function up() {
    Schema::table('user_withdraws', function (Blueprint $table) {
      $table->timestamp('verified_at')->nullable();
      $table->timestamp('cancelled_at')->nullable();
    });
  }


  public function down() {
    Schema::table('user_withdraws', function (Blueprint $table) {
      $table->dropColumn('verified_at');
      $table->dropColumn('cancelled_at');
    });
  }
}
