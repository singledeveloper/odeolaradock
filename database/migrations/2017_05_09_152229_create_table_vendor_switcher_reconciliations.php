<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVendorSwitcherReconciliations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_switcher_reconciliations', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->bigInteger('order_detail_pulsa_switcher_id')->nullable();
          $table->foreign('order_detail_pulsa_switcher_id')->references('id')->on('order_detail_pulsa_switchers')->onDelete('cascade');
          $table->integer('vendor_switcher_id')->unsigned();
          $table->foreign('vendor_switcher_id')->references('id')->on('vendor_switchers')->onDelete('cascade');
          $table->bigInteger('transaction_id');
          $table->string('biller_transaction_id', 255)->nullable();
          $table->integer('base_price')->nullable();
          $table->text('request')->nullable();
          $table->text('response')->nullable();
          $table->text('notify')->nullable();
          $table->string('reference_number', 255)->nullable();
          $table->char('status', 5);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
