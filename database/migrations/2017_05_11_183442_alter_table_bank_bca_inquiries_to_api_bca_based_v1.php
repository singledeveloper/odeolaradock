<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBankBcaInquiriesToApiBcaBasedV1 extends Migration {

  public function up() {
    Schema::table('bank_bca_inquiries', function (Blueprint $table) {
      $table->decimal('debit',17, 2)->nullable();
      $table->decimal('credit',17, 2)->nullable();
      $table->string('name')->nullable();

    });
  }

  public function down() {
    Schema::table('bank_bca_inquiries', function (Blueprint $table) {
      $table->dropColumn('debit');
      $table->dropColumn('credit');
      $table->dropColumn('name');
    });
  }
}
