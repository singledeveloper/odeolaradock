<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePulsaOdeoHistories extends Migration {

  public function up() {
    Schema::create('pulsa_odeo_histories', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('pulsa_odeo_id')->unsigned();
      $table->foreign('pulsa_odeo_id')->references('id')->on('pulsa_odeos')->onDelete('cascade');
      $table->integer('price');
      $table->timestamp("changed_at");
      $table->boolean('is_changed')->default(false);
      $table->timestamps();
    });
  }


  public function down() {
    //
  }
}
