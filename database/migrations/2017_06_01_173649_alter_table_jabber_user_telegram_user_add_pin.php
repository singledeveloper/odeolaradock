<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableJabberUserTelegramUserAddPin extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('jabber_users', function(Blueprint $table){
      $table->char('pin', 60)->nullable();
    });

    Schema::table('telegram_users', function(Blueprint $table){
      $table->char('pin', 60)->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //
  }
}
