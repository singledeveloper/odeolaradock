<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTelegramUsersChangeChatIdNullable extends Migration {

  public function up() {
    Schema::table('telegram_users', function (Blueprint $table) {
      $table->integer('chat_id')->nullable()->change();
    });
  }

  public function down() {
    //
  }
}
