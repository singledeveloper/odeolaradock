<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserFcmTokensDropColumnDeletedAt extends Migration {

  public function up() {
    Schema::table('user_fcm_tokens', function (Blueprint $table) {
      $table->dropColumn('deleted_at');
    });
  }

  public function down() {
    Schema::table('user_fcm_tokens', function (Blueprint $table) {
      $table->softDeletes();
    });
  }
}
