<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableJabberOrdersAddJabberStoreIdTemporary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('jabber_orders', function (Blueprint $table) {
        $table->bigInteger('jabber_store_id')->nullable();
        $table->foreign('jabber_store_id')->references('id')->on('jabber_stores')->onDelete('set null');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
