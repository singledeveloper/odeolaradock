<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInvoicesTable extends Migration {

  public function up() {
    Schema::create('user_invoices', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('invoice_number')->unique();
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->bigInteger('store_id')->unsigned();
      $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
      $table->string('billed_user_telephone', 20)->nullable();
      $table->bigInteger('billed_user_id')->unsigned()->nullable();
      $table->string('name', 128);
      $table->integer('category_id');
      $table->foreign('category_id')->references('id')->on('invoice_categories')->onDelete('set null');
      $table->text('notes');
      $table->longText('items')->default('');
      $table->decimal('total', 17, 2);
      $table->char('status', 5);
      $table->timestamp('due_date')->nullable();
      $table->timestamps();
      $table->timestamp('paid_at')->nullable();
    });
  }


  public function down() {
    Schema::drop('user_invoices');
  }
}
