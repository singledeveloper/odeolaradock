<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAndRecreateStoreInventoriesTable extends Migration {

  public function up() {

    Schema::drop('store_inventories');

    Schema::create('store_inventories', function (Blueprint $table) {

      $table->increments('id');

      $table->bigInteger('store_id');
      $table->foreign('store_id')->references('id')->on('stores')->onUpdate('cascade')->onDelete('cascade');

      $table->integer('service_id')->unsigned();
      $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');

      $table->integer('service_detail_id')->unsigned();
      $table->foreign('service_detail_id')->references('id')->on('service_details')->onDelete('cascade');

      $table->boolean('active')->default(true);

      $table->index(['store_id', 'service_id', 'service_detail_id']);

      $table->timestamps();

    });
  }

  public function down() {
    Schema::drop('store_inventories');
  }
}

