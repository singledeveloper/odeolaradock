<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreInventoryVendorsTable extends Migration {

  public function up() {
    Schema::create('store_inventory_vendors', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->bigInteger('store_inventory_id');
      $table->foreign('store_inventory_id')->references('id')->on('store_inventories')->onDelete('cascade');

      $table->bigInteger('vendor_id');
      $table->foreign('vendor_id')->references('id')->on('stores')->onDelete('cascade');

      $table->index(['store_inventory_id', 'vendor_id']);

      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('store_inventory_vendors');
  }
}
