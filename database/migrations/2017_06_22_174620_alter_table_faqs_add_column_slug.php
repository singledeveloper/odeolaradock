<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableFaqsAddColumnSlug extends Migration {

  public function up() {
    Schema::table('faqs', function (Blueprint $table) {
      $table->string('slug', 50)->nullable();
    });
  }


  public function down() {
    Schema::table('faqs', function (Blueprint $table) {
      $table->dropColumn('slug');
    });
  }
}
