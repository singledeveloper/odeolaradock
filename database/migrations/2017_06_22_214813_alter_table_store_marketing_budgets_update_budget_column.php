<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStoreMarketingBudgetsUpdateBudgetColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('store_marketing_budgets', function(Blueprint $table){
        $table->renameColumn('additional_budget', 'total_budget');
        $table->renameColumn('plan_price_per_day', 'budget_allocation');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('store_marketing_budgets', function(Blueprint $table){
        $table->renameColumn('total_budget', 'additional_budget');
        $table->renameColumn('budget_allocation', 'plan_price_per_day');
      });
    }
}
