<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePulsaOdeosAddColumnDeletedAt extends Migration {

  public function up() {
    Schema::table('pulsa_odeos', function (Blueprint $table) {
      $table->timestamp('deleted_at')->nullable();
    });
  }

  public function down() {
    Schema::table('pulsa_odeos', function (Blueprint $table) {
      $table->dropColumn('deleted_at');
    });
  }
}
