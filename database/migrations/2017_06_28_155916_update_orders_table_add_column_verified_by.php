<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdersTableAddColumnVerifiedBy extends Migration {

  public function up() {
    Schema::table('orders', function (Blueprint $table) {
      $table->bigInteger('verified_by')->unsigned()->nullable();
      $table->foreign('verified_by')->references('id')->on('users')->onDelete('set null');
    });
  }


  public function down() {
    Schema::table('orders', function (Blueprint $table) {
      $table->dropColumn('verified_by');
    });
  }
}
