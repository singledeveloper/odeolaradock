<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreInventoryGroupPrices extends Migration {

  public function up() {
    Schema::create('store_inventory_group_prices', function (Blueprint $table) {

      $table->bigIncrements('id');

      $table->string('type');

      $table->string('name')->nullable();

      $table->string('description')->nullable();

      $table->timestamp('pricing_apply_at')->nullable();

      $table->index(['type']);

      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('store_inventory_group_prices');
  }
}
