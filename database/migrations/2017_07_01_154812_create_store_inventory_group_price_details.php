<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreInventoryGroupPriceDetails extends Migration {

  public function up() {
    Schema::create('store_inventory_group_price_details', function (Blueprint $table) {

      $table->bigIncrements('id');

      $table->bigInteger('store_inventory_detail_id')->nullable();
      $table->foreign('store_inventory_detail_id')->references('id')->on('store_inventory_details')->onDelete('cascade');

      $table->bigInteger('store_inventory_group_price_id')->nullable();
      $table->foreign('store_inventory_group_price_id')->references('id')->on('store_inventory_group_prices')->onDelete('cascade');

      $table->decimal('sell_price', 17, 2)->nullable();
      $table->decimal('planned_price', 17, 2)->nullable();

      $table->index(['store_inventory_detail_id', 'store_inventory_group_price_id']);

      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('store_inventory_group_price_details');
  }
}
