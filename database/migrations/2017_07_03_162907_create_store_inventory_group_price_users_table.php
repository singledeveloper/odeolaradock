<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreInventoryGroupPriceUsersTable extends Migration {

  public function up() {
    Schema::create('store_inventory_group_price_users', function (Blueprint $table) {

      $table->bigIncrements('id');

      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

      $table->bigInteger('store_inventory_group_price_id')->nullable();
      $table->foreign('store_inventory_group_price_id')->references('id')->on('store_inventory_group_prices')->onDelete('cascade');

      $table->bigInteger('new_store_inventory_group_price_id')->nullable();
      $table->foreign('new_store_inventory_group_price_id')->references('id')->on('store_inventory_group_prices')->onDelete('cascade');

      $table->timestamp('group_price_apply_at')->nullable();

      $table->index(['user_id', 'store_inventory_group_price_id']);

      $table->timestamps();

    });
  }


  public function down() {
    Schema::drop('store_inventory_group_price_users');
  }
}
