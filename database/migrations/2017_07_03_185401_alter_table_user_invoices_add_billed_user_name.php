<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserInvoicesAddBIlledUserName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('user_invoices', function(Blueprint $table){
        $table->string('billed_user_name', 50)->default('');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('user_invoices', function(Blueprint $table){
        $table->dropColumn('billed_user_name');
      });
    }
}
