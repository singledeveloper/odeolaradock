<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBankBriInquiriesRenameRemark extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('bank_bri_inquiries', function(Blueprint $table){
        $table->renameColumn('remark', 'description');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('bank_bri_inquiries', function(Blueprint $table){
        $table->renameColumn('description', 'remark');
      });
    }
}
