<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQlusterUserInvoicesTable extends Migration {

  public function up() {
    Schema::create('qluster_user_invoices', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('invoice_number')->unique();
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->bigInteger('billed_qluster_user_id')->unsigned()->nullable();
      $table->string('period');
      $table->string('name', 128);
      $table->longText('items')->default('');
      $table->decimal('subtotal', 17, 2);
      $table->decimal('charges', 17, 2)->default(0);
      $table->decimal('total', 17, 2);
      $table->decimal('late_charge', 5, 2)->nullable();
      $table->integer('late_charge_interval')->nullable();
      $table->boolean('enable_late_charge')->default(false);
      $table->boolean('enable_partial_payment')->default(false);
      $table->decimal('minimum_payment', 5, 2)->default(0);
      $table->char('status', 5);
      $table->timestamp('due_date')->nullable();
      $table->timestamps();
      $table->decimal('total_paid', 17, 2)->default(0);
      $table->timestamp('paid_at')->nullable();
      $table->string('parent_invoice_id')->nullable();
      $table->string('child_invoice_ids')->default('');
      $table->decimal('total_charges_child', 17, 2)->default(0);
      $table->decimal('total_child', 17, 2)->default(0);
    });
  }


  public function down() {
    Schema::drop('qluster_user_invoices');
  }
}
