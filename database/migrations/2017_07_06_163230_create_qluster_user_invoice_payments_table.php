<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQlusterUserInvoicePaymentsTable extends Migration {

  public function up() {
    Schema::create('qluster_user_invoice_payments', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('invoice_id')->unsigned();
      $table->foreign('invoice_id')->references('id')->on('qluster_user_invoices')->onDelete('set null');
      $table->bigInteger('order_id')->unsigned();
      $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');
      $table->decimal('amount', 17, 2);
      $table->timestamp('paid_at')->nullable();
    });
  }


  public function down() {
    Schema::drop('qluster_user_invoice_payments');
  }
}
