<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQlusterUserInvoiceChargesTable extends Migration {

  public function up() {
    Schema::create('qluster_user_invoice_charges', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('invoice_id')->unsigned();
      $table->foreign('invoice_id')->references('id')->on('qluster_user_invoices')->onDelete('set null');
      $table->decimal('charged_amount', 17, 2);
      $table->decimal('total_charges', 17, 2);
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('qluster_user_invoice_charges');
  }
}
