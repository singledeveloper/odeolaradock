<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSmsSwitchers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('sms_switchers', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->string('number', 20);
        $table->text('sms_text');
        $table->text('vendor_query_dumps')->nullable();
        $table->timestamps();
      });

      Schema::table('sms_nexmo_logs', function(Blueprint $table) {
        $table->bigInteger('sms_switcher_id')->nullable();
        $table->foreign('sms_switcher_id')->references('id')->on('sms_switchers')->onDelete('cascade');
      });

      Schema::table('sms_zenziva_logs', function(Blueprint $table) {
        $table->bigInteger('sms_switcher_id')->nullable();
        $table->foreign('sms_switcher_id')->references('id')->on('sms_switchers')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
