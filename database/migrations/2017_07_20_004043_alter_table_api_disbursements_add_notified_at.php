<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableApiDisbursementsAddNotifiedAt extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('api_disbursements', function (Blueprint $table) {
      $table->datetime('notified_at')->nullable();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('api_disbursements', function (Blueprint $table) {
      $table->dropColumn('notified_at');
    });
  }
}
