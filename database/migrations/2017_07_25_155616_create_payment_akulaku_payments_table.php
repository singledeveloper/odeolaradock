<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentAkulakuPaymentsTable extends Migration
{
    public function up()
    {
      Schema::create('payment_akulaku_payments', function (Blueprint $table) {
        $table->increments('id');
        $table->string('ref_no')->nullable();
        $table->string('payment_entry_url')->nullable();
        $table->string('status')->nullable();
        $table->text('log_request')->nullable();
        $table->text('log_response')->nullable();
        $table->text('log_notify')->nullable();
        $table->boolean('is_verified')->default(false);
        $table->timestamps();
      });
    }

    public function down()
    {
      Schema::drop('payment_akulaku_payments');
    }
}
