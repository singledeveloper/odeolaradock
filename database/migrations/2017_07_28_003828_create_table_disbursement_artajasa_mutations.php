<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDisbursementArtajasaMutations extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('disbursement_artajasa_mutations', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->bigInteger('disbursement_artajasa_disbursement_id')->unsigned()->nullable();
      $table->foreign('disbursement_artajasa_disbursement_id')->references('id')->on('disbursement_artajasa_disbursements')->onDelete('set null');

      $table->decimal('amount', 17, 2);
      $table->decimal('fee', 17, 2);

      $table->decimal('balance_before', 17, 2);
      $table->decimal('balance_after', 17, 2);

      $table->string('type');

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('disbursement_artajasa_mutations');
  }
}
