<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvincesTable extends Migration
{
    public function up()
    {
      Schema::create('provinces', function (Blueprint $table) {
        $table->unsignedInteger('id');
        $table->primary('id');
        $table->string('name');
        $table->timestamps();
      });

    }

    public function down()
    {
      Schema::drop('provinces');
    }
}
