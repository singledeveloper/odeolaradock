<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsersAddAddressDetailColumns extends Migration
{
  public function up() {
    Schema::table('users', function (Blueprint $table) {
      $table->integer('province_id')->unsigned()->nullable();
      $table->foreign('province_id')->references('id')->on('provinces');
      $table->integer('city_id')->unsigned()->nullable();
      $table->foreign('city_id')->references('id')->on('cities');
      $table->string('street')->nullable();
      $table->string('postcode')->nullable();
    });
  }

  public function down() {
    Schema::table('users', function (Blueprint $table) {
      $table->dropColumn('province_id');
      $table->dropColumn('city_id');
      $table->dropColumn('street');
      $table->dropColumn('postcode');
    });
  }
}
