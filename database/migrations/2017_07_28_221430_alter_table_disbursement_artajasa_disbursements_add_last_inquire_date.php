<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDisbursementArtajasaDisbursementsAddLastInquireDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('disbursement_artajasa_disbursements', function (Blueprint $table) {
            $table->datetime('last_inquire_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('disbursement_artajasa_disbursements', function (Blueprint $table) {
            $table->dropColumn('last_inquire_date');
        });
    }
}
