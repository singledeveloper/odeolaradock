<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailsAddColumnAgentBasePrice extends Migration {

  public function up() {
    Schema::table('order_details', function (Blueprint $table) {
      $table->decimal('agent_base_price', 17, 2)->nullable();
    });
  }

  public function down() {
    Schema::table('order_details', function (Blueprint $table) {
      $table->dropColumn('agent_base_price');
    });
  }
}
