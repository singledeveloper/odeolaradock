<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStoreTempDepositsAddColumnAgentAmount extends Migration {

  public function up() {
    Schema::table('store_temp_deposits', function (Blueprint $table) {
      $table->decimal('agent_amount', 17, 2)->default(0);
    });
  }

  public function down() {
    Schema::table('store_temp_deposits', function (Blueprint $table) {
      $table->dropColumn('agent_amount');
    });
  }
}
