<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVirtualAccountTable extends Migration {

  public function up() {
    Schema::create('user_virtual_accounts', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id');
      $table->integer('biller');
      $table->bigInteger('store_id')->nullable();
      $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
      $table->integer('service_detail_id')->nullable();
      $table->string('service_reference_id')->nullable();
      $table->decimal('amount', 15, 2)->default(0);
      $table->bigInteger('order_id')->nullable();
      $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('user_virtual_accounts');
  }
}
