<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVirtualAccountVendorTable extends Migration {

  public function up() {
    Schema::create('virtual_account_vendors', function (Blueprint $table) {
      $table->increments('id');
      $table->string('code');
      $table->string('name');
      $table->string('prefix');
      $table->boolean('support_fixed_payment');
      $table->boolean('support_open_payment');
      $table->boolean('support_partial_payment');
      $table->decimal('fee', 15, 2);
      $table->integer('opc');
      $table->foreign('opc')->references('code')->on('payment_odeo_payment_channels')->onDelete('set null');
      $table->boolean('is_active');
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('virtual_account_vendors');
  }
}
