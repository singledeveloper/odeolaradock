<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailsAddColumnStoreInventoryDetailId extends Migration {

  public function up() {
    Schema::table('order_details', function (Blueprint $table) {
      $table->bigInteger('store_inventory_detail_id')->nullable();
      $table->foreign('store_inventory_detail_id')->references('id')->on('store_inventory_details')->onDelete('set null');
    });
  }

  public function down() {
    Schema::table('order_details', function (Blueprint $table) {
      $table->dropColumn('store_inventory_detail_id');
    });
  }
}