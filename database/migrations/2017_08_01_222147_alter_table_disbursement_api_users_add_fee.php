<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDisbursementApiUsersAddFee extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('disbursement_api_users', function (Blueprint $table) {
      $table->decimal('fee', 17, 2)->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('disbursement_api_users', function (Blueprint $table) {
      $table->dropColumn('fee');
    });
  }
}
