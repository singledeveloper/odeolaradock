<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentMasterSalesTable extends Migration {

  public function up() {
    Schema::create('agent_master_sales', function (Blueprint $table) {

      $table->bigIncrements('id');
      $table->date('date');

      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

      $table->bigInteger('store_id')->unsigned();
      $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');

      $table->integer('sales_quantity')->unsigned()->default(0);
      $table->decimal('sales_amount', 17, 2)->default(0);
      $table->decimal('base_sales_amount', 17, 2)->default(0);

      $table->bigInteger('service_id');
      $table->foreign('service_id')->references('id')->on('services')->onDelete('set null');

      $table->index(['user_id', 'store_id', 'service_id', 'date']);

      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('agent_master_sales');
  }
}
