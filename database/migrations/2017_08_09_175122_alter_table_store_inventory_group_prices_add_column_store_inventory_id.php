<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStoreInventoryGroupPricesAddColumnStoreInventoryId extends Migration {

  public function up() {
    Schema::table('store_inventory_group_prices', function (Blueprint $table) {
      $table->bigInteger('store_inventory_id');
      $table->foreign('store_inventory_id')->references('id')->on('store_inventories')->onDelete('cascade');

    });
  }

  public function down() {
    Schema::table('store_inventory_group_prices', function (Blueprint $table) {
      $table->dropColumn('store_inventory_id');
    });
  }
}
