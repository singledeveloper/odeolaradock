<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStoreTempDepositsAddColumnSaleAmountAndDescription extends Migration {

  public function up() {
    Schema::table('store_temp_deposits', function (Blueprint $table) {
      $table->dropColumn('agent_amount');
      $table->decimal('sale_amount', 17, 2)->nullable();
      $table->text('description')->nullable();
    });
  }


  public function down() {
    Schema::table('store_temp_deposits', function (Blueprint $table) {
      $table->dropColumn('sale_amount');
      $table->dropColumn('description');
    });
  }
}
