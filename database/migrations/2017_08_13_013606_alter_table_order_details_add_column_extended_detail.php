<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailsAddColumnExtendedDetail extends Migration {

  public function up() {
    Schema::table('order_details', function (Blueprint $table) {
      $table->text('extended_detail')->nullable();
    });
  }


  public function down() {
    Schema::table('order_details', function (Blueprint $table) {
      $table->dropColumn('extended_detail');

    });
  }
}
