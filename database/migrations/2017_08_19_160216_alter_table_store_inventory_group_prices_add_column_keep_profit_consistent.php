<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStoreInventoryGroupPricesAddColumnKeepProfitConsistent extends Migration {

  public function up() {
    Schema::table('store_inventory_group_prices', function (Blueprint $table) {
      $table->boolean('keep_profit_consistent')->default(true);
    });
  }


  public function down() {
  }
}
