<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailVoucherGameDetailsTable extends Migration
{
    public function up()
    {
      Schema::create('order_detail_voucher_game_details', function (Blueprint $table) {
        $table->increments('id');
        $table->bigInteger('order_detail_pulsa_switcher_id');
        $table->foreign('order_detail_pulsa_switcher_id')->references('id')->on('order_detail_pulsa_switchers')->onDelete('cascade');
        $table->string('email', 10);
        $table->string('pin', 10);
        $table->integer('base_price');
        $table->integer('admin_fee');
        $table->timestamps();
      });
    }

    public function down()
    {
      Schema::drop('order_detail_voucher_game_details');
    }
}
