<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserVirtualAccountsAddDisplayName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('user_virtual_accounts', function(Blueprint $table){
        $table->string('display_name', 50)->default('');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('user_virtual_accounts', function(Blueprint $table){
        $table->dropColumn('display_name');
      });
    }
}
