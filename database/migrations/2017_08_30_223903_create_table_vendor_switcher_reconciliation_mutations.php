<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVendorSwitcherReconciliationMutations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_switcher_reconciliation_mutations', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->bigInteger('order_detail_pulsa_switcher_id')->nullable();
          $table->foreign('order_detail_pulsa_switcher_id')->references('id')->on('order_detail_pulsa_switchers')->onDelete('cascade');
          $table->integer('vendor_switcher_id')->unsigned();
          $table->foreign('vendor_switcher_id')->references('id')->on('vendor_switchers')->onDelete('cascade');
          $table->bigInteger('transaction_id')->nullable();
          $table->integer('amount')->nullable();
          $table->integer('balance_before')->nullable();
          $table->integer('balance_after');
          $table->integer('last_balance_recorded_from_biller')->nullable();
          $table->boolean('is_reconciled')->default(false);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
