<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStoreAgentMasterSettingsAddColumnAutoApproveAgent extends Migration {

  public function up() {
    Schema::table('store_agent_master_settings', function (Blueprint $table) {
      $table->boolean('auto_approve_agent')->default(true);
    });
  }


  public function down() {
    Schema::table('store_agent_master_settings', function (Blueprint $table) {
      $table->removeColumn('auto_approve_agent');
    });
  }
}
