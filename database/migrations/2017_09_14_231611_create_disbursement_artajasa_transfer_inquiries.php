<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDisbursementArtajasaTransferInquiries extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('disbursement_artajasa_transfer_inquiries', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->string('reference_type')->nullable();
      $table->unsignedBigInteger('reference_id')->nullable();

      $table->integer('transaction_id')->nullable();
      $table->dateTime('transaction_datetime')->nullable();
      $table->string('channel_type')->nullable();

      $table->string('bank_code')->nullable();
      $table->decimal('amount', 17, 2)->nullable();
      $table->string('transfer_to')->nullable();
      $table->string('transfer_to_name')->nullable();
      $table->string('purpose_desc')->nullable();

      $table->string('status')->nullable();
      $table->string('response_code')->nullable();
      $table->text('response_description')->nullable();
      $table->dateTime('response_datetime')->nullable();
      $table->text('error_exception_message')->nullable();
      $table->integer('error_exception_code')->nullable();

      $table->index(['reference_type', 'reference_id']);

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('disbursement_artajasa_transfer_inquiries');
  }
}
