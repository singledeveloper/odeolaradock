<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentBniEcollectionPaymentsTable extends Migration
{
    public function up()
    {
      Schema::create('payment_bni_ecollection_payments', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('user_virtual_account_id')->unsigned()->nullable();
        $table->foreign('user_virtual_account_id')->references('id')->on('user_virtual_accounts');
        $table->bigInteger('order_id')->unsigned()->nullable();
        $table->foreign('order_id')->references('id')->on('orders');
        $table->text('log_request')->nullable();
        $table->text('log_response')->nullable();
        $table->text('log_notify')->nullable();
        $table->string('payment_ntb')->nullable();
        $table->timestamps();
      });
    }

    public function down()
    {
      Schema::drop('payment_bni_ecollection_payments');
    }
}
