<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignMembersTable extends Migration {

  public function up() {
    Schema::create('campaign_members', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('user_id');
      $table->foreign('user_id')
        ->references('id')
        ->on('users')
        ->onUpdate('cascade')
        ->onDelete('cascade');
      $table->string('campaign');
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('campaign_members');
  }
}
