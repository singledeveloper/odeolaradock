<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterTableArtajasaStatusInquiryAddStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('disbursement_artajasa_status_inquiries', function (Blueprint $table) {
        $table->string('status')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('disbursement_artajasa_status_inquiries', function (Blueprint $table) {
        $table->dropColumn('status');
      });
    }
}
