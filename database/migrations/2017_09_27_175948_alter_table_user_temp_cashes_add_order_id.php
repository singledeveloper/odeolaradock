<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserTempCashesAddOrderId extends Migration {

  public function up() {
    Schema::table('user_temp_cashes', function (Blueprint $table) {
      $table->bigInteger('order_id')->nullable();
      $table->foreign('order_id')->references('id')->on('orders')->onUpdate('cascade');
    });
  }


  public function down() {
    Schema::table('user_temp_cashes', function (Blueprint $table) {
      $table->dropColumn('order_id');
    });
  }
}
