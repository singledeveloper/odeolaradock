<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisbursementsCimbDisbursementTable extends Migration
{
  public function up() {
    Schema::create('disbursement_cimb_disbursements', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->decimal('amount', 17, 2)->nullable();
      $table->string('disbursement_type')->nullable();
      $table->string('status')->nullable();

      $table->string('transfer_to')->nullable();
      $table->string('status_code')->nullable();
      $table->string('status_message')->nullable();
      $table->string('bank_reff_no')->nullable();
      $table->string('transaction_date')->nullable();
      $table->string('response_datetime')->nullable();
      $table->string('memo')->nullable();

      $table->string('error_code')->nullable();
      $table->text('error_message')->nullable();
      $table->text('error_log_response')->nullable();

      $table->unsignedBigInteger('disbursement_reference_id')->nullable();
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('disbursement_cimb_disbursements');
  }
}
