<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentBniEcollectionPaymentsAddColumnExpiredAt extends Migration {

  public function up() {
    Schema::table('payment_bni_ecollection_payments', function (Blueprint $table) {
      $table->timestamp('expired_at')->nullable();
    });
  }


  public function down() {
    Schema::table('payment_bni_ecollection_payments', function (Blueprint $table) {
      $table->dropColumn('expired_at');
    });
  }
}
