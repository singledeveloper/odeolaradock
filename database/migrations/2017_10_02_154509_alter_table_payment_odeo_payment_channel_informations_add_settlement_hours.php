<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentOdeoPaymentChannelInformationsAddSettlementHours extends Migration {

  public function up() {
    Schema::table('payment_odeo_payment_channel_informations', function (Blueprint $table) {
      $table->smallInteger('settlement_hours')->default(0);
    });
  }


  public function down() {
    Schema::table('payment_odeo_payment_channel_informations', function (Blueprint $table) {
      $table->dropColumn('settlement_hours');
    });
  }
}
