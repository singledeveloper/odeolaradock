<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreDistributionChannelsTable extends Migration {

  public function up() {
    Schema::create('store_distribution_channels', function (Blueprint $table) {

      $table->bigIncrements('id');

      $table->bigInteger('store_id')->unsigned();
      $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');

      $table->boolean('marketplace')->default(false);
      $table->boolean('webstore')->default(false);

      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('store_distribution_channels');
  }
}
