<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSmsCentersAddPulsaRecurringId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('sms_centers', function (Blueprint $table) {
        $table->bigInteger('pulsa_recurring_id')->nullable();
        $table->foreign('pulsa_recurring_id')->references('id')->on('pulsa_recurrings')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
