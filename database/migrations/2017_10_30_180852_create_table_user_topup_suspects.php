<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserTopupSuspects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_topup_bank_transfers', function(Blueprint $table){
          $table->increments('id');
          $table->integer('user_id');
          $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
          $table->integer('from_bank_id')->nullable();
          $table->string('from_bank_account_name');
          $table->boolean('is_suspected')->default(true);
          $table->string('route', 255);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
