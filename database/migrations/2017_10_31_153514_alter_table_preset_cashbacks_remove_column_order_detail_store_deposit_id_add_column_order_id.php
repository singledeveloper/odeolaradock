<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePresetCashbacksRemoveColumnOrderDetailStoreDepositIdAddColumnOrderId extends Migration {

  public function up() {
    Schema::table('preset_cashbacks', function (Blueprint $table) {

      $table->dropColumn('order_detail_store_deposit_id');
      $table->bigInteger('order_id')->unsigned();
      $table->foreign('order_id')
        ->references('id')->on('orders')->onDelete('cascade');
    });
  }


  public function down() {
    Schema::table('preset_cashbacks', function (Blueprint $table) {

    });
  }
}
