<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetitorPricesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('competitor_prices', function (Blueprint $table) {

      $table->bigIncrements('id');

      $table->string('competitor');

      $table->integer('pulsa_odeo_id')->unsigned();
      $table->foreign('pulsa_odeo_id')->references('id')->on('pulsa_odeos')->onDelete('cascade');

      $table->decimal('price', 17, 2);

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('competitor_prices');
  }
}
