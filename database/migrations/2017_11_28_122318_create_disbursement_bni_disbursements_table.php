<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisbursementBniDisbursementsTable extends Migration {

  public function up() {
    Schema::create('disbursement_bni_disbursements', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('remark')->nullable();
      $table->string('transfer_from')->nullable();
      $table->string('transfer_to')->nullable();
      $table->dateTime('transfer_datetime')->nullable();
      $table->string('currency')->nullable();
      $table->string('bank_reference')->nullable();
      $table->text('customer_reference')->nullable();
      $table->dateTime('response_datetime')->nullable();
      $table->decimal('amount', 17, 2)->nullable();
      $table->string('disbursement_type')->nullable();
      $table->string('status')->nullable();
      $table->text('error_message')->nullable();

      $table->unsignedBigInteger('disbursement_reference_id')->nullable();
      $table->index(['disbursement_reference_id', 'disbursement_type']);
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('disbursement_bni_disbursements');
  }
}
