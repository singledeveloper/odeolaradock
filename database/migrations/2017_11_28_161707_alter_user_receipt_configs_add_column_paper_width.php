<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserReceiptConfigsAddColumnPaperWidth extends Migration {

  public function up() {
    Schema::table('user_receipt_configs', function (Blueprint $table) {
      $table->mediumInteger('paper_width')->default(58);
    });
  }


  public function down() {
    Schema::table('user_receipt_configs', function (Blueprint $table) {
      $table->dropColumn('paper_width');
    });
  }
}
