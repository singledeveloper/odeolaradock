<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankInquiryAdditionalInformationsTable extends Migration
{
    public function up()
    {
      Schema::create('bank_inquiry_additional_informations', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('reference_id');
        $table->string('bank_reference');
        $table->text('description')->nullable();
        $table->text('pdf_url')->nullable();
        $table->index(['reference_id', 'bank_reference']);

        $table->timestamps();
      });
    }

    public function down()
    {
        //
    }
}
