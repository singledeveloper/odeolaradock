<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAffiliateUserInvoiceNotifyLogs extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('affiliate_user_invoice_notify_logs', function(Blueprint $table){
      $table->increments('id');
      $table->bigInteger('user_id')->nullable();
      $table->string('invoice_number', 16);
      $table->text('request');
      $table->text('response');
      $table->integer('response_code');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('affiliate_user_invoice_notify_logs');
  }
}
