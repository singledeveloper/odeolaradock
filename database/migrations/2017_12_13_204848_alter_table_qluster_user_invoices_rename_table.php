<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableQlusterUserInvoicesRenameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::rename('qluster_user_invoices', 'affiliate_user_invoices');
      Schema::rename('qluster_user_invoice_payments', 'affiliate_user_invoice_payments');
      Schema::rename('qluster_user_invoice_charges', 'affiliate_user_invoice_charges');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::rename('affiliate_user_invoices', 'qluster_user_invoices');
      Schema::rename('affiliate_user_invoice_payments', 'qluster_user_invoice_payments');
      Schema::rename('affiliate_user_invoice_charges', 'qluster_user_invoice_charges');
    }
}
