<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAffiliateUserInvoicesAddInvoiceConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('affiliate_user_invoices', function(Blueprint $table){
        $table->boolean('enable_notification')->default(false);
        $table->decimal('biller_fee', 15, 2)->default(0);
        $table->integer('biller_id')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('affiliate_user_invoices', function(Blueprint $table){
        $table->dropColumn('enable_notification');
        $table->dropColumn('biller_fee');
        $table->dropColumn('biller_id');
      });
    }
}
