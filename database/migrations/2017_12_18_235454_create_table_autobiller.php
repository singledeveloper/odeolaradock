<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAutobiller extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('vendor_switcher_reconciliations', function(Blueprint $table){
        $table->bigInteger('transaction_id')->nullable()->change();
        $table->integer('sale_amount')->nullable();
      });

      Schema::table('vendor_switchers', function(Blueprint $table){
        $table->increments('id')->change();
        $table->integer('owner_store_id')->nullable();
        $table->foreign('owner_store_id')->references('id')->on('stores')->onDelete('set null');
        $table->time('recon_started_at')->nullable();
        $table->time('recon_ended_at')->nullable();
        $table->integer('recon_interval')->default(0);
        $table->string('notify_slug', 255)->nullable();
        $table->text('info_to_client')->nullable();
        $table->index(['owner_store_id']);
      });

      Schema::table('vendor_switchers', function(Blueprint $table){
        DB::update("ALTER SEQUENCE vendor_switchers_id_seq RESTART WITH 100");
      });

      Schema::table('pulsa_odeos', function(Blueprint $table){
        $table->integer('owner_store_id')->nullable();
        $table->foreign('owner_store_id')->references('id')->on('stores')->onDelete('set null');
        $table->index(['owner_store_id']);
      });

      Schema::table('pulsa_odeo_inventories', function(Blueprint $table){
        $table->integer('store_base_price')->nullable();
        $table->integer('base_price')->nullable()->change();
      });

      Schema::create('vendor_switcher_templates', function(Blueprint $table){
        $table->increments('id');
        $table->string('name', 255);
        $table->integer('predefined_store_id')->nullable();
        $table->foreign('predefined_store_id')->references('id')->on('stores')->onDelete('set null');
        $table->string('format_type', 20); // xml, xmlrpc, json, text, http_query
        $table->text('headers')->nullable();
        $table->string('body_title', 255)->nullable();
        $table->text('body_details'); // json if not text
        $table->boolean('is_active')->default(true);
        $table->timestamps();
        $table->index(['predefined_store_id']);
      });

      Schema::create('vendor_switcher_template_responses', function(Blueprint $table){
        $table->increments('id');
        $table->integer('template_id')->nullable();
        $table->foreign('template_id')->references('id')->on('vendor_switcher_templates')->onDelete('cascade');
        $table->integer('predefined_vendor_switcher_id')->nullable();
        $table->foreign('predefined_vendor_switcher_id')->references('id')->on('vendor_switchers')->onDelete('cascade');
        $table->string('format_type', 20); // xml, xmlrpc, json, text, http_query
        $table->string('route_type', 20); // response, report
        $table->boolean('is_active')->default(true);
        $table->timestamps();
        $table->index(['template_id', 'predefined_vendor_switcher_id']);
      });

      Schema::create('vendor_switcher_template_response_details', function(Blueprint $table){
        $table->increments('id');
        $table->integer('template_response_id')->nullable();
        $table->foreign('template_response_id')->references('id')->on('vendor_switcher_template_responses')->onDelete('cascade');
        $table->integer('predefined_vendor_switcher_id')->nullable();
        $table->foreign('predefined_vendor_switcher_id')->references('id')->on('vendor_switchers')->onDelete('cascade');
        $table->string('response_status_type', 20); // fail, success, loop_fail, in_queue
        $table->text('response_details'); // json if not text, conditions of how the response should be at response_type.
        $table->boolean('is_active')->default(true);
        $table->timestamps();
        $table->index(['template_response_id', 'predefined_vendor_switcher_id']);
      });

      Schema::create('vendor_switcher_connections', function(Blueprint $table){
        $table->increments('id');
        $table->integer('vendor_switcher_id');
        $table->foreign('vendor_switcher_id')->references('id')->on('vendor_switchers')->onDelete('set null');
        $table->integer('template_id')->nullable();
        $table->foreign('template_id')->references('id')->on('stores')->onDelete('set null');
        $table->string('server_destination', 255); // can be an email if jabber
        $table->string('server_user_id', 255)->nullable();
        $table->string('server_password', 255)->nullable();
        $table->string('server_pin', 255)->nullable();
        $table->string('server_token', 255)->nullable();
        $table->timestamps();
        $table->index(['vendor_switcher_id', 'template_id']);
      });

      Schema::create('vendor_switcher_connection_logs', function(Blueprint $table){
        $table->increments('id');
        $table->integer('vendor_switcher_id');
        $table->foreign('vendor_switcher_id')->references('id')->on('vendor_switchers')->onDelete('set null');
        $table->string('type', 255);
        $table->text('description');
        $table->timestamps();
        $table->index(['vendor_switcher_id']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
