<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMailSettingsTable extends Migration {

  public function up() {
    Schema::create('user_mail_settings', function (Blueprint $table) {
      $table->increments('id');
      $table->boolean('purchase_receipt')->default(false);
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('user_mail_settings');
  }
}
