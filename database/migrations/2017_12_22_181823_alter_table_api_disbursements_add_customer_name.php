<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableApiDisbursementsAddCustomerName extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('api_disbursements', function (Blueprint $table) {
      $table->string('customer_name')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('api_disbursements', function (Blueprint $table) {
      $table->dropColumn('customer_name');
    });
  }
}
