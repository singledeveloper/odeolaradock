<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDisbursementApiUsersAddAlias extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('disbursement_api_users', function (Blueprint $table) {
      $table->string('alias')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('disbursement_api_users', function (Blueprint $table) {
      $table->dropColumn('alias');
    });
  }
}
