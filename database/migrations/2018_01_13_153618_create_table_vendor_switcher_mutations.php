<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVendorSwitcherMutations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_switcher_mutations', function(Blueprint $table){
          $table->increments('id');
          $table->integer('vendor_switcher_id')->nullable();
          $table->foreign('vendor_switcher_id')->references('id')->on('vendor_switchers')->onDelete('cascade');
          $table->integer('current_mutation');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
