<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDisbursementBriDisbursementsRenameBriStatus extends Migration {

  public function up() {
    Schema::table('disbursement_bri_disbursements', function (Blueprint $table) {
      $table->renameColumn('status_bri', 'bri_status');
    });
  }


  public function down() {
    Schema::table('disbursement_bri_disbursements', function (Blueprint $table) {
      //
    });
  }
}
