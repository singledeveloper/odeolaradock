<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDisbursementBriDisbursementsAddColumnResponseLog extends Migration {

  public function up() {
    Schema::table('disbursement_bri_disbursements', function (Blueprint $table) {
      $table->text('response_log')->nullable();
    });
  }


  public function down() {
    Schema::table('disbursement_bri_disbursements', function (Blueprint $table) {
      //
    });
  }
}
