<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableVendorSwitchersAddBriAccountNumber extends Migration {

  public function up() {
    Schema::table('vendor_switchers', function (Blueprint $table) {
      $table->string('bri_account_number', 255)->nullable();
    });
  }


  public function down() {
    //
  }
}
