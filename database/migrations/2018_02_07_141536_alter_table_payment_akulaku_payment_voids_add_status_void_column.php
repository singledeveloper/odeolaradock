<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentAkulakuPaymentVoidsAddStatusVoidColumn extends Migration {

  public function up() {
    Schema::table('payment_akulaku_payment_voids', function (Blueprint $table) {
      $table->boolean('void_status')->default(false);
    });
  }

  public function down() {
    Schema::table('payment_akulaku_payment_voids', function (Blueprint $table) {
      $table->dropColumn('void_status');
    });
  }
}
