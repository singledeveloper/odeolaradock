<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSalesOcommerceReportsAddTotalProfitColumn extends Migration {

  public function up() {
    Schema::table('sales_ocommerce_reports', function (Blueprint $table) {
      $table->decimal('total_profit', 17, 2)->unsigned()->default(0);
    });
  }


  public function down() {
    Schema::table('sales_ocommerce_reports', function (Blueprint $table) {
      //
    });
  }
}
