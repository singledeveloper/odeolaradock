<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSmsCenterHistoriesAddCenterId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('sms_center_histories', function(Blueprint $table){
        $table->integer('sms_center_id')->nullable();
        $table->foreign('sms_center_id')->references('id')->on('sms_centers');
        $table->index('sms_center_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
