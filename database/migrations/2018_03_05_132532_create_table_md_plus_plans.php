<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMdPlusPlans extends Migration {

  public function up() {
    Schema::create('md_plus_plans', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name', 20);
      $table->string('color', 7);
      $table->tinyInteger('subscription_months');
      $table->decimal('price', 13, 2);
      $table->decimal('discounted_price', 13, 2);
      $table->decimal('referral_bonus', 13, 2);
      $table->boolean('is_active')->default(false);
      $table->softDeletes();
    });
  }


  public function down() {
    //
  }
}
