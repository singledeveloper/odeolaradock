<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaymentOdeoBankTransferDetailsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('payment_odeo_bank_transfer_details', function (Blueprint $table) {
      $table->string('account_number')->nullable()->change();
      $table->string('account_name')->nullable()->change();
      $table->boolean('is_auto_confirm_transfer')->default(false);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }
}
