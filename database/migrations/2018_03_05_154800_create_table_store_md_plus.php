<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStoreMdPlus extends Migration {

  public function up() {
    Schema::create('store_md_plus', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('store_id')->unique();
      $table->foreign('store_id')->references('id')->on('stores');
      $table->index('store_id');
      $table->integer('md_plus_plan_id')->nullable();
      $table->foreign('md_plus_plan_id')->references('id')->on('md_plus_plans');
      $table->index('md_plus_plan_id');
      $table->char('status', 5);
      $table->boolean('is_auto_renew')->default(true);
      $table->timestamps();
      $table->timestamp('expired_at')->nullable();
    });
  }


  public function down() {
    //
  }
}
