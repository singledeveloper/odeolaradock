<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderReconciliationsAddTransactionMonth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::beginTransaction();
      Schema::table('order_reconciliations', function(Blueprint $table){
        $table->integer('trx_month')->nullable();
        $table->index('trx_month');
        $table->integer('trx_year')->nullable();
        $table->index('trx_year');
      });
      
      DB::unprepared('update order_reconciliations set trx_month = date_part(\'month\', transaction_date), trx_year = date_part(\'year\', transaction_date)');
      DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
