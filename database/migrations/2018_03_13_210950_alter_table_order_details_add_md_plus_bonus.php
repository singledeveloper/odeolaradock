<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailsAddMdPlusBonus extends Migration {

  public function up() {
    Schema::table('order_details', function (Blueprint $table) {
      $table->decimal('referral_cashback', 10, 2)->default(0);
    });
  }


  public function down() {
    //
  }
}
