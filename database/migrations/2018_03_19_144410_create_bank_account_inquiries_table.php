<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountInquiriesTable extends Migration {

  public function up() {
    Schema::create('bank_account_inquiries', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->integer('bank_id')
        ->unsigned();

      $table->foreign('bank_id')
        ->references('id')
        ->on('banks');

      $table->string('account_name');
      $table->string('account_number');

      $table->integer('vendor_disbursement_id')
        ->unsigned();

      $table->index(['bank_id', 'account_number']);

      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('bank_account_inquiries');
  }
}
