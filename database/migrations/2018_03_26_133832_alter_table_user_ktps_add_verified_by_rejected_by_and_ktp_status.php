<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserKtpsAddVerifiedByRejectedByAndKtpStatus extends Migration {

  public function up() {
    Schema::table('user_ktps', function (Blueprint $table) {

      $table->bigInteger('verified_by_user_id')->nullable();
      $table->foreign('verified_by_user_id')->references('id')->on('users');

      $table->bigInteger('rejected_by_user_id')->nullable();
      $table->foreign('rejected_by_user_id')->references('id')->on('users');

      $table->string('nik_status', 5)->nullable();
      $table->string('ktp_status', 5)->nullable();
      $table->string('ktp_selfie_status', 5)->nullable();
    });
  }

  public function down() {
    //
  }
}
