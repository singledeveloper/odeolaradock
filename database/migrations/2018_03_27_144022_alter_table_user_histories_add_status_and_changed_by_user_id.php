<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserHistoriesAddStatusAndChangedByUserId extends Migration {

  public function up() {
    Schema::table('user_histories', function (Blueprint $table) {
      $table->string('status', 5)->nullable();

      $table->bigInteger('changed_by_user_id')->nullable();
      $table->foreign('changed_by_user_id')->references('id')->on('users');
    });
  }

  public function down() {
    Schema::table('user_histories', function (Blueprint $table) {
      $table->dropColumn('status');
      $table->dropColumn('changed_by_user_id');
    });
  }
}
