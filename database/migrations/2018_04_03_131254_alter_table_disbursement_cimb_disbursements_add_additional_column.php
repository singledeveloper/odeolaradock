<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDisbursementCimbDisbursementsAddAdditionalColumn extends Migration {

  public function up() {
    Schema::table('disbursement_cimb_disbursements', function (Blueprint $table) {
      $table->string('request_id')->nullable();
      $table->string('transfer_from')->nullable();
    });
  }

  public function down() {
    Schema::table('disbursement_cimb_disbursements', function (Blueprint $table) {
      //
    });
  }
}
