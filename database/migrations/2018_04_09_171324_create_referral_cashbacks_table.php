<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralCashbacksTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('referral_cashbacks', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->date('date');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
      $table->bigInteger('store_id')->unsigned();
      $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
      $table->integer('sales_quantity')->unsigned()->default(0);
      $table->decimal('claimed_amount', 17, 2)->default(0);
      $table->decimal('unclaimed_amount', 17, 2)->default(0);
      $table->bigInteger('service_id');
      $table->foreign('service_id')->references('id')->on('services')->onDelete('set null');
      $table->index('user_id');
      $table->index('store_id');
      $table->index('service_id');
      $table->index('date');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('referral_cashbacks');
  }
}
