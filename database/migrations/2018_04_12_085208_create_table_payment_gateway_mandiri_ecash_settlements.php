<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentGatewayMandiriEcashSettlements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_gateway_mandiri_ecash_settlements', function(Blueprint $table){
          $table->bigIncrements('id');

          $table->date('transaction_date');
          $table->timestamp('transaction_date_time');
          $table->string('name');
          $table->text('description');
          $table->decimal('amount', 17, 2);
          $table->string('transaction_ref_no');
          $table->bigInteger('transfer_id');
          $table->timestamps();

          $table->index('transaction_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
