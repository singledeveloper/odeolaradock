<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaymentGatewayMandiriEcashMutationsAddColumnSettlementId extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('payment_gateway_mandiri_ecash_mutations', function (Blueprint $table) {
      $table->bigInteger('settlement_id')->nullable();
      $table->foreign('settlement_id')->references('id')->on('payment_gateway_mandiri_ecash_settlements');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }
}
