<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaymentGatewayMandiriEcashSettlementsAddIndexTransactionDateTime extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('payment_gateway_mandiri_ecash_settlements', function (Blueprint $table) {
      $table->index('transaction_date_time');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }
}
