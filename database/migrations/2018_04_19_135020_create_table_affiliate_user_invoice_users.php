<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAffiliateUserInvoiceUsers extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('affiliate_user_invoice_users', function(Blueprint $table){
      $table->bigIncrements('id');
      $table->integer('biller_id');
      $table->foreign('biller_id')->references('id')->on('user_invoice_billers');
      $table->index('biller_id');
      $table->string('user_id', 255);
      $table->index('user_id');
      $table->string('name', 50);
      $table->string('email', 50);
      $table->string('telephone', 20);
      $table->text('address');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('affiliate_user_invoice_users');
  }
}
