<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderReconciliationSummaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_reconciliation_summaries', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->date('date');
          $table->string('diff_reason');
          $table->decimal('total_diff', 17, 2)->default(0);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
