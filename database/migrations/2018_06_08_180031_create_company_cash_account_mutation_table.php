<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyCashAccountMutationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('company_cash_account_transactions', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->integer('company_cash_account_id');
        $table->foreign('company_cash_account_id')->references('id')->on('company_cash_accounts')->onDelete('cascade');
        $table->bigInteger('cash_transaction_id');
        $table->foreign('cash_transaction_id')->references('id')->on('cash_transactions')->onDelete('set null');
        $table->bigInteger('user_id')->unsigned()->nullable();
        $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        $table->bigInteger('store_id')->unsigned()->nullable();
        $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
        $table->string('trx_type');
        $table->string('cash_type');
        $table->decimal('credit', 17, 2);
        $table->decimal('debit', 17, 2);
        $table->text('data')->default('');
        $table->decimal('balance_before', 17, 2);
        $table->decimal('balance_after', 17, 2);
        $table->tinyInteger('trx_month');
        $table->smallInteger('trx_year');
        $table->timestamp('transaction_date');
        $table->bigInteger('order_id')->unsigned()->nullable();
        $table->index('order_id');
        $table->string('reference_type')->nullable();
        $table->text('reference')->nullable();
        $table->timestamps();

        $table->index(['company_cash_account_id', 'cash_transaction_id', 'user_id', 'trx_type', 'cash_type', 'data', 'order_id', 'reference_type', 'reference']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
