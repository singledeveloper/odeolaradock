<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTransactionAdditionalInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('company_transaction_additional_informations', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->unsignedBigInteger('company_cash_account_transaction_id');
        $table->foreign('company_cash_account_transaction_id')
          ->references('id')
          ->on('company_cash_account_transactions')
          ->onDelete('set null');
        $table->text('description')->nullable();
        $table->text('pdf_url')->nullable();
        $table->timestamps();

        $table->index(['company_cash_account_transaction_id', 'description']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
