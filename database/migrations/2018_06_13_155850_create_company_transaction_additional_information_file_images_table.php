<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTransactionAdditionalInformationFileImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('company_transaction_additional_information_file_images', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->unsignedBigInteger('company_transaction_additional_information_id')
          ->references('id')
          ->on('company_transaction_additional_informations')
          ->onDelete('cascade');

        $table->text('image_url');
        $table->string('type');

        $table->index(['company_transaction_additional_information_id', 'type']);
        $table->timestamps();
      });
    }

    public function down()
    {
        //
    }
}
