<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableApiDisbursementsAddColumnDelayedAt extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('api_disbursements', function (Blueprint $table) {
      $table->dateTime('delayed_at')->nullable();
      $table->index('delayed_at');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('api_disbursements', function (Blueprint $table) {
      $table->dropIndex('delayed_at');
      $table->dropColumn('delayed_at');
    });
  }
}
