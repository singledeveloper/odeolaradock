<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsersAddColumnMailVerifiedAt extends Migration {

  public function up() {
    Schema::table('users', function (Blueprint $table) {
      $table->timestamp('mail_verified_at')->nullable();
    });
  }


  public function down() {
    Schema::table('users', function (Blueprint $table) {
      //
    });
  }
}
