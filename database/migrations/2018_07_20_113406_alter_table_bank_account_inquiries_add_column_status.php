<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBankAccountInquiriesAddColumnStatus extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    \DB::transaction(function () {
      Schema::table('bank_account_inquiries', function (Blueprint $table) {
        $table->string('status')->nullable();
      });
      \DB::statement("update bank_account_inquiries set status='50000'");
      Schema::table('bank_account_inquiries', function (Blueprint $table) {
        $table->string('status')->nullable(false)->change();
      });
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }
}
