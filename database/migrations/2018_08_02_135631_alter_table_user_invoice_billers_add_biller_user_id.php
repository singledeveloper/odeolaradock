<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserInvoiceBillersAddBillerUserId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      \DB::transaction(function () {
        Schema::table('user_invoice_billers', function (Blueprint $table) {
          $table->bigInteger('biller_user_id')->nullable();
          $table->foreign('biller_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');
        });
        \DB::statement("update user_invoice_billers set biller_user_id = user_id");
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
