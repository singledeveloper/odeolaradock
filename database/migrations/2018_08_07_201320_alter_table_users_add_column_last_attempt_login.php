<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsersAddColumnLastAttemptLogin extends Migration {

  public function up() {
    Schema::table('users', function (Blueprint $table) {
      $table->timestamp('last_attempt_login')->nullable();
    });
  }


  public function down() {
    Schema::table('users', function (Blueprint $table) {
      //
    });
  }
}
