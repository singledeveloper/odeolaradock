<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableVendorSwitcherConnectionTemplatesAddPasswordAliasUserNameAliasTokenAlias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_switcher_templates', function(Blueprint $table){
          $table->renameColumn('user_alias', 'user_id_alias');
          $table->string('user_name_alias', 255)->nullable();
          $table->string('password_alias', 255)->nullable();
          $table->string('token_alias', 255)->nullable();
        });

      Schema::table('vendor_switcher_connections', function(Blueprint $table){
        $table->string('server_user_name', 255)->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
