<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserKtpsAddBannedInformationColumns extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('user_ktps', function (Blueprint $table) {
      $table->integer('last_ocash_balance')->nullable();
      $table->string('last_transaction_description')->nullable();
      $table->string('last_ocash_balance_status', 5)->nullable();
      $table->string('last_transaction_description_status', 5)->nullable();
      $table->string('name_status', 5)->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {

  }
}
