<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBankBriInquiryAddSoftdeletes extends Migration {

  public function up() {
    Schema::table('bank_bri_inquiries', function (Blueprint $table) {
      $table->softDeletes();
    });
  }

  public function down() {
    //
  }
}
