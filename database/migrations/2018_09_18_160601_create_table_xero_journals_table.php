<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableXeroJournalsTable extends Migration
{
    public function up()
    {
      Schema::create('xero_journals', function(Blueprint $table) {
        $table->bigIncrements('id');
        $table->date('date');
        $table->string('status');
        $table->string('line_amount_types');
        $table->timestamp('updated_date_utc');
        $table->string('manual_journal_id');
        $table->text('narration');
        $table->boolean('show_on_cash_basis_reports');
        $table->boolean('has_attachments');
      });
    }

    public function down()
    {
        //
    }
}
