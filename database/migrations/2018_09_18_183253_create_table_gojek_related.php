<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGojekRelated extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gojek_users', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->bigInteger('store_id')->nullable();
          $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
          $table->string('telephone', 50);
          $table->string('login_token', 255);
          $table->string('client_secret', 255);
          $table->string('token', 255)->nullable();
          $table->string('refresh_token', 255)->nullable();
          $table->string('pin', 255)->nullable();
          $table->integer('current_balance')->default(0);
          $table->text('last_error_message')->nullable();
          $table->char('status', 5);
          $table->boolean('is_active')->default(true);
          $table->timestamps();
          $table->index(['store_id']);
          $table->index(['telephone']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
