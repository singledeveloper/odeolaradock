<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGojekQrIds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('gojek_qr_ids', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->string('telephone', 50);
        $table->string('qr_id', 255);
        $table->timestamps();
        $table->index(['telephone']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
