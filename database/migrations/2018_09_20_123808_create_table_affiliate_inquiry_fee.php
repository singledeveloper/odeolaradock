<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAffiliateInquiryFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliate_inquiry_fees', function(Blueprint $table){
          $table->increments('id');
          $table->bigInteger('user_id');
          $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
          $table->integer('pln_inquiry_fee')->nullable();
          $table->integer('product_category_fee')->nullable();
          $table->integer('product_code_fee')->nullable();
          $table->integer('product_price_fee')->nullable();
          $table->integer('product_description_fee')->nullable();
          $table->timestamps();
          $table->index(['user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
