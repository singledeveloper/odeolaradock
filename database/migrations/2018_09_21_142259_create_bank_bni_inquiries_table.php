<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankBniInquiriesTable extends Migration {

  public function up() {
    if (Schema::hasTable('bank_bni_inquiries')) {
      return;
    }

    Schema::create('bank_bni_inquiries', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->timestamp('date');
      $table->string('branch');
      $table->string('journal');
      $table->string('description');
      $table->decimal('balance', 17, 2);
      $table->string('reference_type');
      $table->text('reference');

      $table->integer('bank_scrape_account_number_id')->unsigned();
      $table->foreign('bank_scrape_account_number_id')
        ->references('id')
        ->on('bank_scrape_account_numbers');

      $table->decimal('debit', 17, 2);
      $table->decimal('credit', 17, 2);

      $table->timestamps();
    });
  }

  public function down() {
    Schema::dropIfExists('bank_bni_inquiries');
  }
}
