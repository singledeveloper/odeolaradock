<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCimbInquiryBalanceSummaryTable extends Migration {

  public function up() {
    Schema::create('bank_cimb_inquiry_balance_summaries', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->decimal('balance_change', 17, 2)->default(0);
      $table->decimal('previous_balance', 17, 2)->default(0);
      $table->decimal('balance', 17, 2)->default(0);
      $table->decimal('scrape_ledger_balance', 17, 2)->default(0);
      $table->decimal('scrape_yesterday_balance', 17, 2)->default(0);
      $table->date('scrape_start_date');
      $table->date('scrape_end_date');
      $table->char('status', 5)->nullable();
      $table->unsignedBigInteger('first_distinct_id')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }
}
