<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBankCimbInquiryTable extends Migration {

  public function up() {
    Schema::table('bank_cimb_inquiries', function (Blueprint $table) {
      $table->decimal('credit', 17, 2)->change();
      $table->decimal('debit', 17, 2)->change();
      $table->decimal('balance', 17, 2)->nullable();
      $table->date('post_date')->nullable()->after('date');
      $table->date('eff_date')->nullable()->after('post_date');
      $table->string('cheque_no')->nullable()->after('reference_type');
      $table->string('transaction_code')->nullable()->after('cheque_no');
      $table->unsignedBigInteger('sequence_number')->nullable();
      $table->softDeletes();

      $table->index('date');
      $table->index('balance');
      $table->index('sequence_number');
    });
  }
}
