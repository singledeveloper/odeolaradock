<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableXeroAccountsTable extends Migration
{
    public function up()
    {
      Schema::create('xero_accounts', function(Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('account_id');
        $table->string('code');
        $table->string('name');
        $table->string('status');
        $table->string('type');
        $table->string('tax_type');
        $table->text('description');
        $table->string('class');
        $table->boolean('enable_payments_to_account');
        $table->boolean('show_in_expense_claims');
        $table->string('bank_account_type');
        $table->string('reporting_code');
        $table->string('reporting_code_name');
        $table->boolean('has_attachments');
        $table->timestamp('updated_date_utc');
      });
    }

    public function down()
    {
        //
    }
}
