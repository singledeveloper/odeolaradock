<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserTokens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_tokens', function (Blueprint $table) {
            $table->string('device_ssid', 255)->nullable();
            $table->string('device_name', 255)->nullable();
            $table->timestamp('last_login_time')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->integer('platform_id')->nullable();
        });

        \DB::statement('UPDATE user_tokens SET expired_at = created_at + interval \'30d\'');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
