<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTcashTransferHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tcash_transfer_histories', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->bigInteger('tcash_user_id')->nullable();
          $table->foreign('tcash_user_id')->references('id')->on('tcash_users')->onDelete('cascade');
          $table->bigInteger('transfer_tcash_user_id');
          $table->foreign('transfer_tcash_user_id')->references('id')->on('tcash_users')->onDelete('cascade');
          $table->string('status', 5);
          $table->string('ref', 50)->nullable();
          $table->string('res_message', 255)->nullable();
          $table->timestamp('created_at');

          $table->index(['tcash_user_id']);
          $table->index(['transfer_tcash_user_id']);
          $table->index(['status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
