<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSubUsers extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    if (Schema::hasTable('sub_users')) {
      return;
    }

    Schema::create('sub_users', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('user_id');
      $table->bigInteger('parent_user_id');
      $table->string('name', 50)->nullable();
      $table->string('phone_number', 20)->nullable();
      $table->string('email', 50)->nullable();
      $table->timestamp('created_at');
      $table->timestamp('updated_at');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
//    Schema::dropIfExists('sub_users');
  }
}
