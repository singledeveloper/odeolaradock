<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCashWebhooks extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('cash_webhooks', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id');
      $table->string('type');
      $table->string('url');
      $table->timestamp('created_at');
      $table->timestamp('updated_at');

      $table->index(['user_id', 'type']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
//    Schema::dropIfExists('cash_webhooks');
  }
}
