<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisbursementPermataDisbursementsTable extends Migration
{
  public function up() {
    Schema::create('disbursement_permata_disbursements', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->decimal('amount', 17, 2)->nullable();
      $table->string('to_account_number')->nullable();
      $table->string('to_account_name')->nullable();

      $table->unsignedBigInteger('disbursement_reference_id')->nullable();
      $table->string('disbursement_type')->nullable();

      $table->string('status')->nullable();
      $table->string('status_code')->nullable();
      $table->string('response_timestamp')->nullable();
      $table->string('cust_reff_id')->nullable();
      $table->string('status_desc')->nullable();
      $table->string('trx_reff_no')->nullable();
      $table->dateTime('response_datetime')->nullable();
      $table->text('error_log_response')->nullable();
      $table->text('ex_error_message')->nullable();

      $table->index(['disbursement_reference_id', 'disbursement_type']);

      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('disbursement_permata_disbursements');
  }
}
