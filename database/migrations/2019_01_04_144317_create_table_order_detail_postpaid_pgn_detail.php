<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderDetailPostpaidPgnDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('order_detail_postpaid_pgn_details', function (Blueprint $table) {
        $table->increments('id');
        $table->bigInteger('order_detail_pulsa_switcher_id');
        $table->foreign('order_detail_pulsa_switcher_id')->references('id')->on('order_detail_pulsa_switchers')->onDelete('cascade');
        $table->string('subscriber_id', 50);
        $table->string('subscriber_name', 50);
        $table->string('period', 50);
        $table->integer('base_price');
        $table->integer('admin_fee');
        $table->string('meter_changes', 50)->nullable();
        $table->string('usages', 50)->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
