<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePulsaBulkPurchase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pulsa_bulk_purchases', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->bigInteger('user_id');
          $table->integer('batch_flag');
          $table->string('number', 50);
          $table->string('request_message', 255);
          $table->string('response_message', 255)->nullable();
          $table->bigInteger('order_id')->nullable();
          $table->boolean('is_bulk_processed')->boolean(false);
          $table->timestamps();
          $table->index(['user_id']);
          $table->index(['is_bulk_processed']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
