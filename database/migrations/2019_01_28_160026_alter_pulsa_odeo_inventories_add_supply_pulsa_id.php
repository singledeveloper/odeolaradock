<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPulsaOdeoInventoriesAddSupplyPulsaId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pulsa_odeo_inventories', function(Blueprint $table){
          $table->bigInteger('supply_pulsa_id')->nullable();
          $table->foreign('supply_pulsa_id')->references('id')->on('pulsa_odeos')->onDelete('set null');
          $table->index(['supply_pulsa_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
