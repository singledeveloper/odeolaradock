<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePulsaBulksRemoveIsApprovedIsActive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pulsa_bulks', function(Blueprint $table){
          $table->dropIndex(['is_approved']);
          $table->dropColumn('is_approved');
          $table->dropColumn('is_active');
          $table->char('status', 5)->nullable();
          $table->index(['status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
