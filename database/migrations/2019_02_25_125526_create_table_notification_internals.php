<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotificationInternals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_internals', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->text('message');
          $table->boolean('is_read')->default(false);
          $table->bigInteger('read_by_user_id')->nullable();
          $table->timestamps();
          $table->index(['is_read']);
          $table->index(['read_by_user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
