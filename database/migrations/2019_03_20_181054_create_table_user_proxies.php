<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserProxies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_proxies', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->bigInteger('user_id');
          $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
          $table->string('ip', 20);
          $table->timestamps();
          $table->index(['user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
