<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaxPaymentsAddTid extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('pax_payments', function (Blueprint $table) {
      $table->string('tid', 32)->default('');
      $table->string('actual_card_type', 32)->default('');
      $table->decimal('mdr_pct', 5, 2)->default(0);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}
