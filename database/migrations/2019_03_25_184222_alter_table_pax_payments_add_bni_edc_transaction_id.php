<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaxPaymentsAddBniEdcTransactionId extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('pax_payments', function (Blueprint $table) {
      $table->bigInteger('edc_transaction_reference_id')->nullable(true);
      $table->string('edc_transaction_reference_type')->nullable(true);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}
