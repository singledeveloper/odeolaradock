<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBniEdcTransactionsAddIndexes extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('bni_edc_transactions', function (Blueprint $table) {
      $table->index(['trx_date', 'tid']);
      $table->index('tid');
      $table->index('bni_edc_settlement_id');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}
