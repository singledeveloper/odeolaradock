<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisbursementMandiriDisbursementsTable extends Migration
{
    public function up() {
      Schema::create('disbursement_mandiri_disbursements', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->decimal('amount', 17, 2)->nullable();
        $table->string('disbursement_type')->nullable();
        $table->string('status')->nullable();

        $table->string('to_account_number')->nullable();
        $table->string('to_account_name')->nullable();
        $table->string('cust_reff_no')->nullable();
        $table->string('remark')->nullable();
        $table->string('request_time')->nullable();
        $table->string('transaction_key')->nullable();
        $table->string('response_code')->nullable();
        $table->string('response_message')->nullable();
        $table->string('response_timestamp')->nullable();
        $table->string('response_remmittance_no')->nullable();
        $table->boolean('is_sign_verified')->default(false);

        $table->string('response_error_message')->nullable();
        $table->string('response_error_number')->nullable();
        $table->string('ex_error_message')->nullable();
        $table->string('status_code', 255)->nullable();

        $table->unsignedBigInteger('disbursement_reference_id')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
