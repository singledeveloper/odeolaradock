<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserWithdrawsChangeUserBankAccountIdToBankId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_withdraws', function (Blueprint $table) {
            $table->bigInteger('bank_id')->unsigned()->nullable();
            $table->foreign('bank_id')->references('id')->on('banks')->onDelete('cascade');
        });

        \DB::statement('UPDATE user_withdraws SET bank_id = t.bank_id from ( select user_withdraws.id, user_bank_accounts.bank_id from user_withdraws join user_bank_accounts on user_withdraws.user_bank_account_id = user_bank_accounts.id ) t where t.id = user_withdraws.id');
    
        Schema::table('user_withdraws', function (Blueprint $table) {
            $table->bigInteger('bank_id')->unsigned()->nullable(false)->change();
            $table->dropColumn('user_bank_account_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
