<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankPermataInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('bank_permata_inquiries', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->date('post_date');
        $table->date('eff_date');
        $table->decimal('balance', 17, 2);
        $table->string('trx_code');
        $table->string('cheque_no');
        $table->string('reff_no');
        $table->string('cust_reff_no');
        $table->text('description');
        $table->decimal('debit', 17, 2);
        $table->decimal('credit', 17, 2);

        $table->string('reference')->nullable();
        $table->text('reference_type')->nullable();

        $table->integer('scrape_sequence_no');
        $table->bigInteger('sequence_number', false, true)->nullable();

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
