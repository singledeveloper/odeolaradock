<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDisbursementBcaDisbursementsAddDomesticTransferColumns extends Migration
{
    public function up()
    {
      Schema::table('disbursement_bca_disbursements', function (Blueprint $table) {
        $table->string('bank_code')->nullable();
        $table->string('transfer_to_name')->nullable();
        $table->string('ppu_number')->nullable();
      });
    }

    public function down()
    {
        //
    }
}
