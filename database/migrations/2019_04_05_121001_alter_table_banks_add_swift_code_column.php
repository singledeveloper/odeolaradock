<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBanksAddSwiftCodeColumn extends Migration
{
    public function up()
    {
      Schema::table('banks', function (Blueprint $table) {
        $table->string('swift_code')->nullable();
      });
    }

    public function down()
    {
        //
    }
}
