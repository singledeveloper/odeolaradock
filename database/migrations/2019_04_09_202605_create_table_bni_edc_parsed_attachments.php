<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBniEdcParsedAttachments extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if (Schema::hasTable('bni_edc_parsed_attachments')) {
      return;
    }

    Schema::create('bni_edc_parsed_attachments', function (Blueprint $table) {
      $table->increments('id');
      $table->string('filename');
      $table->string('subject');
      $table->timestamp('date');
      $table->boolean('parsed');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('bni_edc_voidparsed_attachments');
  }
}
