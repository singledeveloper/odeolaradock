<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentGatewayUserPaymentChannels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('payment_gateway_user_payment_channels')) {
            return;
        }

        Schema::create('payment_gateway_user_payment_channels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pg_user_id');
            $table->foreign('pg_user_id')->references('id')->on('payment_gateway_users');
            $table->integer('payment_group_id');
            $table->decimal('fee', 17, 2);
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_gateway_user_payment_channels');
    }
}
