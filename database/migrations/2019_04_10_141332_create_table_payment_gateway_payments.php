<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentGatewayPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('payment_gateway_payments')) {
            return;
        }

        Schema::create('payment_gateway_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pg_user_id');
            $table->foreign('pg_user_id')->references('id')->on('payment_gateway_users');
            $table->integer('payment_group_id')->nullable();
            $table->decimal('fee', 17, 2)->nullable();
            $table->decimal('amount', 17, 2)->nullable();
            $table->string('external_reference_id')->nullable();
            $table->integer('settlement_status')->nullable();
            $table->dateTime('settlement_at')->nullable();
            $table->dateTime('notify_at')->nullable();
            $table->dateTime('last_retry_notify_time')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_gateway_payments');
    }
}
