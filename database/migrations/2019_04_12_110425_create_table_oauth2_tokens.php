<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOauth2Tokens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('oauth2_tokens')) {
            return;
        }

        Schema::create('oauth2_tokens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('client_id');
            $table->text('redirect_uri');
            $table->text('scope');
            $table->text('code');
            $table->dateTime('code_create_at');
            $table->bigInteger('code_expires_in');
            $table->text('access');
            $table->dateTime('access_create_at');
            $table->bigInteger('access_expires_in');
            $table->text('refresh');
            $table->dateTime('refresh_create_at');
            $table->bigInteger('refresh_expires_in');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oauth2_tokens');
    }
}
