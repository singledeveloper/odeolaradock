<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBankPermataInquiriesAddScrapeAccountNumberId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('bank_permata_inquiries', function (Blueprint $table) {
        $table->integer('bank_scrape_account_number_id')->unsigned()->default(6);
        $table->foreign('bank_scrape_account_number_id')
          ->references('id')
          ->on('bank_scrape_account_numbers');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
