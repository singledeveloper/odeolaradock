<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashTransactionPatchLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cash_transaction_patch_logs', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('transaction_id');
        $table->foreign('transaction_id')->references('id')->on('cash_transactions');
        $table->bigInteger('referred_transaction_id');
        $table->foreign('referred_transaction_id')->references('id')->on('cash_transactions');
        $table->bigInteger('patch_by_user_id');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
