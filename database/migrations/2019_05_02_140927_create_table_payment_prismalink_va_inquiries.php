<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentPrismalinkVaInquiries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('payment_prismalink_va_inquiries')) {
            return;
        }

        Schema::create('payment_prismalink_va_inquiries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('va_code')->nullable();
            $table->string('display_text')->nullable();
            $table->string('customer_name')->nullable();
            $table->decimal('amount', 17, 2)->nullable();
            $table->string('trace_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_prismalink_va_inquiries');
    }
}
