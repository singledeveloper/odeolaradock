<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentPrismalinkVaStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('payment_prismalink_va_statuses')) {
            return;
        }

        Schema::create('payment_prismalink_va_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('prismalink_payment_id')->nullable();
            $table->foreign('prismalink_payment_id')->references('id')->on('payment_prismalink_va_payments');
            $table->integer('status_code')->nullable();
            $table->text('request_dump')->nullable();
            $table->text('response_dump')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_prismalink_va_statuses');
    }
}
