<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserWithdrawAddIsReconciled extends Migration
{
    public function up()
    {
      Schema::table('user_withdraws', function(Blueprint $table){
        $table->boolean('is_reconciled')->default(false);
      });
    }

    public function down()
    {
        //
    }
}
