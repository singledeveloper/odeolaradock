<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaymentPrismalinkVaDirectPaymentsAddBankReference extends Migration
{
    public function up()
    {
        Schema::table('payment_prismalink_va_direct_payments', function(Blueprint $table) {
          $table->bigInteger('bank_reference_id')->nullable();
        });
    }

    public function down()
    {
    }
}
