<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRedigApiRequest extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('redig_api_requests', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('command', 255);
      $table->string('reference_type')->nullable();
      $table->string('reference_id')->nullable();
      $table->smallInteger('http_status_code')->nullable();
      $table->text('response')->nullable();
      $table->text('error')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}
