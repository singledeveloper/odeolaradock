<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDisbursementRedigDisbursements extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('disbursement_redig_disbursements', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('status');
      $table->decimal('amount', 17, 2);
      $table->string('description');
      $table->string('account_no');
      $table->string('account_name')->nullable();
      $table->integer('bank_id');
      $table->foreign('bank_id')->references('id')->on('banks');

      $table->string('disbursement_type')->nullable();
      $table->bigInteger('disbursement_reference_id')->nullable();

      $table->string('inquiry_status')->nullable();
      $table->string('inquiry_status_code')->nullable();
      $table->bigInteger('inquiry_request_id')->nullable();
      $table->foreign('inquiry_request_id')->references('id')->on('redig_api_requests');

      $table->string('transfer_status_code')->nullable();
      $table->bigInteger('transfer_request_id')->nullable();
      $table->foreign('transfer_request_id')->references('id')->on('redig_api_requests');

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}
