<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDisbursementApiUsersAddInquiryValidationFee extends Migration
{
  public function up()
  {
    Schema::table('disbursement_api_users', function(Blueprint $table) {
      $table->decimal('inquiry_validation_fee', 17, 2)->nullable();
    });
  }

  public function down()
  {
    Schema::table('disbursement_api_users', function (Blueprint $table) {
      $table->dropColumn('inquiry_validation_fee');
    });
  }
}
