<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDisbursementRedigMutations extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('disbursement_redig_mutations', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->decimal('amount', 17, 2);
      $table->string('type', 60);
      $table->decimal('balance_before', 17, 2);
      $table->decimal('balance_after', 17, 2);
      $table->string('reference_type', 60)->nullable();
      $table->bigInteger('reference_id')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}
