<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PulsaDuplicates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pulsa_duplicates', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->bigInteger('vendor_switcher_reconciliation_id');
          $table->string('number', 50);
          $table->integer('inventory_id');
          $table->bigInteger('order_id');
          $table->bigInteger('new_order_id')->nullable();
          $table->boolean('is_active')->default(true);
          $table->timestamps();
          $table->index(['vendor_switcher_reconciliation_id']);
          $table->index(['number']);
          $table->index(['inventory_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
