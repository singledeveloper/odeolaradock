<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserTokensAddAdditionDevicesColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_tokens', function(Blueprint $table){
            $table->renameColumn('device_name', 'device_model');
            $table->string('device_system_name')->nullable();
            $table->string('device_system_version')->nullable();
            $table->string('login_ip_address')->nullable();
            $table->string('login_country')->nullable();
        });

        Schema::table('user_tokens', function(Blueprint $table){
            $table->string('device_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
