<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBankBriInquiriesAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_bri_inquiries', function (Blueprint $table) {
            $table->string('account_number')->nullable();
            $table->string('currency')->nullable();
            $table->string('scale_position')->nullable();
            $table->decimal('balance_before', 17, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
