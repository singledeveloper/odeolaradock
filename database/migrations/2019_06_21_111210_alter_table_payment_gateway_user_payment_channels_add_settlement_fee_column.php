<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentGatewayUserPaymentChannelsAddSettlementFeeColumn extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('payment_gateway_user_payment_channels', function (Blueprint $table) {
      $table->decimal('settlement_fee', 17, 2)->default(0);
      $table->string('settlement_type')->default('default');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }
}
