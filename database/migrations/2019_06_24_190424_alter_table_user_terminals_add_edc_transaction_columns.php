<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserTerminalsAddEdcTransactionColumns extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('user_terminals', function (Blueprint $table) {
      $table->boolean('enable_edc_transactions')->default('false');
      $table->string('mid')->nullable();
      $table->string('tid')->nullable();
      $table->decimal('credit_mdr', 10, 2)->default(0);
      $table->decimal('debit_mdr', 10, 2)->default(0);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}
