<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEdcTransactions extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('edc_transactions', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('order_id')->nullable();
      $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');
      $table->bigInteger('user_id');
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
      $table->decimal('mdr_pct', 10, 2);
      $table->decimal('mdr', 10, 2);
      $table->decimal('amount', 10, 2);
      $table->date('trx_date');
      $table->string('status');
      $table->string('reference_type')->nullable();
      $table->text('reference_id')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}
