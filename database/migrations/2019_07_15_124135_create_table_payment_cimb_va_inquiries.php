<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentCimbVaInquiries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('payment_cimb_va_inquiries', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('transaction_id')->nullable();
        $table->string('channel_id')->nullable();
        $table->string('terminal_id')->nullable();
        $table->string('transaction_date')->nullable();
        $table->string('company_code')->nullable();
        $table->string('customer_key_1')->nullable();
        $table->string('customer_key_2')->nullable();
        $table->string('customer_key_3')->nullable();
        $table->string('additional_data_1')->nullable();
        $table->string('additional_data_2')->nullable();
        $table->string('additional_data_3')->nullable();
        $table->string('additional_data_4')->nullable();

        $table->string('bill_currency')->nullable();
        $table->string('bill_reference')->nullable();
        $table->string('currency')->nullable();
        $table->decimal('amount', 17, 2)->nullable();
        $table->decimal('fee', 17, 2)->nullable();
        $table->decimal('paid_amount', 17, 2)->nullable();
        $table->string('customer_name')->nullable();
        $table->string('flag_payment')->nullable();
        $table->string('response_code')->nullable();
        $table->string('response_description')->nullable();

        $table->string('virtual_account_number')->nullable();

        $table->timestamps();

        $table->index('transaction_id');
        $table->index('bill_reference');
        $table->index('virtual_account_number');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
