<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentDokuVaInquiries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('payment_doku_va_inquiries')) {
            return;
        }

        Schema::create('payment_doku_va_inquiries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('va_code')->nullable();
            $table->string('display_text')->nullable();
            $table->string('customer_name')->nullable();
            $table->decimal('amount', 17, 2)->nullable();
            $table->string('chain_merchant')->nullable();
            $table->string('session_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_doku_va_inquiries');
    }
}
