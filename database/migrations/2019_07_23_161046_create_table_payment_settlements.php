<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentSettlements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('payment_settlements', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('bank_id');
        $table->foreign('bank_id')->references('id')->on('banks');
        $table->index('bank_id');
        $table->integer('bank_inquiry_id');
        $table->index('bank_inquiry_id');
        $table->decimal('settlement_amount', 17, 2);
        $table->text('settlement_description');
        $table->date('settlement_date');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
