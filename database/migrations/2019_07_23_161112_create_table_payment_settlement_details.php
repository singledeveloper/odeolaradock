<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentSettlementDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('payment_settlement_details', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->integer('payment_settlement_id');
        $table->foreign('payment_settlement_id')->references('id')->on('payment_settlements');
        $table->index('payment_settlement_id');
        $table->integer('source_id');
        $table->string('source_type');
        $table->integer('vendor_id');
        $table->string('vendor_type');
        $table->decimal('amount', 17, 2);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
