<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentPrismalinkVaPaymentsAddIsReconciled extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      \DB::transaction(function () {
        Schema::table('payment_prismalink_va_payments', function (Blueprint $table) {
          $table->boolean('is_reconciled')->default(false);
        });
        \DB::statement('update payment_prismalink_va_payments set is_reconciled = true where bank_reference_id is not null');
        Schema::table('payment_prismalink_va_payments', function (Blueprint $table) {
          $table->dropColumn('bank_reference_id');
        });
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
