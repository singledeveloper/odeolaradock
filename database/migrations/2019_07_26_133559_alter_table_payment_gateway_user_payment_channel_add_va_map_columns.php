<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentGatewayUserPaymentChannelAddVaMapColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      \DB::transaction(function () {
        Schema::table('payment_gateway_user_payment_channels', function(Blueprint $table){
          $table->string('prefix')->unique()->nullable();
          $table->index('prefix');
        });
        \DB::statement('update payment_gateway_user_payment_channels 
          set prefix = payment_gateway_users.virtual_account_prefix
          from payment_gateway_users 
          where payment_gateway_users.id = payment_gateway_user_payment_channels.pg_user_id');
        Schema::table('payment_gateway_users', function(Blueprint $table){
          $table->dropColumn('virtual_account_prefix');
        });
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
