<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentGatewayChannels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_gateway_channels', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->string('channel_name')->nullable();
          $table->bigInteger('va_vendor_id')->nullable();
          $table->foreign('va_vendor_id')->references('id')->on('virtual_account_vendors');
        });

        Schema::table('payment_gateway_user_payment_channels', function(Blueprint $table) {
          $table->bigInteger('payment_gateway_channel_id')->nullable();
          $table->foreign('payment_gateway_channel_id')->references('id')->on('payment_gateway_channels');
          $table->dropColumn('payment_group_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
