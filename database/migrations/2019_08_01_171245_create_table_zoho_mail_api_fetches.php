<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableZohoMailApiFetches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('zoho_mail_api_fetches', function(Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('message_id');
        $table->string('folder_id');
        $table->string('type');
        $table->string('attachment_id')->nullable();
        $table->boolean('is_downloaded')->default(false);
        $table->string('filename')->nullable();
        $table->date('trx_date')->nullable();

        $table->index(['message_id', 'folder_id', 'type', 'attachment_id', 'is_downloaded']);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
