<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAjVaDirectInquiryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('payment_artajasa_va_inquiries', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->string('va_id');
        $table->string('booking_datetime');
        $table->string('reference_number');
        $table->string('username');
        $table->string('booking_id');
        $table->string('customer_name');
        $table->string('display_text');
        $table->string('product_id');
        $table->decimal('min_amount', 17, 2);
        $table->decimal('max_amount', 17, 2);

        $table->index(['va_id', 'reference_number', 'booking_id']);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
