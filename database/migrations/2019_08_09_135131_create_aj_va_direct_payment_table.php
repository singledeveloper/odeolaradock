<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAjVaDirectPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('payment_artajasa_va_payments', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->string('booking_id');
        $table->string('customer_name');
        $table->string('issuer_bank');
        $table->string('issuer_name');
        $table->decimal('amount', 17, 2);
        $table->string('product_id');
        $table->string('trx_id');
        $table->string('trx_date');
        $table->string('username');
        $table->string('notification_datetime');
        $table->integer('status')->nullable();

        $table->bigInteger('inquiry_id')->nullable();
        $table->foreign('inquiry_id')->references('id')->on('payment_artajasa_va_inquiries');
        $table->bigInteger('payment_id')->nullable();
        $table->foreign('payment_id')->references('id')->on('payments');
        $table->index(['booking_id']);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
