<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCompanyCashTransactionRevampAdditionalInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('company_transaction_additional_informations');
        Schema::rename('company_transaction_additional_information_file_images', 'company_transaction_additional_informations');
        Schema::table('company_transaction_additional_informations', function(Blueprint $table){
          $table->renameColumn('company_transaction_additional_information_id', 'cash_transaction_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
