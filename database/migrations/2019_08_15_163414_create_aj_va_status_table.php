<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAjVaStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('payment_artajasa_va_statuses', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('artajasa_payment_id')->nullable();
        $table->foreign('artajasa_payment_id')->references('id')->on('payment_artajasa_va_payments');
        $table->integer('status_code')->nullable();
        $table->text('request_dump')->nullable();
        $table->text('response_dump')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
