<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerminalTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('terminal_transactions', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('mti');
        $table->string('trx_type')->nullable();
        $table->string('f3_processing_code');
        $table->decimal('f4_transaction_amount', 17, 2);
        $table->string('f11_stan');
        $table->string('f24_nii');
        $table->string('f35_card_number')->nullable();
        $table->string('f38_auth_id_response')->nullable();
        $table->string('f39_response_code');
        $table->string('f41_tid');
        $table->string('f42_mid');
        $table->string('f60_batch')->nullable();
        $table->string('f62')->nullable();
        $table->integer('f63a_sale_count');
        $table->decimal('f63b_sale_amount', 17, 2);
        $table->integer('f63c_refund_count');
        $table->decimal('f63d_refund_amount', 17, 2);
        $table->string('request_date');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('terminal_transactions');
    }
}
