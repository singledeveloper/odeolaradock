<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBusinessResellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('business_resellers', function(Blueprint $table){
        $table->dropColumn('fee_type');
        $table->dropColumn('fee_value');
        $table->dropColumn('scope');
        $table->dropColumn('payment_group_id');
        $table->boolean('is_active')->default(true);
      });

      Schema::create('business_reseller_groups', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->integer('business_reseller_id');
        $table->foreign('business_reseller_id')->references('id')->on('business_resellers');
        $table->string('type', 50);
        $table->integer('payment_group_id')->nullable();
        $table->boolean('is_active')->default(true);
        $table->timestamps();
        $table->index(['business_reseller_id']);
        $table->index(['type']);
      });

      Schema::create('business_reseller_group_details', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('business_reseller_group_id');
        $table->foreign('business_reseller_group_id')->references('id')->on('business_reseller_groups');
        $table->string('minimal_type', 50)->nullable();
        $table->bigInteger('minimal_value')->nullable();
        $table->string('fee_type', 50);
        $table->integer('fee_value');
        $table->boolean('is_active')->default(true);
        $table->timestamps();
        $table->index(['business_reseller_group_id']);
      });

      Schema::table('business_reseller_bonuses', function(Blueprint $table){
        $table->dropIndex(['business_reseller_id']);
        $table->dropForeign('business_reseller_bonuses_business_reseller_id_foreign');
        $table->dropColumn('business_reseller_id');
        $table->bigInteger('business_reseller_group_detail_id')->nullable();
        $table->foreign('business_reseller_group_detail_id')->references('id')->on('business_reseller_group_details');
        $table->string('fee_type', 50);
        $table->integer('fee_value');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
