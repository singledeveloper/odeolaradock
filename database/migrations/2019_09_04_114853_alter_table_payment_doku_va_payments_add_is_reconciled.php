<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentDokuVaPaymentsAddIsReconciled extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('payment_doku_va_payments', function (Blueprint $table) {
        $table->boolean('is_reconciled')->default(false);
        $table->decimal('cost', 15, 2)->default(3500);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
