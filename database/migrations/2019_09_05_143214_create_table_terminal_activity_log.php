<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTerminalActivityLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('terminal_activity_logs', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('command');
        $table->string('terminal_id');
        $table->bigInteger('order_id')->nullable();
        $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');
        $table->string('status');
        $table->text('app_response');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('terminal_activity_logs');
    }
}
