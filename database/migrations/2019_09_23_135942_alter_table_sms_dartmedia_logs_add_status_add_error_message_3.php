<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSmsDartmediaLogsAddStatusAddErrorMessage3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('sms_dartmedia_logs', function(Blueprint $table){
        $table->bigInteger('user_id')->nullable();
      });

      Schema::table('affiliate_inquiry_fees', function(Blueprint $table){
        $table->integer('sms_fee')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
