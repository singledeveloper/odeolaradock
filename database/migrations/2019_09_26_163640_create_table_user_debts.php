<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserDebts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_debts', function(Blueprint $table){
          $table->bigInteger('id');
          $table->bigInteger('user_id');
          $table->string('trx_type', 255);
          $table->bigInteger('amount');
          $table->bigInteger('order_id')->nullable();
          $table->string('reference_type', 255)->nullable();
          $table->bigInteger('reference_id')->nullable();
          $table->boolean('is_processed')->default('false');
          $table->timestamps();
          $table->index(['user_id']);
          $table->index(['is_processed']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
