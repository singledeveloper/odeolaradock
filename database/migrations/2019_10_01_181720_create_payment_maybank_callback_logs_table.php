<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMaybankCallbackLogsTable extends Migration
{
    public function up()
    {
      Schema::create('payment_maybank_callback_logs', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->string('type');
        $table->text('body');
        $table->timestamps();
      });
    }

    public function down()
    {
        //
    }
}
