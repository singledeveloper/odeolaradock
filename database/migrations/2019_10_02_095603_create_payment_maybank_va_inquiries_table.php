<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMaybankVaInquiriesTable extends Migration
{
    public function up()
    {
      Schema::create('payment_maybank_va_inquiries', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->string('va_no');
        $table->string('bin_number');
        $table->string('trace_no');
        $table->string('transmission_date_time');
        $table->string('terminal_id');
        $table->string('delivery_channel_type');
        $table->string('display_text');
        $table->decimal('amount', 17, 2);
        $table->string('customer_name');
        $table->string('reference');
        $table->timestamps();

        $table->index(['va_no', 'reference']);
      });
    }

    public function down()
    {
        //
    }
}
