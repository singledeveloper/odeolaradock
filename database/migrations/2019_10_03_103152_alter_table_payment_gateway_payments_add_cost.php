<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentGatewayPaymentsAddCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      \DB::transaction(function () {
        Schema::table('payment_gateway_payments', function (Blueprint $table) {
          $table->decimal('cost', 15, 2)->default(0);
        });
        \DB::statement("update payment_gateway_payments pgp
          set cost = (select cost from payment_prismalink_va_payments ppvp join payments p on ppvp.id = p.vendor_id
          where p.source_id = pgp.id and p.source_type = 'payment_gateway' and p.vendor_type = 'va_prismalink')
          where payment_group_id = 31 and status = '50000'");
        \DB::statement("update payment_gateway_payments pgp
          set cost = (select cost from payment_artajasa_va_payments pavp join payments p on pavp.id = p.vendor_id
          where p.source_id = pgp.id and p.source_type = 'payment_gateway' and p.vendor_type = 'va_artajasa')
          where payment_group_id = 41 and status = '50000'");
        \DB::statement("update payment_gateway_payments pgp
          set cost = (select cost from payment_doku_va_payments pdvp join payments p on pdvp.id = p.vendor_id
          where p.source_id = pgp.id and p.source_type = 'payment_gateway' and p.vendor_type = 'va_doku')
          where payment_group_id = 5 and status = '50000'");
        \DB::statement("update payment_gateway_payments pgp
          set cost = (select cost from payment_permata_va_payments ptvp join payments p on ptvp.id = p.vendor_id
          where p.source_id = pgp.id and p.source_type = 'payment_gateway' and p.vendor_type = 'va_permata')
          where payment_group_id = 43 and status = '50000'");
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
