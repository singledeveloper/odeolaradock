<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBusinessResellerBonusAndCreateTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_reseller_bonuses', function(Blueprint $table){
          $table->dropForeign('business_reseller_bonuses_business_reseller_group_detail_id_for');
        });

        Schema::create('business_reseller_templates', function(Blueprint $table){
          $table->increments('id');
          $table->string('name', 50);
          $table->boolean('is_active')->default(true);
          $table->timestamps();
        });

        Schema::table('business_resellers', function(Blueprint $table){
          $table->integer('template_id')->nullable();
          $table->foreign('template_id')->references('id')->on('business_reseller_templates');
          $table->index(['template_id']);
        });

        Schema::table('business_reseller_groups', function(Blueprint $table){
          $table->integer('template_id')->nullable();
          $table->foreign('template_id')->references('id')->on('business_reseller_templates');
          $table->index(['template_id']);
        });

        Schema::table('business_reseller_group_details', function(Blueprint $table){
          $table->integer('business_reseller_id')->nullable();
          $table->foreign('business_reseller_id')->references('id')->on('business_resellers');
          $table->char('campaign_start_date', 10)->nullable();
          $table->char('campaign_end_date', 10)->nullable();
          $table->index(['business_reseller_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
