<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentGatewayInvoicePaymentLogsTable extends Migration
{
    public function up()
    {
      Schema::create('payment_gateway_invoice_payment_logs', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->string('type');
        $table->string('vendor');
        $table->text('log_request');
        $table->bigInteger('order_id')->nullable();
        $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');

        $table->timestamps();
        $table->index(['type', 'vendor' ]);
      });
    }

    public function down()
    {
        //
    }
}
