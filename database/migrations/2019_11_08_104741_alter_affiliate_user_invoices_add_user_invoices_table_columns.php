<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAffiliateUserInvoicesAddUserInvoicesTableColumns extends Migration
{
    public function up()
    {
      Schema::table('affiliate_user_invoices', function(Blueprint $table){
        $table->unsignedBigInteger('store_id')->nullable();
        $table->integer('category_id')->nullable();
        $table->text('notes')->nullable();

        $table->foreign('store_id')->references('id')->on('stores')->onDelete('SET NULL');
        $table->foreign('category_id')->references('id')->on('invoice_categories')->onDelete('SET NULL');
      });
    }

    public function down()
    {
        //
    }
}
