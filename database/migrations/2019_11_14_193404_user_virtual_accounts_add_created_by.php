<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserVirtualAccountsAddCreatedBy extends Migration
{
    public function up()
    {
      Schema::table('user_virtual_accounts', function(Blueprint $table){
        $table->unsignedBigInteger('created_by')->nullable();

        $table->foreign('created_by')->references('id')->on('users')->onDelete('SET NULL');
      });
    }

    public function down()
    {
        //
    }
}
