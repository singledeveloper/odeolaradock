<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AffiliateUserInvoiceUsersAddCreatedBy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('affiliate_user_invoice_users', function(Blueprint $table){
        $table->unsignedBigInteger('created_by')->nullable();

        $table->foreign('created_by')->references('id')->on('users')->onDelete('SET NULL');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
