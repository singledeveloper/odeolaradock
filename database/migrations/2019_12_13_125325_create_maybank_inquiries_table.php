<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaybankInquiriesTable extends Migration
{
    public function up()
    {
      Schema::create('bank_maybank_inquiries', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->integer('bank_scrape_account_number_id');
        $table->foreign('bank_scrape_account_number_id')
          ->references('id')
          ->on('bank_scrape_account_numbers');
        $table->string('date')->nullable();
        $table->string('transaction_date')->nullable();
        $table->string('transaction_time')->nullable();
        $table->string('posting_date')->nullable();
        $table->string('processing_time')->nullable();
        $table->string('description')->nullable();
        $table->string('transaction_ref')->nullable();
        $table->decimal('debit', 17, 2);
        $table->decimal('credit', 17, 2);
        $table->decimal('end_balance', 17, 2);
        $table->string('source_code')->nullable();
        $table->string('teller_id')->nullable();
        $table->string('transaction_code')->nullable();
        $table->string('reference_type')->nullable();
        $table->string('reference')->nullable();
        $table->timestamps();
      });
    }

    public function down()
    {
        //
    }
}
