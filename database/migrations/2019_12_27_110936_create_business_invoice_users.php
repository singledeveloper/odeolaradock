<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessInvoiceUsers extends Migration
{
    public function up()
    {
      Schema::create('business_invoice_users', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('user_id');
        $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        $table->boolean('charge_to_customer')->default(false);

        $table->timestamps();
      });
    }

    public function down()
    {
        //
    }
}
