<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessInvoiceUserSettings extends Migration
{
    public function up()
    {
      Schema::create('business_invoice_user_settings', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('business_invoice_user_id');
        $table->foreign('business_invoice_user_id')->references('id')->on('business_invoice_users')->onDelete('set null');
        $table->integer('payment_gateway_channel_id');
        $table->foreign('payment_gateway_channel_id')->references('id')->on('payment_gateway_channels')->onDelete('set null');
        $table->string('settlement_type');
        $table->decimal('fee', 17, 2);
        $table->decimal('settlement_fee', 17, 2);
        $table->boolean('active')->default(false);

        $table->timestamps();
      });
    }

    public function down()
    {
        //
    }
}
