<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserOtpAttemptsAddColumnIsSentEmail extends Migration {

  public function up() {
    Schema::table('user_otp_attempts', function (Blueprint $table) {
      $table->boolean('is_sent_email')->default(0);
    });
  }

  public function down() {
    Schema::table('user_otp_attempts', function (Blueprint $table) {
    //   $table->dropColumn("is_sent_email");
    });
  }
}
