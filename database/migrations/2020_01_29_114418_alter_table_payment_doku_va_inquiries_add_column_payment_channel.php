<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentDokuVaInquiriesAddColumnPaymentChannel extends Migration {

  public function up() {
    Schema::table('payment_doku_va_inquiries', function (Blueprint $table) {
      $table->integer('payment_channel')->nullable();
      $table->boolean('has_payment')->default(false);
    });
  }

  public function down() {
    Schema::table('payment_doku_va_inquiries', function (Blueprint $table) {
      $table->dropColumn('payment_channel');
      $table->dropColumn('has_payment');
    });
  }
}
