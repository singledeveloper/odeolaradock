<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentDokuVaInquiriesAddColumnDokuStatus extends Migration {

  public function up() {
    Schema::table('payment_doku_va_inquiries', function (Blueprint $table) {
      $table->string('doku_status', 4)->nullable();
    });
  }

  public function down() {
    Schema::table('payment_doku_va_inquiries', function (Blueprint $table) {
      $table->dropColumn('doku_status');
    });
  }
}
