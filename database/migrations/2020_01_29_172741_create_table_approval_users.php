<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableApprovalUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approval_users', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->bigInteger('maker_user_id');
          $table->bigInteger('approver_user_id');
          $table->string('path', 255);
          $table->timestamps();

          $table->index(['maker_user_id']);
          $table->index(['approver_user_id']);
          $table->index(['path']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
