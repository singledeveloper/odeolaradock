<?php

use Illuminate\Database\Seeder;

class AirlineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tiket_airlines')->insert([
            'code'      => 'QZ',
            'name'      => 'AirAsia',
            'logo_path' => storage_path() . '/airlines_logo/icon_airasia_2.jpg',
        ]);
        DB::table('tiket_airlines')->insert([
            'code'      => 'QG',
            'name'      => 'Citilink',
            'logo_path' => storage_path() . '/airlines_logo/icon_citilink_2.jpg',
        ]);
        DB::table('tiket_airlines')->insert([
            'code'      => 'SJ',
            'name'      => 'Sriwijaya Air',
            'logo_path' => storage_path() . '/airlines_logo/icon_sriwijaya_2.jpg',
        ]);
        DB::table('tiket_airlines')->insert([
            'code'      => 'GA',
            'name'      => 'Garuda Indonesia',
            'logo_path' => storage_path() . '/airlines_logo/icon_garuda_2.jpg',
        ]);
        DB::table('tiket_airlines')->insert([
            'code'      => 'JT',
            'name'      => 'Lion Air',
            'logo_path' => storage_path() . '/airlines_logo/icon_lion_2.jpg',
        ]);
        DB::table('tiket_airlines')->insert([
            'code'      => 'TR',
            'name'      => 'Tigerair',
            'logo_path' => storage_path() . '/airlines_logo/icon_tiger_2.jpg',
        ]);
        DB::table('tiket_airlines')->insert([
            'code'      => 'ID',
            'name'      => 'Batik Air',
            'logo_path' => storage_path() . '/airlines_logo/icon_batik_2.jpg',
        ]);
        DB::table('tiket_airlines')->insert([
            'code'      => 'IW',
            'name'      => 'Wings Air',
            'logo_path' => storage_path() . '/airlines_logo/icon_wings_2.jpg',
        ]);
        DB::table('tiket_airlines')->insert([
            'code'      => 'OD',
            'name'      => 'Malindo Air',
            'logo_path' => storage_path() . '/airlines_logo/icon_malindo_2.jpg',
        ]);
        DB::table('tiket_airlines')->insert([
            'code'      => 'SL',
            'name'      => 'Thai Lion Air',
            'logo_path' => storage_path() . '/airlines_logo/icon_thailion_2.jpg',
        ]);
        DB::table('tiket_airlines')->insert([
            'code'      => 'IN',
            'name'      => 'Nam Air',
            'logo_path' => storage_path() . '/airlines_logo/icon_namair_2.jpg',
        ]);
        DB::table('tiket_airlines')->insert([
            'code'      => 'XN',
            'name'      => 'Xpress Air',
            'logo_path' => storage_path() . '/airlines_logo/icon_xpress_2.jpg',
        ]);
        DB::table('tiket_airlines')->insert([
            'code'      => 'KD',
            'name'      => 'Kalstar',
            'logo_path' => storage_path() . '/airlines_logo/icon_kalstar_2.jpg',
        ]);
        DB::table('tiket_airlines')->insert([
            'code'      => '3K',
            'name'      => 'Jetstar',
            'logo_path' => storage_path() . '/airlines_logo/icon_jetstar_2.jpg',
        ]);
    }
}
