<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

  public function run() {

    if (app()->environment() == 'production') return;

    $this->call(AirlineTableSeeder::class);
    $this->call(BankTableSeeder::class);
    $this->call(FAQTableSeeder::class);
    $this->call(MarketingBudgetSeeder::class);
    $this->call(PaymentOdeoPaymentChannelInformationSeeder::class);
    $this->call(PaymentOdeoPaymentChannelSeeder::class);
    $this->call(PlansTableSeeder::class);
    $this->call(PulsaOperatorsTableSeeder::class);
    $this->call(PulsaPrefixesTableSeeder::class);
    $this->call(ServiceTableSeeder::class);
    $this->call(VendorsTableSeeder::class);
    $this->call(VendorSwitchersTableSeeder::class);
    $this->call(ServiceDetailsTableSeeder::class);
    $this->call(UsersTableSeeder::class);
    $this->call(NotificationsTableSeeder::class);
    $this->call(StoresTableSeeder::class);
    $this->call(UserCashesTableSeeder::class);
    $this->call(UserStoresTableSeeder::class);
    $this->call(NetworkTreeSeeder::class);
    $this->call(BankAccountsTableSeeder::class);
    $this->call(ScrapeInformationTableSeeder::class);
    $this->call(ScrapeAccountTableSeeder::class);
    $this->call(ScrapeAccountNumberSeeder::class);
    $this->call(VendorDisbursementsTableSeeder::class);
    $this->call(ProvincesTableSeeder::class);
    $this->call(CitiesTableSeeder::class);
  }
}
