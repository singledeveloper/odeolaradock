<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MdPlusPlansSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {

    DB::table('md_plus_plans')->insert([
      'name' => '1 month',
      'color' => '#FF3700',
      'subscription_months' => 1,
      'price' => 20000,
      'discounted_price' => 20000,
      'referral_bonus' => 5000
    ]);

    DB::table('md_plus_plans')->insert([
      'name' => '3 months',
      'color' => '#F87D0B',
      'subscription_months' => 3,
      'price' => 60000,
      'discounted_price' => 49000,
      'referral_bonus' => 10000
    ]);

    DB::table('md_plus_plans')->insert([
      'name' => '6 months',
      'color' => '#F5B80E',
      'subscription_months' => 6,
      'price' => 120000,
      'discounted_price' => 79000,
      'referral_bonus' => 20000
    ]);

    DB::table('md_plus_plans')->insert([
      'name' => '12 months',
      'color' => '#7CE020',
      'subscription_months' => 12,
      'price' => 240000,
      'discounted_price' => 149000,
      'referral_bonus' => 25000
    ]);
  }
}
