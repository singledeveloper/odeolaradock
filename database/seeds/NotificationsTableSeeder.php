<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class NotificationsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('notifications')->insert([
            'description' => '[{"type":"plainText","text":"Welcome to"},{"type":"attributedText","color":"#FFFF00","text":"odeo"},{"type":"plainText","text":". Kindly check our FAQ for recent questions. Start your business now!"}]',
            'type' => 'alphabet',
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
