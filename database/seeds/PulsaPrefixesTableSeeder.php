<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PulsaPrefixesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Telkomsel
        DB::table('pulsa_prefixes')->insert(['prefix' => '62811', 'operator_id' => DB::table('pulsa_operators')->where('code', 'TS')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62812', 'operator_id' => DB::table('pulsa_operators')->where('code', 'TS')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62813', 'operator_id' => DB::table('pulsa_operators')->where('code', 'TS')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62821', 'operator_id' => DB::table('pulsa_operators')->where('code', 'TS')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62822', 'operator_id' => DB::table('pulsa_operators')->where('code', 'TS')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62823', 'operator_id' => DB::table('pulsa_operators')->where('code', 'TS')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62851', 'operator_id' => DB::table('pulsa_operators')->where('code', 'TS')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62852', 'operator_id' => DB::table('pulsa_operators')->where('code', 'TS')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62853', 'operator_id' => DB::table('pulsa_operators')->where('code', 'TS')->first()->id]);

        // Indosat
        DB::table('pulsa_prefixes')->insert(['prefix' => '62814', 'operator_id' => DB::table('pulsa_operators')->where('code', 'IS')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62815', 'operator_id' => DB::table('pulsa_operators')->where('code', 'IS')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62816', 'operator_id' => DB::table('pulsa_operators')->where('code', 'IS')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62855', 'operator_id' => DB::table('pulsa_operators')->where('code', 'IS')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62856', 'operator_id' => DB::table('pulsa_operators')->where('code', 'IS')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62857', 'operator_id' => DB::table('pulsa_operators')->where('code', 'IS')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62858', 'operator_id' => DB::table('pulsa_operators')->where('code', 'IS')->first()->id]);

        // XL
        DB::table('pulsa_prefixes')->insert(['prefix' => '62817', 'operator_id' => DB::table('pulsa_operators')->where('code', 'XL')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62818', 'operator_id' => DB::table('pulsa_operators')->where('code', 'XL')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62819', 'operator_id' => DB::table('pulsa_operators')->where('code', 'XL')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62859', 'operator_id' => DB::table('pulsa_operators')->where('code', 'XL')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62877', 'operator_id' => DB::table('pulsa_operators')->where('code', 'XL')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62878', 'operator_id' => DB::table('pulsa_operators')->where('code', 'XL')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62879', 'operator_id' => DB::table('pulsa_operators')->where('code', 'XL')->first()->id]);

        // Axis
        DB::table('pulsa_prefixes')->insert(['prefix' => '62831', 'operator_id' => DB::table('pulsa_operators')->where('code', 'AX')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62832', 'operator_id' => DB::table('pulsa_operators')->where('code', 'AX')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62838', 'operator_id' => DB::table('pulsa_operators')->where('code', 'AX')->first()->id]);

        // Three
        DB::table('pulsa_prefixes')->insert(['prefix' => '62895', 'operator_id' => DB::table('pulsa_operators')->where('code', 'TH')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62896', 'operator_id' => DB::table('pulsa_operators')->where('code', 'TH')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62897', 'operator_id' => DB::table('pulsa_operators')->where('code', 'TH')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62898', 'operator_id' => DB::table('pulsa_operators')->where('code', 'TH')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62899', 'operator_id' => DB::table('pulsa_operators')->where('code', 'TH')->first()->id]);

        // Smartfren
        DB::table('pulsa_prefixes')->insert(['prefix' => '62511', 'operator_id' => DB::table('pulsa_operators')->where('code', 'SF')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62881', 'operator_id' => DB::table('pulsa_operators')->where('code', 'SF')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62882', 'operator_id' => DB::table('pulsa_operators')->where('code', 'SF')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62883', 'operator_id' => DB::table('pulsa_operators')->where('code', 'SF')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62884', 'operator_id' => DB::table('pulsa_operators')->where('code', 'SF')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62885', 'operator_id' => DB::table('pulsa_operators')->where('code', 'SF')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62886', 'operator_id' => DB::table('pulsa_operators')->where('code', 'SF')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62887', 'operator_id' => DB::table('pulsa_operators')->where('code', 'SF')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62888', 'operator_id' => DB::table('pulsa_operators')->where('code', 'SF')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62889', 'operator_id' => DB::table('pulsa_operators')->where('code', 'SF')->first()->id]);

        // Esia
        DB::table('pulsa_prefixes')->insert(['prefix' => '62651', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '6261', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62751', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62761', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62771', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62741', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62711', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62717', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62736', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62721', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '6221', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62254', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '6222', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '6224', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62274', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62231', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62361', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62370', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62380', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62561', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62536', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62511', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62541', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62552', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62431', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62435', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62451', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62426', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62411', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62401', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62911', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62921', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62967', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        DB::table('pulsa_prefixes')->insert(['prefix' => '62986', 'operator_id' => DB::table('pulsa_operators')->where('code', 'ES')->first()->id]);
        
        // BOLT
        DB::table('pulsa_prefixes')->insert(['prefix' => '999', 'operator_id' => DB::table('pulsa_operators')->where('code', 'BO')->first()->id]);
    }
}
