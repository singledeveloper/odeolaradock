<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ScrapeAccountNumberSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $now = Carbon::now();

    DB::table('bank_scrape_account_numbers')
      ->insert([
        [
          'id' => 1,
          'bank_scrape_account_id' => 1,
          'number' => '1650088664421',
          'last_scrapped_date' => date('Y-m-d H:i:s', mktime(0, 0, 0, 7, 1, 2016)),
          'last_time_scrapped' => date('Y-m-d H:i:s', mktime(0, 0, 0, 7, 1, 2016)),
          'active' => true,
          'detail' => null,
          'created_at' => $now,
          'updated_at' => $now,
        ],
        [
          'id' => 2,
          'bank_scrape_account_id' => 2,
          'number' => '0762299966',
          'last_scrapped_date' => Carbon::now()->subDay(31),
          'last_time_scrapped' => Carbon::now()->subDay(31),
          'active' => true,
          'detail' => null,
          'created_at' => $now,
          'updated_at' => $now,
        ]
      ]);

  }
}
