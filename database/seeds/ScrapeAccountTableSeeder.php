<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ScrapeAccountTableSeeder extends Seeder {

  public function run() {
    $now = Carbon::now();

    DB::table('bank_scrape_accounts')->insert([
      [
        'id' => 1,
        'referred_target' => 'mandiri',
        'account' => json_encode([
          'cooperate_id' => 'ODEO001',
          'user_id' => 'maker3',
          'password' => '1geoA329',
        ]),
        'active' => true,
        'created_at' => $now,
        'updated_at' => $now,
      ],
      [
        'id' => 2,
        'referred_target' => 'bca',
        'account' => json_encode([
          'cooperate_id' => 'ibsodeotek',
          'user_id' => 'maker9'
        ]),
        'active' => true,
        'created_at' => $now,
        'updated_at' => $now,
      ]
    ]);

  }
}
