<?php

use Illuminate\Database\Seeder;
use Odeo\Domains\Constant\CashType;

class UserCashesTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {

    DB::table('user_cashes')->insert(
      [
        'user_id' => 1,
        'cash_type' => CashType::OCASH,
        'amount' => 1000000000
      ]
    );
    
    DB::table('user_cashes')->insert(
      [
        'user_id' => 2,
        'cash_type' => CashType::OCASH,
        'amount' => 1000000000
      ]
    );
    
    DB::table('user_cashes')->insert(
      [
        'user_id' => 3,
        'cash_type' => CashType::OCASH,
        'amount' => 1000000000
      ]
    );
    DB::table('user_cashes')->insert(
      [
        'user_id' => 1,
        'store_id' => 1,
        'cash_type' => CashType::ODEPOSIT,
        'amount' => 1000000000
      ]
    );
  }
}
