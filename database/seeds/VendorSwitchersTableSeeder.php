<?php

use Illuminate\Database\Seeder;
use \Odeo\Domains\Constant\SwitcherConfig;

class VendorSwitchersTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    DB::table('vendor_switchers')->insert([
      'id' => SwitcherConfig::DATACELL,
      'vendor_id' => 1,
      'name' => 'Datacell'
    ]);

    DB::table('vendor_switchers')->insert([
      'id' => SwitcherConfig::RAJABILLER,
      'vendor_id' => 1,
      'name' => 'RajaBiller'
    ]);

    DB::table('vendor_switchers')->insert([
      'id' => SwitcherConfig::MKM,
      'vendor_id' => 1,
      'name' => 'MKM'
    ]);

    DB::table('vendor_switchers')->insert([
      'id' => SwitcherConfig::INDOSMART,
      'vendor_id' => 1,
      'name' => 'Indosmart'
    ]);

    DB::table('vendor_switchers')->insert([
      'id' => SwitcherConfig::JATELINDO,
      'vendor_id' => 1,
      'name' => 'Jatelindo'
    ]);

    DB::table('vendor_switchers')->insert([
      'id' => SwitcherConfig::MITRACOMM,
      'vendor_id' => 1,
      'name' => 'Mitracomm'
    ]);

    DB::table('vendor_switchers')->insert([
      'id' => SwitcherConfig::BAKOEL,
      'vendor_id' => 1,
      'name' => 'Bakoel'
    ]);

    DB::table('vendor_switchers')->insert([
      'id' => SwitcherConfig::SERVINDO,
      'vendor_id' => 1,
      'name' => 'Servindo'
    ]);

    DB::table('vendor_switchers')->insert([
      'id' => SwitcherConfig::PPS,
      'vendor_id' => 1,
      'name' => 'PPS Erajaya'
    ]);
  }
}
