<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class VendorsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vendors')->insert([
            'code'          => 'OD',
            'name'          => 'Odeo Internal'
        ]);
        DB::table('vendors')->insert([
            'code'          => 'OP',
            'name'          => 'Odeo'
        ]);
        DB::table('vendors')->insert([
            'code'          => 'SP',
            'name'          => 'Sepulsa'
        ]);
        DB::table('vendors')->insert([
            'code'          => 'TL',
            'name'          => 'Traveloka'
        ]);
        DB::table('vendors')->insert([
            'code'          => 'TK',
            'name'          => 'Tiket.com'
        ]);
    }
}
