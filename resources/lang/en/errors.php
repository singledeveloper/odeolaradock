<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Errors Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used by the paginator library to build
  | the simple pagination links. You are free to change them to anything
  | you want to customize your views to better match your application.
  |
  */

  // user
  'invalid_otp' => 'The OTP is either invalid or expired.',
  'invalid_otp_try_sgain' => 'The otp is either invalid or expired. You have :count more tri(es).',
  'email_not_verified' => 'Email is not verified.',
  'email_taken' => 'Email has taken.',
  'telephone_not_verified' => 'Phone number is not verified.',
  'user_not_verified' => 'User is not verified.',
  'invalid_email' => 'Email is invalid.',
  'invalid_password' => 'Password is invalid. You have :count more tri(es).',
  'invalid_transaction_pin' => 'Transaction PIN is invalid. You have :count more tri(es).',
  'invalid_password_deactivated_temporary' => 'PIN is invalid. Your account will be deactivated for :hours hour(s).',
  'invalid_email_or_password' => 'Invalid email or password.',
  'invalid_old_pin' => 'Current pin is invalid.',
  'account_blocked' => 'Account is blocked. Please contact cs@odeo.co.id.',
  'account_blocked_try_again' => 'Account is blocked. Please try again after :count hour(s)',
  'account_banned' => 'Account is banned. Please contact cs@odeo.co.id.',
  'account_deactivated_temporary' => 'Account is deactivated. Please wait for :minutes minute(s)',
  'account_deactivated' => 'Account is deactivated.',
  'account_reset_pin' => 'Account password expired. Please choose forgot password to reactivate account.',
  'invalid_refresh_token' => 'Refresh token either invalid or expired.',
  'invalid_reset_token' => 'Password reset token either invalid or expired.',
  'invalid_user_id' => 'User id is invalid.',
  'otp_input_blocked_try_again' => 'OTP input is blocked, please try again after :count hour(s)',
  'invalid_authorization' => 'Invalid. Authorization is required',
  'pin_attempt_blocked' => 'PIN has reached limit attempts. Your action is rejected. Please contact cs@odeo.co.id for further information.',
  'brute_force_attempt_otp' => 'User (:telephone) has attempted OTP many times',

  // subscription
  'invalid_plan' => 'Plan is invalid.',
  'subdomain_name_taken' => 'Subdomain name has been taken.',
  'domain_name_taken' => 'Domain name has been taken.',
  'purchase_domain_name_failed' => 'Failed while purchasing domain name.',
  'invalid_user_type' => 'User type is invalid.',
  'user_store_not_found' => 'User store not found.',
  'store_not_verified' => 'Store is not verified.',
  'domain_not_supported' => 'Sorry, we can\'t support this domain for now. Please use another domain.',

  // pulsa
  'invalid_telephone' => 'Phone number is invalid.',
  'invalid_telephone_or_password' => 'Invalid phone number or password.',
  'server_error' => 'Unknown error.',
  'server_query_timeout' => 'Query timeout.',

  'data_not_exists' => 'Requested data does not exists',
  'account_mismatch' => 'Account does not belong to user',
  'database' => 'Database connection error',

  'invalid_request' => 'Invalid request',

  'cart' => [
    'not_found' => 'Cart not found',
    'item_not_found' => 'Item not found',
    'duplicate_item' => 'Item has already existed',
    'invalid_amount' => 'Cart total does not match stored cart'
  ],

  'cash' => [
    'insufficient_cash' => 'Insufficient :cash balance',
    'no_pending_withdraw' => 'User have no pending withdraw request',
    'pending_withdraw_exists' => 'User already have a pending withdraw request',
    'no_pending_topup' => 'User have no pending topup request',
    'pending_topup_exists' => 'User already have a pending topup request',
    'pending_deposit_exists' => 'User already have a pending deposit request',
    'maximum_cash_in_particular_time' => ':subject tidak dapat melakukan topup melebihi :amount per :time'
  ],

  'bank_account' => [
    'account_name_mismatch' => 'The given account name does not match with registered account number (:hint)',
    'bank_not_supported' => 'Bank not supported',
    'invalid_account_number' => 'Invalid account number',
    'failed_to_check_account' => 'Failed to check account number. Please try again later',
  ],

  'payment_gateway' => [
    'not_found' => 'Payment not found',
    'error_update' => 'Update Failed',
    'payment_not_found' => 'Pg Payment not found',
    'invalid_vendor' => 'Invalid Vendor',
    'vendor_payment_not_found' => 'Vendor Payment not found'
  ]

];
