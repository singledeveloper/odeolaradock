<?php

return [
  /*
  'marketing_copy_title_1' => 'One stop for entertainment',
  'marketing_copy_desc_1' => 'Whether you\'re looking for new movies, books, magazines, or Android apps and games—it\'s all available on Google Play.',
  'marketing_copy_title_2' => 'Anytime, anywhere',
  'marketing_copy_desc_2' => 'Access your movies, books, and more on Android devices, iPhones and iPads, Chromecast, and your web browser.',
  'marketing_copy_title_3' => 'Fast, flexible spending',
  'marketing_copy_desc_3' => 'There’s no credit card required to redeem a Google Play gift card, and balances don’t expire. Buy one to celebrate someone special, or treat yourself.',
  */

  'marketing_copy_title_1' => 'Satu tempat untuk segala bentuk hiburan',
  'marketing_copy_desc_1' => 'Film, buku, majalah, atau aplikasi dan game Android terbaru—semua tersedia di Google Play.',
  'marketing_copy_title_2' => 'Kapan saja, dimana saja',
  'marketing_copy_desc_2' => 'Akses film dan lainnya di perangkat Android, iPhone dan iPad, Chromecast, serta browser web Anda.',
  'marketing_copy_title_3' => 'Belanja dengan cepat dan fleksibel',
  'marketing_copy_desc_3' => 'Tak perlu kartu kredit untuk menukarkan kartu hadiah Google Play, dan masa berlaku saldo Anda tidak ada batasnya. Beli untuk orang terdekat atau hadiahkan kepada diri Anda sendiri.',

  'tnc' => 'Hanya berlaku untuk pengguna berusia 13 tahun ke atas yang merupakan penduduk Indonesia dengan akun Google Payments untuk Indonesia. ' .
    'Hanya berlaku untuk pembelian di Google Play. Terdapat perangkat dan langganan tertentu yang tidak termasuk dalam pembelian, silahkan lihat persyaratan lengkap untuk informasi lebih lanjut mengenai hal ini. ' .
    'Tidak dikenakan biaya, dengan tunduk pada persyaratan lengkap. Tidak ada pengembalian dana, kecuali diwajibkan oleh undang-undang; berapa pun jumlah dana yang dikembalikan akan ditambahkan ke saldo Google Play untuk kartu hadiah agar dapat digunakan di masa mendatang. ' .
    'Tidak dapat ditukarkan dengan uang tunai atau kartu lain; tidak dapat berlaku untuk rekening kredit; tidak dapat diisi ulang; tidak dapat digabungkan dengan saldo non-Google Play, dijual kembali, ditukarkan, atau ditransfer nilainya. ' .
    'Pengguna bertanggung jawab atas hilangnya kartu. Untuk bantuan atau untuk melihat saldo silahkan kunjungi support.google.com/googleplay/go/cardhelp. ' .
    '© ' . date('Y') . ' Google Payment Corp. Seluruh hak-hak tidak dikesampingkan.',

  'redeem_instruction_1' => 'Untuk menukar di web:',
  'redeem_instruction_1_list_1' => 'Buka situ play.google.com/redeem',
  'redeem_instruction_1_list_2' => 'Masukkan kode Google Play',
  'redeem_instruction_1_list_3' => 'Mulailah berbelanja',
  'redeem_instruction_2' => 'Untuk menukar di tablet/ponsel Android:',
  'redeem_instruction_2_list_1' => 'Buka aplikasi Google Play Store di tablet atau ponsel Android Anda',
  'redeem_instruction_2_list_2' => 'Pilih Tukarkan',
  'redeem_instruction_2_list_3' => 'Masukkan kode Google Play',
  'redeem_instruction_2_list_4' => 'Mulailah berbelanja'

];
