<?php
return [
    'user_id_exists' => 'Billed User Id already used',
    'billed_user_not_found' => 'Billed User not found',
    'template_outdated' => 'Your template is outdated. Please download and use the latest template (:version)',
    'template_input_invalid' => 'Invalid input format, please check your file input.',
    'invoice_user_exists' => 'Invoice User already exists',
    'invoice_user_not_found' => 'Invoice User not found',
];