<?php

return [
  'tnc_bank_transfer_bca_1' => 'If you transfer between 20:30 to 05:00, our system can\'t detect your payment because BCA is offline. Your payment will be processed later when BCA is online and give a report that your transfer is correct. We can\'t be responsible if there\'s transfer in those time.',
  'tnc_bank_transfer_mandiri_1' => 'If you transfer between 22:30 to 03:00, our system can\'t detect your payment because Mandiri is offline. Your payment will be processed later when Mandiri is online and give a report that your transfer is correct. We can\'t be responsible if there\'s transfer in those time.',
  'tnc_bank_transfer_bri_1' => 'If you transfer between 22:30 to 06:00, our system can\'t detect your payment because BRI is offline. Your payment will be processed later when BRI is online and give a report that your transfer is correct. We can\'t be responsible if there\'s transfer in those time.',
  'tnc_bank_transfer_cimb_1' => 'If you transfer between 23:30 to 02:30, our system can\'t detect your payment because CIMB Niaga is offline. Your payment will be processed later when CIMB Niaga is online and give a report that your transfer is correct. We can\'t be responsible if there\'s transfer in those time.',

  'tnc_bank_transfer_1' => 'You need to pay with the unique code. This will make the payment automatically verified in 10 minutes up to 1 hour.',
  'tnc_bank_transfer_2' => 'If you don\'t use unique code, please confirm your payment as soon as possible with your amount of transfer and bank account name that do the transfer. Payment will be verified within 2x24 hours.',
];