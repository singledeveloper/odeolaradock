<?php


return [
  'tnc_kredivo_1' => 'Pengisian oCash menggunakan Kredivo, dana tidak dapat dicairkan sesuai nominal yang diisi. Hal ini berdasarkan peraturan Bank Indonesia (PBI) No. 11/11/PBI/2009 dan PBI No 14/2/2012.',

  'tnc_withdraw_kredivo_1' => 'Sesuai dengan ketentuan yang terlampir pada saat pengisian oCash menggunakan Kredivo, dana tidak dapat dicairkan sesuai nominal yang diisi.',
  'tnc_withdraw_kredivo_2' => 'Berdasarkan Peraturan Bank Indonesia (PBI) No. 11/11/PBI/2009 dan PBI No 14/2/2012, transaksi gesek tunai merupakan transaksi yang dilarang. Oleh sebab itu Anda dapat mengembalikan dana oCash yang ditahan dan membatalkan transaksi Kredivo dengan menekan tombol batalkan pembayaran Kredivo.',
  'tnc_withdraw_kredivo_3' => 'Apabila Anda tidak ingin membatalkan pengisian oCash melalui Kredivo, Anda tetap dapat menggunakan saldo oCash yang tertahan untuk melakukan pembelian produk di ODEO.'
];
