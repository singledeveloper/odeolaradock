<?php
return [
  'name' => 'Payment Gateway',
  'data_not_found' => 'Data not found',
  'settlement_time' => 'Settlement time is 1x24 hours for Weekday, 2x24 hours for Sunday and 3x24 hours for Saturday and Friday after 9PM Jakarta Time',

  'channel_alfa' => 'Alfa Group (Alfamart, Alfamidi, Alfa Express, Lawson, & DAN+DAN)',
  'channel_bca_closed' => 'BCA Virtual Account (Closed Amount)',
  'channel_bca_open' => 'BCA Virtual Account (Open Amount)',
  'channel_atm_bersama' => 'ATM Bersama Virtual Account (Closed Amount)',
  'channel_bri' => 'BRI Virtual Account (Closed Amount)',
  'channel_cimb' => 'CIMB Virtual Account (Closed Amount)',
  'channel_permata' => 'Permata Virtual Account (Closed Amount)',
  'channel_mandiri' => 'Mandiri Virtual Account (Closed Amount)',
  'channel_maybank' => 'Maybank Virtual Account (Closed Amount)'
];