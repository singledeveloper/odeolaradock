<?php


return [
  'withdraw' => 'Withdraw',
  'transfer' => 'Transfer',
  'tnc_withdraw_1' => ':process fee to all banks is Rp' . \Odeo\Domains\Constant\Withdraw::REAL_TIME_FEE . '. This is for using realtime transfer online system for better and faster process.',
];
