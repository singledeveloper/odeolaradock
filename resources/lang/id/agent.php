<?php


return [
  'description_for_agent_when_available' => 'Masukkan kode master dan dapatkan harga khusus agen dari master Anda.',
  'description_for_agent_when_pending' => 'Mohon menunggu maksimal 2x24 jam untuk persetujuan master. Anda bisa menghubungi informasi master yang tercantum untuk mempercepat proses verifikasi.',
  'description_for_agent_when_accepted' => '',
  'description_unlock_agent_feature' => 'Unggah KTP Anda untuk membuka fitur ini. Jika Anda telah mengunggah KTP, silahkan tunggu 2x24 jam hingga admin selesai memverifikasi data Anda.',
  'description_agent_feature' => 'Ajak, bagikan, dan tingkatkan pendapatan Anda',
];
