<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'telkomsel_otp_register_fail' => "Sedang ada masalah dengan nomor Telkomsel. Silakan gunakan nomor dari provider lain atau kontak cs@odeo.co.id untuk membantu Anda lebih lanjut.",
    'telkomsel_otp_reset_password_fail' => "Sedang ada masalah dengan nomor Telkomsel. Silakan kontak cs@odeo.co.id untuk membantu Anda lebih lanjut."
];
