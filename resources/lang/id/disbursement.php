<?php

return [
  'name' => 'Pencairan',
  'inquiry_name' => 'Verifikasi Akun Bank',
  'not_ready' => 'Batch Disbursement masih belum siap, pastikan semua validasi berhasil terlebih dahulu',
  'template_outdated' => 'Template anda bukanlah yang terbaru. Silakan unduh dan gunakan template yang paling baru (:version)',
  'cant_delete_the_only_disbursement' => 'Anda tidak dapat menghapus baris terakhir ini'
];