<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Errors Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used by the paginator library to build
  | the simple pagination links. You are free to change them to anything
  | you want to customize your views to better match your application.
  |
  */

  'invalid_otp' => 'OTP tidak valid atau kadaluarsa.',
  'email_not_verified' => 'Email belum terverifikasi.',
  'email_taken' => 'Email sudah digunakan oleh user lain.',
  'telephone_not_verified' => 'Nomor belum terverifikasi.',
  'user_not_verified' => 'User belum terverifikasi.',
  'invalid_email' => 'Email tidak valid.',
  'invalid_password' => 'Password tidak valid. Anda masih mempunyai :count kali kesempatan.',
  'invalid_transaction_pin' => 'PIN Transaksi tidak valid. Anda masih mempunyai :count kali kesempatan.',
  'invalid_password_deactivated_temporary' => 'PIN tidak valid. Akun Anda terkunci selama :hours jam.',
  'invalid_email_or_password' => 'Email atau password tidak valid.',
  'invalid_old_password' => 'PIN lama tidak valid.',
  'account_blocked' => 'Akun terblokir. Mohon kontak cs@odeo.co.id.',
  'account_blocked_try_again' => 'Akun terblokir. Akun anda terkunci selama :count jam',
  'account_banned' => 'Akun terblokir. Mohon kontak cs@odeo.co.id.',
  'account_deactivated_temporary' => 'Akun Anda sedang terkunci. Mohon tunggu :minutes menit lagi.',
  'account_deactivated' => 'Akun Anda terkunci. Mohon kontak cs@odeo.co.id.',
  'account_reset_pin' => 'Password akun Anda kadaluarsa. Silahkan pilih lupa password untuk aktivasi kembali.',
  'invalid_refresh_token' => 'Token refresh tidak valid atau kadaluarsa.',
  'invalid_reset_token' => 'Token reset password tidak valid atau kadaluarsa.',
  'invalid_user_id' => 'User ID tidak valid.',
  'invalid_authorization' => 'Tidak valid. Authorisasi dibutuhkan.',
  'pin_attempt_blocked' => 'PIN melebihi batas input. Aksi anda ditolak. Hubungi cs@odeo.co.id untuk keterangan lebih lanjut.',
  'brute_force_attempt_otp' => 'User (:telephone) has attempted OTP many times',

  // subscription
  'invalid_plan' => 'Plan is invalid.',
  'subdomain_name_taken' => 'Subdomain name is taken.',
  'domain_name_taken' => 'Domain name is taken.',
  'purchase_domain_name_failed' => 'Failed while purchasing domain name.',
  'invalid_user_type' => 'User type is invalid.',
  'user_store_not_found' => 'User store is not found.',
  'store_not_verified' => 'Store is not verified.',
  'domain_not_supported' => 'Sorry, we can\'t support this domain for now. Please use another domain.',

  // pulsa
  'invalid_telephone' => 'Phone number is invalid.',
  'invalid_telephone_or_password' => 'Invalid telephone or password.',
  'server_error' => 'Unknown error.',
  'server_query_timeout' => 'Query timeout.',

  'invalid_request' => 'Permintaan tidak valid',

  'cash' => [
    'insufficient_cash' => 'Saldo Anda tidak mencukupi',
    'pending_topup_exists' => 'Anda sudah pernah melakukan topup/tiket.',
    'maximum_cash_in_particular_time' => ':subject tidak dapat melakukan topup melebihi :amount per :time'
  ],

  'bank_account' => [
    'account_name_mismatch' => 'Nama yang diberikan tidak sesuai dengan nama rekening yang terdaftar (:hint)',
    'bank_not_supported' => 'Pilihan Bank tidak valid',
    'invalid_account_number' => 'Nomor rekening salah',
    'failed_to_check_account' => 'Pengecekan nomor rekening gagal. Silahkan dicoba kembali',
  ],

  'payment_gateway' => [
    'not_found' => 'Payment tidak ditemukan',
    'error_update' => 'Update gagal',
    'payment_not_found' => 'Pg Payment tidak ditemukan',
    'invalid_vendor' => 'Vendor salah',
    'vendor_payment_not_found' => 'Vendor Payment tidakditemukan'
  ]
];
