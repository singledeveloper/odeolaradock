<?php

return [
  'new_year' => 'Tahun Baru Masehi :year',
  'chinese_new_year' => 'Imlek :year',
  'isra_miraj' => 'Isra\' Mi\'raj',
  'nyepi' => 'Nyepi :year',
  'good_friday' => 'Wafat Isa Almasih',
  'international_labor' => 'Hari Buruh Internasional',
  'vesak' => 'Waisak :year',
  'ascension_day' => 'Kenaikan Isa Almasih',
  'eid_al_fitr' => 'Idul Fitri :year H',
  'pancasila_birthday' => 'Hari Lahir Pancasila',
  'eid_al_adha' => 'Idul Adha :year H',
  'independence_day' => 'Hari Kemerdekaan Indonesia',
  'islamic_new_year' => 'Tahun Baru Islam :year H',
  'maulid' => 'Maulid Nabi Muhammad SAW',
  'christmas' => 'Natal'
];