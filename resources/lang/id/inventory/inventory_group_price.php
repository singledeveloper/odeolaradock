<?php

return [

  'group_price_type_agent_default' => 'Agent (default)',
  'group_price_description_agent_default' => 'Harga setelan standar agen',

  'group_price_type_market_place' => 'Market Place',
  'group_price_description_market_place' => 'Harga jual pada pusat belanja odeo',

  'group_price_type_self' => 'Self Purchase',
  'group_price_description_self' => 'Harga pembelian ke toko sendiri',

];
