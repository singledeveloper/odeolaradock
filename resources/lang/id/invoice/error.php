<?php
return [
    'user_id_exists' => 'Billed User Id sudah ada',
    'billed_user_not_found' => 'Billed User tidak ditemukan',
    'template_outdated' => 'Template anda sudah tidak berlaku. Silahkan unduh dan gunakan template terbaru (:version).',
    'template_input_invalid' => 'Format input anda salah, silahkan cek input anda',
    'invoice_user_exists' => 'Invoice User sudah ada',
    'invoice_user_not_found' => 'Invoice User tidak ditemukan',
];