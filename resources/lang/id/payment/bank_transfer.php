<?php



return [
  'tnc_bank_transfer_bca_1' => 'Apabila anda melakukan pembayaran pada waktu 20:30 - 05:00, sistem kami tidak dapat melakukan mendeteksi pembayaran anda dikarenakan bank BCA sedang offline. Pembayaran anda akan diproses apabila bank BCA kembali online dan memberikan laporan bahwa transfer anda sudah berhasil. Kami tidak dapat bertanggung jawab jika ada transfer di jam tersebut.',
  'tnc_bank_transfer_mandiri_1' => 'Apabila anda melakukan pembayaran pada waktu 22:30 - 03:00, sistem kami tidak dapat melakukan mendeteksi pembayaran anda dikarenakan bank Mandiri sedang offline. Pembayaran anda akan diproses apabila bank Mandiri kembali online dan memberikan laporan bahwa transfer anda sudah berhasil. Kami tidak dapat bertanggung jawab jika ada transfer di jam tersebut.',
  'tnc_bank_transfer_bri_1' => 'Apabila anda melakukan pembayaran pada waktu 00:00 - 06:00, sistem kami tidak dapat mendeteksi pembayaran anda dikarenakan bank BRI sedang offline. Pembayaran anda akan diproses apabila bank BRI kembali online dan memberikan laporan bahwa transfer anda sudah berhasil. Kami tidak dapat bertanggung jawab jika ada transfer di jam tersebut.',
  'tnc_bank_transfer_cimb_1' => 'Apabila anda melakukan pembayaran pada waktu 23:30 - 02:30, sistem kami tidak dapat mendeteksi pembayaran anda dikarenakan bank CIMB Niaga sedang offline. Pembayaran anda akan diproses apabila bank CIMB Niaga kembali online dan memberikan laporan bahwa transfer anda sudah berhasil. Kami tidak dapat bertanggung jawab jika ada transfer di jam tersebut.',

  'tnc_bank_transfer_1' => 'Harap selalu membayar beserta kode unik. Pembayaran yang menyertakan kode unik akan diverifikasi dalam waktu normal 10 menit - 1 jam.',
  'tnc_bank_transfer_2' => 'Apabila anda membayar tanpa kode unik, harap melakukan konfirmasi pembayaran dengan nominal dan nama yang sesuai dengan yang di transfer. Pembayaran tanpa kode unik akan di diverifikasi dalam waktu 2x24 jam.',

];
