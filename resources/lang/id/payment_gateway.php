<?php
return [
  'name' => 'Gerbang Pembayaran',
  'data_not_found' => 'Data tidak ditemukan',
  'settlement_time' => 'Waktu settlement adalah 1x24 jam untuk hari biasa, 2x24 jam untuk hari Minggu dan 3x24 jam untuk hari Sabtu dan Jumat setelah pukul 21.00 WIB',

  'channel_alfa' => 'Alfa Group (Alfamart, Alfamidi, Alfa Express, Lawson, & DAN+DAN)',
  'channel_bca_closed' => 'BCA Virtual Account (Closed Amount)',
  'channel_bca_open' => 'BCA Virtual Account (Open Amount)',
  'channel_atm_bersama' => 'ATM Bersama Virtual Account (Closed Amount)',
  'channel_bri' => 'BRI Virtual Account (Closed Amount)',
  'channel_cimb' => 'CIMB Virtual Account (Closed Amount)',
  'channel_permata' => 'Permata Virtual Account (Closed Amount)',
  'channel_mandiri' => 'Mandiri Virtual Account (Closed Amount)',
  'channel_maybank' => 'Maybank Virtual Account (Closed Amount)'
];