<?php

return array_merge([
  'plan_name_1' => 'lite',
  'plan_name_2' => 'Starter',
  'plan_name_3' => 'Business',
  'plan_name_4' => 'Pro',
  'plan_name_5' => 'Enterprise',
  'plan_name_99' => 'Free',

  'description_of_id_1' => 'Semua yang Anda butuhkan untuk memulai toko online dan menjual pulsa.',
  'description_of_id_2' => 'Jual pulsa lebih banyak setiap hari dan dapatkan uang lebih banyak.',
  'description_of_id_3' => 'Mulai menjual tiket pesawat dan perluas toko Anda. Sekarang Anda tidak dapat dihentikan.',
  'description_of_id_4' => 'Mulai menjual tiket hotel seperti layaknya professional dan perluas toko Anda. Sekarang Anda tidak dapat dihentikan.',
  'description_of_id_5' => 'Jadi yang paling atas dan jual semua hal yang Anda inginkan. Jadilah Raja.',
  'description_of_id_99' => 'Semua yang Anda butuhkan untuk memulai toko online.',

  'sub_name_of_id_1' => 'Individual',
  'sub_name_of_id_2' => 'Paket Pemula',
  'sub_name_of_id_3' => 'Terpopuler',
  'sub_name_of_id_4' => 'Rekomendasi odeo',
  'sub_name_of_id_5' => 'Paling Untung',
  'sub_name_of_id_99' => 'Paket Gratis',

  'pulsa_inventory' => 'Jual Pulsa',
  'paket_data_inventory' => 'Jual Paket Data',
  'bolt_inventory' => 'Jual Pulsa Bolt',
  'pln_inventory' => 'Jual Token PLN',
  'pln_postpaid_inventory' => 'Bayar PLN Pascabayar',
  'bpjs_kes_inventory' => 'Bayar BPJS Kesehatan',
  'bpjs_tk_inventory' => 'Bayar BPJS Tenagakerjaan',
  'pulsa_postpaid_inventory' => 'Bayar Pulsa Pascabayar',
  'broadband_inventory' => 'Bayar Tagihan Internet',
  'landline_inventory' => 'Bayar Tagihan Telepon Rumah',
  'pdam_inventory' => 'Bayar Tagihan PDAM',
  'game_voucher_inventory' => 'Jual Voucher Game',
  'google_play_inventory' => 'Jual Voucher Google Play',
  'transportation_inventory' => 'Isi Ulang Saldo Go-Pay / GrabPay / e-Toll',

  'bo_title' => 'Peluang Bisnis',
  'bo_of_id_1' => [
    "Mentoring Bonus" => "Hingga Rp78,000/hustler",
    /*"Team Commission" => "$0",
    "Residual Team Commission" => "$0",
    "Team Earning Potential" => "$0"*/
  ],
  'bo_of_id_2' => [
    "Mentoring Bonus" => "Hingga Rp390,000/hustler",
    /*"Team Commission" => "Hingga 1x$25/hari",
    "Residual Team Commission" => "$5",
    "Team Earning Potential" => "Hingga $50/hari"*/
  ],
  'bo_of_id_3' => [
    "Mentoring Bonus" => "Hingga Rp1,170,000/hustler",
    /*"Team Commission" => "Hingga 4x$25/hari",
    "Residual Team Commission" => "$5",
    "Team Earning Potential" => "Hingga $200/hari"*/
  ],
  'bo_of_id_4' => [
    "Mentoring Bonus" => "Hingga Rp2,340,000/hustler",
    /*"Team Commission" => "Hingga 8x$25/hari",
    "Residual Team Commission" => "$5",
    "Team Earning Potential" => "Hingga $500/hari"*/
  ],
  'bo_of_id_5' => [
    "Mentoring Bonus" => "Hingga Rp3,900,000/hustler",
    /*"Team Commission" => "Hingga 12x$25/hari",
    "Residual Team Commission" => "$5",
    "Team Earning Potential" => "Hingga $1,000/hari"*/
  ],
  'bo_of_id_99' => [
    "Mentoring Bonus" => "Not available",
    /*"Team Commission" => "Hingga 12x$25/hari",
    "Residual Team Commission" => "$5",
    "Team Earning Potential" => "Hingga $1,000/hari"*/
  ],


  'tnc_plan_1' => 'Harga di atas belum termasuk deposit.',
  'tnc_plan_2' => 'Anda akan mendapatkan deposit sebesar 200 ribu hanya setelah melakukan pembayaran toko dan memasukkan kode referensi dari toko lain.',
  'tnc_plan_3' => 'Segala bentuk pembayaran toko tidak dapat dikembalikan dalam bentuk apapun.',

  'tnc_upgrade_plan_1' => 'Harga yang tertera telah dikurangi harga prorata berdasarkan jumlah hari tersisa dari paket yang sedang aktif.',
  'tnc_upgrade_plan_2' => 'Masa aktif toko akan diperpanjang 6 bulan, dimulai dari tanggal peningkatan paket.',
  'tnc_upgrade_plan_3' => 'Pembayaran untuk peningkatan paket tidak dapat dikembalikan dalam bentuk apapun.'

], array_fill_keys([
  'advance_features_id_2',
  'advance_features_id_3',
  'advance_features_id_4',
  'advance_features_id_5'
], [
  'Market Place',
  'Barang tersedia dari ODEO'
]));

