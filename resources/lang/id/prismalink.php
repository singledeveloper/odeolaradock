<?php

return [
  'invalid_code' => 'Nomor kode salah',
  'invalid_partner_id' => 'Partner Id tidak valid',
  'signature_not_match' => 'Signature Tidak Cocok',
  'request_cannot_be_processed' => 'Permintaan tidak dapat diproses'
];
