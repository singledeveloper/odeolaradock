<?php
use Odeo\Domains\Constant\Recurring;

return [
  Recurring::TYPE_ONCE => 'Sekali',
  Recurring::TYPE_DAY => 'Setiap Hari',
  Recurring::TYPE_DAY_WORK => 'Setiap Hari (Senin - Jumat)',
  Recurring::TYPE_DAY_MON_SAT => 'Setiap Hari (Senin - Sabtu)',
  Recurring::TYPE_WEEK . '_1' => 'Setiap Senin',
  Recurring::TYPE_WEEK . '_2' => 'Setiap Selasa',
  Recurring::TYPE_WEEK . '_3' => 'Setiap Rabu',
  Recurring::TYPE_WEEK . '_4' => 'Setiap Kamis',
  Recurring::TYPE_WEEK . '_5' => 'Setiap Jumat',
  Recurring::TYPE_WEEK . '_6' => 'Setiap Sabtu',
  Recurring::TYPE_WEEK . '_7' => 'Setiap Minggu',
  Recurring::TYPE_MONTH => 'Setiap Bulan',
  'date' => 'Tanggal',
  'time_format' => '\j\a\m H:00 \W\I\B'
];
