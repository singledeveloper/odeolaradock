<?php


return [
  'withdraw' => 'pencairan',
  'transfer' => 'transfer',
  'tnc_withdraw_1' => 'Biaya :process dana ke semua Bank adalah Rp' . \Odeo\Domains\Constant\Withdraw::REAL_TIME_FEE . '. Sehubung dengan penggunaan sistem online realtime transfer untuk mempercepat proses transfer dana.',
];
