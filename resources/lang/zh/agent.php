<?php


return [
  'description_for_agent_when_available' => 'Input master code and get a special agent price from your master.',
  'description_for_agent_when_pending' => 'Please wait up to 2x24 hours for master approval. You can contact the master using the provided information for faster approval.',
  'description_for_agent_when_accepted' => '',
  'description_unlock_agent_feature' => 'Upload your KTP to unlock this feature. If your KTP is uploaded but you see this, please wait for admin verification in 2x24 hours.',
  'description_agent_feature' => 'Share, invite, and boost up your revenue',
];
