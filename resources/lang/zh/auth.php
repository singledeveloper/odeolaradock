<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '这些凭据与我们的记录不匹配',
    'throttle' => '登录尝试过多。 请在:seconds秒后重试',
    'telkomsel_otp_register_fail' => "Telkomsel的电话号码正存在问题。 请尝试使用其他提供商的号吗或联系cs@odeo.co.id以进一步为您提供服务。",
    'telkomsel_otp_reset_password_fail' => "Telkomsel的电话号码正存在问题。 请联系cs@odeo.co.id以提供重置密码的提示。"

];
