<?php

return [
  'name' => '支出',
  'inquiry_name' => '银行账户验证',
  'not_ready' => '批量付款仍未准备好，请确保所有验证均先成功完成',
  'template_outdated' => '您的模板不是最新的。 请下载并使用最新模板 (:version)',
  'cant_delete_the_only_disbursement' => '您不能删除最后一行',
];