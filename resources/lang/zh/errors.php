<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Errors Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used by the paginator library to build
  | the simple pagination links. You are free to change them to anything
  | you want to customize your views to better match your application.
  |
  */

  // user
  'invalid_otp' => 'OTP无效或已过期',
  'invalid_otp_try_sgain' => 'OTP无效或已过期.你有:count三次机会',
  'email_not_verified' => '邮件未验证',
  'email_taken' => '邮箱已使用',
  'telephone_not_verified' => '电话号码未验证',
  'user_not_verified' => '用户未验证',
  'invalid_email' => '邮箱无效',
  'invalid_password' => '密码无效. 你有:count三次机会',
  'invalid_transaction_pin' => '交易PIN码无效. 你有:count三次机会',
  'invalid_password_deactivated_temporary' => 'PIN 无效. 您的帐户将被停用:hours小时',
  'invalid_email_or_password' => '无效的电子邮件或密码',
  'invalid_old_pin' => '当前引脚无效',
  'account_blocked' => '帐户被封锁。 请联系cs@odeo.co.id',
  'account_blocked_try_again' => '帐户被封锁。 请稍后:count小时再试',
  'account_banned' => '帐户被禁止。 请联系cs@odeo.co.id',
  'account_deactivated_temporary' => '帐户已停用。 请等:minutes分钟',
  'account_deactivated' => '帐户已停用',
  'account_reset_pin' => '帐户密码已过期. 请选择忘记密码以重新激活帐户',
  'invalid_refresh_token' => '刷新令牌无效或已过期',
  'invalid_reset_token' => '密码重置令牌无效或已过期',
  'invalid_user_id' => '用户ID无效',
  'otp_input_blocked_try_again' => 'OTP输入被封锁, 请稍后:count小时再试',
  'invalid_authorization' => '无效. 需要授权',
  'pin_attempt_blocked' => 'PIN码已达到限制次数。 您的操作被拒绝。请联系cs@odeo.co.id了解更多信息',
  'brute_force_attempt_otp' => '用户 (:telephone) 尝试过多次OTP',

  // subscription
  'invalid_plan' => '计划无效',
  'subdomain_name_taken' => '子域名已被占用',
  'domain_name_taken' => '域名已被占用',
  'purchase_domain_name_failed' => '购买域名失败',
  'invalid_user_type' => '用户类型无效',
  'user_store_not_found' => '找不到用户存储',
  'store_not_verified' => '商店未验证',
  'domain_not_supported' => '抱歉，我们暂时不支持此域名。 请使用其他域名',

  // pulsa
  'invalid_telephone' => '电话号码无效',
  'invalid_telephone_or_password' => '无效的电话号码或密码',
  'server_error' => '未知错误',
  'server_query_timeout' => '查询超时',

  'data_not_exists' => '请求的数据不存在',
  'account_mismatch' => '帐户不属于用户',
  'database' => '数据库连接错误',

  'invalid_request' => '非法请求',

  'cart' => [
    'not_found' => '找不到购物车',
    'item_not_found' => '找不到项目',
    'duplicate_item' => '项目已存在',
    'invalid_amount' => '购物车总数与存储的购物车不符'
  ],

  'cash' => [
    'insufficient_cash' => '不足:cash余额',
    'no_pending_withdraw' => '用户没有待处理的提款请求',
    'pending_withdraw_exists' => '用户有待处理的提款请求',
    'no_pending_topup' => '用户没有待处理的充值请求',
    'pending_topup_exists' => '用户有一个待处理的充值请求',
    'pending_deposit_exists' => '用户有待处理的存款请求',
    'maximum_cash_in_particular_time' => ':subject充值不能超过:amount每:time'
  ],

  'bank_account' => [
    'account_name_mismatch' => '指定的帐户名与注册的帐号不匹配(:hint)',
    'bank_not_supported' => '不支持此银行',
    'invalid_account_number' => '帐号无效',
    'failed_to_check_account' => '无法检查帐号。 请稍后再试',
  ],

  'payment_gateway' => [
    'not_found' => '找不到付款',
    'error_update' => '更新失败',
    'payment_not_found' => '找不到付款',
    'invalid_vendor' => '供应商无效',
    'vendor_payment_not_found' => '找不到供应商付款'
  ]

];
