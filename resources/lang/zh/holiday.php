<?php

return [
  'new_year' => '新年 :year',
  'chinese_new_year' => '春节 :year',
  'isra_miraj' => '伊斯兰教登宵节',
  'nyepi' => '印度教的宁静日 :year',
  'good_friday' => '圣周五 ',
  'international_labor' => '国际劳动节',
  'vesak' => '卫塞节 :year',
  'ascension_day' => '耶稣升天',
  'eid_al_fitr' => '开斋节 :year H',
  'pancasila_birthday' => '班查西拉的诞生',
  'eid_al_adha' => '古尔邦节 :year H',
  'independence_day' => '​印度尼西亚独立日',
  'islamic_new_year' => '​伊斯兰教新年 :year H',
  'maulid' => '先知穆哈默德诞辰',
  'christmas' => '圣诞节'
];