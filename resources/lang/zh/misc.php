<?php

return [
  'free' => 'Free',
  'chat_notification' => 'Our customer service opens every day from 06:00 to 24:00 WIB',
  'row_field' => 'row :row\'s :field',
  'or' => ':a or :b',

  'rp_label' => 'IDR '
];