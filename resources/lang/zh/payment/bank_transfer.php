<?php

return [
  'tnc_bank_transfer_bca_1' => '如果您在20:30到05:00之间转帐，由于BCA处于离线状态我们的系统将无法检测到您的付款。 当BCA联机时，您的付款将在稍后处理，并提供汇款成功的报告。 如果在那段时间转账，我们将不承担任何责任。',
  'tnc_bank_transfer_mandiri_1' => '如果您在22:30到03:00之间转帐，由于Mandiri处在离线状态我们的系统无法检测到您的付款。 当Mandiri联机时，您的付款将在稍后处理，并提供汇款成功的报告。 如果那段时间有转账，我们将不承担任何责任。',
  'tnc_bank_transfer_bri_1' => '如果您在22:30到06:00之间转帐, 由于BRI处在离线状态我们的系统无法检测到您的付款。 当BRI联机时，您的付款将在稍后处理，并提供汇款成功的报告。 如果那段时间有转账，我们将不承担任何责任。',
  'tnc_bank_transfer_cimb_1' => '如果您在23:30到02:30之间转帐, 由于CIMB Niaga处在离线状态我们的系统无法检测到您的付款。 当CIMB Niaga联机时，您的付款将在稍后处理，并提供汇款成功的报告。 如果那段时间有转账，我们将不承担任何责任。',

  'tnc_bank_transfer_1' => '您需要使用唯一的代码付款。 这将使付款在最多1小时最少10分钟内自动完成验证。',
  'tnc_bank_transfer_2' => '如果您不使用唯一的代码，请尽快通过转账金额和进行转账的银行帐户名称确认付款。 付款将在2x24小时内验证。',
];