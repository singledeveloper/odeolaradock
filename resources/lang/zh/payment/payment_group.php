<?php
use Odeo\Domains\Constant\Payment;

return [
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_EMONEY => 'Electronic Money',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_INTERNET_BANKING => 'Internet Banking',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_ONLINE_LOAN => 'Online Loan',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_DEBIT_CARD => 'Debit Card',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_CREDIT_CARD => 'Credit Card',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_INSTALLMENT => 'Installment',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_BANK_TRANSFER => 'Bank Transfer',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_ATM_TRANSFER => 'ATM Transfer',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_GROCERY_STORE => 'Grocery Store',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_DEBIT_CREDIT_CARD_PRESENT => 'Debit / Credit Card',
];
