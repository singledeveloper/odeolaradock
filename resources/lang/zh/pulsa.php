<?php

return [

  'tnc_general_right_number' => 'Harap pastikan nomor yang Anda masukkan menggunakan format pengisian nomor dengan benar',
  'tnc_general_wrong_number' => 'Kesalahan baik kesalahan pengisian nomor tujuan atau nomor kadaluarsa, dapat kami bantu jika order tersebut gagal, selain itu menjadi tanggung jawab pembeli dan kami tidak akan melakukan proses refund untuk kondisi tersebut.',
  'tnc_general_complain_statement' => 'Batas maksimal komplain:',
  'tnc_general_complain_statement_suspect' => 'Jika order pending/suspect, batas maksimal komplain adalah 5 hari kerja dari hari transaksi tersebut diproses.',
  'tnc_general_suspect' => 'Jika saldo/item tidak masuk:',
  'tnc_general_suspect_max_complain' => 'Jika order sudah sukses, harap menghubungi customer service kami.',
  'tnc_general_suspect_max_wait' => 'Jika order pending/suspect, harap menunggu maksimal 1x24 jam.',
  'tnc_general_suspect_more_than_24h' => 'Jika order pending/suspect dan sudah melewati 1x24 jam, harap menghubungi customer service kami.',
  'tnc_beforehand_refund_risk' => 'Jika order pending/suspect dan terjadi proses pengembalian dana sebelum adanya update dari sistem kami mengenai pending/suspect tersebut, maka kami tidak dapat menanggung resiko kehilangan dana dari proses tersebut. Mohon dimengerti.',
  'tnc_general_operator_change' => 'Jumlah item yang Anda dapatkan dapat berubah sewaktu-waktu sesuai dengan ketentuan operator. Mohon cek operator bersangkutan secara berkala jika memungkinkan.',

  'tnc_pulsa_right_number' => 'Harap pastikan nomor yang Anda masukkan menggunakan format pengisian nomor dengan benar dan merupakan nomor dalam kondisi masa aktif masih berlaku.',
  'tnc_pulsa_suspect' => 'Pulsa akan terisi secara otomatis setelah Anda melakukan pembayaran. Jika pulsa tidak masuk:',
  'tnc_pulsa_suspect_list_1' => 'Jika order sudah sukses, harap menghubungi operator dengan menyertakan nomor SN yang kami informasikan di dalam aplikasi kami.',
  'tnc_pulsa_suspect_max_complain' => 'Jika order sudah sukses dan nomor SN tidak tersedia, harap menghubungi customer service kami.',
  'tnc_pulsa_suspect_max_wait' => 'Jika order pending/suspect, harap menunggu maksimal 1x24 jam karena hal tersebut sudah menjadi ketentuan operator.',

  'tnc_transportation_right_number' => 'Harap pastikan nomor yang Anda masukkan:',
  'tnc_transportation_right_number_list_1' => 'Untuk Grab adalah nomor akun Grab Anda yang benar',
  'tnc_transportation_right_number_list_2' => 'Untuk Gojek adalah nomor akun Gojek Anda yang benar',
  'tnc_transportation_right_number_list_3' => 'Untuk e-Toll adalah nomor kartu e-Toll / e-Money Mandiri / Indomaret Anda yang benar',
  'tnc_transportation_1' => 'Setelah melakukan pembayaran:',
  'tnc_transportation_1_list_1' => 'Untuk Grab / Gojek saldo akan terisi secara otomatis setelah Anda melakukan pembayaran, mohon cek notifikasi di aplikasi terkait.',
  'tnc_transportation_1_list_2' => 'Untuk e-Toll, Anda harus meng-update secara manual ke ATM Mandiri yang mempunyai reader e-Money.',
  'tnc_transportation_2' => 'Cara update saldo e-Toll:',
  'tnc_transportation_2_list_1' => 'Tempelkan kartu e-Money Anda di reader e-Money ATM',
  'tnc_transportation_2_list_2' => 'Tekan keypad hijau',
  'tnc_transportation_2_list_3' => 'Tekan pilihan update saldo',
  'tnc_transportation_2_list_4' => 'Saldo Anda akan ter-update dan struk keluar dari mesin ATM',

  'tnc_game_voucher_1' => 'Cara mendapatkan ID Mobile Legend:',
  'tnc_game_voucher_1_list_1' => 'Buka game Mobile Legend',
  'tnc_game_voucher_1_list_2' => 'Tekan gambar avatar profile (kiri atas)',
  'tnc_game_voucher_1_list_3' => 'Cek informasi personal, nanti akan muncul 8 id awal + 4/5 id akhir',
  'tnc_game_voucher_1_list_4' => 'Masukkan total 12/13 id tersebut (digabung tanpa spasi) ke kolom input ID Player',

  'empty_stock' => 'Stok kosong, silakan melakukan transaksi dengan produk lain.',
  'cant_purchase' => 'Tidak dapat melakukan transaksi sekarang, silakan mencoba lagi.',
  'cant_purchase_suggest_other' => 'Tidak dapat melakukan transaksi dengan nomor telepon dan harga yang sama.',
  'cant_purchase_cutoff' => ':Sedang ada cutoff antara jam 23.30 - 00.30, silakan coba lagi setelah proses selesai.',
  'cant_purchase_no_backup' => 'Anda tidak dapat membeli produk ini.',
  'cant_buy_without_kyc' => 'Anda tidak dapat melakukan pembelian pulsa lagi sebelum melakukan verifikasi KTP.',

  'inline' => [
    'empty_stock' => 'GAGAL Trx ke :number. Stok kosong, silakan melakukan transaksi dengan produk lain.',
    'failed' => 'GAGAL Trx ke :number. Tidak dapat melakukan transaksi sekarang, silakan mencoba lagi.',
    'same_transaction' => 'Trx sdh pernah @:date status: :message',
    'cut_off' => 'GAGAL Trx ke :number. Sedang ada cutoff antara jam 23.30 - 00.30, silakan coba lagi setelah proses selesai.',
    'pending' => 'PENDING Trx #:order_id :item_name; :number sedang diproses.',
    'success' => 'SUKSES Trx #:order_id :item_name; :number SN: :sn. Sisa saldo :sisa_saldo.',
    'success_for_customer' => ':item_name; :number SN: :sn.',
    'refund' => 'GAGAL Trx #:order_id :item_name; :number, sudah di-refund. Sisa saldo :sisa_saldo.',
    'topup_queue' => 'Transfer :amount ke rekening :bank_name :account_number a/n :account_name selambatnya :expired_at.',
    'topup_success' => 'SUKSES Saldo berhasil ditambahkan. Sisa saldo :sisa_saldo.',
    'block_sms_to' => 'GAGAL Trx ke :number. Anda tidak dapat menggunakan format ini pada kode non-PLN.'
  ]

];
