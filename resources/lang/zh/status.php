<?php

return [
  'api_pending' => '待处理',
  'api_pending_not_enough_deposit' => '等待中的存款不足',
  'api_validation_on_progress' => '进度验证',
  'api_validation_success' => '验证成功',
  'api_validation_manual_success' => '手动验证成功',
  'api_validation_failed' => '验证失败',
  'api_validation_failed_name_mismatch' => '验证失败名称不匹配',
  'api_validation_failed_wrong_number' => '验证失败帐号错误',
  'api_validation_failed_closed_account' => '验证失败银行帐户已关闭',
  'api_on_progress' => '进行中',
  'api_on_progress_wait_notify' => '正在等待通知',
  'api_fail_processing' => '处理失败',
  'api_completed' => '已完成',
  'api_suspect' => '未知',
  'api_suspect_even_success' => '未知',
  'api_failed' => '失败',
  'api_failed_wrong_number' => '失败帐号错误',
  'api_failed_closed_account' => '失败银行帐户已关闭',
  'api_failed_bank_rejection' => '失败银行回绝',
  'api_failed_vendor_down' => '失败供应商系统故障',
  'api_failed_duplicate' => '失败请求重复',
  'api_cancelled' => '取消',
  'api_retried' => '已重试',
  'api_unknown' => '状态码未知',
  'api_on_hold' => '暂停',
  'api_settled' => '已结算',
  'api_no_settlement' => '没有结算',
  'api_on_progress_payment_approval' => '在进度付款审批',
  'api_pending_merchant_acceptance' => '等候商户受理',
  'api_unpaid' => '未付款',
  'api_paid' => '已付款',
  'api_void' => '无效',
  'api_partial_paid' => '部分付款已支付',
  'api_overdue' => '逾期',
  'api_verified' => '已验证',
  'api_approved' => '已批准'
];