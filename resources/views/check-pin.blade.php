<style>
    table {
        font-family: Arial;
        margin: auto;
    }

    table td {padding: 10px 5px; border: 1pt solid #666;}

</style>
<table>
    <tr>
      <th>No</th>
      <th>ID</th>
      <th>Name</th>
      <th>Email</th>
      <th>Telephone</th>
      <th>Android Version</th>
      <th>SMS</th>
      <th>Jabber</th>
      <th>Telegram</th>
      <th>Notes</th>
    </tr>
  <?php $x = 1; foreach ($users as $key => $item): ?>
    <tr>
        <td>{{ $x }}.</td>
        <td>{{ $key }}</td>
        <td>{{ $item['name'] }}</td>
        <td>{{ $item['email'] }}</td>
        <td>{{ $item['telephone'] }}</td>
        <td>{{ $item['android_version'] }}</td>
        <td>{{ $item['is_sms'] ? 'YES' : 'NO' }}</td>
        <td>{{ $item['is_jabber'] ? 'YES' : 'NO' }}</td>
        <td>{{ $item['is_telegram'] ? 'YES' : 'NO' }}</td>
        <td>
            {{ $item['note'] }}<br/>
            {{ json_encode($item['h2h_accounts']) }}
        </td>
    </tr>
  <?php $x++; endforeach; ?>
</table>