<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
@inject('currencyHelper', 'Odeo\Domains\Transaction\Helper\Currency')

<h2>Artajasa topup detected on date {{$data['datetime']}}</h2>

Artajasa balance: {{$currencyHelper->formatPrice($data['artajasa_balance'])['formatted_amount']}} <br>
Recorded balance: {{$currencyHelper->formatPrice($data['recorded_balance'])['formatted_amount']}} <br>
Topup amount: {{$currencyHelper->formatPrice($data['topup_amount'])['formatted_amount']}} <br>
Final balance: {{$currencyHelper->formatPrice($data['final_balance'])['formatted_amount']}} <br>

</body>
</html>