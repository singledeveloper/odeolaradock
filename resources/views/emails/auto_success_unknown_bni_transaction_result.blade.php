<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
<h1>Auto Success Unknown BNI Transaction Result - {{$date}}</h1>
<table>
    <tr>
        <th>BNI EDC ID</th>
        <th>Status</th>
        <th>Message</th>
    </tr>
    @foreach($result as $row)
        <tr>
            <td>{{$row['id']}}</td>
            <td>{{$row['status']}}</td>
            <td>{{$row['message']}}</td>
        </tr>
    @endforeach
</table>

</body>
</html>