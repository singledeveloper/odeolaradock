<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
<p>Mutations of bank {{$data['bank_name']}} are not matched. Possible missing mutation exists. Last scraped mutations' date: {{$data['last_scraped_date']}}</p>
</body>
</html>