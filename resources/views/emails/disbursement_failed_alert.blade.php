<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
<table>
    <h2>Disbursement Failed!</h2>
    <tr>
        <td>Disbursement ID</td>
        <td>{{$data['disbursement_id']}}</td>
    </tr>
    <tr>
        <td>Status</td>
        <td>{{$data['status']}}</td>
    </tr>
    <tr>
        <td>Vendor</td>
        <td>{{$data['vendor']}}</td>
    </tr>
</table>
</body>
</html>