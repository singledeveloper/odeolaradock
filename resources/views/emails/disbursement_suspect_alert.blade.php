<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
API Disbursement suspect or long pending detected. <br>
<table>
  <tr>
    <td>ID</td>
    <td>Vendor</td>
    <td>Status</td>
    <td>Date</td>
  </tr>
  @foreach ($data as $row)
    <tr>
      <td>{{$row['id']}}</td>
      <td>{{$row['auto_disbursement_vendor']}}</td>
      <td>{{\Odeo\Domains\Constant\ApiDisbursement::getStatusMessage($row['status'])}}</td>
      <td>{{$row['created_at']}}</td>
    </tr>
  @endforeach
</table>
</body>
</html>