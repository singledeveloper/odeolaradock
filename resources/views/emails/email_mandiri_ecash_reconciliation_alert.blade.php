<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
    .text-center, .text-center td, .text-center th {
        text-align: left;
    }

    tr.total td {
        font-weight: bold;
    }
</style>

@inject('currencyHelper', 'Odeo\Domains\Transaction\Helper\Currency')

<body style="font-family: 'Arial'; font-size: 14px">

@if (count($odeoOnly) > 0)
    <p>The following order(s) <strong>only exist on Odeo</strong> and missing from Mandiri E-cash:</p>
    <ul>
        @foreach ($odeoOnly as $item)
            <li>{{ $item['order_id'] }}</li>
        @endforeach
    </ul>
    <br/>
@endif

@if (count($mandiriOnly) > 0)
    <p>The following order(s) <strong>only exist on Mandiri E-cash</strong> and missing from Odeo:</p>
    <ul>
        @foreach ($mandiriOnly as $item)
            <li>{{ $item['order_id'] }}</li>
        @endforeach
    </ul>
    <br/>
@endif

@if (count($wrongAmounts) > 0)
    <p>The following order(s) have <strong>mismatch amount:</strong></p>
    <table border="1" cellspacing="0" cellpadding="5" class="text-center">
        <tr>
            <th>Order ID</th>
            <th>Odeo Amount</th>
            <th>Mandiri Amount</th>
            <th>Odeo Charge Amount</th>
            <th>Mandiri Charge Amount (MDR)</th>
        </tr>
        @foreach ($wrongAmounts as $item)
            <tr>
                <td>{{ $item['odeo_order_id'] }}</td>
                <td>{{ $currencyHelper->formatPrice($item['subtotal'])['formatted_amount'] }}</td>
                <td>{{ $currencyHelper->formatPrice($item['amount'])['formatted_amount'] }}</td>
                <td>{{ $currencyHelper->formatPrice($item['charge_amount'])['formatted_amount'] }}</td>
                <td>{{ $currencyHelper->formatPrice($item['merchant_discount_rate'])['formatted_amount'] }}</td>
            </tr>
        @endforeach
    </table>
@endif


@if (count($statusNotMatch) > 0)
    <p>The following order(s) have <strong>mismatch status:</strong></p>
    <table border="1" cellspacing="0" cellpadding="5" class="text-center">
        <tr>
            <th>Order ID</th>
            <th>Odeo Status</th>
        </tr>
        @foreach ($statusNotMatch as $item)
            <tr>
                <td>{{ $item['odeo_order_id'] }}</td>
                <td>{{ $item['status'] }}</td>
            </tr>
        @endforeach
    </table>
@endif

@if(count($missingCharges) > 0)
    <p>The following order(s) <strong>charge is missing</strong> or <strong>mismatch status</strong>:</p>
    <ul>
        @foreach ($missingCharges as $charge)
            <li>{{ $charge['order_id'] }}</li>
        @endforeach
    </ul>
@endif

</body>
</html>
