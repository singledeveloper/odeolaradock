<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
@inject('currencyHelper', 'Odeo\Domains\Transaction\Helper\Currency')

Double purchase risk because of {{ $criminalName }}<br/>
Detail:<br/>
Name: {{ $switcherOrder->orderDetail->name }}<br/>
Order ID: {{ $switcherOrder->orderDetail->order_id }}<br/>
Estimated Loss: {{ $currencyHelper->formatPrice($switcherOrder->current_base_price)['formatted_amount'] }}
Please check this later.
</body>
</html>