<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
  .text-center, .text-center td, .text-center th {
    text-align: left;
  }

  tr.total td {
    font-weight: bold;
  }
</style>

@inject('ch', 'Odeo\Domains\Transaction\Helper\Currency')

<body style="font-family: 'Arial'; font-size: 14px">

<h4>Sales Report Date : {{$date}}</h4>

<h2>GMV Overview</h2>
<table border="1" cellspacing="0" cellpadding="5">
  <tr>
    <th>Product</th>
    <th>This Month Target</th>
    <th>Target MTD</th>
    <th>Achievement MTD</th>
    <th>Run Rate</th>
    <th>Prediction</th>
    <th>All Time High</th>
    <th>ATH Run Rate</th>
    <th>Loss Sales</th>
    <th>Last 1 Year</th>
  </tr>
  @foreach($products as $p => $label)
    <tr>
      <td style="font-weight: bold;">{{ $label }}</td>
      <td style="text-align: right;">{{ $ch->getFormattedOnly($targetMap[$p]->gmv) }}</td>
      <td style="text-align: right;">{{ $ch->getFormattedOnly($targetMtdMap[$p]->gmv) }}</td>
      <td style="text-align: right;">{{ $ch->getFormattedOnly($mtdMap[$p]->gmv) }}</td>
      <td style="text-align: right;">{{ number_format($runRateMap[$p]->gmv, 0, ",", ".") }}%</td>
      <td style="text-align: right;">{{ $ch->getFormattedOnly($predictionMap[$p]->gmv) }}</td>
      <td style="text-align: right;">{{ $ch->getFormattedOnly($athMap[$p]->gmv) }}</td>
      <td style="text-align: right;">{{ number_format($athRunRateMap[$p]->gmv, 0, ",", ".") }}%</td>
      <td
        style="text-align: right;">{{ isset($refundedMtdMap[$p]) ? $ch->getFormattedOnly($refundedMtdMap[$p]->gmv) : '-' }}</td>
      <td style="text-align: right;">{{ $ch->getFormattedOnly($ytdMap[$p]->gmv) }}</td>
    </tr>
  @endforeach
  <tr>
    <td style="font-weight: bold;">Total (IDR)</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTarget->gmv) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTargetMtd->gmv) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesMtd->gmv) }}</td>
    <td style="text-align: right;">{{ number_format($salesRunRate->gmv, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesPrediction->gmv) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesAth->gmv) }}</td>
    <td style="text-align: right;">{{ number_format($salesAthRunRate->gmv, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesRefundedMtd->gmv) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesYtd->gmv) }}</td>
  </tr>
  <tr>
    <td style="font-weight: bold;">USD Rate</td>
    <td colspan="9" style="text-align: center;">{{ $ch->getFormattedOnly($fxRate) }}</td>
  </tr>
  <tr>
    <td style="font-weight: bold;">Total (USD)</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTarget->gmv / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTargetMtd->gmv / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesMtd->gmv / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ number_format($salesRunRate->gmv, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesPrediction->gmv / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesAth->gmv / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ number_format($salesAthRunRate->gmv, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesRefundedMtd->gmv / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesYtd->gmv / $fxRate, 'USD') }}</td>
  </tr>
</table>

<h2>Revenue Overview</h2>
<table border="1" cellspacing="0" cellpadding="5">
  <tr>
    <th>Product</th>
    <th>This Month Target</th>
    <th>Target MTD</th>
    <th>Achievement MTD</th>
    <th>Run Rate</th>
    <th>Prediction</th>
    <th>All Time High</th>
    <th>ATH Run Rate</th>
    <th>Loss Sales</th>
    <th>Last 1 Year</th>
  </tr>
  @foreach($products as $p => $label)
    <tr>
      <td style="font-weight: bold;">{{ $label }}</td>
      <td style="text-align: right;">{{ $ch->getFormattedOnly($targetMap[$p]->revenue) }}</td>
      <td style="text-align: right;">{{ $ch->getFormattedOnly($targetMtdMap[$p]->revenue) }}</td>
      <td style="text-align: right;">{{ $ch->getFormattedOnly($mtdMap[$p]->revenue) }}</td>
      <td style="text-align: right;">{{ number_format($runRateMap[$p]->revenue, 0, ",", ".") }}%</td>
      <td style="text-align: right;">{{ $ch->getFormattedOnly($predictionMap[$p]->revenue) }}</td>
      <td style="text-align: right;">{{ $ch->getFormattedOnly($athMap[$p]->revenue) }}</td>
      <td style="text-align: right;">{{ number_format($athRunRateMap[$p]->revenue, 0, ",", ".") }}%</td>
      <td
        style="text-align: right;">{{ isset($refundedMtdMap[$p]) ? $ch->getFormattedOnly($refundedMtdMap[$p]->revenue) : '-' }}</td>
      <td style="text-align: right;">{{ $ch->getFormattedOnly($ytdMap[$p]->revenue) }}</td>
    </tr>
  @endforeach
  <tr>
    <td style="font-weight: bold;">Total (IDR)</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTarget->revenue) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTargetMtd->revenue) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesMtd->revenue) }}</td>
    <td style="text-align: right;">{{ number_format($salesRunRate->revenue, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesPrediction->revenue) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesAth->revenue) }}</td>
    <td style="text-align: right;">{{ number_format($salesAthRunRate->revenue, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesRefundedMtd->revenue) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesYtd->revenue) }}</td>
  </tr>
  <tr>
    <td style="font-weight: bold;">USD Rate</td>
    <td colspan="9" style="text-align: center;">{{ $ch->getFormattedOnly($fxRate) }}</td>
  </tr>
  <tr>
    <td style="font-weight: bold;">Total (USD)</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTarget->revenue / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTargetMtd->revenue / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesMtd->revenue / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ number_format($salesRunRate->revenue, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesPrediction->revenue / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesAth->revenue / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ number_format($salesAthRunRate->revenue, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesRefundedMtd->revenue / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesYtd->revenue / $fxRate, 'USD') }}</td>
  </tr>
</table>

<h2>Transaction Count Overview</h2>
<table border="1" cellspacing="0" cellpadding="5">
  <tr>
    <th>Product</th>
    <th>This Month Target</th>
    <th>Target MTD</th>
    <th>Achievement MTD</th>
    <th>Run Rate</th>
    <th>Prediction</th>
    <th>All Time High</th>
    <th>ATH Run Rate</th>
    <th>Loss Sales</th>
    <th>Last 1 Year</th>
  </tr>
  @foreach($products as $p => $label)
    <tr>
      <td style="font-weight: bold;">{{ $label }}</td>
      <td style="text-align: right;">{{ number_format($targetMap[$p]->trx, 0, ",", ".") }}</td>
      <td style="text-align: right;">{{ number_format($targetMtdMap[$p]->trx, 0, ",", ".") }}</td>
      <td style="text-align: right;">{{ number_format($mtdMap[$p]->trx, 0, ",", ".") }}</td>
      <td style="text-align: right;">{{ number_format($runRateMap[$p]->trx, 0, ",", ".") }}%</td>
      <td style="text-align: right;">{{ number_format($predictionMap[$p]->trx, 0, ",", ".") }}</td>
      <td style="text-align: right;">{{ number_format($athMap[$p]->trx, 0, ",", ".") }}</td>
      <td style="text-align: right;">{{ number_format($athRunRateMap[$p]->trx, 0, ",", ".") }}%</td>
      <td
        style="text-align: right;">{{ isset($refundedMtdMap[$p]) ? number_format($refundedMtdMap[$p]->trx, 0, ",", ".") : '-' }}</td>
      <td style="text-align: right;">{{ number_format($ytdMap[$p]->trx, 0, ",", ".") }}</td>
    </tr>
  @endforeach
  <tr>
    <td style="font-weight: bold;">Total</td>
    <td style="text-align: right;">{{ number_format($salesTarget->trx, 0, ",", ".") }}</td>
    <td style="text-align: right;">{{ number_format($salesTargetMtd->trx, 0, ",", ".") }}</td>
    <td style="text-align: right;">{{ number_format($salesMtd->trx, 0, ",", ".") }}</td>
    <td style="text-align: right;">{{ number_format($salesRunRate->trx, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ number_format($salesPrediction->trx, 0, ",", ".") }}</td>
    <td style="text-align: right;">{{ number_format($salesAth->trx, 0, ",", ".") }}</td>
    <td style="text-align: right;">{{ number_format($salesAthRunRate->trx, 0, ",", ".") }}</td>
    <td style="text-align: right;">{{ number_format($salesRefundedMtd->trx, 0, ",", ".") }}</td>
    <td style="text-align: right;">{{ number_format($salesYtd->trx, 0, ",", ".") }}</td>
  </tr>
</table>

<br>
<br>
<br>

<h2>Sales MTD Marketplace</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <td></td>
    <th>GMV</th>
    <th>PROFIT</th>
    <th>TRX</th>
    <th>Group</th>
    <th>GMV</th>
    <th>PROFIT</th>
    <th>TRX</th>
  </tr>

  @foreach($orderPerPlatformMP as $groupName => $group)
    @foreach($group as $key => $summary)
      <tr>
        <th>{{ $summary['platform_name'] }}</th>
        <td style="text-align: right">{{ $ch->formatPrice($summary['gross_amount'])['formatted_amount'] }}</td>
        <td style="text-align: right">{{ $ch->formatPrice($summary['surplus'])['formatted_amount'] }}</td>
        <td style="text-align: right">{{ number_format($summary['order_total'], 0, ",", ".") }}</td>
        @if ($key == 0)
          <th rowspan="{{count($group)}}">{{ strtoupper($groupName) }}</th>
          <td rowspan="{{count($group)}}"
              style="text-align: right">{{ $ch->formatPrice($group->sum('gross_amount'))['formatted_amount'] }}</td>
          <td rowspan="{{count($group)}}"
              style="text-align: right">{{ $ch->formatPrice($group->sum('surplus'))['formatted_amount'] }}</td>
          <td rowspan="{{count($group)}}"
              style="text-align: right">{{ number_format($group->sum('order_total'), 0, ",", ".") }}</td>
        @endif
      </tr>
    @endforeach
  @endforeach
</table>

<h2>Sales MTD Agent</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <td></td>
    <th>GMV</th>
    <th>PROFIT</th>
    <th>TRX</th>
    <th>Group</th>
    <th>GMV</th>
    <th>PROFIT</th>
    <th>TRX</th>
  </tr>

  @foreach($orderPerPlatformAgent as $groupName => $group)
    @foreach($group as $key => $summary)
      <tr>
        <th>{{ $summary['platform_name'] }}</th>
        <td style="text-align: right">{{ $ch->formatPrice($summary['gross_amount'])['formatted_amount'] }}</td>
        <td style="text-align: right">{{ $ch->formatPrice($summary['surplus'])['formatted_amount'] }}</td>
        <td style="text-align: right">{{ number_format($summary['order_total'], 0, ",", ".") }}</td>
        @if ($key == 0)
          <th rowspan="{{count($group)}}">{{ strtoupper($groupName) }}</th>
          <td rowspan="{{count($group)}}"
              style="text-align: right">{{ $ch->formatPrice($group->sum('gross_amount'))['formatted_amount'] }}</td>
          <td rowspan="{{count($group)}}"
              style="text-align: right">{{ $ch->formatPrice($group->sum('surplus'))['formatted_amount'] }}</td>
          <td rowspan="{{count($group)}}"
              style="text-align: right">{{ number_format($group->sum('order_total'), 0, ",", ".") }}</td>
        @endif
      </tr>
    @endforeach
  @endforeach
</table>

<h2>Report Sales Product Based On Service</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Type</th>
    <th>Sales</th>
    <th>Merchant Price</th>
  </tr>
  <?php $sales = 0; $salesAmount = 0; ?>
  @foreach ($salesOcommerceReports as $item)
    <tr>
      <td>{{ $item->service->displayed_name }}</td>
      <td style="text-align: right;">{{ $item->sales_quantity }}<?php $sales += $item->sales_quantity; ?></td>
      <td style="text-align: right;">
        {{ $ch->formatPrice($item->sales_amount)['formatted_amount'] }}
        <?php $salesAmount += $item->sales_amount; ?>
      </td>
    </tr>
  @endforeach
  <tr class="total">
    <td>Total</td>
    <td style="text-align: right;">{{ $sales }}</td>
    <td style="text-align: right;">{{ $ch->formatPrice($salesAmount)['formatted_amount'] }}</td>
  </tr>
</table>

<h2>Report Sales Product Based On Payment Gateway</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Type</th>
    <th>Usage</th>
    <th>Total Usage Amount</th>
  </tr>
  <?php $sales = 0; $salesAmount = 0; ?>
  @foreach ($salesByPaymentReports as $item)
    <tr>
      <td>{{ $item->information->name }}</td>
      <td style="text-align: right">{{ $item->sales_quantity }}<?php $sales += $item->sales_quantity; ?></td>
      <td style="text-align: right">
        {{ $ch->formatPrice($item->sales_amount)['formatted_amount'] }}
        <?php $salesAmount += $item->sales_amount; ?>
      </td>
    </tr>
  @endforeach
  <tr class="total">
    <td>Total</td>
    <td style="text-align: right">{{ $sales }}</td>
    <td style="text-align: right">{{ $ch->formatPrice($salesAmount)['formatted_amount'] }}</td>
  </tr>
</table>

<h2>Report API Disbursements Based On Vendor</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Vendor</th>
    <th>GMV</th>
    <th>Profit</th>
    <th>TRX</th>
  </tr>
  <?php $gmv = 0; $profit = 0; $trx = 0; ?>
  @foreach ($apiDisbursementReports as $item)
    <tr>
      <td>{{ strtoupper(explode('_', $item->auto_disbursement_vendor)[0]) }}</td>
      <td style="text-align: right">
        {{ $ch->formatPrice($item->gross_amount)['formatted_amount'] }}
        <?php $gmv += $item->gross_amount; ?>
      </td>
      <td style="text-align: right">
        {{ $ch->formatPrice($item->surplus)['formatted_amount'] }}
        <?php $profit += $item->surplus; ?>
      </td>
      <td style="text-align: right">
        {{ $item->order_total }}
        <?php $trx += $item->order_total; ?>
      </td>
    </tr>
  @endforeach
  <tr class="total">
    <td>Total</td>
    <td style="text-align: right">{{ $ch->formatPrice($gmv)['formatted_amount'] }}</td>
    <td style="text-align: right">{{ $ch->formatPrice($profit)['formatted_amount'] }}</td>
    <td style="text-align: right">{{ $trx }}</td>
  </tr>
</table>


<h2>Report API Disbursement Inquiries Based On Vendor</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Vendor</th>
    <th>GMV</th>
    <th>Profit</th>
    <th>TRX</th>
  </tr>
  <?php $gmv = 0; $profit = 0; $trx = 0; ?>
  @foreach ($apiDisbursementInquiryReports as $item)
    <tr>
      <td>{{ strtoupper(explode('_', $item->vendor)[0]) }}</td>
      <td style="text-align: right">
        {{ $ch->formatPrice($item->gross_amount)['formatted_amount'] }}
        <?php $gmv += $item->gross_amount; ?>
      </td>
      <td style="text-align: right">
        {{ $ch->formatPrice($item->surplus)['formatted_amount'] }}
        <?php $profit += $item->surplus; ?>
      </td>
      <td style="text-align: right">
        {{ $item->order_total }}
        <?php $trx += $item->order_total; ?>
      </td>
    </tr>
  @endforeach
  <tr class="total">
    <td>Total</td>
    <td style="text-align: right">{{ $ch->formatPrice($gmv)['formatted_amount'] }}</td>
    <td style="text-align: right">{{ $ch->formatPrice($profit)['formatted_amount'] }}</td>
    <td style="text-align: right">{{ $trx }}</td>
  </tr>
</table>

<h2>Report API Disbursements Based On Beneficiary</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Beneficiary Bank</th>
    <th>Total Value</th>
    <th>Total Quantity</th>
    <th>Percentage</th>
  </tr>

  <?php $totalValue = 0; $totalQuantity = 0; ?>
  @foreach ($apiDisbursementRecipients as $item)
    <tr>
      <td>{{ $item->name }}</td>
      <td style="text-align: right">
        {{ $ch->formatPrice($item->gross_amount)['formatted_amount'] }}
        <?php $totalValue += $item->gross_amount; ?>
      </td>
      <td style="text-align: right">
        {{ $item->order_total }}
        <?php $totalQuantity += $item->order_total; ?>
      </td>
      <td style="text-align: right">
        {{ sprintf('%.2f%%', $item->percentage) }}
      </td>
    </tr>
  @endforeach
  <tr class="total">
    <td>Total</td>
    <td style="text-align: right">{{ $ch->formatPrice($totalValue)['formatted_amount'] }}</td>
    <td style="text-align: right">{{ $totalQuantity }}</td>
    <td style="text-align: right">100%</td>
  </tr>
</table>

<h2>Top 10 API Disbursement User</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Client Name</th>
    <th>Total Value</th>
    <th>Total Transaction</th>
    <th>Percentage</th>
  </tr>

  <?php $totalTransaction = 0; $totalValue = 0; ?>
  @foreach ($apiDisbursementTopUsers as $item)
    <tr>
      <td>{{ $item->name }}</td>
      <td style="text-align: right">
        {{ $ch->formatPrice($item->gross_amount)['formatted_amount'] }}
        <?php $totalValue += $item->gross_amount; ?>
      </td>
      <td style="text-align: right">
        {{ $item->transaction_total }}
        <?php $totalTransaction += $item->transaction_total; ?>
      </td>
      <td style="text-align: right">
        {{ sprintf('%.2f%%', $item->percentage) }}
      </td>
    </tr>
  @endforeach
  <tr class="total">
    <td>Total</td>
    <td style="text-align: right">{{ $ch->formatPrice($totalValue)['formatted_amount'] }}</td>
    <td style="text-align: right">{{ $totalTransaction }}</td>
    <td style="text-align: right">100%</td>
  </tr>
</table>

<h2>Top 10 API Disbursement Inquiry User</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Client Name</th>
    <th>Total Transaction</th>
    <th>Percentage</th>
  </tr>

  <?php $totalTransaction = 0; ?>
  @foreach ($apiDisbursementInquiryTopUsers as $item)
    <tr>
      <td>{{ $item->name }}</td>
      <td style="text-align: right">
        {{ $item->transaction_total }}
        <?php $totalTransaction += $item->transaction_total; ?>
      </td>
      <td style="text-align: right">
        {{ sprintf('%.2f%%', $item->percentage) }}
      </td>
    </tr>
  @endforeach
  <tr class="total">
    <td>Total</td>
    <td style="text-align: right">{{ $totalTransaction }}</td>
    <td style="text-align: right">100%</td>
  </tr>
</table>

<h2>Report Payment Gateway Based On Vendor</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Vendor</th>
    <th>GMV</th>
    <th>Profit</th>
    <th>TRX</th>
  </tr>
  <?php $gmv = 0; $profit = 0; $trx = 0; ?>
  @foreach ($paymentGatewayReports as $item)
    <tr>
      <td>{{ strtoupper(explode('_', $item->vendor_name)[0]) }}</td>
      <td style="text-align: right">
        {{ $ch->formatPrice($item->gmv)['formatted_amount'] }}
        <?php $gmv += $item->gmv; ?>
      </td>
      <td style="text-align: right">
        {{ $ch->formatPrice($item->profit)['formatted_amount'] }}
        <?php $profit += $item->profit; ?>
      </td>
      <td style="text-align: right">
        {{ $item->total }}
        <?php $trx += $item->total; ?>
      </td>
    </tr>
  @endforeach
  <tr class="total">
    <td>Total</td>
    <td style="text-align: right">{{ $ch->formatPrice($gmv)['formatted_amount'] }}</td>
    <td style="text-align: right">{{ $ch->formatPrice($profit)['formatted_amount'] }}</td>
    <td style="text-align: right">{{ $trx }}</td>
  </tr>
</table>

<h2>Top 10 Payment Gateway User</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Client Name</th>
    <th>Total Value</th>
    <th>Total Transaction</th>
    <th>Percentage</th>
  </tr>

  <?php $totalTransaction = 0; $totalValue = 0; ?>
  @foreach ($paymentGatewayTopUsers as $item)
    <tr>
      <td>{{ $item->name }}</td>
      <td style="text-align: right">
        {{ $ch->formatPrice($item->gross_amount)['formatted_amount'] }}
        <?php $totalValue += $item->gross_amount; ?>
      </td>
      <td style="text-align: right">
        {{ $item->transaction_total }}
        <?php $totalTransaction += $item->transaction_total; ?>
      </td>
      <td style="text-align: right">
        {{ sprintf('%.2f%%', $item->percentage) }}
      </td>
    </tr>
  @endforeach
  <tr class="total">
    <td>Total</td>
    <td style="text-align: right">{{ $ch->formatPrice($totalValue)['formatted_amount'] }}</td>
    <td style="text-align: right">{{ $totalTransaction }}</td>
    <td style="text-align: right">100%</td>
  </tr>
</table>

</body>
</html>