@extends('emails.base')

@section('styles')
  <style type="text/css">
    #title {
      font-size: 24px;
      font-weight: bold;
      margin: 0px;
      margin-left: 10px;
    }
    #upper-description {
      margin: 10px 0px 0px 0px;
    }
    #detail-box {
      margin: 0 auto;
      background-color: rgb(248,248,248);
    }
    #inner-detail-box {
      margin: 20px 30px;
    }
    #best-regards {
      margin: 10px 10px; 
      margin-bottom: 5px; 
      color: rgb(144,145,146);
      font-size: 14px;
    }
    #best-regards-description {
      margin: 0px 10px;
      margin-top: 5px;
      margin-bottom: 15px;
      color: rgb(0,151,143); 
      font-weight: bold;
      font-size: 14px;
    }
    .common-margin {
      margin: 20px 10px;
    }
    .left-box-text {
      color: rgb(144,145,146); 
      margin: 10px 0px;
      font-size: 14px;
    }
    .middle-box-text {
      color: rgb(144,145,146); 
      margin: 10px 10px;
      font-size: 14px;
    }
    .right-box-text {
      margin: 10px 0px;
      font-size: 14px;
      font-weight: bold;
    }
  </style>
@endsection

@section('content')
  <p id="title" class="text-24 bold-text">{{ isset($title) ? $title : 'Laporan Pembelian' }}</p>

  <table class="common-margin" cellpadding="0" cellspacing="0">
    <tr>
      <td>
        <p class="no-margin">Dear <b>{{$data['user_name'] or '-'}}</b>,</p>
      </td>
    </tr>
    <tr>
      <td>
        <p id="upper-description">Terlampir pada email ini {{ isset($title) ? strtolower($title) : 'laporan pembelian' }} mitra dengan filter:</p>
      </td>
    </tr>
  </table>

  <table id="detail-box">
    <tr>
      <td width="520">
      <table id="inner-detail-box" cellpadding="0" cellspacing="0">
        <tr>
          <td width="220">
            <p class="left-box-text">Dari Tanggal</p>
          </td>
          <td>
            <p class="middle-box-text">:</p>
          </td>
          <td>
            <p class="right-box-text">{{$data['start_date'] or '-'}}</p>
          </td>
        </tr>
        <tr>
          <td width="220">
            <p class="left-box-text">Hingga Tanggal</p>
          </td>
          <td>
            <p class="middle-box-text">:</p>
          </td>
          <td>
            <p class="right-box-text">{{$data['end_date'] or '-'}}</p>
          </td>
        </tr>

        @if(isset($data['order_id']) && $data['order_id'] !== '')
        <tr>
          <td width="220">
            <p class="left-box-text">Order ID</p>
          </td>
          <td>
            <p class="middle-box-text">:</p>
          </td>
          <td>
            <p class="right-box-text">{{$data['order_id']}}</p>
          </td>
        </tr>
        @endif

        @if(isset($data['status_message']) && $data['status_message'] !== '')
        <tr>
          <td width="220">
            <p class="left-box-text">Status</p>
          </td>
          <td>
            <p class="middle-box-text">:</p>
          </td>
          <td>
            <p class="right-box-text">{{$data['status_message']}}</p>
          </td>
        </tr>
        @endif

        @if(isset($data['service_name']) && $data['service_name'] !== '')
        <tr>
          <td width="220">
            <p class="left-box-text">Product</p>
          </td>
          <td>
            <p class="middle-box-text">:</p>
          </td>
          <td>
            <p class="right-box-text">{{$data['service_name']}}</p>
          </td>
        </tr>
        @endif

        @if(isset($data['payment_name']) && $data['payment_name'] !== '')
        <tr>
          <td width="220">
            <p class="left-box-text">Payment Method</p>
          </td>
          <td>
            <p class="middle-box-text">:</p>
          </td>
          <td>
            <p class="right-box-text">{{$data['payment_name']}}</p>
          </td>
        </tr>
        @endif

        @if(isset($data['trx_type']) && $data['trx_type'] !== '')
        <tr>
          <td width="220">
            <p class="left-box-text">Transaction Type</p>
          </td>
          <td>
            <p class="middle-box-text">:</p>
          </td>
          <td>
            <p class="right-box-text">{{$data['trx_type']}}</p>
          </td>
        </tr>
        @endif

        @if(isset($data['category']) && $data['category'] !== '')
        <tr>
          <td width="220">
            <p class="left-box-text">Category</p>
          </td>
          <td>
            <p class="middle-box-text">:</p>
          </td>
          <td>
            <p class="right-box-text">{{$data['category']}}</p>
          </td>
        </tr>
        @endif

      </table>
      </td>
    </tr>
  </table>

  <p id="best-regards" >Best Regards,</p>
  
  <p id="best-regards-description">Customer Service - ODEO</p>

@endsection
