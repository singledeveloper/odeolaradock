<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
<h2>FLIP HAS NO ENOUGH DEPOSIT</h2>
Cannot process {{$data['reference_type']}} - {{$data['reference_id']}} in amount {{$data['requested_amount']['formatted_amount']}} for user ID {{$data['user_id']}} <br><br>
last Flip balance: {{$data['balance']['formatted_amount']}}
</body>
</html>