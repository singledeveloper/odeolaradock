<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
<h1>Invalid Pax Payment Data</h1>
<table>
    <tr>
        <td>Terminal ID</td>
        <td>{{$terminalId}}</td>
    </tr>
    <tr>
        <td>Order ID</td>
        <td>{{$orderId}}</td>
    </tr>
    <tr>
        <td>Payment Data</td>
        <td>
            <pre>{{$paxPaymentData}}</pre>
        </td>
    </tr>
</table>

</body>
</html>