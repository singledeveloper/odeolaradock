<table style="color: #0a2529; width: 100%;">
    <tr style="height: 20px; ">
        <td valign="top" style="font-weight:bold; width: 50%;">Metode Pembayaran</td>
        <td style="width: 50%; text-align: right;">
            <?php echo $data['payment_data']['name']; ?>
            <br><br>
        </td>
    </tr>
    <tr>
        <td valign="top" colspan="2">
            <p style="font-weight:bold; ">Cara pembayaran di <?php echo $data['payment_data']['name']; ?>
            </p>
            <div>
                <ol style="margin: 0; padding: 0;">
                    <span style="font-weight: bold;">Apabila pembayaran via ATM BCA/Prima</span>
                    <li>Pilih menu Transaksi Lainnya</li>
                    <li> Pilih menu Transfer</li>
                    <li> Pilih menu Rek Bank lain</li>
                    <li>Masukkan Kode Permata (013) dan tekan Benar</li>
                    <li>Masukkan jumlah pembayaran sesuai dengan yang ditagihkan (Jika berbeda, transaksi akan gagal)</li>
                    <li>Masukkan Kode Pembayaran yang diberikan dan tekan Benar</li>
                    <li>Muncul layar Konfirmasi Transfer, tekan benar jika sudah sesuai (No Rek tujuan Permata, Nama Customer, Jumlah
                        yang dibayar)
                    </li>
                    <li>Terima struk sebagai bukti sukses pembayaran apabila transaksi berhasil</li>

                </ol>
                <ol style="margin: 0; padding: 0;"><span style="font-weight: bold;">Apabila pembayaran via ATM Mandiri/bersama</span>
                    <li>Pilih transaksi lainnya</li>
                    <li> Pilih menu Transfer</li>
                    <li>Pilih menu Rek Bank lain ATM bersama/Link</li>
                    <li>Masukkan Kode Permata(013) dilanjutkan Kode Pembayaran yang diberikan dan tekan benar</li>
                    <li>Masukkan jumlah pembayaran sesuai dengan yang ditagihkan (Jika berbeda, transaksi akan gagal)</li>
                    <li>Kosongkan Nomor Referensi Transfer dan tekan benar</li>
                    <li>Muncul layar Konfirmasi Transfer, tekan benar jika sudah sesuai (No Rek tujuan Permata, Nama Customer, Jumlah
                        yang dibayar)
                    </li>
                    <li>Terima struk sebagai bukti sukses pembayaran apabila transaksi berhasil</li>
                </ol>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center">
            <img src="<?php echo baseUrl('/images/payment/' . $data['payment_data']['logo']); ?>" alt="" style="width: 100%;">
            <br><br>
            <h4 style="margin-bottom: 0;">Kode Pembayaran</h4>
            <h3 style="margin-top:5px;"><?php echo $data['pay_code']; ?></h3>
        </td>
    </tr>
</table>
<hr style="display: block; height: 1px; border: 0; border-top: 1px solid #eee; margin: 1em 0; padding: 0;">
<p style="margin: 0 auto; text-align: center; width: 75%;">Harap segera melakukan pembayaran paling lambat
    <br>
    <strong><?php echo \Carbon\Carbon::parse($data['date_expired'])->format('H:i') . ' WIB, ' . \Carbon\Carbon::parse($data['date_expired'])->format('d F Y'); ?></strong>
    <br>
    atau transaksi anda akan secara otomatis kami batalkan.</p>
