@section('styles')
<style>
  .top-3 {
    margin-top: 3px;
  }
  .top-10 {
    margin-top: 10px;
  }
  #table-detail {
    border-top: 1.5px solid rgb(238,238,238);
    border-bottom: 1.5px solid rgb(238,238,238);
  }
  #inner-table-detail {
      vertical-align: top;
      border-right: 1.5px solid rgb(238,238,238);
  }
  #left-table-detail {
      padding: 10px 0 10px 20px;
  }
  #right-table-detail {
      vertical-align: top;
  }
  .dashed-border {
      padding: 0 15px;
      border-top: 1.5px dashed rgb(238,238,238);
  }
  .left-15 {
      margin-left: 15px;
  }
  .top-bottom-5{
      margin-top: 5px;
      margin-bottom: 5px;
  }
</style>
@append

<table id="table-detail" cellpadding="0" cellspacing="0">
  <tr>
    <td id="inner-table-detail" width="275">
      
      <table id="left-table-detail" width="275" height="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2">
            <p class="text-14 green-text bold-text no-margin">DETAIL PEMESANAN</p>
          </td>
        </tr>
        
        @if(isset($data['cart_data']['items'][0]['name']))
        <tr>
          <td colspan="2">
            <p class="text-14 gray-text no-margin top-10">Nama Produk</p>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <p class="text-16 bold-text no-margin top-3">{{$data['cart_data']['items'][0]['name']}}</p>
          </td>
        </tr>
        @endif

        @if(isset($data['name']))
          <tr>
            <td colspan="2">
              <p class="text-14 gray-text no-margin top-10">Dari</p>
            </td>
          </tr>
          <tr>
            @if(isset($data['store_logo_path']))
              <td width="30">
                <img class="top-3" src="{{ $data['store_logo_path'] }}" alt="" width="25"/>
              </td>
            @endif
            <td>
              <p class="text-16 bold-text no-margin top-3">{{$data['name']}}</p>
            </td>
          </tr>
        @endif

        @if(isset($data['cart_data']['items'][0]['item_detail']['number']))
        <tr>
          <td colspan="2">
            <p class="text-14 gray-text no-margin top-10">No. Pelanggan</p>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <p class="text-16 bold-text no-margin top-3">{{$data['cart_data']['items'][0]['item_detail']['number']}}</p>
          </td>
        </tr>
        @endif

        @if(isset($data['cart_data']['items'][0]['item_detail']['inquiry']['subscriber_name']))
        <tr>
          <td colspan="2">
            <p class="text-14 gray-text no-margin top-10">Nama Pelanggan</p>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <p class="text-16 bold-text no-margin top-3">{{$data['cart_data']['items'][0]['item_detail']['inquiry']['subscriber_name']}}</p>
          </td>
        </tr>
        @endif
        
      </table>
      
    </td>
    <td id="right-table-detail" width="275">
      
      <table width="275" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2">
            <p class="text-14 green-text bold-text no-margin top-10 left-15">TAGIHAN</p>
          </td>
        </tr>
        <tr>
          <td width="75">
            <img class="left-15" src="{{ $data['payment_logo_path'] }}" alt="" width="50"/>
          </td>
          <td>
            <p class="gray-text bold-text text-16">{{ $data['payment_data']['name'] }}</p>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            
            <table width="275" class="dashed-border">
            @foreach($data['receipt_payment_detail'] as $item)
              <tr>
                <td><p class="text-14 gray-text no-margin top-bottom-5">{{$item['label']}}</p></td>
                <td><p class="text-14 right-text no-margin top-bottom-5">{{$item['value']}}</p></td>
              </tr>
            @endforeach
            </table>

          </td>
        </tr>
        <tr>
          <td colspan="2">

            <table width="275" class="dashed-border">
              <tr>
                <td><p class="text-16 gray-text bold-text top-bottom-5">Total</p></td>
                <td><p class="text-16 right-text bold-text top-bottom-5">{{ $data['cart_data']['total']['formatted_amount'] }}</p></td>
              </tr>
            </table>

          </td>
        </tr>
      </table>

    </td>
  </tr>
</table>