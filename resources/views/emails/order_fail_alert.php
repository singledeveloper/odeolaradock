<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">

<h2>Failed Order: <?php echo $count; ?></h2>
<table border="1">
  <tr>
    <th>Order ID</th>
    <th>User ID</th>
    <th>Seller Store ID</th>
    <th>status</th>
    <th>Created At</th>
    <th>Opened At</th>
    <th>Paid At</th>
  </tr>
  <?php foreach ($data as $d) { ?>
    <tr>
      <td><?php echo $d->id; ?></td>
      <td><?php echo $d->user_id; ?></td>
      <td><?php echo $d->seller_store_id; ?></td>
      <td><?php echo $d->status; ?></td>
      <td><?php echo $d->created_at; ?></td>
      <td><?php echo $d->opened_at; ?></td>
      <td><?php echo $d->paid_at; ?></td>
    </tr>
  <?php } ?>
</table>
</body>
</html>