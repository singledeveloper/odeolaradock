@extends('emails.base_payment')

@section('header')
  <img src="https://s3-ap-southeast-1.amazonaws.com/odeo/mail/trx_sukses.png" style="width: 100%" alt=""/>
@endsection

@section('styles')
  <style type="text/css">
    #title {
      text-align: center;
      font-size: 24px;
      margin: 0;
      margin-top: 20px;
      font-weight: bold;
    }
  </style>
@endsection

@section('content')
    <p id="title" style="text-align: center">Transaksi Berhasil</p>
    <p class="center-text text-14" style="margin-top: 5px">Pembayaran atas transaksi Anda di <a href="{{$data['domain']}}" target="_blank" class="link">{{$data['name']}}</a> telah kami terima</p>

    @include('emails.order_summary')

    @include('emails.order_detail_table')
@endsection
