@extends('emails.base')

@section('styles')
  <style type="text/css">
    #detail-box {
      margin: 20px auto;
      background-color: rgb(248,248,248);
    }
    #inner-detail-box {
      margin: 20px 30px;
    }
    .left-box-text {
      color: rgb(144,145,146); 
      margin: 10px 0px;
      font-size: 14px;
    }
    .middle-box-text {
      color: rgb(144,145,146); 
      margin: 10px 10px;
      font-size: 14px;
    }
    .right-box-text {
      margin: 10px 0px;
      font-size: 14px;
    }
  </style>
@endsection

@section('content')
    <table id="content-table" cellpadding="0" cellspacing="0">
      <tr>
        <td>
          <p class="no-margin text-14">Dear <b>{{$data['name'] or '-'}}</b>,</p>
        </td>
        <td width="45%">
          <p class="no-margin text-14 gray-text right-text">ID Transaksi</p>
        </td>
      </tr>
      <tr>
        <td rowspan="2">
          <p class="no-margin text-14">Transaksi <b>{{$data['item_name']}}</b> Anda telah berhasil, dengan rincian sebagai berikut:</p>
        </td>
        <td>
          <p class="text-18 green-text bold-text right-text no-margin">#{{$data['order_id'] or '-'}}</p>
        </td>
      </tr>
      <tr>
        <td>
          <p class="no-margin text-14 right-text">{{$data['purchase_at']}}</p>
        </td>
      </tr>
    </table>

    <table id="detail-box">
      <tr>
        <td width="520">
        <table id="inner-detail-box" cellpadding="0" cellspacing="0">
          <tr>
            <td width="220">
              <p class="left-box-text">No Meter</p>
            </td>
            <td>
              <p class="middle-box-text">:</p>
            </td>
            <td>
              <p class="right-box-text">{{$data['meter_number'] or '-'}}</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="left-box-text">ID Pelanggan</p>
            </td>
            <td>
              <p class="middle-box-text">:</p>
            </td>
            <td>
              <p class="right-box-text">{{$data['subscriber_id'] or '-'}}</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="left-box-text">Nama Pelanggan</p>
            </td>
            <td>
              <p class="middle-box-text">:</p>
            </td>
            <td>
              <p class="right-box-text">{{$data['subscriber_name'] or '-'}}</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="left-box-text">Jumlah KWH</p>
            </td>
            <td>
              <p class="middle-box-text">:</p>
            </td>
            <td>
              <p class="right-box-text">{{$data['kwh'] or '-'}}</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="left-box-text">Stroom / Token</p>
            </td>
            <td>
              <p class="middle-box-text">:</p>
            </td>
            <td>
              <p class="right-box-text">{{$data['token'] or '-'}}</p>
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
@endsection
