<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>
<body style="font-family: 'Arial'; font-size: 14px">

Dear Mitra Odeo, <br><br>

Terlampir harga jual PPOB dari {{$data['store_name']}}, anda diperbolehkan mengubah harga untuk diperjualkan kembali.
Harga tersebut adalah harga terbaru kami per tanggal {{$data['date']}}. Segala bentuk keluhan/komplain perbedaan harga
diluar tanggal tersebut tidak kami tanggapi.<br><br>

Best Regards, <br>
PT Odeo Teknologi Indonesia
</body>
</html>