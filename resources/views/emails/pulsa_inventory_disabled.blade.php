<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
Pulsa Inventory {{ $inventory_name }} from {{ $vendor_name }} has been disabled.<br/>
Please ask the biller why this happened.
@if(!$has_backup)
  <br/><br/>THIS INVENTORY IS NOW <b>EMPTY STOCK</b>. PLEASE CHECK ASAP.
@endif
</body>
</html>