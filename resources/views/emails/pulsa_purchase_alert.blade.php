<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">

Order ID {{ $order_id }} has been completed using {{ $payment_name }} with a total of {{ $total['formatted_amount'] }}.<br/>
Item: {{ $item_name }}.<br/>
User: {{ $user_name }} - {{ $user_email }} ID: {{ $user_id }}.<br/>
Please take cautious.
</body>
</html>