@extends('emails.base_payment')

@section('header')
  <img src="https://s3-ap-southeast-1.amazonaws.com/odeo/mail/trx_gagal.png" alt="" style="width: 100%"/>
@endsection

@section('styles')
  <style>
    #title {
      text-align: center;
      margin: 0;
      margin-top: 20px;
      font-weight: bold;
    }
    #detail-box {
      background-color: rgb(248,248,248);
      padding: 20px 15px;
    }
    #description {
      margin-bottom: 20px;
      margin-top: 7px;
    }
    #footer-description {
      line-height: 1.3;
    }
    #regards {
      margin-bottom: 0;
    }
    #cs {
      margin: 0;
      margin-bottom: 15px;
      color: rgb(0,151,143);
      font-weight: bold;
    }
    #greeting {
      margin-bottom: 0px;
    }
    #description-pre {
      margin-top: 6px;
    }
    .details {
      margin-bottom: 5px;
    }
  </style>
@endsection

@section('content')
  <p id="title" class="text-24" >Transaksi Tidak Dapat Diproses</p>
  <p class="center-text text-14" id="description">Pembayaran atas transaksi Anda di <a href="{{$store['domain']}}" target="_blank" style="text-decoration: none; color: rgb(1,150,246) ">{{$store['name']}}</a> telah kami kembalikan</p>

  <p class="text-14" id="greeting">Dear <b>{{ $order['name'] }}</b>,</p>
  <p class="text-14" id="description-pre">Transaksi Anda dengan rincian sebagai berikut:</p>

  <table id="detail-box" width="100%">
    <tr>
      <td width="220">
        <p class="no-margin gray-text text-14 details">Order ID</p>
      </td>
      <td>
        <p class="no-margin gray-text text-14 details">:</p>
      </td>
      <td>
        <p class="no-margin text-14 details"><b>{{$order['id']}}</b></p>
      </td>
    </tr>

    <tr>
      <td width="220">
        <p class="no-margin gray-text text-14 details">Produk</p>
      </td>
      <td>
        <p class="no-margin gray-text text-14 details">:</p>
      </td>
      <td>
        <p class="no-margin text-14 details"><b>{{ $orderDetail['name'] }}</b></p>
      </td>
    </tr>

    <tr>
      <td width="220">
        <p class="no-margin gray-text text-14 details">No. Pelanggan</p>
      </td>
      <td>
        <p class="no-margin gray-text text-14 details">:</p>
      </td>
      <td>
        <p class="no-margin text-14 details"><b>{{ $itemNumber }}</b></p>
      </td>
    </tr>

    <tr>
      <td width="220">
        <p class="no-margin gray-text text-14">Total</p>
      </td>
      <td>
        <p class="no-margin gray-text text-14">:</p>
      </td>
      <td>
        <p class="no-margin text-14"><b>{{ $total['formatted_amount'] }}</b></p>
      </td>
    </tr>
  </table>

  <p class="text-14" id="footer-description">Transaksi tersebut tidak dapat diproses, dana pembayaran sudah kami kembalikan ke saldo Ocash Anda</p>

  <p class="text-14">Mohon maaf atas ketidaknyamanannya </p>
  
  <p class="text-14 gray-text" id="regards">Best Regards,</p>
  
  <p class="text-14" id="cs">Customer Service - ODEO</p>

  <hr class="separator"/>
@endsection
