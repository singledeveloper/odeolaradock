@extends('emails.base')

@section('styles')
    <style type="text/css">
        #otp {
            margin: 20px auto;
            padding: 20px 0px;
            text-align: center;
            font-size: 30px; 
            color: rgb(0,151,143); 
            font-weight: bold;
            background-color: rgb(248,248,248);
            width: 210px;
        }
    </style>
@endsection

@section('content')
    <p class="center-text text-24 bold-text no-margin">Kode OTP</p>
    <p class="center-text text-14" style="line-height: 1.3; margin: 14px 60px;">Jangan pernah memberitahukan ke orang lain ataupun pihak ODEO demi keamanan akun Anda</p>
    <p id="otp">{{ $otp }}</p>
@endsection
