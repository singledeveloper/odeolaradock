<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
<?php echo $body; ?><br><br>
Store ID: <?php echo $data['store']['id']; ?><br>
Store Name: <?php echo $data['store']['name']; ?><br>
Domain: <?php echo 'http://' . $data['store']['store_domain']; ?><br>
Plan: <?php echo ($data['previous_plan'] ? $data['previous_plan']['name'] . ' -> ' : '') . $data['plan']['name']; ?><br>
Invited By: <?php echo (isset($data['refer_by']) && $data['refer_by'] ? $data['refer_by'] : '-') ?><br>
</body>
</html>
