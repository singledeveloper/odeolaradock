<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
<h1>Please kindly check this suspected withdraw</h1>
<table>
<tr>
  <td>Withdraw ID</td>
  <td>Bank</td>
  <td>Inquiry ID</td>
</tr>
@foreach($data['suspected_withdraw'] as $withdraw)
  <tr>
    <td>{{$withdraw['id']}}</td>
    <td>{{$withdraw['bank']}}</td>
    <td>{{$withdraw['inquiry_id']}}</td>
  </tr>
@endforeach
</table>

</body>
</html>