<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">

<h2>TOPUP WARNING</h2>
User <strong><?php echo $data['user']['name']; ?></strong> / <strong><?php echo $data['user']['telephone']?></strong>
has added <?php echo $data['topup_amount']['formatted_amount']; ?> using
<strong><?php echo $data['payment_method']; ?></strong>. Please check this ASAP.
</body>
</html>