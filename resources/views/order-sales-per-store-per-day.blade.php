<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">

<h2>Sales Revenue</h2>
<table border="1">
  <tr>
    <th>Name</th>
    <th>Domain</th>
    <th>Total Revenue</th>
    @for($loop = 1; $loop <= $longest_days; $loop++)
      <th>{{ date('Y-m-d', time() - ($loop * 24 * 60 * 60)) }}</th>
    @endfor
  </tr>
  @foreach ($sales_value as $item)
    <?php $total = 0; foreach($item['details'] as $output) $total += round($output); ?>
    <tr>
      <td>{{ $item['name'] }}</td>
      <td>{{ $item['domain_name'] }}</td>
      <td>{{ number_format($total) }}</td>
      @foreach($item['details'] as $output)
        <td>{{ number_format(round($output)) }}</td>
      @endforeach
    </tr>
  @endforeach
</table>

<h2>Sales Count</h2>
<table border="1">
  <tr>
    <th>Name</th>
    <th>Domain</th>
    <th>Total Order Count</th>
    @for($loop = 1; $loop <= $longest_days; $loop++)
      <th>{{ date('Y-m-d', time() - ($loop * 24 * 60 * 60)) }}</th>
    @endfor
  </tr>
  @foreach ($sales_count as $item)
    <?php $total = 0; foreach($item['details'] as $output) $total += $output; ?>
    <tr>
      <td>{{ $item['name'] }}</td>
      <td>{{ $item['domain_name'] }}</td>
      <td>{{ $total }}</td>
      @foreach($item['details'] as $output)
        <td>{{ $output }}</td>
      @endforeach
    </tr>
  @endforeach
</table>

</body>
</html>