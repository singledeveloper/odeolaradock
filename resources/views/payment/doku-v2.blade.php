<style>
    /*#request-form {*/
    /*opacity: 0;*/
    /*}*/

    #request-form input {
        display: block;
        margin: 0 0 10px 0;
    }

    form {
        opacity: 0;
    }

</style>


redirect to doku..

<form id="request-form" action="{{$URL}}" method="post">
    <input type="text" name="MALLID" value="{{$MALLID}}">
    <input type="text" name="CHAINMERCHANT" value="{{$CHAINMERCHANT}}">
    <input type="text" name="AMOUNT" value="{{$AMOUNT}}">
    <input type="text" name="PURCHASEAMOUNT" value="{{$PURCHASEAMOUNT}}">
    <input type="text" name="TRANSIDMERCHANT" value="{{$TRANSIDMERCHANT}}">
    @if(isset($TENOR))
        <input type="text" name="TENOR" value="{{$TENOR}}">
    @endif
    @if(isset($PROMOID))
        <input type="text" name="PROMOID" value="{{$PROMOID}}">
    @endif
    @if(isset($INSTALLMENT_ACQUIRER))
        <input type="text" name="INSTALLMENT_ACQUIRER" value="{{$INSTALLMENT_ACQUIRER}}">
    @endif
    @if(isset($PAYMENTTYPE))
        <input type="text" name="PAYMENTTYPE" value="{{$PAYMENTTYPE}}">
    @endif
    <input type="text" name="REQUESTDATETIME" value="{{$REQUESTDATETIME}}">
    <input type="text" name="WORDS" value="{{$WORDS}}">
    <input type="text" name="CURRENCY" value="{{$CURRENCY}}">
    <input type="text" name="PURCHASECURRENCY" value="{{$PURCHASECURRENCY}}">
    <input type="text" name="SESSIONID" value="{{$SESSIONID}}">
    <input type="text" name="EMAIL" value="{{$EMAIL}}">
    <input type="text" name="NAME" value="{{$NAME}}">
    <input type="text" name="BASKET" value="{{$BASKET}}">
    <input type="text" name="PAYMENTCHANNEL" value="{{$PAYMENTCHANNEL}}">
    <input type="submit" value="submit">

</form>


<script>
    document.getElementById('request-form').submit();
</script>